<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>İptal</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfigürasyon</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Bağlandı</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation> Tarih</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Tarih</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Sil</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Etkin</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dosya Adı</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>Dosyalar</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP Adresi</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresi</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mesaj</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Sonraki</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Hayır.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hayır</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Hiçbiri</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Uzaktan Erişim</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Evet</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
