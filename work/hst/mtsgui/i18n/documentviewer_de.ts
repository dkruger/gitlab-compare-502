<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Beenden</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>Laden: </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>Beim Laden der PDF-Datei ist ein Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>Fehler beim Rendern von Seite: </translation>
        </message>
    </context>
</TS>
