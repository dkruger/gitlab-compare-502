<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>유효하지 않은 IP 주소 할당이 거부되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>유효하지 않은 게이트웨이 할당 : 172.29.0.7. MTS 시스템 재시작하세요 .</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>IP 주소가 변경되었습니다 .  BERT 모듈을 재시작하세요 .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>BERT 모듈 초기화 중 .  OFF 요청이 거부되었음 .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>Optical 지터 기능으로 BERT 모듈 초기화 중 . OFF 요청이 거부되었음 .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>BERT 모듈 종료</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>BERT 모듈 OFF</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>BERT 모듈이 꺼져 있습니다 . HOME/SYSTEM 을 누른 다음 BERT 아이콘을 눌러 모듈을 활성화 하세요 .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>BERT 모듈 ON</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** 애플리케이션 소프트웨어 업그레이드 중 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** 업그레이드 완료 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** 지터 소프트웨어 업그레이드 중 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** 커널 소프트웨어 업그레이드 중 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** 업그레이드 시작 *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>BERT 모듈 UI 실패</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Optical 지터 기능으로 BERT 모듈 UI 실행 중</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>BERT 모듈 UI 실행 중</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>BERT 모듈 UI 실패 . 모듈 OFF</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>모듈 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>모듈 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>모듈 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>모듈 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>모듈 </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>모듈 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>모듈 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>모듈 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>모듈 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>모듈 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>모듈 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>모듈 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>모듈 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>모듈 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>모듈 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>모듈 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>모듈 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>모듈 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>모듈 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>모듈 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>모듈 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>모듈 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>모듈 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>기반 소프트웨어를 업그레이드 해야 합니다 . 현재의 버전은 BERT 모듈과 호환되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>BERT 소프트웨어를 재설치 해야 합니다 . 첨부된 BERT 모듈을 위한 올바른 BERT 소프트웨어가 설치되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>BERT 하드웨어를 업그레이드 해야 합니다 . 현재의 하드웨어는 호환되지 않습니다 .</translation>
        </message>
    </context>
</TS>
