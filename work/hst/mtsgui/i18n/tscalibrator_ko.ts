<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CEvdevCalibrator</name>
        <message utf8="true">
            <source>Please wait...</source>
            <translation>기다려 주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Calibration timed out</source>
            <translation>보정 타임아웃</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CTsCalibrateWindow</name>
        <message utf8="true">
            <source>Touch crosshair to calibrate the touchscreen.</source>
            <translation>터치스크린을 보정하기 위해 십자 커서를 터치하세요.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CTslibCalibrator</name>
        <message utf8="true">
            <source>Calibration timed out</source>
            <translation>보정 타임아웃</translation>
        </message>
    </context>
</TS>
