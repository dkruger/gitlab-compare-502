<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi 비디오 플레이어</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>종료</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>비디오 파일 이름</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>시간:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>비디오 상태</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>크기 조정 및 자르기</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>스케일</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>열기 &amp; 파일 ...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>화면 비율</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>자동</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>스케일</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>스케일 모드</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>화면에 맞추기</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>크기 조정 및 자르기</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>파일 열기 ...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>멀티미디어 (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>열기</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>오픈 미디어 없음</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>중지됨</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>로딩 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>버퍼링...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>재생 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>일시 정지</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>에러...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>유휴 - 미디어 정지 중</translation>
        </message>
    </context>
</TS>
