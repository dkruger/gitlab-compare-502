<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Verificação CPRI</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>Configurar</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>Editar configuração anterior</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>Carregar a configuração de um perfil</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>Inicie uma nova configuração (para o padrão)</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>Manualmente</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>Configurar manualmente os ajustes do teste.</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>Configurações do teste</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>Salvar perfis</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>Fim: Configurar manualmente</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>Execute os testes</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>Armazenado</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>Perfis de carga</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>Fim: Perfis de carga</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>Editar configuração</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Teste</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Executar</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>Executar verificação CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>Verificação SFP</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>Fim: Teste</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Criar relatório</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>Repetir o teste</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>Visualizar resultados detalhados</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>Sair Verificação CPRI</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>Revisão</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultados</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>Interface</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Nível 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>Fim: Revisar Resultados</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Relatório</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>Informações do relatório</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>Fim: Criar relatório</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>Revisar resultados detalhados</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Executando</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Teste abortado</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>INCOMPLETO</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>COMPLETO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALHA</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASSA</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NENHUMA</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>Verificação CPRI:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** Iniciar verificação de CPRI ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** Teste concluído ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** Teste cancelado ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>Pular salvar perfis</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>Você pode salvar a configuração utilizada na realização deste teste.&#xA;&#xA;Ela poderá ser usada (toda ou parte, selecionando "perfis" individuais) para configurar testes futuros.</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>Pular a carga de perfis</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>Verificação SFP locais</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Conector</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>Insira um SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>Comprimento de onda SFP (nm):</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>Fornecedor SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>Rev fornecedor SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>P/N Fornecedor SFP</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>Taxas recomendadas</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>Mostrar dados SFP adicionais</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP está boa.</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>Não é possível verificar SFP para esta taxa.</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP não é aceitável.</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Ajustes</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Duração do Teste</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>Dispositivo de extremo remoto</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Outro</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>Hard Loop</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sim</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Não</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>Limite Máx. de nível Rx ótico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>Limite Mín. de nível Rx ótico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>Limite Máx. de Atraso de ida e volta (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>Pular verificação CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>Verificação SFP</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>Chave de status do teste</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Completada</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Passa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>Agendado</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>Executar&#xA;teste</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Parar&#xA;Teste</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>Resultados SFP locais</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>Nenhum SFP detectado</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Comprimento de onda (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Máx nivel Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Máx nivel Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Transceptor</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>Resultados de interface</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>CPRI Verificar vereditos de teste</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>Teste Interface</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>Camada 2 Teste</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>Taste RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>Taste BERT</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>Sinal está presente</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Sinc obtido</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Desvio máximo da frequência Rx (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violações de código</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>Nível Rx óptico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>Resultados de camada 2</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>Estado de Inicialização</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>Sinc. quadros</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>Resultados RTD</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>Média atraso ida e volta (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>Resultados BERT</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>Sinc de padrões</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>Perdas de padrões</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Erros Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>Configurações</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>Tipo de equipamento</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>Sincronização L1</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>Configuração de Protocolo</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>C&amp;M Instalação de Avião</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>Operação</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>Ligação Passiva</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>Pular a criação do relatório</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informações do relatório de teste</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>Nome do cliente:</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>ID de Técnico:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>Local do teste:</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>Ordem de Serviço:</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>Comentários/Notas:</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>Rádio:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>Banda:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>Situação Geral</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Teste FC aprimorado</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>Taste FC</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Simetria</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>Configurações locais</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>Conectar ao remoto</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Rede</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>Configurações de Canal de Fibra</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>Teste de FC</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>Modelos de Configuração</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Selecione os testes</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>Utilização</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Comprimento de quadros</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Teste de vazão (throughput)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Teste de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>Teste de verificação</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Teste de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>Ctls do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>Durações Teste</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>Limites Teste</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>Alterar configuração</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>Configurações Avançadaa de Canal de Fibra</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>Configurações avançadas de Utilização</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>Configurações avançadas de latência Taxa de transferência</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>Configurações avançadas de teste de verificação.</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>Configurações avançadas de teste perda de processos</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>Execute o teste de ativação do serviço</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>Alterar a configuração e executar o teste novamente</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latência</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdas de quadros</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Verificação</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Vazão de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>Sair do teste FC</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>Criar um outro relatório</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>Página Inicial</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>A sincronização One Way Delay Time Source foi perdida durante a realização do teste na unidade local.  Verifique as conexões do hardware de OWD Time Source.  O teste continuará se não for completado, mas os resultados do Frame Delay não estarão disponíveis.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>A sincronização One Way Delay Time Source foi perdida durante a realização do teste na unidade remota.  Verifique as conexões do hardware de OWD Time Source.  O teste continuará se não for completado, mas os resultados do Frame Delay não estarão disponíveis.</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>Circuito ativo encontrado</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>A resolução do endereço vizinho não teve sucesso.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>Serviço #1: Enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>Serviço #1 no lado Local: Enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>Serviço #1 no lado Remoto: Enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>Lado local enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>Lado remoto enviando solicitação ARP para MAC do destino.</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>A porta do elemento de rede para operação half duplex. Se você quiser continuar, pressione o botão "Continuar em Half Duplex". Senão, pressione "Abortar teste".</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>Tentando um loop up. Verificando existência de loop ativo.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>Verificando loop de hardware.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Verificação de um circuito LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>Verificando loop permanente.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>Tempo esgotado de solicitação de parâmetro DHCP. O parâmetro DHCP não pôde ser obtido.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>Ao selecionar o modo Loopback você foi desconectado da unidade remota</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>O tempo de SAMComplete se esgotou porque não foi possível determinar uma contagem final de quadros recebidos. A contagem medida de quadros recebidos continuou a crescer inesperadamente.</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>Circuito rígido encontrado</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>O J-QuickCheck não pode executar o teste de conectividade de trafego a menos que seja estabelecida uma conexão com a unidade remota.</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>Circuito LBM/LBR encontrado</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>O link local foi perdido e a conexão à unidade remota foi cortada. Assim que o link for restabelecido você poderá tentar se conectar novamente à extremidade remota.</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>O link não está ativo no momento.</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>Os IP de origem e de destino são idênticos. Reconfigure o endereço IP da origem ou do destino.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo local é um aplicativo Traffic e o remoto, no endereço IP #1, é um aplicativo Streams.</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>Nenhum loop pôde ser estabelecido</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>Não foi estabelecida a conexão com a unidade remota. Vá até a página Connect, verifique o endereço IP de destino e então pressione o botão Connect to Remote.</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Ir para a página "Rede", verifique as configurações e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>À espera que o conetor ótico esteja ativo ou não está presente</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>Circuito Permanente Encontrado</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>Tempo limite de conexão PPPoE. Por favor, verifique as configurações de PPPoE e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>Um erro PPPoE irrecuperável foi encontrado</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Tentando fazer Log-on no servidor ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>Foi detectado um problema na conexão remota. A unidade remota pode não estar mais acessível</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>Não foi possível estabelecer uma conexão com a unidade remota no endereço IP #1. Verifique seu endereço IP de origem remoto e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>O aplicativo remoto no #1 endereço de IP é um aplicativo de loopback. Não é compatível com o RFC 2544 Aprimorado.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo remoto, no endereço IP #1, não é um aplicativo TCP WireSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>A versão de software na unidade remota parece ser mais recente. Se você continuar a testar, algumas funcionalidades poderão ser limitadas. É recomendável atualizar o software nesta unidade para um desempenho ideal.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>A versão de software na unidade remota parece ser mais antiga. Se você continuar a testar, algumas funcionalidades poderão ser limitadas. É recomendável atualizar o software na unidade remota para um desempenho ideal.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>A versão de software na unidade remota não pôde ser determinada. Se você continuar a testar, algumas funcionalidades poderão ser limitadas. É recomendável testar as unidades com as versões equivalentes de software.</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>Tentativa de loop up sem sucesso Tentando novamente.</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>As configurações para o modelo selecionado foram aplicadas com sucesso.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>A versão do software na unidade remota não pode ser determinada ou não corresponde à versão nesta unidade. Se você continuar o teste, algumas funcionalidades podem estar limitadas. É recomendável testar entre as unidades com versões equivalentes do software.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>Não foi possível completar login explícito.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo local é um aplicativo Layer #1 e o remoto, no endereço IP #2, é um aplicativo Layer #3.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>A taxa de linha de unidade local (#1 Mbps) não coincide com o da unidade remota (#2 Mbps). Favor reconfigurar para Assimétrica na página "Conectar".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>A taxa de linha de unidade local (#1 kbps) não coincide com o da unidade remota (#2 kbps). Favor reconfigurar para Assimétrica na página "Conectar".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>A taxa de linha de unidade local (#1 Mbps) não coincide com o da unidade remota (#2 Mbps). Por favor, reconfigurar para assimétrica na página de "Conect" e reinicie o teste.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>A taxa de linha de unidade local (#1 kbps) não coincide com o da unidade remota (#2 kbps). Favor reconfigurar para Assimétrica na página "Conectar" e reiniciar o teste.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>A taxa de linha da unidade local (#1 Mbps) não coincide com o da unidade remota (#2 Mbps). Reconfigure para a página Assimétrico na “Simetria” de RFC2544.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>A taxa de linha de unidade local (#1 kbps) não coincide com o da unidade remota (#2 kbps). Por favor, reconfigurar a assimétrica na pagina "Symmetry" RFC2544.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>Configuração inválida:&#xA;&#xA;Pelo menos um teste deve ser selecionado. Favor seleciona pelo menos um teste e tentar novamente.</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>Você não selecionou nenhum tamanho de processo para testar. Por favor, selecione pelo menos um tamanho da processo antes de começar.</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>A taxa de perda de informação que excedeu o limite de perda de informações configurado foi detectado no último segundo. Por favor, verifique o desempenho do link com um teste de tráfego manual. Depois de ter feito os testes manuais você pode executar novamente RFC 2544.&#xA;Configuração de teste manual recomendada:&#xA;Tamanho do quadro: #1 bytes&#xA;Tráfego: Constante com #2 Mbps &#xA;Duração do teste: Pelo menos 3 vezes a duração do teste configurado. A Duração do teste de transferência foi #3 segundos.&#xA;Resultados para monitorar: &#xA;Use a tela Resumo do status para procurar eventos de erro. Se os contadores de erro são incrementando de forma esporádica, executar o teste manual em diferentes taxas de tráfego mais baixos. Se você ainda obtiver erros, mesmo em taxas mais baixas o link pode ter problemas gerais não relacionados com a carga máxima. Você também pode usar os resultados gráficos Taxa de Perda de Quadro para ver se há eventos de perda quadro esporádicos ou constantes. Se você não conseguir resolver o problema com os erros esporádicos pode definir o Limíte de Perda de Quadros para tolerar pequenas taxas de perda de quadros. Observação: Quando você usa uma tolerância de perda de quadro, o teste não está em conformidade com a recomendação RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>Observação:  Devido às profundidades de pilha VLAN diferentes para o transmissor e o receptor, a taxa configurada será ajustada para compensar a diferença de taxa entre as portas.</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>Quadros de #1 bytes</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>Pacotes de #1 bytes</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>A contagem de filtro de processo L2 Rx Acterna continua a incrementar durante os últimos 10 segundos, embora o tráfego deva ser interrompido</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Zerar na taxa máxima de vazão</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>Tentando #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>Tentando #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>Tentando #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>Tentando #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>Tentando #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>Verificando #1 L1 Mbps. Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>Verificando #1 L2 Mbps. Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>Verificando #1 L1 kbps. Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>Verificando #1 L2 kbps. Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>Verificando #1 %. Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>Taxa de transferência máxima medida: #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>Taxa de transferência máxima medida: #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>Taxa de transferência máxima medida: #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>Taxa de transferência máxima medida: #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>Máxima vazão medida: #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Não está disponível uma medição de vazão máxima</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>Teste de Perda de Processo (Padrão RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>Teste de Perda de Processo (de cima para baixo)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>Teste de Perda de Processo (De baixo para cima)</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>Rodando teste a #1 L1 Mbps de carga Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>Rodando teste a #1 L2 Mbps de carga Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>Rodando teste a #1 L1 kbps de carga Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>Rodando teste a #1 L2 kbps de carga Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>Executando o teste com carga #1 %.  Isso levará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Teste de quadros 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>Ensaio #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Quadro(s) de pausa detectado(s)</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 burst de pacote: falha</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 burst de quadro: falha</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 burst de pacote: passa</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 burst de quadro: passa</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>Teste de Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>Tentando uma ruptura de #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>Mais que #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>Menos que #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>O tamanho do Buffer é #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>O tamanho do Buffer é menor que #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>O tamanho do Buffer é maior ou igual a #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>#1 processos enviados</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>#1 Quadros recebidos</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>#1 Quadros perdidos</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>Teste de Ruptura (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>CBS Estimado: #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>CBS Estimado: Indisponível</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>O teste CBS será ignorado. O Tamanho do teste de ruptura é muito grande para testar esta configuração com precisão.</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>O teste CBS será ignorado. A Duração do teste não é longa o suficiente para testar precisamente esta configuração.</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>Explosão de pacote: falha</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>Explosão em quadro: falha</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>Explosão de pacote: passou</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>Explosão em quadro: passou</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>Explosão de Resultados de Ensaio de Policiamento #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>Teste de recuperação do sistema</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>Teste não é válido para este tamanho de pacote</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>Teste não é válido para este tamanho de processo</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>Ensaio #1 de #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>Não será possível induzir perda de quadros porque o teste de vazão passou, com largura máxima de banda, sem que se observasse perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>Incapaz de induzir qualquer evento de perda Teste não é válido para este tamanho de pacote</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>Incapaz de induzir qualquer evento de perda Teste não é válido para este tamanho de processo</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>Tempo médio de recuperação: Mais que #1 segundos</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>Mais que #1 segundos</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>Tempo médio de recuperação: Indisponível</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>Tempo médio de recuperação: #1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 nós</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>Mirar no buffer de de crédito ideal. Teste #1 créditos</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>Testando #1 créditos</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>Verificando agora com #1 créditos.  Isso levará #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Teste de vazão de créditos de buffer</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>Observação: Assume um loop rígido com créditos de buffer menos de 2. Este teste é inválido</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>Nota: Baseado na suposição do loop físico, as medições throughput são feitas o dobro de vezes do que os créditos buffer em cada etapa para compensar o dobro do comprimento de onda</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>Medindo a vazão a #1 créditos de buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>Testes de vazão e latência</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Testes de vazão e de jitter de pacotes</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>Testes de Taxa de transferência, Latência e Pacote de instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>Teste de Latência e Pacote jitter #1. Isso vai levar #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>Ensaio de teste de pacote Jitter #1. Isso vai levar #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>Ensaio de teste de latência #1. Isso vai levar #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>Ensaio de teste de latência #1 em #2% da carga de rendimento verificada. Isso vai levar #3 segundos</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 Iniciando teste de RFC 2544 #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 Iniciando teste de FC #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>Teste completo.</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvando resultados, aguarde.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>Teste de Carga Estendida</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>Taste FC:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>Configuração de Rede</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Tipo de quadro</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>Modo de teste</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>Manutenção do Nível de Domínio</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>Remetente TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulamento</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profundidade da pilha VLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridade de usuário da SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>Bit DEI da SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>TPID da SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID de usuário da SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>Prioridade de usuário da CVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridade do usuário</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>Prioridade do usuário SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>TPDI da SVLAN 7 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 7 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>Bit DEI da SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>Prioridade do usuário SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>TPDI da SVLAN 6 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 6 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>Bit DEI da SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>Prioridade do usuário SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>TPDI da SVLAN 5 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 5 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>Bit DEI da SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>Prioridade do usuário SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>TPDI da SVLAN 4 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 4 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>Bit DEI da SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>Prioridade do usuário SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>TPDI da SVLAN 3 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 3 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>Bit DEI da SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>Prioridade do usuário SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>TPDI da SVLAN 2 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 2 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>Bit DEI da SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>Prioridade do usuário SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>TPDI da SVLAN 1 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>TPID de usuário da SVLAN 1 (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>Bit DEI da SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Tipo de Loop</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC da origem</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>Auto-incremento de SA MAC</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation># MACs em sequência</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>Desativar EtherType do IP</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>Desativar Resultados OoS</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>Tipo DA</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC do destino</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Modo de dados</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>Usar Autenticação</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nome do usuário</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Senha</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>Provedor de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nome do serviço</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>Tipo de IP de origem</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Endereço IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>Gateway padrão</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>Máscara de subrede</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>Endereço IP do destino</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Tempo de vida (hops)</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>Incrementação IP ID</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>Fonte de endereços de conexão local</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>Fonte global de endereços</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Comprimento do prefixo da subrede</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Endereço do destino</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>Classe de tráfego</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>Rótulo de fluxo</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>Limite do Hop</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>Modo de tráfego</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>Porta Fonte de Tipo de Serviço</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>Porta de origem</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>Tipo de seviço de porta de destino</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>Porta de destino</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>Tipo IP de escuta ATP</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>Endereço IP de escuta ATP</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID da origem</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID do destino</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID da sequência</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID do originador</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID do respondente</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Configuração do teste</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Acterna Versão Payload</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>Precisão da Medição de Latência</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>Unidade de banda larga</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>Teste de Banda larga máxima (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>Teste de Banda larga máxima Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>Teste de Banda larga máxima (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>Teste de Banda larga máxima Upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>Teste de Banda larga máxima Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>Teste de Banda larga máxima Downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>Teste de Banda larga máxima (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>Permitir 100% de Tráfico real</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>Precisão da medição de taxa de transferência</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>Precisão da medição de taxa de transferência Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>Precisão da medição de taxa de transferência Downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Processo de zerar na vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolerância de perda de quadros na vazão (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolerância de perda de processo Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolerância de perda de processo Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>Duração de testes (s)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>Número de tentativas de todos os testes</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>Duração de transferência (s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Limiar para passar na vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>Limite passagem de taxa de transferência (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>Limite passagem de taxa de transferência Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>Limite passagem de taxa de transferência (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>Limite passagem de taxa de transferência Upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>Limite passagem de taxa de transferência Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>Limite passagem de taxa de transferência Downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>Limiar para passar na vazão (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>Número de Testes Latência.</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>Duração do teste de Latência (s)</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>Largura de banda da latência (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>Limite de passagem de Latência</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>Limite de Latência de passagem (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>Limite de Latência de passagem Upstream (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>Limite de Latência de passagem Downstream (us)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Número de ensaios de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>Duração do teste do pacote de instabilidade (s)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Limiar para passar em jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>Limite de passagem do pacote de instabilidade (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>Limite de passagem do pacote de instabilidade Upstream (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>Limite de passagem do pacote de instabilidade Downstream (us)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Procedimento de teste de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>Duração teste de perda de processo</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularidade de perda de processo de banda larga (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularidade de banda larga de perda de processo Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularidade de perda de processo de banda larga (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularidade de banda larga de perda de processo Upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularidade de banda larga de perda de processo Downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularidade de banda larga de perda de processo Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>Granularidade da banda em perda de quadros (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>Faixa Mínima de Perda de Processo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>Faixa Mínima de Perda de Processo (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>Faixa Mínima de Perda de Processo (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>Faixa Máxima de Perda de Processo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>Faixa Máxima de Perda de Processo (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>Faixa Máxima de Perda de Processo (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Faixa Mínima de Perda de Processo Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>Faixa Mínima de Perda de Processo Upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Faixa Máxima de Perda de Processo Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>Faixa Máxima de Perda de Processo Upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Faixa Mínima de Perda de Processo Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>Faixa Mínima de Perda de Processo Downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Faixa Máxima de Perda de Processo Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>Faixa Máxima de Perda de Processo Downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>Número de passos de perda de processo.</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>Número de tentativas de verificação</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>Verificação de granularidade (processos)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>Verificação de duração de ruptura máxima</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>Verificação de duração de ruptura máxima Upstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>Verificação de duração de ruptura máxima Downstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>Ignorar quadros de pausa</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>Teste de Tipo de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>Tamanho (kB) de Ruptura de CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>Tamanho de Ruptura Upstream de CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>Tamanho de Ruptura Downstream de CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>Tamanho mínimo (kB) da Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>Tamanho máximo (kB) da Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>Tamanho mínimo (kB) da Pesquisa de Ruptura Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>Tamanho máximo (kB) da Pesquisa de Ruptura Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>Tamanho mínimo (kB) da Pesquisa de Ruptura Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>Tamanho máximo (kB) da Pesquisa de Ruptura Downstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>CBS Duração(s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>Número de tentativas de teste de ruptura</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>Mostrar Mostrar aprovação / reprovação de Ruptura de CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>Exibir Limite de apovação/reprovação da Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>Tamanho limite (kB) da Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>Tamanho limite (kB) da Pesquisa de Ruptura Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>Tamanho limite (kB) da Pesquisa de Ruptura Downstream</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>Número de Tentativas de Recuperação do Sistema</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>Tempo de recuperação de sobrecarga do sistema</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>Duração de carga estendida (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>Escalonamento da saída da carga estendida (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>Comprimento do pacote de carga estendida</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>Comprimento do quadro da carga estendida</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>Buffer de Crédito de Tipo Entrada</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>Buffer de Crédito de tamanho máximo do buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>Buffer de crédito de etapas da vazão</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>Buffer de duração de Crédito</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>Loop down remoto bem sucedido: Unid **#1** do modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>Loop down remoto bem sucedido: Unid **#1** do modo LLB transparente</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>A unidade remota **#1** já estava looped down</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>Loop Remoto Para Baixo Devido à Mudança de Configuração</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>Loop up remoto bem sucedido: Unid **#1** em modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>Loop up remoto bem sucedido: Unid **#1** em modo LLB transparente</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>A unidade remota **#1** já estava em modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>A unidade remota **#1** já estava em modo LLB transparente</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>Selfloop ou loop de outra porta não é admitido</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Fluxo #1: Loop down remoto sem sucesso: tempo limite do reconhecimento</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Fluxo #1: Loop up remoto sem sucesso: tempo limite do reconhecimento</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop down remoto sem sucesso: tempo limite do reconhecimento</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop down remoto transparente sem sucesso: tempo limite do reconhecimento</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop up remoto sem sucesso: tempo limite do reconhecimento</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop up remoto transparente sem sucesso: tempo limite do reconhecimento</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>Iniciada detecção de endereço global duplicado.</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>Falha na configuração do endereço global (verifique as configurações).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>Aguardando detecção de endereço global duplicado.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>Detecção bem sucedida de endereço duplicado. O endereço global é válido.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>Falha na detecção do endereço duplicado. O endereço global é inválido.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>Iniciada detecção de endereço Link-Local duplicado.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>Falha na configuração do endereço Link-Local (verifique as configurações).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>Aguardando detecção de endereço link-local duplicado.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>Detecção bem sucedida de endereço duplicado. O endereço Link-Local é válido.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>Falha na detecção do endereço duplicado. O endereço Link-Local é inválido.</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>Iniciada recuperação de IP stateless</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>Falha na obtenção de IP stateless.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>IP stateless obtido com sucesso.</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>Iniciada recuperação de IP stateful</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>Não foram encontradas rotas para fornecimento do IP stateful.</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>Tentando novamente a recuperação do IP steteful.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>IP stateful obtido com sucesso.</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>Rede fora de alcance</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>Host fora de alcance</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>Protocolo fora de alcance</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>Porta fora de alcance</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>Mensagem longa demais</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>Rede de destino desconhecida</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>Host de destini desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>Rede de destino proibida</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>Host de destino proibido</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>Rede inalcançável para TOS</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>Host inalcançável para TOS</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Excedido o tempo durante o trânsito</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>Excedido o tempo durante a remontagem</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>Erro ICMP desconhecido</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>Destino fora de alcance</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>Endereço fora de alcance</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>Não há rota para o destino</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>O destino não é um vizinho</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>A comunicação com o destino está administrativamente proibida</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>Pacote grande demais</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Limite de hop excedido em trânsito</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>Excedido o tempo na remontagem do fragmento</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>Encontrado campo de cabeçalho com erro</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>Encontrado tipo do próximo cabeçalho não reconhecido</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>Encontrada opção IPv6 não reconhecida</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>Inativo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE ativo</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP ativo</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Rede ligada</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Usuário solicitou inativa</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Nível de dados parado</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Tempo limite do PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Tempo limite do PPP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>Falha no PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>Falha no PPP LCP</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Falha na autenticação do PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>Falha desconhecida no PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>Falha na ligação do PPP</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config inválido</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Erro interno</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MÁX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP bem sucedido. MAC do destino obtido</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>Aguardando serviço ARP...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>Sem ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abortar</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continuar em half-duplex</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>Parar o J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>Taste RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>Teste J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>Status loop remoto</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>Status de loop local</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>Status PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>Status IPv6</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>Status ARP</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>Status DHCP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>Status ICMP</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>Status de descoberta de vizinhos</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>Status Autoconfig</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>Status endereço</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>Descoberta de Unidade</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>Pesquisar unidades</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Procurando</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>Usar unidade selecionada</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>Saindo</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fechar</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>Conectar&#xA;à remota</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>Conectar&#xA;ao remoto</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Desconectado</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>Conectado&#xA; ao remoto</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>LIGADO</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Conectando</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Desliga</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simétrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Assimétrico</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>Unidirecional</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Loopback</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>Downstream e Upstream</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>Ambos os diretórios</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>Vazão (throughput):</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>Rendimentos de Downstream e Upstream são iguais.</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>Rendimentos de Downstream e Upstream são diferentes.</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>Somente testar o network em uma só  direção.</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>Medições:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>Tráfego é gerado localmente e as medidas são tomadas localmente.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>Tráfego é gerado local e remotamente, e as medidas são tomadas em cada direção.</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>Direção da medição:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>Tráfego é gerado localmente e medido pelo instrumento remoto do teste.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>Tráfego é gerao remotamente e é medido pelo instrumento do teste local.</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>Atraso de viagem</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Atraso em um só sentido</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Apenas medições de atraso de viagem. &#xA;A unidade remota não tem capacidade para atraso em sentido único.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Apenas medições de atraso em sentido único. &#xA;A unidade remota não tem capacidade para RTD bidirecional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>Nenhum atraso de medições pode ocorer. &#xA;A unidade remota não tem capacidade para RTD bidirecional ou atraso em um sentido.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Nenhum atraso de medições pode ocorer. &#xA;A unidade remota não tem capacidade de atraso em um sentido.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Nenhum atraso de medições pode ocorer. &#xA;A unidade remota não tem capacidade para RTD bidirecional.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Apenas medições de atraso em sentido único.&#xA;RTD não é suportado em teste unidirecional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Nenhum atraso de medições pode ocorer. &#xA;RTD não é suportado quando o teste é unidirecional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Nenhum atraso de medições pode ocorer. &#xA;A unidade remota não tem capacidade de atraso em um sentido.&#xA; RTD não é suportado em teste unidirecional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>Nenhum atraso de medições pode ocorer.</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>Tipo de Medição de Latência</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>Necessita instrumento de teste Viavi remoto.</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>Versão 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>Versão 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Calibração RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>Seleção ótica</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>Configurações do IP para canal de comunicação para extremidade distante</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>Configuração de  Endereços de IP Local não é requerida para o teste de Loopback.</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Estático</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>Estático - Por serviço</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>Estático - Simples</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manual</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>Stateful</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Stateless</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP da origem</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>Link Src - Endereço local</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>Endereço global Src</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>Obtida automaticamente</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>Def. Usuário</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Endereço MAC   </translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>Modo Arp</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>Tipo de endereço de origem</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>Modo PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>Avançado:</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Ativado</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Desabilitado</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>MAC da origem do cliente</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>Padrão de fábrica</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>MAC padrão da origem</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>MAC padrão do cliente</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>MAC da origem do usuário</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>Cliente MAC do usuário</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>Pular etapa de conexão</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-em-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>VLAN empilhada</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>Quantas VLANs são usadas na porta de rede local?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>Nenhum VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLANs (VLAN empilhada)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>Introduzir definições de Identificação VLAN:</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>Pilha de Profundidade</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>Usuário</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>Configurações do Servidor Discovery</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>Nota: um IP de destino local do link não pode ser usado para "Conetar ao remoto"</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP do destino</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>IP de destino global</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Pingar</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>Ajuda-me a encontrar o &#xA;IP de destino</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Porta local</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Status desconhecido</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>UP (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>UP (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>UP (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>UP (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>UP (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>UP (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>UP (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>UP (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>UP (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Link perdido</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>Porta local:</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Auto negociação</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analisando</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>DESLIGADO</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Auto negociação:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>Aguardando para Conectar</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Conectando...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>Tentando outra vez...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONECTADOS</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>Conexão Falhou</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>Conexão Abortada</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>Canal de comunicação:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>Configurações avançadas do servidor Discovery</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>Obter IP de destino de um Servidor Discovery</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>IP do servidor</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>Porta do Servidor</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>Senha de Servidor</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>Tempo de concessão solicitado (min.)</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>Tempo de concessão autorizado (min.)</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>Configuração da Rede L2 - Local</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>Configurações da unidade local</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>Tráfego</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>Tráfego LBM</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 (o mais baixo)</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 (o mais alto)</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>TPID de usuário da SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>Definir Tipo de Loop, Tipo de Ethernet e Endereços MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>Definir endereços MAC, Tipo de Ethernet e LBM</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>Definir os endereços MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>Definir os endereços MAC e Modo ARP</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Tipo do destino</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>Unicast</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>Multicast</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>Broadcast</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>Prioridade de usuário da VLAN</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>Unidade remota não conectada.</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>Configuração da Rede L2 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>Configurações da unidade remota</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>Configuração da Rede L3 - Local</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>Definir classe de tráfego, etiqueta de fluxo e limite de hop</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>Que tipo de priorização de IP é usado por sua rede?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>Definir Vida Útil, Incrementação de IP ID e modo PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>Definir tempo para ativo e modo PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>Definir Vida Útil e Incrementação de IP ID</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>Definir tempo para ativo</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>Qual priorização de IP é usado por sua rede?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>Configuração da Rede L3 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>Configuração da Rede L4 - Local</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>Tipo de serviço da origem</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>Clinte DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>Servidor DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>Finger</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>Tipo de serviço do destino</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>Definir ATP de Escuta de IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>Configuração da Rede L4 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>Definir Tipo de Circuito e seqüência, interlocutor, e IDs do Originador</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>Configurações avançadas L2 - Local</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Tipo da origem</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC padrão</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC do usuário</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>Configuração LBM</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>Tipo LBM</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>Ativar Expedidor TLV</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ligado</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>Configurações avançadas L2 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>Configurações avançadas L3 - Local</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Tempo de vida (hops)</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>Configurações avançadas L3 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>Configurações avançadas L4 - Local</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>Configurações avançadas L4 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>Configurações avançadas de Rede - Local</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>Modelos</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>Deseja usar um modelo de configuração?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Taxa de acesso pacote Viavi aprimorada> Taxa de Transporte</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - Melhor Esforço</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - Continental</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - Global</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - Metro</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - Backhaul H Móvel</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - Regional</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - Emulação de dados VoIP</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>Bit Transparente RFC2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>Taxa de pacote de acesso RFC2544 > taxa de transporte</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>Taxa de pacote de acesso RFC2544 = taxa de transporte</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>Aplicar o Modelo</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>Configurações de modelo:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>Perda de processo de transferência (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>Limite de Latência de passagem (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>Limiar de jitter de pacote (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>Tamanho de Processos: Padrão</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>Largura de banda máxima (Upstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>Largura de banda máxima (Downstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>Largura de banda máxima: #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>Testes: Testes de vazão e latência</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>Testes: Taxa de transferência, latência, perda de processo e verificação</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>Duração e tentativas: #1 segundos com 1 tentativa</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>Algoritmo de transferência: Viavi Aprimorado</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>Algoritmo de transferência: Padrão RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>Limite de Taxa de transferência (Upstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>Limite de Taxa de transferência (Downstream): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>Limiar de vazão: #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>Algoritmo de perda de processo: Padrão RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>Tempo de verificação de ruptura: 1 seg</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>Verificação de granularidade: 1 processo</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testes</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>Definir Configurações Avançadas de Utilização</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>Largura de banda máx.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>Máximo Downstream de Banda Larga (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>Máximo Downstream de Banda Larga (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>Máximo Downstream de Banda Larga (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>Máximo Downstream de Banda Larga (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>Banda larga máxima Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>Máximo Upstream de Banda Larga (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>Máximo Upstream de Banda Larga (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>Máximo Upstream de Banda Larga (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>Máximo Upstream de Banda Larga (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>Banda larga máxima Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>Banda larga máxima (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>Banda larga máxima (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>Banda larga máxima (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>Banda larga máxima (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Largura de banda máx. (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>Processos Selecionados</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>Tipo de comprimenrto</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Quadro</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Pacote</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>Reinicializar</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Limpar tudo</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleciona tudo</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Comprimento do quadro</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Comprimento do pacote</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>Upstream&#xA;Comprim. quadro</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>Unidade remota não conectada</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>Downstream&#xA;Comprim. quadro</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>Unidade&#xA;remota não&#xA;conectada</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>Testes Selecionados</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>Testes RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>Latência (requer Rendimento)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>Crédito Buffer (requer Rendimento)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>Throughput de crédito Buffer (requer crédito Buffer)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>Recuperação do sistema (apneas Loopback, requer Rendimento)</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>Testes adicionais</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>Vibração do pacote (requer Rendimento)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>Teste de ruptura</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>Carga Estendida (apenas Loopback)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>Taxa de tansferência Cfg</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Processo de zerar em</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544 Standard</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi aperfeiçoado</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>Estabeleça  configurações avançada de rendimento</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>Precisão de medição Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Medição de Precisão Downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisão de medição Downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>Precisão de medição Downstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>Precisão de medição Downstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>Precisão de medição Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>Dentro dos 1.0%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>Dentro dos 0.1%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>Dentro dos 0.01%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>Até 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>Para dentro de 400 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>Para dentro de 40 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>Para dentro de 4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>Dentro dos .4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>Para dentro de 1000 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>Para dentro de 100 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>Para dentro de 10 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>Para dentro de 1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>Dentro dos .1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>Dentro dos .01 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>Dentro dos .001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>Dentro dos .0001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>Para dentro de 92.942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>Dentro dos 9.2942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>Dentro dos .9294 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>Dentro dos .0929 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>Para dentro de 10000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>Para dentro de 1000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>Para dentro de 100 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>Para dentro de 10 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>Para dentro de 1 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>Dentro dos .1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>Precisão de medição Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Medição de Precisão Upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisão de medição Upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>Precisão de medição Upstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>Precisão de medição Upstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>Precisão de medição Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Precisão das medições</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>Precisão de medição (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisão de medição (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>Precisão de medição (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>Precisão de medição (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>Precisão da medição (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>Mais informações:</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>Resolução de problemas</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>Comissionamento</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>Configurações avançadas de Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>Configurar Latência / Pacote Jitter teste duração separadamente</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>Configurar Latência teste duração separadamente</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>Configurar Packet Jitter teste duração separadamente</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>Perda de pocesso Cfg</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Procedimento de teste</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>De cima para baixo</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>De baixo para cima</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>Número de etapas</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 Mbps por etapa</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1 kbps por etapa</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % Taxa de linha por etapa</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>Faixa Teste Downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>Faixa Teste Downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>Faixa Teste Downstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>Faixa Teste Downstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>Intervalo de Teste Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Máx.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularidade de banda larga Downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularidade de banda larga Downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularidade de banda larga Downstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularidade de banda larga Downstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>Granularidade de banda larga Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>Faixa Teste Upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>Faixa Teste Upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>Faixa Teste Upstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>Faixa Teste Upstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>Intervalo de Teste Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularidade de banda larga Upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularidade de banda larga Upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularidade de banda larga Upstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularidade de banda larga Upstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>Granularidade de banda larga Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>Intervalo do Teste (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>Intervalo do Teste (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>Intervalo do Teste (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>Intervalo do Teste (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Faixa de teste (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularidade da Banda (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularidade da Banda (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularidade da Banda (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularidade da Banda (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularidade de banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>Definir Configurações avançadas de medição de perda de processos</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>Configurações avançadas de perda de processos</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>Medidas de Teste Opcional</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>Medir latência</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>Medir pacote de Instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>Verificação Cfg</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>Duração máxima de ruptura Downstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>Duração máxima de ruptura Upstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>Duração máxima de ruptura (s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>Granularidade do burst (quadros)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>Definir configurações avançadas de verificação.</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>Política de quadro de pausa (Pause Frame)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>Buffer de Crédito Cfg</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Tipo de login para controle do fluxo</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>Implícita (link transparente)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explícito (porta-e)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Tamanho máximo do buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>Passos de Rendimento</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>Controles do teste</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>Configurar durações dos testes separadamente?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>Duração (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Número de ensaios</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>Latência / Paquete Jitter</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Ruptura (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Recuperação do sistema</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Todos os testes</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>Exibir&#xA;aprovação/reprovação</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>Limite&#xA;Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>Limite&#xA;Downstream</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Limite</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>Limite de Rendimento (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>Limite de Rendimento (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>Limite de Rendimento (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>Limite de Rendimento (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Limiar de vazão (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>Unidade remota&#xA;não conectada.</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>Latência RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>Latência OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>Jitter de pacote (us)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>Tamanho (kB) da Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>Ruptura (Policiamento CBS)</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>Alta precisão - Baixo atraso</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>Baixa precisão - Alto atraso</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>Executar testes FC</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>Ignorar Testes FC (de Velocidade Real)</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>ligado</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>desligado</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>Vazão de crédito&#xA;de buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Gráfico do Teste de Taxa de transferência</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Gráfico do teste de Tráfego Upstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Resultados do teste de vazão</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Resultados do teste de Tráfego Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Gráfico do teste de Tráfego Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Resultados do teste de Tráfego Downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>Anomalias da taxa de tansferência</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>Anomalias da taxa de tansferência Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>Anomalias da taxa de tansferência Downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>Resultados de Taxa de transferência</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>Rendimento (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>Rendimento (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Vazão (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Quadro 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Quadro 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Quadro 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Quadro 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Quadro 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Quadro 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Quadro 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Quadro 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Quadro 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Quadro 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>Passa/Falha</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>Comprimento do&#xA;quadro (byte)</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>Comprimento&#xA;do pacote(byte)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>Taxa&#xA;medida (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>Taxa&#xA;medida (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>Taxa&#xA;Medida (processos/seg)</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>Detectar&#xA;R_RDY</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Taxa Cfg&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>Taxa L1 &#xA;Medida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>Taxa L1 &#xA;Medida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>(% Linha de Taxa)&#xA;L1 medida</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>Taxa L2 &#xA;Medida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>Taxa L2 &#xA;Medida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>(% Linha de Taxa) L2&#xA; medida</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>Taxa L3 &#xA;Medida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>Taxa L3 &#xA;Medida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>(% Linha de Taxa) L3&#xA; medida</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>Taxa L4 &#xA;Medida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>Taxa L4 &#xA;Medida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>(% Linha de Taxa) L4&#xA; medida</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>Taxa medida&#xA; (processos/seg)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>Taxa medida&#xA; (pacotes/seg)</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>Detectar&#xA;Pausa</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Taxa de Cfg&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Taxa de Cfg&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Taxa de Cfg&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Taxa de Cfg&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalias</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>Processo OoS (s)&#xA;Detectado</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Carga paga de Acterna&#xA;Erro Detectado</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>Erro de FCS&#xA;Detectado</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>Dígito verificador de IP&#xA;Erro Detectado</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>Dígito verificador de TCP/UDP&#xA;Erro Detectado</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>Teste de Latência</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Gráfico do Teste de Latência</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Gráfico do Teste de Latência Upstream</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Resultados do Teste de Latência</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Resultados do Teste de Latência Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Gráfico do Teste de Latência Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Resultados do Teste de Latência Downstream</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>Latência&#xA;RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>Latência&#xA;OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>% Linha de Taxa &#xA; Medida</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>Pausa&#xA;Detectar</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>Resultados do teste de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>Resultados do Teste de Perda de Processo Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>Resultados do Teste de Perda de Processo Downstream</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>Resultados do Teste de Perda de Processo</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Quadro 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>Poporção configurada (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>Poporção configurada (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>Poporção configurada (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>Poporção configurada (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>Poporção configurada (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Perdas de quadros (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>Taxa de transfer-&#xA;ência (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>Taxa de transfer-&#xA;ência (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>Taxa de transfer-&#xA;ência (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>Taxa de transfer-&#xA;ência (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>Taxa de transfer-&#xA;ência (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>Taxa de Perdas&#xA;de quadros (%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>Processo Perdido</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>Méd Máxima de pacote&#xA;de Instabilidade (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>Erro&#xA;Detectar</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>Detectar&#xA;OoS</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Taxa Cfg&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Resultados do teste 'back to back'</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Resultados do Teste de verificação de Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Resultados do Teste de verificação de Downstream</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>Média&#xA;Ruptura de processos</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>Média&#xA;Segundos de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>Resultados do teste de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Atenção</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>Tamanho mínimo de Buffer&#xA;(Créditos)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>Resultados do teste de vazão de crédito de buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>Buffer de crédito de resultados da vazão</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>Rendimento (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>Cred 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>Cred 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>Cred 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>Cred 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>Cred 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>Cred 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>Cred 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>Cred 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>Cred 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>Cred 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>Tamanho do Buffer&#xA;(Créditos)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>Taxa medida&#xA;(Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>Taxa medida&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>Taxa medida&#xA;(processos/seg)</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>J-Proof - Teste de transparência L2 de Ethernet</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>Quadros J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>Execute o teste</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>Executar o teste J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumo</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>Fim: Resultados detalhados</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>Sair do teste J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Aplicando</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>Configurar tipos de quadro para serviço de teste para transparência da camada 2</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   Protocolo   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>Protocolo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> Tipo de quadro </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> Encapsulamento </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Empilhados</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>Tamanho&#xA;do quadro</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamanho do quadro</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Contar</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>Taxa&#xA; (quadro/s)</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Taxa</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>Tempo limite&#xA;(ms)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>Tempo Esgotado</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Controle</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>Bit&#xA;da DEI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>TPID de usuário da SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>Auto Inc CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>Auto Inc SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>Encapsulamento</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>Encapsulamento</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>Tipo&#xA;de quadro</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit Inc</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>Config&#xA;rápida</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>Adicionar&#xA;quadro</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>Remover&#xA;quadro</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>O endereço de origem é comum para todos os quadros. Isso está configurado na página Configurações locais.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Incremento do PBit</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>Stack da VLAN</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>Incremento do SPbit</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Comprim</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dados</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>Configuração J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensidade</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rápido (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>Cheio (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Família</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Todos</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>Spanning Tree</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>Laser&#xA;Ligado</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>Laser&#xA;Desligado</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>Sequência&#xA;iniciar quadro</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>Sequência&#xA;parar quadro</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>Taste J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>PARADO</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>EM PROGRESSO</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>Erros Carga</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>Erros de cabeçalho</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>Discrepância na contagem</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>Resumo dos resultados</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Ocioso</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>Em Progresso</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>Discrepância na carga útil</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>Discrepância no cabeçalho</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>Resultados J-Proof</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>Rx Óptico&#xA;Reinicializar</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>Verificação Rápida</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>Rodar Verificação Rápida</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>Configuração de Verificação Rápida</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>Verificação Rápida de Resultados de carga estendida</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Detalhes</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Simples</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>Por fluxo</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>FROM_TEST</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>Teste de vazão (throughput):</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>Resultados QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Resultados do Teste de Carga Estendida</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Contagem de quadros Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Contagem de quadros Rx</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>Contagem Errored Frame</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>Contagem de quadros OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>Contagem de Processo Perdido</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Relação perd. quad.</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanente</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Ativo</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Rendimento: %1 Mbps (L1), Duração: %2 segundos, Tamanho quadro: %3 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Rendimento: %1 kbps (L1), Duração: %2 segundos, Tamanho quadro: %3 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>Não é o que você queria?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Parar</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Iniciar</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Sucesso</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>Procurando por Destino</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>Status ARP:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Loop remoto</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Verificando loop ativo</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Verificando hard loop</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Verificando o loop permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Verificação de circuito LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Loop ativo</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Loop LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Falha no loop</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>Loop remoto:</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>Rendimento medido (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Não determinada</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Indisponível</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>Rendimento medido (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>Visualizar erros</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>Carga (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>Carga (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>Duração do teste de saída segundos)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>Tamanho do quadro (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Bit Rate</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>Duração da Execução do Teste</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 segundos</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 segundos</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 minuto</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 minutos</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 minutos</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 minutos</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 hora</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 horas</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 horas</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 horas</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>Usuário definido</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>Duração do teste (segundos)</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>Duração do teste de desempenho (minutos)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Comprimento do quadro (byte)</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>Tamanho usuário</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>tamanho Jumbo</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  Conector elétrico:  10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>Comprimento de onda do laser</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>Conector elétrico</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>Conector óptico</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>Detalhes...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Link ativo</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Resultados do Tráfego</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Erro de Contagens</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Quadros com erro</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Quadros OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Quadros perdidos</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>Detalhes da Interface</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>Detalhes SFP/XFP</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>Comprimento de onda (nm): SFP sintonizável, refere-se a configuração de sintonia do canal/comprimento de onda</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>Comprimento do cabo (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>Sintonia admitida</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>Comprimento de onda;Canal</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Comprimento de onda</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>Taxas Recomendadas&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>Taxa nominal (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>Taxa mínima (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>Taxa máxima (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>Cód. fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Rev fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Tipo de nível da alimentação</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Monitoramento de diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Byte de diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>Configure JMEP</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>Comprimento de onda (nm): Dispositivo ajustável, refere-se à configuração de ajuste de canal/comprimento de onda.</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>Detalhes CFP/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>Detalhes CFP2/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>Detalhes CFP4/QSFP</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN do fornecedor</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Código da data</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Código do lote</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>N.° da versão HW / SW</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>N.° da revisão da especificação MSA HW</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>N.° da revisão I/F do gerenciamento MSA</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Classe de energia</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Tipo de nível da alimentação Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Potência Rx Lambda Máxima (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Potência Tx Lambda Máxima (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>N.° de fibras ativas</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Comprimentos de onda por fibra</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>Byte de diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL por intervalo da fibra (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Taxa de bits máxima da linha da rede (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>Módulo ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Taxas suportadas</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Comprimento de onda nominal (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Taxa nominal de Bits (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** Uso recomendado para aplicações RS-FEC 100GigE ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>Especialista CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>Especialista CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>Especialista CFP4</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Ativar Viavi Loopback</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>Ativar enlace de retorno CFP Viavi</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>Ativar Módulo Expert CFP</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>Ativar Módulo Expert CFP2</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>Ativar Modo Especialista CFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>Pré-ênfase</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>Pré-êmfase Tx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Padrão</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>Baixa</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>Nominal</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>Alta</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>Divisor de relógio</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>Divisor Relógio Tx PCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>Inclinação de Deslocamento (bytes)</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>Deslocamento dinâmico Tx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>Inverter Polaridade</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>CFP Tx Inverter Polaridade</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Reiniciar Tx FIFO</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 Rx</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>Equalização</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>Equalização Rx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>CFP Rx Inverter Polaridade</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>Ignorar LOS</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>CFP Rx Ignorar LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Reiniciar Rx FIFO</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>Especialista QSFP</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>Ativar Módulo Expert QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx Ignorar LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>Desvio CDR</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>Receber</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR Recebe desvio</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>Transmissão</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR Transmite desvio</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>transmitir e receber controle do bypass CDR estaria disponível se o módulo QSFP suportasse-o.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>Engenharia</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>Loopback de núcleo XCVR</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>Enlace de retorno de núcleo XCVR CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>Loopback de linha XCVR</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>Enlace de retorno de linha XCVR CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>Loopback host de módulo</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>Enlace de retorno do host do módulo CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>Loopback módulo de rede</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>Enlace de retorno da rede do módulo CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>Enlace de retorno do sistema da caixa de mudanças CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>Enlace de retorno de linha da caixa de mudanças CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>ID de Chip Gearbox</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Rev de Chip Gearbox</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Ver Ucode Gearbox</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>CRC Ucode Gearbox</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Pistas de Sistema Gearbox</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Pistas de Linha Gearbox</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>Pistas de Host CFPn</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>Pistas de Rede CFPn</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>Enlace de retorno de núcleo XCVR QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>Enlace de retorno de linha XCVR QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>DevType Pico MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>PhyAddr Pico MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>RegAddr Pico MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>DevType bolsa MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>PhyAddr bolsa MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>RegAddr bolsa MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>Valor bolsa MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>Registre controles A013 por lane laser ativar/desativar.</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>PartSel Pico I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>DevAddr Pico I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>RegAddr Pico I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>PartSel bolsa I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>DevAddr bolsa I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>RegAddr bolsa I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>Valor bolsa I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>Registro controles 0x56 laser por rota ativar/desativar.</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP + detalhes</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Sinal</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Clock do sinal Tx</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>Fonte do clock</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>Interno</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>Recuperada</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>Externa</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>1.5M externa</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>2M externa</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>10M externa</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>Remota recuperada</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Módulo de temporização</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>Origem VC-12</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>Interna - deslocamento (offset) de frequência (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>Clock remoto</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>Dispositivo portátil:</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>Modo de sintonia</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>Frequência</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>Frequência</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>Primeira frequência sintonizável (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>Última frequência sintonizável (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>Espaçamento da grade (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>Alarme de distorção</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>Limite de Alarme de distorção (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>Ressinc necessária</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>Ressinc. completa</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>Calibração RS-FEC aprovada</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>Calibração RS-FEC falhou</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC 100GigE é tipicamente usada com SR4, PSM4, CWDM4</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Para executar a calibragem RS-FEC, faça o seguinte (também se aplica a CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Inserir um adaptador QSFP28 no CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Inserir um transceptor QSFP28 no adaptador</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Inserir um dispositivo de fibra de enlace de retorno para o transceptor</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Clique em Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Calibrar...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Ressinc.</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Agora deve voltar a sincronizar o transceptor para o dispositivo em teste (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Remover o dispositivo de enlace de retorno da fibra do transceptor</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Estabelecer uma conexão com o dispositivo em teste (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Ligue a laser de DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Verifique se o sinal presente LED em seu CSAM está verde</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Clique em Ressinc. Rota</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>Ressinc&#xA;Rota</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>Resultados J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>Pressione o botão "Start" para iniciar o teste J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>Continuando em modo half-duplex.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>Verificando a conectividade de tráfego na direção upstream.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>Verificando a conectividade de tráfego na direção downstream.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>A conectividade de tráfego foi verificada com sucesso para todos os serviços.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>A conectividade de tráfego na direção upstream não pode ser verificada com sucesso para o(s) serviço(s): #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>A conectividade de tráfego na direção downstream não pode ser verificada com sucesso para o(s) serviço(s): #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>A conectividade de tráfego na direção upstream não pode ser verificada com sucesso para o(s) serviço(s): #1 e, na direção downstream, para o(s) serviço(s): 2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>A conectividade de tráfego na direção upstream não pode ser verificada com sucesso.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>A conectividade de tráfego na direção downstream não pode ser verificada com sucesso.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>A conectividade de tráfego não pode ser verificada com sucesso, tanto na direção upstream como na downstream.</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>Verificando a conectividade</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>Conectividade de tráfego:</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>Iniciar QC</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>Parar QC</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Autoteste Ótico</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>Término: Salvar Perfis</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Relatórios</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>Óptica</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>Sair autoteste ótico</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>Sinal óptico foi perdido. O teste foi abortado.</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>Nível de potência baixo detectado. O teste foi abortado.</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>Alto nível de energia detectado. O teste foi abortado.</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>Foi detectado um erro. O teste foi cancelado.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>O teste FALHOU porque foi detectado excesso de distorção.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>O teste FALHOU porque foi detectado excesso de distorção. O teste foi abortado.</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>Um Bit de erro detectado. O teste foi abortado.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>O teste FALHOU porque BER excedeu o limite configurado.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>O teste FALHOU devido à perda de Sincronia do Padrão.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>Não é possível ler o QSFP+ inserido.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>Não é possível ler o CFP inserido.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>Não é possível ler o CFP2 inserido.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>Não é possível ler CFP4 inserido.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>Um erro de bit corrigível RS-FEC foi detectado.  O teste foi interrompido.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>Um erro de bit não corrigível RS-FEC foi detectado.  O teste foi interrompido.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>Calibração RS-FEC falhou</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>Entrada de Erro de desvio de freqüência detectado</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>Erro de desvio de freqüência de saída detectado</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Sinc perdido</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>Violações de Código detectadas</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>Marcador de alinhamento de bloqueio perdido</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>Marcadores de Alinhamento inválidos detectados</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>BIP 8 AM Bit erros detectados</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>Erros detectados no Bloco BIP</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>Distorção detectada</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>Erros de bloco detectados</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>Processos subdimensionadas detectados</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>Falha emota Detectada</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 erro de bit detetado</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>Padrão de Sincronização perdido</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>O Limite de BER não pôde ser medido com precisão, devido à perda do padrão de sincronização</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>O BER medido excedeu o limite BER escolhido</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS detectado</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>Erro FAS Detectado</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>Sem processo detectado</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>Perda de processo detectada</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>Sincronização de Processo Perdida</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>Sem marcador lógicos de faixa detectados</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>Erros lógicos de marcador faixa detectados</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>Perda de alinhamento de faixa detectada</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>Alinhamento faixa perdido</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>Erros MFAS Detectados</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>Sem alinhamento de faixa detectado</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS detectado</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>Sem recuperação detectada</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>Distorção Excessiva detectada</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>Calibração RS-FEC bem sucedida</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Erro</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>Teste Ótico</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>Tipo do teste</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>Teste usa 100GE RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>Tipo óptica</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>Veredicto geral do teste</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>Teste presença de sinal</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>Teste nível do sinal óptico</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>Teste Distorção Excessiva</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>Erro de teste de bit</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>Teste de Erro Geral</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>Teste de Limite de BER</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>Potência ótica</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Nível Rx (dBm) de Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Soma de nível Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Nível Tx (dBm) de Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Soma de nível Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>Configurações</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>Conecte um cabo curto, limpo entre os terminais Tx e Rx do conector que você deseja testar.</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>Teste Ótico&#xA;CFP</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>Teste CFP2&#xA;Ópticos/Slot</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Abortar o teste</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>Teste Ótico&#xA;QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>Teste QSFP28&#xA;Ótico</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Configurações:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>Recomendado</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 minutos</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 minutos</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 horas</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 horas</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>Duração de usuário (minutos)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>Duração recomendada (minutos)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>O tempo recomendado para esta configuração é &lt;b>%1&lt;/b> minuto(s).</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>Tempo limite BER</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Pós FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Pré-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>Limite de BER</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>Deslocamento de linha PPM</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>Deslocamento máx PPM (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>Parar no erro</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>Visão Geral dos Resultados</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>Tipo ótico/slot</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>Compensação PPM atual</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>BER atual </translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>Potência ótica (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Nível Rx de Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Nível Rx de Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Nível Rx de Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Nível Rx de Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Nível Rx de Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Nível Rx de Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Nível Rx de Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Nível Rx de Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Nível Rx de Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Nível Rx de Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Soma de nível Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Nível Tx de Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Nível Tx de Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Nível Tx de Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Nível Tx de Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Nível Tx de Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Nível Tx de Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Nível Tx de Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Nível Tx de Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Nível Tx de Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Nível Tx de Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Soma de nível Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>Detalhes de Interface de CPF</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>Detalhes da Interface CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>Detalhes interface CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>Detalhes da Interface QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>Detalhes interface QSFP+</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>Detalhes interface QSFP28</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>Nenhum QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>Não foi possível ler o QSFP - Por favor, inserte-o novamente</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>QSFP erro checksum</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>Não é possível interrogar os registrados des QSFP necessários.</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>Não é possível confirmar identidade QSFP.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>Teste e duração de carga útil BERT </translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>Transparência GCC</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>Selecionar e executar testes</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>Configurações avançadas</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>Executar testes de verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>Carga útil BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>Sair do teste de verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>Frequência de medição</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Status da auto negociação</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>Configuração RTD</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>Todas as pistas</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>Verificação OTN:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** Começar teste de verificação OTN ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>Erro bit BERT de carga útil detectado</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>Mais de 100.000 erros bit BERT de carga útil detectados</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>Limite BER BERT de carga útil excedido</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>Erros bit BERT de carga útil #1 detectados</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>Taxa de erro bit BERT de carga útil: #1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>Percentagem RTD excedido</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: Mín: #3, Máx: #4, média: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD indisponível</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>Teste de carga útil BERT a correr</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>Teste RTD a correr</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>Teste de transparência GCC a correr</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>Erro de bit GCC detectado</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>Pré-definido BER GCC excedido</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 erro de bit GCC detetado</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>Mais de 100.000 erros de bit GCC detetados</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>Taxa de erro de bit GCC: #1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** Iniciar verificação de enlace de retorno ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** Pular verificação de enlace de retorno ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** Verificação de enlace de retorno concluída ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>Enlace de retorno detectado</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>Nenhum enlace de retorno detectado</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>Canal #1 está indisponível</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>Canal #1 está agora disponível</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>Perda de sinc padrão.</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>Perda de sinc padrão GCC.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>Teste cancelado: erro de bit detectado.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>Falha ao testar: BER excedeu o valor pré-definido configurado</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>Falha ao testar: RTD excedeu o valor pré-definido configurado</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>Teste abortado: Nenhum enlace de retorno detectado.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>Teste cancelado: nenhum sinal ótico presente.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>Teste cancelado: perda de quadro.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>Teste cancelado: perda de sinc padrão.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>Teste cancelado: perda de sinc padrão</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>Teste cancelado: perda de sinc padrão GCC.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>Teste cancelado: perda de alinhmento de linha</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>Teste cancelado: perda do marcador bloqueado</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>Pelo menos 1 canal deve ser selecionado</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>Enlace de retorno não verificado</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>Enlace de retorno não detectado</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>Seleção do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>Duração planeada do teste</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>Tempo de execução do teste</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>Verificação OTN requer um loopback de tráfego para executar; este loopback é necessário na extremidade mais distante do circuito OTN.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>Testes de verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>Desvio ótico, mapeamento de sinal</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>Mapeamento de sinal</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>Mapeamento de sinal e seleção ótica</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>Conf avançada</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>Estrutura do sinal</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP Optico e Desvio RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>CFP Optico e Desvio RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 Optico e Desvio RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>Deslocamento RTD ótico CFP4 (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>Configurar teste de duração e BERT de carga útil</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>Conf e duração de carga útil BERT </translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>Conf de carga útil BERT</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>Nível de confidência (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>Com base na taxa de linha, Percentual BER e Nível de confidência, o tempo de teste recomendado é &lt;b>%1&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Padrão</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>Padrão BERT</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>Erro pré-definido</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>Mostrar aprovado/reprovado</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>Definições de atraso de Ida-Volta</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>Conf RTD</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>Inclui</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>Valor pré-&#xA;definido (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>Medida da Frequência (s)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>Conf GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>Configurar transparência GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>Canal GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>Pré-definido BER GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>Padrão BERT 2^23-1 é usado no GCC.</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>Verificação de Loopback</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>Pular verificação de enlace de retorno</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>Pressione o botão "Start" (Início) para iniciar a verificação de enlace de retorno</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>Saltar testes de verificação OTN</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>Verificar enlace de retorno</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>Enlace de retorno detectado</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>Pular verificação de enlace de retorno</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>Detecção de enlace de retorno</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>Detecção de enlace de retorno</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>Resultados de carga útil BERT</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>BER medido</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>Contagem de erro em bits</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>Veredito</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>Configuração de carga útil BERT </translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>Resultados de atraso de Ida-Volta</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>Mín (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>Máx (ms)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>Média (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>Nota: condição de falha ocorre quando o RTD da média excede o limite.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuração</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>Configuração de atraso de Ida-Volta</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>Resultados transparência GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>Taxa de erros de bit GCC BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>Erros de bit GCC BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>Configuração transparência GCC</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>Análise de Protocolo</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Capturar</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>Capturar e Analisar CDP</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>Capturar e Analisar</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>Selecionar Protocolo a Analisar</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>Iniciar&#xA;Análise</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>Abortar&#xA;Análise</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Análise</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>PTP especializado</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>Configurar manualmente os ajustes do teste. </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>Limiares</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>Executar verificação rápida</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>Sair teste PTP especializado</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>Link não está mais ativo.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>Sincronização de origem de tempo de atraso de uma via foi peridida</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>A seção Escravo PTP parou inexperadamente.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>Sincronização de origem de tempo não está presente. Verifique se sua origem de tempo está cnfigurada e conectada adequadamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>Não é possível estabelecer uma seção Escravo PTP.</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>Nenhum receptor GPS detectado.</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>Tipo TOS</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Tempo limite de Announce Rx</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>Announce</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 por segundo</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 por segundo</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 por segundo</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 por segundo</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 por segundo</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 por segundo</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 por segundo</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 por segundo</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Sinc</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>Solicitação de atraso</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>Duração da locação (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Ativar</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Limite Erro tempo máx. ativado</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Erro tempo máx. (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Limite de erro tempo máx. (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>Duração do teste (minutos)</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>Verificação rápida</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>IP mestre</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>Domínio PTP</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>Início&#xA;Verificar</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>Partida&#xA;Sessão</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>Sessão&#xA;Estabelecido</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Limite de erro tempo máx. (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Erro tempo máx. (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>Erro tempo, atual (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>Erro tempo, atual (us) vs. Tempo</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Resultados do teste</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>Duração (minutos)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Verificação PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>Teste de verificação PTP</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>Término: Verificação PTP</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>Iniciar outro teste</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>Sair da Verificação PTP</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>RFC 2544 melhorado</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>Instalações Inválidas</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>Configurações avançadas IP - Local</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>Configurações avançadas de latência RTD</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>Configurações Avançadas de Teste de Ruptura (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Executar o J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>Configurações J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>Jitter</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carga Estendida</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>Sair do Teste RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>Configuração de Rede Local</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>Configuração de Rede Remota</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>Status de auto negociação local</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Velocidade (Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Controle do fluxo</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Aceita FDX</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Aceita HDX</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Aceita pausa</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>Status de negociação remota automática</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Metade</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Cheio</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Nenhum</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Tanto Rx como Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Somente Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Somente Rx</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>Taxa de processo RTD</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 Processo por segundo</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 Processos por segundo</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>Ruptura Cfg</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>Tamanho de Ruptura Comprometida</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>Controle de CBS (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>CBS Downstream (kb)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>Tamanho de Ruptura Downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Mínimo</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>Máximo</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>CBS Upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>Tamanho de Ruptura Upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>Tamanho de Ruptura (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>Definir Configurações Avançadas de CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>Definir Configurações Avançadas de Controle de CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>Definir Configurações Avançadas de pesquisa de ruptura</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>Tolerância</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>Carga Cfg Estendida</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>Escala de Rendimento (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 tem as seguintes configurações inválidas:</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>Conectividade de tráfego foi verificada com êxito. Executando o teste de carga.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>Pressione o botão "Iniciar" para executar a taxa de linha.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>Pressione o botão "Iniciar" para executar usando banda larga configurada em RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>A taxa de transferência medida NÃO será usada nos testes RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>A taxa de transferência medida SERÁ usada nos testes RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>Carregar Teste de Tamanho de Processo: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>Carregar Tamanho do Pacote de Teste: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>Carregar Teste de Tamanho de Processo Upstream: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>Carregar Teste de Tamanho de Processo Downstream: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>Taxa de transferência medida:</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>Teste usando banda larga máxima configurada RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>Usar a medição da taxa de transferência medida como Banda Larga RFC 2544 máxima</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>Carregar Teste de Tamanho de Processo (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>Carregar Tamanho (bytes) do Pacote de Teste</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>Carregar Teste de Tamanho de Processo Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>Carregar Teste de Tamanho de Processo Downstream</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>Executar Testes RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>Pular Testes RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>Verificando loop de hardware.</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Teste de jitter de pacote</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Gráfico do Teste de Instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Gráfico do Teste de Instabilidade Upstream</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Resultados do Teste de Instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Resultados do Teste de Instabilidade Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Gráfico do Teste de Instabilidade Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Resultados do Teste de Instabilidade Downstream</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>Max Méd Jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>Méd Máxima de Instabilidade</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>Méd Máxima de&#xA;Instabilidade (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Resultados de Teste CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Resultados do Teste de CBS Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Resultados do Teste de CBS Downstream</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Resultados do Teste de Pesquisa de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Resultados do Teste de Pesquisa de Ruptura Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Resultados do Teste de Pesquisa de Ruptura Downstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Resultados do Controle de CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Resultados do Teste de Controle de CBS Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Resultados do Teste de Controle de CBS Downstream</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>Teste de ruptura (Policiamento CBS)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Ruptura Cfg&#xA;Tamanho (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Ruptura TX&#xA;Tamanho (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>Média Rx Tamanho&#xA;da ruptura (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>Sistemas&#xA;Enviados</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>Quadros&#xA;Recebidos</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>Quadros&#xA;Perdidos</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>Tamanho da gravação&#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>Latência&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>Instabilidade&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>Configurar Tamanho&#xA;de Explosão (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Ruptura Tx&#xA;Tamanho de Policiamento (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>CBS&#xA;Estimado (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Gráfico do Teste de Recuperação de Sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Gráfico do Teste de Recuperação de Sistema Upstream</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Resultados do teste de recuperação do sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Resultados do Teste de Recuperação de Sistema Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Gráfico do Teste de Recuperação de Sistema Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Resultados do Teste de Recuperação de Sistema Downstream</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>Tempo de recuperação (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>Taxa de sobrecarga&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>Taxa de sobrecarga&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>Taxa de sobrecarga&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>Taxa de sobrecarga&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>Taxa de sobrecarga&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>Taxa de Recuperação&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>Taxa de Recuperação&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>Taxa de Recuperação&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>Taxa de Recuperação&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>Taxa de recuperação&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>Recuperação média&#xA;Tempo</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Teste TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>Controles</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>Controles do TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>Modelagem</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>Configuração de Passos</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>Selecionar Passos</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>MTU do caminho</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Vazão TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avançado</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Gravar</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>Configurações de conexão</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>TrueSpeed Ctl (Avançado)</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Escolher Unidade Bc</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>Janela de andar</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>Sair do teste TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>Host TCP falhou ao estabelecer a conexão. O teste em ação foi parado.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>Por selecionar o módulo Solucionar Problemas, você foi desconectado da unidade remota.</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>Seleção de teste Inválida: Pelo menos um teste deve ser selecionado para executar TrueSpeed(VelocidadeVerdadeira).</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>O MTU medido é muito pequeno para continuar. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>Seu arquivo transferiu muito rapidamente! Favor escolher um tamanho de arquivo para rendimento TCP para que o teste possa executar por pelo menos 5 segundos. É recomendado que o teste deve levar pelo menos 10 segundos. </translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>Retransmissão máxima obtida. Teste abortado.</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>Host TCP encontrou um erro. O teste em ação foi parado.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>Iniciando teste TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>Reduzindo a zero o tamanho do MTU.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>Testando byte MTU #1 com byte MSS #2.</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>O Caminho MTU foi determinado para ser #1 Bytes. Isto iguala um MSS de #2 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>Executando teste RTT. Levará #1 segundo.</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>O Tempo de Ida e Volta (RTT) foi determinado para ser #1 msec.</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>Executando teste de Siga a Janela (Walk the Window) upstream</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>Executando teste de Siga a Janela (Walk the Window) downstream</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>Enviando #1 bytes de tráfego TCP.</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>Tráfego local de saída: Não modelado</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>Tráfego remoto de saída: Não modelado</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>Tráfego local de saída: Com forma</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>Tráfego remoto de saída: Com forma</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>Duração (seg): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>Conexões: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>Tamanho da Janela (kB): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>Executando teste de Rendimento TCP upstream.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>Executando teste de Rendimento TCP downstream</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>Realizar teste avançado de TCP. Isto levará #1 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>Tipo IP local</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>Endereço de IP Local</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>Porta de Entrada Local Predefinida</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>Máscara de Sub-Rede</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>Encapsulamento Local</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Endereço IP remoto</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>Selecionar Módulo</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>Que tipo de teste está executando?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>Estou &lt;b>instalando&lt;/b> ou &lt;b>aumentando&lt;/b> um novo circuito.*</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>Estou &lt;b>consertando&lt;/b>um circuito existente </translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>*Requer um Instrumento Remoto de Teste MTS/T-BERD</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>Como seu rendimento será configurado?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>Meus rendimentos downstream e upstream são &lt;b>o mesmo&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>Meus rendimentos downstream e upstream são &lt;b>different&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Instalar Banda de Afunilamento (Bottleneck Bandwidth) para igualar SAMCompleto CIR quando carregando Truespeed&#xA;configuração.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Instalar Banda de Afunilamento (Bottleneck Bandwidth) para igualar Banda Máxima RFC 2544 quando carregando Truespeed&#xA;configuração.</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Servidor Iperf</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>Instrumento de Teste T-BERD/MTS</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>Tipo IP</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Endereço IP   </translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>Instalações Remotas</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>Instalação de Servidor Hospedeiro de TCP</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>Este passo irá configurar definições globais para todos os passos subsequentes do TrueSpeed. Isto inclui CIR (Taxa de Informação Comprometida) e % de Passagem de TCP, que é o percentual do CIR requerido para passar o teste de throughput.</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>Executar o Teste Walk-the-Window</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>Total do Tempo do Teste(s)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>Automaticamente encontrar tamanho MTU</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>Tamanho de MTU (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>Identificação de VLAN local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>Prioridade</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>Prioridade Local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>TOS local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>DSCP local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>CIR Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>CIR Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>Remoto&#xA;TCP Hospedeiro</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>Identificação Remota de VLAN</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>Prioridade Remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>TOS remoto</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>DSCP remoto</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>Definir Configurações Avançadas de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Escolher Unidade Bc</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>Formatação do tráfego.</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Unidade Bc</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>Perfil de Modelagem</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>Modelagem de tráfego de saída Local e Remoto</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>Somente tráfego modelado de saída Local</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>Somente tráfego modelado de saída Remoto</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>Modelagem de tráfego de saída nem Local ou Remoto</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>Modelagem de Tráfego (Apenas Testes de Walk the Window e de Taxa de Transferência)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>Execução de testes não modelados e depois modelados</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) Local</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) Remoto</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (remoto)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) Remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) Remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) Remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) Remoto</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>Você quer moldar o tráfego TCP?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>Tráfego Não Modelado</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>Tráfego Modelado</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>Executar teste não modelado</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>THEN</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>Executar teste modelado</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>Executar teste modelado no Local</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>Executar com modelagem em Local e Remoto</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>Executar sem qualque modelagem</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>Executar com modelagem em Local e sem modelagem em Remoto</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>Executar com modelagem em Local</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>Executar sem modelagem em Local e com modelagem em Remoto</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>Modelar tráfego Local</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>A unidade remota é&#xA;não conectado</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>Modelar tráfego Remoto</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>Mostrar opções de formato adicional</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>Controles de TrueSpeed (Avançado)</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>Conetar à porta</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>Passagem de TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>Limite superior de MTU (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>Usar várias conexões</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>Ativar janela de saturação</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>Janela de impulso (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>Conexões de impulso (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>Configuração de Passos</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>Passos do TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>É necessário ter pelo menos um passo selecionado para executar o teste.</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>Este passo usa o procedimento definido em RFC4821 para automaticamente determinar a Unidade Máxima de Transmissão do final do fim do caminho do network. O teste do Cliente TCP objetivará enviar segmentos TCP em vários tamanhos de pacotes e determinar o MTU sem que seja necessário usar ICMP (como requerido pelo tradicional Descoberta de Caminho MTU).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>Essa etapa realizará uma transferência TCP de baixa intensidade e informará o retorno  do tempo de ida e volta (RTT) que será usado como base para os seguintes resultados das etapas de teste.  A linha de base RTT é a latência inerente da rede, excluindo os atrasos adicionais causados pelo  congestionamento da rede.</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>Duração (segundos)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>Este passo conduzirá a "Scan (Varredura) de Janela TCP e informará de volta resultados de rendimento de TCP para até quatro (4) combinações de tamanho de janela TCP e conexões.</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>Tamanhos de janela</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>Tamanho de Janela 1 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># Con.</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>Conexões de Janela 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>Tamanho de Janela 2 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>Conexões de Janela 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>Tamanho de Janela 3 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>Conexões de Janela 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>Tamanho de Janela 4 (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>Conexões de Janela 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>Tam. máx. seg. (byte)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>Usa o MSS encontrado&#xA;no passo MTU do caminho</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>Tamanho Máximo de Segmento (bytes)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>Com base na ligação de banda larga de &lt;b>%1&lt;/b> Mbps e um RTT de &lt;b>%2&lt;/b> ms, a janela de TCP ideal seria de &lt;b>%3&lt;/b> bytes. Com &lt;b>%4&lt;/b> conexões e uma janela de TCP de &lt;b>%5&lt;/b> byte para cada conexão, o rendimento aproximado de transferência ideal seria de &lt;b>%6&lt;/b> kbps e um arquivo de &lt;b>%7&lt;/b> MB transferido através de cada conexão deveria levar &lt;b>%8&lt;/b> segundos.</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>Com base na ligação de banda larga de &lt;b>%1&lt;/b> Mbps e um RTT de &lt;b>%2&lt;/b> ms, a janela de TCP ideal seria de &lt;b>%3&lt;/b> bytes. &lt;font color="red> Com &lt;b>%4&lt;/b> conexões e uma janela de TCP de &lt;b>%5&lt;/b> byte para cada conexão, a capacidade da ligação está excedida. Os resultados reais podem ser piores do que os resultados previstos, devido à perda de pacotes e atraso adicional. Reduza o número de conexões TCP e / ou tamanho da janela para executar um teste dentro da capacidade de ligação.&lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>Tamanho de Janela (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>Tamanho do arquivo por conexão (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>Automaticamente encontrar tamanho de arquivo para transmissão de 30 segundos</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>Número de conexões</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>Usar o RTT encontrado&#xA;no passo do RTT&#xA;(%1)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>Usar o MSS encontrado&#xA;no passo do RTT&#xA;(%1 bytes)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>Essa etapa irá realizar transferências múltiplas de conexão TCP para testar  se o link divide de maneira igualitária a banda larga (adequação do tráfico) ou desigual (políticas de tráfego).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>Essa etapa realizará múltiplas conexões de transferência TCP para testarse o link está compartilhando a largura de banda (adequação de tráfego) ou se o link está priorizando o tráfego (priorização de tráfego). Para que o tamanho da janela e o número de conexões sejam automaticamente computados, por favor execute a etapa RTT.</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Tamanho da janela (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>Automaticamente computado quando o&#xA;passo RTT é conduzido</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 bytes</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>Executar testes TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>Ignorar Testes TrueSpeed (de Velocidade Real)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>Unidade de Transmissão Máxima (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>Unidade de Transmissão Máxima (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>Este passo determinou que a Unidade de Transmissão Máxima (MTU) é de &lt;b>%1&lt;/b>bytes para esta ligação (final do fim). Este valor, menos a camada 3/4 acima, será usado como o tamanho do Máximo Tamanho de Segmento do TCP (MSS), para os passos subsequentes.  Neste caso, o MSS é de &lt;b>%2&lt;/b> bytes</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tempo (s)</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>Resultados do resumo RTT</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>RTT Médio (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>RTT Mínimo (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>Max. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>O Tempo de Ida e Volta (RTT) foi medido para ser &lt;b>%1&lt;/b> msec. O Mínimo RTT é usado, pois representa mais de perto a latência inerente do network. Passos subsequentes usam-no como base para o desenvolvimento previsto de TCP.</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>O tempo de ida e volta (RTT) foi medido para ser %1 msec. O RTT Médio (Base) é usado assim para representar mais de perto a latência inerente da rede. As etapas subsequentes usam isso como base para o desempenho previsto de TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>Siga a Janela (Walk the Window) upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>Siga pela Janela (Walk the Window)Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>Tamanho de Janela 1 Upstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>Tamanho de Janela 1 Downstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>Tamanho de Janela 2 Upstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>Tamanho de Janela 2 Downstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>Tamanho de Janela 3 Upstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>Tamanho de Janela 3 Downstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>Tamanho de Janela 4 Upstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>Tamanho de Janela 4 Downstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>Tamanho de Janela 5 Upstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>Tamanho de Janela 5 Downstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>Conexões de Window 1 Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>Conexões da Janela 1 Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>Conexões de Window 2 Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>Conexões da Janela 2 Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>Conexões de Window 3 Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>Conexões da Janela 3 Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>Conexões de Window 4 Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>Conexões da Janela 4 Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>Conexões de Window 5 Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>Conexões da Janela 5 Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>Janela 1 Real Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>Janela Ascendente 1 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Janela Ascendente 1 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>Janela 1 Prevista Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>Janela 2 Real Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>Janela Ascendente 2 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Janela Ascendente 2 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>Janela 2 Prevista Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>Janela 3 Real Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>Janela Ascendente 3 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Janela Ascendente 3 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>Janela 3 Prevista Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>Janela 4 Real Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Janela Ascendente 4 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Janela Ascendente 4 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>Janela 4 Prevista Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>Janela 5 Real Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>Janela Ascendente 5 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Janela Ascendente 5 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>Janela 5 Prevista Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>Janela 1 Real Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Janela Descendente 1 Real Modelado (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>Janela 1 Prevista Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>Janela 2 Real Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Janela Descendente 2 Real Modelado (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>Janela 2 Prevista Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>Janela 3 Real Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Janela Descendente 3 Real Modelado (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>Janela 3 Prevista Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>Janela 4 Real Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Janela Descendente 4 Real Modelado (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>Janela 4 Prevista Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>Janela 5 Real Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Janela Descendente 5 Real Modelado (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>Janela 5 Prevista Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Tx Mbps, média.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>Janela 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>Real</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>Real Não Modelado</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>Real Modelado</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>Ideal</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>Janela 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>Janela 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>Janela 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>Janela 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>Os resultados da janela TCP walk exibe o rendimento atual versus o ideal para cada tamanho/conexão de janela testada. O atual estar menor que o ideal, pode ser causado por perda ou por congestionamento de tráfego. Se o real é maior que o ideal, então o RTT utilizado como linha de base é demasiado elevado. A etapa de vazão TCP fornece uma análise mais profunda das transferências TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>Rendimento Real de TCP versus Ideal Upstream </translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>Gráficos de Rendimento de TCP Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>Gráficos de Retransmissão de Taxa de Transferência  TCP Ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>Gráficos RTT de Taxa de Transferência  TCP Ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>Rendimento TCP Downstream Real versus Ideal</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>Gráficos de Rendimento TCP Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>Gráficos de Retransmissão de Taxa de Transferência de TCP Descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>Gráficos RTT de Taxa de Transferência de TCP Descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>Tempo(s) de Transmissão Ideal Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>Tempo(s) de Rendimento Real Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>Taxa de transferência L4 real para upload (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Throughput de Upstream L4 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>Throughput de Upstream L4 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>Throughput de Upstream L4 Ideal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>Eficiência TCP para upload (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>Eficiência de TCP Não Modelado Ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>Eficiência de TCP Não Modelado Ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>Atraso de buffer para upload (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>Atraso de Buffer Não Modelado Ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>Atraso de Buffer Modelado Ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>Taxa de transferência L4 real para download (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Throughput de Downstream L4 Não Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>Throughput de Downstream L4 Modelado Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>Throughput de Downstream L4 Ideal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>Eficiência TCP para download (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>Eficiência de TCP Não Modelado Descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>Eficiência de TCP Modelado Descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>Atraso de buffer para download (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>Atraso de Buffer Não Modelado Descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>Atraso de Buffer Modelado Descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>Resultados de Rendimento TCP</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>Real versus Ideal</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>Os resultados dos testes de upstream podem indicar:</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>Nenhuma recomendação extra</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>The teste não foi executado durante um tempo prolongado</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>Segurança de Rede / modelador precisa de afinação</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>Policer perdeu volume devido à rupturas de TCP.</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>O Rendimento era bom, mas retransmissões foram detectadas.</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>A Rede está congestionado ou o tráfego está sendo moldado</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>O CIR pode estar configurado incorretamente</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>Seu arquivo transferiu muito rapidamente!&#xA;Favor rever o tempo de transferência previsto para o arquivo.</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>conexão(ões)&lt;b>%1&lt;/b>, cada com uma janela de tamanho &lt;b>%2&lt;/b> bytes foram usadas para transferir um arquivo MB &lt;b>%3&lt;/b>através cada conexão (MB total &lt;b>%4&lt;/b>).</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b> bytes de janela TCP transmitidos em &lt;b>%2&lt;/b> conexão(s).</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>L4 atual (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>L4 ideal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>Transferir medições</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>Eficiência TCP &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>Tampão de Atraso&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>Os resultados dos testes de downstream podem indicar:</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>Real modelado vs.  Ideal</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>Eficiência do TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>Atraso do Buffer (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>Os resultados do teste não modelado podem indicar:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>Real não modelado vs.  Ideal</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>Gráficos Throughput</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Fórmula de Retransmissão</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>Linha de base RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>Usar esses gráficos para correlacionar possíveis problemas do desempenho do TCP causados por retransmissões e/ou efeitos de network congestionado (RTT excedendo a linha de base).</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>Gráficos de retransmissão</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>Gráficos RTT</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>Throughput ideal por conexão</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>Para uma ligação em forma de tráfego, cada conexão deve receber uma porção relativamente igual da banda. Para uma ligação com policiamento de tráfego, cada conexão será devolvida, pois retransmissões ocorrem devido ao policiamento. Para cada uma das conexões &lt;b>%2&lt;/b>, cada conexão deve consumir cerca de &lt;b>%1&lt;/b>Mbps de banda. </translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Randômico</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Teste de TrueSpeed VNF </translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>Configurações de teste</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>Conexão Avançada de Servidor</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>Configurações avançadas de testes</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>Criar relatório localmente</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identificação do teste</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>Final: Exibir resultados detalhados</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>Final: Criar relatório localmente</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>Criar outro relatório localmente</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>Taste MSS</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>Taste RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>Teste de Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>Teste de downstream</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>A seguinte configuração é exigida.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>A seguinte configuração está fora do intervalo.  Insira um valor entre #1 e #2.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>A configuração seguinte possui um valor proibido.  Faça uma nova seleção.</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>Nenhuma licença de teste cliente-para-servidor.</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>Nenhuma licença de teste servidor-para-servidor.</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>Há muitos testes ativos (o máximo é #1).</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>O servidor local não é reservado.</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>o servidor remoto não é reservado.</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>A instância do teste já existe.</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>Erro de leitura do banco de dados de teste.</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>O teste não foi encontrado no banco de dados de teste.</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>O teste expirou.</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>O tipo de teste não é suportado.</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>O servidor de teste não está selecionado.</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>O servidor de teste está reservado.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>Testar modo de solicitação do servidor incorreto</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>Os testes não são permitidos no servidor remoto.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>A solicitação HTTP falhou no servidor remoto.</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>O servidor remoto não possui recursos suficientes disponíveis.</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>O cliente de teste não está selecionado.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>A porta teste não é suportada.</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>Várias tentativas de teste por hora.</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>A compilação da instância de teste falhou.</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>A compilação do fluxo de trabalho de teste falhou.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>O fluxo de trabalho da solicitação HTTP está incorreto. </translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>O fluxo de trabalho da solicitação HTTP falhou.</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>Testes remotos não são permitidos.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>Ocorreu um erro no gerenciador de recursos.</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>A instância de teste não foi encontrada.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>O estado do teste possui um conflito.</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>O estado do teste é inválido.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>A criação do teste falhou.</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>A atualização do teste falhou.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>O teste foi criado com êxito.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>O teste foi atualizado com êxito.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>Falha na solicitação HTTP: #1 / #2 / 3 #</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>A versão do servidor VNF (#2)  pode não ser compatível com a versão do instrumento (#1).</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>Digite nome de usuário e a chave de autenticação do servidor em #1.</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>Falha ao inicializar o teste: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>Teste anulado: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>Conexão com o servidor</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>Sem informações necessárias para conectar ao servidor.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>Não há link para realizar comunicações de rede.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>Sem um endereço IP de origem válido para comunicações de rede.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>O ping não realizado no endereço IP especificado.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>Não é possível detectar a unidade no endereço IP especificado.</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>O servidor ainda não identificou o endereço IP especificado.</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>O servidor não pôde ser identificado no endereço IP especificado.</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>Servidor identificado mas não autenticado.</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>Falha de autenticação de servidor.</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>Falha na autorização, tentando identificar.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>Não autorizados ou identificados, tentando ping.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>Servidor autenticado e disponível para testes.</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>O servidor está conectado e o teste está sendo executado.</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>Identificando</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>Identificar</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>Versão da Proxy TCP</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>Versão do controlador de teste</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>Link ativo:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID do servidor:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>Status do servidor:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>Configurações avançadas</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>Configurações avançadas de conexão</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>Chave de autenticação</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>Memorizar nomes de usuário e senhas</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>Unidade local</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>Servidor</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Duração de Window Walk (seg)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>Duração automática</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>Configurações de teste (Avançadas)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>Parâmetros de teste avançados</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>Porta TCP</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>Porta TCP automática</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Número de Window Walks</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>Contagem  de conexão </translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>Contagem automática de conexão </translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Janela de saturação</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>Executar a janela de saturação</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>Conexão de impulso (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>Informações de relatório de servidor</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nome do teste</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nome do técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID de Técnico</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>Nome do cliente*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Empresa</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>E-mail*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Telefone:</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Comentários</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>Exibir ID teste</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>Pular Teste TrueSpeed </translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>O servidor não está sendo conectado.</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>Méd. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>Esperando o recurso de teste ficar pronto.  Você é #%1 na lista de espera.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Janela</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>Janela Saturação&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>Tamanho da Janela (kB): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>Conexões</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>Diagnóstico de Upstream:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>Nada a relatar</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>Taxa de transferência muito baixa</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>RTT inconsistente</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>A eficiência de TCP é baixa</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>O atraso de buffer é alto</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>Taxa de transferência inferior a 85% do CIR</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>MTU menor que 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>Diagnóstico de downstream:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Código de autenticação</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Data de criação da autenticação</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>Data de expiração</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>Modificar hora</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Tempo de parar o teste</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>Última modificação</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome do cliente</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>E-mail</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>Resultados da taxa de transferência de upstream</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>Atual vs. Alvo</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>Nº de conexões upstream reais vs.  Alvo</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>Resumo de upstream</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>Pico de Taxa de Transferência TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>Tempo de ida e volta (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>Resultados do resumo de upstream (janela de taxa de transferência máx)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>Tamanho da janela por conexão (kB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>Janela agregada (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>Taxa de transferência TCP alvo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>Taxa de Transferência TCP Média (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>Vazão TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>Alvo</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>Janela 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>Janela 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>Janela 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>Janela 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>Janela 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>Janela 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>Janela de taxa de transferência máxima:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>%1 kB Window: %2 con. x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Média</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>Gráfico de taxa de transferência de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>Detalhes de Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 1 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>Tamanho da janela por conexão (kB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>Throughput atual (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>Pico de Taxa de Transferência TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>Total de retransmissão</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 2 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 3 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 4 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 5 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 6 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 7 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 8 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 9 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela 10 de upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela de saturação de upstream</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>Rendimento médio (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>Resultados de taxa de transferência de downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>Downstream real vs. Alvo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>Resumo de downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>Resultados do resumo de downstream ( Janela de taxa de transferência máx.)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>Gráficos de taxa de transferência de downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>Detalhes de downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 6</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 7</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 8</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 9</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>Resultados de taxa de transferência da janela de downstream 10</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>Resultados da taxa de transferência da janela de saturação downstream</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation>Nº de Window Walks</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>Janelas sobressaturadas (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>Conexões sobressaturadas (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>Varredura da VLAN</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 Iniciando Teste de Varredura VLAN #2</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>Testando ID VLAN #1 por #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1: APROVADO</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1: FALHA</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>O loop ativo não é bem sucedido. Teste falhou para ID VLAN #1</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>Total de IDs VLAN Testados</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>Número de IDs Aprovados</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>Número de Falhas de ID</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>Duração por ID (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>Número de Intervalos</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>Intervalos selecionados</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>Mín. ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>Máx VLAN-ID</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Iniciar Teste</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>IDs totais</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>IDs Aprovados</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>Falha de IDs</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>Configurações Avançadas de Varredura de VLAN</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>Largura de banda (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>Critérios de Aprovação</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>Nenhum quadro perdido</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>Alguns quadros recebidos</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>Configurar TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>Executar TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>Tempo de Execução Estimado</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Parar em falha</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Visualizar relatório</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>Visualizar Relatório TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completado</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Falha</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Aprovado</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Parado por usuário</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>Perda de sinal.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>Perda do link.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>Comunicação com o conjunto do teste remoto foi perdida. Favor reestabelecer o canal de comunicação e tentar outra vez.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Um problema foi encontrado ao configurar a definição do teste local, comunicação com a definição do teste remoto foi perdida. Favor reestabelecer o canal de comunicação e tentar outra vez.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Um problema foi encontrado durante a configuração do conjunto de teste remoto, a comunicação com o conjunto de teste foi perdida. Por favor, restabeleça o canal de comunicação e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>Um problema foi encontrado ao configurar a definição do teste local, TrueSAM vai sair agora.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Um problema foi encontrado na tentativa de configurar a definição do teste local. O teste foi forçado a parar e o canal de comunicação com a definição do teste remoto foi perdido.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Um problema foi encontrado na tentativa de configurar a definição do teste remoto. O teste foi forçado a parar, e o canal de comunicação com a definição do teste remoto foi perdido.</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>O protetor de tela foi inativado para prevenir interferência enquanto o teste é executado.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>Um problema foi encontrado na definição do teste local. TrueSAM agora vai sair.</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>Solicitando parâmetros DHCP.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>Parâmetros DHCP obtidos.</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>Inicializando interface Ethernet. Aguarde.</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>América do Norte</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>América do Norte e Coréia</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>PCS North America</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Índia</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>Perdeu o sinal da hora do dia a partir da fonte de tempo externa, fará com que sincronia 1PPS apareça desativada.</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>Iniciando sincronização com sinal de hora do dia a partir de fonte de tempo externa...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>Sincronizou com sucesso com sinal de hora do dia a partir de fonte de tempo externa.</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>Loop&#xA;Down</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>Páginas de Cobertura</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>Selecionar testes para executar</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>Teste de Conectividade de Tráfego de Ponta a Ponta</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>SAMComplete / RFC 2544 melhorado</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>Suíte de Testes de Referência de Ethernet</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Testes de desempenho e configuração dos serviços de Ethernet Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>Teste de Transparência da Camada 2 para Quadros do Plano de Controle (CDP, STP, etc).</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>Teste de desempenho e taxa de transferência de RFC 6349 TCP</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>Tipo de fonte L3</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>Configurações para canal de comunicação (usando serviço 1)</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>Configurações do canal de comunicação</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>O serviço 1 deve ser configurado para corresponder à stream 1 no instrumento de teste Viavi remoto.</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>Status Local</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>Sinc ToD</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>Sinc 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>Conectar&#xA;ao canal</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>Nível físico</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>Duração da pausa (Quanta)</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>Duração da pausa (tempo - ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>Executar&#xA;testes</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>Iniciando&#xA;testes</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>Parando&#xA;Testes</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>Tempo estimado para executar testes</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>Interromper em caso de falha</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>Critérios de Aprovar/Reprovar para testes escolhidos</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>Upstream e Downstream</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>Este não tem conceito de Aprovar/Reprovar, então esta configuração não se aplica.</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>Aprovar se seguimento dos limites foram satisfeitos:</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>Nenhum rendimento definido - não informará Aprovado/Reprovado.</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>Local:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Remoto:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>Local e remoto:</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>Rendimento (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>Rendimento (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>Rendimento (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolerância para perda de pacotes (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>Latência (us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>Aprovar se seguimento dos parâmetros de SLA foram satisfeitas:</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>Upstream:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>Downstream:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>Vazão CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR Rendimento (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>CIR Rendimento (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>Vazão EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>EIR Rendimento (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>EIR Rendimento (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - Tolerância (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - Tolerância (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - Tolerância (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>Atraso de quadro (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>Variação de Delay (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>Aprovar/Reprovar será determinado internamente.</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>Aprovar se rendimento medido de TCP satisfaz os seguintes limites:</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>Percentual de rendimento previsto</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>Teste de Rendimento TCP não foi ativado - não comunicará Aprovado/Reprovado.</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>Parar Testes</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>Isto também vai parar o teste sendo presentemente executado, e adiará a execução do teste. Tem certeza de que quer parar?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga total máxima para Serviço (s) #1 é 0 ou excede a taxa de linha de #2 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga total máxima para a direção Upstream de Serviço(s) #1 é 0 ou excede a taxa de linha #2 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga total máxima para a direção Downstream de Serviço(s) #1 é 0 ou excede a taxa de linha #2 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga total máxima para Serviço (s) #1 é 0 ou excede a de taxa de linha (s) L2 Mbps respectiva.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga total máxima para a direção Upstream de Serviço(s) #1 é 0 ou excede a taxa de linha #2 L1 Mbps respectiva.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga total máxima para a direção Downstream de Serviço(s) #1 é 0 ou excede a taxa de linha #2 L1 Mbps respectiva.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga máxima é superior a de taxa de linha de #1 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga máxima excede a taxa de linha #1 L1 Mbps ascendente</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>Configuração inválida:&#xA;&#xA;A Carga Máxima excede a taxa de linha de #1 L1 Mbps na direção Descendente.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga máxima é superior a de taxa de linha de #1 L2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga máxima excede a taxa de linha #1 N2 Mbps na direção Upstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>Configuração inválida:&#xA;&#xA;A carga máxima excede a taxa de linha #1 N2 Mbps na direção Downstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>Configuração inválida:&#xA;&#xA;CIR, EIR e Policing não podem todos ser 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>Configuração inválida:&#xA;&#xA;CIR, EIR e Policing não podem ser todos 0, na direção da entrada (upstream).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>Configuração inválida:&#xA;&#xA;CIR, EIR e Policing não podem ser todos 0, na direção de saída (downstream).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>Configuração inválida:&#xA;&#xA;O menor Step Load (#1% de CIR) não pode ser obtido usando-se a configuração CIR #2 Mbps para o serviço #3. O menor valor de Step, usando o CIR atual para o serviço #3 é #4%.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (L1 Mbps) total é 0 ou excede a de taxa de linha de #1 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (L1 Mbps) total para a direção Upstream é 0 ou excede a de taxa de linha de #1 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (L1 Mbps) total para a direção Downstream é 0 ou excede a de taxa de linha de #1 L1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (N2 Mbps) total é 0 ou excede a taxa de linha #1 N2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (N2 Mbps) total para a direção Upstream é 0 ou excede a taxa de linha #1 N2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (N2 Mbps) total para a direção Downstream é 0 ou excede a taxa de linha #1 N2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>Configuração inválida:&#xA;&#xA;Devem ser selecionados ou o teste de configuração do serviço ou o teste de desempenho do serviço.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>Configuração inválida:&#xA;&#xA;Pelo menos um serviço deve estar selecionado.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;O total dos serviços selecionados CIR (ou EIR para os serviços que têm um CIR de 0) não poderá exceder a taxa de linha, se desejar executar o teste de Desempenho do Serviço.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;A medida do valor máximo especificado para a vazão (Throughput - RFC 2544) não pode ser menor do que a soma dos valore CIR dos serviços selecionados, ao executar o teste de desempenho do serviço (Service Performance test).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;A medida do valor máximo especificado para a vazão (Throughput - RFC 2544) não pode ser menor do que a soma dos valores CIR dos serviços selecionados, ao executar o teste de desempenho do serviço (Service Performance test).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;A medida do valor máximo especificado para a vazão, na direção de saída (Throughput - RFC 2544), não pode ser menor do que a soma dos valores CIR dos serviços selecionados, ao executar o teste de desempenho do serviço (Service Performance test).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;A medida do valor máximo especificado para a vazão (Throughput - RFC 2544) não pode ser menor do que o valor CIR, ao executar o teste de desempenho do serviço (Service Performance test).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;A medida do valor máximo especificado para a vazão (Throughput - RFC 2544), na direção da entrada, não pode ser menor do que o valor CIR, ao executar o teste de desempenho do serviço (Service Performance test).</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuração inválida:&#xA;&#xA;A medida do valor máximo especificado para a vazão na direção de saída (Throughput - RFC 2544) não pode ser menor do que o valor CIR, ao executar o teste de desempenho do serviço (Service Performance test).</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>O SAMComplete foi interrompido porque foi selecionado 'Stop on Failure' (interromper em caso de falha) e pelo menos um KPI não satisfez o SLA para o Serviço #1.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>O SAMComplete foi interrompido porque não houve retorno de dados no Serviço #1 no prazo de 10 segundos após o início do tráfego. A extremidade remota pode não estar mais retornando o tráfego.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>O SAMComplete foi interrompido porque não houve retorno de dados no Serviço #1 no prazo de 10 segundos após o início do tráfego. A extremidade remota pode não estar mais transmitindo tráfego.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>O SAMComplete foi interrompido porque não houve retorno de dados durante os 10 segundos após o tráfego ter sido iniciado. A extremidade remota pode não estar mais retornando o tráfego.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>O SAMComplete foi interrompido porque não houve retorno de dados durante os 10 segundos após o tráfego ter sido iniciado. A extremidade remota pode não estar mais transmitindo tráfego.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo local é um aplicativo Streams e o remoto, no endereço IP #1, é um aplicativo Traffic.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo local é um aplicativo Streams e o remoto, no endereço IP #1, é um aplicativo TCP WireSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo local é um aplicativo Layer 3 e o remoto, no endereço IP #1, é um aplicativo Layer 2.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>Os aplicativos local e remoto são incompatíveis. O aplicativo local é um aplicativo Layer 2 e o remoto, no endereço IP #1, é um aplicativo Layer 3.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>A aplicação remota no IP no endereço #1 é definido para o IP WAN. Não é compatível com SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>O aplicativo remoto no endereço de IP #1 está definido para encapsulamento de Stacked VLAN. Ele não é compatível com SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>O aplicativo remoto, no endereço IP #1, está configurado para encapsulamento VPLS. Ele não é compatível com o SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>O aplicativo remoto, no endereço IP #1, está configurado para encapsulamento MPLS. Ele não é compatível com o SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>O aplicativo remoto suporta apenas #1 serviços. </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>O teste One Way Delay exige que as unidades de teste tanto local como remota possuam sincronização OWD Time Source.  Falha na sincronização One Way Delay na unidade local.  Verifique todas as conexões do hardware de OWD Time Source.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>O teste One Way Delay exige que as unidades de teste tanto local como remota possuam sincronização OWD Time Source.  Falha na sincronização One Way Delay na unidade remota.  Verifique todas as conexões do hardware de OWD Time Source.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>Configuração inválida:&#xA;&#xA;Deve ser executado um teste Round-Trip Time (RTT) antes de executar o teste SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossível estabelecer uma sessão TrueSpeed. Vá à página "Network", verifique as configurações e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Não foi possível estabelecer a sessão TrueSpeed para a direção para upload. Vá para a página "Rede", verifique as configurações e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Não foi possível estabelecer a sessão TrueSpeed para a direção para download. Vá para a página "Rede", verifique as configurações e tente novamente.</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>O teste Round-Trip Time (RTT) foi invalidado pela alteração dos serviços atualmente selecionados para serem testados. Vá para a página "TrueSpeed Controls" e execute novamente o teste RTT.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (Mbps) total para a direção para download para serviço(s) #1 não pode ser 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (Mbps) total para a direção para upload para serviço(s) #1 não pode ser 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>Configuração inválida:&#xA;&#xA;O CIR (Mbps) para Serviço(s) #1 não pode ser 0.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Nenhum tráfego recebido. Favor ir para a página "Network", verificar configurações e tentar outra vez.</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>Visualização de resultado principal</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>Parar&#xA;Testes</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>Relatório criado, clicar em Próximo para visualizar.</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>Pular visualização do relatório</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete - Teste de ativação do serviço Ethernet</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>com TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>Configurações da rede local</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>A unidade local não requer configuração.</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>Configurações da rede remota</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>A unidade remota não requer configuração.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>Configurações do IP Local</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>Configurações de IP Remoto</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Serviços</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>Marcando</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>Marcação não é usada.</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>Vazão SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>Política SLA</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>Nenhum teste de policiamento selecionado.</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>Ruptura de SLA</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>Testes de ruptura foi desativado devido à ausência de funcionalidade necessária.</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>Desempenho SLA</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>TrueSpeed está desabilitado, no momento.&#xA;&#xA;TrueSpeed pode ser habilitado na página "Network" das configurações, quando não estiver no modo de medição em Loopback.</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>Configurações locais avançadas</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>Configurações avançadas de tráfego</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>Configurações avançadas LBM</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>Configurações avançadas SLA</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>Tamanho aleatória/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>IP avançado</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 Avançado</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>Guia Avançado</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>Teste Config do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>Teste Perf do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Sair do teste Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>Status do Servidor Discovery</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>Mensagem do Servidor Discovery</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>Endereço MAC e modo de ARP</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>Seleção de SFP</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>IP da origem WAN</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>IP Estático - WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>Gateway WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>Máscara da subrede WAN</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>IP Origem de Tráfego</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>Máscara sub-rede de tráfego</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>O guia VLAN é usado no portal do network Local?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-em-Q (VLAN empilhados)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>Aguardando</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>Acessando o Servidor ...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>Não foi possível acessar o servidor</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP Obtido</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>Concessão autorizada: #1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>Concessão Reduzida: #1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>Concessão Liberada</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>Servidor Discovery:</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 ms FTD, #3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>Serviço 1: VoIP - 25% de tráfego - quadros de byte #1 e #2</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>Serviço 2: Telefonia com vídeo - 10% do Tráfego - quadros de byte #1 e #2</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>Serviço 3: Dados de Prioridade - 15% de tráfego - quadros de byte #1 e #2</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>Serviço 4: Dados BE - 50% de tráfego - quadros de byte #1 e #2</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>Todos os serviços</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 1</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>No 10GE, o nível de granularidade da largura de banda é de 0.1 Mbps; como resultado, o valor de CIR é um múltiplo desse nível de granularidade</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>No 40GE, o nível de granularidade é de 0.4 Mbps; como resultado, o valor CIR é o múltiplo do nível de granularidade</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>No 100GE, o nível de granularidade da largura de banda é de 1 Mbps; como resultado, o valor de CIR é um múltiplo desse nível de granularidade</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>Configurar Tamanhos</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>Tamanho do quadro (byte)</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>Duração definida</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>Tamanho do Ciclo EMIX</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Comprimento do pacote (byte)</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Calc. Comprimento do quadro</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>O cálculo do tamanho do quadro é determinado através da duração do pacote e do encapsulamento</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 2</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 3</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 4</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 5</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 6</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 7</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 8</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 9</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>Propriedades Triple Play do serviço 10</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>Subdimensionado</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>Número de serviços</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>Nível</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>Definições LBM</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>Configurar Triple Play...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>Propriedades do Serviço</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>DA MAC e as configurações de tamanho do quadro</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>Configurações de DA MAC, tamanho de quadro e EtherType</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>DA MAC, configurações TTL e comprimento de informação.</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>Definições DA MAC e comprimento do pacote</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>Configurações da rede (avançado)</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>Uma incompatibilidade de unidade local/remota requer um comprimento de pacote de bytes de &lt;b>%1&lt;/b> ou maior.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>Assistência</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configurar ...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>  Tamanho do usuário</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL (salta)</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>Endereço MAC do dest.</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>Exibir ambos</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>Apenas remoto</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>Apenas local</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>Definições LBM (avançado)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>Configurações de rede Tamanho aleatório/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 1/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>Tamanho do quadro 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>Comprimento de pacote 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>Tamanho do usuário 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>Tamanho Jumbo 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>Tamanho do quadro 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>Comprimento de pacote 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>Tamanho do usuário 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>Tamanho Jumbo 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>Tamanho do quadro 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>Comprimento de pacote 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>Tamanho do usuário 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>Tamanho Jumbo 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>Tamanho do quadro 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>Comprimento de pacote 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>Tamanho do usuário 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>Tamanho Jumbo 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>Tamanho do quadro 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>Comprimento de pacote 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>Tamanho do usuário 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>Tamanho Jumbo 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>Tamanho do quadro 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>Comprimento de pacote 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>Tamanho do usuário 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>Tamanho Jumbo 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>Tamanho do quadro 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>Comprimento de pacote 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>Tamanho do usuário 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>Tamanho Jumbo 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>Tamanho do quadro 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>Comprimento de pacote 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>Tamanho do usuário 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>Tamanho Jumbo 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 1 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>Serviço 1 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 2/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 2 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>Serviço 2 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 3/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 3 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>Serviço 3 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 4/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 4 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>Serviço 4 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 5/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 5 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>Serviço 5 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 6/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 6 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>Serviço 6 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 7/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 7 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>Serviço 7 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 8/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 8 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>Serviço 8 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 9/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 9 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>Serviço 9 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>Serviço Aleatório 10/Comprimentos de EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>Serviços 10 Comprimentos aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>Serviço 10 Comprimentos aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>Os serviços tem Identificações VLAN ou Prioridades de Usuário diferentes?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>Não, todos os serviços usam as mesmas definições VLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Bit DEI</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>Avançado...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>Pri. Do usuário</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>Pri. da SVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>Configuração de Marcação de VLAN (Encapsulamento) </translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>Prioridade SVLAN</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>Todos os serviços usarão o mesmo IP de destino do tráfego.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>IP de destino do tráfego</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP de dest.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>IP Dest. Tráfego</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>Definir IP ID incrementando</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>Definições de IP (localmente)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>Configurações IP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>Definições de IP (localmente)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>Definições IP (avançado)</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>Configurações avançadas IP</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>Os serviços tem definições TOS ou DSCP diferentes?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>Não, TOS/DSCP é o mesmo em todos os serviços</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>SLAs agregados</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>CIR agregado</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>CIR Agregado Ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>CIR Agregado Descendente </translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>EIR agregado</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>EIR Agregado Ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>EIR Agregado Descendente </translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>Modo agregado</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>Máx. teste de vazão Local (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>Máx. teste de vazão Remoto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>Habilitar Modo Agregado</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>AVISO: Os valores de peso selecionados atualmente somam até &lt;b>%1%&lt;/b>, não 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>Vazão de SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>Taxa de Transferência de SLA, L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>Taxa de Transferência de SLA, L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>Taxa de Transferência de SLA, L1 Mbps (sentido único)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>Taxa de Transferência de SLA, L2 Mbps (sentido único)</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>Peso (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Policiamento</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>Carga máxima</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>Somente downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>Somente upstream</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>Definir Configurações avançadas de tráfego</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>Controle de SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>Policiamento do máx.</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>Realizar teste de ruptura</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>Tolerância (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>Tolerância (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>Gostaria de realizar teste de ruptura?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>Teste de Tipo de Ruptura:</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>Definir Configurações Avançadas de ruptura</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>Precisão de Atraso de Quadro</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>Incluir um atraso de quadro como uma solicitação SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>Incluir uma variação do atraso do quadro como uma solicitação SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>Desempenho SLA (sentido único)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Atraso de quadro (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Atraso de quadro (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>Definir Configurações Avançadas de SLA</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>Config. do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>Número de etapas abaixo de CIR</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>Duração da etapa (seg)</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>CIR % da etapa 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>CIR % da etapa 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>CIR % da etapa 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>CIR % da etapa 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>CIR % da etapa 5</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>CIR % da etapa 6</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>CIR % da etapa 7</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>CIR % da etapa 8</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>CIR % da etapa 9</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>CIR % da etapa 10</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>Percentual de Passos</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>Desempenho do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>Cada direção é testada separadamente, de modo geral a duração do teste será duas vezes o valor inserido.</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Parar teste em falha</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>Configurações Avançadas de Teste de Ruptura</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>Pular o J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Selecione os testes Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>Seleciones os serviços a testar</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>Ajustar tudo</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  Total:</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>Teste da configuração do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>Teste do desempenho do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>Medições opcionais</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>Vazão (Throughput RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>Máx. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>Máximo (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>Máximo (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>As medições opcionais não poderão ser feitas quando um serviço TrueSpeed tiver sido ativado.</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Execute os testes Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Pular os testes Y.1564</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Atraso</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>Delay Var</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>Resumo das falhas no teste</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>Resultados</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Resultado Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>Resultado do teste de configuração</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>Resultado do teste de configuração Svc 1</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>Resultado do teste de configuração Svc 2</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>Resultado do teste de configuração Svc 3</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>Resultado do teste de configuração Svc 4</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>Resultado do teste de configuração Svc 5</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>Resultado do teste de configuração Svc 6</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>Resultado do teste de configuração Svc 7</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>Resultado do teste de configuração Svc 8</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>Resultado do teste de configuração Svc 9</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>Resultado do teste de configuração Svc 10</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Resultado do teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Resultado do Svc 1 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Resultado do Svc 2 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Resultado do Svc 3 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Resultado do Svc 4 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Resultado do Svc 5 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Resultado do Svc 6 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Resultado do Svc 7 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Resultado do Svc 8 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Resultado do Svc 9 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Resultado do Svc 10 no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Resultado do IR no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Resultado da perda de quadros no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Resultado do atraso no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Resultado da variação do atraso no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Resultado do TrueSpeed no teste Perf</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>Resultados da configuração do Serviço 1</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 1</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>Taxa de Transferência máx (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>Taxa de Transferência máx Downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>Taxa de Transferência máx Upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>Taxa de Transferência máx (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>Taxa de Transferência máx Downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>Taxa de Transferência máx Upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>Resultado CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>Resultado CIR Downstream</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>Variação de atraso de quadro (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>Contagem Oos</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>Detecção de Erro de pocesso</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>Detectar Pausa</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>Resultado CIR Upstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>Resultado CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>Resultado CBS Downstream</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>Configurar Tamanho de Explosão (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Tamanho de Ruptura Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>Rx Médio Tamanho da Rajada (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>CBS Estimado (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>Resultado CBS Upstream</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>Resultado EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>Resultado EIR Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>Resultado EIR Upstream</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>Policing Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>Policing Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>Policing Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>Etapa 1 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>Etapa 1 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>Etapa 1 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>Etapa 2 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>Etapa 2 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>Etapa 2 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>Etapa 3 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>Etapa 3 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>Etapa 3 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>Etapa 4 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>Etapa 4 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>Etapa 4 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>Etapa 5 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>Etapa 5 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>Etapa 5 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>Etapa 6 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>Etapa 6 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>Etapa 6 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>Etapa 7 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>Etapa 7 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>Etapa 7 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>Etapa 8 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>Etapa 8 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>Etapa 8 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>Etapa 9 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>Etapa 9 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>Etapa 9 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>Etapa 10 Resultado</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>Etapa 10 Resultado Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>Etapa 10 Resultado Upstream</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>Máximo Taxa de Transferência (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>Máximo Taxa de Transferência (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>Máx. Fluxo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>Etapas</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>Tecla</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>Clique nas barras para revisar os resultados de cada etapa.</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (Vazão Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (Vazão L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (Vazão L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>Detecção de Erro</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>Etapa 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>Etapa 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>Etapa 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>Etapa 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>Etapa 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>Etapa 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>Etapa 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>Etapa 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>Etapa 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>Etapa 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>Limiares SLA</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 2</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 3</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 4</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 5</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 6</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 7</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 8</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 9</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>Resultados da configuração do Serviço 10</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>Resultados do desempenho do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Svc. Resultado</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR Atl.</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR Máx.</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR Mín.</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR Méd.</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>Segundos de perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>Contagem de perdas de pacotes</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>Atraso atual</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>Atraso máximo</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>Atraso mínimo</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>Atraso médio</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>Var. atraso Atual</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>Var. atraso Máx.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>Var. atraso Mín.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>Var. atraso Média</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>Disponibilidade</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponível</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>Segundos disponíveis</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>Segundos não disponível</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>Severely Errored Seconds (segundos com erros graves)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>Fluxo de Perf do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Visão geral</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variação de Delay</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>Serviço TrueSpeed Veja os resultados na página de resultados do TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR, Med&#xA;(Vazão&#xA;L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR, Med&#xA;(Vazão&#xA;L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>Razão de perda&#xA;de quadros</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>Atraso em um só sentido&#xA;Médio (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>Delay Var.&#xA;Máx. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>Erros Detectados</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>Atraso&#xA;Média (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, Cur.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, Cur.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, Max.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, Max.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, Min.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, Min.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>Contagem de&#xA;perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>Limiar&#xA;da Taxa&#xA;de Perda de quadros</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>Atraso&#xA;Atual (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>Atraso&#xA;Máximo (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>Atraso&#xA;Mínimo (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>Atraso&#xA;Média (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>Limite&#xA;de atraso&#xA;(OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>Atraso&#xA;Atual (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>Atraso&#xA;Máximo (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>Atraso&#xA;Mín. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>Limite&#xA;de atraso&#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>Var. atraso&#xA;Atual (ms):</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>Delay Var.&#xA;Média (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>Delay Var.&#xA;Limite (ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>Razão de&#xA;segundos&#xA;disponíveis</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>Segundos&#xA;não disponível</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>Severely&#xA;Errored&#xA;Seconds</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Config de Serviço&#xA;Taxa de Transferência&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Config de Serviço&#xA;Taxa de Transferência&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Perf de Serviço&#xA;Taxa de Transferência&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Perf de Serviço&#xA;Taxa de Transferência&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>Configurações da rede</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Tamanho do quadro do usuário</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>Tamanho Jumbo de quadro</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>Comprimento do pacote do usuário</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>Comprimento do pacote Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>Calc. Tamanho do quadro (byte)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>Configurações de rede (Tamanho aleatório/EMIX)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>Comprimentos Aleatórios/EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>Comprimentos Aleatórios/EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>Comprimentos Aleatórios/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>Configurações avançadas IP - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>CIR para download</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>EIR para download</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>Policiamento para download</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>M para download</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>CIR para upload</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>EIR para upload</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>Policiamento para upload</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>M para upload</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>Ruptura avançada de SLA</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>Proporção de perda de quadros para download</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>Atraso de quadro para download (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>Atraso de quadro para download (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>Variação do atraso para download (ns)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>Proporção de perda de quadros para upload</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>Atraso de quadro para upload (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>Atraso de quadro para upload (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>Variação do atraso para upload (ns)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>Incluir como uma solicitação SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>Incluir o atraso de quadro</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>Incluir a variação do atraso do quadro</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Completo tem a seguinte instalação de configuracão inválida:</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>Resultados da configuração do Serviço</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>Executar TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>OBSERVAÇÃO: O teste TrueSpeed só será executado se o teste de desempenho de serviço estiver habilitado.</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>Definir pacote Duração TTL</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>Definir portas TCP/UDP</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>Tipo Src.</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Src. Porta</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>Dest.   Tipo</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>Dest.   Porta</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>Limite de taxa de transferência TCP</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>Tamanho Total recomendado de janela (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>Tamanho total da janela  impulsionada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>Conexões # Recomendadas</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>Nº de conexões impulsionadas</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>Tamanho Total recomendado de janela Upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>Tamanho de janela do total impulsionado upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation># de Conexões Upstream Recomendadas</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>Upstreams impulsionados</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>Tamanho Total recomendado de janela Downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>Tamanho de janela total impulsionada de downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation># de Conexões Downstream Recomendadas</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>Nº de conexões impulsionadas de downstream</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>Recomendado # de conexões</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>Vazão TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Carga útil Acterna</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>Tempo de viagem (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>O RTT será usado em etapas subsequentes para criar um tamanho de janela e um número de recomendação de conexões.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>Resultados do TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>Métrica de transferência TCP</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>L4 alvo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>Resultado do serviço TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>Veredito do serviço TrueSpeed para upload</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>Taxa de transferência TCP real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>Taxa de transferência TCP real para upload (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>Taxa de transferência TCP alvo para upload (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>Taxa de transferência TCP alvo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>Veredito do serviço TrueSpeed para download</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>Taxa de transferência TCP real para download (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>Taxa de transferência TCP alvo para download (Mbps)</translation>
        </message>
    </context>
</TS>
