<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>Dieses Prüfgerät wurde gesperrt, um unbefugten Zugriff zu verhindern.</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>Kein Kennwort erforderlich. Drücken Sie auf 'OK', um das Gerät zu entsperren.</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>Prüfgerät entsperren?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>Geben Sie zum Entsperren das Kennwort ein:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>Kennwort eingeben</translation>
        </message>
    </context>
</TS>
