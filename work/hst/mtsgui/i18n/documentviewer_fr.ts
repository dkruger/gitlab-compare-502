<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Sortie</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>Chargement : </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>Échec dans le chargement PDF</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>Échec dans la restitution de la page : </translation>
        </message>
    </context>
</TS>
