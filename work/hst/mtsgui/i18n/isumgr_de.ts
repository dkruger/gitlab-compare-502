<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>Ungültige IP-Adressenzuordnung wurde abgelehnt.</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>Ungültige Gateway-Zuordnung: 172.29.0.7. MTS-System wird neu gestartet.</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>IP-Adresse wurde geändert. Bitte BERT Modul neu starten.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>BERT-Modul wird initialisiert. AUS-Anforderung zurückgewiesen.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>BERT-Modul wird mit optische Jitterfunktion initialisiert. OFF-Anforderung zurückgewiesen.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>BERT-Modul wird heruntergefahren</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>BERT Modul ausgeschaltet</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>Das BERT Modul ist aus. HOME/SYSTEM drücken, dann das BERT Symbol, um es zu aktivieren.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>BERT Modul eingeschaltet</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** UPGRADE DER APPLIKATIONSSOFTWARE *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** UPGRADE ABGESCHLOSSEN *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** UPGRADE DER JITTER-SOFTWARE *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** UPGRADE DER KERNEL-SOFTWARE *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** UPGRADE GESTARTET *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>BERT Modul: Fehler in der Benutzeroberfläche </translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Benutzeroberfläche des BERT Moduls mit optischem Jitter wird aufgebaut</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>BERT Modul-UI wird gestartet</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>BERT Modulfehler. Modul ausgeschaltet.</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>Modul 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>Modul 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>Modul 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>Modul 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation> Modul </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>Modul 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>Modul 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>Modul 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>Modul 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>Modul 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>Modul 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>Modul 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>Modul 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>Modul 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>Modul 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>Modul 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>Modul 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>Modul 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>Modul 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>Modul 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>Modul 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>Modul 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>Modul 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>Basissoftware-Upgrade notwendig. Die aktuelle Version ist nicht mit dem BERT Modul kompatibel.</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>BERT Software Reinstallierung notwendig. Die richtige BERT Software ist nicht installiert für das zugefügte BERT Modul.</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>BERT Hardware-Upgrade erforderlich Die derzeitige Hardware ist nicht kompatibel.</translation>
        </message>
    </context>
</TS>
