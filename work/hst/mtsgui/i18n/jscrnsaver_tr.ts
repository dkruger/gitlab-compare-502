<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
