<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>Configurar</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>Editar configuración anterior</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>Cargar configuración desde un perfil</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>Iniciar nueva configuración (por defecto)</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>Manualmente</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>Configurar configuraciones de prueba manualmente</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>Configuración de prueba</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>Guardar perfiles</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>Extremo: Configurar manualmente</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>Ejecutar pruebas</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>Guardado</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>Cargar perfiles</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>Extremo: Cargar perfiles</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>Editar configuración</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Medida</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Ejecutar</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>Ejecutar comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>Verificación de SFP</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>Finalizar: Medida</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crear Informe</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>Repetir prueba</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>Ver Resultados detallados</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>Salir de comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>Revisar</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resulta.</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>Interfaz</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Capa 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>Final: revisar resultados</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>Información del reporte</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>Extremo: Crear reporte</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>Revisar resultados detallados</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Corriendo</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abortado</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>INCOMPLETO</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>COMPLETO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FAIL</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>PASA</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NINGUNO</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>Comprobación de CPRI:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** Iniciando comprobación de CPRI ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** Prueba terminada ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** Prueba cancelada ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>Omitir guardar perfiles</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>Puede guardar la configuración usada para ejecutar esta prueba.&#xA;&#xA;Se puede usar (en un todo o seleccionando "perfiles"&#xA;individuales) para configurar futuras pruebas.</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>Omitir cargar perfiles</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>Verificación de SFP local</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Conector</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>Inserte un SFP.</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>Long onda(nm) SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>Fabricante SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>Rev Fabr SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>N/P de proveedor de SFP</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>Velocidades recomendadas</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>Mostrar datos adicionales de SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP correcto.</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>No ha sido posible verificar SFP para esta velocidad.</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP inadmisible.</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Parámetros</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Duración test</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>Dispositivo más alejado</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Otro</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>Bucle Hardware</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Si</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>Límite máx. de nivel de recepción óptico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>Límite mín. de nivel de recepción óptico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>Límite máx. de retardo de ida y vuelta (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>Omitir comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>Comprobación de SFP</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>Tecla Estado de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Completo</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Pasa</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fallo</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>Programado</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>Correr&#xA;pruebas</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Detener&#xA;Prueba</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>Resultados de SFP local</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>No se ha detectado SFP.</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Longitud de onda (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Nivel Tx Máx(dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Nivel Rx Máx(dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Transceptor</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>Resultados de interfaz</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>Veredictos de la prueba de comprobación de CPRI</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>Prueba de interfaz</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>Prueba de Capa 2</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>RTD Test</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>BERT Test</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>Señal presente</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Adq Sincr.</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Rx Desv. Máx Frec (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violaciones Código</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>Nivel Óptico Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>Resultados de Capa 2</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>Estado de arranque</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>Sincr. trama</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>Resultados RTD</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>Retardo de ida y vuelta, promedio (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>Resultados BERT</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>Sincr. patrón</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>Pérdidas de patrón</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Errores Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>Configuraciones</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>Tipo de equipo</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>Sincronización L1</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>Configuración del protocolo</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>Configuración del plano C&amp;M</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>Funcionamiento</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>Enlace pasivo</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>Omitir creación de informe</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Información del reporte de prueba</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>Nombre del Cliente:</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>ID del técnico:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>Ubicación de la prueba:</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>Orden de trabajo:</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>Comando / Notas:</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>Radio:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>Banda:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>Estado general</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Prueba FC mejorada</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC Test</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Simetría</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>Configuraciones locales</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>Conectar a remota</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Red</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>Configuración del canal de fibra</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>Pruebas FC</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>Plantillas de configuración</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Seleccionar pruebas</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>Utilización</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Longitudes Trama</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Test Throughput</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Medida Pérdidas de Tramas</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>Prueba opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Buffer Credit Test</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>Probar Ctls</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>Duraciones de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>Probar umbrales</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>Cambie la configuración</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>Configuración de canal de fibra avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>Configuración de utilización avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>Configuración de latencia de rendimiento avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>Configuración de prueba opuesto con opuesto avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>Configuración de prueba de pérdida de marco avanzada</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>Ejecutar pruebas de activación de servicio</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>Cambie la configuración y ejecute de nuevo la prueba</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latencia</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Pérdida Trama</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>Salir de prueba FC</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>Crear otro informe</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>Página de portada</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>Se ha perdido la sincronización para retardo de ida en el equipo local durante la prueba.  Por favor, revise las conexiones de OWD Time Source. La prueba continuará pero los resultados de retardo de trama no estarán disponibles.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>Se ha perdido la sincronización para retardo de ida en el equipo remoto durante la prueba. Por favor, revise la conexión para OWD Time Source. La prueba continuará, pero los resultados de retardo de trama no estarán disponibles.</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>Bucle activo encontrado</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>La resolución de dirección vecina no tuvo éxito.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>Servicio #1: Enviando solicitud ARP para destino MAC.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>Servicio #1 en el lado local: Enviando solicitud ARP a MAC de destino.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>Servicio #1 en el lado remoto: Enviando solicitud ARP a MAC de destino.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Enviando peticon ARP para MAC destino.</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>Equipo local enviando la solicitud ARP al MAC de destino.</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>Equipo remoto enviando solicitud ARP para MAC de destino.</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>El puerto del elemento de red está aprovisionado para su funcionamiento en dúplex medio. Si desea proceder, pulse el botón "Continuar en dúplex medio". En caso contrario, pulse "Abortar prueba".</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>Intentando bucle remoto. Comprobando si hay bucle activo.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>Revisando en búsqueda de un bucle de hardware.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Revisión de un bucle LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>Revisando si hay un bucle permanente.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>Expiró la solicitud de parámetro DHCP. No se pudieron obtener parámetros DHCP.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>Al seleccionar el modo Bucle, no está conectado con el equipo remoto.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>Expiró el tiempo de SAMComplete, porque no se pudo determinar un conteo final de tramas recibidas. El conteo en la medición de tramas recibidas continuó incrementando inesperadamente.</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>Bucle duro encontrado</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>J-QuickCheck no puede realizar la prueba de conectividad de tráfico a menos que se establezca una conexión a la unidad remota.</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>Bucle LBM/LBR encontrado</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>Se ha caído el enlace y la conexión a la unidad remota se cortó. Una vez que se restablezca el enlace puede intentar conectarse al equipo remoto de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>El enlace actualmente no está activo.</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>La IP del origen y destino son idénticos. Por favor, reconfigure su dirección IP de origen o destino.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación local es para tráfico y la aplicación remota en la dirección IP #1 es para Streams.</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>No se pudo establecer el bucle.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>No se ha conectado con la unidad remota. Por favor, vaya a la página "Conectar", verifique su dirección IP destino y después pulse "Conectar remoto".</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Vaya a la página "Red", verifique las configuraciones y vuelva a intentarlo.</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>Esperando que la óptica esté lista, ó la óptica no está presente.</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>Bucle permanente encontrado</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>Tiempo de espera de conexión PPoE. Por favor, compruebe su configuración PPoE y vuelva a intentarlo.</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>Se ha encontrado un error PPPoE irrecuperable</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Intentando iniciar sesión en el servidor...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>Se detectó un problema con la conexión remota. Es posible que no se pueda acceder al equipo</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>No se pudo conectar con la unidad remota en la dirección IP #1. Por favor, revise la dirección IP origen del equipo remoto e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>La aplicación remota de la dirección IP #1 es una aplicación de bucle. Es incompatible con RFC mejorado 2544.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación remota en la dirección IP#1 no es una aplicación TCP WireSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>La versión de software en la unidad remota parece ser muy nueva. Si continúa probándola, se puede limitar alguna funcionalidad. Se recomienda actualizar el software de esta unidad para un rendimiento óptimo.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>La versión de software en la unidad remota parece ser muy antigua. Si continúa probándola, se puede limitar alguna funcionalidad. Se recomienda actualizar el software de esta unidad para un rendimiento óptimo.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>La versión de software en la unidad remota no se puede determinar. Si continúa probándola, se puede limitar alguna funcionalidad. Se recomienda probar entre unidades con versiones equivalentes de software.</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>Intento de bucle sin éxito. Intentando de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>Se ha aplicado con éxito la configuración para la plantilla seleccionada.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>La versión de software de la unidad remota no pudo determinarse, o bien no coincide con la versión de esta unidad. Si continúa la comprobación, es posible que algunas funciones estén limitadas Se recomienda comprobar entre unidades con versiones de software equivalentes.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>No se ha podido completar el inicio de sesión explícito.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación local es capa #1 y la aplicación remota en la dirección IP #2 es capa #3.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>La velocidad de línea de la unidad local (#1 Mbps) no se corresponde con la de la unidad remota (#2 Mbps). Por favor, reconfigúrela a Asimétrica en la página "Connect"</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>La velocidad de línea de la unidad local (#1 kbps) no se corresponde con la de la unidad remota (#2 kbps). Por favor, reconfigúrela a Asimétrica en la página "Connect"</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>La velocidad de línea de la unidad local (#1 Mbps) no se corresponde con la de la unidad remota (#2 Mbps). Por favor, reconfigúrela a Asimétrica en la página "Connect" y reinicie la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>La velocidad de línea de la unidad local (#1 kbps) no se corresponde con la de la unidad remota (#2 kbps). Por favor, reconfigúrela a Asimétrica en la página "Connectar" y reinicie la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>La velocidad de línea de la unidad local (#1 Mbps) no se corresponde con la de la unidad remota (#2 Mbps). Por favor, reconfigúrela a Asimétrica en la página RFC2544 "Simetría".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>La velocidad de línea de la unidad local (#1 kbps) no se corresponde con la de la unidad remota (#2 kbps). Por favor, reconfigúrela a Asimétrica en la página RFC2544 "Simetría".</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>Configuración no válida:&#xA;&#xA;se debe seleccionar al menos una prueba. Por favor, seleccione al menos una prueba y vuelva a intentarlo.</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>No ha seleccionado ningún tamaño de marco a probar. Por favor, seleccione un tamaño de marco antes de empezar.</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>Se ha detectado una frecuencia de pérdida de tramas que supera el umbral de pérdida de tramas configurado en el último segundo. Por favor, verifique el funcionamiento del enlace con un test de tráfico manual. Una vez que haya realizado sus tests manuales puede volver a iniciar el RFC 2544.&#xA;Configuración del test manual recomendada:&#xA;Tamaño de trama: #1 bytes&#xA;Tráfico: constante con #2 Mbps&#xA;Duración del test: al menos 3 veces la duración del test configurado. la duración del test de rendimiento fue de #3 segundos.&#xA;Resultados al monitor:&#xA;Use la pantalla de resumen de estado para buscar eventos de error. Si los contadores de error aumentan de forma esporádica inicie el test manual a tasas de tráfico menores diferentes. Si todavía obtiene errores incluso en tasas más bajas el enlace puede que tenga problemas generales no relacionados con la carga máxima. Asimismo puede usar el gráfico de curva de frecuencia de pérdida de tramas de resultados gráficos para ver su los eventos de pérdida de tramas son esporádicos o constantes. Si no puede resolver el problema con los errores esporádicos puede fijar el umbral de pérdida de tramas para que tolere una frecuencia de pérdida de tramas menor. Nota: una vez que use la tolerancia de pérdida de tramas el test no cumple con la recomendación RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>Nota:  Debido a las diferentes profundidades de pila VLAN para el transmisory el receptor, se deberá ajustar la tasa configurada para compensar la diferencia de tasas entre los puertos.</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>Tramas #1 Byte</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>Paquetes #1 Byte</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>El conteo de marcos C2 Filter Rx Acterna ha continuado incrementándose en los últimos 10 segundos aunque se debería detener el tráfico</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Buscando la tasa máxima de throughput</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>Intento #1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>Intento #1 C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>Intentando #1 C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>Intento #1 C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>Intentando #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>Verificando #1 C1 Mbps. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>Verificando #1 C2 Mbps. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>Verificando ahora #1 C1 kbps. Esto tardará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>Verificando #1 C2 kbps. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>Verificando ahora #1 %.  Se tardará #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>Medición de transferencia máxima: #1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>Medición de transferencia máxima: #1 C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>Rendimiento máximo medido:#1 C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>Medición de transferencia máxima: #1 C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>Máxima velocidad de transmisión medida: #1</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>No está disponible una medición de velocidad máxima de transmisión (throughput)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>Prueba de pérdida de marcos (estándar RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>Prueba de pérdida de marcos (descendente)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>Prueba de pérdida de marcos (ascendente)</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>Ejecución de prueba a una carga #1 de C1 Mbps. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>Ejecución de prueba a una carga #1 de C2 Mbps. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>Poner en funcionamiento la prueba a una carga de #1 C1 kbps. Esto tardará #2 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>Ejecución de prueba a una carga #1 de C2 kbps. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>Realizando prueba a carga #1.  Se tardará #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Test Tramas Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>Prueba #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Se han detectado Tramas Pause</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 ráfaga paquetes: falla</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 ráfaga tramas: falla</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 ráfaga paquetes: pasa</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 ráfaga tramas: pasa</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>Prueba de extensión de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>Intentando una ráfaga de #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>Mayor de #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>Inferior a #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>El tamaño del buffer es #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>El tamaño del buffer es menor que #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>El tamaño del buffer es mayor o igual que #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>Marcos #1 enviados</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>Tramas #1 Recibidas</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>Tramas #1 Perdidas</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>Prueba de ráfaga (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>CBS estimado: #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>CBS estimada: no disponible</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>Se saltará la prueba CBS. El tamaño de la ráfaga de prueba es muy grande para probar con precisión esta configuración.</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>Se saltará la prueba CBS. La duración de la prueba no es lo suficientemente larga para probar con precisión esta configuración.</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>Ráfaga de paquetes: falla</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>Ráfaga de trama: falla</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>Ráfaga de paquetes: aprueba</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>Ráfaga de trama: aprueba</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>Intento #1 de monitoreo de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>Prueba de recuperación de sistema</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>La prueba no es válida para este tamaño de paquete</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>La prueba no es válida para este tamaño de marco</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>Prueba #1 de #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>No será posible inducir pérdida de trama porque la prueba de desempeño pasó a máximo ancho de banda sin pérdida de trama observada</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>Imposible inducir ningún evento de pérdida. La prueba no es válida por el tamaño del paquete.</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>Imposible inducir ningún evento de pérdida. La prueba no es válida por el tamaño del marco.</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>Tiempo de recuperación medio: superior a #1 segundos</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>Mayor de #1 segundos</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>Tiempo de recuperación medio: no disponible</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>Tiempo de recuperación medio: #1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 nosotros</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>Centrándose en el buffer de crédito óptimo. Probando #1 créditos</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>Midiendo #1 credits</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>Verificando ahora con #1 credits.  Esto llevará #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Buffer Credit Throughput Test</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>Nota: Asume un bucle duro si los creditos de acumulación son menos que 2. Esta prueba no es válida</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>Nota: Basado en el supuesto del bucle duro, las mediciones de rendimiento se hicieron con el doble del número de créditos búfer en cada etapa para compensar la doble longitud de la fibra</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>Midiendo Throughput en #1 Buffer Credits</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>Pruebas de rendimiento y latencia</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Pruebas de velocidad de transmisión y fluctuación de fase de paquete</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>Pruebas de rendimiento,m latencia y vibración del paquete</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>Prueba # 1 de latencia y variación de retraso de paquetes. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>Intento de prueba #1 de variación de retraso de paquetes. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>Intento de prueba #1 de latencia. Dura #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>Intento de prueba d latencia #1 a #2% de la carga de transferencia verificada. Dura #3 segundos</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 Iniciando RFC 2544 Test #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 Iniciando FC Test #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>Prueba completa.</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Guardando resultados, por favo, espere.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>Prueba de carga extendida</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>FC Test:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>Configuración de red</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Tipo de Trama</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>Modo de test</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>Mant. Nivel de dominio</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>Emisor TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulación</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profundidad de pila VLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Prioridad Usuario SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>Bit DEI SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>Usuario SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>Prioridad usuario CVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Prioridad Usuario</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>SVLAN Prioridad Usuario 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>Usuario SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>Bit DEI SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>SVLAN Prioridad Usuario 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>Usuario SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>Bit DEI SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>SVLAN Prioridad Usuario 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>Usuario SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>Bit DEI SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>SVLAN Prioridad Usuario 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>Usuario SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>Bit DEI SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>SVLAN Prioridad Usuario 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>Usuario SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>Bit DEI SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>SVLAN Prioridad Usuario 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>Usuario SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>Bit DEI SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>SVLAN Prioridad Usuario 1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>Usuario SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>Bit DEI SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Tipo Bucle</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC Fuente</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>Incremento automático SA MAC</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation>No. de MACs en sequencia</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>Desactivar  IP EtherType</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>Desactivar resultados OoS</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>Tipo DA</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>Destino MAC</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Modo Datos</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>Usar autenticación</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nombre de usuario</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Contraseña</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>Proveedor de servicio</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nombre del Servicio</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>Tipo IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Dirección IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>Gateway por defecto</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>Máscara Subred</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>Dir IP Destino</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Tiempo de vida (saltos)</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>Incrementando ID de IP</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>Enlace a fuente - Dirección local</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>Dirección global de fuente</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Long prefijo Subred</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Dirección Destino</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>Clase Tráfico</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>Etiqueta Flujo</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>Límite Saltos</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>Modo de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>Tipo de servicio de puerto de fuente</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>Puerto origen</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>Tipo de servicio de puerto de destino</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>Puerto destino</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>ATP Listen tipo IP</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>ATP Listen dirección IP </translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID Fuente</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>Destino ID</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID Secuencia</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID  Autor</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID Responder</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Configuración de prueba</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Versión Carga útil de Acterna</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>Precisión de la medición de latencia</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>Unidad de anchura de banda</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>Anchura de banda de prueba máxima (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>Anchura de banda de prueba máxima de subida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>Anchura de banda de prueba máxima (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>Anchura de banda de prueba máxima de subida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>Anchura de banda de prueba máxima de bajada (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>Anchura de banda de prueba máxima de bajada (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>Anchura de banda de prueba máxima (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>Permitir tráfico verdadero 100%</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>Precisión de medición de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>Precisión de la medición de rendimiento de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>Precisión de la medición de rendimiento de bajada</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Proceso de puesta a cero en velocidad de transmisión</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolerancia Pérd. Tramas Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolerancia de pérdida de marco de rendimiento de subida (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolerancia de pérdida de marco de rendimiento de bajada (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>Duración de todas las pruebas</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>Todos los números de ensayos de las pruebas</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>Duración de rendimiento (s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Umbral Pasa Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>Umbral de aprobado de rendimiento (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>Umbral de paso de rendimiento de subida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>Umbral de aprobado de rendimiento (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>Umbral de paso de rendimiento de subida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>Umbral de paso de rendimiento de bajada (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>Umbral de paso de rendimiento de bajada (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>Umbral Pasa Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>Número de ensayos de latencia</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>Duración de ensayo de latencia</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>Ancho de banda de latencia (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>Umbral de paso de latencia</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>Umbral de paso de latencia (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>Umbral de paso de latencia de subida (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>Umbral de paso de latencia de bajada (us)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Número de pruebas de Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>Duración del ensayo de vibraciones del paquete</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Jitter de paquete pasa el umbral</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>Umbral de aprobado de vibraciones del paquete (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>Umbral de paso de vibración de paquete de subida (us)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>Umbral de paso de vibración de paquete de bajada (us)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Frame Loss Test Procedure</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>Duración del ensayo de pérdida de marcos (s)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularidad de ancho de banda de pérdida de trama (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularidad de banda ancha de pérdida de marco de subida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularidad de anchura de banda de pérdida de marcos (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularidad de banda ancha de pérdida de marco de subida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularidad de banda ancha de pérdida de marco de bajada (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularidad de banda ancha de pérdida de marco de bajada (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>Gran. Ancho Banda Pérd. de Tramas (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>Gama de pérdida de marcos mínima (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>Gama de pérdida de marcos mínima (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>Gama de pérdida de marcos mínima (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>Gama de pérdida de marcos máxima (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>Gama de pérdida de marcos máxima (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>Gama de pérdida de marcos máxima (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Gama mínima de pérdida de marco de subida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>Gama mínima de pérdida de marco de subida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Gama máxima de pérdida de marco de subida (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>Gama máxima de pérdida de marco de subida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Gama mínima de pérdida de marco de bajada (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>Gama mínima de pérdida de marco de bajada (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Gama máxima de pérdida de marco de bajada (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>Gama máxima de pérdida de marco de bajada (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>Número de pasos de pérdida de marcos</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>Número de ensayos opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>Granularidad opuesto a opuesto (Marcos)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>Duración de ráfaga opuesto a opuesto máxima</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>Bajando la duración de subida máxima opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>Bajando la duración de bajada máxima opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>Ignorar tramas Pause</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>Tipo de prueba de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>Tamaño de ráfaga CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>Tamaño CBS de ráfaga de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>Tamaño CBS de ráfaga de bajada</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>Tamaño mínimo de extensión de ráfaga (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>Tamaño máximo de extensión de ráfaga (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>Tamaño mínimo de extensión de ráfaga de subida (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>Tamaño máximo de extensión de ráfaga de subida (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>Tamaño mínimo de extensión de ráfaga de bajada (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>Tamaño máximo de extensión de ráfaga de bajada (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>Duración (es) CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>Número de intentos de prueba de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>Mostrar aprobado / suspenso ráfaga CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>Mostrar aprobado / suspenso de extensión de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>Umbral de tamaño de extensión de ráfaga (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>Umbral de tamaño de extensión de ráfaga de subida (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>Umbral de tamaño de extensión de ráfaga de bajada (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>Número de ensayos de recuperación del sistema</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>Duración de la sobrecarga de recuperación del sistema (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>Duración de la carga ampliada</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>Escalado de rendimiento de carga ampliado (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>Longitud de paquete de carga ampliada</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>Longitud de trama de carga ampliada</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>Tipo de inicio de sesión de créditos del buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>Tamaño del buffer máximo de créditos del buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>Pasos de rendimiento de créditos del buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>Duración de créditos del buffer</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>Bucle remoto desactivado con éxito:Unidad **#1** Fuera de Modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>Bucle remoto desactivado con éxito:Unidad **#1** Fuera de Modo LLB Trsp</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>Unidad remota **#1** ya estaba sin bucle</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>Bucle descendente remoto por un cambio de configuración</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>Bucle remoto activado con éxito:Unidad **#1** en Modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>Bucle remoto activado con éxito:Unidad **#1** en Modo LLB Trsp</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>Unidad Remota **#1** ya estaba en Modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>Unidad Remota **#1** ya estaba en Modo LLB Trsp</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>Auto bucle u bucle en otro puerto no está soportado</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Flujo #1: Desactivación del bucle remoto sin éxito:Acknowledgement Timeout</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Flujo #1: Activación del bucle remoto sin éxito:Acknowledgement Timeout</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Bucle remoto desactivado sin éxito:Acknowledgement Timeout</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Bucle remoto trsp desactivado sin éxito:Acknowledgement Timeout</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Activación del bucle remoto sin éxito:Acknowledgement Timeout</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Activación del bucle remoto trsp sin éxito:Acknowledgement Timeout</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>Comenzó detección de dirección duplicada en dirección global</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>Falló configuración global de dirección (configuraciones de verificación).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>Esperando por la detección de dirección duplicada en dirección global</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>Detección exitosa de dirección duplicada. La dirección global es válida.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>Falló la detección de dirección duplicada. La dirección global es inválida.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>Comenzó detección de dirección duplicada en dirección de enlace local.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>Falló la configuración de dirección de enlace local (configuraciones de verificación).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>Esperando por detección de dirección duplicada en dirección de enlace local.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>Detección exitosa de dirección duplicada. La dirección de enlace local es válida.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>La detección de dirección duplicada falló. La dirección de enlace local es inválida.</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>Comenzó recuperación de IP sin registro de estados.</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>Falló en obtener IP sin registro de estados.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>Se obtuvo IP sin registro de estados con éxito.</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>Comenzó recuperación de IP con registro de estados.</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>No se encontraron enrutadores para proveer IP con registro de estados.</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>Reintentando recuperación de IP con registro de estados.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>Se obtuvo IP con registro de estados con éxito.</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>RED INALCANZABLE</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>HOST INALCANZABLE</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>PROTOCOLO INALCANZABLE</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>PUERTO INALCANZABLE</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>Mensaje muy largo</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>Red Destino Desconocido</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>Host Destino Desconocido</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>Red Destino Prohibida</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>Host Destino Prohibido</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>Red Inalcanzable para TOS</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>Host Inalcanzable para TOS</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Tiempo Excedido Durante Transito</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>Tiempo Excedido Durante Reensamble</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>Error ICMP desconocido</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>Destino inalcanzable</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>Dir no alcanzable</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>No hay ruta al destino</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>El destino no es un Neighbor</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>Comunicación con destino prohibida administrativamente</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>Paquete demasiado grande</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Excedido límite de saltos en tránsito</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>Excedido tiempo de reensamble de fragmentos</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>Encontrado campo de cabecera erróneo</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>Encontrado Tipo Siguiente Cabecera no reconocido</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>Encontrado opción IPv6 no reconocida</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>Inactivo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE Activo</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP Activo</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Red Activa</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>El Usuario Requiere Inact</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Parado capa datos</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE Espera Máxima</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP Espera Máxima</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP Auten. Fallo</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP Fallo Desconocido</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP Fallo</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config Inválida</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Error Interno</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MÁX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP Exitoso. MAC destino obtenida</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>Esperando servicio ARP...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>No ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abort</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continúe en semi dúplex</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>Detener J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>RFC2544 Test</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>Prueba J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>Estado Bucle Remoto</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>Estado Bucle Local</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>Estado PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>Estado IPv6</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>Estado ARP</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>Estado DHCP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>Estado ICMP</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>Estado de descubrimiento vecino</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>Estado de autoconfiguración</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>Estado de la dirección</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>Unidad de descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>Buscar unidades</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Buscando</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>Usar unidad seleccionada</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>Saliendo</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Cerrar</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>Conectando&#xA;a equipo remoto</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>Conectar&#xA;a remoto</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Desconectar</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>Conectado&#xA; al Remoto</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Conectando</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Off</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simétrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asimétrico</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>Unidireccional</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Bucle de retorno</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>Flujo descendente y ascendente</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>Bidireccional</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>Los rendimientos de los flujos descendente y ascendente son iguales.</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>Los rendimientos de los flujos descendente y ascendente son diferentes.</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>Probar la red solamente en una dirección.</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>Mediciones:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>El tráfico se  genera localmente y las mediciones se toman localmente.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>El tráfico se genera local y remotamente y las mediciones se toman en cada dirección.</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>Dirección de medida:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>El tráfico se  genera localmente y se mide con el instrumento de prueba remoto.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>El tráfico se genera remotamente y se mide con el instrumento de prueba local.</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>Retardo de ida y vuelta</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Retardo de ida</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Medición de retardo de ida y vuelta solo.&#xA;La unidad remota no es capaz de un retardo unidireccional</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Solo mediciones de retardo unidireccional.&#xA;La unidad remota no es capaz de RTD bidireccional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>No se pueden realizar mediciones de retardo.&#xA;La unidad remota no es capaz de RTD bidireccional o retardo unidireccional</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>No se pueden realizar mediciones de retardo.&#xA;La unidad remota no es capaz de retardo unidireccional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>No se pueden realizar mediciones de retardo.&#xA;La unidad remota no es capaz de RTD bidireccional</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Solo mediciones de retardo unidireccional.&#xA;RTD no es compatible cuando de prueba de forma unidireccional.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>No se pueden realizar mediciones de retardo.&#xA;RTD no es compatible cuando se realiza la prueba de forma unidireccional</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>No se pueden realizar mediciones de retardo.&#xA;La unidad remota no es capaz de retardo unidireccional.&#xA;RTD no es compatible cuando se realiza la prueba de forma unidireccional</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>No se pueden realizar mediciones de retardo.</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>Tipo de medición de latencia</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>Necesita un instrumento de prueba remoto Viavi.</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>Versión 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>Versión 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Calibración de RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>Selección de óptica</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>Ajustes IP para canal de comunicaciones del extremo lejano</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>No hay configuración de dirección IP local, requerida para la prueba de Bucle de retorno.</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Estática</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>Estático - Por servicio</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>Estático - Sencillo</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manual</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>Con estado</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Sin estado</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Fuente</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>Dirección enlace local de origen</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>Dirección global de origen</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>Obtenido auto</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>Definida por usuario</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>Modo ARP</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>Tipo dirección fuente</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>Modo PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>Avanzado</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Activado</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Deshabilitado</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>MAC Fuente Cliente</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>Valores de Fábrica</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>MAC Fuente por defecto</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>MAC cliente por defecto</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>MAC Fuente Usuario</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>MAC usu. Cliente</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>Omitir conexión</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>VLAN apilada</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>¿Cuántas VLAN se usan en el puerto de red local?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>No hay VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-en-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLANs (VLAN apilada)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>Introduzca la configuración del ID de VLAN:</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>Profundidad de pila</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>Usuario</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>Configuración del servidor de descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>NOTA: No se puede utilizar un IP de destino de enlace local para 'Conectar a remoto'</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Destino IP</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>IP de destino global</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Pinging</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>Ayúdame a encontrar la &#xA;IP de destino</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Puerto local</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Estado desconocido</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>ARRIBA (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>ARRIBA (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>ARRIBA (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>ARRIBA (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>ARRIBA (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>ARRIBA (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>ARRIBA (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>ARRIBA (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>ARRIBA (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Enlace perdido</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>Puerto local:</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Autonegociación</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analizando</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Autonegociación:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>Esperando para conectar </translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Conectando...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>Reintentando... </translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Conectado</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>Falla en la conexión</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>Conexión abortada</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>Canal de comunicaciones:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>Configuración avanzada del servidor de descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>Conseguir IP de destino desde un servidor de descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Servidor IP</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>Puerto del servidor</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>Frase de contraseña del servidor</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>Tiempo de uso solicitado (min.)</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>Tiempo de concesión garantizado (min.)</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>Configuración de red C2 - Local</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>Configuraciones de la unidad local</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>Tráfico</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>Tráfico LBM</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 (Mínimo)</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 (Máximo)</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>Usuario SVLAN TPID (hex) </translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>Configurar Tipo de bucle, Tipo de Ethernet y Direcciones MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>Configurar Direcciones MAC, Tipo de Ethernet y LBM</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>Fijar direcciones MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>Fijar las direcciones MAC y el modo ARP</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Tipo Destino</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>Unicast</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>Multicast</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>Transmisión</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>Prioridad Usuario VLAN</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>El equipo remoto no está conectado.</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>Configuración de red C2 - Remota</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>Configuraciones de equipo remoto</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>Configuración de red C3  Local</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>Establecer clase de tráfico, etiqueta de flujo y límite de salto</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>¿Qué tipo de priorización de IP se usa en su red?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>Configurar Tiempo a directo, Incremento de ID de IP y Modo PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>Fijar tiempo para directo y modo PPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>Configurar Tiempo a directo e Incremento de ID de IP</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>Fijar tiempo para directo</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>¿Qué priorización de IP se usa en su red?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>Configuración de red C3  Remota</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>Configuración de red C4  Local</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>Tipo servicio fuente</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>Cliente-DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>Servidor-DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>Finger</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Hora</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>Tipo servicio de destino</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>Fijar IP de Escucha ATP</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>Configuración de red C4  Remota</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>Fijar tipo de bucle y secuencia, respondedor e ID de originador</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>Configuración C2 avanzada - local</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Tipo Fuente</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC por defecto</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC Usuario</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>Configuración LBM</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>Tipo LBM</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>Activar emisor TLV</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>On</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>Configuración C2 avanzada - remota</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>Configuración C3 avanzada  Local</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Tiempo de vida (saltos)</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>Configuración C3 avanzada  Remota</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>Configuración C4 avanzada  Local</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>Configuración C4 avanzada  Remota</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>Configuraciones avanzadas de conexión - Local</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>Plantillas</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>¿Desea usar una plantilla de configuración?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Tasa de acceso al paquete Viavi mejorada > Tasa de transporte</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - Mayor esfuerzo</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - Continental</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - Global</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - Metro</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - Retroceso móvil H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - Regional</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - Emulación de datos VoIP</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>Transparencia de bit RFC2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>Velocidad de acceso al paquete RFC2544 > velocidad de transporte</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>Velocidad de acceso al paquete RFC2544 = velocidad de transporte</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>Aplicar plantilla</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>Configuraciones de plantillas</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>Pérdida de marco de rendimiento (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>Umbral de latencia (us):#1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>Packet Jitter Threshold (us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>Tamaños del marco: por defecto</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>Anchura de banda máxima (subida):#1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>Anchura de banda máxima (bajada):#1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>Anchura de banda máxima:#1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>Pruebas: pruebas de latencia y rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>Pruebas: rendimiento, latencia,, pérdida de marcos y opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>Duración y ensayos: #1 segundos con 1 ensayo</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>Algoritmo de rendimiento: Viavi mejorado</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>Algoritmo de rendimiento: RFC 2544 estándar</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>Umbral de rendimiento (Subida): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>Umbral de rendimiento (Bajada): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>Umbral Througput: #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>Algoritmo de pérdida de marcos: estándar RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>Tiempo de ráfaga opuesto a opuesto: 1 seg.</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>Granularidad opuesto a opuesto : 1 marco</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Pruebas</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>Fijar configuración avanzada de utilización</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>Máx. ancho de banda</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>Ancho de banda máximo de bajada (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>Ancho de banda máximo de bajada (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>Ancho de banda máximo de bajada (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>Ancho de banda máximo de bajada (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>Anchura de banda máxima de bajada (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>Ancho de banda máximo de subida (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>Ancho de banda máximo de subida (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>Ancho de banda máximo de subida (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>Ancho de banda máximo de subida (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>Anchura de banda máxima de subida (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>Máx. ancho de banda (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>Máx. ancho de banda (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>Máx. ancho de banda (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>Máx. ancho de banda (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Máx. ancho de banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>Marcos seleccionados</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>Tipo de Longitud</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trama</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Paquete</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>Inicializ.</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Borrar todo</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Selecc todo</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Longitud trama</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Longitud del Paquete</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>Subida longitud&#xA;de marco</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>El equipo remoto no está conectado</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>Bajando longitud&#xA;de marco</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>El equipo&#xA;remoto&#xA;no está&#xA;conectado.</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>Pruebas seleccionadas</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>Pruebas RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>latencia (requiere rendimiento)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>Buffer Credit (requiere Rendimiento)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>Rendimiento de Buffer Credit (requiere Buffer Credit)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>Recuperación del sistema (sólo bucle inverso y requiere rendimiento)</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>Pruebas adicionales</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>Fluctuación de paquetes requiere tasa de transferencia</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>Prueba de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>Carga extendida (solo bucle invertido)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>Configuración de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Proceso puesta a cero</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>Norma RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi Mejorado</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>Configure medidas de producción avanzada </translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>Precisión de medición de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Precisión de medición de bajada (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisión de medición ascendente (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>Precisión de medición de bajada (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>Precisión de medición ascendente (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>Precisión de medición de bajada (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>Dentro de 1,0%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>Dentro de 0,1%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>Dentro de 0,01%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>Dentro de 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>Dentro de 400 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>Dentro de 40 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>Dentro de 4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>Dentro de .4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>Dentro de 1000 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>Dentro de 100 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>Dentro de 10 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>Dentro de 1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>Dentro de .1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>Dentro de .01 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>Dentro de .001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>Dentro de 0,0001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>Dentro de 92.942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>Dentro de 9,2942 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>Dentro de .9294 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>Dentro de .0929 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>Dentro de 10000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>Dentro de 1000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>Dentro de 100 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>Dentro de 10 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>Dentro de 1 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>Dentro de .1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>Precisión de medición de subida</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Precisión de medición de subida (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisión de medición de subida (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>Precisión de medición de subida (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>Precisión de medición de subida (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>Precisión de medición de subida (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Exactitud de la Medida</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>Precisión de medición (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisión de medición (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>Precisión de la medición (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>Precisión de medición (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>Precisión de medición (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>Más información</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>Solución de problemas</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>Puesta en servicio</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>Opciones de Rendimiento Avanzado</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>Configure la duración de la prueba Latency / Packet Jitter por separado.</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>Configure la duración de la prueba Latency por separado.</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>Configure la duración de la prueba Packet Jitter por separado.</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>Configuración de pérdida de marcos</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Test Procedure</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Top Down</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Bottom Up</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>Número de pasos</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 Mbps por paso</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1 kbps por paso</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % de velocidad de línea por paso</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>Ámbito del test de bajada (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>Ámbito del test de bajada (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>Ámbito del test de bajada (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>Ámbito del test de bajada (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>Gama de prueba de bajada (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Mín.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Máx.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularidad de Ancho de banda de bajada (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularidad de Ancho de banda de bajada (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularidad de Ancho de banda de bajada (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularidad de Ancho de banda de bajada (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>Granularidad de banda ancha de bajada (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>Ámbito del test de subida (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>Ámbito del test de subida (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>Ámbito del test de subida (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>Ámbito del test de subida (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>Gama de prueba de subida (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularidad de Ancho de banda de subida (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularidad de Ancho de banda de subida (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularidad de Ancho de banda de subida (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularidad de Ancho de banda de subida (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>Granularidad de banda ancha de subida (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>Rango de prueba (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>Rango de prueba (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>Rango de la prueba (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>Rango de la prueba (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Rango de la prueba (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularidad de Ancho de banda (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularidad de Ancho de banda (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularidad del ancho de banda (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularidad del ancho de banda (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularidad del ancho de banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>Fijar configuración avanzada de mediciones de pérdida de marcos</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>Configuración de pérdida de marco avanzada</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>Mediciones de prueba óptica</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>Medir latencia</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>Medir vibración del paquete</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>Configuración opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>Duración de ráfaga máxima de bajada (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>Duración de ráfaga máxima de subida (s)</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>Duración de ráfaga máxima</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>Granularidad Ráfaga (tramas)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>Fijar configuración avanzada opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>Política de tramas Pause</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>Configuración de créditos del buffer</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Flow Control Login Type</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>Implícito (enlace transparente)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explícito (Puerto- E)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Max Buffer Size</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>Pasos de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>Controles de prueba</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>¿Configurar duraciones de la prueba por separado?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>Duración</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Número de Pruebas</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Jitter de paquete</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>Latency / Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Ráfaga (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Recup. sistema</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Todas las pruebas</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>Mostrar&#xA; Aprobador/Suspenso</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>Umbral&#xA;de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>Umbral&#xA;de bajada</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Umbral</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>Rendimiento Umbral (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>Rendimiento Umbral (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>Umbral de rendimiento (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>Umbral de rendimiento (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Umbral de caudal (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>El equipo remoto&#xA;no está conectado.</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>latencia RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>latencia OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>Paquete Jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>Tamaño de extensión de ráfaga (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>Ráfaga (Control CBS)</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>Alta precisión - bajo retardo</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>Baja precisión - alto retardo</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>Correr pruebas FC</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>Omitir pruebas FC</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>on</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>off</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>Buffer Credit&#xA;Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Gráfico de prueba de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Gráfico de prueba de rendimiento de subida</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Resultados Medida Throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Resultados de la prueba de rendimiento de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Gráfico de prueba de rendimiento de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Resultados de la prueba de rendimiento de bajada</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>Anomalías de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>Anomalías de rendimiento de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>Anomalías de rendimiento de bajada</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>Resultados de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>Rendimiento (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>Rendimiento (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Trama 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>C1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>C2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>C3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>C4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Trama 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Trama 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Trama 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Trama 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Trama 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Trama 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Trama 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Trama 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Trama 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>Passa/Fallo</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>Longitud Trama&#xA;(Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>Longitud Paquetes&#xA;(bytes)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>Measured&#xA;Rate(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>Measured&#xA;Rate(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>Velocidad&#xA;medida (frms/seg)</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>Detectar&#xA;R_RDY</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Tasa Conf&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>Velocidad C1&#xA; medida (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>Medición de velocidad &#xA;C1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>Medición C1&#xA;(% vel. de línea)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>Medición de velocidad &#xA;C2 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>Medición de velocidad &#xA;C2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>Medición C2&#xA;(% vel. de línea)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>Medición de velocidad &#xA;C3 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>Medición de velocidad &#xA;C3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>Medición C3&#xA;(% vel. de línea)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>Medición de velocidad &#xA;C4 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>Medición de velocidad &#xA;C4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>Medición C4&#xA;(% vel. de línea)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>Velocidad medida&#xA; (frms/seg)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>Velocidad medida&#xA; (pkts/seg)</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>Pausa&#xA;detectada</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Velocidad Cfg &#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Velocidad Cfg &#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Tasa de configuración&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Velocidad Cfg &#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalias</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>Marcos OoS&#xA;detectados</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Carga Acterna&#xA;Error detectado</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>Error FCS&#xA; detectado</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>Suma de comprobación de IP&#xA;Error detectado</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>Suma de comprobación de TCP/UDP&#xA;Error detectado</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>Prueba de latencia</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Gráfico de prueba de latencia</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Gráfico de prueba de latencia de subida</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Resultados de la prueba de latencia</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Resultados de prueba de latencia de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Gráfico de prueba de latencia de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Resultados de prueba de latencia de bajada</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>Latencia&#xA;RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>Latencia&#xA;OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>Velocidad de línes%&#xA;medida</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>Pausa &#xA;Detectar</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>Resultados Medida Pérdidas de Tramas</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>Resultados de la prueba de pérdida de marco de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>Resultados de la prueba de pérdida de marco de bajada</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>Resultados de pérdida de marcos</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Trama 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>Velocidad configurada (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>Velocidad configurada (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>Tasa configurada (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>Velocidad configurada (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>Tasa configurada (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Pérdidas de Tramas (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>Velocidad de transferencia &#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>Velocidad de transferencia &#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>Velocidad de rendimiento&#xA;(C1 kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>Velocidad de transferencia &#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>Velocidad de rendimiento&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>Frame Loss Rate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>Marcos perdidos</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>Vibración de paquete&#xA;máxima media (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>Detectar error&#xA;</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>Detectar&#xA;OoS</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Tasa Conf&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Resultados Test Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Resultados de la prueba de subida opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Resultados de la prueba de bajada opuesto a opuesto</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>Marcos de&#xA;ráfaga medios</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>Segundos de&#xA;ráfaga medios</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>Resultados medida Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Atención</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>Tamaño del buffer mínimo&#xA; (Créditos)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>Resultados medida Throughput Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>Resultados de rendimiento de créditos del buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>Rendimiento (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>Cred 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>Cred 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>Cred 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>Cred 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>Cred 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>Cred 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>Cred 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>Cred 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>Cred 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>Cred 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>Tamaño del buffer&#xA; (créditos)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>Measured Rate&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>Measured Rate&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>Velocidad medida&#xA; (marcos/seg)</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>Prueba de transparencia J-Proof - Ethernet C2</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>Tramas J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>Ejecutar prueba</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>Correr Prueba J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Resumen</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>Extremo: Resultados detallados</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>Salir de prueba J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:.</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Aplicando</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>Configurar tipos de marco para probar la Transparencia del Servicio en la Capa 2</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nombre</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   Protocolo   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>Protocolo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> Tipo de Trama </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> Encapsulación </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Apilado</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>Tamaño&#xA;Trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Tamaño Trama</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Número</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>Tasa&#xA;(tr/s)</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Tasa</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>Timeout&#xA;(ms)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>Tiempo de espera</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Control</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>Bit&#xA;DEI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>Usuario SVLAN TPID</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation> Encap. </translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>Encap.</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>Tipo de&#xA;Trama</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit Inc</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>Config&#xA;Rápida</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>Añadir&#xA;tramas</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>Retirar&#xA;tramas</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>La dirección fuente es común para todos los marcos; esto se configura en la Página de Ajustes Locales.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Incremento PBit</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>Pila VLAN</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>Incremento SPbit</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Long.</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Datos</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>Configuración J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Profundidad</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rápido (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>Full (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Grupo</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Todos</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>Spanning Tree</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>Láser&#xA;On</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>Láser&#xA;Off</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>Iniciar secuencia&#xA;Tramas</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>Parar secuencia&#xA;Tramas</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>J-Proof Test</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>PARADO</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>EN CURSO</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>Errores de carga</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>Errores de cabecera</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>Desajuste cuentas</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Parado</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>Resumen de resultados</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Idle</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>En curso</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>Desajuste de carga</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>Desajuste de encabezamiento de trama</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>Resultados J-Proof</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>Reset&#xA;Rx Óptico</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>Comprobación rápida</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>Iniciar el test de comprobación rápida</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>Configuración de comprobación rápida</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>Resultados de carga prolongada de comprobación rápida</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Detalles</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Sencillo</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>Por Flujo</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>FROM_TEST</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>Test Throughput:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>Resultados de QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Resultados de prueba de carga extendida</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Conteo de tramas Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Conteo de tramas Rx</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>Conteo de tramas con error</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>Conteo de tramas OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>Conteo de marcos perdidos</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Ratio Pérdidas de Trama</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanente</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Activo</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Rendimiento: %1 Mbps (L1), Duración: %2 segundos, Tamaño de trama: %3 bytes</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Producción: %1 kbps (L1), Duración: %2 Segundos, Tamaño: %3 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>¿No es lo que quería?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Fin</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Inicio</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>¡Con éxito!</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>Buscando destino</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>Estado ARP:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Bucle remoto</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Revisando bucle activo</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Revisando bucle físico</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Revisando un bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Revisión de un bucle LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Activar bucle</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Bucle permanente</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR Loop</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Bucle fallido</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>Bucle remoto:</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>Rendimiento medido (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>No determinado</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>No Disponible</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>Producción medida (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>Ver errores</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>Carga (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>Cargar (L1 Kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>Duración de la prueba de rendimiento (segundos)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>Tamaño de la trama (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Tasa Bit</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>Duración de la prueba de calidad</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 segundos</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 segundos</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 minuto</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 minutos</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 minutos</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 minutos</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 hora</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 horas</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 horas</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 horas</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>Definida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>Duración de la prueba (seg.)</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>Duración de la prueba de calidad (minutos)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Longitud Trama (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>Longitud usuario</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>Longitud Jumbo</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>Conector eléctrico:10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>Long. Onda Láser</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>Conector eléctrico</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>Conector óptico</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>Detalles...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Enlace Activo</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Resultados de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Total errores</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Tramas con Error</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Tramas OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Tramas Perdidas</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>Detalles de la interfaze</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>Detalles de los SFP/XFP</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>Longitud de onda (nm): SFP sintonizable, refiérase a la configuración de sintonía  Canal/Longitud de onda.</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>Largo del cable (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>Sintonía soportada</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>Longitud de onda; Canal</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Longitud de onda</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Canal</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>Velocidades recomendadas&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>Velocidad nominal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>Velocidad min (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>Velocidad máx (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fabricante</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>Fabricante PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Fabricante Rev</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Tipo de nivel potencia</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Monitorización diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Byte diagnóstico</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>Configure JMEP</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>Longitud de onda (nm): Dispositivo sintonizable, consulte la configuración de sintonización de Canal / Longitud de onda.</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>Detalles de los CFP/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>Detalles de los CFP2/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>Detalles de CFP4/QSFP</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN fabricante</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Código de fecha</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Código de lote</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>No. versión hardware/software</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>No. rev. Especificación hardware MSA</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>No. rev. admin. I/F</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Clase de potencia</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Tipo de nivel de potencia Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Potencia Lambda máxima Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Potencia Lambda máxima Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation># de capas activas</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Anchos de banda por fibra</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>Byte de diagnóstico.</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>Ancho de banda por rango de fibras (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Tasa máxima bit de carril de red (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>Módulo ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Tasas soportadas</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Ancho de banda nominal (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Tasa de bit nominal (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** Uso recomendado para aplicaciones RS-FEC de 100GigE ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>Experto CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>Experto CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>Experto de CFP4</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Activar bucle invertido Viavi</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>Activar bucle de CFP Viavi</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>Activar Modo experto de CFP</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>Activar Modo experto de CFP2</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>Activar modo Experto de CFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>Transmisión de CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>Pre-énfasis</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>Pre-énfasis de transmisión de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predeter.</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>Baja</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>Nominal</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>Terminación</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>Divisor de reloj</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>Divisor de reloj de transmisión de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>Offset de desviación (bytes)</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>Compensar sesgo de transmisión de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>Invertir polaridad</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>Invertir polaridad de transmisión de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Restablecer Tx FIFO</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>Recepción de CFP4</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>Ecualización</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>Ecualización de recepción de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>Invertir polaridad de recepción de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>Ignorar LOS</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>Ignorar LOS de recepción de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Restablecer Rx FIFO</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>Experto QSFP</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>Activar Modo experto de QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>Ignorar LOS de recepción de QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>Derivación de CDR</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>Recibir</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>Derivación de recepción de CDR de QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>Transmitir</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>Derivación de transmisión de CDR de QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>El control de derivación de transmisión y recepción de CDR estaría disponible si fuese compatible con el módulo QSFP.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>Ingenería</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>Bucle de núcleo de XCVR</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>Bucle de núcleo de XCVR de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>Bucle de línea de XCVR</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>Bucle de línea de XCVR de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>Bucle de host del módulo</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>Bucle de host del módulo CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>Bucle de red del módulo</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>Bucle de red del módulo CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>Bucle del sistema de caja de engranajes de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>Bucle de línea de caja de engranajes de CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>ID de chip de caja de engranajes</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Rev. de chip de caja de engranajes</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Ver. de Ucode de caja de engranajes</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>CRC de Ucode de caja de engranajes</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Vías de sistema de caja de engranajes</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Vías de línea de caja de engranajes</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>Vías del host de CFPn</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>Vías de la red de CFPn</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>Bucle de núcleo de XCVR de QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>Bucle de línea de XCVR de QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>TipoDisp examinador de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>DirFís examinador de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>DirecReg examinador de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>TipoDisp inserción de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>DirFís inserción de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>DirReg inserción de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>Valor inserción de MDIO</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>Registro de controles A013 por carril habilitado / deshabilitado.</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>SelecPieza examinador de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>DirecDisp examinador de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>DirecReg examinador de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>SelecPieza inserción de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>DirecDisp inserción de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>DirReg inserción de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>Valor inserción de I2C</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>Activar/desactivar láser de registro de controles 0x56 por carril.</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>Detalles SFP+</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Señal</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Señal Reloj Tx</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>Fuente de reloj</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>Interno</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>Recuperado de Rx</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>Externo</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>1.5M Externo</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>2M Externo</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>10M Externo</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>Recuperado Remoto</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Módulo de sincronización</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>VC-12 de origen</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>Interno - Desvio Frec. (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>Reloj Remoto</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>Dispositivo sintonizable</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>Modo de sintonía</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>Frecuencia</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>Frecuencia (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>Primera frecuencia sintonizable (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>Última frecuencia sintonizable (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>Espaciado de la cuadrícula (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>Alarma de sesgado</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>Umbral de alarma de sesgado (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>Resincronización necesaria</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>Resincronización completa</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>Calibración de RS-FEC aprobada</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>Error al calibrar RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>Normalmente, la aplicación RS-FEC se utiliza con dispositivos SR4, PSM4 y CWDM4.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Para realizar una calibración de RS-FEC, efectúe el siguiente procedimiento (también aplicable a CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Inserte un adaptador QSFP28 en el CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Inserte un transceptor QSFP28 en el adaptador</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Inserte un dispositivo de bucle de fibra en el transceptor</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Haga clic en Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Calibrar</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Calibrando...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Resincronizar</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Ahora debe resincronizar el transceptor al dispositivo en pruebas (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Desconecte el dispositivo de bucle de fibra del transceptor</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Establecer una conexión al dispositivo en pruebas (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Encienda el láser del DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Verifique que el LED de Señal presente del CSAM esté en verde</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Haga clic en Resincronizar carril</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>Resinc.&#xA;carril</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>Resultados J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>Presione el botón "Inicio" para comenzar la prueba J-QuickCheck.</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>Continuando en el modo semi dúplex.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>Revisión conectividad de tráfico en la dirección Upstream.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>Revisión de la conectividad de tráfico en la dirección Dowstream.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>La conectividad de tráfico para todos los servicios se verificó con éxito.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>La conectividad de tráfico no se pudo verificar con éxito para el(los) servicio(s) en la dirección Upstream: #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>La conectividad de tráfico no se pudo verificar con éxito para el(los) servicio(s) en la dirección Downstream: #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>La conectividad de tráfico no se pudo verificar con éxito para el(los) servicio(s) en la dirección aguas arriba: #1 y en la dirección aguas abajo para el(los) servicio(s): #2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>La conectividad de tráfico no se pudo verificar con éxito en la dirección aguas arriba.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>La conectividad de tráfico no se pudo verificar con éxito en la dirección aguas abajo.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>La conectividad de tráfico no se pudo verificar con éxito en la dirección aguas arriba o aguas abajo.</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>Revisión de conectividad</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>Conectividad de tráfico:</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>Iniciar QC</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>Stop QC</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Pruebas automáticas de óptica</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>Fin: Guardar perfiles</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Informes</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>Óptico</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>Salir del autodiagnóstico de óptica</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>Se ha perdido la señal óptica. Se ha abortado la prueba.</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>Se ha detectado un nivel bajo de energía. Se ha abortado la prueba.</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>Alto nivel de energía detectado. Se ha abortado la prueba</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>Se ha detectado un error. La comprobación ha sido cancelada.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>La prueba ha FALLADO porque se ha encontrado un sesgado excesivo.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>La prueba ha FALLADO porque se ha encontrado un sesgado excesivo.La prueba se ha abortado.</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>Se ha detectado un error de bits. Se ha abortado la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>La comprobación ha fallado porque el BER ha excedido el umbral configurado.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>La prueba ha FALLADO debido a la pérdida de sincronización de tramas.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>Imposible leer QSFP+ insertada.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>Imposible leer CFP insertada.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>Imposible leer CFP2 insertada.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>No ha sido posible leer el CFP4 insertado.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>Se ha detectado un error de bits corregible de RS-FEC.  La comprobación ha sido cancelada.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>Se ha detectado un error de bits incorregible de RS-FEC.  La comprobación ha sido cancelada.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>Error al calibrar RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>Error de desviación de frecuencia de entrada detectado</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>Error de desviación de frecuencia de salida detectado</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Se perdió la sincronización</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>Violaciones de código detectadas</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>Alineación de bloqueo de marcador perdida</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>Marcadores de alineación no válidos detectados</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>Errores BIP 8 AM bit detectados</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>Errores de bloqueo BIP detectados</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>Sesgado detectado</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>Bloquear errores detectados</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>Marcos menores detectados</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>Fallo remoto detectado</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>Detectados #1 errores de bitios</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>Sincronización de modelos perdida</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>No se puede medir de forma precisa el umbral VER debido a la pérdida de sincronización de modelos</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>El BER medido ha superado el umbral BER elegido</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS detectado</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>Errores FAS detectados</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>Fuera de marco detectado</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>Pérdida de marco detectada</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>Sincronización de marcos perdida</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>Marcador de fuera de carril lógico detectado</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>Errores de marcador de canal lógico detectados</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>Pérdida de alineación de canal detectada</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>Alineación de carril perdida</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>Errores MFAS detectados</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>Alineación fuera de carril detectada</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS detectado</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>Fuera de recuperación detectado</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>Sesgado excesivo detectado</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>Calibración de RS-FEC correcta</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Error</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>Prueba óptica</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>Tipo de test</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>La prueba utiliza RS-FEC de 100 GE</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>Tipo de Ópticos</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>Veredicto general de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>Prueba de presencia de señal</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>Prueba de nivel de señal óptica</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>Prueba de sesgado excesivo</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>Prueba de errores bit</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>Prueba de errores generales</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>Prueba de umbral BER</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>Potencia óptica</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Nivel Lambda #1 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Nivel Lambda #2 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Nivel Lambda #3 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Nivel Lambda #4 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Nivel Lambda #5 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Nivel Lambda #6 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Nivel Lambda #7 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Nivel Lambda #8 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Nivel Lambda #9 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Nivel Lambda #10 Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Suma de nivel de recepción (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Nivel Lambda #1 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Nivel Lambda #2 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Nivel Lambda #3 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Nivel Lambda #4 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Nivel Lambda #5 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Nivel Lambda #6 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Nivel Lambda #7 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Nivel Lambda #8 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Nivel Lambda #9 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Nivel Lambda #10 Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Suma de nivel Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>Config.</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>Conecte un cable prolongador corto y limpio entre los terminales Tx y Rx del conector que desee someter a prueba.</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>Prueba&#xA;óptica CFP</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>Prueba CFP2&#xA;Óptica/Ranura</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Abortar Test</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>Probar&#xA;óptica QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>Prueba de óptica de QSFP28</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Config.:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>Recomendado</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 minutos</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 minutos</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 horas</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 horas</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>Duración del usuario (minutos)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>Duración recomendada (minutos)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>El tiempo recomendado para esta configuración es &lt;b>%1&lt;/b> minuto(s).</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>Tipo de umbral de BER</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Posterior a FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Anterior a FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>Umbral BER</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>Activar desplazamiento de línea PPM</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>Desplazamiento máx. PPM (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>Detener en error</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>Información general de resultados</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>Tipo de óptica/ranura</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>Contrapartida PPM actual</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>Actual BER</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>Potencia óptica (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Lambda #1 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Nivel RX Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Lambda #3 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Lambda #4 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Lambda #5 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Lambda #6 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Lambda #7 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Lambda #8 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Lambda #9 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Lambda #10 de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Suma de nivel de recepción</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Nivel Lambda #1 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Nivel Lambda #2 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Nivel Lambda #3 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Nivel Lambda #4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Nivel Lambda #5 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Nivel Lambda #6 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Nivel Lambda #7 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Nivel Lambda #8 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Nivel Lambda #9 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Nivel Lambda #10 Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Suma de nivel Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>Detalles de la interfaz de CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>Detalles de la interfaz de CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>Detalles de la interfaz CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>Detalles de la interfaz de QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>Detalles de interfaz QSFP+</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>Detalles de interfaz QSFP28</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>No hay QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>No se pudo leer QSFP - Por favor, insértelo de nuevo</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>QSFP Error del checksum</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>Imposible interrogar los registros QSFP requeridos</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>No se puede confirmar la identidad QSFP.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>Prueba de duración y carga de la BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>Transparencia de GCC</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>Seleccionar y ejecutar pruebas</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>Configuración avanzada</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>Ejecutar Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>Carga BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>Salir de Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>Frequencia de Medición</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Estado Autonegociación</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>Configuración RTD</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>Todas las pistas</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>Comprobación de OTN:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** Iniciando Comprobación de OTN ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>Se ha detectado errores de bitio de carga</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>Detectado mas de 100,000 errores de bitios de carga</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>Se ha excedido el umbral de BER de carga</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>Detectados #1 errores de bitios de carga</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>Tasa de errores de bitios de carga: #1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>Se ha excedido el umbral de RTD</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: Mín: #3, Máx: #4, Prom: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD no disponible</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>Ejecutando prueba de carga de BERT</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>Ejecutando prueba de RTD</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>Ejecutando prueba de transparencia de GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>Error detectados de bitios GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>Excedido el umbral de BER de GCC</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>Detectados #1 errores de bitios GCC</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>Detectados más de 100.000 errores de bitios GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>Frecuencia de error de bitios de GCC: #1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** Iniciando comprobación de bucle ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** Omitiendo comprobación de bucle ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** Comprobación de bucle finalizada ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>Bucle detectado</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>No se ha detectado bucle</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>El canal #1 no está disponible</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>El canal #1 está ahora disponible</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>Pérdida de sincronización de patrón</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>Pérdida de sincronización de patrón de GCC</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>Prueba cancelada: detectado error de bitios.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>Prueba fallida: BER ha excedido el umbral configurado.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>Prueba fallida: RTD ha excedido el umbral configurado.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>Prueba cancelada: No se ha detectado bucle.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>Prueba cancelada: No hay presente ninguna señal óptica.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>Prueba cancelada: Pérdida de trama.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>Prueba cancelada: Pérdida de sincronización de trama</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>Prueba cancelada: Pérdida de sincronización de patrón.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>Prueba cancelada: Pérdida de sincronización de patrón de GCC.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>Prueba cancelada: Pérdida de alineación de pista.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>Prueba cancelada: Pérdida de bloqueo de marcador.</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>Debe seleccionar al menos 1 canal</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>Bucle no comprobado</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>Bucle no detectado</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>Selección de prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>Duración planificada de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>Tiempo de ejecución de la prueba</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>La Comprobación de OTN requiere la ejecución de un bucle de tráfico; este bucle se requiere en el extremo alejado del circuito de OTN.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>Comprobación de OTN</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>Compensación de óptica, asignación de señal</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>Asignación de señal</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>Asignación de señal y selección de óptica</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>Cfg avanzado</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>Estructura de señal</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>Compensación de RTD de óptica de QSFP (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>Compensación de RTD de óptica de CFP (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>Compensación de RTD de óptica de CFP2 (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>Compensación de RTD de óptica de CFP4 (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>Configure la duración y la carga de la prueba de BERT</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>Cfg de duración y carga de BERT</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>Ajustes de BERT de carga</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>Nivel de confianza (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>Basada en la velocidad de línea, el umbral de BER y el nivel de confianza, el tiempo recomendado para la prueba es de &lt;b>%1&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Patrón</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>Patrón BERT</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>Umbral de error</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>Mostrar correcto/error</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>Ajustes de retardo de ida y vuelta</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>Cfg de RTD</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>Incluye</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>Umbral (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>Frecuencia de medición (s)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>Cfg de GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>Configuración de transparencia de GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>Canal GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>Umbral de BER de GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>El patrón de BERT 2^23-1 se utiliza en GCC.</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>Comprobación de bucle</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>Omitir comprobación de bucle</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>Pulse el botón "Iniciar" para iniciar la comprobación de bucle.</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>Omitir pruebas</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>Comprobando bucle</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>Bucle detectado</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>Omitir comprobación de bucle</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>Detección de bucle</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>Detección de bucle</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>Resultados de BERT de carga</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>BER medido</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>Conteo de errores de bitio</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>Veredicto</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>Configuración de BERT de carga</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>Resultados de retardo de ida y vuelta</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>Mín (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>Máx. (ms) </translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>Prom (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>Nota: la condición de error se produce cuando el RTD excede del umbral.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuración</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>Configuración retardo de trayecto</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>Resultados de transparencia de GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>Tasa de error de bit GCC BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>Errores de bit GCC BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>Configuración de transparencia de GCC</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>Análisis de protocolo</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Captura</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>Capturar y Analizar CDP</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>Capturar y Analizar</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>Seleccionar protocolo para analizar</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>Iniciar&#xA;Análisis</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>Anular&#xA;Análisis</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Análisis</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>PTP experto</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>Configurar configuraciones de prueba manualmente </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>Umbrales</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>Ejecutar comprobación rápida</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>Salir de prueba de PTP experto</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>El enlace ya no está activo.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>Se ha perdido la sincronización de origen de retardo unilateral</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>La sesión del esclavo de PTP se ha interrumpido imprevistamente.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>Sincronización de origen de hora no presente. Verifique que el origen de hora esté correctamente configurado y conectado.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>No ha sido posible establecer una sesión de esclavo de PTP.</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>No se detectó receptor GPS.</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>Tipo TOS</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Vencimiento del Anuncio Rx</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>Anunciar</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 por segundo</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 por segundo</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 por segundo</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 por segundo</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 por segundo</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 por segundo</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 por segundo</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 por segundo</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Sincr</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>Solicitud de retardo</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>Duración de concesión (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Habilitar</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Habilitar umbral máximo de error de tiempo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Error de tiempo máx. (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Umbral de error de tiempo máx. (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>Duración de la medida (minutos)</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>Comprobación rápida</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>IP principal</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>Dominio de PTP</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>Iniciar&#xA;comprobación</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>Iniciando&#xA;sesión</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>Sesión&#xA;establecida</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Umbral de error de tiempo máximo (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Error de tiempo máximo (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>Error de tiempo, actual (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>Error de tiempo, actual (us) vs tiempo</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Resultados de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>Duración (minutos)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Comprobación de PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>Comprobación de PTP</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>Fin: Comprobación de PTP</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>Iniciar otra prueba</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>Salir de Comprobación de PTP</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>RFC 2544 mejorado</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>Configuración inválida</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>Configuración IP avanzada - local</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>Configuración de latencia RTD avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>Configuración de prueba de ráfaga (CBS) avanzada</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Correr J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>Ajustes J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>Jitter</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carga extendida</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>Salir de la prueba RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>Configuración de red local</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>Configuración de red remota</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>Cargar estado de negociación automática</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Velocidad (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Dúplex</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Control de flujo</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Aceptar FDX</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Aceptar HDX</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Aceptar Pause</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>Estado de negociación automática remoto</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Half</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Full</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Ambos, Tx y Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx Sólo</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx Sólo</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>Velocidad de marco RTD</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 marco por segundo</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 marcos por segundo</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>Configuración de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>Tamaño de ráfaga comprometido</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>Política CBS (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>Extensión de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>CBS del flujo descendente (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>Tamaños de ráfaga de bajada (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimo</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>Maximo</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>CBS del flujo ascendente (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>Tamaños de ráfaga de subida (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>Tamaños de ráfaga (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>Fijar configuración avanzada de CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>Fijar configuración avanzada de políticas CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>Fijar configuración avanzada de extensiones de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>Tolerancia</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>Configuración de carga extendida</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>Escala de rendimiento (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 tiene la siguiente configuración inválida:</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>Se ha verificado con éxito la conectividad del tráfico. Poniendo en funcionamiento la prueba de carga.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>Pulse "Start" para poner en funcionamiento la velocidad de línea.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>Pulse "Start" para poner en funcionamiento el uso de la anchura de banda RFC 2544 configurada.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>NO se usará el rendimiento medido para las pruebas RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>SE usará el rendimiento medido para las pruebas RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>Cargar tamaño de carga de prueba: %1 bytes</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>Tamaño de paquete de prueba de carga: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>Tamaño de marco de prueba de carga de subida: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>Tamaño de marco de prueba de carga de bajada: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>Rendimiento medido:</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>Prueba usando la anchura de banda máxima RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>Use la medición de rendimiento medida como la anchura de banda máxima RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>Cargar tamaño de carga de prueba (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>Tamaño de paquete de prueba de carga (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>Tamaño de marco de prueba de carga de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>Tamaño de marco de prueba de carga de bajada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>Ejecutar pruebas RFC 2544 </translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>Saltar pruebas RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>Revisar bucle Hardware</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Packet Jitter Test</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Gráfico de prueba de vibración</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Gráfico de prueba de vibración de subida</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Resultados de la prueba de vibración</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Resultados de prueba de vibración de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Gráfico de prueba de vibración de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Resultados de prueba de vibración de bajada</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>Max Avg Jitter (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>Vibración máxima media</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>Vibración máx.&#xA;media (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Resultados de la prueba CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Resultados de la prueba CBS de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Resultados de la prueba CBS de bajada</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Resultados de la prueba de extensión de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Resultados de la prueba de extensión de ráfaga de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Resultados de la prueba de extensión de ráfaga de bajada</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Resultados de la prueba de política CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Resultados de la prueba de políticas CBS de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Resultados de la prueba de políticas CBS de bajada</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>Prueba de ráfaga (Control CBS)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Tamaño de ráfaga &#xA; Cfg (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Tamaño de ráfaga &#xA; Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>Tamaño de ráfaga&#xA;media Rx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>Tramas&#xA;enviadas</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>Tramas&#xA;Recibidas</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>Tramas&#xA;Perdidas</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>Tamaño de la ráfaga&#xA;(kB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>Latencia&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>Vibración&#xA; (us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>Tamaño de ráfaga&#xA;configurada (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Tamaño de control&#xA;de ráfaga Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>CBS estimado&#xA; (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Gráfico de prueba de la recuperación del sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Gráfico de prueba de recuperación del sistema de subida</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Resultados de la prueba de recuperación de sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Resultados de la prueba de recuperación del sistema de subida</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Gráfico de prueba de recuperación del sistema de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Resultados de la prueba de recuperación del sistema de bajada</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>Tiempo de recuperación (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>Velocidad de sobrecarga &#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>Velocidad de sobrecarga &#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>Velocidad de sobrecarga&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>Velocidad de sobrecarga &#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>Tasa de sobrecarga&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>Velocidad de recuperación &#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>Velocidad de recuperación &#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>Velocidad de recuperación&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>Velocidad de recuperación &#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>Tasa de recuperación&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>Tiempo de recuperación&#xA;medio (us)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modo</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>Controles</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>Controles de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>Conformación</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>Configuración del paso</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>Seleccionar pasos</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Ruta MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Pasear la ventana</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Throughput TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avanzado</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>Configuraciones de conexión</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>CTl TrueSpeed (Avanzado)</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Seleccione unidades Bc</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>Recorrer ventana</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>Salir de prueba TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>Host TCP no pudo establecer una conexión. La prueba actual fue anulada.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>Al seleccionar el modo Solución de Problemas, usted fue desconectado de la unidad remota.</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>Selección de prueba inválida: Se debe seleccionar al menos una prueba para ejecutar TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>La MTU medida es demasiado pequeña para continuar. Prueba anulada.</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>¡Su archivo fue transferido demasiado rápidamente! Por favor, seleccione un tamaño de archivo para el rendimiento TCP, de manera que la prueba se ejecute durante al menos 5 segundos. Se recomienda que la prueba se ejecute por al menos 10 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>Se ha alcanzado el número máximo de intentos de transmisión. Se ha cancelado la prueba.</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>Host TCP encontró un error. La prueba actual fue anulada.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:.</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>Iniciando prueba TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>Reducción a cero en el tamaño de MTU.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>Probando #1 byte MTU con #2 byte MSS.</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>Se determinó que la MTU de la ruta sea de #1 bytes. Esto iguala un MSS de #2 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>Realizando la prueba RTT. Esto tomará #1 segundos.</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>El tiempo de ida y vuelta (RTT) fue determinado como de #1ms.</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>Realizando la prueba Walk the Window del flujo ascendente. </translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>Realizando la prueba Walk the Window del flujo descendente.</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>Enviando #1 bytes al tráfico TCP.</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>Tráfico de salida local: no modelado</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>Tráfico de salida remoto: no modelado</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>Tráfico de salida local: modelado</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>Tráfico de salida remoto: modelado</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>Duración (segundos) #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>Conexiones: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>Tamaño de ventana (kB): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>Realizando la prueba de rendimiento TCP del flujo ascendente.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>Realizando la prueba de rendimiento TCP flujo descendente.</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>Realizando comprobación avanzada de TCP. Esto llevará #1 segundos.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>Tipo IP local</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>Dirección IP local</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>Pasarela predeterminada local</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>Máscara subred local</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>Encapsulación local</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Dirección IP remota</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>Seleccionar Modo</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>¿Qué tipo de prueba está ejecutando?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>Estoy &lt;b>instalando&lt;/b> o &lt;b>activando&lt;/b> un nuevo circuito.*</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>Estoy &lt;b>solucionando los problemas de &lt;/b> un circuito existente.</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>*Requiere un Instrumento de Prueba MTS/T-BERD</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>¿Cómo se configurará su rendimiento?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>Mis rendimientos de flujos descendente y ascendente son &lt;b>iguales&lt;/b>. </translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>Mis rendimientos de flujos descendente y ascendente son &lt;b>diferentes&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Configurar el ancho de banda de cuello de botella para coincidir con el CIR de SAMComplete cuando se cargue la configuración de&#xA;Truespeed.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Configurar el ancho de banda de cuello de botella para coincidir con el máximo ancho de banda de RFC 2544 cuando se cargue la configuración de&#xA;Truespeed.</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Servidor Iperf</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>Instrumento de prueba T-BERD/MTS </translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>Tipo IP</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>Configuración remota</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>Configuración del servidoro host TCP</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>Este paso configurará los ajustes globales de todos los pasos de TruSpeed siguientes. Esto incluye la CIR (tasa de información comprometida) y el % de paso de TCP, que es el porcentaje de CIR necesario para pasar la prueba de rendimiento.</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>Poner en funcionamiento de prueba navegar por la ventana</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>Tiempo total de la prueba (s)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>Encontrar automáticamente el tamaño de la MTU</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>Tamaño de de la MTU (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>Identificación VLAN local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>Prioridad</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>Prioridad local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>TOS Local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>DSCP Local</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>CIR del flujo descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>CIR del flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>Remoto&#xA;Host TCP </translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>Identificación VLAN remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>Prioridad remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>TOS remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>DSCP remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>Fijar configuración avanzada de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Seleccione unidad Bc</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>Modelaje de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Unidad Bc</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>KB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>Perfil de modelado</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>El tráfico de salida local y remoto está modelado</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>Solamente el tráfico de salida local está modelado</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>Solamente el tráfico de salida remoto está modelado</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>Ni el tráfico de salida local ni el remoto está modelado</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>Modelado de tráfico (solamente comprobaciones secuenciales de ventana y de rendimiento)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>Las comprobaciones se ejecutan no modeladas y, seguidamente, modeladas</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) local</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) local</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) local</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) local</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) local</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) remoto</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (remoto)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) remoto</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>¿Desea modelar el tráfico de TCP?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>Tráfico no modelado</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>Tráfico modelado</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>Ejecutar la comprobación no modelada</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>ENTONCES</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>Ejecutar la comprobación modelada</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>Ejecutar la comprobación modelada en local</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>Ejecutar con modelado en local y en remoto</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>Ejecutar sin ninguna modelado</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>Ejecutar con modelado en local, y sin modelado en remoto</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>Ejecutar con modelado en local</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>Ejecutar sin modelado en local, y con modelado en remoto</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>Modelar tráfico local</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>La unidad remota no está conectada</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>Modelar tráfico remoto</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>Mostrar opciones de modelado adicionales</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>Controles TrueSpeed (avanzado)</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>Conectar a puerto</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>% de paso de TCP</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>Límite superior MTU (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>Usar conexiones múltiples</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>Habilitar ventana de saturación</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>Acelerar ventana (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>Acelerar conexiones (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>Configuración del paso</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>Pasos de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>Debe tener seleccionado al menos un paso para ejecutar la prueba.</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>Esta etapa usa el procedimiento definido en RFC4821 para determinar automáticamente la Unidad de Transmisión Máxima de la ruta de red extremo a extremo. El instrumento de prueba del Cliente TCP intentará enviar segmentos TCP en varios tamaños de paquete y determinará la MTU sin necesidad de ICMP (como la requerida por la exploración tradicional de la MTU de la RTU).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>Esta etapa realizará una transferencia TCP de baja intensidad e informará de regreso el tiempo de ida y vuelta (RTT) de línea de base que se utilizará como base para los siguientes resultados de etapas de prueba. El RTT de línea de base es la latencia inherente de la red, excluyendo los retardos adicionales causados por congestión de la red.</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>Duración (segundos)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>Este paso ejecutará una "exploración de ventana" y entregará un informe de resultados de rendimiento TCP por hasta cuatro (4) combinaciones de tamaños de ventana TCP.Esta etapa también informa rendimientos TCP reales versus previstos para cada tamaño de ventana.</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>Tamaños de ventana</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>Tamaño de Ventana 1 (bytes)</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># Con.</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>Conexiones Window 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>Tamaño de Ventana 2 (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>Conexiones Window 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>Tamaño de Ventana 3 (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>Conexiones Window 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>Tamaño de Ventana 4 (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>Conexiones Window 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>Bytes de tamaño máx seg</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>Usa el MSS encontrado en el&#xA;paso de la ruta MTU</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>Tamaño máx. de segmento (bytes)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>Basándose en la anchura de banda de &lt;b>%1&lt;/b> Mbps y una RTT de &lt;b>%2&lt;/b> ms, la ventana TCP ideal sería &lt;b>%3&lt;/b> bytes. Con las &lt;b>%4&lt;/b> conexiones y un tamaño de ventana TCP de &lt;b>%5&lt;/b> bytes para cada conexión, el rendimiento aproximado de transferencia ideal sería &lt;b>%6&lt;/b> kbps y un archivo de &lt;b>%7&lt;/b> MB transferido a través de cada conexión debería tardar &lt;b>%8&lt;/b> segundos.</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>Basándose en la anchura de banda del enlace de &lt;b>%1&lt;/b> Mbps y una RTT de &lt;b>%2&lt;/b> ms, la ventana TCP ideal sería &lt;b>%3&lt;/b> bytes. &lt;font color="red"> Con las &lt;b>%4&lt;/b> conexiones y un &lt;b>%5&lt;/b> bytes de tamaño de ventana TCP para cada conexión, se supera la capacidad del enlace. Los resultados reales pueden ser peores que los resultados previstos debido a la pérdida de paquetes y al retardo adicional. Reduzca el número de conexiones y/o el tamaño de la ventana TCP para realizar una prueba dentro de la capacidad del enlace.&lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>Tamaño de Ventana (bytes)</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>Tamaño de archivo por conexión (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>Encontrar automáticamente el tamaño del archivo en una transmisión de 30 segundos</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>Número de conexiones</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>Usa el RTT encontrado en la etapa RTT (%1 ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>Usa el MSS encontrado en el paso de la MTU de la ruta (%1 bytes)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>Esta etapa realizará transferencias múltiples de conexión TCP para probar si el enlace comparte de manera justa el ancho de banda (modelado de tráfico) o injusta (políticas de tráfico).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>Esta etapa realizará transferencias múltiples de conexión TCP para probar si el enlace comparte de manera justa el ancho de banda (modelado de tráfico) o injusta (políticas de tráfico). Para que el tamaño de ventana y el número de conexiones se compute automáticamente, ejecute, por favor, la etapa RTT.</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Tamaño ventana (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>Calculado automáticamente cuando de realiza el paso &#xA;RTT</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 bytes</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>Correr pruebas TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>Omitir pruebas TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>Unidad de Transmisión Máxima (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>Tamaño máximo del segmento (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>Esta etapa determinó que la Unidad de Transmisión Máxima (MTU) es &lt;b>%1&lt;/b> bytes para este enlace (extremo a extremo). Este valor, menos el encabezado de las capas 3/4, se utilizará como el tamaño del segmento máximo de TCP (MSS) para las etapas siguientes. En este caso, el MSS is de &lt;b>%2&lt;/b> bytes.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tiempo (s)</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>Resultados del resumen RTT</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>Prom. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>Mín. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>Máx. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>El tiempo de ida y vuelta (RTT) fue medido en &lt;b>%1&lt;/b> ms. El RTT mínimo se usa como la mejor representación de latencia inherente de la red. Las subsiguientes etapas lo usan como base para predecir el comportamiento de la transferencia TCP.</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>El tiempo de ida y vuelta (RTT) se midió como %1 msec. Se utilizó el RTT promedio (base) puesto que representa más cercanamente la latencia inherente de la red. Los pasos siguientes lo usarán como base para el desempeño TCP predicho.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>Walk the Window de flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>Walk the Window del flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>Tamaño 1 de la ventana de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>Tamaño 1 de la ventana de bajada (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>Tamaño 2 de la ventana de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>Tamaño 2 de la ventana de bajada (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>Tamaño 3 de la ventana de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>Tamaño 3 de la ventana de bajada (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>Tamaño 4 de la ventana de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>Tamaño 4 de la ventana de bajada (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>Tamaño 5 de la ventana de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>Tamaño 5 de la ventana de bajada (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>Conexiones de Window 1 de flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>Conexiones de Window 1 flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>Conexiones de Window 2 de flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>Conexiones de Window 2 flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>Conexiones de Window 3 de flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>Conexiones de Window 3 flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>Conexiones de Window 4 de flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>Conexiones de Window 4 flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>Conexiones de Window 5 de flujo ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>Conexiones de Window 5 flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>Window 1 real de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>Ventana ascendente real no modelada 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>Window 1 previsto de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>Window 2 real de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>Window 2 previsto de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>Window 3 real de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>Window 3 previsto de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>Window 4 real de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>Window 4 previsto de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>Window 5 real de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>Ventana ascendente real no modelada 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Ventana ascendente real modelada 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>Window 5 previsto de flujo ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>Window 1 real del flujo descendente (Mbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Ventana descendente real modelada 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>Predicción de Window 1 del flujo descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>Ventana 2 del Flujo descendente Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Ventana descendente real modelada 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>Predicción de Window 2 del flujo descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>Ventana 3 del Flujo descendente Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Ventana descendente real modelada 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>Predicción de Window 3 del flujo descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>Ventana 4 del Flujo descendente Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Ventana descendente real modelada 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>Predicción de Window 4 del flujo descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>Ventana 5 del Flujo descendente Real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Ventana descendente real modelada 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>Predicción de Window 5 del flujo descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Tx Mbps, Prom.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>Ventana 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>Real</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>No modelado real</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>Modelado real</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>Ideal</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>Ventana 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>Ventana 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>Ventana 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>Ventana 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>Los resultados de la etapa "Walk the Window" de TCP muestran el rendimiento real versus el ideal para cada tamaño de ventana/conexión probada. La diferencia entre real e Ideal puede ser causada por pérdidas congestión. Si el real es mayor que el ideal, el RTT usado como línea de base es demasiado alto. La etapa de rendimiento TCP suministra un análisis más profundo de las transferencias TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>Rendimiento real vs. ideal del TCP del flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>Gráficos de rendimiento TCP del flujo ascendente </translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>Gráficos de retransmisión de rendimiento de TCP ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>Gráficos de RTT de rendimiento de TCP ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>Rendimiento real del TCP del flujo descendente vs. Ideal</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>Gráficos de rendimiento TCP del flujo descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>Gráficos de retransmisión de rendimiento de TCP descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>Gráficos de RTT de rendimiento de TCP descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>Tiempo de transmisión ideal de flujo ascendente (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>Tiempo de transmisión real del flujo ascendente (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>Tasa de transferencia C4 ascendente actual (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Rendimiento real no modelado de L4 ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>Rendimiento real modelado de L4 ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>Rendimiento ideal de L4 ascendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>Eficiencia TCP ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>Eficiencia de TCP no modelada ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>Eficiencia de TCP modelada ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>Demora de acumulador ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>Retardo de búfer no modelado ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>Retardo de búfer modelado ascendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>Tasa de transferencia C4 descendente actual (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Rendimiento no modelado real de L4 descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>Rendimiento real modelado de L4 descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>Rendimiento ideal de L4 descendente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>Eficiencia TCP descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>Eficiencia de TCP no modelada descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>Eficiencia de TCP modelada descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>Demora en acumulador descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>Retardo de búfer no modelado descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>Retardo de búfer modelado descendente (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>Resultados de rendimiento TCP</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>Real vs. Ideal</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>Los resultados del test de subida pueden indicar:</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>No más recomendaciones.</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>La prueba no ha tenido duración suficiente</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>El conformador / buffer de red necesita sintonizarse</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>El filtro ha dejado caer paquetes debido a ráfagas TCP.</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>El rendimiento era bueno pero se han detectado retransmisiones.</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>La red está congestionada o el tráfico se está conformando</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>Su CIR puede estar desconfigurado.</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>¡Su archivo se transifirió demasiado rápidamente!&#xA;Por favor, revise el tiempo de transferencia previsto para el archivo.</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>&lt;b>%1&lt;/b> conexión(es), cada uno con un tamaño de ventana de &lt;b>%2&lt;/b> bytes se usaron para transferir un archivo de  &lt;b>%3&lt;/b> MB a través de cada conexión (un total de &lt;b>%4&lt;/b> MB).</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b> bytes de ventana TCP transmitidos sobre &lt;b>%2&lt;/b> conexiones.</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>C4 real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>C4 Ideal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>Transferir mediciones</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>Eficiencia del TCP&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>Retardo del búfer&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>Los resultados del test de bajada pueden indicar:</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>Ascendente real e ideal</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>Eficiencia del TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>Retardo del Buffer (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>Los resultados de la comprobación no modelada pueden indicar:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>Descendente real e ideal</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>Gráficos de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Trama retransm.</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>Línea base RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>Use estos gráficos para correlacionar posibles asuntos de desempeño TCP debido a retransmisiones y/o efectos de red congestiva (RTT excediendo la línea de base).</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>Gráficos de retransmisión</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>Gráficos de RTT</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>Rendimiento ideal por conexión</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>Para un enlace cuyo tráfico está modelado, cada conexión debería recibir una porción aún relativamente pareja del ancho de banda. Para un enlace que tiene políticas de tráfico, cada conexión rebotará cuando ocurran retransmisiones debidas a este monitoreo. Para todas las conexiones &lt;b>%1&lt;/b>, cada una debería consumir alrededor de &lt;b>%2&lt;/b> Mbps de ancho de banda.</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Aleatorio</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Prueba TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>Configuraciones de pruebas</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>Conexión avanzada de servidor</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>Configuraciones avanzadas de prueba</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>Crear informe localmente</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identificación de la prueba</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>Final: Ver resultados detallados</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>Final: Crear informe localmente</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>Crear otro informe localmente</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>MSS Test</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>RTT Test</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>Prueba corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>Prueba corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>Se requiere la siguiente configuración.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>La siguiente configuración está fuera de rango. Ingrese un valor entre #1 y #2.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>La siguiente configuración tiene un valor ilegal. Haga una nueva selección.</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>No hay licencia de prueba cliente a servidor.</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>No hay licencia de prueba servidor a servidor.</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>Hay demasiadas pruebas activas (el máximo es #1).</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>El servidor local no está reservado.</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>El servidor remoto no está reservado.</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>La instancia de prueba ya existe.</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>Error de lectura de la base de datos de pruebas.</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>La prueba no se encontró en la base de datos de prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>Expiró la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>El tipo de prueba no está soportado.</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>El servidor de prueba no es una opción.</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>El servidor de prueba está reservado.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>Probar modo de solicitud defectuosa de servidor de prueba.</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>No se permiten pruebas en el servidor remoto.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>Solicitud HTTP falló en el servidor remoto.</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>El servidor remoto no tiene suficientes recursos disponibles.</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>La prueba del cliente no es una opción.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>El puerto de prueba no está soportado.</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>Se intenta probar demasiadas veces por hora.</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>Falló la construcción de la instancia de prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>Falló la generación del flujo de trabajo de la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>La solicitud HTTP del flujo de trabajo es errónea.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>Falló la solicitud HTTP del flujo de trabajo.</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>No están permitidas las pruebas remotas.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>Ocurrió un error en el gestor de recursos.</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>No se encontró la instancia de prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>El estado de prueba tiene un conflicto.</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>El estado de prueba es inválido.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>Falló la creación de la prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>Falló la actualización de prueba.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>La prueba se creó con éxito.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>La prueba se actualizó con éxito.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>Solicitud HTTP falló:  #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>Es posible que la versión de servidor VNF (#2) con la versión de instrumento (#1). </translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>Ingrese el nombre de usuario y la clave de autenticación para el servidor en #1.</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>Falló la inicialización de la prueba: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>Prueba abortada: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>Conexión con el servidor</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>No se tiene la información necesaria para conectarse al servidor.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>No está presente un enlace para realizar las comunicaciones de red.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>No se tiene una dirección IP de fuente válida para las comunicaciones de red.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>No se realizó ping a la dirección IP especificada.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>No se puede detectar la unidad en la dirección IP especificada.</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>No se ha identificado aun el servidor en la dirección IP especificada.</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>No se puede identificar el servidor en la dirección IP especificada.</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>Servidor identificado pero no autenticado.</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>Falló la autenticación del servidor.</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>Falló la autorización, se está intentando identificar.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>No está autorizado o identificado, intentando ping.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>Servidor autenticado y disponible para pruebas.</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>El servidor está conectado y la prueba se está ejecutando.</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>Identificando</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>Identificar</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>Versión proxy TCP</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>Versión del controlador de pruebas</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>Enlace activo:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>IP servidor:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>Estado del servidor:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>Configuraciones avanzadas</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>Configuraciones avanzadas de conexión</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>Clave de autenticación</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>Memorizar nombres de usuario y claves</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>Unidad local:</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>Servidor</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Duración de recorrido de ventana (sec)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>Duración automática</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>Configuraciones de pruebas (Avanzado)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>Parámetros avanzados de prueba</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>Puerto TCP</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>Puerto TCP automático</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Cantidad de Walks de ventana</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>Conteo de conexión</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>Conteo de conexión automática</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Ventana de saturación</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>Ventana de saturación de ejecución</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>Acelerar conexión (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>Información de informe del servidor</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nombre de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nombre Técnico</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>Nombre del Cliente*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Compañía</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>Correo electrónico*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Teléfono</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Comentarios</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>Mostrar ID de prueba</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>Saltar prueba TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>El servidor no está conectado.</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>Promedio (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>En espera que el recurso de prueba esté listo. Usted es el #%1 en la lista de espera.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Ventana</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>Ventana&#xA;Saturación</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>Tamaño de ventana (kB)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>Conexiones</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>Diagnóstico corriente arriba:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>Nada que informar</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>El rendimiento es demasiado bajo.</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>RTT inconsistente</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>La eficiencia TCP es baja</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>El retardo del buffer es alto</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>El rendimiento es menor que el 85% del CIR</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>MTU menor que 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>Diagnóstico corriente abajo:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Código de autenticación</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Fecha de creación de la autenticación</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>Fecha de vencimiento</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>Modificar tiempo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Hora de detención de la prueba</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>Modificado última vez</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Correo electrónico</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>resultados de rendimiento corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>Real vs. Objetivo</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>Corriente arriba actual vs. Objetivo</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>Resumen corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>Rendimiento pico TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>Tiempo de ida y vuelta (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>Resumen de resultados corriente arriba (ventana de rendimiento máx.)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>Tamaño de ventana por conexión (kB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>Ventana de agregado (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>Tasa de transferencia TCP objetivo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>Rendimiento TCP promedio (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>Rendimiento TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>Objetivo</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>Ventana 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>Ventana 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>Ventana 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>Ventana 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>Ventana 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>Ventana 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>Ventana de rendimiento máximo:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>Ventana %1 kB: %2 con. x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>Gráficos de rendimiento corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>Detalles corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 1 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>Tamaño de ventana por conexión (kB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>Rendimiento real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>Rendimiento del objetivo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>Retransmisiones totales</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 2 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 3 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 4 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 5 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 6 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 7 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 8 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 9 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 10 corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>Resultados del rendimiento de ventana de saturación corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>Rendimiento medio (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>Resultados de rendimiento corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>Corriente abajo Real vs. Objetivo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>Resumen corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>Resultados resumen corriente abajo (Ventana de rendimiento máximo)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>Gráficos de rendimiento corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>Detalles corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 1 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 2 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 3 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 4 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 5 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 6 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 7 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 8 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 9 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>Resultados de rendimiento ventana 10 corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>Resultados de rendimiento de ventana de saturación corriente abajo</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation># Recorridos de Window </translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>Ventanas sobresaturadas (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>Conexiones sobresaturadas (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>Barrido VLAN</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 iniciando comprobación de exploración de VLAN  #2</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>Comprobación de ID de VLAN #1 durante #2 segundos</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>ID de VLAN #1: CORRECTO</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>ID de VLAN #1: ERROR</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>Bucle activo insatisfactorio. Error de comprobación de ID de VLAN #1</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>Total de ID de VLAN comprobados</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>Número de ID correctos</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>Número de ID fallidos</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>Duración por ID (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>Número de intervalos</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>Intervalos seleccionados</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>Mín. ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>Máx ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Iniciar medida</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>Total de ID</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>ID correctos</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>ID erróneos</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>Configuración avanzada de Exploración de VLAN</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>Ancho de banda (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>Criterios de paso</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>No se han perdido tramas</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>Se han recibido algunas tramas</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>Configurar TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>Ejecutar TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>Tiempo estimado de ejecución</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Detener en caso de falla</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Ver Informe</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>Ver informe TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abortado</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Finalizado</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fallado</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Pasó</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Detenida por el usuario</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>Pérdida señal.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>Pérdidas Enlace.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>Se perdió la comunicación con el instrumento de prueba remoto. Por favor, restablezca el canal de comunicación e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Se encontró un problema mientras se configuraba el instrumento de prueba local, se perdió la comunicación con el instrumento de prueba remoto.  Por favor, restablezca el canal de comunicación e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Se ha detectado un problema al configurar la batería de comprobaciones remotas. Se ha perdido la comunicación con la batería de comprobaciones. Restablezca el canal de comunicaciones y vuelva a intentarlo.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>Se encontró un problema mientras se configuraba el instrumento de prueba local, TrueSAM saldrá ahora.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Se encontró un problema mientras se intentaba configurar el instrumento de prueba local.  La prueba se forzó a detenerse y el canal de comunicaciones con el instrumento de prueba remoto se perdió.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Se encontró un problema mientras se intentaba configurar el instrumento de prueba remoto. La prueba se forzó a detenerse y el canal de comunicaciones con el instrumento de prueba remoto se perdió.</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>El protector de pantalla se desactivó para evitar interferencia mientras realiza la prueba.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>Se encontró un problema en el instrumento de prueba local. TrueSAM saldrá ahora.</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>Solicitando Parámetros DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>Parámetros DHCP obtenidos</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>Inicializando Interfaz Ethernet. Por favor, espere.</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>América del Norte</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>América del Norte y Corea</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>PCS de América del Norte</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>India</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>Pérdida de señal de Hora del día de fuente de tiempo externa, provocará que la sincronización 1PPS aparezca apagada.</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>Iniciando sincronización con la señal de Hora del día proveniente de la fuente de tiempo externa...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>Existosamente sincronizado con la señal de Hora del día proveniente de la fuente de tiempo externa.</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>Bucle&#xA;Abajo</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>Páginas de la portada</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>Seleccionar pruebas a correr</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>Prueba de conectividad de tráfico extremo a extremo</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>RFC 2544 mejorado / SAM completo</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>Conjunto de pruebas de evaluación comparativa de Ethernet</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Configuración de servicios Ethernet Y.1564 y prueba de rendimiento</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>Prueba de transparencia de la Capa 2 para tramas del plano de control (CDP, STP, etc).</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>Prueba de rendimiento y desempeño TCP RFC 6349</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>Tipo de fuente C3</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>Ajustes para el canal de comunicaciones (usando el Servicio 1)</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>Ajustes del canal de comunicaciones</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>El servicio 1 debe estar configurado para estar de acuerdo con el flujo 1 en el instrumento remoto de prueba Viavi.</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>Estado local</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>Sincr. ToD</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>Sincr. 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>Conectar&#xA;al Canal</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>Capa física</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>Longitud de Pausa (Quanta)</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>Longitud de Pausa (Tiempo - ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>Correr&#xA;pruebas</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>Iniciando&#xA;pruebas</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>Detención&#xA;pruebas</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>Tiempo estimado para ejecutar las pruebas</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>Detener en caso de fallo</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>Criterios de Aprueba/Falla para pruebas seleccionadas</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>Subida y bajada</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>No tiene el concepto de Aprueba/Falla, por lo tanto, no aplica esta configuración.</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>Aprueba si se cumplen los siguientes umbrales:</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>No hay configuración de umbrales - no se informarán Aprueba/Falla</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>Local:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Remoto:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>Local y remoto</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>Rendimiento (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>Producción (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>Rendimiento (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolerancia Pérdida Tramas (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>Latencia (us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>Aprueba si se satisfacen los siguientes parámetros SLA:</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>Ascendente:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>Descendente:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>Rendimiento CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR Throughput (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>Rendimiento de CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>Rendimiento EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>Rendimiento de EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>Rendimiento EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - Tolerancia (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - Tolerancia (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - Tolerancia (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>Retardo de trama (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>Var de retardo (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>Aprueba/Falla se determinará internamente.</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>Aprueba si el rendimiento TCP medido cumple con el siguient umbral:</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>Porcentaje de rendimiento previsto</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>Prueba de rendimiento TCP no activo - no se informará el Aprueba/Falla.</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>Detener pruebas</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>Esto detendrá la prueba que se está ejecutando y la ejecución de pruebas adicionales. ¿Está seguro que desea parar?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima total para el servicio #1 es 0 o supera la velocidad de línea #2 C1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima total para la dirección de subida para el servicio #1 es 0 o supera la velocidad de línea #2 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima total para la dirección de bajada para el servicio #1 es 0 o supera la velocidad de línea #2 C1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima total para el servicio #1 es 0 o supera la velocidad de línea respectiva C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima total para la dirección de subida para el servicio #1 es 0 o supera la velocidad de línea C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima total para la dirección de bajada para el servicio #1 es 0 o supera la velocidad de línea C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima supera la velocidad de línea #1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima supera la velocidad de línea #1 C1 Mbps en la dirección de subida.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>Configuración no válida:&#xA;&#xA;la carga máxima supera la velocidad de línea #1 C1 Mbps en la dirección de bajada.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;La carga máxima supera la velocidad de línea #1 de C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>Configuración no válida:&#xA;&#xA;La carga máxima supera la velocidad de línea #1 de C2 Mbps en la dirección ascendente.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>Configuración no válida:&#xA;&#xA;La carga máxima supera la velocidad de línea #1 de C2 Mbps en la dirección descendente.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>Configuración inválida:&#xA;&#xA;CIR, EIR y Política no pueden ser todos 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>Configuración no válida:&#xA;&#xA;CIR, EIR y Políticas no pueden estar todas en 0 en la dirección upstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>Configuración no válida:&#xA;&#xA;CIR, EIR y Políticas no pueden estar todas en 0 en la dirección dowstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>Configuración inválida:&#xA;&#xA;La carga más pequeña de la etapa  (#1% de CIR) no se puede obtener usando la configuración  #2 Mbps CIR para el Servicio #3. El valor más pequeño de la etapa, usando el CIR actual para el Servicio #3 es #4%. El valor más pequeño de la etapa, usando el CIR actual para el Servicio #3 es #4%.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;el CIR (C1 Mbps) total es 0 o supera la velocidad de línea #1 C1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;el CIR (C1 Mbps) total para la dirección de subida es 0 o supera la velocidad de línea #1 C1 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;el CIR (C1 Mbps) total para la dirección de bajada es 0 o supera la velocidad de línea de #1 C1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;CIR (C2 Mbps) total es 0 o supera la velocidad de línea #1 de C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;CIR (C2 Mbps) total correspondiente a la dirección ascendente es 0 o supera la velocidad de línea #1 de C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuración no válida:&#xA;&#xA;CIR (C2 Mbps) total correspondiente a la dirección descendente es 0 o supera la velocidad de línea #1 de C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>Configuración no válida:&#xA;&#xA;Se debe seleccionar la prueba Configuración de servicio o la prueba Rendimiento del servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>Configuración inválida:&#xA;&#xA;Al menos se debe seleccionar un servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>Configuración no válida: El CIR total de los servicios seleccionados (o EIR, para aquellos servicios con un CIR de 0) no puede exceder de la velocidad de línea si se desea realizar la comprobación de rendimiento del servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuración inválida:&#xA;&#xA;El máximo valor especificado en la medición de rendimiento (RFC 2544) no puede ser menor al valor CIR, cuando ejecute la prueba de comportamiento del servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuración no válida:&#xA;&#xA;El valor máx. especificado para la medición de rendimiento de subida (RFC 2544) no puede ser menor que la suma de los valores CIR para los servicios seleccionados, cuando se ejecuta la prueba de calidad del servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuración no válida:&#xA;&#xA;El valor máx. especificado para la medición de rendimiento de bajada (RFC 2544) no puede ser menor que la suma de los valores CIR para los servicios seleccionados, cuando se ejecuta la prueba de calidad de servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuración inválida:&#xA;&#xA;El máximo valor especificado en la medición de rendimiento (RFC 2544) no puede ser menor al valor CIR, cuando ejecute la prueba de comportamiento del servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuración no válida:&#xA;&#xA;El valor máx. especificado para la medición de rendimiento de subida (RFC 2544) no puede ser menor que el valor CIR, cuando se ejecuta la prueba de calidad del servicio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuración no válida:&#xA;&#xA; El valor máx. especificado para la medición de rendimiento de bajada (RFC 2544) es menor que el valor CIR cuando se ejecuta la prueba de calidad de servicio.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>SAMComplete se detuvo porque se seleccionó Detener si falla y, al menos, un KPI no satisface el SLA para el Servicio #1.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete se detuvo porque no regresaron datos en el Servicio #1 durante los 10 segundos siguientes al inicio del tráfico. El extremo lejano ya no puede hacer el bucle para retorno de tráfico.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete se ha parado porque no llegaban datos en el Servicio #1 durante los 10 segundos después del inicio del tráfico. El equipo remoto ya no puede transmitir tráfico.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete se detuvo porque no se regresaron datos dentro de los 10 segundos siguientes al inicio del tráfico. El extremo lejano ya no puede hacer el bucle para retorno de tráfico.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete se a parado porque no llegaba tráfico después de 10 segundos de iniciar el tráfico. El equipo remoto ya no puede transmitir tráfico.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación local es para Streams y la aplicación remota en la dirección IP #1 es para tráfico.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación local es de tipo Streams y la remota en la dirección IP #1 es de tipo TCP WireSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación local es capa 3 y la aplicación remota en la dirección IP #1 es capa 2.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>Las aplicaciones local y remota son incompatibles. La aplicación local es capa 2 y la aplicación remota en la dirección IP #1 es capa 3.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>La aplicación remota en la dirección IP #1 está configurada para un IP de WAN. Es incompatible con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>La aplicación remota de la dirección IP #1 está configurada para encapsulado VLAN en serie. No es compatible con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>La aplicación remota en la dirección IP #1 está configurada para encapsulación VPLS. No es compatible con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>La aplicación remota en la dirección IP #1 es para encapsulación de MPLS. No es compatible con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>La aplicación remota admite solamente #1 servicios. </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>La prueba de retardo de IDA  requiere que ambas unidades, local y remota, tengan señal de sincronización OWD.  Falló la sincronización de retardo de una vía en la unidad local.  Por favor, revise todas las conexiones con la señal OWD.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>La prueba de retardo de IDA  requiere que ambas unidades, local y remota, tengan señal de sincronización OWD.  Falló la señal de sincronización OWD en el equipo remoto.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>Configuración no válida:&#xA;&#xA;Se debe ejecutar una prueba de retardo ida y vuelta (RTT) antes de la prueba SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>No se puede establecer la sesión de TrueSpeed. Por favor, vaya a la página de la "Red", verifique las configuraciones e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>No fue posible establecer TrueSpeed session for the ascendente direction. Por favor vaya a la página "Red", verifique las configuraciones e intente de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>No fue posible establecer una sesión TrueSpeed en la dirección descendente. Por favor vaya a la página "Red", verifique las configuraciones e intente de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>La prueba de tiempo de ida y vuelta (RTT) fue invalidada por cambiar los servicios seleccionados actualmente para la prueba. Vaya a la página "Controles de TrueSpeed" y vuelva a ejecutar la prueba RTT.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configuración inválida:&#xA;&#xA;El CIR (Mbps) total para la dirección descendente del (de los) Servicio(s) #1 no puede ser 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configuración inválida:&#xA;&#xA;El CIR (Mbps) total para la dirección ascendente del (de los) Servicio(s) #1 no puede ser 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>Configuración no válida: El CIR (Mbps) del/los servicio(s) #1 no puede ser 0.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>No se recibió tráfico. Por favor, consulte la página "Red", verifique configuraciones e intente de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>Vista del resultado principal</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>Detener&#xA;pruebas</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>  Informe creado, haga clic en Siguiente para verlo</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>Omitir revisión de reportes</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete - Prueba de activación del servicio Ethernet</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>con TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>Ajustes de red local</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>El equipo local no requiere configuración.</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>Ajustes de red remota</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>El equipo remoto no requiere configuración.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>Configuraciones IP local</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>Configuración de IP remota</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Servicios</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>Etiquetado</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>No se usó el etiquetado.</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>Throughput SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>Control SLA</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>No se seleccionaron pruebas de monitoreo.</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>Ráfaga SLA</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>Se ha deshabilitado la prueba de ráfaga por no existir la función necesaria.</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>Desempeño SLA</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>TrueSpeed está actualmente inhabilitado.&#xA;&#xA;Se puede actuar TrueSpeed en la página de configuración de la "Red" si no está en el modo de medición de bucle de retorno.</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>Configuración avanzada local</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>Configuración de tráfico avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>Configuración LBM avanzada</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>Configuración SLA avanzada</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>Tamaño aleatorio / Remix</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>IP avanzado</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>C4 Avanzado</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>Etiquetado avanzado</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>Servicio Cfg</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>Calidad del servicio</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Salir de la prueba Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>Estado del servidor de descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>Mensaje del servidor de descubrimiento</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>Dirección MAC y modo ARP</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>Selección SFP</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>IP Fuente WAN</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>IP WAN - Estática</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>Gateway WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>Máscara Subred WAN</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>IP de fuente de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>Máscara de subred de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>¿El Etiquetado VLAN está siendo utilizado en el puerto de red local?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q (VLAN apilado)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>Esperando</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>Accediendo al servidor...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>No se puede acceder al servidor</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP obtenida</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>Concesión garantizada:#1</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>Concesión reducida:#1</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>Concesión liberada</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>Servidor de descubrimiento:</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 C2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 ms FTD, #3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>Servicio 1: VoIP - 25% del tráfico - tramas de #1 y #2 bytes</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>Servicio 2: Videotelefonía - 10% del tráfico - tramas de #1 y #2 bytes</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>Servicio 3: Datos prioritarios - 15% del tráfico - tramas de #1 y #2 bytes</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>Servicio 4: Datos de BE - 50% del tráfico - tramas de #1 y #2 bytes</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>Toda Servicios</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>Propiedades del Servicio 1 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voz</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U ley 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A Law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U ley 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A ley 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>En 10GE, el nivel de granularidad del ancho de banda es 0,1 Mbps; como resultado, el valor CIR es un múltiplo de este nivel de granularidad</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>En 40GE, el nivel de granularidad del ancho de banda es 0,4 Mbps; como resultado, el valor CIR es un múltiplo de este nivel de granularidad</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>En 100GE, el nivel de granularidad del ancho de banda es 1 Mbps; como resultado, el valor CIR es un múltiplo de este nivel de granularidad</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>Tamaños de configuración</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>Tamaño Trama (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>Longitud definida</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>Longitud del ciclo EMIX</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Longitud Paquetes (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Cálc. Longitud Trama</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>El  calc. del tamaño de la trama está determinado por el uso de la longitud de paquete y la encapsulación.</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>Propiedades del Servicio 2 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>Propiedades del Servicio 3 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>Propiedades del Servicio 4 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>Propiedades del Servicio 5 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>Propiedades del Servicio 6 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>Propiedades del Servicio 7 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>Propiedades del Servicio 8 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>Propiedades del Servicio 9 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>Propiedades del Servicio 10 Triple Play</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>Tamaño Insuficiente</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>Número de servicios</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>Capa</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>Configuración LBM</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>Configurar Triple Play ...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>Propiedades del servicio </translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>DA MAC y configuración del tamaño de trama</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC; configuraciones de tamaño de marco y EtherType</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>DA MAC, longitud del paquete y configuración del TTL</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>Configuración de MAC y de longitud de paquete de DA</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>Configuraciones de red (Avanzado)</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>La incompatibilidad de una unidad local/remota requiere una longitud de paquete de &lt;b>%1&lt;/b> byte o más.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>Servicio</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configurar...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>Tamaño Usuario</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL (saltos)</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>Dirección MAC Dest</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>Mostrar ambos</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>Solamente remoto</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>Solamente local</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>Configuración LBM (Avanzada)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>Configuraciones de red Aleatorio / Tamaño EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>Tamaño del marco 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>Longitud de paquete 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>Tamaño de usuario 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>Tamaño Jumbo 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>Tamaño del marco 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>Longitud de paquete 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>Tamaño de usuario 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>Tamaño Jumbo 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>Tamaño del marco 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>Longitud de paquete 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>Tamaño de usuario 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>Tamaño Jumbo 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>Tamaño del marco 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>Longitud de paquete 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>Tamaño de usuario 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>Tamaño Jumbo 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>Tamaño del marco 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>Longitud de paquete 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>Tamaño de usuario 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>Tamaño Jumbo 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>Tamaño del marco 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>Longitud de paquete 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>Tamaño de usuario 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>Tamaño Jumbo 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>Tamaño del marco 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>Longitud de paquete 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>Tamaño de usuario 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>Tamaño Jumbo 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>Tamaño del marco 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>Longitud de paquete 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>Tamaño de usuario 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>Tamaño Jumbo 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 1</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 1</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 2</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 2</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 2</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 3</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 3</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 3</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 4</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 4</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 4</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 5</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 5</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 5</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 6</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 6</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 6</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 7</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 7</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 7</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 8</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 8</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 8</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 9</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 9</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 9</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto) Servicio 10</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local) Servicio 10</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX Servicio 10</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>¿Los servicios tienen diferentes identificaciones VLAN o prioridades de usuario?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>No, todos los servicios usan la misma configuración VLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Bit DEI</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>Avanzado...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>Pri. usuario</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>Pri. SVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>Configuración de Etiquetado VLAN (Encapsulación)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>Prioridad SVLAN</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>Todos los servicios utilizarán la misma IP de destino de tráfico.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>IP de destino de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest. IP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>IP de dest. de tráfico</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>Configurar incremento de ID de IP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>Configuración de IP (solo local)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>Ajustes IP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>Configuración de IP (solo remoto)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>Configuración IP (Avanzada)</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>Configuración IP avanzada</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>¿Los servicios tienen diferente configuración TOS o DSCP?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>No, TOS/DSCP es el mismo en todos los servicios</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>SLA agregados</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>CIR agregado</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>CIR agregado ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>Agregado CIR descendente</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>EIR agregado</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>EIR agregada ascendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>Agregado EIR descendente</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>Modo Agregado</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>Prueba de Throughput máx. Local (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>Prueba de Throughput máx. Remoto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>Activar modo agregado</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>ADVERTENCIA: los valores de ponderación seleccionados suman actualmente hasta &lt;b>%1&lt;/b>%, no 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>Throughput SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>Transferencia de SLA, (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>Rendimiento SLA, C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>Rendimiento SLA, C1 Mbps (unidireccional)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>Transferencia de SLA, C2 Mbps (Unidireccional)</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>Peso (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Políticas</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>Máx. carga</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>Solamente Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>Solamente Upstream</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>Fijar configuración avanzada de tráfico</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>Políticas SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>Máx. políticas</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>Realizar prueba de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>Tolerancia (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>Tolerancia (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>¿Desea realizar una prueba de ráfaga?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>Tipo de prueba de ráfaga:</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>Fijar configuración avanzada de ráfaga</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>Precisión de retardo de trama</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>Incluir retardo de marco como un requisito SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>Incluir variación de retardo de marco como un requisito SLA </translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>Calidad SLA (IDA)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Retardo de trama (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Retardo de trama (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>Fijar configuración avanzada de SLA</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>Config. del servicio</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>Número de etapas por debajo de CIR</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>Duración del paso (seg)</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>Etapa 1 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>Etapa 2 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>Etapa 3 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>Etapa 4 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>Etapa 5 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>Etapa 6 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>Etapa 7 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>Etapa 8 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>Etapa 9 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>Etapa 10 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>Porcentajes del paso</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>Calidad del servicio </translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>Cada dirección se prueba por separado, por lo que la duración general de la prueba será dos veces el valor introducido.</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Detener la prueba en caso de falla</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>Configuración de prueba de ráfaga avanzada</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>Omitir J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Seleccionar pruebas Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>Seleccionar servicios para la prueba</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>Configurar Todos</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  Total:</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>Prueba de configuración del servicio </translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>Prueba de calidad del servicio </translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>Medidas opcionales</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>Throughput (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>Máx. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>Máx. (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>Máx. (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>Las medidas opcionales no se pueden llevar a cabo si está activo el servicio TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Ejecutar pruebas Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Omitir pruebas Y.1564</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>Var de retardo</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>Resumen de las Fallas en las pruebas</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>Veredictos</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Veredicto Y.1564 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>Veredicto Prueba de Config </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 1 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 2 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 3 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 4 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 5 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 6 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 7 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 8 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 9 </translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>Veredicto Prueba de Config Svc 10 </translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Veredicto prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Veredicto Serv.1 prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Veredicto Serv 2 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Veredicto Serv 3 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Veredicto Serv 4 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Veredicto Serv 5 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Veredicto Serv 6 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Veredicto Serv 7 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Veredicto Serv 8 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Veredicto Serv 9 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Veredicto Serv 10 prueba de desempeño</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Veredicto IR de Prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Veredicto de la pérdida de trama de la prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Veredicto del retardo de la prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Veredicto de la variación de retardo de la prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Veredicto TrueSpeed de Prueba de calidad</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>Resultados de la configuración del servicio </translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 1 </translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>Rendimiento máx. (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>Rendimiento máx. de bajada (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>Rendimiento máx. de subida (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>Rendimiento máx. (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>Rendimiento máx. de bajada (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>Rendimiento máx. de subida (c2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>Veredicto CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>Veredicto CIR de bajada</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>Variación de retardo de trama (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>Conteo OoS</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>Detectar marco de error</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>Pausa detectada</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>Veredicto CIR de subida</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>Veredicto CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>Veredicto CBS de bajada</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>Tamaño de ráfaga configurada (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Tamaño de ráfaga Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>Tamaño de ráfaga de Rx Prom. (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>CBS estimado (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>Veredicto CBS de subida</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>Veredicto EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>Veredicto EIR de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>Veredicto EIR de subida</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>Veredicto de Políticas</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>Veredicto de Políticas de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>Veredicto de Políticas de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>Veredicto del Paso 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>Veredicto del Paso 1 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>Veredicto del Paso 1 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>Veredicto del Paso 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>Veredicto del Paso 2 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>Veredicto del Paso 2 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>Veredicto del Paso 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>Veredicto del Paso 3 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>Veredicto del Paso 3 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>Veredicto del Paso 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>Veredicto del Paso 4 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>Veredicto del Paso 4 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>Veredicto del Paso 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>Veredicto del Paso 5 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>Veredicto del Paso 5 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>Veredicto del Paso 6</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>Veredicto del Paso 6 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>Veredicto del Paso 6 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>Veredicto del Paso 7</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>Veredicto del Paso 7 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>Veredicto del Paso 7 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>Veredicto del Paso 8</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>Veredicto del Paso 8 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>Veredicto del Paso 8 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>Veredicto del Paso 9</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>Veredicto del Paso 9 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>Veredicto del Paso 9 de subida</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>Veredicto del Paso 10</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>Veredicto del Paso 10 de bajada</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>Veredicto del Paso 10 de subida</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>Rendimiento máx. (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>Transferencia máx. (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>Máx. Rendimiento (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>Pasos</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Ráfagas</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>Clave</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>Haga clic en las barras para revisar resultados para cada paso.</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (Throughput Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (Throughput C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (Throughput C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>Detectar error</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>Paso 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>Paso 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>Paso 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>Paso 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>Paso 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>Paso 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>Paso 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>Paso 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>9 Paso 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>Paso 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>Umbrales SLA</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 2 </translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 3 </translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 4 </translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 5 </translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 6 </translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 7 </translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 8 </translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 9 </translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>Resultados de la configuración del Servicio 10 </translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>Resultados de calidad del servicio </translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Svc. Veredicto</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>Act. IR</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>Máx. IR</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>Mín. IR</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>Prom. IR</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>Segundos de pérdida de trama</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>Conteo de pérdida de tramas</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>Retardo Act.</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>Retardo Máx.</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>Retardo Mín.</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>Retardo Prom.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>Var. de retardo. Act.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>Var. de retardo. Máx.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>Var. de retardo. Mín.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>Var. de retardo. Prom.</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>Disponibilidad</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponible</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>Segundos disponibles</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>Segundos no disponibles</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>Segundos con muchos errores</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>Rendimiento de calidad del servicio </translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Overview</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variación de retardo</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>Servicio TrueSpeed. Ver resultados en la página de resultados de TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR, media.&#xA;(Throughput&#xA;C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR, media.&#xA;(Throughput&#xA;C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>Ratio Pérdidas&#xA;de Trama</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>Retardo de ida&#xA;Prom. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>Var de retardo.&#xA;Máx. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>Errores detectados</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>Retardo&#xA;Prom. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, Cur.&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, Cur.&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, Máx.&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, Máx.&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, Mín.&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, Mín.&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>Conteo de pérdida&#xA;de tramas</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>Umbral de la&#xA;Tasa&#xA;de pérdida de trama</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>Retardo&#xA;Act. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>Retardo&#xA;Máx. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>Retardo&#xA;Mín. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>Retardo&#xA;Prom. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>Umbral&#xA;demora&#xA;(OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>Retardo&#xA;Act. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>Retardo&#xA;Máx. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>Retardo&#xA;Mín. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>Umbral&#xA;demora&#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>Var. de retardo.&#xA;Act. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>Var de retardo.&#xA;Prom. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>Var de retardo.&#xA;Umbral (ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>Tasa de&#xA;segundos&#xA;disponibles</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>Segundos&#xA;no disponibles</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>Segundos&#xA;con muchos&#xA;errores</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Rendimiento&#xA;de configuración&#xA;de servicio (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Transferencia de configuración de&#xA;servicio&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Rendimiento&#xA;de Perf&#xA;de servicio (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Transferencia de rendimiento de&#xA;servicio&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>Configuraciones de red</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Tamaño Trama Usuario</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>Tamaño Trama Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>Longitud Paquete definido por Usuario</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>Longitud Paquete Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>Cálc. Tamaño Trama (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>Configuraciones de red (Aleatorio / Tamaño EMIX)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>Longitudes aleatorias / EMIX (Remoto)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>Longitudes aleatorias / EMIX (Local)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>Longitudes aleatorias / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>Configuración IP avanzada - remota</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>CIR descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>EIR descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>Control descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>M descendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>CIR ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>EIR ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>Control ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>M ascendente</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>Ráfaga avanzada SLA</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>Tasa de pérdida de marco descendente</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>Demora de marco descendente (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>Demora de marco descendente (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>Variación de demora descendente (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>Relación de pérdida de marco ascendente</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>Demora de marco ascendente (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>Demora de marco ascendente (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>Variación de demora ascendente (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>Incluir como un Requisito SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>Incluir retardo de marco</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>Incluir variación de retardo de marco</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete tiene la siguiente configuración inválida:</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>Resultados de la configuración del servicio</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>Lanzar TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>Nota: La prueba TrueSpeed solo se puede correr si se habilita la Prueba de Desempeño de Servicio.</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>Fijar longitud del paquete TTL</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>Fijar puertos TCP/UDP</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>Tipo Orig.</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Puerto Orig. </translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation> Tipo dest.</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation> Puerto dest.</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>Umbral de rendimiento TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>Tamaño total de la ventana recomendado (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>Tamaño total de ventanas aceleradas (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>Conexiones # recomendadas</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation># Conexiones aceleradas</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>Tamaño de la ventana total recomendada de subida (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>Tamaño total de ventana acelerada corriente arriba (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation># de conexiones recomendadas de subida</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation># de conexiones aceleradas corriente arriba</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>Tamaño de la ventana total recomendada de bajada (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>Tamaño total de ventana acelerada corriente abajo (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation># de conexiones recomendadas de bajada</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation># Conexiones aceleradas corriente abajo</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation># recomendado de conexiones</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>Rendimiento TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Acterna Payload</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>Tiempo de viaje de ida y vuelta (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>El RTT se utilizará en los pasos siguientes para realizar la recomendación del tamaño de ventana y de las conexiones.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>Resultados de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>Transferir mediciones TCP</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>Objetivo C4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>Veredicto del servicio TrueSpeed </translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>Veredicto de servicio TrueSpeed ascendente</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>Tasa de transferencia TCP real (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>Tasa de transferencia TCP ascendente actual (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>Tasa de transferencia TCP ascendente objetivo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>Tasa de transferencia TCP objetivo (Mbps))</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>Veredicto de servicio descendente TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>Tasa de transferencia TCP descendente actual (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>Tasa de transferencia TCP descendente objetivo (Mbps)</translation>
        </message>
    </context>
</TS>
