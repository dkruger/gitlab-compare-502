<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation> 확실히 취소하고 싶으십니까 ?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>파일을 읽을 때 에러가 발생했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>파일에 프레임이 너무 많습니다 (50,000 개 이상 ).  부분 버퍼를 저장하거나 ,&#xA;USB 드라이브로 내보내기 하고 , 다른 장치에서 그것을 로딩하세요 . </translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>저장된 모든 데이터를 삭제하시겠습니까?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>선택된 항목을 제거하시겠습니까?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>이름이 이미 존재합니다.&#xA;오래된 데이터를 덮어 쓰시겠습니까?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>그래프가 디스크에 기록하는 중에 에러가 발생했습니다 . 작업을 보호하기 위해 &#xA; 그래프를 지금 저장하세요 . 위험 수준에 도달하면 그래프가 정지되고 자동적으로 삭제될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>너무 많은 디스크 에러가 그래프에 발생하여 계속할 수 없어 정지되었습니다 .&#xA; 그래프 데이터가 삭제되었습니다 . 원하시면 그래프를 재시작할 수 있습니다 .</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>UsbFlash</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>제거 가능한 장치</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>로그가 가득 찼습니다</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>유효하지 않은 설정 :&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>무효</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>음성</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>데이터 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>데이터 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>SW가 지원하는 CFP MSA Mgmt I/F 사양 개정</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>SW가 지원하는 QSFP MSA Mgmt I/F 사양 개정</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>DMC 정보</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>바 코드</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>제조 일자</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>교정 일자</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>SW 리비전</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>옵션 요청 ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>조립 시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>집합 바코드</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>설치된 소프트웨어</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>옵션:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>패킷을 기다리는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>패킷 분석 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>분석</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>링크를 기다리고 있음 ...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>시작하기 위해 "Start Analysis" 를 누르세요 ...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>결과를 저장하고 있습니다. 기다려주세요.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>결과 저장이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>결과가 저장되었습니다.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>시작 대기중</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>이전 테스트가 사용자에 의해 중지되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>이전 테스트가 에러로 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>이전 테스트는 실패하였고 실패 시 중단 기능이 활성화되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>실행되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>남아 있는 시간: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>실행 중</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>테스트를 완료할 수 없었으며 에러로 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>실패 !</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>테스트 완료, 일부 결과 실패</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>테스트 완료, 모든 결과 통과</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>사용자에 의해 정지됨</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>테스트가 사용자에 의해 중지되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>정지 중</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>실행되는 테스트 없음</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>시작 중</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>연결 확인됨</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>연결 상실됨</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>연결 확인 중</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>유효하지 않은 설정</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>결과를 저장하고 있습니다. 기다려주세요.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>결과 저장이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>결과가 저장되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>처리량 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>업스트림 처리량 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>업스트림 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>다운스트림 처리량 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>다운스트림 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>예외</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>업스트림 예외</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>다운스트림 예외</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>레이턴시 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>레이턴시 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>업스트림 레이턴시 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>업스트림 레이턴시 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>다운스트림 레이턴시 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>다운스트림 레이턴시 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>지터 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>지터 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>업스트림 지터 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>업스트림 지터 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>다운스트림 지터 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>다운스트림 지터 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 바이트 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 바이트 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 바이트 업스트림 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 바이트 업스트림 프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 바이트 다운스트림 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 바이트 다운스트림 프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>업스트림 CBS 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>다운스트림 CBS 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS 폴리싱 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>업스트림 CBS 폴리싱 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>다운스트림 CBS 폴리싱 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>버스트 헌트 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>업스트림 버스트 헌트 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>다운스트림 버스트 헌트 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>백투백 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>업스트림 백투백 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>다운스트림 백투백 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>시스템 복구 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>시스템 복구 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>업스트림 시스템 복구 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>업스트림 시스템 복구 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>다운스트림 시스템 복구 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>다운스트림 시스템 복구 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>확장 로드 테스트 결과</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>패스 MTU 단계</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>RTT 단계</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>업스트림 워크 더 윈도우 단계</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>다운스트림 워크 더 윈도우 단계</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>업스트림 TCP 처리량 단계</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>다운스트림 TCP 처리량 단계</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>결과를 저장하고 있습니다. 기다려주세요.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>결과 저장이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>결과가 저장되었습니다.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Sync를 확보할 수 없습니다. 모든 케이블이 부착되어있는지, 포트는 올바르게 설정되어 있는지 확인하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>활성화된 링크를 찾을 수 없었습니다. 모든 케이블이 부착되어있는지, 포트는 올바르게 설정되어 있는지 확인하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>연결된 포트의 속도나 이중을 결정할 수 없었습니다. 모든 케이블이 부착되어있는지, 포트는 올바르게 설정되어 있는지 확인하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>로컬 테스트 세트를 위한 IP 주소를 얻을 수 없었습니다 . 통신 설정을 확인하고 다시 시도하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>원격 테스트 설정으로 통신 채널을 설정할 수 없습니다 . 통신 설정을 확인하고 다시 시도하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>원격 테스트 세트에 BERT 소프트웨어의 호환 버전이 없는 것 같습니다. 최소 호환 버전은 %1입니다.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>TrueSpeed를 실행하기 위해 레이어 4 TCP 와이어스피드 애플리케이션을 로컬 장치와 원격 장치 모두에서 사용할 수 있어야 합니다.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>TrueSpeed 테스트 선택이 이 캡슐화 설정에 대해 유효하지 않으므로 제거될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>TrueSpeed 테스트 선택이 이 프레임 종류 설정에 대해 유효하지 않으므로 제거될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>TrueSpeed 테스트 선택이 이 원격 캡슐화에 대해 유효하지 않으므로 제거될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>SAM-Complete 테스트 선택이 이 캡슐화 설정에 대해 유효하지 않으므로 제거될 것입니다 .</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>유효하지 않은 설정</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>CBS 정책</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>폴리싱</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>단계 #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>단계 #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>단계 #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>단계 #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>단계 #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>단계 #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>단계 #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>단계 #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>단계 #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>단계 #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>결과를 저장하고 있습니다. 기다려주세요.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM Complete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>결과 저장이 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>결과가 저장되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>로컬 ARP 실패 .</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>원격 ARP 실패 .</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> 테이블 , cont.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>스크린샷</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - 막대 그래프가 여러 페이지에 결쳐있습니다</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>시간 척도</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>사용할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>사용자 정보</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>설정 그룹</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>결과 그룹</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>이벤트 로거</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>막대 그래프</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>스크린샷</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>예상 TCP 성능 (Througpht)</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation> 다중 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Viavi 8000 이 생성함 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Viavi 6000 이 생성함 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Viavi 5800 이 생성함 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Viavi 테스트 장치가 생성함 </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> 통과</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>스캔 주파수 (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>통과 / 실패</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>설정 :  필터 /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> ( 패턴 및 마스크 )</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>패턴</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>마스크</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>설정 : </translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>해당되는 설정이 없음…</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI 체크</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>라디오</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>밴드</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>CPRI 테스트 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>테스트가 완료되지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>진행 중</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>정지 시간</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>켜짐</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>이벤트 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>지속 시간 / 값</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>확장 로드 테스트 이벤트 로그</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>알 수 없음 - 상태</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>진행 중 . 기다려 주세요…</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>실패 !</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>명령실행 완료 !</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>중단 !</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>루프 업</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>루프 다운</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>장착</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>해제</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>전원 끄기</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>루프 명령 전송</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>스위치</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>스위치 리셋</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>쿼리 생성</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>쿼리 루프백</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>근단 장착</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>근단 해제</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>전원 쿼리</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>범위 쿼리</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>타임아웃 해지</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>타임아웃 리셋</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>순차적 루프</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 층 1 트레이스</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 보류된된</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 보류된된</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 운송 노드 클락 트레이스</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 보류된된</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 보류된된</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 층 2 트레이스</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 보류된된</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 보류된된</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 층 3 트레이스</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 보류된된</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Sonet 최소 클락 트레이스</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 층 3E 트레이스</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Netwk Op 제공</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 동기화를 위해 사용하지 마세요 .</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>불특정으로 지정</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>TUG 구조</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>잠긴 TU</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>비동기 34M/45M</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>비동기 140M</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>ATM 매핑</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>MAN (DQDB) 매핑</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>FDDI 매핑</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>HDLC/PPP 매핑</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 해독</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>O.181 테스트 신호</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>비동기</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>비트 동기화</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>바이트 동기화</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>보류된된</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>VT- 구조 STS-1 SPE</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>잠긴 VT 모드</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>Async. DS3 매핑</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>Async. DS4NA 매핑</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>Async. FDDI 매핑</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>포맷 ?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>범위 초과</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>켜짐</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>이력</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>초과</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + 이력</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>공간</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>마크</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>GREEN</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>YELLOW</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>RED</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NONE</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>모두</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>거절</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>불분명한</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>승인</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>알 수 없음</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>성공</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>실행 중</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>테스트가 완료되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>상태 알 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>확인됨</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>확인할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>언노운 확인</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>%1 시간 %2 분 남아 있음</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>%1 분 남아 있음</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>너무 낮음</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>너무 높음</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>( 너무 낮음 ) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>( 너무 높음 ) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>바이트</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>값</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Trib 포트</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>지정되지 않음</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>준비되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>진행 중인 매핑</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC over SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>단순 데이터 링크 매핑 (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>HCLC/LAP-S 매핑</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>단순 데이터 링크 매핑 ( 설정 - 재설정 )</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>10 Gbit/s 이더넷 프레임 매핑</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>GFP 매핑</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>10 Gbit/s 파이버 채널 매핑</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>보류된 – 전용</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>보류된 - 내셔널</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>테스트 신호 O.181 매핑</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>보류된 (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>불특정으로 지정</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>비동기 DS3 매핑</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>비동기 DS4NA 매핑</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>비동기 FDDI 매핑</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC Over SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 VT 페이로드 결함</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI Unassgn.</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Await. TEI</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Est. Await. TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI Assigned</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Await. Est.</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Await. Rel.</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Mult. Frm. Est.</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Timer Recovery</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Link Unknown</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>AWAITING ESTABLISHMENT</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>MULTIFRAME ESTABLISHED</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>ONHOOK</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>DIALTONE</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>ENBLOCK DIALING</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>RINGING</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>CONNECTED</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>CALL RELEASING</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>스피치</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>데이터</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>팩스 G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Teletex</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Videotex</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>스피치 BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>데이터 BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>데이터 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>팩스 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>검색</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>사용 가능</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>결합 요청</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>재시도 요청</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Ack 버스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>결합 응답</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>버스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>상태 응답</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>스트림 구멍 파악</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>에러</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>에러 : 서비스가 아직 버퍼링되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>에러 : 재시도 패킷 요청이 유효하지 않음</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>에러 : 서비스가 없음</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>에러 : 그러한 섹션이 없음 </translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>에러 : 세션 에러</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>에러 : 지원하지 않는 명령 및 컨트롤 버전</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>에러 : 서버가 가득 참</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>에러 : 중복 결합</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>에러 : 중복 세션 ID</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>에러 : 비트 전송률이 좋지 않음</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>에러 : 서버가 파괴한 세션</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>짧음</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>열림</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>정상</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>거꾸로</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>레벨이 너무 낮음</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 바이트</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 바이트</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>아니오</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>선택됨</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>선택되지 않음</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>START</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOP</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>에러 프레임</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS 프레임</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>손실된 프레임</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>코드 위반</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>이벤트 로그가 가득 찼습니다</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>USB 키가 발견되지 않음 . USB 를 삽입하고 다시 시도하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>이 아이템에 대한 도움말이 제공되지 않음 .</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>장치 Id</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>신호 없음</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>신호</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>준비 됨</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>사용됨</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>위성 ID</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>GNSS ID</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>프레이밍</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>마스크</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>MTIE 마스크</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>TDEV 마스크</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>MTIE 결과</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>MTIE 마스크</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>TDEV 결과</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>TDEV 마스크</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Orig. TIE 데이터</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Offset rem. 데이터</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>MTIE/TDEV 곡선 스타일</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>선 + 점</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>점으로만 </translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>MTIE 만</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>TDEV 만</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>마스크 종류</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>MTIE 통과</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>MTIE 실패</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>TDEV 통과</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>TDEV 실패</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>관측 간격 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>계산 중 </translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>계산 취소</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>계산 완료</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>TIE 데이터 업데이트 </translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>TIE 데이터가 로딩됨</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>TIE 데이터가 로딩되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>Wander Analysis 를 로컬로 실행하기 위한 메모리가 충분하지 않습니다 .&#xA;256MB 램이 필요합니다 . 외부 분석 소프트웨어를 대신 사용하세요 .&#xA; 자세한 사항은 매뉴얼을 참조하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Freq. 오프셋 (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>드리프트율 (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>샘플</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>샘플율 ( 초 당 )</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>블록</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>현재 블록</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>오프셋 제거</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>곡선 선택</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>곡선 둘 다</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Offs.rem. 만</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>스크린샷 캡쳐</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>로컬</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>원격</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>라벨 없음</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>포트 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>포트 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>툴바</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>메시지 로그</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM Complete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>일반 정보 :</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>그래프 사용 불가</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>인쇄 에러 !</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbps 최소</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbps 최대</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR 지터</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR 지터 최대</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC 에러</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT 에러</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID 에러</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID 에러 최대</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># 스트림</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP ChkSum 에러</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP ChkSum 에러</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>전송 Strm ID</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP 있음</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pkt 손실 전체</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pkt 손실 현재</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pkt 손실 피크</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Pkt 지터 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Pkt 지터 최대 (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>OoS Pkts 전체 .</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS Pkts 현재</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS Pkts 최대</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Dist 에러전체</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Dist 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Dist 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>기간 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>기간 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>기간 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>초대 손실 기간</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>최소 손실 Dist</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Sync 손실 전체</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Sync 바이트 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Sync 바이트 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Sync 바이트 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF 현재</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF 최대</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR 현재</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR 최대</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Transp 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Transp 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Transp 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT 에러 최대</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation># 스트림  분석됨</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>총 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP ChkSum 에러</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP ChkSum 에러</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>전송 Strm ID</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>Prog No</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Prog Mbps 현재</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Prog Mbps 최소</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Prog Mbps 최대</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP 있음</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pkt 손실 전체</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pkt 손실 현재</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pkt 손실 피크</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Pkt 지터 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Pkt 지터 최대 (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>OoS Pkts Tot</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS Pkts 현재</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS Pkts 최대</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Dist 에러전체</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Dist 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Dist 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>기간 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>기간 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>기간 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>초대 손실 기간</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>최소 손실 Dist </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Sync 손실 전체</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Sync 바이트 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Sync 바이트 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Sync 바이트 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF 현재</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF 최대</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR 현재</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR 최대</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>PCR 지터 현재</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR 지터 최대</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>CC 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Transp 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Transp 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Transp 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>PMT 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID 에러 전체</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>PID 에러 현재</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID 에러 최대</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Pkt 손실</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Pkt 손실 최대</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Pkt Jit</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Pkt Jit 최대</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># 스트림</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Rx Mbps, 현재 L1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Prog Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Prog Mbps 최소</translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>전송 ID</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Sync 손실</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>전체 Sync 바이트 에러</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Sync 바이트 에러</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Sync 바이트 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>전체 PAT 에러</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>PAT 에러</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR 지터</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>PCR 지터 최대</translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>총 CC 에러</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC 에러</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>전체 PMT 에러</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT 에러</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>전체 PID 에러</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID 에러</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID 에러 최대</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>전체 Transp 에러</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Transp 에러</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Transp 에러 최대</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation># 스트림  분석됨</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>총 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP ChkSum 에러</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP ChkSum 에러</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>MPEG 히스토리</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP 있음</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pkt 손실 현재</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pkt 손실 전체</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pkt 손실 피크</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Pkt 지터 현재</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Pkt 지터 최대</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>피크 - 피크</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos- 피크</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg- 피크</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Peak 피크</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Pos 피크</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Neg 피크</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>테스트 보고서 정보</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>코드</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>목적지</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>출발지</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>경로</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Chan</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>사용하지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>광학 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>광학 셀프테스트</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>설정 : </translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>세부사항</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>스택</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>스택 VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN 스택 깊이</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>SVLAN %1 DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>SVLAN %1 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>사용자 SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>프레임이 정의되지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>바이트</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>값</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Trib 포트</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>설정 : :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>OTN 체크 테스트</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>OTN 체크 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN 체크</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>테스트가 완료되지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>POH 바이트 캡쳐</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>바이너리</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>설정 : </translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>해당되는 설정이 없음…</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>오버헤드 바이트</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>CDMA 리시버</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>인쇄 에러 !</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>PTP 테스트</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>로딩된 프로파일</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>PTP 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP 체크</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>테스트가 완료되지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>요약</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>모든 요약 결과 OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>향상된 FC 테스트</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>향상된 RFC 2544 테스트</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>모드</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>처리량</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>프레임 손실</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>버스트</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>시스템 복구</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>버퍼 크레딧 처리량</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>확장 로드</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>사용자에 의해 정지됨</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>실행되지 않습니다 - 이전 테스트가 사용자에 의해 중지되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>실행되지 않습니다 - 이전 테스트가 에러로 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>실행되지 않습니다 - 이전 테스트는 실패하였고 실패 시 중단 기능이 활성화되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>실행되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>대칭 루프백</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>대칭 업스트림 및 다운스트림</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>비대칭 업스트림 및 다운스트림</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>업스트림</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>다운스트림</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>처리량</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>패킷 지터</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>프레임 손실</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>버스트 (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>백투백</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>시스템 복구</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>확장 로드</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>버퍼 크레딧 처리량</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>실행할 테스트</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>선택된 프레임 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>선택된 패킷 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>선택된 업스트림 프레임 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>선택된 업스트림 패킷 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>선택된 다운스트림 프레임 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>선택된 다운스트림 패킷 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>다운스트림 프레임 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 바이트 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 바이트 업스트림 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 바이트 다운스트림 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 바이트 버퍼 크레딧 처리량 테스트 그래프</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>초과</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>지속 시간 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>정지 시간</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>초과</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>최장</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>최단</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>가장 최근</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>평균</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>단절</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>총</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>설정 : </translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>커넥터</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>공칭 파장 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>공칭 비트 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>파장 (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>최소 비트 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>최대 비트 전송률 (Mbits/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>전원 레벨 종류</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>제조사</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>제조사 PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>제조사 리비전</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>최대 Rx 레벨 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>최대 Tx 레벨 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>제조사 SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>날짜 코드</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>로트 코드</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>진단 모니터링</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>진단 바이트</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>트랜시버</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW / SW 버전 #</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA HW Spec. Rev#</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA Mgmt. I/F Rev#</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>파워 클래스</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx 전원 레벨 종류</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>최대 람다 파워 (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>활성화된 광케이블의 #</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>파이버당 파장</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>파이버 범위 당 WL (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>최대 네트워크 레인 비트율 (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>지원되는 트래픽율</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>무효</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>프레임 지연 (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>프레임 지연 (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>편도 지연</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>업스트림</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>다운스트림</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>열</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>설정 : :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed 테스트</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>턴업</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>트러블슈트</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>모드</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>대칭</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>비대칭</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>처리량 대칭</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>경로 MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>워크 더 윈도우</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP 처리량</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>고급 TCP</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>실행 단계</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>사용자에 의해 정지됨</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>실행되지 않습니다 - 이전 테스트가 사용자에 의해 중지되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>실행되지 않습니다 - 이전 테스트가 에러로 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>실행되지 않습니다 - 이전 테스트는 실패하였고 실패 시 중단 기능이 활성화되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>실행되지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>테스트가 완료되지 않음</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>테스트가 사용자에 의해 중단되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>테스트가 시작되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>업스트림 통과</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>처리량이 목표의 90% 이상입니다.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>업스트림 실패</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>처리량이 목표의 90% 미만입니다.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>다운스트림 통과</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>다운스트림 실패</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>테스트 보고서 정보</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>기술자 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>회사</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>이메일</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>전화</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>테스트 확인</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>테스트 이름</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>인증 코드</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>인증 생성일</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>테스트 정지 시간</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>설명</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>정지 시간</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>트레이스</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>시퀀스</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>설정 : </translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>로그 :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>StrmIP:Port</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Strm 이름</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>정지 시간</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur/Val</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Prog 이름</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>진행 중</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>VLAN 스캔 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN 스캔 테스트</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>설정 : </translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>결과 :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>TrueSAM 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>테스트를 완료할 수 없었으며 에러로 중단되었습니다 보고서의 결과가 완전하지 않을 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>테스트가 사용자에 의해 중지되었습니다 . 보고서의 결과가 완전하지 않을 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>하위 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM Complete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>J-QuickCheck 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>RFC 2544 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>SAMComplete 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>J-Proof 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>TrueSpeed 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>사용자에 의해 정지됨</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>실행되지 않습니다 - 이전 테스트가 사용자에 의해 중지되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>실행되지 않습니다 - 이전 테스트가 에러로 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>실행되지 않습니다 - 이전 테스트는 실패하였고 실패 시 중단 기능이 활성화되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>실행되지 않음</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>메시지 로그</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>메시지 로그 ( 계속됨 )</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>개요</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>기술자 ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>작업 순서</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>설명 / 주의</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW 버전</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>SAMComplete 전체 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>전체 구성 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>전체 성능 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>프레임 손실</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>지연 변이</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>처리량</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>보고서 그룹</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>보고서를 생성할 수 없음 : </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>디스크 공간이 충분하지 않음 .</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>보고서를 생성할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>포트 설정</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>홀수</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>짝수</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>보오레이트</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>패리티</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>플로우 컨트롤</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>하드웨어</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>데이터 비트</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>정지 비트</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>터미널 설정</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>엔터 / 리턴</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>로컬 에코</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>보류된 키 활성</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>해제된 키</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>켜짐</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>해제</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>활성</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, 패널 소프트키</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, 패널 소프트 키</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, SETUP, RESULTS, SCRIPT, START/STOP, 패널 소프트 키</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, SETUP, RESULTS, EXPORT, START/STOP, 패널 소프트 키</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>터미널 &#xA; 윈도우</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>기본값 &#xA; 복구</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>VT100&#xA; 설정</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>스크린 &#xA; 삭제</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>키보드 &#xA; 보기</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>키보드 &#xA; 이동</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>오토보오드</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>스크린 &#xA; 캡쳐</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>키보드 &#xA; 숨기기</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>루프 진행 :</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>결과 :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>예상하지 않은 에러 발생</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>실행 &#xA; 스크립트</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA; 이것은 링크에 있는 스트림을 재스캔하고 테스트를 재시작할 것입니다 .&#xA;&#xA; 익스플로러에서 &#xA; 전송되었던 스트림만 보는 것이 아닐 것입니다 .&#xA;&#xA; 계속하시겠습니까 ?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>자동 진행 :</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>세부 사항 :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>자동으로 진행 중 . 기다려 주세요…</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>자동 작업 실패 .</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>자동 작업 완료 .</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>자동 작업 취소 .</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>알 수 없음 - 상태</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>검색 중 </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>스캐닝 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>실패 !</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>예상하지 않은 에러 발생</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>페어링 된 장치 세부사항</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>파일 전송</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>연결</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>잊기</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>파일 선택</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>전송</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>연결 중...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>연결 해제 중...</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>연결 해제</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>번들 이름 : </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>번들 이름 입력 :</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>증명서 및 키 (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>무효 번들 이름</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>이름이 "%1" 인 번들이 이미 존재합니다 .&#xA; 다른 이름을 선택해 주세요 .</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>파일 추가</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>번들 생성</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>번들 이름 수정</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>번들 수정</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>폴더 열기</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>새로운 번들 추가 ...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>%1 에 증명서 추가 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>삭제 %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>%2 에서 %1 삭제</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> 비트</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>가상 레인 ID</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>주입된 스큐 ( 비트 )</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>주입된 스큐 (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>물리 레이어 #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>범위:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>날짜를 설정할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> 숫자</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>트리뷰터리 슬롯</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>적용</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>기본값</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>대역폭</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>변경사항이 아직 적용되지 않았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>선택된 트립 슬롯이 너무 많습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>선택된 트립 슬롯이 너무 적습니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>기타...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> 글자</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 숫자</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>오늘</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>일자 입력 : dd/mm/yyyy</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>일자 입력 : mm/dd/yyyy</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> 바이트</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>까지 </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> 바이트</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> 숫자</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 숫자</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>일</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>dd/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh:mm</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>지속 시간: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>모든 서비스 탭에서의 목적지 주소를 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>모든 스트림 탭에서의 목적지 주소를 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>목적지 MAC</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>목적지 종류</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>루프 종류</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>이 홉 소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>다음 홉 목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>다음 홉 서브넷 마스크</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>모든 프레임을 위해 이더넷 탭에서 소스 주소를 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>소스 종류</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>기본값 MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>사용자 MAC</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>모든 서비스 탭에서의 소스 주소를 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>모든 스트림 탭에서의 소스 주소를 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>소스 MAC</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>PBit 증가</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>사용자 SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>루프백 모드에서는 설정할 수 없음 .</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>VLAN ID 지정</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>길이</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>데이터 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>컨트롤</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>종류 /&#xA; 길이</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>터널 &#xA; 라벨</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>B-Tag VLAN ID 필터</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>B-Tag 우선순위</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>B-Tag DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>터널 라벨</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>터널 우선순위</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>B-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>터널 TTL</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC&#xA; 라벨</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>I-Tag 우선순위</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>I-Tag DEI 비트</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>I-Tag UCA 비트</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>I-Tag 서비스 ID 필터</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>I-Tag 서비스 ID</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>VC 라벨</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>VC 우선순위</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>I-Tag EtherType</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA; 라벨</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>MPLS1 라벨</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>MPLS1 우선순위</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA; 라벨</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>MPLS2 라벨</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>MPLS2 우선순위</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>데이터</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>고객 프레임이 이동 중 :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>캡슐화</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>프레임 종류</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>고객 프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>사용자 프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>소형 크기</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>점보 크기</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>데이터 섹션이 IP 패킷을 포함하고 있습니다 . IP 탭에서 이 패킷을 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>IP 필터 탭에서 데이터 필터링을 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Tx 페이로드</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>RTD 설정</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>페이로드 분석</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Rx 페이로드</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>LPAC 타이머</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Rx BERT 패턴</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Tx BERT 패턴</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>사용자 패턴</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>인입되는 프레임 설정 :</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>아웃고잉 프레임 설정 :</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>길이 / 종류 필드는 0x8870 입니다</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>데이터 길이는 랜덤입니다</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>사용자 우선순위 시작 인덱스</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>파일 종류 :</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>파일 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>확실히 삭제하고 싶으십니까 ?&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>이 폴더에 있는 모든 파일을 삭제하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> 숫자</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation>바이트</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>까지 </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> 바이트</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>범위 :  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> (hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> 숫자</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> 유효하지 않음 – 포트 1 과 &#xA;2 의 소스 IP 가 일치해서는 안됩니다 .  이전 IP 복구됨 .</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>유효하지 않는 IP 주소 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>범위 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>해당 IP 주소는 이 설정에 적절하지 않습니다 .&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>바이트당 2 개 글자 , 까지 </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> 바이트</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>바이트당 2 개 글자 , </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>루프 - 코드 이름</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>비트 패턴</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>전송</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>밴드 내</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>밴드 외</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>루프 업</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>루프 다운</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>기타</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>루프 - 코드 이름</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 비트</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Del.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>추가 / 제거</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Tx 트레이스</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>범위 :  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>채널 선택</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>KLM 값 입력</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>범위 :  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>VCG 멤버 선택</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>PSI 바이트</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>바이트 값</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>ODU 종류</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>트리뷰터리 포트 #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>범위 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>VCG 멤버 선택</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>범위 :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>오버헤드 바이트 에디터</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>편집하기 위해 유효한 바이트를 선택하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>인식되지 않은 광학</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>페이지 구성 중 .  기다려 주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>페이지 비어 있음 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>아이템 추가</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>아이템 수정</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>아이템 삭제</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>열 추가</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>열 수정</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>열 삭제</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 트레이스 가능성 미상</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>준비되지 않음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>준비되지 않음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>준비되지 않음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>준비되지 않음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>길이 :  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> 글자</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>길이:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> 글자</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>지금</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>시간 입력 : hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>모두 선택 해제</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>바이트 %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation> 비트</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>서비스 이름</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>업스트림</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>다운스트림</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>양방향</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>서비스 종류</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>프레임 길이나 CIR 로의 변경으로 인해 서비스 종류가 데이터로 리셋되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>샘플링 속도 (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># 통화</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># 채널</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>압축</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>VCG 생성</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>기본값 채널 넘버링으로 VCG 멤버 정의 :</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Tx VCG 정의</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Rx VCG 정의</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>페이로드 대역폭 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>멤버의 수</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>분배 &#xA; 멤버</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>VCG 멤버 분배</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>VCG 멤버의 임의 설정 분포 정의</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>인스턴스</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>스텝</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>VCG 멤버 편집</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>주소 포맷</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>새 멤버</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>기본값</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>연결된 스트림을 익스플로러에서 가져왔습니다 . &#xA; 연결하려면 [ 스트림 연결… ] 버튼을 누르세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>출발지 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>목적지 IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>주소록</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>선택하지 않음</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>다음 찾기</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>제거</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>선택하지 않음</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>출발지 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>목적지 IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>스트림 연결…</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>추가</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>입력한 IP 가 주소록에 추가됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>연결하기</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>이미 연결됨</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>출발지 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>연결</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>스트림 나가기</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>소스 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>목적지 IP 주소</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>스트림 나가기…</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>모두 선택 해제</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>빠른 설정</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>주의 : 이것은 현재 프레임 설정을 무효로 할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>빠른 (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>전체 (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>집중도</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>가족</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>캡슐화</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>PBit 증가</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (hex)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>사용자 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>적용 중</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>사용자 우선순위 시작 인덱스</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 사용자 우선순위</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>로딩…</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>긴 날짜 :</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>짧은 날짜 :</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>긴 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>짧은 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>숫자 :</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>할당되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>할당됨</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>보류10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>반전11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reserved01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reserved00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>저장…</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>기본값로 테스트 리셋</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>스트림 &#xA; 구성…...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>분배 로딩</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>선택 &#xA; 전체</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>삭제 &#xA; 전체</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>자동 &#xA; 분배</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>스트림</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>프레임률</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>라인 율의 %</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>램프 시작</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>버스트</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>일정</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>최대 Util 한계</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>총 (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>총 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>총 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>총 (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>주의 : 이것은</translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>최소 하나의 스트림을 사용해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>최대 이용률 한계는 </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>최대 가능 로드는 </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>지정된 최대 로드는 이것을 초과해서는 안 됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>퍼센트 입력 :  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>프레임 전송률 입력:  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>비트 전송률 입력 :  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>주의 :&#xA; 비트 전송률이 발견되지 않음 . 취소를 누르고 &#xA; 비트 전송률이 발견되었을 때 다시 시도하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>트리플 플레이 서비스 정의</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>샘플링 &#xA; 속도 (kbps)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># 통화</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>통화 당 &#xA; 속도 (kpbs)</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>총 속도 &#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>총 기본 프레임 &#xA; 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>묵음 압축</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>지터 버퍼 (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># 채널</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>압축</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>총 기본 프레임 크기 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>시작 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>로드 종류</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>시간 간격 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>로드 간격 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>총 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>모의</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>데이터 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>데이터 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>데이터 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>데이터 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>랜덤</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>음성</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>비디오</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>데이터</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>주의 :</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>최소 하나의 서비스를 사용해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>최대 가능 로드는 </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>지정된 최대 로드는 이것을 초과해서는 안 됩니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>분배 : </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>대역폭 : </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>구조 : </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>기본값</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>스텝</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>주소록</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>새로운 내용</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>소스 IP (0.0.0.0 = Any)</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>필수</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>이름 ( 최대 16 자 )</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>내용 추가</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>가져오기 / 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>가져오기</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>USB 드라이브로 내용 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>내보내기</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>USB 드라이브로 내용 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>전체 삭제</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>저장하고 닫기</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>옵션</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>USB 에서 엔트라 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>내용 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>콤마로 분리됨 (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>내용 추가 중</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>각 내용는 필드가 4 개여야 합니다 : &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> 콤마로 분리합니다 .  라인은 건너뜁니다 .</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>중단</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>계속</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>은 유효한 소스 IP 주소가 아닙니다 .  라인은 건너뜁니다 .</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>은 유효한 목적지 IP 주소가 아닙니다 .  라인은 건너뜁니다 .</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>은 유효한 PID 가 아닙니다 .  라인은 건너뜁니다 .</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>내용는 이름이 있어야 합니다 ( 최대 16 자 ).  라인은 건너뜁니다 .</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>내용 이름은 16 자보다 길지 않아야 합니다 .  라인은 건너뜁니다 .</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>동일한 소스 IP, 목적지 IP 와 PID 를 가진 내용가 &#xA; 존재하며 이름을 가지고 있습니다</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>기존의 내용의 이름을 덮어쓰기 &#xA; 하거나 &#xA; 이 아이템을 가져오기를 건너뛰기 할 것입니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>덮어쓰기</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>가져온 내용 중 하나가 PID 값을 가지고 있습니다 .&#xA;PID 값이 있는 내용는 MPTS 애플리케이션에서만 사용됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>USB 로 내용 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>내용 내보내기</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>IPTV_Address_Book</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>이미 사용 중 . 다른 이름을 선택하거나 기존의 내용를 편집하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>이러한 파라미터를 가지고 있는 내용가 이미 존재합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>모든 내용를 삭제하시겠습니까 ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>내용 추가 중</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>동일한 이름의 내용가 이미 존재합니다 .</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>무엇을 하시겠습니까 ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>날짜 : </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>시간 : </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>일</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 이벤트 데이터를 보려면 , 메뉴에서 보기 -> 결과 창 -> 싱글을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>켜짐</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>코드</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Chan</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>목적지</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>출발지</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>경로</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 K1/K2 바이트 데이터를 보려면 , 메뉴에서 보기 -> 결과 창 -> 싱글을 선택하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>바이너리</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 편도 지연 이벤트 데이터를 보려면 , 메뉴에서 보기 -> 결과 윈도우 -> 싱글을 선택하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>소스 손실</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>B1 에러</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>REI-L 에러</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>MS-REI 에러</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>B2 에러</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>REI-P 에러</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>B2 에러</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>HP-REI 에러</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>B3 에러</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>REI-V 에러</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>BIP-V 에러</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>B3 에러</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>LP-REI 에러</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>LP-BIP 에러</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>STL FAS 에러</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Bit/TSE 에러</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>신호 손실</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Frm Syn 손실</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Frm Wd 에러</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>FAS 에러</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>최장</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>최단</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>가장 최근</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>평균</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>단절</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 서비스 단절 데이터를 보려면 , 메뉴에서 보기 -> 결과 윈도우 -> 싱글을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>총</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>초과</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD 번호</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 서비스 단절 데이터를 보려면 , 메뉴에서 보기 -> 결과 윈도우 -> 싱글을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>통과</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>초과</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>START</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOP</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD 번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>초과</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 통화 결과 데이터를 보려면 , 메뉴에서 보기 -> 결과 윈도우 -> 싱글을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>다이얼 톤</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  TRUE</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>무효</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>날짜 : </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>시간 : </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>일</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Prog 이름</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>Strm IP:Port</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Strm 이름</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>저장된 임의 설정 결과 분류 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>저장된 임의 설정 결과 분류</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>내보내기</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>USB 플래시 드라이브를 찾을 수 없습니다 .&#xA; 플래시 드라이브를 삽입하거나 , 일단 제거한 후 다시 삽입하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>PTP 데이터를 USB 로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>PTP 데이터 파일 (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>보고서를 USB 로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>텍스트 (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>스크린샷을 USB 로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>저장된 스크린샷 (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>선택한 파일을 다른 이름으로 압축 :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>파일명 입력 : 최대 60 자</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>압축 &#xA;&amp;&amp; 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>내보내기</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>압축 파일의 이름을 입력하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>USB 플래시 드라이브를 찾을 수 없습니다 .&#xA; 플래시 드라이브를 삽입하거나 , 일단 제거한 후 다시 삽입하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>파일을 압축 할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>TIE 데이터를 USB 로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Wander TIE 데이터 파일 (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>타이밍 데이터를 USB로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>모든 타이밍 데이터 파일 (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>파일 종류 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>열기</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>저장된 임의 설정 결과 분류를 USB 에서 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>저장된 임의 설정 결과 분류</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>가져오기 &#xA; 임의 설정 분류</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>보고서 로고를 USB 에서 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>이미지 파일 (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>가져오기 &#xA; 로고</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>퀵 카드를 USB 에서 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Pdf 파일 (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>퀵 카드 &#xA; 가져오기</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>가져오기 &#xA; 테스트</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>압축풀기 &#xA;&amp;&amp; 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>에러 - 파일 중 하나의 압축을 풀 수 없음 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>대상 장치에 빈 공간이 충분하지 않습니다 .&#xA; 복사 작업이 취소되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>파일 복사 중…</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>완료 . 파일이 복사되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>에러 : 다음의 아이템을 복사하는데 실패했습니다 : &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>PTP 데이터 저장</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>PTP 파일 (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>복사</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>디스크 공간이 충분하지 않습니다. 저장된 다른 파일을 삭제하거나 USB로 이동하십시오.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%2 의 %1 프리</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>선택된 파일 없음</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>선택된 파일에서 총 %1</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>%2 선택된 파일에서 총 %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>파일 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>파일명 입력 : 최대 60 자</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>이 폴더 안의 모든 파일을 삭제하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>( 읽기 전용 파일은 삭제되지 않습니다 .)</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>파일 삭제 중…</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>확실히 삭제하고 싶으십니까 ?&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>이 아이템을 삭제하시겠습니까 ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>추천 광모듈이 아님</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>이더넷</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>파이버 채널</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>신호 구조 발견 및 스캔 결과를 표시합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>정렬 :</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>정렬</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>재정렬</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>선택한 채널을 분석하기 위해 사용되는 사용 가능한 테스트 목록을 표시합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>테스트 시작</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>기다려 주세요 .. 선택한 채널 구성 중…</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>세부사항 보기</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>세부사항 감추기</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>범위 : %1 에서 %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>찾기</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>원본</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>폭 맞추기</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>높이 맞추기</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>듀얼 테스트 보기 선택</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>기타...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(계속)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>경고</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>로그 버퍼가 가득 참</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>캡쳐가 정지됨</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>열 편집</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>바이트 선택 :</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>스크린샷을 캡쳐할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>디스크 공간이 충분하지 않음</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>캡쳐된 스크린샷 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>스트림에 대해서</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>DP 다이얼</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>MF 다이얼</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>DTMF 다이얼</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>보류된된</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Unidir</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>휴지</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>추가 트래픽</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>총</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>아무것도 사용할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>외부 링크로 이동할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>발견되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>페이지 끝에 도착함 , 위에서부터 계속됨</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>도구 선택</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>스크립트를 시작하기 전에 자동 보고서 OFF.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>이전에 해제된 자동 보고서 ON.</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>생성</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>기본값</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>전체 삭제</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>전송</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>재시도</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>보기</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS Sink</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS ( 소스 )</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>컨테이너</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>채널</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>신호 라벨</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>트레이스</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>트레이스 포맷</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>이것은 오직 현재의 레벨만 표시하며 , 더 낮거나 높은 순위의 채널은 고려하지 않습니다 . 오직 현재 선택한 채널만 실시간 업데이트를 받게 될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>채널의 상태가 아이콘으로 표시됩니다 .</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>확인된 경보 없음</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>경보 있음</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>확인된 경보 없이 모니터링</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>경보와 함께 모니터링</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>채널 컨테이너의 이름 . 끝에 ‘ c ’자는 연결된 채널을 표시합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>RFC 4606 이 지정한 채널의 N KLM 번호</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>채널의 신호 라벨</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>가장 최근에 알려진 채널의 상태 .</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>채널이 유효하지 않음 .</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI 있음</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>AIS 있음</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP 있음</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>모니터링</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>아니오</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>상태 업데이트 시간 : </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>결코 없음</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>채널의 트레이스 정보</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>채널의 트레이스 포맷 정보</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>지원되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>스캐닝 중</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>경보</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>무효</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>포맷되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>싱글 바이트</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>CR/LF 종료됨</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>페어링된 장치</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>발견된 장치</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>CA Cert</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>클라이언트 Cert</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>클라이언트 키</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>포맷</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>빈 공간</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>전체 용량</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>제조사</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>라벨</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>업그레이드 버전</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>설치된 버전</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>분류</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>임의 설정 분류에 제공된 이름을 나열합니다 . 이름을 클릭하여 임의 설정 분류를 작동 / 정지할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>클릭하면 임의 설정 분류의 설정을 허용합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>삭제하기 위해 원하는 분류를 변환하여 임의 설정 분류의 삭제를 허용합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>임의 설정 분류에 주어진 이름 . 이름을 클릭하여 임의 설정 분류를 작동 / 정지할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>설정 아이콘을 클릭하여 임의 설정 분류를 위한 설정 다이얼로그를 실행합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>삭제 아이콘을 클릭하여 삭제를 위한 임의 설정 분류를 표시 또는 표시해제 합니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>보고서 생성</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>텍스트 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>생성</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>포맷 :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>생성 후 보고서 보기</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>에러 – 파일명을 비울 수 없습니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>설정을 수정하면 현재의 결과가 갱신됩니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 이름</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 이름</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>서브넷에 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>링크 액티브를 기다리고 있습니다…</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>DHCP 를 기다리고 있습니다…</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>소스 IP 를 재설정하고 있습니다…</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>모드</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>소스 IP</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 이름</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 이름</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>시스템 이름</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>서브넷에 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>서브넷에 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 이름</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 이름</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>서비스</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>서브넷에 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>서비스</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS 이름</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>발견</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>VLAN 우선순위</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>장치</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>네트워크 IP</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>장치</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS 이름</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>장치</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>IP 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>도메인</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>서버</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>호스트</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>스위치</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>라우터</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>기반 시설</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>코어</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>분배</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>접속</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>발견</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>발견된 IP 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>발견된 NetBIOS 도메인</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>발견된 VLANS</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>발견된 라우터</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>발견된 스위치</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>발견된 호스트</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>발견된 서버</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>네트워크 발견 보고서</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>보고서를 생성할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>디스크 공간이 충분하지 않음</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Viavi 8000 이 생성함 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Viavi 6000 이 생성함 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Viavi 5800 이 생성함 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Viavi 테스트 장치가 생성함 </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>일반</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>디스크가 가득 차 , 모든 그래프가 중지되었습니다 .&#xA; 공간을 확보한 후 테스트를 재시작하세요 .&#xA;&#xA; 또는 , 메뉴바의 도구 -> 커스터마이즈 에서 &#xA; 그래프 기능을 해제할 수 있습니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>시간 스케일을 중앙 정렬하기 위해 누르세요</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>그래프 특성</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>평균</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>최대</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>윈도우</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>포화 윈도우</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>플롯 데이터 저장</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>대상 장치에 빈 공간이 충분하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>USB 플래시 장치가 삽입되어 있다면 USB 로 직접 내보내기할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>그래프 데이터 저장 중 . 이 작업은 시간이 걸릴 수 있습니다 . 기다려 주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>그래프 데이터 저장 중</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>장치에 빈 공간이 충분하지 않습니다 . 내보낸 그래프 데이터가 완전하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>그래프 데이터를 내보내기할 때 에러가 발생했습니다. 데이터가 불완전할 수 있습니다.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>스케일</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 일</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 시간</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 시간</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 분</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 분</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 초</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Plot_Data</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>확대하기 위해 클릭해서 드래그하세요</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>포화</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>윈도우</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Conn.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>번호</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>출발지 IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>메시지</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>소스 포트</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>목적지 포트</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>삭제…</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>확인…</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>선택한 범주가 현재 실행되는 모든 테스트에서 제거될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>선택한 아이템을 삭제하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>신규...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>새로운 임의 설정 결과 분류를 설정하기 위해 대화창을 엽니다 .</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>이것을 누르면 장치에서 삭제할 임의 설정 분류를 표시할 수 있습니다 . 선택이 끝난 후에 파일을 삭제하기 위해 버튼을 다시 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>시작하기 위해 &#xA;%1 을 누르세요</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>임의 설정 결과 분류 설정</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>'*' 로 표시된 선택 결과는 현재 테스트 설정에는 적용되지 않으며 , 결과 창에 나타나지 않을 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>분류 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>임의 설정 분류 이름 입력 : 최대 %1 자</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>파일 이름 :</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>해당 없음</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>다른 이름으로 저장</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>새로 저장</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>분류 %2 를 포함하는 &#xA; 파일 %1</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>선택한 결과 : </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   ( 최대 선택 </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>설정…</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>요약</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>요약</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>최대 (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>값 (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>toolButton 홈 도움말…</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>toolButton 뒤로 도움말…</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>toolButton 앞으로 도움말…</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>toolButton 종료 도움말…</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>통화 히스토리 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>toolButton 홈 도움말…</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>toolButton 뒤로 도움말…</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>toolButton 앞으로 도움말…</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>toolButton 종료 도움말…</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>피크 - 피크</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos- 피크</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg- 피크</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>일</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>일</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>피크 - 피크</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos- 피크</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg- 피크</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>대기 시간 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>갯수</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>결과 &#xA; 사용할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>상태 : 통과</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>상태 : 실패</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>상태 : N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>보기 필터 옵션으로 확장</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation>발견된 MEP 의 #</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>피어로 설정</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>화면 필터링</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>필터 조건</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>필터 값 입력 : 최대 %1 글자</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>CCM 종류</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>CCM 속도</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>피어 MEP Id</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>관리 도메인 레벨 </translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>도메인 ID 지정</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>관리 도메인 ID</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>관리 연관 ID</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>테스트 세트가 구성되었습니다 . 강조된 열은 이 테스트 세트를 위한 피어 MEP 로 설정되었습니다 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>강조된 피어 MEP 가 실패하였으므로 테스트 세트를 설정합니다 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>%1 을 %2 로 설정할 수 없습니다 .&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>결과 &#xA; 사용할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>그래프 기능 &#xA; 비활성화</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>전체 스크린으로 보기 위해 이 결과 윈도우를 전환합니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>사용할 수 있는 &#xA; 결과가 없습니다</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>임의 설정</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>전체</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 바이트 프레임 손실 테스트 그래프</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 바이트 업스트림 프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 바이트 다운스트림 프레임 손실 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 바이트 버퍼 크레딧 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 바이트 업스트림 버퍼 크레딧 처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 바이트 다운스트림 버퍼 크레딧 처리량 테스트 결과</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>텍스트 파일 내보내기 ...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>로 내보낸 로그</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>스트림 &#xA; 상세</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>이 윈도우에서 모든 결과 트리 축약</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>이 윈도우에서 모든 결과 트리 확장</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>결과 &#xA; 분류 &#xA; 가 비어있음</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>결과 &#xA; 사용할 수 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>결과 &#xA; 사용할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>모든 요약 &#xA; 결과 &#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>행</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>행 보기</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>모두 선택 해제</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>에러만 보기</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>행…</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>에러 &#xA; 열만 &#xA; 보기</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>에러가 발생한 열만 보기</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>트래픽률 (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>트래픽률 (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation># 분석된 스트림</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>트래픽 분류</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>총 링크</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>표시된 스트림 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>추가 스트림 > 128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>행…</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>정지됨</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>홉</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>지연 시간 (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>더 많은 트레이스라우트 데이터를 보려면 , 메뉴에서 보기 -> 결과 윈도우 -> 싱글을 선택하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>이상적 전송 시간</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>실제 전송 시간</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>그룹 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>스트림</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>프로그램</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>패킷 손실</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>패킷 지터</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>거리 에러</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>기간 에러</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Sync 바이트 에러</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>에러가 발생한 프로그램만 보기</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>행…</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>총 Prog. Mbps</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>에러가 &#xA; 발생한 프로그램만 보기</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>에러가 발생한 프로그램만 보기</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>에러 &#xA; 스트림만 &#xA; 보기</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>에러가 발생한 스트림만 보기</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>분석</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>이름</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation># 분석된 &#xA; 스트림</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>총 &#xA;L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>IP Chksum&#xA; 에러</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>UDP Chksum&#xA; 에러</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>분석장치 &#xA; 시작</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>행…</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>기다려주세요 .. 분석장치 애플리케이션 실행 중 ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>일</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>일</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>스크립트를 선택하세요 .. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>스크립트 :</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>상태 :</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>현재 상태</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>정지됨</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>타이머 :</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>스크립트 경과 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>타이머 양</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>출력 :</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>스크립트 종료 시간 :  </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>선택 &#xA; 스크립트</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>스크립트 실행</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>삭제 &#xA; 출력</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>스크립트 정지</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>실행 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>스크립트 경과 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>다른 스크립트를 선택하세요 .. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>에러.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>실행 중 .</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>실행 중 ..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>테스트 목록 커스터마이즈</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>시작 시 결과 보기</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>위로 이동</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>아래로 이동</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>전체 삭제</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>이름바꾸기</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>구분자</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>바로가기 추가</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>저장된 테스트 추가</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>모든 즐겨찾기를 삭제하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>즐겨찾기 목록이 기본입니다 .</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>모든 사용자 즐겨찾기가 삭제되고 목록은 이 장치에 대한 기본 값으로 복구될 것입니다 . 계속하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>구성 테스트 (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>듀얼 테스트 설정 (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>저장된 테스트 선택</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>기다려주세요...&#xA;빈 테스트 뷰를 실행 중입니다</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>핀 테스트</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>핀 테스트 이름 변경</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>테스트 목록으로 핀</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>테스트 구성 저장</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>테스트 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>표시할 이름 입력</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>이 테스트는 %1 와 같습니다</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>이것은 테스트 애플리케이션을 실행시키는 바로가기입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>설명 : %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>이것은 지정된 테스트 설정입니다 .</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>파일 이름 : %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>교체</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>테스트를 실행할 수 없습니다 ...&#xA; 옵션이 만기되었습니다 .&#xA; 나가서 시스템 페이지에서 BERT 를 재실행하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>이 테스트는 지금 바로 실행될 수 없습니다 . 현재 하드웨어 설정이 지원하지 않거나 더 많은 리소스가 필요한 것 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>다른 탭에서 실행되는 테스트를 제거해 보시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>이 테스트는 다른 포트에서 실행되고 있습니다 .   그 테스트로 가시겠습니까 ? ( 탭 %1 상 )</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>또 다른 테스트 ( 탭 %1) 가 선택한 테스트로 재구성될 수 있습니다 .   그 테스트를 재구성하시겠습니까 ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>목록이 비어 있음 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>테스트를 실행할 수 없습니다 ...&#xA; 광학 지터 기능 OFF.&#xA; 홈 / 시스템 페이지에서 광학 지터 기능을 실행하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>테스트를 실행할 수 없습니다… &#xA; 전원 리소스를 사용할 수 없습니다 .&#xA; 기존의 테스트를 제거 / 재설정하세요 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>테스트를 실행할 수 없습니다… &#xA; 전원 리소스를 사용할 수 없습니다 .&#xA; 다른 모듈을 선택 해제하거나 &#xA; 기존의 테스트를 제거 / 재설정하세요 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>기다려 주세요… &#xA; 테스트 추가 중 </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>기다려 주세요… &#xA; 테스트 재설정 중 </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>테스트를 실행할 수 없습니다… &#xA; 필요한 리소스가 사용 중인 것 같습니다 .&#xA;&#xA; 기술 지원팀에 연락하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>UI 객체 구성 중</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>애플리케이션 모듈과 UI 동기화 중</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>UI 보기 초기화 중</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>뒤로</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>선택테스트</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Viavi 솔루션 저작권 소유</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>장치 정보</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>옵션</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>저장된 파일</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>사용자 인터페이스 접속 모드</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>리모컨을 사용하고 있습니다 .&#xA;&#xA; 읽기 전용 접속 모드로 인해 사용자가 리모컨의 작동에 &#xA; 영향을 줄 수 있는 설정을 변경할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>접속 모드</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>PIM 구성 변경으로 MSAM 이 리셋 되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>PIM 이 삽입되거나 제거되었습니다 . PIM 을 교체하고 있다면 , 지금 계속할 수 있습니다 .&#xA;MSAM 은 지금 재시작될 것입니다 . 이 작업은 최대 2 분 정도 걸릴 것입니다 . 기다려 주세요…</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>BERT 모듈이 너무 과열되게 실행되어 &#xA; 내부 온도가 계속 상승하면 자동적으로 &#xA; 종료될 것입니다 . 데이터를 저장하고 , BERT&#xA; 모듈을 종료한 후 기술 지원을 요청하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>과열로 인해 BERT 모듈이 강제적으로 종료되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>XFP PIM 이 잘못된 슬롯에 있습니다 . XFP PIM 을 포트 #1 로 옮겨주시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>BERT 모듈 XFP PIM 은 잘못된 슬롯에 있습니다 .&#xA; XFP PIM 을 포트 #1 로 이동하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>전기 테스트를 선택하셨지만 , 선택한 SFP 는 광학 SFP 인 것 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>전기 SFP 를 사용하고 있는지 확인하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>BERT 모듈이 애플리케이션 %1 에서 에러 가능성을 발견했습니다 .&#xA;&#xA; 전기 테스트를 선택하였지만 이 SFP 는 광학 SFP 인 것 같습니다 .  교체하거나 다른 SFP 를 선택하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>광학 테스트를 선택하셨지만 , 선택한 SFP 는 전기 SFP 인 것 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>광학 SFP 를 사용하고 있는지 확인하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>테스트 세트가 애플리케이션 %1 에서 에러 가능성을 발견했습니다 .&#xA;&#xA; 광학 테스트를 선택하였지만 이 SFP 는 &#xA; 전기 SFP 인 것 같습니다 .  교체하거나 다른 SFP 를 선택하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>10G 테스트를 선택하셨지만 , 삽입된 트랜시버는 SFP+ 가 아닌 것 같습니다 . </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>SFP+ 를 사용하고 있는지 확인하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>테스트 세트는 에러 가능성을 발견했습니다 .  이 테스트는 SFP+ 가 필요하지만 , 트랜시버는 그것처럼 보이지 않습니다 . SFP+ 로 교체하시기 바랍니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>자동 보고서 설정</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>같은 파일 덮어쓰기</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoReport</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>경고 :    선택한 드라이브가 가득 찼습니다 . 공간을 수동으로 확보하시거나 , 가장 오래된 보고서 5 개를 &#xA; 자동적으로 삭제되게 할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>자동 보고서 활성</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>보고 기간</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>분:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>최대:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>보고서 생성 후 재시작 테스트</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>모드</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>별도 파일 생성</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>보고서 이름</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>이름에 자동적으로 생성 날짜와 시간 추가</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>포맷 :</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>텍스트</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>읽기 전용 접속 모드에서는 자동 보고서가 지원되지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>자동 보고서는 하드디스크에 저장될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>자동 보고서는 하드디스크가 필요합니다 .  이 장치에는 설치되지 않은 것 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>자동 스크립트가 실행 중일 때는 자동 보고서를 활성화할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>자동 보고서 생성 중</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>준비 중…</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>생성 중 </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>이전 보고서 삭제 중…</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>완료 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** 전원이 충분하지 않음 .  다른 모듈을 분리하세요 ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>시리얼 연결이 성공적임</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>애플리케이션 업그레이드가 있는지 확인중</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>애플리케이션이 통신 준비되었음</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** 커널 업그레이드에서 에러 발생 *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** 이더넷 보안 = 표준을 확인하거나 BERT 소프트웨어를 재설치 하세요 *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** 애플리케이션 업그레이드에서 에러 발생 .  모듈 소프트웨어를 재설치하세요 ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** 업그레이드에서 에러 발생 . 전원이 충분하지 않음 . ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** 시작 에러 : BERT 모듈을 비활성화한 후 다시 활성화하세요 ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>보기</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>보고서</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>도구</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>보고서 생성…</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>자동 보고서 ...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>측정 시작</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>측정 종료</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>커스터마이즈…</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>접속 모드…</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>기본값로 테스트 리셋</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>히스토리 삭제</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>스크립트 실행…</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>VT100 에뮬레이션</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>모뎀 설정…</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>기본값 레이아웃 복구</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>결과 윈도우</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>한번</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>왼쪽 / 오른쪽 분리</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>위 / 아래 분리</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>2 x 2 그리드</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>아래 연결</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>왼쪽 연결</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>결과만 보기</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>테스트 상태</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>설정 패널</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>액션 패널</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>스크립트 선택</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>스크립트 파일 (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>선택 &#xA; 스크립트</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>신호 연결</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>보고서 생성</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>텍스트 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>선택 &#xA; 컨텐츠</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>생성</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>포맷 :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>생성 후 보고서 보기</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>에러 – 파일명을 비울 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>다음을 위한 컨텐츠 선택</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>포맷</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>파일 이름</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>선택 ...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>생성 후 보고서 보기</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>메시지 로그 포함</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>보고서 &#xA; 생성</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>보고서 &#xA; 보기</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>텍스트 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>에러 – 파일명을 비울 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>보고서 저장됨</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>신호를 감소시키세요 .</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>이벤트 로그와 막대 그래프가 가득 찼습니다 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>K1/K2 로그가 가득 찼습니다 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>로그가 가득 찼습니다 .&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>이런 종류의 아이템을 추가적으로 &#xA; 로깅하지 않고 테스트가 계속될 것입니다 .  재시작 &#xA; 테스트는 모든 로그와 막대 그래프를 삭제할 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>광학 &#xA; 리셋</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>테스트 종료</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>리시버 오버로드</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>로그가 가득 찼습니다</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>주의</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>실행되는 테스트 없음</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>사용자 인터페이스의 모양과 느낌 커스터마이즈</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>테스트 저장</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>듀얼 테스트 저장</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>테스트 불러오기</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>모든 파일 (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>저장된 테스트 (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>저장된 듀얼 테스트 (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>설정 불러오기</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>듀얼 테스트 불러오기</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>USB 에서 저장된 테스트 가져오기</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>모든 파일 (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>저장된 전형적인 RFC 테스트 설정 (*.classic_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>저장된 RFC 테스트 설정 (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>저장된 FC 테스트 설정 (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>저장된 TrueSpeed 설정 (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>저장된 TrueSpeed VNF 설정 (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>저장된 SAMComplete 설정 (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>저장된 SAMComplete 설정 (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>저장된 OTN 체크 구성 (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>저장된 광학 셀프테스트 구성 (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>저장된 CPRI 확인 구성 (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>저장된 PTP 확인 구성 (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>저장된 압축 파일 (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>저장된 테스트를 USB 로 보내기</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>모든 파일 (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>사용자 저장 멀티 테스트</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>사용자 저장 테스트</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>리모컨을 사용하고 있습니다 . &#xA;&#xA; 이 작업은 읽기 전용 접속 모드에서는 사용할 수 없습니다 .&#xA; 전체 접속을 활성화하기 위해 도구 -> 접속 모드를 사용하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>BERT 모듈 상의 옵션이 만기되었습니다 .&#xA; 나가서 시스템 페이지에서 BERT 를 재실행하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>숨기기</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>테스트 모두 재시작</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>재시작</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>도구:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>작업</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>테스트를 위해 최대화된 결과 창 : </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>전체 보기</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>테스트 추가</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>사용자 매뉴얼</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>권장 광학</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>주파수 그리드</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>이것은 무엇입니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>듀얼 테스트 설정을 다른 이름으로 저장…</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>듀얼 테스트 설정 로딩…</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>보기</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>테스트 선택 변경…</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>전체 테스트 보기로 가기</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>보고서</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>듀얼 테스트 보고서 생성…</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>보고서 보기…</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>도움말</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>경고</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>파일 저장 중</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>새 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>새로운 파일명 입력</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>이름바꾸기</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>이 이름으로 된 파일이 존재하기 때문에 이름을 바꿀 수 없습니다 .&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>재시작</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>사용자 정보 편집</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>선택한 것 없음…</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>최대 글자 수 : </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>로고 선택…</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>미리보기 할 수 없음 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>도표 도움말</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>사용자 매뉴얼</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>뒤로</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>홈</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>앞으로</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>QuickLaunch</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** 지터 업그레이드에서 에러 발생 .  모듈 소프트웨어를 재설치하세요 ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** Optical 지터 기능에서 에러 발생 . 전원이 충분하지 않음 . ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>Optical 지터 기능 작동 중</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 프로파일 (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>프로파일 선택</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>모두 선택</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>모두 선택 해제</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>주의: "연결" 프로파일을 로딩하면 필요한 경우 통신 채널을 원격 장치로 연결합니다.</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>전체 삭제</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>호환되지 않는 프로파일</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>프로파일 &#xA; 로딩</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>확실히 %1 를 삭제하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>%1 프로파일에 있는 모든 것을 삭제하고 싶으십니까 ?&#xA;&#xA; 이 작업은 취소할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>( 읽기 전용 파일은 삭제되지 않습니다 .)</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>다음으로부터 프로파일을 로딩하는데 실패했습니다 :</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>다음으로부터 프로파일이 로딩 됨 :</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>일부 설정은 이 애플리케이션에 없기 때문에 올바르게 로딩되지 않았습니다.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>다음으로부터 성공적으로 프로파일이 로딩 됨 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>듀얼 테스트 활성</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>애플리케이션 인덱스 중</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>옵션 확인 중</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>설치된 하드웨어나 옵션을 위해 사용할 수 있는 테스트가 없음</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>테스트를 실행할 수 없습니다 . 설치된 옵션과 현재 하드웨어 설정을 확인하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>전원 차단 시 작동하던 애플리케이션 복구</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>애플리케이션 작동 중</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>내부 USB 플래시를 설치할 수 없음 .&#xA;&#xA; 장치가 배터리 옆에 정확하게 삽입되었는지 확인하세요 . 일단 삽입한 후 BERT 모듈을 재시작하세요 .</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>내부 USB 플래시 파일 시스템에 오류가 있는 것으로 보입니다 . 도움을 위해 기술 지원팀에 연락하세요 .</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>내부 USB 플래시 용량이 권장 크기인 1G 보다 작습니다 .&#xA;&#xA; 장치가 배터리 옆에 정확하게 삽입되었는지 확인하세요 . 일단 삽입한 후 장치를 재시작하세요 .</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>설정을 복구하는 동안 에러가 발생했습니다 . &#xA; 다시 시도하십시오 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>저장된 테스트를 로딩하기 전에 실행 중인 모든 테스트를 종료할 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>기다려 주세요… &#xA; 저장된 테스트 로딩 중</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>테스트 문서명이나 파일 경로가 유효하지 않습니다 .&#xA; 문서의 위치를 찾기 위해 저장된 테스트 로딩을 사용하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>테스트를 복구하는 중에 에러가 발생했습니다 .&#xA; 다시 시도하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>배터리 충전기가 작동하고 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>배터리 충전기가 이 모드에서 작동되지 않을 것입니다 . 적절한 테스트가 선택되면 충전기는 자동으로 활성화될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>이 모듈에서 리모컨이 사용되고 있습니다 &#xA; 화면의 작동이 중지되었습니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>메시지 로그됨 . 보려면 클릭하세요…</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>%1 에 대한 메시지 로그</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>메시지 없음</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 메시지</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 메시지</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>%1 에 대한 메시지 로그</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>메시지 로그</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>모뎀 설정</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>이 서버의 주소를 위한 IP 와 다이얼인 클라이언트에 지정할 IP 를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>서버 IP</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>클라이언트 IP</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>현재 위치 ( 국가 코드 )</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>모뎀을 탐지할 수 없음 . 모뎀이 정확하게 삽입되었는지 확인하고 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>장치가 통화 중입니다 . 모든 다이얼인 세션의 연결을 끊고 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>모뎀을 업데이트할 수 없음 . 모든 다이얼인 세션의 연결을 끊고 다시 시도하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>시간 변경으로 테스트 재시작 중 .</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>새 옵션 키를 설치하기 위해 아래에 입력하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>옵션 키</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>설치</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>가져오기</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>소프트웨어 옵션을 구입하기 위해 Viavi 에 연락하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>옵션 챌린지 ID:</translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>키가 승인되었습니다 ! 새로운 옵션을 활성화하기 위해 재부팅하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>거절 키 - 만기되는 옵션 슬롯이 가득 찼습니다.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>거절 키 - 만기되는 옵션이 이미 설치되었습니다.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>유효하지 않은 키 – 다시 시도하세요 .</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%2 키 중 %1 이 승인되었습니다 ! 새로운 옵션을 활성화하기 위해 재부팅하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>USB 플래시 드라이브에서 '%1' 를 열 수 없습니다 .</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>옵션 정보를 얻는데 문제가 있었습니다…</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>선택한 바이트</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>기본값</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>범례</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>범례 :</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI ( 사용하지 않음 )</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>보류된된</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>BERT 모듈에 대하여</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>전송 모듈</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>퀵 카드 가져오기</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>퀵 카드 가져오기</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> 메뉴 숨기기</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> 모든 테스트</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>사용자 임의 설정</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>현재 테스트가 시작하기 전에 선택한 스크린샷이 &#xA; 하나 이상 캡쳐되었습니다 .  정확한 파일을 선택하였는지 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>선택 &#xA; 전체</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>해제 &#xA; 전체</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>선택 &#xA; 스크린샷</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>해제 &#xA; 스크린샷</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC 교정</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>교정</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>교정 중...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>교정이 완료되지 않았습니다. 교정은 RS-FEC를 사용해야 합니다.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(교정은 설정 페이지의 RS-FEC 탭에서 실행할 수 있습니다.)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>교정 재시도</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>교정 나가기</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>교정 미완료</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC는 일반적으로 SR4, PSM, CWDM4와 함께 사용합니다.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>RS-FEC 교정을 실행하려면 다음과 같이 하십시오. (CFP4에도 적용됩니다):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>QSFP28 어댑터를 CSAM에 삽입하기</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>QSFP28 트랜시버를 어댑터에 삽입하기</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>파이버 루프백 장치를 트랜시버에 삽입하기</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>교정 클릭</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>상태</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>리싱크</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>지금 트랜시버를 테스트 중인 장치(DUT)와 리싱크해야 합니다.</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>파이버 루프백 장치를 트랜시버에서 제거하기</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>테스트 중인 장치(DUT)로 연결하기 </translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>DUT 레이저를 ON합니다. </translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>CSAM의 신호 확인 LED가 초록색인지 확인합니다.</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>레인 리싱크 클릭</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>리싱크가 완료되었습니다. 대화창이 지금 닫힐 것입니다.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>읽기 전용으로 저장</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>테스트 목록으로 핀</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>파일 이름</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>선택 ...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>읽기 전용으로 저장</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>프로파일 &#xA; 저장</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 프로파일 (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>프로파일 </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>는 읽기 전용 파일입니다 . 교체할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>프로파일 저장됨</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>로고 선택</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>이미지 파일 (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>파일이 너무 큽니다 . 다른 파일을 선택하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>선택한 바이트</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>기본값</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>범례</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>테스트 &#xA; 시작</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>테스트 &#xA; 정지</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>실행 중</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>정지됨</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>지연</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>전체 접속</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>읽기 전용</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>어두운</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>밝은</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>빠른 실행</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>결과 보기</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>예상되는 총 TCP 처리량 (Mbps) 의 근거 :</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>최대 사용 가능 대역폭 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>평균 왕복 시간 지연 (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>최대 처리량을 달성하기 위해 필요한 병렬 TCP 세션의 수 :</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>비트 전송률</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>윈도우 크기</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>세션</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>예상되는 총 TCP 처리량 (kbps) 의 근거 :</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>최대 사용 가능 대역폭 (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP 처리량</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>테스트 설정을 저장하는 중에 에러가 발생하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>테스트 설정을 로딩하는 중에 에러가 발생하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>선택한 디스크가 가득찼습니다 .&#xA; 몇 개의 파일을 제거하고 다시 저장하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>다음에 저장된 임의 설정 결과 분류 파일이 현재 로딩된 파일과 다릅니다 :</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... 기타 %1 개</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>덮어쓰면 다른 테스트에 영향을 줄 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>계속 덮어 쓰시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>아니오</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>기다려 주세요 ...&#xA; 설정 복구 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>지금은 %1 테스트를 로딩하기에 리소스가 충분하지 않습니다 .&#xA; 현재 설정과 함께 사용할 수 있는 테스트 목록을 보려면 테스트 메뉴를 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>모든 테스트 설정을 복구할 수 없습니다 .&#xA; 파일 컨텐츠가 오래되었거나 오류가 발생했을 가능성이 있습니다 .&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>접속 모드가 읽기 전용입니다 . 변경하기 위해 도구 -> 접속 모드를 사용하세요 .</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>실행되는 테스트 없음</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>이것은 모든 설정을 기본값로 리셋할 것입니다 .&#xA;&#xA; 계속 하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>이것은 테스트를 종료하고 재시작할 것입니다 .&#xA; 테스트 설정은 기본값로 복구될 것입니다 .&#xA;&#xA; 계속 하시겠습니까 ?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>이 워크플로우가 현재 실행 중입니다 . 이것을 종료하고 새로운 것을 시작하시겠습니까 ?&#xA;&#xA; 이전 워크플로우를 계속 실행하려면 취소를 클릭하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>이 워크플로우를 종료하시겠습니까 ?&#xA;&#xA; 계속 실행하려면 취소를 클릭하세요 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>VT100 을 실행할 수 없습니다 . 시리얼 장치가 이미 사용 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>모듈 </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>재시작을 누르면 * 양쪽 * 포트의 결과가 삭제된다는 것을 유의하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>선택테스트</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>테스트 불러오기...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>테스트를 다른 이름으로 저장 ...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>설정만 불러오기 ...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>테스트 추가</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>테스트 제거</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>도움말</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>도표 도움말</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>보고서 보기...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>보고서 내보내기 ...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>사용자 정보 편집...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>보고서 로고 가져오기 ...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>USB 에서 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>저장된 테스트 ...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>저장된 임의 설정 분류…</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>USB 로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>스크린샷...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>타이밍 데이터...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>옵션 리뷰 / 설치…</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>스크린샷 찍기</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>사용자 매뉴얼</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>권장 광학</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>주파수 그리드</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>신호 연결</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>이것은 무엇입니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>퀵 카드</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>기다려 주세요… &#xA; 테스트 제거 중</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>포트 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>포트 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>선택한 것 없음…</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>파일 선택 ...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>USB 로부터 패킷 캡펴 가져오기</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>선택 &#xA; 파일</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>저장된 패킷 캡쳐 (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>보고서 보기</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>모든 파일 (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>텍스트 (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>로그 (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>보기</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>이 보고서&#xA;보기</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>다른 &#xA; 보고서 보기</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>보고서 &#xA; 이름 변경</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> 이미 존재함 .&#xA; 대체하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>보고서 이름 변경됨</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>에러 – 파일명을 비울 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>보고서는 다음으로 저장되었습니다</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>어떤 보고서도 저장되지 않았습니다</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>가십시오</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>TIE 데이터 저장 ...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>종류로 저장 : </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>HRD 파일</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>CHRD 파일</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>저장 중 </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>이 작업은 몇 분 정도 걸릴 수 있습니다…</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>에러 : HRD 파일을 열 수 없음 . 다시 저장하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>취소 중 ...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>TIE 데이터가 저장됨 .</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>에러 : 파일을 저장할 수 없음 . 다시 시도하세요 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>경고</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Wander Analysis 를 닫으면 모든 분석 결과를 잃게 될 것입니다 .&#xA; 분석을 계속하려면 분석 계속을 클릭하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>분석 닫기</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>분석 계속</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Wander Analysis</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>TIE 데이터 &#xA; 업데이트</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>TIE 업데이트 &#xA; 정지</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>MTIE/TDEV&#xA; 계산</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>계산 &#xA; 정지</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>촬영 &#xA; 스크린샷</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>로드 &#xA;TIE 데이터</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>TIE&#xA; 로드 정지</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>분석 &#xA; 닫기</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>로드 TIE 데이터</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>로딩</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>모든 Wander 파일 (*.chrd *.hrd);;Hrd 파일 (*.hrd);;Chrd 파일 (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>직사각형을 정의하려면 두 번 누르세요</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>전체 삭제</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>프로파일 불러오기</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>확실히 %1 를 삭제하고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>%1 프로파일에 있는 모든 것을 삭제하고 싶으십니까 ?&#xA;&#xA; 이 작업은 취소할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>( 읽기 전용 파일은 삭제되지 않습니다 .)</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 프로파일 (%2. %3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>프로파일을 로드할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>로딩 실패</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>다음</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>메시지</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>에러</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>가십시오</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>경고</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>다음은 무엇을 하시겠습니까 ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>테스트에서 나가고 싶으십니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>나가면서 설정 복구</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>결과로 나가기</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>뒤로</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>단계적 :</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>다음</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>가이드 미</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>다른 포트</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>스타트 오버</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>바로 가기 ...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>다른 포트</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>테스트가 시작하고 있습니다 기다려주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>테스트가 종료되고 있습니다. 기다려 주세요...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>메시지 로그</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>메인</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>단계 보기</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>응답 : </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>실행 중</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>선택한 것 없음…</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>보고서 로고</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>로고 선택…</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>미리보기 할 수 없음 .</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>테스트가 시작하고 있습니다 기다려주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>테스트가 종료되고 있습니다. 기다려 주세요...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>테스트가 진행 중입니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>남아 있는 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>실행되는 테스트 없음</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>테스트가 완료되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>스크립트를 시작하기 전에 자동 보고서 OFF.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>이전에 해제된 자동 보고서 ON.</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>메인</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>스크린샷이 저장됨</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>스크린샷이 저장됨 :</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>프로파일 선택</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>작동 레이어</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>저장된 프로파일 불러오기</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>어떻게 TrueSAM 을 구성하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>저장된 프로파일로부터 설정 불러오기</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>가십시오</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>새 프로파일 시작</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>서비스는 어떤 레이어에서 실행됩니까 ?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>레이어 2: MAC 주소를 사용한 테스트, 예 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>레이어 3: IP 주소를 사용한 테스트, 예 192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>기다려주세요 ... 강조된 단계로 진행합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>테스트 선택</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>통신 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>강화 RFC 2544 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>SAMComplete 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>J-Proof 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>TrueSpeed 설정</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>설정 저장</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>테스트</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>보고서 정보 추가</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>선택된 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> 보고서</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>보고서 보기</translation>
        </message>
    </context>
</TS>
