<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi-Mikroskop</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>Momentaufnahme</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>Ansicht 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>Ansicht 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>Ansicht 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>Ansicht 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Ansicht FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Mikroskop</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Erfassen</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Fixieren</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>Bild</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Helligkeit</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>Kontrast</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>Bildschirmlayout</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>Vollbildschirm</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>Geteilt</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Beenden</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Bild speichern</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Bild auswählen</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Bilddateien (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
    </context>
</TS>
