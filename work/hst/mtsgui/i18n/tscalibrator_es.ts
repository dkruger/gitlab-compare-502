<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CEvdevCalibrator</name>
        <message utf8="true">
            <source>Please wait...</source>
            <translation>Espere por favor...</translation>
        </message>
        <message utf8="true">
            <source>Calibration timed out</source>
            <translation>Tiempo de espera de calibración caducó</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CTsCalibrateWindow</name>
        <message utf8="true">
            <source>Touch crosshair to calibrate the touchscreen.</source>
            <translation>Toque el punto de mira para calibrar la pantalla táctil.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CTslibCalibrator</name>
        <message utf8="true">
            <source>Calibration timed out</source>
            <translation>Tiempo de espera de calibración caducó</translation>
        </message>
    </context>
</TS>
