<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>Функция мобильного приложения</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth и WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>Только WiFi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Перезапустите анализатор, чтобы изменения вступили в силу.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Вставить</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Копировать</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Вырезать</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Один</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Неcколько</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Все</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ничего</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Открыть</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Переименовать</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Новая папка</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Диск</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>При вставке существующие файлы будут перезаписаны .</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Переписать файл</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>Мы не можем открыть этот файл из-за неизвестной ошибки.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>Не удается открыть файл:</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>Выбранный файл будет удален без возможности восстановления.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>%1 выбранные файлы будут удалены без возможности восстановления.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Удалить файл</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>Введите имя папки:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Создать папку</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>Невозможно создать папку %1.</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>Папка %1 уже существует .</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Создать папку</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Вставка...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Удаление...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Завершено.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 дал сбой .</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>Не удалось вставить файл.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>Не удалось вставить %1 файлы.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>Недостаточно свободного места .</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>Не удалось удалить файл.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>Не удалось удалить %1 файлы.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Назад</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Опция</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>Обновление URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Название файла</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Действие</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>Модуль</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Другая беспроводная сеть</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Введите ниже информацию о беспроводной сети.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>Название (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Тип шифрования :</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Обновление доступно</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync обнаружил доступное обновление  &#xA;Желаете произвести обновление сейчас ? &#xA;&#xA;Обновление прекращает ход всег запущенных испытаний . В конце процесса тестовая последовательность перезагрузится для завершения обновления .</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Произошла неизвестная ошибка. Повторите действие.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Ошибка обновления</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Информация об устройстве:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>Параметры базы:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Восстановить настройки&#xA;устройства по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Экспортировать журналы &#xA; на USB- накопитель</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Копировать сведения&#xA;о системе в файл</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>Сведения о системе были скопированы&#xA;в этот файл:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Требуется перезагрузка, после чего настройки параметров Система и Испытание будут восстановлены в заданные по умолчанию значения.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отмена</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Перезагрузить и восстановить значения по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Экспортирование журнала выполнено .</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Невозможно экспортировать журналы . Проверьте , вставлен ли USB- накопитель и повторите попытку .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 Версия %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Запуск</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Восстановить настройки по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Выберите способ обновления:</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>Обновление из файлов, сохраненных на флэш-диске USB.</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Сеть</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Загрузить обновление с веб-сервера по сети.</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Введите адрес сервера обновления :</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Адрес сервера</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Введите адрес прокси - сервера , если это необходимо для доступа к серверу обновления :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>Адрес прокси-сервера</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Подключиться</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Запросить сервер о наличии обновлений.</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Назад</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Далее</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Начать обновление</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>Перейти на использование более ранней версии</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>Обновления не обнаружены</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>Ошибка обновления</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>Процедура обновления приведет к завершению всех выполняемых тестов. По окончании процедуры комплект испытаний производит перезагрузку для завершения обновления.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Подтверждение обновления</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>Возврат к более ранней версии тестовой установки возможен, но не рекомендован. Если он все же необходим, обратитесь к Viavi TAC.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>Понижение приведет к прерыванию производимых тестов. В конце процесса испытательная установка перезагрузится для завершения процесса возврата к более ранней версии.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>Возврат к более ранней версии не рекомендуется</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Произошла неизвестная ошибка. Повторите действие.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>Система</translation>
        </message>
    </context>
</TS>
