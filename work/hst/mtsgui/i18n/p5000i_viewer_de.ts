<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi-Mikroskop</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation>TextBenennung</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Ansicht FS</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Beenden</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Fixieren</translation>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation>Vergrößern</translation>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation>Überlappen</translation>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation>Analyse läuft...</translation>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation>Profil</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Von USB importieren</translation>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation>Tipp</translation>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation>Autozentrum</translation>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation>Testknopf:</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation>Standbilder</translation>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation>Andere Einstellungen...</translation>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation>Mikroskop Profil importieren</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation>Profil&#xA;importieren</translation>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation>Mikroskop Profile (*.pro)</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Fixieren</translation>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation>Live</translation>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation>PNG speichern</translation>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation>PDF speichern</translation>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Bild speichern</translation>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation>Bericht speichern</translation>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation>Analyse fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation>Verlauf konnte nicht analysiert werden. Bitte prüfen, dass die Live Videosendungen das Verlaufsende anzeigen und dass es vor dem erneuten Test fokussiert wird.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Bild auswählen</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Bilddateien (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation>Verlaufsüberprüfung und Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation>Verlaufsinformation</translation>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation>Keine zusätzlichen Verlaufsinformationen definiert</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ERFOLG</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FEHLER</translation>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation>Profil:</translation>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation>Tipp:</translation>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation>Überprüfungszusammenfassung</translation>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation>SCHÄDEN</translation>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation>GRUNDLAGEN</translation>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation>oder</translation>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation>Kriterien</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Schwelle</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Anzahl</translation>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation>Ergebnis</translation>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation>jegliche</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation>Generieren Überprüfungszusammenfassung fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation>TIEFE BILDVERGRÖSSERUNG</translation>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation>HOHE BILDVERGRÖSSERUNG</translation>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation>Basistest nicht aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Job:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Kabel:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Anschluss:</translation>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation>Firma:</translation>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation>Techniker:</translation>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation>Kunde:</translation>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation>Standort:</translation>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation>Job:</translation>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation>Kabel:</translation>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation>Anschluss:</translation>
        </message>
    </context>
</TS>
