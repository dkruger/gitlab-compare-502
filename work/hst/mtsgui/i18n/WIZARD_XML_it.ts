<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI Check</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>Configura</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>Modifica configurazione precedente</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>Carica configurazione da un profilo</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>Inizia una nuova configurazione (predefinito)</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>Manualmente</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>Configurare manualmente le impostazioni dei test</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>Impostazioni Test</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>Salva profili</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>Fine: Configura manualmente</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>Esegui i test</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>Immagazzinati</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>Carica profili</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>Fine: Carica profili</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>Modifica configurazione</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Esegui</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>Esegui Controllo CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>Verifica SFP</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>Fine: Test</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crea rapporto</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>Ripeti test</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>Visualizza risultati dettagliati</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>Terminazione Controllo CPRI</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>Revisione</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>Interfaccia</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Layer 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>Fine: Esaminare i risultati</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>Informazioni sul rapporto</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>Fine: Crea rapporto</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>Esamina risultati dettagliati</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>In esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>INCOMPLETO</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>COMPLETO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALLITO</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NESSUNO</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>CPRI Check:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** Avvio Controllo CPRI ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** Test Completato ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** Test Interrotto ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>Salta Salva profili</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>È possibile salvare la configurazione utilizzata per eseguire questo test.&#xA;&#xA;Può essere utilizzata (interamente o selezionando singoli&#xA;"profili") per configurare le prove future.</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>Salta Carica profili</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>Verifica SFP Locale</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Connettore</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>Per favore inserisci un SFP.</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>Lunghezza d'onda SFP (nm)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>Fornitore SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>Rev fornitore SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>P/N Venditore SFP</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>Velocità Raccomandate:</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>Mostra Dati Aggiuntivi SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP buono.</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>Impossibile verificare l'SFP per questa velocità.</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP non accettabile.</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Impostazioni</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Durata del test</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>Dispositivo Remoto</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Altro</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>Loop fisico</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sì</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>Limite Max. Livello Rx Ottico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>Livello Minimo Rx Ottico Limite (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>Limite Max. Round Trip Delay (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>Tralascia Controllo CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>SFP Check</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>Test Status Key</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>Pianificato</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>Esegui&#xA;test</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Arresta&#xA;Test</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>Risultati SFP Locale</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>Nessun SFP rilevato.</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Lunghezza d'onda (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Liv. Tx max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Liv. Rx max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Ricetrasmittente</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>Risultati Interfaccia</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>Risultati Test Controllo CPRI</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>Test Interfaccia</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>Test Layer 2</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>Test RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>Test BERT</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>Segnale presente</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Sinc. acquisita</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Deviazione max frequenza Rx (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violazioni del codice</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>Livello Rx ottico (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>Risultati Layer 2</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>Stato di avvio</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>Frame Sinc</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>Risultati RTD</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>Media Round Trip Delay (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>Risultati BERT</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>Pattern Sync</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>Perdite Pattern</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Errori Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>Configurazoni</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>Tipo di attrezzatura</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>Sincronizzazione L1</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>Impostazione del protocollo</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>Impostazione del Piano C&amp;M</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>Funzionamento</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>Link passivo</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>Salta Creazione rapporto</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informazioni rapporto test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>Nome cliente:</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>ID Tecnico:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>Posizione del test:</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>Ordine di lavoro:</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>Commenti/Note:</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>Radio:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>Banda:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>Stato Complessivo</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Test FC Avanzato</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>Test FC</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connetti</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Simmetria</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>Impostazioni locali</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>Connetti a remoto</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Rete</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>Impostazioni fibra ottica</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>Test FC</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>Modelli di configurazione</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Seleziona i test</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>Utilizzo</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Lunghezze frame</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Test di throughput</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Test perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>Test Back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Test Crediti Buffer</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>Test Ctls</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>Durate dei test</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>Soglie test</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>Cambia configurazione</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>Impostazioni test fibra ottica avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>Impostazioni utilizzo avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>Impostazioni latenza throughput avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>Impostazioni test Back-to-back avanzato</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>Impostazioni test perdita frame avanzate</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>Esegui i Test attivazione servizio</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>Cambia configurazione e riesegui test</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Throughput</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latenza</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to back</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>Prova Exit FC</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>Crea un altro rapporto</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>Pagina di copertina</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>La sincronizzazione sorgente del tempo di ritardo unidirezionale (OWD) è stata persa durante il test sull'unità locale.  Verificare le connessioni dei dispositivi sorgente del tempo di OWD.  Se non è già terminato il test continuerà, tuttavia i risultati del Ritardo frame non saranno disponibili.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>La sincronizzazione sorgente del tempo di ritardo unidirezionale è stata persa durante il test sull'unità remota.  Verificare le connessioni dei dispositivi sorgente del tempo di OWD.  Se non è già terminato il test continuerà, tuttavia i risultati del Ritardo frame non saranno disponibili.</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>Trovato loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>Risoluzione indirizzo adiacente non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>Servizio #1: Invio di richiesta ARP per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>Servizio #1 su unità locale: Invio di richiesta ARP per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>Servizio #1 sul unità remota: Invio di richiesta ARP per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Invio di richiesta ARP per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>Invio di richiesta ARP da unità locale per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>Invio di richiesta ARP da unità remota per MAC di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>La porta dell'elemento della rete è predisposta per il funzionamento half-duplex. Per procedere, premere il pulsante "Prosegui in half-duplex". In caso contrario, premere "Interrompi test".</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>Tentativo di loop. Verificare presenza loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>Verifica presenza loop hardware.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Controllo loop LBM/LBR in corso.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>Verifica presenza loop permanente</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>Richiesta parametro DHCP scaduta. Recupero parametri DHCP non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>La selezione della modalità loopback ha determinato lo scollegamento dall'unità remota.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>SAMComplete ha esaurito il tempo massimo previsto perché non è stato possibile determinare perché non è stato possibile riconoscere un frame finale ricevuto. Il numero di frame ricevuti ha continuato ad aumentare in modo inatteso.</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>Trovato loop fisico</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>Non è possibile eseguire un test di connettività con J-Quickcheck se non in presenza di una connessione attiva con l'unità remota.</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>Trovato loop LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>Il collegamento locale è andato perduto e la connessione con l'unità remota è stata interrotta. Dopo avere ristabilito il collegamento sarà possibile tentare di riconnettersi all'unità remota.</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>Il link non è correntemente attivo.</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>Gli IP sorgente e destinazione sono identici. Riconfigurare l'indirizzo IP di origine o di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione locale è un'applicazione Traffico e l'applicazione remota all'indirizzo IP #1 è un'applicazione Streams.</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>Non è stato possibile stabilire nessun loop.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>La connessione con l'unità remota non è stata eseguita. Accedere alla pagina "Connetti", verificare l'indirizzo IP della destinazione e premere il pulsante "Connetti a remoto".</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Per favore vai alla pagina "Rete", verifica le configurazioni e riprova.</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>In attesa che la fibra ottica sia disponobile o fibra ottica non presente.</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>Trovato loop permanente</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>Timeout connessione PPPoE Controllare l'impostazione del PPPoE e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>È stato riscontrato un errore PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Il tentativo di accesso al server ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>È stato rilevato un problema sulla connessione remota. L'unità remota potrebbe non essere più accessibile</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>Non è stato possibile stabilire un collegamento con l'unità remota all'indirizzo IP #1.  Controllare l'indirizzo IP della sorgente remota e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>L'applicazione remota all'indirizzo IP #1 è un'applicazione di Loopback. Essa non è compatibile con Enhanced RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione remota all'indirizzo IP #1 non è un'applicazione TCP WireSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>La versione del software sull'unità remota sembra essere più recente. Se si continua l'esecuzione del test, alcune funzionalità potrebbero essere limitate. Si consiglia di aggiornare il software di questa unità per prestazioni ottimali.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>La versione del software sull'unità remota sembra essere meno recente. Se si continua l'esecuzione del test, alcune funzionalità potrebbero essere limitate. Si consiglia di aggiornare il software dell'unità remota per prestazioni ottimali.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>Impossibile determinare la versione del software sull'unità remota. Se si continua l'esecuzione del test, alcune funzionalità potrebbero essere limitate. Si consiglia eseguire il test tra unità con versioni equivalenti del software.</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>Il tentativo di loop non è riuscito. Riprovare.</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>Le impostazioni per il modello selezionato sono state correttamente applicate.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>Non è stato possibile determinare la versione del software sull'unità remota oppure essa non corrisponde alla versione di questa unità. Se si continua il test, alcune funzionalità saranno limitate. È consigliabile eseguire il test tra le unità con versioni software equivalenti.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>Impossibile completare l'accesso esplicito.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione locale è un'applicazione Layer #1 e l'applicazione remota all'indirizzo IP #2 è un'applicazione Layer #3.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>La velocità di linea dell'unità locale (#1 Mbps) non corrisponde a quella dell'unità remota (#2 Mbps). Si prega di riconfigurare su Asimmetrico nella sulla pagina "Connetti".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>La velocità di linea dell'unità locale (#1 kbps) non corrisponde a quella dell'unità remota (#2 kbps). Si prega di riconfigurare su Asimmetrico nella sulla pagina "Connetti".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>La velocità di linea dell'unità locale (#1 Mbps) non corrisponde a quella dell'unità remota (#2 Mbps). Riconfigurare ad Asimmetrico sulla pagina Connetti e riavviare il test.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>La velocità di linea dell'unità locale (#1 kbps) non corrisponde a quella dell'unità remota (#2 kbps). Si prega di riconfigurare su Asimmetrico nella sulla pagina "Connetti" e riavviare il test.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>La velocità di linea dell'unità locale (#1 Mbps) non corrisponde a quella dell'unità remota (#2 Mbps). Si prega di riconfigurare su Asimmetrico nella sulla pagina RFC2544 "Simmetria".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>La velocità di linea dell'unità locale (#1 kbps) non corrisponde a quella dell'unità remota (#2 kbps). Si prega di riconfigurare su Asimmetrico nella sulla pagina RFC2544 "Simmetria".</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Deve essere selezionato almeno un test. Selezionare almeno un test e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>Non sono state selezionate dimensioni di frame da sottoporre a test. Selezionare almeno una dimensione di frame prima di iniziare.</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>Nell'ultimo secondo è stata rilevata una frequenza di perdita dei frame che supera la corrispondente soglia configurata. Verificare le prestazioni del collegamento con un test manuale sul traffico. Dopo l'esecuzione dei test manuali è possibile eseguire nuovamente RFC 2544.&#xA;Configurazione raccomandata del test manuale:&#xA;Dimensione frame: #1 bytes&#xA;Traffico: costante con #2 Mbps&#xA;Durata test: almeno 3 volte la durata configurata del test. La durata del test sulla velocità effettiva è stata #3 secondi.&#xA;Risultati da monitorare:&#xA;Usare la schermata di Riepilogo della Stato per cercare casi di errore. Se il contatore degli errori si incrementa in modo sporadico, avviare il test manuale a diverse velocità di traffico. Se si rilevano ancora errori anche a velocità inferiori, il collegamento può essere gravato da problemi generali non correlati al massimo carico. È possibile anche usare la curva di rappresentazione grafica dei risultati relativi alla Frequenza di Perdita dei Frame per vedere se i casi di perdita dei frame sono sporadici o costanti. Se non si riesce a risolvere il problema degli errori sporadici, è possibile impostare la Soglia di Tolleranza della Perdita dei Frame in modo tale da tollerare basse frequenze di perdita dei frame. Nota: una volta utilizzata la Tolleranza alla Perdita dei Frame, il test non sarà conforme alla raccomandazione RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>Nota:  A causa della diversa profondità dello stack VLAN per il trasmettitore e il ricevitore, il tasso configurato sarà regolato per compensare la differenza di velocità tra le porte.</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>Frame da #1 byte</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>Pacch. da #1 byte</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>Il conteggio Filtro Rx Acterna L2 ha continuato ad aumentare per gli ultimi 10 secondi anche sebbene il traffico dovrebbe essersi arrestato</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Azzeramento sulla velocità massima di throughput</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>Tentativo #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>Tentativo #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>Tentativo #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>Tentativo #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>Tentativo in corso #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>Verifica in corso #1 L1 Mbps Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>Verifica in corso #1 L2 Mbps Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>Verifica in corso #1 L1 kbps Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>Verifica in corso #1 L2 kbps Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>Verifica in corso #1 %. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>Throughput massimo misurato: #1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>Throughput massimo misurato: #1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>Throughput massimo misurato: #1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>Throughput massimo misurato: #1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>Throughput massimo misurato: #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Una misura di throughput massimo è Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>Test perdita frame (Standard RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>Test perdita frame (Top Down)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>Test perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>Esecuzione dei test con carico #1 L1 Mbps. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>Esecuzione dei test con carico #1 L2 Mbps. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>Esecuzione dei test con carico #1 L1 kbps. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>Esecuzione dei test con carico #1 L2 kbps. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>Esecuzione dei test con carico #1 %.  Questa operazione richiederà circa #1 secondi</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Test frame back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>Versione di prova #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Individuati frame pausa</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 packet burst: fallito</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 frame burst: fallito</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 packet burst: passato</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 frame burst: passato</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>Test di burst hunt</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>Tentativo di un burst di #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>Maggiore di #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>Meno di #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>La dimensione del buffer è di #1 kB.</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>La dimensione del buffer è inferiore a #1 kB.</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>La dimensione del buffer è maggiore o uguale a #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>#1 frame inviati</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>#1 Frame ricevuti</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>#1 Frame persi</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>Test burst (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>CBS stimato: #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>CBS stimato: Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>Il test CBS verrà ignorato. La dimensione di burst del test è troppo grande per una rilevazione esatta con questa configurazione.</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>Il test CBS verrà ignorato. La durata del test non è abbastanza lunga per una rilevazione esatta con questa configurazione.</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>Packet burst: non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>Frame burst: non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>Packet burst: riuscito</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>frame burst: riuscito</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>Prova Burst Policing #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>Test di Ripristino di sistema</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>Il test non è valido per questa dimensione di pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>Il test non è valido per questa dimensione di frame</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>Tentativo #1 di #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>Non sarà possibile per indurre la perdita di frame in quanto il test di throughput è stato superato con larghezza di banda massima, senza rilevazione di perdita di frame</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>Impossibile indurre gli eventi di perdita. Il test non è valido per questa dimensione di pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>Impossibile indurre gli eventi di perdita. Il test non è valido per questa dimensione di frame</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>Tempo di recupero medio: Maggiore di #1 secondi</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>Maggiore di #1 secondi</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>Tempo di recupero medio: Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>Tempo di recupero medio: #1 µs</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 µs</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>Puntamento su crediti buffer ottimali. Test di #1 crediti in corso</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>Test di #1 crediti in corso</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>Verifica con #1 crediti in corso.  Questa operazione richiederà circa #2 secondi</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Test di Throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>Nota: Presuppone un loop hardware con crediti buffer inferiore a 2. Questo test non è valido</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>Nota: In base al presupposto di un loop hardware, le misurazioni di throughput sono effettuate con un numero di crediti buffer doppio ad ogni passo per compensare la lunghezza raddoppiata della fibra</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>Misura di Throughput a #1 Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>Test Throughput e Latenza</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Test Throughput e Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>Test di Throughput, Latenza e Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>Prova di latenza e packet jitter #1. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>Prova test di packet jitter #1. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>Prova test di latenza #1. Questa operazione richiederà #2 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>Prova test di latenza #1 al #2% del carico di throughput verificato. Questa operazione richiederà #3 secondi.</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 Avvio Test RFC 2544 #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 Avvio Test FC #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>Test completato.</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvataggio risultati, si prega di attendere.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>Test carico esteso</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>Test FC:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>Configurazione rete</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Tipo di frame</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>Modo Test</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>Manut. Livello dominio</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>TLV Mittente</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Incapsulamento</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profondità stack VLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Priorità utente SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>DEI Bit SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID SVLAN utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>Priorità utente CVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Priorità utente</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>Priorità utente SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>SVLAN 7 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>TPID SVLAN 7 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>DEI Bit SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>Priorità utente SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>SVLAN 6 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>TPID SVLAN 6 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>DEI Bit SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>Priorità utente SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>SVLAN 5 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>TPID SVLAN 5 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>DEI Bit SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>Priorità utente SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>SVLAN 4 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>TPID SVLAN 4 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>DEI Bit SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>Priorità utente SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>SVLAN 3 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>TPID SVLAN 3 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>DEI Bit SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>Priorità utente SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>SVLAN 2 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>TPID SVLAN 2 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>DEI Bit SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>Priorità utente SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>SVLAN 1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>TPID SVLAN 1 utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>DEI Bit SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Tipo di Loop</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC origine</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>Auto-incrementa SA MAC</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation>N° di MAC in Sequenza</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>Disattiva IP EtherType</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>Disabilita risultati OoS</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>Tipo DA</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Modo dati</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>Usa autenticazione</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nome utente</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Password</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>Service Provider</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nome servizio</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>Tipo di OP di origine</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Indirizzo OP di origine</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>Gateway predefinito</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>Subnet Mask</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>Indirizzo OP di destinazione</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Time To Live (hops)</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>Incremento ID IP</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>Link-Local Address origine</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>Indirizzo globale di origine</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Lung. prefisso subnet</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Indirizzo di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>Classe di traffico</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>Etichetta di flusso</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>Limite hop</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>Modalità di traffico</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>Tipo di servizio porta di origine</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>Porta di origine</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>Tipo di servizio porta di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>Porta di destinazione</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>Tipo di OP di ascolto ATP</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>Indirizzo OP di ascolto ATP</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID origine</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID sequenza</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID Originatore</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID Responder</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Verifica configurazione</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Versione Payload Acterna</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>Precisione misurazione della latenza</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>Unità di larghezza di banda</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>Largh. di banda max. test (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>Largh. di banda max. test upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>Largh. di banda max. test (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>Largh. di banda max. test upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>Largh. di banda max. test downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>Largh. di banda max. test downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>Largh. banda max. test (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>Consenti traffico al 100% effettivo</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>Precisione di misura del throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>Precisione misura throughput in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>Precisione misura throughput in downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Processo di puntamento Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolleranza perdita di frame Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolleranza perdita di Throughput in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolleranza perdita di Throughput in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>Durata di i tutti test (s)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>Numero di tentativi di tutti i test</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>Durata throughput (s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Soglia di riuscita Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>Soglia accettabile throughput (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>Soglia accettabile throughput in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>Soglia accettabile throughput (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>Soglia accettabile throughput in upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>Soglia accettabile throughput in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>Soglia accettabile throughput in downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>Soglia di riuscita Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>Numero di prove di latenza</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>Durata prova latenza (s)</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>Latenza banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>Soglia accettabile di latenza</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>Soglia accettabile di latenza (µs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>Soglia accettabile di latenza upstream (µs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>Soglia accettabile di latenza downstream (µs)</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Numero di prove Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>Durata prova Packet Jitter (s)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Soglia di riuscita Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>Soglia accettabile Packet Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>Soglia accettabile Packet Jitter in upstream (µs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>Soglia accettabile Packet Jitter in downstream (µs)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Procedura del test Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>Durata prova Perdita frame (s)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularità di banda per perdita frame (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularità di banda perdita frame upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularità di banda per perdita frame (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularità di banda perdita frame upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularità di banda perdita frame downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularità di banda perdita frame downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>Granularità larghezza di banda Perdita frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>Range minimo perdita frame (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>Range minimo perdita frame (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>Range minimo perdita frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>Range massimo perdita frame (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>Range massimo perdita frame (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>Range massimo perdita frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Range minimo perdita frame upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>Range minimo perdita frame upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Range massimo perdita frame upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>Range massimo perdita frame upstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Range minimo perdita frame downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>Range minimo perdita frame downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Range massimo perdita frame downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>Range massimo perdita frame downstream (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>Numero di passi perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>Numero di prove Back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>Granularità Back-to-back (frame)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>Durata massima burst Back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>Durata massima burst Back-to-back upstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>Durata massima burst Back-to-back downstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>Ignora frame di pausa</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>Tipo di test di burst</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>Dimensione CBS burst (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>Dimensione CBS burst upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>Dimensione CBS burst downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>Dimensione minima di burst hunt (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>Dimensione massima di burst hunt (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>Dimensione minima di burst hunt upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>Dimensione massima di burst hunt upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>Dimensione minima di burst hunt downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>Dimensione massima di burst hunt downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>Durata/e CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>Numero di tentativi test di burst</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>Visualizza pass/fail burst CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>Visualizza pass/fail burst hunt</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>Soglia dimensione burst hunt (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>Soglia dimensione burst hunt upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>Soglia dimensione burst hunt downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>Numero di prove Ripristino di sistema</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>Durata sovraccarico Ripristino di sistema (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>Durata/e carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>Riduzione velocità effettiva carico esteso (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>Lunghezza pacchetto carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>Lunghezza frame carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>Tipo login crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>Dimensione massima buffer crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>Passi di throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>Durata crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>Loop Down remoto riuscito: Unità **#1** fuori modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>Loop Down remoto riuscito: Unità **#1** fuori modo LLB trasparente</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>L'unità remota **#1** era già in Loop Down</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>Loop remoto disattivo a causa di cambiamenti di configurazione</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>Loop Up remoto riuscito: Unità **#1** in modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>Loop Up remoto riuscito: Unità **#1** in modo LLB trasparente</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>L'unità remota **#1** era già in modo LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>L'unità remota **#1** era già in modo LLB trasparente</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>Autoloop o Loop altra porta non è supportato</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Flusso #1: Loop Down remoto non riuscito: Tempo di riconoscimento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Flusso #1: Loop Up remoto non riuscito: Tempo di riconoscimento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop Down remoto non riuscito: Tempo di riconoscimento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop Down trasparente remoto non riuscito: Tempo di riconoscimento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop Up remoto non riuscito: Tempo di riconoscimento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Loop Up trasparente remoto non riuscito: Tempo di riconoscimento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>Rilevamento indirizzi globali duplicati avviato.</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>Configurazione indirizzo globale non riuscita (verificare le impostazioni).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>In attesa di Rilevazione indirizzi duplicati su indirizzi globale.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>Rilevazione indirizzi duplicati riuscita. Indirizzo globale è valido.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>Duplica rilevazione indirizzo non riuscito. Indirizzo globale non è valido.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>Rilevamento indirizzi link-locali duplicati avviato.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>Configurazione indirizzo link-locale non riuscita (verificare le impostazioni).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>In attesa di Rilevazione indirizzi duplicati su indirizzi link-locale.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>Rilevazione indirizzi duplicati riuscita. Indirizzo link-locale è valido.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>Duplica rilevazione indirizzo non riuscito. Indirizzo link-locale non è valido.</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>Recupero OP Stateless avviato.</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>Ottenimento OP stateless non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>OP stateless correttamente ottenuto.</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>Recupero OP stateless avviato.</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>Nessun router trovato per fornire OP stateful.</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>Nuovo tentativo di recupero di OP stateful.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>OP stateful correttamente ottenuto.</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>Rete inaccessibile</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>Host irraggiungibile</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>Protocollo irraggiungibile</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>Porta inaccessibile</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>Messaggio troppo lungo</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>Rete di dest. sconosciuta</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>Host di dest sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>Rete di dest. proibita</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>Host di dest. proibito</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>Rete inaccessibile per TOS</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>Host irraggiungibile per TOS</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Tempo scaduto durante transito</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>Tempo scaduto durante ricomposizione</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>Errore sconosciuto ICMP</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>Destinazione irraggiungibile</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>Indirizzo irraggiungibile</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>Nessuna route o destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>Destinazione non è un vicino</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>Comunicazione con destinazione vietata da amministratore</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>Pacchetto di dimensioni troppo grandi</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Limite hop superato in transito</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>Tempo di ricomposizione frammento scaduto</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>Campo d'intestazione erroneo riscontrato</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>Riscontrato Tipo di prossima intestazione non riconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>Riscontrata opzione IPv6 non riconosciuta</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>Inattivo</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE attivo</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP attivo</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Rete attiva</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>L'utente richiesto non è attivo</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Arresto layer dati</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Timeout PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Timeout PPP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Autenticazione PPP non riuscita</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP non riuscito sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config non valida</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Errore interno</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP riuscito. MAC di destinazione ottenuto</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>Attesa servizio ARP...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>Nessun ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Termina</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continua in Half Duplex</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>Arresta J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>Test RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>Test J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>Stato loop remoto</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>Sato Loop Locale</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>Stato PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>Stato IPv6</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>Stato ARP</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>Stato DHCP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>Stato ICMP</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>Stato Neighbor Discovery</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>Stato autconfig</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>Stato indirizzo</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>Rilevamento unità</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>Ricerca unità</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Ricerca in corso</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>Utilizza unità selezionata</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>Uscita</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>Connessione&#xA;a unità remota...</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>Connetti&#xA;a Remoto</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Disconnetti</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>Connesso&#xA;a remoto</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Connessione in corso</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Spento</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simmetrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asimmetrico</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>Unidirezionale</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Loopback</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>Downstream e upstream</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>Entrambe le Dir</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>Throughput:</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>I throughput in downstream e in upstream sono uguali.</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>I throughput in downstream e in upstream sono diversi.</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>Testare la rete in una sola direzione.</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>Misura:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>Il traffico è generato localmente e le misurazioni vengono effettuate localmente.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>Il traffico è generato localmente e da remoto e le misurazioni sono effettuate in ogni direzione.</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>Direzione di misura:</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>Il traffico è generato localmente e misurato dal tester remoto.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>Il traffico generato remotamente e viene misurato dal tester locale.</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>Ritardo roundtrip</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Ritardo unidirezionale (OWD) -</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Solo misura Round Trip Delay.&#xA;L'unità remota non è idonea per One Way Delay.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Solo misura One Way Delay.&#xA;L'unità remota non è idonea per RTD bidirezionale.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>Impossibile eseguire misurazioni del ritardo.&#xA;L'unità remota non è idonea per RTD bidirezionale o One Way Delay.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Impossibile eseguire misurazioni del ritardo.&#xA;L'unità remota non è idonea per One Way Delay.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Impossibile eseguire misurazioni del ritardo.&#xA;L'unità remota non è idonea per RTD bidirezionale.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Solo misura One Way Delay.&#xA;RTD non è supportato durante i test unidirezionali.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Impossibile eseguire misurazioni del ritardo.&#xA;RTD non è supportato durante i test unidirezionali.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Impossibile eseguire misurazioni del ritardo.&#xA;L'unità remota non è idonea per One Way Delay.&#xA;RTD non è supportato durante i test unidirezionali.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>Impossibile eseguire misurazioni del ritardo.</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>Tipo di misurazione latenza</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Locale</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>Richiede strumento di prova Viavi remoto.</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>Versione 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>Versione 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Calibrazione FEC-RS</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>Selezione delle Ottiche</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>Impostazioni IP per Canale di comunicazione lato remoto</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>Non sono richieste impostazioni di Indirizzo IP locale per il test di loopback.</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statico</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>Statico - Per servizio</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>Statico - Singolo</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manuale</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>Stateful</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Stateless</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>OP origine</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>Link orig - Indir. locale</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>Indirizzo globale orig</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>Ottenuto automaticamente</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>Definito dall'utente</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>Modo ARP </translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>Tipo di indirizzo origine</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>Modo PPPoE </translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>Avanzato</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Abilitato</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Disabilitato</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>MAC di origine cliente</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>Impostazione predef.</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>MAC origine predefinito</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>MAC predefinito cliente</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>MAC di origine utente</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>MAC utente clien.</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>Salta connessione</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>Stacked VLAN</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>Quante VLAN sono utilizzate sulla porta di rete locale?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>No VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLANs (Stacked VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>Immettere le impostazioni VLAN ID:</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>Profondità stack</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>Utente</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>Impostazioni di Discovery Server</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>NOTA: un indirizzo IP locale non può essere utilizzato per l'operazione 'Connessione Remota'</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>OP di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>IP Destinazione Globale</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Pinging</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>Aiutami a trovare &#xA;l'IP di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Porta locale</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Stato sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>SU (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>SU (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>SU (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>SU (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>SU (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>SU (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>SU (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>SU (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>SU (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Link perso</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>Porta locale:</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Autonegoziazione</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analisi in corso</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Autonegoziazione:</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>In attesa di connessione</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Connessione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>Nuovo tentativo...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNESSO</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>Connessione non riuscita</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>Connessione annullata</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>Canale di comunicazione:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>Impostazioni avanzate di Discovery Server</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>Ottieni IP di destinazione da un Discovery server</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>OP del server</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>Porta server</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>Passphrase server</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>Tempo di lease richiesto</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>Tempo di lease garantito (min.)</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>Impostazioni di rete L2 - Locale</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>Impostazioni unità locale</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>Traffico</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>Traffico LBM</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 (minimo)</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7(massimo)</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>TPID SVLAN utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>Impostare tipo di loop, EtherType e indirizzi MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>Impostare indirizzi MAC, EtherType e LBM</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>Imposta indirizzi MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>Impostare indirizzi MAC e modalità ARP</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Tipo di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>Unicast</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>Multicast</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>Broadcast</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>Priorità utente VLAN</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>Unità remota non è connessa.</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>Impostazioni di rete L2 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>Impostazioni unità remota</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>Impostazioni di rete L3 - Locale</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>Imposta classe di traffico, etichetta flusso e limite hop</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>Che tipo di priorità IP è utilizzata dalla rete in uso?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF (46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13 (14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21 (18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22 (20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23 (22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31 (26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32 (28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33 (30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41 (34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42 (36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43 (38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE (0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1 (8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2 (16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3 (24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4 (32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5 (40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6 (48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7 (56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>Impostare durata (TTL), incremento ID IP e modalità PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>Imposta modalità PPPoE e Time to Live</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>Impostare durata (TTL) e incremento ID IP</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>Imposta Time to Live</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>Quale priorità IP è utilizzata dalla rete in uso?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>Impostazioni di rete L3 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>Impostazioni di rete L4 - Locale</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>Tipo di servizio origine</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>Client DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>Server DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>Finger</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>Tipo di servizio destinazione</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>Imposta IP di ascolto ATP</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>Impostazioni di rete L4 - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>Imposta Tipo di Loop e ID Sequenza, Responder e Originator</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>Impostazioni L2 avanzate - Locale</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Tipo di origine</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC predefinito</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC utente</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>Configurazione LBM</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>Tipo LBM</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>Abilita mittente TLV</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Acceso</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>Impostazioni L2 avanzate - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>Impostazioni L3 avanzate - Locale</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Time To Live (hops)</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>Impostazioni L3 avanzate - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>Impostazioni L4 avanzate - Locale</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>Impostazioni L4 avanzate - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>Impostazioni avanzate rete - locale</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>Modelli</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>Usare un modello di configurazione?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Velocità accesso pacchetti Viavi enhanced > velocità di trasporto</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - Best Effort</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - Continental</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - Global</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - Metro</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - Mobile Backhaul H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - Regional</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - VoIP Data Emulation</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>RFC2544 Bit Transparent</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>RFC2544 Velocità di accesso pacchetti > Velocità di trasporto</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>RFC2544 Velocità di accesso pacchetti = Velocità di trasporto</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>Applica modello</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>Configurazioni modello:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>Perdita di frame Throughput (%) #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>Soglia latenza (µs): #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>Soglia Packet Jitter(us): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>Dimensioni frame: Predefinito</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>Larghezza di banda massima (upstream) #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>Larghezza di banda massima (downstream) #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>Massima larghezza di banda: #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>Test: Test Throughput e Latenza</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>Test: Throughput, Latenza, Perdita di frame e Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>Durate e prove: #1 secondi con 1 prova</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>Algoritmo di throughput: Viavi Enhanced</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>Algoritmo di throughput: Standard RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>Soglia di throughput (upstream) #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>Soglia di throughput (downstream) #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>Soglia di throughput #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>Algoritmo perdita pacchetti: Standard RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>Tempo Back-to-back: 1 sec</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>Granularità Back-to-back: 1 frame</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>Specifica impostazioni utilizzo avanzate</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>Largh. banda max.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>Massima Larghezza di Banda Downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>Massima Larghezza di Banda Downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>Massima Larghezza di Banda Downstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>Massima Larghezza di Banda Downstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>Largh. di banda max. downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>Massima Larghezza di Banda Upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>Massima Larghezza di Banda Upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>Massima Larghezza di Banda Upstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>Massima Larghezza di Banda Upstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>Largh. di banda max. upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>Largh. banda max. (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>Largh. banda max. (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>Largh. banda max. (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>Largh. banda max. (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Largh. banda max. (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>Frame selezionati</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>Tipo di Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Fotogramma</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>Reimposta</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Cancella tutto</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Lunghezza frame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Lungh. pacchetto</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>Upstream&#xA;Lunghezza frame</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>Unità remota non è connessa</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>Downstream&#xA;Lunghezza frame</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>Unità&#xA;remota&#xA;non è&#xA;connessa</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>Test selezionati</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>Test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>Latenza (richiede Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>Crediti buffer (necessita di Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>Throughput di crediti buffer (necessita di Crediti buffer)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>Ripristino di sistema (solo loopback e richiede throughput)</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>Prove aggiuntive</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>Packet Jitter (necessita di Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>Test di burst</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>Carico esteso (solo loopback)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>Config. throughput</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Processo di puntamento</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>Standard RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi Enhanced</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>Configurare impostazioni di misurazione avanzate della velocità di trasmissione</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>Precisione misura downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Accuratezza della Misurazione Downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisione misura downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>Precisione misura downstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>Precisione misura downstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>Precisione misura downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>A entro 1,0%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>A entro 0,1%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>A entro 0,01%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>A entro 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>A entro 400 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>A entro 40 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>A entro 4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>A entro ,4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>A entro 1000 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>A entro 100 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>A entro 10 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>A entro 1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>A entro ,1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>A entro ,01 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>A entro ,001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>A entro 0,0001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>A entro 92,942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>A entro 9,2942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>A entro ,9294 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>A entro ,0929 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>A entro 10000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>A entro 1000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>A entro 100 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>A entro 10 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>A entro 1 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>A entro ,1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>Precisione misura upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Accuratezza della Misurazione Upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisione misura upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>Precisione misura upstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>Precisione misura upstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>Precisione misura upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Precisione della misura</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>Precisione di misurazione (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>Precisione di misurazione (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>Precisione di misurazione (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>Precisione di misurazione (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>Precisione della misura (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>Ulteriori informazioni</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>Risoluzione dei problemi</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>Messa in servizio</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>Impostazioni avanzate velocità di trasmissione</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>Configurare separatamente durata test di latenza/instabilità dei pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>Configurare separatamente la durata del test di latenza</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>Configurare separatamente la durata del test di instabilità dei pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>Config. perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Procedura di prova</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Top Down</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Bottom-Up</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>Numero di passi</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 Kbps per passo</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1 Kbps per passo</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % Velocità della linea per passo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>Intervallo Test a Valle (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>Intervallo Test a Valle (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>Intervallo Test a Valle (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>Intervallo Test a Valle (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>Range del test in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularità della banda upstream downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularità della banda upstream downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularità della banda upstream downstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularità della banda upstream downstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>Granularità larghezza di banda downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>Intervallo Test a Monte (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>Intervallo Test a Monte (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>Intervallo Test a Monte (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>Intervallo Test a Monte (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>Range del test in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularità della banda upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularità della banda upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularità della banda upstream (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularità della banda upstream (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>Granularità larghezza di banda upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>Gamma del test (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>Gamma del test (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>Gamma del test (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>Gamma del test (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Gamma del test (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularità della banda (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularità della banda (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularità della banda (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularità della banda (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularità della banda (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>Specifica impostazioni misurazione perdita frame avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>Impostazioni perdita frame avanzate</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>Misure dei test facoltativi</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>Misura latenza</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>Misura Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>Config. Back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>Durata massima burst downstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>Durata massima burst upstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>Durata massima burst (s)</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>Granularità Burst (frame)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>Specifica impostazioni Back-to-back avanzate</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>Criterio frame di pausa</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>Config. crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Tipo di login controllo di flusso</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>Implicito (Transparent Link)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Esplicito (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Dimensione massima buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>Passi di throughput</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>Controlli Test</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>Configurare le durate dei test separatamente?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>Durata (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Numero di tentativi</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>Latenza/instabilità dei pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Burst (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Ripristino del sistema</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Tutti i test</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>Visualizza&#xA;Pass/Fail</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>Upstream&#xA;Soglia</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>Downstream&#xA;Soglia</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Soglia</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>Soglia di throughput (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>Soglia di throughput (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>Soglia di throughput (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>Soglia di throughput (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Soglia di throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>Unità remota&#xA;non è connessa.</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>RTD latenza (µs)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>OWD latenza (µs)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>Packet Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>Dimensione burst hunt (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>Burst (CBS Policing)</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>Alta precisione - Basso ritardo</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>Bassa precisione - Alto ritardo</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>Esegui i test FC</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>Salta i test FC</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>su</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>SPENTO</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>Throughput&#xA;crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Grafico del test di throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Grafico del test di throughput in upstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Risultati del test di Throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Risultati del test di throughput in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Grafico del test di throughput in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Risultati del test di throughput in downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>Anomalie di throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>Anomalie throughput in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>Anomalie throughput in downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>Risultati Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>Throughput (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>Throughput (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Frame 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Frame 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Frame 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Frame 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Frame 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Frame 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Frame 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Frame 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Frame 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Frame 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>Riuscito / Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>Lunghezza frame&#xA;(bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>Lungh. pacchetto&#xA;(bytes)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>Velocità&#xA;misurata (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>Velocità&#xA;misurata (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>Velocità&#xA;misurata (frm/sec)</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>Rileva&#xA;R_RDY</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Velocità cfg&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>Velocità L1&#xA;misurata (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>Velocità L1&#xA;misurata (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>L1 misurata&#xA;(velocità linea %)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>Velocità L2&#xA;misurata (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>Velocità L2&#xA;misurata (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>L2 misurata&#xA;(velocità linea %)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>Velocità L3&#xA;misurata (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>Velocità L3&#xA;misurata (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>L3 misurata&#xA;(velocità linea %)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>Velocità L4&#xA;misurata (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>Velocità L4&#xA;misurata (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>L4 misurata&#xA;(velocità linea %)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>Velocità misurata&#xA; (frm/sec)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>Velocità misurata&#xA; (pacch./sec)</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>Rileva&#xA;pausa</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Velocità config.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Velocità config.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Velocità config.&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Velocità config.&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalie</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>OoS Frame&#xA;Rilevato/i</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Payload Acterna&#xA;Rilevato errore</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>Errore&#xA;FCS rilevato</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>Errori checksum&#xA;IP rilevati</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>Errore checksum&#xA;TCP/UDP rilevato</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>Test di latenza</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Grafico test di latenza</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Grafico del test di latenza in upstream</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Risultati test di Latenza</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Risultati del test di latenza in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Grafico del test di latenza in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Risultati del test di latenza in downstream</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>Latenza&#xA;RTD (µs)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>Latenza&#xA;OWD (µs)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>velocità linea % &#xA;misurata</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>Rileva &#xA;pausa</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>Risultati test Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>Risultati del test perdita frame upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>Risultati del test perdita frame downstream</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>Risultati perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Frame 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>Velocità configurata (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>Velocità configurata (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>Velocità configurata (L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>Velocità configurata (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>Velocità configurata (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Perdita frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>Velocità di throughput&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>Velocità di throughput&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>Velocità di throughput&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>Velocità di throughput&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>Velocità di throughput&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>Velocità perdita frame&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>Frame persi</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>Max media Packet&#xA;Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>Rilevazione&#xA;errori</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>Rileva&#xA;OoS</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Velocità cfg&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Risultati test Back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Risultati test Back-to-back in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Risultati test Back-to-back in downstream</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>Media&#xA;Frame burst</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>Media&#xA;Secondi burst</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>Risultati test Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Attenzione</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>DimensioneMinimaBuffer&#xA;(Crediti)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>Risultati test Throughput Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>Risultati di throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>Velocità effettiva (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>Cred 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>Cred 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>Cred 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>Cred 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>Cred 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>Cred 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>Cred 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>Cred 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>Cred 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>Cred 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>Dimensione buffer&#xA;(Crediti)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>Velocità misurata&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>Velocità misurata&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>Velocità misurata&#xA;(frame/sec)</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>Test trasparenza Ethernet L2 J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>Frame J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>Esegui test</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>Esegui test J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Riepilogo</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>Fine: Risultati dettagliati</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>Prova Exit J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Applicazione</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>Configura tipi di frame per testare il Servizio per Trasparenza Layer 2</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   Protocollo   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>Protocollo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> Tipo di frame </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> Incapsulamento </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Stacked</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>Dimensione&#xA;frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Dimensioni frame</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Conteggio</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>Velocità&#xA;(fr/sec)</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Velocità</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>Timeout&#xA;(msec)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>Timeout</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Controlla</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>Bit&#xA;DEI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>TPID SVLAN utente</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>Auto Inc CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>Auto Inc SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>Encap.</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>Encap.</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>Tipo&#xA;di frame</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit Inc</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>Config.&#xA;rapida</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>Aggiungi&#xA;Frame</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>Elimina&#xA;Frame</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>L'indirizzo origine è comune per tutti i frame. Questo viene configurato nella pagina Impostazioni locali.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Incremento PBit</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>VLAN Stack</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>Incremento SPbit</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dati</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>Configurazione J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensità</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rapido (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>Full (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Famiglia</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tutto</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>Spanning Tree</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>Laser&#xA;acceso</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>Laser&#xA;spento</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>Sequenza&#xA;frame di avvio</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>Sequenza&#xA;frame di arresto</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>Test J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>ARRESTATO</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>IN CORSO</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>Errori payload</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>Errori header</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>Errore di conteggio</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>Riepilogo dei risultati</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Inattivo</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>In corso</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>Errata corrisp. Payload</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>Errata corrispondenza Header</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>Risultati J-Proof</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>Reimpostazione&#xA;Rx ottico</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>ControlloRapido</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>Eseguire Test di ControlloRapido</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>Impostazioni ControlloRapido</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>Risultati del ControlloRapido sul Carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Dettagli</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Singola</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>Per Flusso</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>FROM_TEST</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>Test di throughput:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>Risultati QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Risultati test carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Conteggio frame Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Conteggio frame Rx</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>Conteggio Frame con errori</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>Conteggio frame OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>Conteggio frame persi</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Tasso di perdita frm</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanente</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Attivo</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Velocità effettiva: %1 Mbps (L1), durata: %2 secondi, dimensione frame: %3 byte</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Velocità di trasmissione: %1 Kbps /L1), Durata: %2 secondi, Dimensione frame: %3 byte</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>Non corrisponde a quanto richiesto?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Operazione completata</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>Ricerca destinazione</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>Stato ARP:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Loop remoto</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Verifica loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Verifica Loop hardware</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Verifica di Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Controllo loop LBM/LBR in corso</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Loop permanente</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Loop LBM/LBR </translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Loop non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>Loop remoto:</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>Velocità effettiva misurata (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Non determinato</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>Velocità di trasmissione rilevata (L1 Kbps)</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>Visualizza errori</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>Carico (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>Caricare (L1 Kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>Durata test velocità effettiva (secondi)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>Dimensioni frame (byte)</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Velocità Bit</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>Durata del test delle prestazioni</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 secondi</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 secondi</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 minuto</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 minuti</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 minuti</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 minuti</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 ore</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 ore</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 ore</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 ore</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>Definito dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>Durata test (sec.)</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>Durata test di funzionamento (minuti)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Lunghezza frame (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>Lunghezza utente</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>Lunghezza jumbo</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  Connettore elettrico:  10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>Lunghezza d'onda laser</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>Connettore elettrico</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>Connettore ottico</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>Dettagli...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Link attivo</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Risultati di traffico</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Conteggio errori</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Frame errati</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Frame OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Frame persi</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>Dettagli interfaccia</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>Dati SFP/XFP</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>Lunghezza d'onda (nm): SFP sintonizzabile, consultare impostazione sintonia Canale/Lunghezza d'onda.</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>Lunghezza del Cavo (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>Sintonia supportata</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>Lunghezza d'onda;Canale</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Lunghezza d'onda</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Canale</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>Velocità Consigliate&lt;/ p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>Velocità nominale (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>Velocità min (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>Velocità max (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fornitore</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>PN fornitore</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Rev fornitore</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Tipo di livello potenza</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Monitoraggio diagnostico</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Byte diagnostico</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>Configurare JMEP</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>Lunghezza d'onda (nm): Dispositivo ottimizzabile; fare riferimento alla configurazione di ottimizzazione lunghezza d'onda/canale.</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>Dati CFP/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>Dati CFP2/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>Dettagli CFP4/QSFP</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN fornitore</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Codice data</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Codice lotto</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>N. Versione HW/SW</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>Spec. HW MSA  N. Rev</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>Gestione MSA N. rev I/F</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Power Class</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Tipo di livello potenza Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Max Lambda Power Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Max Lambda Power Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>N. di fibre attive</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Lunghezze d'onda per fibra</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>Byte diagnostico</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL per gamma fibre (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Max tasso corsia rete (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>Modulo ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Velocità supportate</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Lunghezza d'onda nominale (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Bit Rate nominale (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** Uso consigliato per le applicazioni 100GigE RS-FEC ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>Esperto CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>Esperto CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>Esperto CFP4</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Abilita loopback Viavi</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>Abilita Loopback CFP Viavi</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>Abilita modalità Expert CFP</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>Abilita modalità Expert CFP2</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>Attiva modalità Esperta CFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>Pre-enfasi</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>Pre-Enfasi CFP Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predefinito</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>Basso</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>Nominale</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>Alto</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>Divisore di frequenza</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>Divisore di frequenza CFP Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>Skew Offset (bytes)</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>Offset Distorsione CFP Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>Inverti la polarità</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>Invertire Polarità CFP Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Ripristina FIFO Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 Rx</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>Equalizzazione</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>Livellamento CFP Rx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>Inversione Polarità CFP Rx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>Ignora LOS</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>Ignora LOS CFP Rx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Ripristina FIFO Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>Esperto QSFP</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>Abilita modalità Expert QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx Ignora LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>Bypass CDR</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>Ricevi</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>Bypass Ricezione CDR QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>Trasmetti</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>Bypass Trasmissione CDR QSFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>Il controllo del bypass della trasmissione e della ricezione del CDR sarebbe disponibile se il modulo QSFP fosse supportato.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>Ingegnerizzazione</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>XCVR Loopback Core</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>Loopback Core XCVR CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>XCVR Loopback Linea</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>Loopback Linea XCVR CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>Modulo Host Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>Loopback Host Modulo CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>Modulo Network Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>Loopback Rete Modulo CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>Loopback Sistema Trasmissione CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>Loopback Linea Trasmissione CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>ID chip scatola</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Riv. chip scatola</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Ver. ucode scatola</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>CRC ucode scatola</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Lane sistema scatola</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Lane linea scatola</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>Lane host CFPn</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>Lane rete CFPn</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>QSFP XCVR Loopback Core</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>QSFP XCVR Loopback Linea</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO Peek DEVTYPE</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO Peek PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO Poke DEVTYPE</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO Poke PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>Valore MDIO Poke</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>Attivazione/disattivazione laser per corso controlli A013 registro.</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C Peek PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C Peek DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C Poke PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C Poke DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>Valore I2C Poke</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>Controlli registro 0x56 controlli laser per-traccia abilita/disabilita.</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>Dettagli SFP+</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Segnale</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Segnale di Clock Tx</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>Origine clock</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>Interno</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>Recuperato</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>Esterno</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>1.5M esterno</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>2M esterno</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>10M esterno</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>Recuperato remoto</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Modulo di Temporizzazione</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>Origine VC-12</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>Interno - Offset freq. (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>Clock remoto</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>Dispositivo ottimizzabile</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>Modalità di sintonia</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>Frequenza</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>Frequenza (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>Prima frequenza sintonizzabile (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>Ultima frequenza sintonizzabile (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>Spaziatura della griglia (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>Allarme Skew</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>Soglia allarme Skew (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>Risincronizzazione necessaria</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>Risincronizzazione completa</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>Calibrazione RS-FEC conclusa con successo</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>Calibrazione RS-FEC fallita</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC è in genere utilizzato con SR4, PSM4, CWDM4.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Per eseguire la calibrazione RS-FEC, effettuare le seguenti operazioni (vale anche per CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Inserire un adattatore QSFP28 nella CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Inserire un ricetrasmettitore QSFP28 nell'adattatore</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Inserire un dispositivo di loopback in fibra nel ricetrasmettitore</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Fare clic su Calibra</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Calibrare</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Calibrazione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Risincronizza</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Ora è necessario risincronizzare il ricetrasmettitore con il dispositivo in prova (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Rimuovere il dispositivo di loopback in fibra dal ricetrasmettitore</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Stabilire una connessione verso il dispositivo in prova (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Accendere il laser DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Verificare che il LED di Segnale Presente sulla CSAM sia verde</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Fare clic su Risincronizza Pista</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>Risinc.&#xA;Pista</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>Risultati J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>Premere il pulsante "Start" per iniziare il test J-QuickCheck.</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>Continuazione in modalità half-duplex.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>Controllo della connettività di traffico in direzione upstream.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>Controllo della connettività di traffico in direzione downstream.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>La connettività di traffico per tutti i servizi è stata verificata con successo.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>Non è stato possibile verificare la connettività di traffico in direzione upstream per i(l) servizi(o): #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>Non è stato possibile verificare la connettività di traffico in direzione downstream sui seguenti servizi: #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>Non è stato possibile verificare la connettività di traffico in direzione upstream per i(l) servizi(o): #1, e in direzione downstream per i(l) servizi(o): #2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>Non è stato possibile verificare la connettività di traffico in direzione upstream</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>Non è stato possibile verificare la connettività di traffico in direzione downstream.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>Non è stato possibile verificare la connettività di traffico in direzione upstream o downstream.</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>Verifica della connettività</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>Connettività di traffico:</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>Start QC</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>Stop QC</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Autotest delle reti ottiche</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>Fine: Salva i Profili</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Report</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>Ottica</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>Uscire dall'Auto-Diagnosi dell'Ottica</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>Segnale ottico perso. Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>Rilevato livello di energia basso. Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>Rilevato livello di energia elevato. Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>È stato rilevato un errore. Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>Il test è FALLITO perché è stato rilevato uno skew eccessivo.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>Il test è FALLITO perché è stato rilevato uno skew eccessivo. Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>È stato rilevato un errore bit. Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>Test NON RIUSCITO: BER superiore alla soglia configurata.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>Test NON RIUSCITO per perdita di sincronizzazione dei pattern.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>Impossibile leggere QSFP+ inserito.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>Impossibile leggere CFP inserito.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>Impossibile leggere CFP2 inserito.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>Impossibile leggere CFP4 inserito.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>È stato rilevato un errore su un bit correggibile RS-FEC.  Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>È stato rilevato un errore su un bit non correggibile RS-FEC.  Il test è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>Calibrazione RS-FEC fallita</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>Rilevato errore di deviazione di frequenza ingresso</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>Rilevato errore di deviazione di frequenza uscita</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Sinc perso</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>Rilevate violazioni del codice</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>Blocco marker allineamento perso</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>Rilevati marker di allineamento non validi</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>Rilevati errori Bit BIP-8 AM</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>Errori di blocco BIP</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>Skew rilevato</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>Errori di blocco rilevati</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>Frame sottodimensionati rilevati</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>Rilevato errore remoto</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 bit errati rilevati</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>Sinc pattern perso</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>Impossibile misurare con precisione la soglia BER a causa della perdita di sinc. pattern</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>Il BER misurato ha superato la soglia BER scelta</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS rilevato</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>Errori FAS rilevati</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>Rilevato fuori frame</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>Rilevata perdita di frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>Frame Sinc perso</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>Rilevato marker fuori corsia logica</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>Rilevati errori marker corsia logica</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>Rilevata perdita allineamento corsia</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>Allineamento corsia perduto</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>Errori MFAS rilevati</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>Rilevato allineamento fuori corsia</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS rilevato</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>Rilevato fuori recupero</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>Skew eccessivo rilevato</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>Calibrazione RS-FEC conclusa con successo</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Errore</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>Test Ottica</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>Tipo di test</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>Il test utilizza 100GE RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>Tipo di ottiche</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>Verdetto test complessivo</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>Test presenza segnale</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>Test livello segnale ottico</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>Test Skew eccessivo</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>Test errori bit</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>Test errori generali</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>Test soglia BER</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>Potenza Ottica</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Livello Rx Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Livello Rx Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Livello Rx Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Livello Rx Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Livello Rx Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Livello Rx Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Livello Rx Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Livello Rx Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Livello Rx Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Livello Rx Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Totale Livello Rx  (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Livello Tx Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Livello Tx Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Livello Tx Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Livello Tx Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Livello Tx Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Livello Tx Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Livello Tx Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Livello Tx Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Livello Tx Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Livello Tx Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Totale Livello Tx  (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>Impostazioni</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>Connettere un breve cavo patch tra i terminali Tx e Rx del connettore che si desidera testare.</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>Test Ottica&#xA;PCP</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>Test CFP2&#xA;Ottica/Slot</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Annulla Test</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>Test Ottica&#xA;QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>Test Ottiche&#xA;QSFP28</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Impostazioni:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>Consigliato</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 minuti</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 minuti</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 ore</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 ore</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>Durata utente (minuti)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>Durata Consigliata (minuti)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>Il tempo consigliato per questa configurazione è di &lt;b>%1&lt;/b> minuti.</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>Tipo Soglia BER</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Post-FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Pre-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>Soglia BER</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>Abilita sfalsamento linea PPM</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>PPM Max Offset (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>Arresta su errore</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>Panoramica dei risultati</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>Tipologia Ottica/Slot</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>Sfalsamento PPM corrente</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>BER corrente</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>Potenza Ottica (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Livello Rx  Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Rx Level Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Rx Level Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Rx Level Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Rx Level Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Rx Level Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Rx Level Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Rx Level Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Rx Level Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Rx Level Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Totale Livello Rx </translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Tx Level Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Tx Level Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Tx Level Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Tx Level Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Tx Level Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Tx Level Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Tx Level Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Tx Level Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Tx Level Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Tx Level Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Totale Livello Tx </translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>Dettagli interfaccia CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>Dettagli interfaccia CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>Dettagli Interfaccia CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>Dettagli interfaccia QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>Dettagli Interfaccia QSFP+</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>Dettagli Interfaccia QSFP28</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>No QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>Lettura QSFP non riuscita - Reinserire QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>Errore checksum QSFP</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>Impossibile interrogare i registri QSFP richiesti.</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>Impossibile confermare l'identità QSFP.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>Test BERT Payload con Durata </translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>Transparenza GCC </translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>Seleziona ed esegui test</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>Impostazioni avanzate</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>Esegui Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>Payload BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>Uscita Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>Frequenza Misura</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Stato Autonegoziazione</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>Configurazione RTD</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>Tutte le Lane</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>OTN Check:</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** Avvio test OTN Check ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>Bit Payload BERT errati rilevati </translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>Più di 100.000 bit Payload errati rilevati</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>Soglia Payload BERT superata</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 bit Payload errati rilevati</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>Bit error rate Payload BERT:#1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>Soglia RTD superata</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: Min: #3, Max: #4, Media: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>Test BERT Payload in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>Test RTD in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>Test Transparenza GCC in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>Bit GCC errati rilevati</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>Soglia GCC BER superata</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 bit GCC errati rilevati</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>Più di 100.000 bit GCC errati rilevati</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>Tasso di bit GCC errati: #1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** Avvio Controllo Loopback ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** Controllo Loopback Tralasciato ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** Controllo Loopback Terminato ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>Loopback Rilevato</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>Nessun loopback rilevato</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>Il Canale #1 non è disponibile</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>Il Canale #1 è ora disponibile</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>Perdita sincronizzazione pattern.</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>Perdita sincronizzazione pattern GCC.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>Test interrotto: è stato rilevato un bit errato.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>Test fallito: Il BER ha superato la soglia configurata.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>Test fallito: Il RTD ha superato la soglia configurata.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>Test abortito: Nessun loopback rilevato.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>Test interrotto: Nessun segnale ottico presente.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>Test interrotto: Perdita di frame.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>Test interrotto: Perdita di sincronizzazione frame.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>Test interrotto: Perdita di sincronizzazione del pattern.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>Test interrotto: Perdita di sincronizzazione del GCC pattern.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>Test interrotto: Perdita di allineamento della lane.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>Test interrotto: Perdita del marker lock.</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>Deve essere selezionato almeno un canale.</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>Loopback non selezionato</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>Loopback non rilevato</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>Selezione Test</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>Durata Prevista del Test</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>Tempo di Esecuzione del Test</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>L'esecuzione del OTN Check richiede un traffico in loopback ; questo loopback è richiesto all'estremità del circuito OTN.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>Offset Ottiche, Mappatura del Segnale</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>Mappatura del Segnale</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>Mappatura del Segnale e Selezione delle Ottiche</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>Configurazione Avanzata</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>Struttura segnale</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP Optics RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>RTD Offset (us) Ottica CFP </translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 Optics RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>Offset RTD Ottica CFP4 (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>Configura Durata e Payload del Test BERT</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>Configurazione Durata e Payload del Test BERT</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>Impostazioni Payload BERT</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>Livello di Confidenza (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>In base a velocità della linea, soglia BER e livello di confidenza, il tempo di test raccomandato è &lt;b>%1&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Pattern</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>Pattern BERT</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>Soglia di Errore</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>Mostra Successi/Fallimenti</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>Impostazioni Round Trip Delay</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>Configurazione RTD</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>Include</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>Soglia (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>Frequenza(e) di misura</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>Configurazione GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>Impostazioni Transparenza GCC </translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>Canale GCC </translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>Soglia GCC BER</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>Il GCC utilizza il BERT Pattern 2^23-1.</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>Controllo Loopback</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>Tralascia controllo loopback</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>Premere il pulsante "Start" per iniziare il controllo del loopback.</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>Salta Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>Controllo Loopback</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>Loopback Rilevato</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>Tralascia controllo loopback</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>Rilevamento loopback</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>Rilevamento loopback</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>Risultati Payload BERT</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>BER Misurato</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>Conteggio Bit Errati</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>Verdetto</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>Configurazione Payload BERT</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>Risultati Round Trip Delay</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>Min (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>Max (ms)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>Media (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>Nota: la condizione di fallimento è verificata quando il RTD medio supera la soglia.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configurazione</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>Configurazione Round Trip Delay</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>Risultati Transparenza GCC </translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>Tasso d'errore GCC BERT Bit</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>Errori GCC BERT Bit</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>Configurazione GCC Transparency</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>Analisi del protocollo</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Acquisizione</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>Cattura e analizza CDP</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>Cattura e analizza</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>Selezionare Protocollo per l'analisi</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>Avvia&#xA;analisi</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>Annulla&#xA;Analisi</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analisi</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>Expert PTP</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>Configurare manualmente le impostazioni dei test </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>Soglie</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>Esegui Controllo Rapido</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>Terminazione Test Expert PTP</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>Il link non è più attivo.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>La sincronizzazione monodirezionale con la sorgente temporale è stata persa.</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>La sessione PTP Slave si è arrestata in modo inaspettato.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>Sincronizzazione con la sorgente temporale non presente. Si prega di verificare che la sorgente temporale sia correttamente configurata e collegata.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>Impossibile stabilire una sessione PTP Slave.</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>Ricevitore GPS non rilevato.</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>Tipo TOS</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Timeout Annuncio Rx</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>Annuncio</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 / secondo</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 / secondo</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 / secondo</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 / secondo</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 / secondo</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 / secondo</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 / secondo</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 / secondo</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Sincronizza</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>Richiesta ritardo</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>Durata lease (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Abilita</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Massimo Errore Tempo Abilita Soglia</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Time Error Max. (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Time Error Max. Soglia (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>Durata test (minuti)</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>Controllo Rapido</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>IP Master</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>Dominio PTP</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>Avvio&#xA;Controllo</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>Avvio&#xA;Sessione</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>Sessione&#xA;Attivata</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Soglia Time Error Massimo (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Time Error Massimo (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>Time Error, Cur (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>Time Error, Cur (us) vs. Tempo</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Risultati del test</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>Durata (minuti)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP Check</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>Test PTP Check</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>Fine: Controllo PTP</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>Avvia un altro test</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>Terminazione Controllo PTP</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>RFC 2544 avanzata</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>Impostazioni non valide</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>Impostazioni OP avanzate - Locale</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>Impostazioni latenza RTD avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>Impostazioni test burst (CBS) avanzato</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Esegui J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>Impostazioni J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>Jitter</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>Esci da test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>Configurazione rete locale</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>Configurazione rete remota</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>Stato Autonegoziazione locale</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Velocità (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Controllo di flusso</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Supporto FDX</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Supporto HDX</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Capace di pausa</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>Stato Autonegoziazione remoto</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Half</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Full</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Nessuno dei due</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx e Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Solo Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Solo Rx</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>Velocità frame RTD</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 frame al secondo</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 frame al secondo</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>Config burst</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>Dimensione burst stabilita (CBS)</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>CBS Policing (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>Burst hunt</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>CBS in downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>Dimensioni di burst downstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimo</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>Massimo</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>CBS in upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>Dimensioni di burst upstream (kB)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>Dimensioni di burst (kB)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>Specifica impostazioni CBS avanzate</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>Specifica impostazioni CBS Policing avanzate</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>Specifica impostazioni Burst Hunt avanzate</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>Tolleranza</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>Config. carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>Riduzione del throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 presenta le seguenti impostazioni di configurazione non valide:</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>La connettività del traffico è stata verificata con successo. Esecuzione del test di carico.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>Premere "Start" per eseguire alla velocità della linea.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>Premere "Start" per eseguire usando la banda RFC 2544 configurata.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>Il throughput misurato NON SARÀ usato per i test RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>Il throughput misurato SARÀ usato per i test RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>Dimensione frame test di carico: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>Dimensione pacchetto test di carico: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>Dimensione frame test di carico upstream: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>Dimensione frame test di carico downstream: %1 bytes.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>Throughput misurato:</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>Test con larghezza di banda massima configurata RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>Utilizza la misura del throughput misurato come massima larghezza di banda per RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>Dimensione frame test di carico (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>Dimensione pacchetto test di carico (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>Dimensione frame test di carico upstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>Dimensione frame test di carico downstream (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>Esegui test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>Ignora i test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>Verifica loop hardware</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Test Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Grafico del test di Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Grafico del test di Jitter in upstream</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Risultati del test di Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Risultati del test di Jitter in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Grafico del test di Jitter in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Risultati del test di Jitter in downstream</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>Instabilità pacchetti media massima (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>Max media Jitter</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>Max media&#xA;Jitter (µs)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Risultati test CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Risultati del test di CBS in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Risultati del test di CBS in downstream</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Risultati del test di burst hunt</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Risultati del test di burst hunt in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Risultati del test di burst hunt in downstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Risultati del test CBS Policing</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Risultati del test di CBS Policing in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Risultati del test di CBS Policing in downstream</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>Test di burst (CBS Policing)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Conf. dim.&#xA;burst (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Dimensioni di burst&#xA;in Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>Dimensione media&#xA;burst Rx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>Frame&#xA;inviati</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>Frame&#xA;ricevuti</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>Frame&#xA;persi</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>Dimensioni burst&#xA;(KB)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>Latenza&#xA;(µs)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>Jitter&#xA;(µs)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>Burst Size&#xA;configurato (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Dimensioni di burst&#xA;policing in Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>CBS&#xA;stimato (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Grafico test ripristino di sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Grafico del test di ripristino sistema in upstream</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Risultati test Ripristino di sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Risultati del test di ripristino sistema in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Grafico del test di ripristino sistema in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Risultati del test di ripristino sistema in downstream</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>Tempo di ripristino (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>Tasso di sovraccarico&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>Tasso di sovraccarico&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>Tasso di sovraccarico&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>Tasso di sovraccarico&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>Tasso di sovraccarico&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>Tasso di recupero&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>Tasso di recupero&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>Tasso di recupero&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>Tasso di recupero&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>Tasso di ripristino&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>Tempo di recupero&#xA;medio (µs)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modalità</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>Controlli</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>Controlli TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>Shaping</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>Config. passo</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>Seleziona passi</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Percorso MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Throughput TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avanzato</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>Impostazioni di connessione</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>Contr. TrueSpeed (Avanzato)</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Selezionare le unità CB</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>Finestra percorri</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>Prova Exit TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>Host TCP non è riuscito a stabilire una connessione. Il test corrente è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>Selezionando la modalità di Risoluzione dei problemi ci si è disconnessi dall'unità remota.</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>Selezione test non valida: Per eseguire TrueSpeed deve essere selezionato almeno un test.</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>La MTU misurata è troppo piccola per continuare. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>Il file è stato trasmesso troppo rapidamente! Scegliere una dimensione del file per TCP tale che il test venga eseguito per almeno 5 secondi. È consigliabile che l'esecuzione del test duri almeno 10 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>È stato raggiunto il numero massimo di tentativi di ritrasmissione. Test interrotto.</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>Host TCP ha rilevato un errore. Il test corrente è stato interrotto.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>Avvio test TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>Puntamento sulla dimensione MTU.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>Test MTU #1 byte con MSS #2 byte.</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>Il Path MTU è stato determinato pari a #1 bytes. Ciò equivale a un MSS di #2 byte.</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>Esecuzione del test RTT. Questa operazione richiederà #1 secondi.</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>Il Round-trip time (RTT) è stato determinato pari a #1 msec.</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>Esecuzione del test Walk the Window in upstream.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>Esecuzione del test Walk the Window in downstream.</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>Invio di #1 byte di traffico TCP in corso...</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>Traffico in uscita locale: non soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>Traffico in uscita remoto: non soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>Traffico in uscita locale: soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>Traffico in uscita remoto: soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>Durata (secondi): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>Connessioni: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>Dimensione finestra (kB): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>Esecuzione del test di throughput TCP in upstream.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>Esecuzione del test di throughput TCP in downstream.</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>Esecuzione test TCP avanzato in corso... Saranno necessari #1 secondi.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>Tipo OP locale</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>Indirizzo IP locale</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>Gateway predefinito locale</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>Maschera di sottorete locale</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>Incapsulamento locale</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Indirizzo OP remoto</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>Seleziona modalità</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>Che tipo di test sono stai eseguendo?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>Sto &lt;b>installando&lt;/b> o &lt;b>attivando&lt;/b> un nuovo circuito.*</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>Sto eseguendo la &lt;b>risoluzione dei problemi&lt;/b> di un circuito esistente.</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>*Richiede un Tester MTS/T-BERD remoto</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>Come sarà configurato il throughput?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>I miei throughput in downstream e upstream sono &lt;b>uguali&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>I miei throughput in downstream e upstream sono &lt;b>diversi&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Imposta collo di bottiglia della larghezza di banda in modo corrispondente al CIR di SAMComplete quando esegui il caricamento&#xA; della configurazione TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Imposta collo di bottiglia della larghezza di banda in modo corrispondente alla Banda massima di RFC 2544 quando esegui il caricamento&#xA; della configurazione TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Iperf Server</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>Tester T-BERD/MTS</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>Tipo OP</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>Impostazioni remote</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>Impostazioni host server TCP</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>Questo passo consente di configurare le impostazioni globali per tutti i successivi passaggi TrueSpeed. Questo include la CIR (Committed Information Rate) e la % TCP Pass, che è la percentuale del CIR necessaria per superare il test di throughput.</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>Esegui test Walk-the-Window</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>Tempo totale del test (s)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>Trova automaticamente la MTU size</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>Dimensione MTU (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>ID VLAN locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>Priorità</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>Priorità locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>TOS locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>DSCP locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>CIR in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>CIR in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>Host TCP&#xA;remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>ID VLAN remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>Priorità remota</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>TOS remoto</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>DSCP remoto</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>Specifica impostazioni TrueSpeed avanzate</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Selezionare l'unità CB</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>Traffic shaping</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Unità CB</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>CB (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>Profilo di shaping</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>Traffico in uscita locale e remoto soggetti a shaping</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>Solo traffico in uscita locale soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>Solo traffico in uscita remoto soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>Traffico in uscita locale e remoto non soggetti a shaping</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>Shaping del traffico (percorrere solo i test sulla finestra e la velocità effettiva)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>Test eseguiti senza shaping poi con shaping</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) locale</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc Locale (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>CB (kbit) locale</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc Locale(MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>CB (Mbit) locale</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) remoto</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (remoto)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc Remoto (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>CB (kbit) remoto</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc Remoto (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>CB (Mbit) remoto</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>Modellare il traffico TCP?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>Traffico non soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>Traffico soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>Eseguire il test senza shaping</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>POI</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>Eseguire il test con shaping</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>Eseguire il test con shaping in locale</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>Eseguire il test con shaping in locale e remoto</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>Eseguire senza nessuno shaping</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>Eseguire il test con shaping in locale e senza shaping in remoto</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>Eseguire senza shaping in locale</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>Eseguire il test senza shaping in locale e con shaping in remoto</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>Shaping traffico locale</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>CB (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>CB (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>L'unità remota non è connessa</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>Shaping traffico remoto</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>Mostrare ulteriori opzioni di shaping</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>Controlli TrueSpeed (Avanzati)</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>Connesso alla porta</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>% TCP Pass</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>Limite superiore MTU (byte)</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>Usa connessioni multiple</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>Attiva finestra di saturazione</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>Incremento finestra (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>Incremento connessioni (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>Configurazione passo</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>Passi TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>È necessario avere almeno un passo selezionato per eseguire il test.</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>In questo passo viene utilizzata la procedura definita in RFC4821 per determinare automaticamente l'MTU (Maximum Transmission Unit) del percorso di rete da un capo all'altro. Il test del TCP Client consiste nel tentativo di inviare segmenti TCP con varie dimensioni dei pacchetti e determinare l'MTU senza necessità di ICMP (richiesto per il tradizionale Path MTU Discovery).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>Questo passo eseguirà un trasferimento TCP a bassa intensità e mostreràil tempo di andata e ritorno (RTT) che verrà utilizzato come base per risultati dei successivi passi di prova. La RTT di base è la latenza intrinseca della rete, con esclusione dei ritardi aggiuntivi causati dalla congestione della rete.</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>Durata (secondi)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>In questo passo viene eseguita una "Scansione finestre" TCP per ricavare i risultati di throughput TCP per fino a quattro (4) combinazioni di dimensioni finestra TCP.  Questo passo riporta anche il throughput TCP effettivo a confronto con quello previsto per ogni dimensione di finestra.</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>Dimensioni finestra</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>Dimensione finestra 1 (bytes)</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># Conn.</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>Connessioni Finestra 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>Dimensione finestra 2 (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>Connessioni Finestra 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>Dimensione finestra 3 (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>Connessioni Finestra 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>Dimensione finestra 4 (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>Connessioni Finestra 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>Max Seg Dimensione (byte)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>Usa MSS trovato nel&#xA;passo Percorso MTU</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>Dimensione massima segmento (bytes)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>In base alla banda del collegamento di &lt;b>%1&lt;/b> Mbps e ad un RTT di &lt;b>%2&lt;/b> ms, la finestra TCP ideale è di &lt;b>%3&lt;/b> bytes. Con &lt;b>%4&lt;/b> connessione/i e una dimensione della finestra TCP di &lt;b>%5&lt;/b> byte per ogni connessione, il throughput di trasferimento ideale sarebbe approssimativamente di &lt;b>%6&lt;/b> kbps e un file di &lt;b>%7&lt;/b> MB trasferito attraverso ciascuna connessione dovrebbe richiedere &lt;b>%8&lt;/b> secondi.</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>In base alla banda del collegamento di &lt;b>%1&lt;/b> Mbps e ad un RTT di &lt;b>%2&lt;/b> ms, la finestra TCP ideale è di &lt;b>%3&lt;/b> bytes. &lt;font color="red">Con &lt;b>%4&lt;/b> connessione/i e una dimensione della finestra TCP di &lt;b>%5&lt;/b> byte per ogni connessione, la capacità del collegamento viene superata. I risultati effettivi potrebbero essere peggiori rispetto ai risultati previsti a causa della perdita di pacchetti e di un ritardo addizionalo. Ridurre il numero di connessioni e/o la dimensione della finestra TCP per eseguire un test entro i limiti di capacità del link.&lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>Dimensione finestra (bytes)</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>Dimensioni file (MB) per connessione</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>Trova automaticamente le dimensioni del file per trasmettere per 30 secondi</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 MB)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>Numero di connessioni</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>Utilizza RTT trovato&#xA;nel passo RTT&#xA;(%1 ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>Utilizza MSS trovato&#xA;nel passo Path MTU&#xA;(%1 bytes)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>In questo passo verranno eseguiti diversi trasferimenti di connessioneTCP per verificare se il link condivide equamente o in equamente la larghezza di banda (traffic shaping).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>In questo passo verranno eseguiti diversi trasferimenti di connessioneTCP per verificare se il link condivide equamente o inequamente la larghezza di banda (traffic shaping). Perché sia possibile calcolare automaticamente la Dimensione della finestra e il Numero di connessioni, si prega di eseguire il passo RTT.</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Dimensioni finestra (KB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>Calcolato automaticamente quando&#xA;viene eseguito il passo RTT</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 bytes</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>Esegui i test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>Salta i test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>Unità di trasmissione massima (MTU).</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>Dimensione massima Segmento (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>Questo passaggio ha determinato che il Maximum Transmission Unit (MTU) è &lt;b>%1&lt;/b> byte per questo link (da un capo all'altro). Questo valore meno Layer 3/4 generale sarà utilizzato come la dimensione del Maximum Segment Size (MSS) del TCP per i passi successivi. In questo caso, il MSS è di &lt;b>%2&lt;/b> bytes.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>Riepilogo risultati RTT</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>Media RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>Min. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>Max. RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Il Round Trip Time (RTT) è stato misurato pari &lt;b>%1&lt;/b> msec. L'RTT minimo è utilizzato poiché rappresenta con la massima approssimazione la latenza intrinseca della rete. I passi successivi usano questo valore come base le prestazioni previste di trasferimento TCP.</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Il tempo roundtrip (RTT) rilevato era pari a %1 ms. Viene utilizzato l'RTT (base) medio, poiché è quello maggiormente rappresentativo della latenza inerente della rete. Le fasi successive lo utilizzano come base per le prestazioni TCP previste.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>Walk the Window in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>Walk the Window in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>Dimensione finestra 1 upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>Dimensione finestra 1 downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>Dimensione finestra 2 upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>Dimensione finestra 2 downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>Dimensione finestra 3 upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>Dimensione finestra 3 downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>Dimensione finestra 4 upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>Dimensione finestra 4 downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>Dimensione finestra 5 upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>Dimensione finestra 5 downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>Connessioni finestra 1 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>Connessioni Finestra 1 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>Connessioni finestra 2 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>Connessioni Finestra 2 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>Connessioni finestra 3 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>Connessioni Finestra 3 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>Connessioni finestra 4 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>Connessioni Finestra 4 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>Connessioni finestra 5 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>Connessioni Finestra 5 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>Finestra 1 effettiva in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>Upstream finestra 1 effettiva non soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Upstream finestra 1 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>Finestra 1 prevista in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>Finestra 2 effettiva in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>Upstream finestra 2 effettiva non soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Upstream finestra 2 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>Finestra 2 prevista in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>Finestra 3 effettiva in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>Upstream finestra 3 effettiva non soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Upstream finestra 3 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>Finestra 3 prevista in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>Finestra 4 effettiva in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Upstream finestra 4 effettiva non soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Upstream finestra 4 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>Finestra 4 prevista in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>Finestra 5 effettiva in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>Upstream finestra 5 effettiva non soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Upstream finestra 5 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>Finestra 5 prevista in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>Finestra 1 effettiva in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Downstream finestra 1 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>Downstream Finestra 1 Previsto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>Finestra 2 effettiva in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Downstream finestra 2 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>Downstream Finestra 2 Previsto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>Finestra 3 effettiva in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Downstream finestra 3 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>Downstream Finestra 3 Previsto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>Finestra 4 effettiva in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Downstream finestra 4 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>Downstream Finestra 4 Previsto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>Finestra 5 effettiva in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Downstream finestra 5 effettiva soggetto a shaping (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>Downstream Finestra 5 Previsto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Mbps Tx, Med.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>Finestra 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>Effettivo</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>Valore effettivo non soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>Valore effettivo soggetto a shaping</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>Ideale</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>Finestra 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>Finestra 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>Finestra 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>Finestra 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>I risultati del passo Walk the Window TCP mostrano il throughput effettivo rispetto a quello ideale per ogni dimensione della finestra/connessione testata. Una misura effettiva inferiore a quella ideale può essere dovuta a perdite o congestione. Se il valore effettivo è maggiore di quello ideale, la durata RTT usata come base di riferimento è troppo elevata. Il passo Throughput TCP fornisce un'analisi più approfondita dei trasferimenti TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>Throughput TCP effettivo contrapposto a ideale in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>Grafici di throughput TCP in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>Grafici di ritrasmissione velocità effettiva TCP in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>Grafici RTT velocità effettiva TCP in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>Throughput TCP effettivo contrapposto a ideale in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>Grafici di Throughput TCP in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>Grafici di ritrasmissione velocità effettiva TCP in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>Grafici RTT velocità effettiva TCP in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>Tempo di trasmissione ideale in upstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>Tempo effettivo di trasmissione upstream (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>Throughput L4 effettivo in Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Unshaped Throughput L4 effettivo in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>Shaped Throughput L4 effettivo in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>Throughput L4 ideale in upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>Efficienza TCP in Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>Efficienza TCP non soggetto a shaping in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>Efficienza TCP soggetto a shaping in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>Buffer ritardo in Upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>Ritardo buffer non soggetto a shaping in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>Ritardo buffer soggetto a shaping in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>Throughput L4 effettivo in Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Unshaped Throughput L4 effettivo in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>Shaped Throughput L4 effettivo in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>Throughput L4 ideale in downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>Efficienza TCP in Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>Efficienza TCP non soggetto a shaping in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>Efficienza TCP soggetto a shaping in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>Ritardo buffer in Downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>Ritardo buffer non soggetto a shaping in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>Ritardo buffer soggetto a shaping in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>Risultati Throughput TCP</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>Effettivo contrapposto a Ideale</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>I risultati del test a monte possono indicare:</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>Nessuna raccomandazione supplementare.</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>Durata del test insufficiente</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>Il buffer/shaper di rete deve essere ottimizzato</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>Il sistema di monitoraggio ha ridotto i pacchetti a causa dei burst del TCP.</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>Il Throughput era buono, ma sono state rilevate ritrasmissioni.</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>La rete è congestionata oppure traffic shaping in corso</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>Il CIR potrebbe non essere configurato correttamente</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>Il file è stato trasferito troppo rapidamente!&#xA;Rivedere il tempo di trasferimento previsto per il file.</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>&lt;b>%1&lt;/b> connessione/i, con dimensione finestra di &lt;b>%2&lt;/b> bytes (ciascuna), è stata utilizzata/sono state utilizzate per trasferire un file di &lt;b>%3&lt;/b> MB su ciascuna connessione (&lt;b>%4&lt;/b> MB in totale).</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b> byte di finestre TCP trasmessi su&lt;b>%2&lt;/b> connessione/i.</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>L4 Corrente (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>L4 Ideale (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>Misurazioni di trasferimento</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>Efficienza TCP&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>Ritardo di Buffer&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>I risultati del test a valle possono indicare:</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>Valore effettivo rispetto a valore ideale in upstream</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>Efficienza TCP (% )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>Ritardo di Buffer (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>I risultati dei test soggetti a shaping possono indicare:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>Valore effettivo rispetto a valore ideale in downstream</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>Grafici di Throughput</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Frm ritrasmessi</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>RTT di base:</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>Utilizzare questi grafici di correlare eventuali problemi di prestazioni a causa di ritrasmissioni TCP e/o gli effetti della congestione di rete (RTT superiore alla baseline).</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>Grafici di ritrasmissione</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>Grafici RTT</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>Throughput Ideale per connessione</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>Per un link che è sottoposto a Traffic Shaper, ogni connessione deve ricevere una porzione relativamente uguale della larghezza di banda. Per un link che è sottoposto a Criteri di traffico, ogni connessione sarà rifiutata in caso di ritrasmissioni causate dai criteri. Per ciascuna delle connessioni &lt;b>%1&lt;/b>, ogni connessione deve consumare circa &lt;b>%2&lt;/b> Mbps di banda.</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Casuale</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Test VNF TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>VNF TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>Configurazioni test</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>Connessione server avanzata</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>Configurazione avanzata test</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>Crea report a livello locale</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identificazione test</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>Fine: Visualizza risultati dettagliati</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>Fine: Crea report a livello locale</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>Crea un altro report a livello locale</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>Test MSS</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>Test RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>Test upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>Test downstream</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>La seguente configurazione è obbligatoria.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>La seguente configurazione non rientra nell'intervallo consentito.  Inserire un valore compreso tra #1 e #2.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>Il valore della seguente configurazione non è consentito.  Effettuare una nuova selezione.</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>Nessuna licenza per test da client a server.</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>Nessuna licenza per test tra server.</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>Troppi test attivi (il massimo è #1).</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>Il server locale non è riservato.</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>Il server remoto non è riservato.</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>Istanza del test già esistente.</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>Errore lettura database test.</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>Test non trovato nel database dei test.</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>Test scaduto.</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>Il tipo di test non è supportato.</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>Il server di test non è riservato.</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>Il server di test è riservato.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>Modalità richiesta non valida server test.</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>Test non consentiti sul server remoto.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>Richiesta HTTP non riuscita sul server remoto.</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>Il server remoto non dispone di risorse sufficienti.</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>Il client di test non è riservato.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>La porta del test non è supportata.</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>L'utente sta tentando di effettuare troppi test all'ora.</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>Compilazione istanza del test non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>Compilazione workflow del test non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>Richiesta HTTP workflow non valida.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>Richiesta HTTP workflow non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>I test remoti non sono consentiti.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>Si è verificato un errore nella gestione risorse.</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>Istanza del test non trovata.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>Conflitto di stato del test.</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>Lo stato del test non è valido.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>Creazione del test non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>Aggiornamento del test non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>Test creato correttamente.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>Test aggiornato correttamente.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>Richiesta HTTP non riuscita: #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>La versione del server VNF (#2) potrebbe non essere compatibile con la versione dello strumento (#1).</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>Inserire Nome utente e Chiave di autenticazione per il server al #1.</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>Inizializzazione test non riuscita: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>Test interrotto: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>Connessione server</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>L'utente non dispone delle informazioni necessarie per la connessione al server.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>Nessun collegamento disponibile per stabilire comunicazioni di rete.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>Nessun indirizzo IP origine valido per le comunicazioni di rete.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>Ping non effettuato all'indirizzo IP specificato.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>Impossibile identificare l'unità all'indirizzo IP specificato.</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>Server non ancora identificato all'indirizzo IP specificato.</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>Impossibile identificare il server all'indirizzo IP specificato.</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>Server identificato ma non autenticato.</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>Autenticazione server non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>Autorizzazione non riuscita. Tentativo di identificazione in corso.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>Utente non autorizzato o identificato. Tentativo di ping in corso.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>Server autenticato e disponibile ai fini di test.</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>Server connesso e test in esecuzione.</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>Identificazione in corso</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>Identifica</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>Versione proxy TCP</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>Versione Test Controller</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>Collegamento attivo:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID del server:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>Stato del server:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>Impostazioni avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>Impostazioni avanzate connessione</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>Chiave di autenticazione</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>Memorizza nomi utente e chiavi</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>Unità locale</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>Server</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Durata Window Walk (secondi)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>Durata automatica</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>Configurazioni test (avanzate)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>Parametri avanzati test</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>Porta TCP</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>Porta TCP automatica</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Numero di Window Walks</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>Numero di connessioni</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>Numero di connessioni automatiche</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Finestra di saturazione</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>Esegui finestra di saturazione</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>Incremento connessione (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>Informazioni report server</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nome test</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nome del Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>Nome cliente*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Società</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>E-mail*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Telefono</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Commenti</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>Mostra ID test</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>Ignora test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>Server non connesso.</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>Media (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>In attesa che la risorsa di test sia pronta.  L'utente è al #%1 posto della lista d'attesa.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Finestra</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>Finestra&#xA;di Saturazione</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>Dimensione finestra (kB)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>Connessioni</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>Diagnosi upstream:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>Niente da segnalare</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>Velocità effettiva troppo bassa</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>RTT incoerente</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>Efficienza TCP bassa</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>Ritardo elevato del buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>Velocità effettiva inferiore all'85% del CIR</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>MTU inferiore a 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>Diagnosi downstream:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Codice di autorizzazione</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Data di creazione dell'autorizzazione</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>Data di scadenza</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>Tempo di modifica</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Ora di arresto del test</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>Data ultima modifica</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>E-mail</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>Risultati velocità effettiva in upstream</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>Valore effettivo rispetto a valore di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>Valore effettivo rispetto a valore di destinazione in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>Riepilogo upstream</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>Velocità effettiva di picco TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>MSS TCP (byte)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>Tempo round trip (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>Risultati riepilogo upstream (finestra massima velocità effettiva)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>Dimensione finestra per connessione (kB)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>Finestra di aggregazione (kB)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>TCP Throughput (Mbps) desiderato</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>Velocità effettiva media TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>Throughput TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>Destinazione</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>Finestra 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>Finestra 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>Finestra 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>Finestra 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>Finestra 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>Finestra 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>Finestra massima velocità effettiva:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>Finestra da %1 kB: %2 conn. x %3 kB</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>Grafici velocità effettiva in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>Dettagli upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 1 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>Dimensione finestra per connessione (kB)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>Throughput effettivo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>Velocità effettiva destinazione (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>Ritrasmissioni totali</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 2 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 3 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 4 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 5 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 6 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 7 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 8 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 9 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 10 in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>Risultati velocità effettiva finestra di saturazione in upstream</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>Throughput medio (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>Risultati  velocità effettiva in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>Valore effettivo rispetto a valore di destinazione in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>Riepilogo downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>Risultati riepilogo downstream (finestra massima velocità effettiva)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>Grafici velocità effettiva in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>Dettagli downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 1 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 2 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 3 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 4 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 5 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 6 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 7 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 8 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 9 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>Risultati  velocità effettiva finestra 10 in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>Risultati velocità effettiva finestra di saturazione in downstream</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation>N. Window Walks</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>Finestre sovrasature (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>Connessioni sovrasature (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>Scansione VLAN</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 Avvio del test VLAN #2</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>Esecuzione del test dell'ID VLAN #1 per #2 secondi in corso...</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>ID VLAN  #1: RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>ID VLAN  #1: NON RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>Loop attivo non riuscito. Test non riuscito per VLAN ID #1</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>ID VLAN totali testati</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>Numero di ID riusciti</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>Numero di ID non riusciti</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>Durata per ID (secondi)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>Numero di intervalli</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>Intervalli selezionati</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>VLAN ID Min</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID Max</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Avvia test</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>ID totali</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>ID riusciti</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>ID non riusciti</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>Impostazioni avanzate analisi VLAN</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>Larghezza della banda (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>Criteri di superamento</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>Nessun frame perso</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>Sono stati ricevuti alcuni frame</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>Configura TrueSAM ...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>Esegui TrueSAM ...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>Tempo di esecuzione stimato</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Arresto su errore</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Visualizza rapporto</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>Visualizza rapporto TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Superato</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrestato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>Perdita segnale.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>Link perso.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>La comunicazione con l'apparecchio di prova remoto è stata persa. Ristabilire il canale di comunicazione e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Si è verificato un problema durante il tentativo di configurare la serie di test locali, la comunicazione con l'apparecchiatura di prova è stata interrotta. Ristabilire il canale di comunicazione e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Si è verificato un problema durante il tentativo di configurare l'apparecchiatura di prova remota, la comunicazione con l'apparecchiatura di prova è stata interrotta. Ristabilire il canale di comunicazione e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>È stato rilevato un problema durante la configurazione dell'apparecchiatura di prova locale, TrueSAM è terminato.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Si è verificato un problema durante il tentativo di configurare l'apparecchiatura di prova locale. Il test si è dovuto interrompere, e il canale di comunicazione con l'apparecchiatura di prova remota è stato perso.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Si è verificato un problema durante il tentativo di configurare l'apparecchiatura di prova remoto. Il test si è dovuto interrompere, e il canale di comunicazione con l'apparecchiatura di prova remota è stato perso.</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>Il salvaschermo è stato disattivato per evitare interferenze durante il test.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>Si è verificato un problema con l'apparecchiatura di prova locale. TrueSAM è terminato.</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>Controllo parametri DHCP.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>Parametri DHCP ricevuti.</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>Inizializzazione interfaccia Ethernet in corso. Si prega di attendere.</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>Nord America</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>Nord America e Corea </translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>Nord America PCS</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>India</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>Perdita dell'ora da fonte esterna, causerà l'apparente mancanza di sinc. 1PPS.</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>Avvia sincronizzazione con segnale Ora da sorgente esterna ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>Sincronizzazione con segnale Ora da sorgente esterna riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>Disattiva&#xA;Loop</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>Copertine</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>Seleziona test da eseguire</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>Test di connettività del traffico da un capo all'altro</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>RFC 2544 avanzata / SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>Suite di prove di valutazione delle prestazioni Ethernet</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Configurazione dei servizi Ethernet Y.1564 e test delle prestazioni</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>Test di trasparenza Layer 2 per Control Plane Frame (CDP, STP, ecc).</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>Test di throughput e prestazioni ﻿TCP RFC 6349 </translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>Tipo L3-Source</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>Impostazioni per il canale di comunicazione (utilizzando il Servizio 1)</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>Impostazioni del canale di comunicazione</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>Il Servizio 1 deve essere configurato per accordarsi col flusso 1 sullo strumento di prova Viavi remoto.</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>Stato locale</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>Sinc ToD</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>Sinc 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>Esegui connessione&#xA;al canale</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>Layer fisico</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>Lunghezza pausa (Quantum)</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>Lunghezza pause (Tempo - ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>Esegui&#xA;i test</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>Avviamento&#xA;dei test</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>Arresto&#xA;dei test</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>Tempo stimato per eseguire i test</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Totale</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>Stop su errore</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>Criteri di Successo/Insuccesso per i test scelti</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>Upstream e downstream</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>Questo non ha attinenza con Successo/Insuccesso, quindi questa impostazione non si applica.</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>Esito positivo se sono soddisfatte le seguenti soglie:</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>Non ci sono soglie fissate - non riporterà Successo/Insuccesso.</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>Locale:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Remoto:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>Locale e remoto:</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>Velocità effettiva (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>Velocità di trasmissione (L1 Kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>Velocità effettiva (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolleranza perdita di frame (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>Latenza (µs)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>Esito positivo se i seguenti parametri SLA sono soddisfatti:</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>Upstream:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>Downstream:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>Throughput CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>Velocità di trasmissione CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>Velocità di trasmissione CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>Throughput EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>Velocità di trasmissione EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>Velocità di trasmissione EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - Tolleranza (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - Tolleranza (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - Tolleranza (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>Ritardo frame (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>Variazione ritardo (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>L'esito sarà determinato internamente.</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>Esito positivo se il throughput TCP misurato soddisfa la seguente soglia:</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>Percentuale del rendimento previsto</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>Test Throughput TCP non abilitato - l'esito non sarà generato</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>Arresta i test</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>Questo interromperà il test attualmente in esecuzione e l'esecuzione di ulteriori test. Arrestare?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo totale per i(l) servizi(o) #1 è 0 o supera i Mbps della velocità della linea L1 #2.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo totale per la direzione upstream per i(l) servizi(o) #1 è 0 o supera i #2 Mbps L1 della velocità della linea.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo totale per la direzione downstream per i(l) servizi(o) #1 è 0 o supera i Mbps della velocità della linea L1 #2.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo totale per i(l) servizi(o) #1 è 0 o supera i Mbps della velocità della rispettiva linea L2.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo totale per la direzione upstream per i(l) servizi(o) #1 è 0 o supera i Mbps della velocità della rispettiva linea L2.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo totale per la direzione downstream per i(l) servizi(o) #1 è 0 o supera i Mbps della velocità della rispettiva linea L2.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo supera i Mbps della velocità della linea L1 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo supera i Mbps della velocità della linea L1 #1 nella direzione upstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo supera i Mbps della velocità della linea L1 #1 nella direzione downstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo supera i Mbps della velocità della linea L2 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo supera i Mbps della velocità della linea L2 #1 nella direzione upstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il carico massimo supera i Mbps della velocità della linea L2 #1 nella direzione downstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>Configurazione non valida:&#xA;&#xA;CIR, EIR e Policing non possono essere impostati tutti su 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>Configurazione non valida:&#xA;&#xA;CIR, EIR e Policing non possono essere impostati tutti su 0 nella direzione UPstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>Configurazione non valida:&#xA;&#xA;CIR, EIR e Policing non possono essere impostati tutti su 0 nella direzione Downstream.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Non è possibile ottenere il Carico passo più piccolo(#1% di CIR) usando l'impostazione CIR #2 Mbps per il Servizio #3. Il valore passo più piccolo usando il CIR corrente per il Servizio #3 è pari al #4%.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;La CIR (L1 Mbps) totale è pari a 0 o supera i Mbps della velocità della linea L1 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;La CIR (L1 Mbps) totale per la direzione upstream è pari a 0 o supera i Mbps della velocità della linea L1 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;La CIR (L1 Mbps) totale per la direzione downstream è pari a 0 o supera i Mbps della velocità della linea L1 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;La CIR (Mbps L2) totale è pari a 0 o supera i Mbps della velocità della linea L2 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;La CIR (Mbps L2) totale per la direzione upstream è pari a 0 o supera i Mbps della velocità della linea L2 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configurazione non valida:&#xA;&#xA;La CIR (Mbps L2) totale per la direzione downstream è pari a 0 o supera i Mbps della velocità della linea L2 #1.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>Configurazione non valida:&#xA;&#xA;si deve selezionare o il Test configurazione servizio o il Test prestazioni di servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Deve essere selezionato almeno un servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Se si intende eseguire un test sulle prestazioni del servizio, il totale dei CIR (o degli EIR per i servizi con CIR uguale a 0) dei servizi selezionati non può superare la velocità della linea.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il valore massimo specifica per la Misura del Throughput (RFC 2544) non può essere inferiore alla somma dei valori CIR per i servizi selezionati quando si esegue il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il valore massimo specifica per la Misura del Throughput (RFC 2544) in direzione Upstream non può essere inferiore alla somma dei valori CIR per i servizi selezionati quando si esegue il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il valore massimo specifica per la Misura del Throughput (RFC 2544) non può essere inferiore alla somma dei valori CIR per i servizi selezionati quando si esegue il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il valore massimo specifica per la Misura del Throughput (RFC 2544) non può essere inferiore al valore CIR quando si esegue il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il valore massimo specificato per la Misura del Throughput (RFC 2544) in Upstream non può essere inferiore al valore CIR quando si esegue il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il valore massimo specificato per la Misura del Throughput (RFC 2544) in Downstream non può essere inferiore al valore CIR quando si esegue il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>SAMComplete si è fermato perché era selezionato Stop su errore e almeno un KPI non soddisfa l'SLA FOR Service #1.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete si è fermato perché nessun dato è stato restituito sul Servizio #1 entro 10 secondi dall'avvio del traffico. L'estremità distante potrebbe avere interrotto il loop di rinvio del traffico.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete si è fermato perché nessun dato è stato restituito sul Servizio #1 entro 10 secondi dall'avvio del traffico. L'estremità distante potrebbe avere interrotto la trasmissione del traffico.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete si è fermato perché nessun dato è stato restituito entro 10 secondi dall'avvio del traffico. L'estremità distante potrebbe avere interrotto il loop di rinvio del traffico.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAMComplete si è fermato perché nessun dato è stato restituito entro 10 secondi dall'avvio del traffico. L'estremità distante potrebbe avere interrotto la trasmissione del traffico.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione locale è un'applicazione Streams e l'applicazione remota all'indirizzo IP #1 è un'applicazione Traffico.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione locale è un'applicazione Streams e l'applicazione remota all'indirizzo IP #1 è un'applicazione TCP Wirespeed.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione locale è un'applicazione Layer 3 e l'applicazione remota all'indirizzo IP #1 è un'applicazione Layer 2.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>Le applicazioni locali e quelle remote sono incompatibili. L'applicazione locale è un'applicazione Layer 2 e l'applicazione remota all'indirizzo IP #1 è un'applicazione Layer 3.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>L'applicazione remota all'indirizzo IP #1 è impostata per WAN IP. Non è compatibile con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>L'applicazione remota all'indirizzo IP #1 è predisposta per l'imbustamento per stacked VLAN. Non è compatibile con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>L'applicazione remota all'indirizzo OP #1 è impostata per l'incapsulamento VPLS. Non è compatibile con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>L'applicazione remota all'indirizzo IP #1 è impostata per l'incapsulamento MPLS. Non è compatibile con SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>L'applicazione remota supporta solo #1 servizi. </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Il test del Ritardo unidirezionale (OWD) richiede la presenza della sincronizzazione sorgente del tempo di ritardo unidirezionale (OWD) sia sull'unità locale sia su quella remota.  Sincronizzazione OWD non riuscita sull'unità locale.  Verificare tutte le connessioni dei dispositivi sorgente del tempo di OWD.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Il test del Ritardo unidirezionale (OWD) richiede la presenza della sincronizzazione sorgente del tempo di ritardo unidirezionale (OWD) sia sull'unità locale sia su quella remota.  Sincronizzazione OWD non riuscita sull'unità remota.  Verificare tutte le connessioni dei dispositivi sorgente del tempo di OWD.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Prima di eseguire il testo SAMComplete è necessario eseguire un test RTT (Round-Trip Time).</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossibile stabilire la connessione TrueSpeed. Andare alla pagina "Rete", verificare le configurazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossibile stabilire una sessione TrueSpeed per la direzione Upstream. Aprire la pagina "Rete", verificare le configurazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossibile stabilire una sessione TrueSpeed per la direzione Downstream. Aprire la pagina "Rete", verificare le configurazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>Il test RTT (Round-Trip Time) è stato reso non valido dalla modifica dei servizi da testare correntemente selezionati. Accedere alla pagina "Controlli TrueSpeed" e rieseguire il test RTT.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il CIR totale (Mbps) per la direzione Downstream dei/l servizi(o) #1 non può essere 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il CIR totale (Mbps) per la direzione Upstream dei/l servizi(o) #1 non può essere 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>Configurazione non valida:&#xA;&#xA;Il CIR (Mbps) per i servizi #1 non può essere 0.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Nessun traffico ricevuto. Aprire la pagina "Rete", verificare le configurazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>Vista risultato principale</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>Arresta&#xA;i test</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>  Report creato, fare clic su Avanti per visualizzarlo</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>Salta la visualizzazione del rapporto</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete&#xA;Test di attivazione Servizio Ethernet</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>con TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>Impostazioni della rete locale</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>L'unità locale non necessita di configurazione.</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>Impostazioni di rete remoto</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>L'unità remota non necessita di configurazione.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>Impostazioni IP locali</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>Impostazioni IP remote</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Servizi</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>Tagging</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>Il Tagging non viene utilizzato.</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>OP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>Throughput SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>Policing SLA</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>Nessun test di policing selezionato.</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>Burst SLA</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>L'esecuzione di test di burst è stata disattivata a causa dell'assenza della funzionalità richiesta.</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>Prestazioni SLA</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>TrueSpeed è disattivato.&#xA;&#xA;TrueSpeed può essere attivato sulla pagina di configurazione "Rete" quando non ci si trova in modalità di misurazione Loopback.</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>Impostazioni avanzate locali</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>Impostazioni traffico avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>Impostazioni LBM avanzate</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>Impostazioni SLA avanzate</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>Dimensione EMIX/casuale</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>IP avanzato</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 avanzato</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>Tagging avanzato</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>Test config servizio</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>Test prest. servizio</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Esci da Test Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>Stato di Discovery Server</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>Messaggio di Discovery Server</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>Indirizzo MAC e modalità ARP</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>Selezione SFP</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>OP di origine WAN</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>IP WAN - statico</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>Gateway WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>Subnet mask WAN</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>IP origine traffico</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>Subnet mask traffico</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>sulla porta di rete locale è utilizzato il VLAN Tagging?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q (Stacked VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>Attesa</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>Accesso al server...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>Impossibile accedere al Server</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP ottenuto</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>Lease garantito: #1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>Lease ridotto: #1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>Lease rilasciato</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>Discovery Server:</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 ms FTD, #3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>Servizio 1: VoIP - 25% di traffico - frame #1 e #2 byte</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>Servizio 2: Videotelefonia - 10% di traffico - frame #1 e #2 byte</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>Servizio 3: Dati prioritari - 15% di traffico - frame #1 e #2 byte</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>Servizio 4: Dati BE- 50% di traffico - frame #1 e #2 byte</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>Tutti i servizi</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 1</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voice</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 U law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 A law 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 U law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 A law 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>A 10GE, il livello di granularità di larghezza della banda è di 0,1 Mbps; di conseguenza, il valore CIR è un multiplo di questo livello di granularità</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>A 40GE, il livello di granularità di larghezza della banda è di 0,4 Mbps; di conseguenza, il valore CIR è un multiplo di questo livello di granularità</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>A 100GE, il livello di granularità di larghezza della banda è di 1 Mbps; di conseguenza, il valore CIR è un multiplo di questo livello di granularità</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>Configurare dimensioni</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>Dimensioni frame (byte)</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>Lunghezza definita</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>Lunghezza ciclo EMIX</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Lungh. pacchetto (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Lunghezza frame calcolata</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>La dimensione frame calcolata viene determinata utilizzando la Lunghezza del pacchetto e l'incapsulamento.</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 2</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 3</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 4</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 5</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 6</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 7</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 8</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 9</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>Proprietà Triple Play Servizio 10</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>Sottodimensionato</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>Numero di servizi</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>Layer</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>Impostazioni LBM</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>Configura Triple Play...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>Proprietà del servizio</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>Impostazioni DA MAC e Dimensione Frame</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>MAC DA, impostazioni dimensione frame ed EtherType</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>Impostazioni DA MAC, Lunghezza Pacchetto e TTL</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>Impostazioni di DA MAC e Lunghezza dei Pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>Impostazioni rete (avanzate)</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>Un'incompatibilità tra unità locale e remota richiede una dimensione pacchetto di &lt;b>%1&lt;/b>  byte o superiore.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>Servizio</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configura...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>  Dimensione utente</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL (hops)</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>Indirizzo MAC di dest.</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>Mostra entrambi</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>Solo remoto</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>Solo locale</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>Impostazioni LBM (avanzate)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>Dimensione EMIX/casuale impostazioni di rete</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 1 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>Dimensione frame 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>Lunghezza pacchetto 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>Dimensione utente 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>Dimensione jumbo 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>Dimensione frame 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>Lunghezza pacchetto 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>Dimensione utente 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>Dimensione jumbo 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>Dimensione frame 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>Lunghezza pacchetto 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>Dimensione utente 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>Dimensione jumbo 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>Dimensione frame 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>Lunghezza pacchetto 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>Dimensione utente 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>Dimensione jumbo 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>Dimensione frame 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>Lunghezza pacchetto 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>Dimensione utente 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>Dimensione jumbo 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>Dimensione frame 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>Lunghezza pacchetto 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>Dimensione utente 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>Dimensione jumbo 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>Dimensione frame 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>Lunghezza pacchetto 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>Dimensione utente 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>Dimensione jumbo 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>Dimensione frame 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>Lunghezza pacchetto 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>Dimensione utente 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>Dimensione jumbo 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 1 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 1</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 2 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 2 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 2</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 3 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 3 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 3</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 4 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 4 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 4</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 5 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 5 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 5</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 6 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 6 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 6</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 7 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 7 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 7</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 8 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 8 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 8</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 9 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 9 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 9</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali servizio 10 (remote)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali servizio 10 (locali)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali servizio 10</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>I servizi hanno VLAN ID o Priorità utente diverse?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>No, tutti i servizi di utilizzare le stesse impostazioni VLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Bit DEI</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>Avanzato...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>Pri. utente</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>Pri SVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>Impostazioni VLAN Tagging (incapsulamento)</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>Priorità SVLAN</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>Tutti i servizi utilizzeranno lo stesso IP di destinazione del traffico.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>IP di destinazione del traffico</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest OP</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>Destinazione traffico IP</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>Impostare incremento ID IP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>Impostazioni IP (solo locale)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>Impostazioni OP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>Impostazioni IP (solo remoto)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>Impostazioni OP (avanzate)</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>Impostazioni OP avanzate</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>I servizi hanno impostazioni DSCP o TOS diverse?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>No, TOS/DSCP sono gli stessi per tutti i servizi</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>SLA aggregati</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>CIR aggregato</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>CIR aggregato in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>CIR aggregato in downstream</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>EIR aggregato</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>EIR aggregato in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>EIR aggregato in downstream</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>Modalità aggregata</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>Max. test Throughput Locale (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>Max. test Throughput Remoto (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>Attiva modalità aggregata</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>ATTENZIONE: La somma dei valori del peso selezionato ammonta attualmente a &lt;b>%1&lt;/b>% e non al 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>Throughput SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>SLA Throughput, L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>SLA Throughput, L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>SLA Throughput, L1 Mbps (One Way)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>SLA Throughput, L2 Mbps (One Way)</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>Peso (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Policing</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>Carico massimo</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>Solo downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>Solo upstream</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>Specifica impostazioni traffico avanzate</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>Criteri SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>Max Policing</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>Esegui test di Burst</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>Tolleranza (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>Tolleranza (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>Eseguire i burst test?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>Tipo di test di burst:</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>Specifica impostazioni Burst avanzate</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>Precisione ritardo frame</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>Includi ritardo frame come requisito SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>Includi variazione ritardo frame come requisito SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>Prestazioni SLA (unidirezionale)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Ritardo frame (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Ritardo frame (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>Specifica impostazioni SLA avanzate</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>Configurazione servizio</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>Numero di passi sotto CIR</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>Durata passo (sec)</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>CIR % Passo 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>CIR % Passo 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>CIR % Passo 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>CIR % Passo 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>CIR % Passo 5</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>CIR % Passo 6</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>CIR % Passo 7</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>CIR % Passo 8</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>CIR % Passo 9</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>CIR % Passo 10</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>Percentuali passo</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>Prestazioni servizio</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>Ciascuna direzione è testata separatamente, quando la durata complessiva della prova sarà pari al doppio del valore inserito.</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Arresta prova su errore</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>Impostazioni test burst avanzato</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>Salta J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Seleziona i test Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>Seleziona servizi da sottoporre a test</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>Imposta tutto</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  Totale:</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>Test configurazione servizio</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>Test prestazioni servizio</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>Misure opzionali</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>Throughput (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>Max. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>Max. (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>Max. (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>Non è possibile eseguire le misurazioni opzionali quando è stato attivato un servizio TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Esegui i test Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Salta test Y.1564</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>Var ritardo</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>Riepilogo dei test non superati</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>Verdetti</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Verdetto Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>Vedretto test di configurazione</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>Verdetto Test di config Svz 1</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>Verdetto Test di config Svz 2</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>Verdetto Test di config Svz 3</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>Verdetto Test di config Svz 4</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>Verdetto Test di config Svz 5</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>Verdetto Test di config Svz 6</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>Verdetto Test di config Svz 7</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>Verdetto Test di config Svz 8</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>Verdetto Test di config Svz 9</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>Verdetto Test di config Svz 10</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Verdetti Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Verdetto Svz 1 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Verdetto Svz 2 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Verdetto Svz 3 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Verdetto Svz 4 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Verdetto Svz 5 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Verdetto Svz 6 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Verdetto Svz 7 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Verdetto Svz 8 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Verdetto Svz 9 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Verdetto Svz 10 Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Verdetto IR Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Verdetto perdita frame Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Verdetto ritardo Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Verdetto variazione ritardo Test prest</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Verdetto TrueSpeed Test prest</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>Risultati configurazione servizio</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>Risultati di configurazione servizio 1</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>Max. throughput (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>Max. throughput downstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>Max. throughput upstream (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>Max. throughput (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>Max. throughput downstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>Max. throughput upstream (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>Verdetto CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>Verdetto CIR downstream</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>Variazione ritardo frame (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>Conteggio OoS</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>Rilevazione frame errore</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>Rileva pausa</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>Verdetto CIR upstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>Verdetto CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>Verdetto CBS downstream</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>Burst Size configurato (kB)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Dimensione burst Tx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>Dim. MEDIA Burst Rx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>CBS stimato (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>Verdetto CBS upstream</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>Verdetto EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>Verdetto EIR downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>Verdetto EIR upstream</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>Verdetto Policing</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>Verdetto Policing downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>Verdetto Policing upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>Verdetto Passo 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>Verdetto Passo 1 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>Verdetto Passo 1 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>Verdetto Passo 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>Verdetto Passo 2 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>Verdetto Passo 2 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>Verdetto Passo 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>Verdetto Passo 3 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>Verdetto Passo 3 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>Verdetto Passo 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>Verdetto Passo 4 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>Verdetto Passo 4 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>Verdetto Passo 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>Verdetto Passo 5 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>Verdetto Passo 5 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>Verdetto Passo 6</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>Verdetto Passo 6 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>Verdetto Passo 6 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>Verdetto Passo 7</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>Verdetto Passo 7 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>Verdetto Passo 7 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>Verdetto Passo 8</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>Verdetto Passo 8 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>Verdetto Passo 8 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>Verdetto Passo 9</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>Verdetto Passo 9 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>Verdetto Passo 9 upstream</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>Verdetto Passo 10</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>Verdetto Passo 10 downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>Verdetto Passo 10 upstream</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>Max. throughput (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>Max. throughput (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>Max. throughput (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>Passi</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>Chiave</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>Cliccare sulle barre per visualizzare i risultati di ciascun passo.</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (Throughput Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (Throughput L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (Throughput L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>Rilevazione errori</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>Passaggio 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>Passaggio 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>Passaggio 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>Passaggio 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>Passaggio 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>Passaggio 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>Passaggio 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>Passaggio 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>Passaggio 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>Passaggio 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>Soglie SLA</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>Risultati di configurazione servizio 2</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>Risultati di configurazione servizio 3</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>Risultati di configurazione servizio 4</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>Risultati di configurazione servizio 5</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>Risultati di configurazione servizio 6</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>Risultati di configurazione servizio 7</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>Risultati di configurazione servizio 8</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>Risultati di configurazione servizio 9</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>Risultati di configurazione servizio 10</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>Risultati prestazioni servizio</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Verdetto serv.</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR Corr.</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR Max.</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR Min.</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR Media</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>Secondi perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>Conteggio Frame persi</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>Ritardo corr.</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>Max. ritardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>Min. ritardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>Media ritardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>Var. ritardo Corr.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>Var. ritardo Max</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>Var. ritardo Min</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>Var. ritardo Media</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>Disponibilità</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponibile</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>Secondi disponibili</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>Secondi non disponibili</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>Secondi con errori gravi</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>Throughput prest. servizio</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variazione ritardo</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>Servizio TrueSpeed. Visualizza i risultati sulla pagina dei risultati di TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR, Media&#xA;(Throughput&#xA;L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR, Media&#xA;(Throughput&#xA;L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>Tasso di&#xA;perdita frame</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>Ritardo unidirezionale (OWD) -&#xA;Medio (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>Var. ritardo&#xA;Max. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>Errori rilevati</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>Ritardo&#xA;Med. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, Corr.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, Corr.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, Max.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, Max.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, Min.&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, Min.&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>Conteggio&#xA;frame persi</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>Perdita frame&#xA;Rapporto&#xA;Soglia</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>Ritardo&#xA;Corr (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>Ritardo&#xA;Max. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>Ritardo&#xA;Min (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>Ritardo&#xA;Medio (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>Soglia&#xA;ritardo&#xA;(OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>Ritardo&#xA;Corr (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>Max&#xA;ritardo (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>Ritardo&#xA;Min. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>Soglia&#xA;ritardo&#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>Var. ritardo&#xA;Corr (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>Var. ritardo&#xA;Media (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>Var. ritardo&#xA;Soglia (ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>Rapporto di&#xA;Secondi&#xA;Disponibili</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>Secondi&#xA;non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>Secondi&#xA;con errori&#xA;gravi</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Throughput&#xA;Configurazione del servizio&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Throughput&#xA;Configurazione del servizio&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Throughput&#xA;Perf. servizio&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Throughput&#xA;Perf. servizio&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>Impostazioni di rete</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Dimensione frame utente</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>Dimensione frame Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>Lunghezza pacchetto utente </translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>Lunghezza pacchetto Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>Dimensioni calcolate frame (byte)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>Impostazioni di rete (dimensione EMIX/casuale)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>Lunghezze EMIX/casuali (remote)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>Lunghezze EMIX/casuali (locali)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>Lunghezze EMIX/casuali</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>Impostazioni OP avanzate - Remoto</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>CIR in Downstream </translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>EIR in Downstream </translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>Policing in Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>M in Downstream </translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>CIR Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>EIR Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>Policing in Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>M Upstream</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>SLA Burst avanzato</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>Tasso di perdita frame in Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>Ritardo frame in Downstream (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>Ritardo frame in Downstream (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>Variazione ritardo in Downstream (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>Tasso di perdita frame in Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>Ritardo frame in Upstream (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>Ritardo frame in Upstream (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>Variazione ritardo in Upstream (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>Includi come requisito SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>Includi ritardo frame</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>Includi variazione ritardo frame</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete presenta le seguenti impostazioni di configurazione non valide:</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>Risultati configurazione servizio</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>Esegui TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>NOTA: Il test TrueSpeed può essere eseguito solo se è abilitato il test Prestazioni servizio.</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>Imposta lunghezza Pacchetto TTL</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>Imposta porte TCP/UDP</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>Indirizzo IP di orig. Tipo</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Src. Porta</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>Dest Tipo</translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>Dest Porta</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>Soglia throughput TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>Dimensione totale finestra consigliata (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>Dimensione totale finestra incrementata (byte)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>N. di connessioni consigliato</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>N. connessioni incrementate (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>Dimensione totale finestra consigliata in Upstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>Dimensione totale finestra incrementata in upstream (byte)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>N. di connessioni consigliato in Upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>Numero di connessioni incrementate in upstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>Dimensione totale finestra consigliata in Downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>Dimensione totale finestra incrementata in downstream (byte)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>N. di connessioni consigliato in Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>N. di connessioni incrementate in downstream (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>N. di connessioni consigliato</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>Throughput TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Payload Acterna</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>Tempo Round-Trip (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>L'RTT verrà utilizzato nelle fasi successive per creare una dimensione finestra e una serie di raccomandazioni di connessione.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>Risultati TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>Misure di trasferimento TCP</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>Obiettico L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>Verdetto servizio TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>Verdetto servizio TrueSpeed in Upstream</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>Throughput TCP effettivo (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>Throughput TCP effettivo in Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>Throughput TCP desiderato in Upstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>TCP Throughput (Mbps)) desiderato</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>Verdetto servizio TrueSpeed in Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>Throughput TCP effettivo in Downstream (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>Throughput TCP desiderato in Downstream (Mbps)</translation>
        </message>
    </context>
</TS>
