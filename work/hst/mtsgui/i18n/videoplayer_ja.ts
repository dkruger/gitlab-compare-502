<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi ﾋﾞﾃﾞｵ ﾌﾟﾚｰﾔー</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>ﾋﾞﾃﾞｵ ﾌｧｲﾙ名</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>時間:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>ﾋﾞﾃﾞｵ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>ｽｹｰﾘﾝｸﾞとﾄﾘﾐﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>ｽｹｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>&amp; ﾌｧｲﾙを開く ...</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>ｱｽﾍﾟｸﾄ比</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>ｵｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>ｽｹｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>ｽｹｰﾙ ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>画面に合わせて表示</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>ｽｹｰﾘﾝｸﾞとﾄﾘﾐﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>ﾌｧｲﾙを開く ...</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>ﾏﾙﾁﾒﾃﾞｨｱ (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>開く</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>ｵｰﾌﾟﾝ ﾒﾃﾞｨｱがありません</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>読み込み中 ...</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>ﾊﾞｯﾌｧﾘﾝｸﾞ...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>再生中 ...</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>一時停止</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>ｴﾗｰ...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>ｱｲﾄﾞﾙ - ﾒﾃﾞｨｱを停止しています</translation>
        </message>
    </context>
</TS>
