<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation> Хотите отменить ?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>Ошибка чтения файла</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>Слишком большое количество кадров в файле ( более 50 000). Попробуйте сохранить часть данных в буфере &#xA; или экспортируйте их на диск USB, а затем загрузите на другое устройство.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>Удалить все сохраненные данные?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>Вы действительно хотите удалить выбранный элемент?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>Название уже существует. &#xA;Заменить существующие данные?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>При записи графиков на диск возникли ошибки. Сохраните графики , чтобы сберечь &#xA; результаты своей работы. По достижении критического уровня графики будут автоматически остановлены и удалены </translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>При записи графиков на диск возникло слишком много ошибок. Запись графиков была прекращена.&#xA; Данные в графиках удалены. В случае необходимости повторите запись графиков.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>UsbFlash</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>Съемное устройство</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>Переполнение журнала</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>Неверная конфигурация :&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн.</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Недействительн.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>Голос.</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Данные 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Данные 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Программная поддержка версии спецификации интерфейса управления CFP MSA</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Программная поддержка версии спецификации интерфейса управления QSFP MSA</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>Информация DMC</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>Серийный номер</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>Штрих-код</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>Дата изготовления:</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>Дата градуировки</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>Идентификатор опции</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>Серийный номер агрегата</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>Штрих - код сборки</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Вер</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>Установленное программное обеспечение</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Параметры:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>Ожидание пакетов ...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>Анализ пакетов ...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Анализ</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>Ожидание линии связи ...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>�ажать " Начать анализ...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Подождите, сохранение результатов.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Не удалось сохранить результаты.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Результаты сохранены.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>Ожидание</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>Предыдущий тест был остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>Предыдущий тест был завершен с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>Сбой предыдущего теста , включение остановки при сбое</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Не запущено</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>Оставшееся время: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>Тест невозможно закончить , он был завершен с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Завершено</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>Проверка завершена</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>Тест завершен - обнаружены сбои</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Пройден</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>Тест завершен - все тесты прошли успешно</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>Тест был остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Остановка</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Не запущенный</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн .</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Запуск</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>Подключение прервано</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>Утеряно соединение</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>Проверка подключения</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Неверная конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Подождите, сохранение результатов.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Не удалось сохранить результаты.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Результаты сохранены.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>График теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Результаты испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Верхний график теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Верхние результаты теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Нисходящий график теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Нисходящие результаты теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Отклонения от нормы</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>Высходящий коэффициент потерь</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>Нисходящий коэффициент потерь</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>График теста  времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Результаты теста времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Верхний график теста времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Верхние результаты времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Нисходящий график теста времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Нисходящие результаты времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>График теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Результаты теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Верхний график теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Верхние результаты теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Нисходящий график теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Нисходящие результаты теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 байт тестового графика потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 байт графика результатов теста потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 направленный вверх график потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 направленный вверх график результатов теста потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 байт нисходящего тестового графика потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 байт нисходящего тестового графика результатов теста потери кадра</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Результаты теста CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Верхние результаты теста CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Нисходящие результаты теста CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Результаты теста политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Верхние результаты теста политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Нисходящие результаты теста политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Результаты теста поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Верхние результаты теста поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Нисходящие результаты теста поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Результаты испытания взаимного процесса</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Верхние последовательные настройки теста</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Нисходящие последовательные настройки теста</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>График теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Результаты испытания восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Верхний график теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Верхние результаты теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Нисходящий график теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Нисходящие результаты теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Результаты тестов длительной нагрузки</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>Шаг Пути MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>RTT шаг</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>Окно обратного потока – шаг</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>Окно выходного ключевого потока – шаг</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>Шаг пропускной способности обратного потока TCP</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>Шаг пропускной способности выходного ключевого потока TCP</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Подождите, сохранение результатов.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Не удалось сохранить результаты.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Результаты сохранены.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Синхронизация невозможна. Убедитесь, что все кабели подключены и порт настроен правильно.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Не удалось найти активный канал. Убедитесь, что все кабели подключены и порт настроен правильно.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Не удалось определить скорость или дуплекс подключения к сети. Убедитесь, что все кабели подключены и порт настроен правильно.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>Невозможно получить IP адрес локальной испытательной установки . Проверьте параметры подключения и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>Невозможно установить канал связи с удаленным испытательным прибором . Проверьте параметры подключения и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>На удаленном прибореустановлена несовместимая версия ПО BERT. Минимальная совместимая версия: %1.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>Для запуска TrueSpeed, опция Layer 4 TCP Wirespeed должна быть установлена для локального и удаленного устройства.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>Нельзя выбрать тест TrueSpeed для данных параметров инкапсуляции . Он будет удален .</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>Выбор диагностики для TrueSpeed является неверным для установки данного типа фрейма и будет удален .</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>Нельзя выбрать тест TrueSpeed для дистанционной инкапсуляции . Он будет удален .</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>Выбор тестов SAM-Complete является неверным для установки енкапсуляции и будет удален .</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Неверная конфигурация</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>CBS поддержание обеспечения</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>Избыточная скорость передачи информации</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Контроль трафика</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>Шаг #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>Шаг #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>Шаг #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>Шаг #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>Шаг #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>Шаг #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>Шаг #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>Шаг #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>Шаг #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>Шаг #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Подождите, сохранение результатов.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Не удалось сохранить результаты.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Результаты сохранены.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>Сбой локального ARP.</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>Сбой удаленного ARP.</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> Таблица , прод.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>Снимок экрана</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - Гистограмма занимает несколько страниц</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>Временная шкала</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>Недоступн.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>Информация о пользователе</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>Группы конфигураций</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>Группы результатов</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>Журналы событий</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>Гистограммы</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>Снимки экрана</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>Предполагаемая производительность TCP</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation> Отчеты</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Создан с использованием Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Создан с использованием Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Создан с использованием Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Создано средством для проведения испытаний компании Viavi</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>Частота сканирования ( Гц )</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Прошел / Ошибка</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>Настройки : Фильтры /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> ( шаблон и маска )</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Шаблон</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Маска</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Настройка :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Отсутствие применимых настроек...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Проверка CPRI</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>Радио</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>Диапазон</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>Общий результат проверки CPRI</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Испытание завершено</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Проверка не завершена</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Длит./ Действит.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>Вкл</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Нет.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>Название события</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>Длительность / Значение</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>Журнал событий тестов длительной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Неизвестное - Состояние</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>Выполняется. Подождите...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>Ошибка !</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>Команда завершена !</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>Прервано !</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Восходящая петля</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Нисходящая петля</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Активир.</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>Деактивир.</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>Выключить питание</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>Передать команду проверки по шлейфу</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>Переключить</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>Сброс переключателя</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>Выдать запрос</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>Запрос проверки по шлейфу</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>Активизировать ближний конец</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>Деактивизировать ближний конец ,</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>Запрос питания</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>Запрос диапазона</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>Отключить превышение лимита времени</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>Установка превышения лимита времени в исходное состояние</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>Последовательная петля ,</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Путь уровня 1</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 Путь синхронизации транзитного узла</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Путь уровня 2</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Путь уровня 3</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Минимальный путь синхронизации Sonet</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Путь уровня 3E</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Предоставлено в результате проведения операции в сети</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Не использовать для синхронизации</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Неспецифическое оснащение</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>Структура TUG</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>Заблокированный транспортный блок</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>Асинхронный режим 34M/45M</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>Асинхронный режим 140M</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>Отображение ATM</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>Отображение MAN (DQDB)</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>Отображение FDDI</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>Отображение HDLC/PPP</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>Режим с расшифровкой RFC 1619</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>Испытательный сигнал O.181</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>Асинхрон. </translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>Передача по битам</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>Передача по байтам</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>STS-1 SPE со структурой VT</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>Режим блокировки VT</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>Асинхронное отображение DS3</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>Асинхронное отображение DS4NA</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>Асинхронное отображение FDDI</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 Дефект полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 Дефекты полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>Формат ?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Вне диапазона</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>ВЫКЛ</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ВКЛ</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>ИСТОРИЯ</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Переполнение</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + ИСТОРИЯ</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>Область</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>Отметка</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>ЗЕЛЕНЫЙ</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>ЖЕЛТЫЙ</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>КРАСНЫЙ</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>НЕТ</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>ВСЕ</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>ОТКАЗАТЬ</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>НЕОПРЕДЕЛЕННЫЙ</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>ПРИНЯТЬ</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>НЕИЗВЕСТНЫЙ</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ОШИБКА</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ПРОЙДЕНО</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Тест не завершен</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Испытание завершено</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Неизвестн. состоян.</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>Идентификация выполнена</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>Невозможно идентифицировать</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>Неизвестная идентификационная информация</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>Остается %1 час. и %2 мин.</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>Остается %1 мин.</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>СЛИШКОМ НИЗКОЕ</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>СЛИШКОМ ВЫСОКОЕ</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>( СЛИШКОМ НИЗКОЕ )</translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>( СЛИШКОМ ВЫСОКОЕ )</translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>Байт</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Значение</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Транспортный порт</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>Неопред.</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Неоснащ.</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>Функция отображения находится в процессе разработки</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC через SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>Простое отображение канала передачи данных (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>Отображение HCLC/LAP-S</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>Простое отображение канала передачи данных ( установка - сброс ),</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>Отображение кадров в сети 10 Gbit/s Ethernet</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>Отображение GFP</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>Отображение каналов в волоконно - оптической сети 10 Gbit/s</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>Резерв - Собственный</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>Резерв - Национальный</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>Отображение испытательного сигнала O.181</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>Зарезервировано (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Неспецифическое оснащение</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>Асинхронное отображение DS3</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>Асинхронное отображение DS4NA</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>Асинхронное отображение FDDI</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC через SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 Дефект полезной нагрузки VT</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>Неназначен. TEI</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Ожидание TEI</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Ожидание установления соединения TEI</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>Назначен. TEI</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Ожидание установления соединения</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Ожидание освобождения соединения</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Установление сверх циклов</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Восстановление таймера</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Неизвестный канал связи</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>ОЖИДАЕТСЯ УСТАНОВЛЕНИЕ СВЯЗИ</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>МУЛТИКАДР УСТАНОВЛЕН</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>ONHOOK</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>ГУДОК</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>ПОБЛОЧНЫЙ НАБОР</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>ЗВОНОК</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>ПОДКЛЮЧЕН</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>ПОДТВЕРЖДЕНИЕ ЗВОНКА</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>Речь</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Данные</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Teletex</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Videotex</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>Speech BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>Данные BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>Данные 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Поиск</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Доступн.</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>Присоединиться к запросу</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>Повторить запрос</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>Выйти</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Подтверждение завершения пакетной передачи</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>Присоединиться к отклику</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>Завершение пакетной передачи</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>Отклик о состоянии</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>Известная ошибка в потоке</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>Ошибка : услуга еще не помещена в буфер</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>Ошибка : недействительный повторный запрос пакета</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>Ошибка : отсутствие подобной услуги</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>Ошибка : отсутствие подобного участка</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>Ошибка : ошибка сеанса</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>Ошибка : неподдерживаемая версия команды и элемента управления</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>Ошибка : переполнение сервера</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>Ошибка : повторное присоединение</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>Ошибка : повторяющиеся идентификационные номера сеанса</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>Ошибка : недействительная скорость передачи битов</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>Ошибка : сеанс нарушен сервером</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>кратк.</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>открыт.</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>обычн.</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>обратн.</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>Слишком низкий уровень</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 байт</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 байт.</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Да</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>Выбрано</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Не выбран .</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>НАЧАЛО</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>ОКОНЧАНИЕ</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Кадры с ошибками</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Кадры OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Потерянные кадры</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Нарушения кода</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>Журнал событий заполнен</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Завершено</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Недоступн .</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>USB не найден . Пожалуйтса , вставьте ключ и попробуйте еще раз .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>Для данного элемента помощь не предусмотрена.</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>Идентификатор блока</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>нет сигнал</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Сигнал</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Готовность</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>Использ.</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/нет (дБ Гц)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>Идентификатор спутника</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>Идентификатор ГЛОНАСС</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Рез.</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Стат.</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Формирование кадров</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Предполаг.</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Маска</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>Маска MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>Маска TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>Результаты MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>Маска MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>Результаты TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>Маска TDEV</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Исходные данные TIE</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Данные сдвига</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>Тип кривой MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>Линия + точки</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>Только точки</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>Только MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>Только TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>Тип маски</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>Успешн. MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>Ошибка MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>Успешн. TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>Ошибка TDEV</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>Интервал ( ы ) наблюдения</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>Идет вычисление</translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>Расчет отменен</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>Расчет закончен</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>Обновление данных TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>Данные TIE загружены</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>Данные TIE не загружены</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>Недостаточно памяти для анализа вандера.&#xA; Требуется 256 МБ. Вместо локального анализа следует использовать внешнее ПО для анализа.&#xA; См. руководство для ознакомления с более подробной информацией.</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Сдвиг частоты (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>Скорость ухода (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>Выборки</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>Частота выборки ( в секунду )</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>Blocks</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>Текущий блок</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>Удалить сдвиг</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>Выбор кривой</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>Обе кривые</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Только сдвиг частоты</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>Сделать снимок экрана</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Время ( с ) </translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Локальн.</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Дистанцион.</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>Непомеченный</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>Порт 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>Порт 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>Панель инструментов</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Журнал сообщений</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>Общая информация :</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>Графики отключены</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Ошибка печати !</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Номер прог.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Мбит / с , мин.</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Мбит / с , макс.</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Джиттер PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Максим. джиттер PCR</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Всего ошибок CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Ошиб. CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Максим. ошиб. CC</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Всего ошибок PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Ошиб. PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Максим. ошиб. PMT</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Всего ошибок PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Ошиб. PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Максим. ошиб. PID</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># потоков</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Ошиб. контрольн. сум. IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Ошиб. контрольн. сум. UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>Идентификационный код транспортного потока</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>Наличие RTP</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Всего потеряно пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Потеряно пакетов за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Максимальное количество потерянных пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Джиттер пакета ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Максим. джиттер пакета ( мс )</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>Всего пакетов с нарушением последовательности</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Пакетов с нарушением последовательности за текущий период ,</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Максим. количество пакетов с нарушением последовательности</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Всего дист. ошибок</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Дист. ошибок за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Максим. количество дист. ошибок</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Всего ошибок период.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Ошибок период. за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Максим. количество ошибок период.</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Максим. период потерь </translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>Мин. дист. потерь</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Всего потерь синхронизации</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Всего ошибок байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Ошибок байт. синхронизац. за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Максим. количество ошибок байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF, текущ.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF, макс.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR, текущ.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR, макс.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Всего ошибок транспорт.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Ошибок транспорт. за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Максим. количество ошибок транспорт.,</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Всего ошибок PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Ошибок PAT за текущий период</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Максим. ошиб. PAT</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation>Количество проанализированных потоков</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Всего L1 Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Ошиб. контрольн. сум. IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Ошиб. контрольн. сум. UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>Идентификационный код транспортного потока</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>Номер прог.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Прогр. Мбит / с за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Прогр. Мбит / с , мин.</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Прогр. Мбит / с , макс.</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>Наличие RTP</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Всего потеряно пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Потеряно пакетов за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Максимальное количество потерянных пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Джиттер пакета ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Максим. джиттер пакета ( мс )</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>OoS Pkts Tot</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>Пакетов с нарушением последовательности за текущий период ,</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>Максим. количество пакетов с нарушением последовательности</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Всего дист. ошибок</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Дист. ошибок за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Максим. количество дист. ошибок</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Всего ошибок период.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Ошибок период. за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Максим. количество ошибок период.</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Максим. период потерь </translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>Мин. дист. потерь </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Всего потерь синхронизации</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Всего ошибок байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Ошибок байт. синхронизац. за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Максим. количество ошибок байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF, текущ.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF, макс.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR, текущ.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR, макс.</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>Джиттер PCR за текущий период</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>Максим. джиттер PCR</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Всего ошибок CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>Ошибок CC за текущий период</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Максим. ошиб. CC</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Всего ошибок транспорт.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Ошибок транспорт. за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Максим. количество ошибок транспорт.,</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Всего ошибок PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Ошибок PAT за текущий период</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Максим. ошиб. PAT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Всего ошибок PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>Ошибок PMT за текущий период</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Максим. ошиб. PMT</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Всего ошибок PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>Ошибок PID за текущий период</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Максим. ошиб. PID</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>Адрес IP</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Потеряно пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Максим. количество потерянных пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Джиттер пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Максим. джиттер  пакетов</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># потоков</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Rx Мбит / с , текущ. L1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Прогр. Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Прогр. Мбит / с , мин.</translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>Идентификационный код транспорт.</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Номер прог.</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Потери синхронизации</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>Всего ошибок байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Ошибк. байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Максим. количество ошибок байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>Всего ошибок PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>Ошиб. PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Максим. ошиб. PAT</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>Джиттер PCR</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>Максим. джиттер PCR</translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>Всего ошибок CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Ошиб. CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Максим. ошиб. CC</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>Всего ошибок PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Ошиб. PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Максим. ошиб. PMT</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>Всего ошибок PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Ошиб. PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Максим. ошиб. PID</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>Всего ошибок транспорт.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Ошиб. транспорт.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Максим. количество ошибок транспорт.,</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation>Количество проанализированных потоков</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Всего L1 Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Ошиб. контрольн. сум. IP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Ошиб. контрольн. сум. UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>История MPEG</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>Наличие RTP</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Потеряно пакетов за текущий период</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Всего потеряно пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Максимальное количество потерянных пакетов</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Джиттер пакета за текущ. период</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Максим. джиттер пакета</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Макс.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Позит.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Отрицат.- Макс.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Макс. макс.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Позит. макс.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Отрицат. макс.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Информация об отчете по проведению теста</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Код</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Назначение</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Путь</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Канал</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Мост</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>не используется</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>Общие результаты проверки оптики</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Самопроверка оптики</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Настройки :</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Сведения</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>В стеке</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>Ярусная сеть VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Глубина яруса сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>Бит DEI %1 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>Приоритет пользователя %1 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) %1 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) %1 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>Кадры не определены</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>Байт</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Значение</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Транспортный порт</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Настройка:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>Тест сети OTN</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>Общий результат проверки OTN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Проверка OTN</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Испытание завершено</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Тест не завершен</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>Захват байтов POH</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Шестнадцатеричн.</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Двоичн.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Настройки :</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Отсутствие применимых настроек...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>Служебные байты</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>Получатель CDMA</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Ошибка печати !</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>Испытание PTP</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>Профиль загрузки</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>Общие результаты проверки PTP</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Проверка PTP</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Испытание завершено</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Проверка не завершена</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>ВСЕ РЕЗУЛЬТАТЫ ОБЗОРА В ПОРЯДКЕ</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Расширен испытан FC</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>Расширенный тест RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>Общий результат испытания</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Режим</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>Общие результаты теста</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Потеря кадров</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Восстановление системы</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Кредит буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Производительность кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Длительная нагрузка</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Завершено</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Не запущено - предыдущий тест был остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Не запущено - предыдущий тест был завершен с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Не запущено - сбой предыдущего теста , включение остановки при сбое</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Не запущено</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>Симметричная обратная петля</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>Симметрично - вниз или вверх</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>Асимметрично - вниз или вверх</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Восходящ.</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Нисходящ.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Джиттер пакета</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Потеря кадров</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Пакет (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Взаимн .</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Восстановление системы</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Длительная нагрузка</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Кредит буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Производительность кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Испытания , подлежащие выполнению</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>Выбранные длины кадров ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>Выбранные длины пакетов ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>Выбраны верхние длины кадров ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>Выбранные верхние длины пакетов ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>Выбраны нисходящие длины кадров ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>Выбранные нисходящие длины пакетов ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>Нисходящая длина кадров ( байты )</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 байт тестового графика потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 направленный вверх график потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 байт нисходящего тестового графика потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 буферные данные пропускной способности в байтах на тестовом графике</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Длительн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Переполнение</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Длительн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>Длительность ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Переполнение</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Самый длительный</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Самый короткий</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Последний</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Среднее</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Нарушения</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Всего</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Настройка :</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Разъем</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Номинальная длина волны (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Номинальная скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Длина волны ( нм )</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>Минимальная скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>Максимальная скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Тип уровня мощности</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Поставщик</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>PN поставщика</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Номер редакции поставщика</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Максимальный уровень приема (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Максимальный уровень передачи (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN поставщика</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Код даты</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Код участка</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Диагностический контроль</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Диагностический байт</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Приемопередатчик</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW / SW Версия №</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA HW Spec. Rev#</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA Mgmt. I/F Rev#</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Категория мощности</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Тип уровня мощности Rx</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>Max Lambda Power (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>Количество активных волокон</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Длина волны для волокна</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL для диапазона волокна (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Макс . Скорость передачи полосы Ntwk (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Поддерживаемые скорости</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Длительность</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Недействительн.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Задержка фрейма (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Задержка кадра (OWD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Односторонняя задержка</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Обратный поток</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Выходной ключевой поток .</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>Строка</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Настройка:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Испытание истинной скорости</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>Заворот</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>Устранение неполадок</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Режим</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Симметрия</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Асимметричный</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>Симметрия пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Путь MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Оконный режим</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Производительность TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>Дополнительные параметры TCP</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>Операции , подлежащие выполнению</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Завершено</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Не запущено - предыдущий тест был остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Не запущено - предыдущий тест был завершен с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Не запущено - сбой предыдущего теста , включение остановки при сбое</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Не запущено</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Тест TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Тест не завершен</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>Тест был прекращен пользователем.</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>Выполнение теста не было начато.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>Успешное выполнение теста восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>Пропускная способность более 90% от заданной.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>Неуспешное выполнение теста восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>Пропускная способность менее 90% от заданной.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>Осуществление нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>Отказ нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Информация об отчете по проведению теста</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Имя специалиста</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Компания</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Эл. почта</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Телефон</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Идентификация теста</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Наименование теста</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Код авторизации</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Дата создания учетной записи</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Время прекращения тестирования</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Комментарии</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Длит./ Действит.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>След</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>Последовательность</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Настройка :</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Журнал :</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>IP- адрес потока : Порт</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Название потока</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Длит./ Действит.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Название прог.</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>Выполняется</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>Общий результат теста VLAN сканирования</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>Испытание сканирования сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Настройка :</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Результаты :</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>Общие результаты проверки TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>Тест невозможно закончить , он был завершен с ошибками . Отчет может содержать неполные результаты .</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>Тест был остановлен пользователем . Отчет может содержать неполные результаты .</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>Результаты частичной проверки</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed </translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>Результат проверки J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>Результаты теста RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>Результаты теста SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>Результат проверки J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>Результат проверки TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Завершено</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Не запущено - предыдущий тест был остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Не запущено - предыдущий тест был завершен с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Не запущено - сбой предыдущего теста , включение остановки при сбое</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Не запущено</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Журнал сообщений</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>Журнал сообщений ( продолжение )</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Заказ на выполнение работ</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Комментарии / Примечания</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Прибор</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>Результаты общего теста SAM</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Дата начала</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Дата окончания</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>Результаты тестов общей конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>Результаты тестов общих характеристик</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Потеря кадров</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Колебание задержки</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed </translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>Создать отчет о группах</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>Невозможно создать отчет : </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>недостаточно места на диске.</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Невозможно создать отчет</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>Настройки порта</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>Нечетн.</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>Четн.</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>Скорость передачи в бодах</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>Четность</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Управление потоком</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Выкл.</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Оборудование</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>Биты данных</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>Стоп. биты</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>Настройки терминала</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>Ввести / Выдать</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>Локальное эхо</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>Включить зарезервированные клавиши</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>Отключенные клавиши</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Вкл</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>Отключить</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Включить</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ЭКСПОРТ , ФАЙЛ , НАСТРОЙКА , РЕЗУЛЬТАТЫ , СЦЕНАРИЙ , СТАРТ / СТОП , программируемые клавиши на панели</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ПЕЧАТЬ , ФАЙЛ , НАСТРОЙКА , РЕЗУЛЬТАТЫ , СЦЕНАРИЙ , СТАРТ / СТОП , программируемые клавиши на панели</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ФАЙЛ , НАСТРОЙКА , РЕЗУЛЬТАТЫ , СЦЕНАРИЙ , СТАРТ / СТОП , программируемые клавиши на панели</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>Виртуальные клавиши панели FILE, SETUP, RESULTS, EXPORT, START/STOP</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>Окно &#xA; терминала</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>Восстановить значения &#xA; по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>Настройка &#xA;VT100</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>Очистить &#xA; экран</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>Показать &#xA; клавиатуру</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>Переместить &#xA; клавиатуру</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>Автоматическое определение скорости передачи в бодах</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Выполнить снимок &#xA; экрана</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>Скрыть &#xA; клавиатуру</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>Ход выполнения проверки по шлейфу :</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>Результат :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Настройка</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Неожиданная ошибка</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Выполнить &#xA; сценарий</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA; Это приведет к повторному сканированию канала связи с целью обнаружения потоков и перезапуску испытания.&#xA;&#xA; Вы больше не будете видеть исключительно потоки , которые были &#xA; переданы из приложения Проводник.&#xA;&#xA; Продолжить ?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>Ход выполнения автоматического режима :</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>Сведения :</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Настройка</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>Идет выполнение автоматического режима. Подождите...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>Автоматический сбой .</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>Автоматическое завершение .</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>Автоматическое прерывание .</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Неизвестное - Состояние</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>Обнаружение </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Сканирование…</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн.</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Неожиданная ошибка</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>Данные о сопряженном устройстве</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>Отправка файла</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Подключиться</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Забыть</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Выбрать файл</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Передать</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Идет подключен....</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>Отключение…</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Разъединить</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>Название пакета :</translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>Введите имя пакета :</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>Сертификаты и ключи (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>Неверное названия пакета</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>Пакет с именем "%1" уже существует.Пожалуйста, выберите другое имя.</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>Добавить файлы</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>Создать пакет</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>Переименовать пакет</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>Изменить пакет</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>Открыть папку</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>Добавить новый пакет ...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>Добавить сертификаты к %1 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>Удалить %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>Удалить %1 из %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> Биты</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>Идентификатор виртуальной полосы</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>Примененный наклон ( бит )</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>Примененный наклон ( нс )</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation># физической дорожки</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Диапазон:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>Невозможно установить дату</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Разряды</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>Трибутарный слот</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>Применить</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Гбит / с</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>Полоса пропускания</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>Изменения еще не применены .</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>Выбрано слишком много триб . слотов .</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>Выбрано слишком мало триб . слотов .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>Другое...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> знаков</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> разряд.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>Сегодня</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>Ввести дату : дд / мм / гггг</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>Ввести дату : мм / дд / гггг</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Байты</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>До </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> байт.</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> Разряды</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> разряд.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Секунды</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Минуты</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Часы</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Дни</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>дд / чч : мм</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>чч : мм</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>мм : сс</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>Длительность: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>Настройка адреса назначения производится во вкладке Все службы.</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>Настройка адреса назначения производится во вкладке Все потоки.</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Тип назначения</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Тип шлейфа</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>IP- адрес источника данного перехода</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>IP- адрес назначения следующего перехода</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>Маска подсети следующего перехода</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>Настройка адреса источника производится во вкладке Ethernet для всех кадров.</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Тип источника</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC- адрес по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>МАС - адрес пользователя</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>Настройка адреса источника производится во вкладке Все службы.</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>Настройка адреса источника производится во вкладке Все потоки.</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Приоритет пользователя сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Бит DEI</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Увеличение PBit </translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN пользователя</translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>Не настраивается в режиме проверки по шлейфу.</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>Указать идентификационный код сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Приоритет пользователя</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Длина</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>Длина данных ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Управление</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>Тип Ether</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>Тип /&#xA; Длина</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>Туннель &#xA; Метка</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>Фильтр B-TAG VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>Приоритет B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>Бит DEI B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>Метка туннеля</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>Приоритет туннеля</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>Тип Ether B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>TTL туннеля</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC&#xA; метка</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>Приоритет I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>Бит DEI I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>Бит UCA I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>Фильтр идентификационных кодов службы I-Tag </translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>Идентификационный код службы I-Tag</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>Метка VC</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>Приоритет VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>Тип Ether I-Tag</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA; Метка</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>Метка MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>Приоритет MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA; Метка</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>Метка MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>Приоритет MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Данные</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>Идет перенос кадра абонента :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Инкапсуляция</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Тип кадра</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>Размер кадра абонента</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Размер пользователя</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>Неполномерн. размер</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>Увеличен. размер</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>Участок данных содержит IP- пакет. Настроить данный пакет во вкладке IP.</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>Настроить фильтрацию данных во вкладке Фильтр IP.</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Полезная нагрузка Tx</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>Настройка RTD</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>Анализ полезной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Полезная нагрузка Rx</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>Таймер LPAC</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Rx BERT</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Tx BERT</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>Последовательность пользователя</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>Настроить входящие кадры :</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>Настроить исходящие кадры :</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>Поле Длина / Тип составляет 0x8870 </translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>Длина данных имеет произвольное значение</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Индекс начала приоритета пользователя</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>Тип файла :</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Название файла :</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Хотите удалить &#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>Хотите удалить все файлы в данной папке ?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Разряды</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation>Байт</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>До </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Байты</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Диапазон :  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> ( шестнадцатеричн.)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> разряд.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> недействительн. - IP- адреса Порта 1 и &#xA; Порта 2 источника не должны совпадать. Восстановлен предыдущий IP- адрес.</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>Недействительный IP- адрес &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Диапазон :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>Данный IP- адрес не подходит для этой конфигурации.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>2 знака на байт , до </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Байты</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>2 знака на байт , </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>Название кода петли</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>Последовательность битов</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>Доставка</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>В пределах полосы</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>Вне полосы</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Восходящая петля</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Нисходящая петля</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Другое</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>Название кода петли</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 битов</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Дост.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Послед.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>Добавить / удалить</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Опред.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Путь Tx</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>Диапазон :  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>Выбрать канал</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>Ввести значение KLM</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>Диапазон :  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Выбрать члена группы VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Послед.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>Байт PSI</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>Значение байта</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>Тип устройства ODU</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>Транспортн. порт №</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Диапазон :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Выбрать члена группы VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Диапазон :  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>Редактор служебных байтов</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>Выбрать действительн. байт для редактирования.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>Неопределенный оптоволоконный канал</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Идет создание страницы.  Подождите...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>Страница пуста.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>Добавить элемент</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>Изменить элемент</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>Удалить элемент</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>Добавить строку</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>Изменить строку</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>Удалить строку</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>Выкл.</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загруз.</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 Возможность контроля неизвестна</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Неоснащ.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Неоснащ.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Неоснащ.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Неоснащ.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Длина :  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> знаков</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Длина:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> знаков</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>Сейчас</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>Введите время : чч : мм : сс</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Очистить все</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>Байт %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation> Биты</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Имя службы</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Обратный поток</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Выходной ключевой поток .</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>Оба направления</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>Тип сервиса</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>Тип сервиса был переустановлен в значение Данные в связи с изменениями длительности кадров и ( или ) значения CIR</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>Частота выборки ( мс )</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation>Число вызовов</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Размер кадра</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Длина пакета</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>Число каналов</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Сжатие</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>Создать группу VCG</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>Определить членов группы VCG с нумерацией каналов , заданной по умолчанию :</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Определить группу VCG Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Определить группу VCG Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>Полоса пропускания полезной нагрузки ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>Количество членов</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>Распределить &#xA; Члены</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>Распределить членов группы VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>Определить настраиваемое распределение членов группы VCG</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>Экземпляр</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Шаг</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>Редактировать членов группы VCG</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>Формат адреса</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>Новый член</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>По умолчанию</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>Потоки , к которым было выполнено подключение , были импортированы из Проводника. &#xA; Нажмите кнопку [ Подключиться к потокам...], чтобы подключиться к потокам.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Адресная книга</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Не выбирать ничего</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>Найти далее</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Не выбирать ничего</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>Подключиться к потокам...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>Добавить</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>Введенные IP- адреса добавлены в адресную книгу.</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Чтобы подключиться</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>Уже подключен</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>Подключиться</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Выйти из потока</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>IP- адрес назначения</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Выйти из потоков…</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Очистить все</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>Быстрая настройка</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>Примечание : при этом произойдет замещение текущей конфигурации кадра.</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Быстр. (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>Полн. (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Интенсивность</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Семейство</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Инкапсуляция</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Увеличение PBit </translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Приоритет пользователя</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID ( шестнадцатеричн.)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>TPID пользователя ( шестнадцатеричн.)</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Примен.</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Индекс начала приоритета пользователя</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Приоритет пользователя сети SVLAN</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>Загрузка...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загруз.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Дата в длинном формате :</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Дата в коротком формате :</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Длительное время :</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Краткая длительность :</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Числа :</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>Не размещенный</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>Назначено</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>Зарезервировано 10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reserved11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reserved01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reserved00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Сохранить...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Установить для испытания значения по умолчанию</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>Конфигурация &#xA; потоков…...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>Загрузить распределение</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Выбрать &#xA; Все</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>Очистить &#xA; все</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>Авто &#xA; распределение</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Поток</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Размер кадра</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загруз.</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>Частота кадров</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% скорости передачи в линии</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>Пилообразн. сигнал начинается в</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>Постоян.</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>Макс. использ. порог</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>Всего (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>Всего (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>Всего (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>Всего (кадров в секунду)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>Примечание : </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>Как минимум один поток должен быть разрешен.</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>Максимальный порог использования составляет </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>Максимальная возможная нагрузка составляет </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>Полная расчетная нагрузка не должна превышать это значение.</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>Ввести значение в процентах :  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>Ввести частоту кадров:  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>Ввести скорость передачи битов :  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>Примечание :&#xA; Скорость передачи данных не определена . Пожалуйтса , нажмите “Отменить” &#xA; и попробуйте еще раз , когда скорость передачи данных будет определена .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>Определить услуги передачи видеоинформации , данных и речи</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>Частота &#xA; выборки ( мс )</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation>Число вызовов</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>Скорость &#xA; в одном вызове ( Кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>Скорость &#xA;( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>Размер базового &#xA; кадра (в байтах)</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>Подавление пауз</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>Буфер. джиттера ( мс )</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>Число каналов</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Сжатие</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>Размер базового кадра (в байтах)</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>Начальн. скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>Тип загрузки</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>Временной шаг ( сек.)</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>Шаг загрузки ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Всего L1 Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>Моделирован.</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Данные 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Данные 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>Данные 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>Данные 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Произвольн.</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Голос.</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>Видео</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Данные</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>Примечание :</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>Как минимум одна служба должна быть разрешена.</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>Максимальная возможная нагрузка составляет </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>Полная расчетная нагрузка не должна превышать это значение.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>Распределение : </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>Полоса пропускания : </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>Структура : </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Шаг</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Адресная книга</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>Новый элемент</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>IP- адрес источника (0.0.0.0 = Любое )</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>Обязательн.</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>Имя ( макс. длина 16 знаков )</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>Добавить элемент</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>Импортировать / Экспортировать</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Импортировать</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>Импортировать элементы из диска USB</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Экспорт.</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>Экспортировать элементы в диск USB</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Удалить все</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>Сохранить и закрыть</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>Необязательн.</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>Импортировать элементы из USB</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>Импортировать элементы</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>Разделен. запятой (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Идет добавление элемента</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>Каждый элемент должен иметь 4 поля : &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name>, разделенные запятыми.  Строка пропущена.</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Прервать</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>Продолжить</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>не является достоверным IP- адресом источника.  Строка пропущена.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>не является достоверным IP- адресом назначения.  Строка пропущена.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>не является достоверным значением PID.  Строка пропущена.</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>Элемент должен иметь имя ( до 16 знаков ).  Строка пропущена</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>Имя элемента не должно превышать 16 знаков.  Строка пропущена</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>Элемент с такими же полями Src. Ip, Dest. IP и PID&#xA; уже существует и имеет имя</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>ПЕРЕПИСАТЬ имя существующего элемента &#xA; или &#xA; ПРОПУСТИТЬ операцию импортирования данного элемента ?</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Пропустить</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>Переписать</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>Один из импортируемых элементов имел значение PID.&#xA; Элементы со значениями PID используются только в приложениях MPTS.</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>Экспортировать элементы в USB</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>Экспортировать элементы</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>Адресная книга IPTV_Address_Book</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>уже используется. Выберите другое имя или измените данный элемент.</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>Элемент с данными параметрами уже существует.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>Удалить все элементы ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Идет добавление элемента</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>Элемент с таким же именем уже существует в</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>Что следует сделать ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Дата : </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Время : </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Час.</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>День</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Длит./ Действит.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных о событиях , воспользуйтесь пунктами меню  Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Вкл</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Код</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Канал</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Мост</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Назначение</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Путь</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных байтов K1/K2, воспользуйтесь пунктами меню  Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Шестнадцатеричн.</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Двоичн.</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных о событиях односторонней задержки , воспользуйтесь пунктами меню  Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>Потеря источника</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>Ошиб. B1</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>Ошиб. REI-L</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>Ошиб. MS-REI</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>Ошиб. B2</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>Ошиб. REI-P</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>Ошибка B2</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>Ошиб. HP-REI</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>Ошиб. B3</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>Ошиб. REI-V</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>Ошиб. BIP-V</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>Ошибка B3</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>Ошиб. LP-REI</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>Ошиб. LP-BIP</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>Ошибки FAS STL</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Ошиб. Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>Потеря сигнала</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Потеря кадровой синхронизации</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Ошибка окна кадра</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>Ошибка FAS</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Длительн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Время начала</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Время окончания</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Самый длительный</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Самый короткий</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Последний</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Среднее</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Нарушения</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных о нарушениях услуг , воспользуйтесь пунктами меню  Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Всего</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Переполнение</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>Номер SD</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Длительн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных о нарушениях услуг , воспользуйтесь пунктами меню  Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Переполнение</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>НАЧАЛО</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>ОКОНЧАНИЕ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>Номер SD</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Длительн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Переполнение</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных о результатах вызова , воспользуйтесь пунктами меню Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн.</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>тоновый набор</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  ИСТИНА</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Длительность</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Недействительн.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Дата : </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Время : </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Час.</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>День</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Длительн.( мс )/ Действит.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Название прог.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Длительн.( мс )/ Действит.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Событие</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>IP- адрес потока : порт</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Название потока</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Длительн.( мс )/ Действит.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>Экспортировать сохраненную категорию настраиваемых результатов</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Сохраненная категория настраиваемых результатов</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>Экспорт .</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Невозможно найти флэш - диск USB.&#xA; Вставьте флэш - диск или удалите флэш - диск и повторно вставьте его.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>Экспортировать данные PTP в USB</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>PTP файлы данных (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>Экспортировать отчет в USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Текст (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>Экспортировать снимки экрана в USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>Сохраненные снимки экрана (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>Архивировать выбранные файлы как :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Введите название файла : максимум 60 знаков</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Архивировать &#xA;&amp;&amp; Экспортировать</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Экспорт.</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Введите имя архивного файла</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Невозможно найти флэш - диск USB.&#xA; Вставьте флэш - диск или удалите флэш - диск и повторно вставьте его.</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>Невозможно заархивировать файл ( ы )</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>Экспортировать данные TIE в USB</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Файлы данных TIE вандера (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>Экспорт временных данных на USB</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>Все файлы данных синхронизации (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>Тип файла :</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Открыть</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>Импортировать сохраненную категорию настраиваемых результатов из USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Сохраненная категория настраиваемых результатов</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>Импортировать &#xA; Настраиваемая категория</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>Импортировать логотип отчета из USB</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Файлы изображения (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>Импортировать &#xA; Логотип</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>Импортировать Quick Card из USB</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>Файлы pdf (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>Импортировать &#xA;Quick Card</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>Импортировать &#xA; Испытание</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Разархивировать &#xA; &amp;&amp; Импортировать</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>Ошибка – Невозможно распаковать один из файлов .</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>Недостаточно свободного места на целевом устройстве.&#xA; Операция копирования отменена.</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>Идет копирование файлов...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>Готово . Файлы скопированы .</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>Ошибка : Не удалось скопировать следующие файлы :: &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>Сохранить данные PTP </translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>PTP файлы (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Копировать</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>Недостаточно места на диске. Удалить другие сохраненные файлы или экспортировать в USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>Свободно %1 из %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>Файлы не выбраны</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>Всего %1 в выбранном файле</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>Всего %1 в %2 выбранных файлах</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Название файла :</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Введите название файла : максимум 60 знаков</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>Удалить все файлы в данной папке ?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>( Файлы , предназначенные только для чтения , не будут удалены.)</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>Идет удаление файлов...</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Хотите удалить &#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>Удалить этот элемент ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>Не рекомендуемые оптика</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Волоконно - оптический канал</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137,6 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>Н/П&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>Выводит результаты исследования и сканирования структуры сигнала.</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>Сортировать по :</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>Сортировать</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>Повторно сортировать</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>Представляет список доступных испытаний , которые могут быть использованы для анализа выбранного канала</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Начать испытание</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>Подождите… , идет настройка выбранного канала…</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>Показать детали</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>Скрыть детали</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>Диапазон : %1 - %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Недоступн .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>Найти</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>Оригинал</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>По ширине</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>По высоте</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>Выбор режима просмотра спаренных испытаний</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>Еще ...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(Продолжение)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>Глоссарий</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Внимание</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>Переполнение буфера журналов</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>Фиксация остановлена</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>Редактировать строку</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>Выбрать байт :</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>Невозможно выполнить снимок экрана</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>недостаточно места на диске</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>Снимок экрана выполнен : </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>О потоке</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>Импульсный набор</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>Набор MF</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>Набор DTMF</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн.</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Зарезервировано</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Однонаправл.</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Двунаправл.</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Незанят.</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>Дополнительный трафик</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>Всего</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>Недоступн.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>Невозможно перейти ко внешним ссылкам</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>Не обнаружен.</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>Достигнут конец страницы , продолж. сверху</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>Выбрать средство</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Отключение автоматических отчетов до запуска сценария.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Включение ранее отключенных автоматических отчетов.</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Создать</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Удалить все</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загруз.</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Передать</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>Повторить</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Просмотр</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS ( приемник )</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS ( источник )</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>Контейнер</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Канал</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>Метка сигнала</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>След</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>Формат пути</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Неизвестн.</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>Представляет только текущий уровень. Не учитывает каналы более низкого или более высокого порядка. Обновления в режиме реального времени доступны только для выбранного в настоящий момент канала.</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>Состояние канала представлено значком.</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>Контролируемые аварийные сигналы не наблюдаются </translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>Наблюдаются аварийные сигналы</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>Контроль / Контролируемые аварийные сигналы не наблюдаются</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>Контроль / Аварийные сигналы наблюдаются</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>Название контейнера канала. Суффикс ‘с’ обозначает каскадный канал.</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>Номер N KLM канала , указанный в RFC 4606</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>Метка сигнала канала</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>Последнее известное состояние канала.</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>Недействительный канал.</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>Наличие RDI</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>Наличие AIS</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>Наличие LOP</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>Контроль</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Да</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>Состояние обновлено : </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>никогда</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>Информация о канале трассировки</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>Сведения о формате трассировки канала</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>Не поддерживается</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Сканирование</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>Аварийный сигнал</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Недействительн.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>Без формата</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>Одиночн. байт</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>Завершен. CR/LF</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>Сопряженные устройства</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>Обнаруженные устройства</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>Сертификат СА</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>Сертификат клиента</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>Ключ клиента</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Формат</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Свободное место</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Общая емкость</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Поставщик</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Метка</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Обновить версию</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Установленная версия</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>Категории</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>Указать имена , данные настраиваемым категориям. Нажатие кнопки мыши на имени включит или отключит данную настраиваемую категорию.</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>Предусматривает настройку настраиваемой конфигурации по щелчку кнопки мыши.</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>Предусматривает удаление настраиваемой категории путем переключения категорий , подлежащих удалению.</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>Имя , данное настраиваемой категории. Нажатие кнопки мыши на этом имени включит или отключит данную настраиваемую категорию.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>Нажатие кнопки мыши на значке настройки приведет к открытию диалогового окна конфигурации настраиваемой категории.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>Нажатие кнопки мыши на значке удаления отметит настраиваемую категорию , как подлежащую удалению , или снимет с нее подобную отметку.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Создать отчет</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Текст (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Создать</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Формат :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Ознакомиться с отчетом после его создания</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Ошибка – Имя файла не может быть пустым .</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>Изменение настроек приведет к обновлению текущих результатов.</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Имя DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Имя NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Отсутствует в подсети</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>Ожидание активности канала связи...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>Ожидание DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>Изменение конфигурации IP- адреса источника...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Режим</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP- адрес источника</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Имя DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Имя NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>Имя системы</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>Отсутствует в подсети</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Отсутствует в подсети</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Имя DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Имя NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Службы</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Отсутствует в подсети</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Службы</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Имя DNS</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Обнаружение</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>Приоритет VLAN</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Устройства</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>IP- адрес сети</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Устройства</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Имя NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Устройства</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>Сети IP</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>Домены</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>Серверы</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>Хост - устройства</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>Коммутаторы</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>Маршрутизаторы</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Настройки</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>Инфраструктура</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>Ядро</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>Распределение</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>Доступ</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Обнаружение</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>Обнаруженные сети IP</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>Обнаруженные домены NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>Обнаруженные сети VLANS</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>Обнаруженные маршрутизаторы</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>Обнаруженные коммутаторы</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>Обнаруженные хост - устройства</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>Обнаруженные серверы</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>Отчет об обнаружении сети</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Невозможно создать отчет</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>Недостаточно места на диске</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Создан с использованием Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Создан с использованием Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Создан с использованием Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Создано средством для проведения испытаний компании Viavi</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>Общ.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>Переполнение диска , все графики остановлены.&#xA; Освободите место и перезапустите испытание.&#xA;&#xA; Или отключите функцию Графики , воспользовавшись пунктами меню Сервис -> Настройка &#xA; в строке меню.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>Нажмите , чтобы центрировать временную шкалу</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>Свойства графиков</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>Средн .</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Макс.</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>кб</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Окно</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Окно насыщения</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>Сохранить данные графика</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>Недостаточно места на приемном устройстве .</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>Вы можете экспортировать непосредственно на USB, если устройство флеш - устройства USB подключено .</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>Сохранение графических данных . Это может занять некоторое время , пожалуйста , подождите ...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>Сохранение графических данных</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>Недостаточно места на устройстве . Экспортированые графические данные являются неполными .</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>Произошла ошибка при экспортировании данных диаграммы. Эти данные могут быть неполными.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>Шкала</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 день</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 часов</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 час</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 минут</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 минута</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 секунд</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Plot_Data</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>Чтобы изменить масштаб выполните операцию Tap and Drag</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>Насыщение</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>кб</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Окно</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Подкл .</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Номер.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Время ( с ) </translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Сообщение</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Порт источника</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>Порт назначения</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Удалить...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>Подтвердить...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>Выбранные категории будут удалены из всех выполняемых в настоящий момент испытаний</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>Удалить выбранные элементы ? </translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>Создать...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>Открывает диалоговое окно для настройки новой категории с выбранными абонентом результатами.  </translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>Нажатие этой клавиши позволяет отметить отдельные настраиваемые категории , подлежащие удалению из устройства.   Повторное нажатие этой клавиши по окончании выбора категорий приводит к удалению файлов.</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>Нажмите %1,&#xA; чтобы начать операцию</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>Настроить категорию настраиваемых результатов</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>Выбранные результаты , отмеченные знаком '*', не относятся к текущей конфигурации испытаний и не будут появляться в окне результатов.</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>Название категории :</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>Ввести название настраиваемой категории : макс. %1 знаков</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Название файла :</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>нет данных</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>Сохранить как</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>Сохранить новый</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>Файл %1, содержащий &#xA; категорию %2</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>Выбранные результаты : </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   ( Максимальное количество выбранных результатов </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Настроить...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Обзор</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>Макс. (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>Значение (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- автоматич.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Служебная кнопка Домой функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Служебная кнопка Назад функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Служебная кнопка Вперед функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Служебная кнопка Конец функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>История вызовов отсутствует</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Служебная кнопка Домой функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Служебная кнопка Назад функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Служебная кнопка Вперед функции Справка...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Служебная кнопка Конец функции Справка...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Макс.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Позит.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Отрицат.- Макс.</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- автоматич.</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Час.</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>День</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Час.</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>День</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Макс.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Позит.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Отрицат.- Макс.</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- автоматич.</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>Задержка ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Количество</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>РЕЗУЛЬТАТЫ &#xA; НЕДОСТУПНЫ</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>Состояние : ПРОЙДЕНО</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>Состояние : ОШИБКА</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>Состояние : НЕТ ДАННЫХ</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>Развернуть для просмотра опций фильтра</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation>Количество обнаруженых MEP</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>Установить как одноранговый узел сети</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>Отфильтровать дисплей</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>Включите фильтр</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>Введите значение фильтра :%1 символ макс</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>Тип CCM</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>Частота CCM</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>Peer MEP Id</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>области обслуживания</translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>Уточните ID домена</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>ID обслуживание доменов</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>Ид . номер технического обслуживания</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>Набор тестов установлен . Выделенный ряд был установлен как равный MEP для проведения этого теста .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>Установка набора тестов в качестве выделенного равного MEP не удалась .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>Не удалось установить t %1 на %2.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>РЕЗУЛЬТАТЫ &#xA; НЕДОСТУПНЫ</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>СОЗДАНИЕ ГРАФИКОВ &#xA; ОТКЛЮЧЕНО</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>Чтобы войти в режим полноэкранного просмотра переключите соответствующим образом данное окно результатов.</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>РЕЗУЛЬТАТЫ &#xA; НЕДОСТУПНЫ</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>Настраиваем.</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Все</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 байт графика результатов теста потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 направленный вверх график результатов теста потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 байт нисходящего тестового графика результатов теста потери кадра</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 буферные данные пропускной способности в байтах на результатах теста</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 нисходящие буферные данные в байтах на результатах теста</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 нисходящие буферные данные пропускной способности в байтах на результатах теста</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>Экспортировать текстовый файл ...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>Журнал экспортирован в</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>Поток &#xA; сведения</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>Свернуть все деревья результатов в этом окне.</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>Развернуть все деревья результатов в этом окне.</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>ПУСТАЯ &#xA; КАТЕГОРИЯ &#xA; РЕЗУЛЬТАТОВ</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>РЕЗУЛЬТАТЫ &#xA; НЕДОСТУПНЫ</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>РЕЗУЛЬТАТЫ &#xA; НЕДОСТУПНЫ</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>ВСЕ РЕЗУЛЬТАТЫ &#xA; ОБЗОРА &#xA; В ПОРЯДКЕ</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>Столбцы</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>Показать столбцы</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Очистить все</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>Показать только с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Столбцы...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>Показать только &#xA; строки &#xA; с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>Показать только строки с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>Скорость трафика (L1 Кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>Скорость трафика (L1 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation>№ анализир. поток.</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>Групировка трафика по</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>общему колич. каналов связи</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>Отображаемые потоки 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>Дополнительные потоки >128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Столбцы...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Остановлен.</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>Переход</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>Задержка ( мс )</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>Для ознакомления с большим количеством данных о контроле прохождения сигналов , воспользуйтесь пунктами меню  Вид -> Окна результатов -> Выбор отдельного меню.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>Идеальное время передачи</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>Фактическое время передачи</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>Группа :</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Поток</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>Программы</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>Потеря пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Джиттер пакета</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Ошибка расстояния</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Ошибка периода</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Ошибк. байт. синхронизац.</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>Показать только программы с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Столбцы...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>Всего прогр. Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>Показать только &#xA; программы с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>Показать только программы с ошибками</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>Показать &#xA; только потоки &#xA; с ошибками</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>Показать только потоки с ошибками</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Анализировать</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation>Количество &#xA; анализируемых потоков</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>Всего &#xA;L1 Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>Ошибки контрольн. суммы &#xA;IP- адр.</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>Ошибки контрольн. суммы UDP- адр.</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>Запустить &#xA; анализатор</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Столбцы...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>Подождите.. идет запуск приложения Анализатор...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- автоматич.</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Час.</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>День</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Сек.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Час.</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>День</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y- автоматич.</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>Выберите сценарий.. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>Сценарий :</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>Состояние :</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>Текущее состояние</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Остановлен.</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>Таймер :</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>Истекшее время сценария :</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>Показатель таймера</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>Результат :</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>Сценарий завершен в :  </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>Выбрать &#xA; сценарий</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Запустить сценарий</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>Очистить &#xA; результат</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>Остановить сценарий</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>ВЫПОЛНЯЕТСЯ...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>Истекшее время сценария :</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>Выберите другой сценарий.. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>Ошибка.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>ВЫПОЛНЯЕТСЯ.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>ВЫПОЛНЯЕТСЯ..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>Настройка списка тестирования</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>Показывать результаты при запуске</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>Подымать</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>Опускать</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Удалить все</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Переименовать</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>Разделитель</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>Добавить ярлык</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>Добавить сохраненный тест</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>Удалить все избранное ?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>Список Избранное устанавливается по умолчанию .</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>Все избранные закладки будут удалены , а список будет восстановлено по умолчанию для данного устройства .   Хотите продолжить ?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>Конфигурации тестов (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>Двойные конфигурации теста (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>Выбрать сохраненный тест</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>Подождите, пожалуйста...&#xA;Запуск пустого обзора тестирования</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>Тест ПИН</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>Переименовать закр. тест</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>Закрепить в список тестов</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>Сохранить конфигурацию тестирования</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Имя испытания :</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>Введите имя для отображения</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>Этот тест идентичен %1</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>Это ярлык для запуска тестового приложения .</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>Описание : %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>Эта сохраненная конфигурация теста .</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>Название файла : %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>Заменить</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>Невозможно запустить испытание...&#xA; Время действия параметров истекло.&#xA; Выполните выход и перезапустите модуль BERT через системную страницу. </translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>Попробуйте удалить тесты , работающие на других вкладках .</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>Данный тест запущен на другом порту .   Хотите перейти к данному тесту ? ( на вкладке %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>Можно перенастроить другой тест ( на вкладке %1) для выбранного теста .   Хотите перенастроить данный тест ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>Список пуст .</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>Невозможно запустить тест ...&#xA; Функция оптического jitter выключена .&#xA; Функция запуска оптического jitter из страницы Домашняя страница / Система .</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>Невозможно запустить испытание...&#xA; Ресурсы электропитания недоступны.&#xA; Удалите / переконфигурируйте данное испытание.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>Невозможно запустить испытание...&#xA; Ресурсы электропитания недоступны.&#xA; Отмените выбор другого модуля или удалите / переконфигурируйте &#xA; данное испытание.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>Подождите...&#xA; Идет добавление испытания </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>Подождите...&#xA; Идет переконфигурация испытания в </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>Невозможно запустить испытание...&#xA; Необходимые ресурсы , возможно , используются.&#xA;&#xA; Обратитесь в отдел технической поддержки.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>Создание объектов интерфейса пользователя</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>Синхронизация интерфейса пользователя с модулем приложения</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>Инициализация режимов просмотра интерфейса пользователя</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Назад</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Выбрать&#xA;Испытание</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Решения VIAVI защищенные авторским правом</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>Информация об устройстве</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>Параметры</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>Сохраненный файл</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>Режим доступа к интерфейсу пользователя</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>Используется дистанционное управление.&#xA;&#xA; Режим доступа только для чтения не позволяет пользователю изменять настройки ,&#xA; что может сказаться на выполняемых операциях дистанционного управления.</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>Режим доступа</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>Модуль MSAM был установлен в исходное состояние в связи с изменением конфигурации модуля PIM.</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>Модуль PIM был установлен или удален. Если вы производите замену модулей PIM, вы можете продолжить замену.&#xA; Модуль MSAM сейчас будет повторно запущен. Это может занять до 2 минут. Подождите...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>BERT модуль перегрелся и будет &#xA; автоматически выключен , если внутренняя температура будет продолжать &#xA; повышаться .   Пожалуйста , сохраните Ваши данные , выключите модуль BERT&#xA; и позвоните в службу технической поддержки .</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>BERT модуль был вынужденно отключен вследствие перегрева .</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>Модуль XFP PIM расположен в несоответствующем гнезде. Переставьте модуль XFP PIM в Порт #1.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>Модуль XFP PIM модуля BERT установлен в несоответствующем гнезде.&#xA; Переустановите модуль XFP PIM в Порт #1.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>Вы выбрали электрические испытания , но выбранный вами модуль SFP является оптическим модулем SFP.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>Используйте электрический модуль SFP.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>Модуль BERT обнаружил возможную ошибку в приложении %1.&#xA;&#xA; Вы выбрали электрическое испытание , но используемый модуль SFP является оптическим модулем SFP.   Замените модуль SFP или выберите другой модуль SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>Вы выбрали оптические испытания , но выбранный вами модуль SFP является электрическим модулем SFP.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>Используйте оптический модуль SFP.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>Во время проведения комплекса испытаний была обнаружена возможная ошибка в приложении %1.&#xA;&#xA; Вы выбрали оптическое испытание , но используемый модуль SFP является &#xA; электрическим модулем SFP.  Замените модуль SFP или выберите другой модуль SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>Вы выбрали испытание 10G, но установленный приемопередатчик не является модулем SFP+. </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>Используйте модуль SFP+.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>Во время проведения комплекса испытаний была обнаружена возможная ошибка.  Для данного испытания требуется модуль SFP+, но приемопередатчик не является таким модулем.  Замените его на модуль SFP+.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>Настройки автоматического отчета</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>Переписать этот файл</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>Автоотчет</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Внимание :    Выбранный диск переполнен. Вы можете освободить место вручную или выбрать автоматическое удаление &#xA;5 самых старых отчетов. </translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>Включить функцию автоматических отчетов</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>Отчетный период</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>Мин.:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>Макс.:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>Перезапустить тест после создания отчета</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Режим</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>Создать отдельный файл</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>Название отчета</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>Дата и время создания автоматически прикрепляются к названию</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Формат :</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>Текст</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>Автоматические отчеты не поддерживаются в режиме доступа только для чтения.</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>Автоматические отчеты будут сохранены на жестком диске.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>Автоматическим отчетам требуется жесткий диск.  В данном устройстве жесткий диск не установлен.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>Автоматические отчеты не активируются , если запущен автоматизированный сценарий .</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>Создание автоматического отчета</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>Подготовка…</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>Создание </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>Удаление предыдущего отчета…</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>Выполнено.</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** НЕДОСТАТОЧНАЯ МОЩНОСТЬ.  ОТМЕНИТЕ ВЫБОР ДРУГОГО МОДУЛЯ ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>Последовательное соединение установлено успешно</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>Проверка обновлений приложения</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>Приложение готово для связи</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** ОШИБКА ПРИ ОБНОВЛЕНИИ ЯДРА *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** Проверьте , имеет ли параметр Безопасность Ethernet значение Стандарт и / или переустановите приложение BERT *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** ОШИБКА ПРИ ОБНОВЛЕНИИ ПРИЛОЖЕНИЯ.  Переустановите программное обеспечение модуля ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>*** ОШИБКА ПРИ ОБНОВЛЕНИИ. НЕДОСТАТОЧНАЯ МОЩНОСТЬ. ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** Ошибка запуска : Отключите модуль BERT, а затем повторно включите его ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>Просмотр</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Отчеты</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>Сервис</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>Создать отчет…</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>Автоматический отчет ...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Старт Теста</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>Стоп Теста</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Настроить...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>Режим доступа…</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Установить для испытания значения по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>Очистить историю</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>Запустить сценарии…</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>Эмуляция VT100</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Настройки модема…</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>Восстановить заданное по умолчанию расположение</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>Окна результатов</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Один</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>Разделить на левое / правое</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>Разделить на верхнее / нижнее</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>Сетка 2 x 2</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>Присоединить нижнее</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>Присоединить левое</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>Показать только результаты</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>Состояние испытания</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>Светодиоды</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>Панель конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Панель операций</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>Выбрать сценарий</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>Файлы сценариев (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>Выбрать &#xA; Сценарий</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Соединения сигналов</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Создать отчет</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Текст (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>Выбрать &#xA; Содержимое</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Создать</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Формат :</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Ознакомиться с отчетом после его создания</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Ошибка – Имя файла не может быть пустым .</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>Выбрать содержимое для</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Формат</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Название файла</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Выбрать...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Ознакомиться с отчетом после его создания</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>Включить журнал сообщений</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>Создать &#xA; отчет</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>Ознакомиться с &#xA; отчетом</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Текст (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Ошибка – Имя файла не может быть пустым .</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>Отчет сохранен</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>Выполните ослабление сигнала.</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>Переполнение журнала событий и гистограммы.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>Переполнение журналов K1/K2.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>Переполнение журнала.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>Испытания будут продолжаться без записи информации &#xA; о дополнительных элементах такого вида.  Перезапуск &#xA; испытания приведет к очистке всех журналов и гистограмм.</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>Оптическ.&#xA; Установка в исходное состояние</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>Заверш. испытание</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Перегрузка приемника</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>Переполнение журнала</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Внимание</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Никакие испытания не выполняются</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>Настроить внешний вид и функции интерфейса пользователя</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>Сохранить тест</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>Сохранить дублированный тест</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>Загрузка теста…</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загруз.</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>Все файлы (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>Сохраненные тесты (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>Сохранить дублированные тесты (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>Загрузка установки</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>Load Dual Test</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>Импорт сохраненных испытаний из USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>Все файлы (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>Сохранены Classic RFC Test Configurations ( класические конфигурации RFC теста ) (*.classic_rfc - расширение )</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>Сохраненные конфигурации испытаний RFC (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>Сохраненные конфигурации FC теста (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>Сохраненные конфигурации действительной скорости TrueSpeed Configurations (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>Сохранены параметры конфигурации TrueSpeed VNF (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>Сохраненные конфигурации испытания SAMComplete (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>Сохраненные конфигурации SAM-Complete (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>Сохраненная конфигурация OTN проверки (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>Сохраненные конфигурации самопроверки оптики (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>Сохраненные конфигурации проверки CPRI (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>Сохраненные конфигурации проверки PTP (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>Сохраненные архивные файлы (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>Экспортируйте сохраненный тест на USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>Все файлы (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>Сохраненные пользователем испытания , количеством больше одного</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>Сохраненное пользователем испытание</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>Используется дистанционное управление.&#xA;&#xA; Данная операция не разрешается в режиме доступа только для чтения.&#xA; Используйте пункты меню Сервис -> Режим доступа , чтобы включить режим Полный доступ.</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>Время действия параметров модуля BERT истекло.&#xA; Выполните выход и перезапустите модуль BERT через системную страницу.  </translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>Скрыть</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>Перезапустить оба испытания</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Перезапустить</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>Сервис:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>Действия</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>Конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>Развернутое окно результатов испытания : </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>Во весь экран</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Настройка</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Добавить испытание</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Руководство пользователя</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Рекомендуемая оптика</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Сетка частот</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Что это ?</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Испытание</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>Сохранить конфигурацию спаренных испытаний как...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>Загрузить конфигурацию спаренных  испытаний…</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Просмотр</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>Изменить выбор испытания ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>Перейти к просмотру испытания во весь экран</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Отчеты</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>Создать отчет о спаренном испытании...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Ознакомиться с отчетом...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Справка</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Внимание</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>Сохранение файла</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>Новое имя :</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>Ввести новое имя файла</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Переименовать</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>Невозможно переименовать , поскольку файл с таким именем уже существует.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Настройка</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Перезапустить</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>Редактировать информацию о пользователе</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Ничего не выбрано...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Максим. количество знаков : </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Выбрать логотип...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Функция предварительного просмотра недоступна.</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Справочные диаграммы</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Руководство пользователя</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Настройка</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Назад</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Домой</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>Вперед</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>Быстрый запуск</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** ОШИБКА ПРИ ОБНОВЛЕНИИ ДЖИТТЕРА.  Переустановите программное обеспечение модуля ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** ОШИБКА ФУНКЦИИ ОПТИЧЕСКОГО ДЖИТТЕРА. НЕДОСТАТОЧНАЯ МОЩНОСТЬ. ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>Выполняется функция оптического джиттера</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 профил. (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>Выбрать профили</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>Отменить выбор всех элементов</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>Примечание. Загрузка профиля «Подключение» соединит канал связи с удаленным устройством.</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Удалить все</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>Несовместимый профиль</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>Загрузить &#xA; профили</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Хотите удалить %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Хотите удалить все профили %1?&#xA;&#xA; Эту операцию нельзя будет отменить.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>( Файлы , предназначенные только для чтения , не будут удалены.)</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>Невозможно загрузить профили из :</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>Профили загружены из :</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>Некоторые конфигурации загружены неверно, так как они отсутствуют в данном приложении.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>Профили успешно загружены из :</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>Разрешить спаренные испытания</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>Индексирование приложений</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>Проверка параметров</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>Испытания для установленного оборудования или параметров недоступны.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>Не удалось начать диагностику .  Проверьте функции и конфигурацию аппаратных средств .</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>Восстановление приложения , функционирующего при выключении питания</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>Приложение функционирует</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>Невозможно установить внутреннюю флэш - память USB.&#xA;&#xA; Установите устройство рядом с батареей. После его установки перезапустите модуль BERT.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>Файловая система внутренней флэш - памяти USB повреждена.  Обратитесь в службу технической поддержки.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>Емкость внутренней флэш - памяти USB меньше рекомендуемого размера 1 ГБ.&#xA;&#xA; Установите подходящее устройство рядом с батареей. После его установки перезапустите устройство.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>Произошла ошибка при восстановлении установок . &#xA; Пожалуйста , попробуйте еще раз .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>Все выполняемые в настоящий момент испытания будут остановлены перед загрузкой сохраненных испытаний.</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>Подождите...&#xA; Идет загрузка сохраненных испытаний</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>Недействительное название документа испытания или недействительный путь к файлу.&#xA; Используйте пункт меню Загрузить сохраненное испытание , чтобы найти соответствующий документ.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>При восстановлении испытания произошла ошибка.&#xA; Повторите попытку.&#xA; </translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>Использование зарядного устройства разрешено.</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>Обратите внимание , в данном режиме использование зарядного устройства не разрешено. Использование зарядного устройства будет автоматически разрешено при выборе соответствующих испытаний.</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>Для этого модуля используется дистанционное управление , а &#xA; использование монитора запрещено.</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>Сообщения зарегистрированы. Нажмите для ознакомления</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Журнал сообщений для %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>Сообщений нет</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 сообщение</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 сообщений</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Журнал сообщений для %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Журнал сообщений</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Настройки модема</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>Выберите IP- адрес для адреса данного сервера и IP- адрес , который должен быть назначен абоненту , осуществляющему удаленный доступ</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>IP- адрес сервера</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>IP- адрес абонента</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>Текущее местоположение ( код страны )</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>Невозможно обнаружить модем. Проверьте , установлен ли модем надлежащим образом и повторите подключение.</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>Устройство занято. Отключите все сеансы удаленного доступа и повторите подключение.</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>Невозможно обновить модем. Отключите все сеансы удаленного доступа и повторите подключение.</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>Повторное проведение теста (- ов ) ввиду изменения времени .</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Введите новую вариантную клавишу ниже , чтобы установить ее. </translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Вариантная клавиша</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Установить</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Импортировать</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Свяжитесь с Viavi для приобретения отдельных компонентов программного обеспечения .</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>Идентификатор опции</translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>Клавиша принята ! Перезагрузите компьютер , чтобы активировать новый параметр.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Отказ. ключ. - гнезда параметра с истекшим сроком действия заполнены.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Отказ. ключ. - параметр с истекшим сроком действия уже установлен.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Недействительная клавиша – Попробуйте установить клавишу еще раз.</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>Принят %1 из %2 ключа ( ей ) Перезагрузить для активации новых параметров .</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>Невозможно открыть '%1' на флэш - накопителе USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>При получении информации о параметрах возникла проблема.</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Выбранный байт</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Условные обозначения</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>Условные обозначения :</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI ( не используется )</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Зарезервировано</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>О модуле BERT</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>Транспортный модуль</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Импортировать Quick Card</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Импортировать Quick Card</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> Скрыть Меню</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> Все испытания</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Настроить</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>Один или несколько снимков экрана были сделаны до запуска &#xA; текущего испытания.  Проверьте , что вы выбрали надлежащий файл.</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Выбрать &#xA; Все</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>Очистить выделение &#xA; Все</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>Выбрать &#xA; Снимки экрана</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>Очистить выделение &#xA; Снимки экрана</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Калибровка RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Откалибровать</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Идет калибровка...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>Калибровка не завершена.  Для использования RS-FEC требуется калибровка.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(Калибровка может быть запущена из вкладки RS-FEC в страницах настройки.)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>Повторная попытка калибровки</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>Оставить калибровку</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>Калибровка завершена</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC обычно используется с оптикой SR4, PSM4, CWDM4</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Для выполнения калибровки RS-FEC выполните следующие действия (применительно к CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Вставить адаптер QSFP28 в CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Вставить трансивер QSFP28 в адаптер</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Вставить оптоволоконное петлевое устройство в трансивер</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Нажать КАЛИБРОВАТЬ</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Статус</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Повторная синхронизация</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Необходимо произвести повторную синхронизацию трансивера к тестируемому устройству (ТУ).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Изъять оптоволоконное петлевое устройство из трансивера</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Установление соединения с тестируемым устройством (TУ)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Включить лазер TУ</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Убедится в том, что присутствует зеленый сигнал светодиода CSAM</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Нажать Lane Resync</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>Повторная синхронизация завершена.  Теперь диалоговое окно можно закрыть.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Только для чтения</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>Закрепить в список тестов</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Название файла</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Выбрать...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Только для чтения</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>Сохранить &#xA; профили</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1 профил. (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>Профили </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>только для чтения. Невозможно заменить.</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>Профили сохранены</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>Выбрать логотип</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Файлы изображения (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>Слишком большой файл. Выберите другой файл.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Выбранный байт</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Условные обозначения</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>Начать &#xA; испытание</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Остановить &#xA; испытание</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Остановлен.</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>Режим TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>Полный доступ</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>Только чтение</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>Темн .</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>Свет</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>Быстрый запуск</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>Представление результатов</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>Предполагаемая общая производительность TCP ( Мбит / с ) исходя из :</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>Максим. доступн. полос. пропускания ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>Средн. задержк. в связи с подтвержден. приема ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>Количество параллельных сеансов TCP, необходимое для достижения максимальной производительности :</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Скорость передачи битов</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>Размер окна</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>Сеансы</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>Предполагаемая общая производительность TCP ( Кбит / с ) исходя из :</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>Максим. доступн. полос. пропускания ( Кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Производительность TCP</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>При сохранении параметров испытания произошла ошибка.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>При загрузке параметров испытания произошла ошибка.</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>Выбранный диск переполнен.&#xA; Удалите некоторые файлы и повторите операцию сохранения.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>Следующие файлы сохраненной категории настраиваемых результатов отличаются от загруженных в настоящий момент файлов :</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... %1 друг.</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>Перезапись этих файлов может сказаться на других испытаниях.</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>Продолжить перезапись ?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Да</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>Пожалуйста , подождите ...&#xA; Возобновление установок ...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>Недостаточное количество ресурсов для загрузки испытания %1 в настоящий момент.&#xA; См. меню Испытание для ознакомления с перечнем испытаний , доступных в текущей конфигурации.</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>Невозможно восстановить все настройки испытаний.&#xA; Содержимое файлов устарело или повреждено.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>Режим доступа Только для чтения. Чтобы изменить этот режим воспользуйтесь пунктами меню Сервис -> Режим доступа</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Никакие испытания не выполняются</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>Это приведет к установке всех настроек в значения по умолчанию.&#xA;&#xA; Продолжить ?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>Это приведет к прекращению и перезапуску испытания.&#xA; Настройки испытания будут установлены в значения по умолчанию.&#xA;&#xA; Продолжить ?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>Этот процесс в данный момент запущен . Завершить и начать новый ?&#xA;&#xA; Щелкните " Отменить", чтобы продолжить предыдущий процесс .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>Завершить этот процесс ?&#xA;&#xA; Щелкните " Отменить", чтобы продолжить запуск .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>Невозможно запустить VT100. Последовательное устройство уже используется.</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Порт</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Модуль </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>Обратите внимание , нажатие кнопки Перезапуск очистит результаты в * обоих * портах.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Выбрать&#xA;Испытание</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Испытание</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>Загрузка теста…...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>Сохранить тест как ...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>Загрузить только установки ...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Добавить испытание</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>Удалить испытание</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Справка</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Справочные диаграммы</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Ознакомиться с отчетом...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>Экспортировать отчет ...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>Редактировать информацию о пользователе...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>Импортировать логотип отчета ...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Импортировать из USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>Сохраненный тест ...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>Сохраненная настраиваемая категория...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>Экспортировать в USB</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>Снимок экрана...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>Данные синхронизации...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>Ознакомиться с параметрами / Установить параметры...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>Выполнить снимок экрана</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Руководство пользователя</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Рекомендуемая оптика</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Сетка частот</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Соединения сигналов</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Что это ?</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>Quick Cards</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>Подождите...&#xA; Идет удаление испытания</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Порт 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Порт 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Ничего не выбрано...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>Выбрать файл…</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>Импортировать захват пакетов из USB</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>Выбрать &#xA; файл</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Сохраненный захват пакетов (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>Ознакомиться с отчетом</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Все файлы (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Текст (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Журнал (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Просмотр</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>Просмотреть &#xA;отчет</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>Просмотр других &#xA; отчетов</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>Переименовать &#xA; Отчет</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> уже существует.&#xA; Хотите заменить ?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>Переименованный отчет</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Ошибка – Имя файла не может быть пустым .</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>Отчет сохранен как</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>Нет сохраненных отчетов .</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Пойдите</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>Сохранить данные TIE...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>Сохранить как тип :</translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>Файл HRD</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>Файл CHRD</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>Идет сохранение </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>Это может занять несколько минут...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>Ошибка : Не удалось открыть файл HRD. Пожалуйста , попробуйте сохранить еще раз .</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>Отмена ...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>Данные TIE сохранены .</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>Ошибка : Не удалось сохранить файл . Попробуйте еще раз .</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Внимание</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Закрытие приложения Wander Analysis приведет к потере всех результатов анализа.&#xA; Чтобы продолжить анализ нажмите кнопку Продолжить анализ.</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>Закрыть анализ</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>Продолжить анализ</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Приложение Wander Analysis</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>Обновить &#xA; данные TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>Остановить обновление &#xA;TIE</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>Вычислить &#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>Остановить &#xA; вычисление</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>Сделать &#xA; снимок экрана</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>Загрузить данные &#xA;TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>Остановка TIE&#xA; загрузки</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>Закрыть &#xA; анализ</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>Загрузить данные TIE</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Загрузить</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>Все файлы с отклонениями (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>Коснитесь дважды , чтобы определить прямой угол</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Удалить все</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Удалить</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>Загрузить профиль</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Хотите удалить %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Хотите удалить все профили %1?&#xA;&#xA; Эту операцию нельзя будет отменить.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>( Файлы , предназначенные только для чтения , не будут удалены.)</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 профили (*%2.%3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>Не удается загрузить профиль</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>Загрузка не удалась</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Далее</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Сообщение</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Ошибка</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Пойдите</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>Внимание</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>Что Вы хотите сделать затем ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>Хотите выйти ?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>Восстановить настройки при выходе</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>Выход к результатам</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Назад</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>Пошаговое :</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Следующ.</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>Помогите мне</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Другой порт</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Запуск</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>Перейти...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Другой порт</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Идет запуск испытания Подождите ...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Идет закрытие испытания, подождите…</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Журнал сообщений</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>Главн.</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>Показать действия</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>Отклик : </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Выполняется</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Ничего не выбрано...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Логотип отчета</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Очистить</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Выбрать логотип...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Функция предварительного просмотра недоступна.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Идет запуск испытания Подождите...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Идет закрытие испытания, подождите…</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>Тест выполняется ...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>Оставшееся время :</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Не запущенный</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Тест не завершен</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Испытание завершено</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Отключение автоматических отчетов до запуска сценария.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Включение ранее отключенных автоматических отчетов.</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>Главн.</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>Снимок экрана сохранен</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>Снимок экрана сохранен:</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>Выбор профиля</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>Оперативный уровень</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>Загрузить сохраненный профиль .</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>Какие настройки вы желает e произвести в системе TrueSAM?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>Загрузить настройки с сохраненного профиля</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>Пойдите</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>Создать новый профиль</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>На каком уровне работает Ваша услуга ?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>Уровень 2. Проверка с помощью MAC-адресов, например, 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>Уровень 3. При тестировании используются IP-адреса, например, 192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>Подождите ... переход на выделенный шаг .</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Выбрать испытания</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>Установить связь</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>Настроить расширенный RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>Настройка SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>Настроить J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>Настроить TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>Сохранить конфигурацию</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Испытание</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>Добавить информацию об отчете</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>Запуск выбранных проверок</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Ознакомиться с отчетом</translation>
        </message>
    </context>
</TS>
