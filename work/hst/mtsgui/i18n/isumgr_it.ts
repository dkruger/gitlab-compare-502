<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>Assegnazione di indirizzo OP non valido rifiutata.</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>assegnazione gateway non valida: 172.29.0.7. Riavviare il sistema MTS.</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>L'indirizzo OP è cambiato.  Riavviare il modulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>Inizializzazione modulo BERT.  Richiesta OFF rifiutata.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>Inizializzazione modulo BERT con funzione jitter ottico. Richiesta OFF rifiutata.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>Chiusura Modulo BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>Modulo BERT OFF</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>Lo schermo BERT è spento. Premere HOME/SISTEMA e poi l'icona BERT per attivarlo.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>Modulo BERT ON</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** AGGIORNAMENTO SOFTWARE APPLICAZIONE *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** AGGIORNAMENTO COMPLETATO *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** AGGIORNAMENTO SOFTWARE JITTER *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** AGGIORNAMENTO SOFTWARE KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** AVVIO AGGIORNAMENTO *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>Avaria UI modulo BERT</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>Inizializzazione UI modulo BERT con funzione jitter ottico.</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>Avvio UI modulo BERT</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>Avaria UI modulo BERT. Modulo OFF</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>Modulo 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>Modulo 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>Modulo 0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>Modulo 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Modulo </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>Modulo 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>Modulo 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>Modulo 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>Modulo 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>Modulo 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>Modulo 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>Modulo 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>Modulo 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>Modulo 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>Modulo 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>Modulo 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>Modulo 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>Modulo 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>Modulo 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>Modulo 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>Modulo 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>Modulo 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>Modulo 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>Aggiornamento software di base necessario. La versione corrente non è compatibile con il modulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>È necessaria la reinstallazione del software BERT. Il corretto software BERT per il modulo BERT connesso non è installato.</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>Aggiornamento hardware BERT necessario. L'hardware corrente non è compatibile.</translation>
        </message>
    </context>
</TS>
