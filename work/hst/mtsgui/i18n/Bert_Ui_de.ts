<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation>Sind Sie sicher, dass Sie den Vorgang abbrechen möchten?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>Es ist ein Fehler beim Lesen der Datei aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>Die Datei hat zu viele Pakete (mehr als 50,000). Wenn möglich nur einen Teilbuffer speichern,&#xA;oder die Datei zu einem USB Speichermedium exportieren und auf einem PC/Laptop öffnen.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>Möchten Sie alle gespeicherten Daten löschen?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>Möchten Sie die Auswahl löschen?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>Name ist schon vergeben.&#xA;Möchten Sie alte Werte überschreiben?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>Beim Schreiben der Diagramme auf den Datenträger ist ein Fehler aufgetreten. Speichern&#xA;Sie die Diagramme jetzt, um keine Daten zu verlieren. Die Diagrammerstellung wird unterbrochen und die Diagramme werden automatisch gelöscht, wenn eine kritische Menge erreicht ist.</translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>Während der Diagrammerstellung sind zu viele Datenträgerfehler aufgetreten. Die Diagrammerstellung wird beendet.&#xA;Die Diagrammdaten wurden gelöscht. Bei Bedarf können Sie die Diagrammerstellung neu starten.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>USB-Flashmedium</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>Wechselmedium</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>Protokoll ist voll</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>Ungültige Konfiguration.&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Ungültiger</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>Sprache</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Daten 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Daten 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>CFP MSA Mgmt I/F Spezifikationsüberprüfung durch SW unterstützt</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>QSFP MSA Mgmt I/F Spezifikationsüberprüfung durch SW unterstützt</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>DMC Info</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>Barcode</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>Herstellungsdatum</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>Kalibrierungsdatum</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>SW-Version</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>Option Challenge-ID</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>Baugruppen-Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>Montagebarcode</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>Installierte Software</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Optionen:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>Warte auf Pakete...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>Pakete werden analysiert...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>Warte auf Link...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>Drücken Sie 'Analyse starten', um zu beginnen...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Ergebnisse werden gespeichert, bitte warten.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Ergebnisse können nicht gespeichert werden.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Ergebnisse werden gespeichert.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>Warte auf den Start</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>Der vorherige Test wurde vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>Der vorherige Test wurde fehlerhaft abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>Der vorherige Test ist fehlgeschlagen und Stoppen bei Fehler wurde aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Nicht erlauben</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>Restzeit: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Läuft</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>Der Test konnte nicht beendet werden und wurde fehlerhaft abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>Test vollständig</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>Test vollständig  mit Fehlermeldungen</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Bestanden</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>Test vollständig und bestanden</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>Test vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Anhalten</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Kein Test aktiv</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Starten</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>Verbindung bestätigt</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>Verbindung verloren</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>Prüfe Verbindung</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Ungültige Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Ergebnisse werden gespeichert, bitte warten.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Ergebnisse können nicht gespeichert werden.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Ergebnisse werden gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Durchlauf Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Ergebnis Durchsatztest</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Upstream Durchlauf Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Upstream Durchlauf Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Downstream Durchlauf Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Downstream Durchlauf Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalien</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>Upstream Anomalien</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>Downstream Anomalien</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Latenztestdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Latenztestergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Upstream Latenz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Upstream Latenz Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Downstream Latenz Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Downstream Latenz Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Jitter Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Jitter Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Upstream Jitter Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Upstream Jitter Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Downstream Jitter Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Downstream Jitter Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 Byte Frame Verlust Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 Byte Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 Byte Upstream Frame Verlust Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 Byte Upstream Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 Byte Downstream Frame Verlust Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 Byte Downstream Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>CBS-Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Upstream CBS Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Downstream CBS Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>CBS Kontolle Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Upstream CBS Kontroll-Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Downstream CBS Kontroll-Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Burst Jagdtestergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Upstream Burst Jagd Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Downstream Burst Jagd Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Back to Back Test Ergebnis</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Upstream Aufeinanderfolgende Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Downstream Aufeinanderfolgende Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Systemwiederherstellung Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Prüfergebnisse Systemerholung</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Upstream Systemwiederherstellung Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Upstream Systemwiederherstellung Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Downstream Systemwiederherstellung Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Downstream Systemwiederherstellung Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Ausgedehnter Lasttest Ergebnisse</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>Setup Pfad-MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>RTT-Schritt</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>Upstream Walk the Window-Schritt</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>Downstream Walk the Window-Schritt</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>Upstream TCP-Durchsatzschritt</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>Downstream TCP-Durchsatzschritt</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Ergebnisse werden gespeichert, bitte warten.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Ergebnisse können nicht gespeichert werden.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Ergebnisse werden gespeichert.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Nicht möglich sync zu bekommen. Bitte vergewissern Sie sich, daß alle Kabel angeschlossen sind und der Port ordnungsgemäß konfiguriert ist.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Wenn Sie keinen aktiven Link finden können: Stellen Sie sicher. Daß alle Kabel angeschlossen sind und der Port ordnungsgemäß konfiguriert ist.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Sie können die Geschwindigkeit oder das Duplex des verbundenen Ports nicht bestimmen. Stellen Sie sicher, daß alle Kabel verbunden sind und der Port ordnungsgemäß konfiguriert ist.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>Für den lokalen Tester konnte keine IP-Adresse abgerufen werden. Prüfen Sie Ihre Kommunikationseinstellungen und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>Es kann kein Kommunikationskanal zum fernen Tester aufgebaut werden. Prüfen Sie Ihre Kommunikationseinstellungen und versuchen Sie es erneut.</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>Das Remote-Testset scheint über keine kompatible Version  der BERT-Software zu verfügen. Die minimale Kompatibiltät liegt bei %1.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>Die Ebene 4 TCP der Wirespeed-Anwendung muß sowohl für die lokale Unit als auch für die Fernsteuerungs-Unit empfangbar sein, damit TrueSpeed funktioniert.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>Die TrueSpeed-Testauswahl ist für diese Verkapselungseinstellung nicht gültig und wird entfernt.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>Die TrueSpeed-Testauswahl ist für diese Frametypeeinstellung nicht gültig und wird entfernt.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>Die TrueSpeed-Testauswahl ist für die ferne Verkapselungseinstellung nicht gültig und wird entfernt.</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>Die Testauswahl SAM-Complete ist für diese Verkapselungseinstellung nicht gültig und wird entfernt.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Ungültige Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>CBS-Überwachung</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Überwachung</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>Schritt #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>Schritt #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>Schritt #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>Schritt #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>Schritt #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>Schritt #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>Schritt #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>Schritt #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>Schritt #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>Schritt #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Ergebnisse werden gespeichert, bitte warten.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Ergebnisse können nicht gespeichert werden.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>Ergebnisse werden gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>Lokaler ARP fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>Ferner ARP fehlgeschlagen.</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> Liste, fortg.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>Screenshot</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - Das Histogramm geht über mehrere Seiten</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>Zeitskala</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>Benutzerinformation</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>Configuration Groups</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>Resultatgruppen</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>Ereignisprotokolle</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>Histogramme</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>Screenshots</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>Geschätzter TCP-Durchsatz</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation>Bericht für mehrere Tests</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Generiert von Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Generiert von Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Generiert von Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Erstellt von dem Viavi-Prüfgerät </translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>Scan-Frequenz (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Erfolg / Fehl</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>Einrichtung: Filter/</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> (Muster und Maske)</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Muster</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Maske</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Keine zutreffenden Konfigurationen...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI-Check</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>Radio</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>Band</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>Gesamttestergebnis des CPRI-Check</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test unvollständig</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>Fortschritt</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nein.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>Ereignisname</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>Laufzeit/Wert</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>Ausgedehnter Lasttest Ereignis Protokoll</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Unbekannt - Status</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>Fortschritt. Bitte warten ...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>Fehlgeschlagen!</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>Befehl ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>Abgebrochen!</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Loop Up</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Loop Down</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Arm</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>Entschärfen</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>Ausschalten</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>Loop-Kommando senden</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>Umschalten</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>Switch zurückgesetzt</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>Anfragen</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>Loopback Query</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>Lokal scharfschalten</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>Lokal entschärfen</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>Stromabfrage</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>Span-Abfrage</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>Unterbrechung&#xA;deaktivieren</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>Unterbrechung&#xA;zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>Sequentielle Loop</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Stratum 1 Trace</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 Reserved</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 Reserved</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 Transit Node Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 Reserved</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 Reserved</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Stratum 2 Trace</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 Reserved</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 Reserved</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Stratum 3 Trace</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 Reserved</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Sonet Min Clock Trace</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Stratum 3E Trace</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Provision by Netwk Op</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Don't Use for Synchronization</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Unspezifisch bestückt</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>TUG-Struktur</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>Locked TU</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>Asynchrones 34M/45M</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>Asynchrones 140M</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>ATM Mapping</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>MAN (DQDB) Mapping</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>FDDI Mapping</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>HDLC/PPP-Mapping</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 Unverschlüsselt</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>O.181 Testsignal</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>Asynchron</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>Bitsynchron</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>Bytesynchron</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reserviert</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>VT-strukturierte STS-1 SPE</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>Locked VT Mode</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>Async. DS3 Mapping</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>Async. DS4NA Mapping</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>Async. FDDI Mapping</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 VT Payloaddefekt</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 VT Payloaddefekte</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dd %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dd %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>Format?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Außerhalb des Bereichs</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>AUS</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>EIN</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>HISTORIE</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Überlauf</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + HISTORIE</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>Abstand</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>Marke</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>GRÜN</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>GELB</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>ROT</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>KEINE</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>ALLE</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>ABWEISEN</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>UNGEWISS</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>ACCEPT</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>UNBEKANNT</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FEHLER</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ERFOLG</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Läuft</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test unvollständig</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Status unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>Identifiziert</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>Identifizierung nicht möglich</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>Indentität unbekannt</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>Noch %1 Stunden und %2 Minuten</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>Noch %1 Minuten</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>ZU NIEDRIG</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>ZU HOCH</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>(ZU NIEDRIG) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>(ZU HOCH) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Wert</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Trib Port</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>Undefiniert</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Nicht bestückt</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>Mapping in Entwicklung</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC over SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>Einfaches Data Link Mapping (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>HCLC/LAP-S Mapping</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>Einfaches Data Link Mapping (set-reset)</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>10 Gbit/s Ethernet Rahmen-Mapping</translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>GFP Mapping</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>10 Gbit/s Fiber Channel Mapping</translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>Reserviert - Proprietär</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>Reserviert - National</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>Testsignal O.181-Mapping</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>Reserved (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Unspezifisch bestückt</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>Asynchrones DS3-Mapping</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>Asynchrones DS4NA-Mapping</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>Asynchrones FDDI-Mapping</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC over SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 VT Payloaddefekt</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI nicht zugewiesen</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Erwart. (TEI)</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Erwart. TEI (ca.)</translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI Zugeordnet</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Erwart. (ca.)</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Erwart. (rel.)</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Mult. Rhm. ca.</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Zeitgeberwiederherstellung</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Verbindung unbekannt</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>WARTE AUF AUFBAU</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>MULTIFRAME HERGESTELLT</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>AUFGELEGT</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>WÄHLTON</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>KOMPLETT WÄHLEN</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>KLINGELN</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>VERBUNDEN</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>VERBINDUNG LÖSEN</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>Sprache</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Daten</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Tele 64</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Videotex</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>Sprache BC</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>Daten BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>Dat. 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Suche</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Verfügbar</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>Verknüpfungsanforderung</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>Wiederholungsanforderung</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>Beibehalten</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Ack Burst abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>Verknüpfungsantwort</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>Burst abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>Statusantwort</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>Loch in Datenstrom bekannt</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>Fehler: Noch kein Puffer für Dienst verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>Fehler: Anfrage für Wiederholung des Pakets ungültig</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>Fehler: Unbekannter Service</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>Fehler: Unbekannter Abschnitt</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>Fehler: Sitzungsfehler</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>Fehler: Nicht unterstützte Befehls- und Steuerungsversion</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>Fehler: Server voll</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>Fehler: Doppelte Subskription</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>Fehler: Doppelte Sitzungs-IDs</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>Fehler: Falsche Bitrate</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>Fehler: Sitzung von Server beendet</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>Kurzschluss</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>Offen</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>Normal</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>Umgekehrt</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>Pegel zu niedrig</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Nein</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>Gewählt</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Nicht ausgewählt</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>START</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOPP</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Fehlerhafte Rahmen</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS-Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Verlorene Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Codeverletzungen</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>Ereignisprotokoll ist voll</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>Kein USB-Schlüssel gefunden. Bitte tragen Sie diesen ein und versuchen Sie es erneut.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>Keine Hilfe für dieses Element verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>Einheit-Id</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>Kein Signal</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Signal</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Bereit</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>Verwendet</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/Nein (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>Satelliten ID</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>GNSS IDENTIFIKATION</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res.</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Statistik</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Rahmung</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exportieren</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Maske</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>MTIE-Maske</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>TDEV-Maske</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>MTIE-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>MTIE-Maske</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>TDEV-Ergebnisse</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>TDEV-Maske</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Urspr. TIE-Daten</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Daten ohne entfernte Verstimmung</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>MTIE/TDEV Kuvengestaltung</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>Kurve + Punkte</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>Nur Punkte</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>Nur MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>Nur TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>Maskentyp</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>MTIE bestanden</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>MTIE fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>TDEV bestanden</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>TDEV fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>Überwachungsintervall (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>Berechnung wird ausgeführt </translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>Berechnung abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>Berechnung beendet</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>TIE-Daten werden aktualisiert </translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>TIE-Daten geladen</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>Keine TIE-Daten geladen</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>Nicht genügend Speicher für lokale Ausführung der Wanderanalyse.&#xA;256 MB RAM sind erforderlich. Verwenden Sie stattdessen eine externe Analysesoftware.&#xA;Im Handbuch finden Sie ausführliche Informationen hierzu.</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Frequenzversatz (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>Driftrate</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>Abtastpunkte</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>Musterquote (pro Sek.)</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>Blöcke</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>Aktueller Block</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>Verstimmung entfernen</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>Kurvenauswahl</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>Beide Kurven</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Vers.entfernen</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>Bildschirmfoto erstellen</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Zeit (s)</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Lokal</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Fern</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>Nicht gekennzeichnet</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>Port 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>Port 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>Toolbar</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Nachrichtenaufzeichnung</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>Allgemeine Informationen:</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>Grafiken deaktiviert</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Ausgabefehler!</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog.-Nr.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbit/s Min.</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbit/s Max</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR Jitter</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR-Jitter Max.</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC Fehler</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT Fehler</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID Fehler</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID-Fehler Max.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Datenströme</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP-Prüfsummenfehler</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP-Prüfsummenfehler</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>Transportstrom-ID</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pak.-Verlust Ges.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pak.-Verlust Akt.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pak.-Verlust Spitze</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Pak.-Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Pak.-Jitter Max. (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>OoS-Pak. Ges.</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS-Pak. Akt.</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS-Pak. Max.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Abst.-Fehler Ges.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Abst.-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Abst.-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Periodenfehl. Ges.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Periodenfehl. Akt.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Periodenfehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Max. Verlustzeitraum</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>Min. Verlustabst.</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Sync-Verluste Ges.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Sync-Byte-Fehl. Ges.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Sync-Byte-Fehl. Akt.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Sync-Byte-Fehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF Akt.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF Max.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR Akt.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR Max.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Transp.-Fehl. Ges.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Transp.-Fehl. Akt.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Transp.-Fehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT-Fehler Max.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation># Analysierte Datenströme</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Gesamt L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP-Prüfsummenfehler</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP-Prüfsummenfehler</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>Transportstrom-ID</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>Prog.-Nr.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Prog Mbit/s Akt.</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Prog Mbit/s Min.</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Prog Mbit/s Max.</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pak.-Verlust Ges.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pak.-Verlust Akt.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pak.-Verlust Spitze</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Pak.-Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Pak.-Jitter Max. (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>OoS Pkts Tot</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS-Pak. Akt.</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS-Pak. Max.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Abst.-Fehler Ges.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Abst.-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Abst.-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Periodenfehl. Ges.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Periodenfehl. Akt.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Periodenfehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Max. Verlustzeitraum</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>Min. Verlustabst. </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Sync-Verluste Ges.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Sync-Byte-Fehl. Ges.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Sync-Byte-Fehl. Akt.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Sync-Byte-Fehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF Akt.</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF Max.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR Akt.</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR Max.</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>PCR-Jitter Akt.</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR-Jitter Max.</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>CC-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>CC-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Transp.-Fehl. Ges.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Transp.-Fehl. Akt.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Transp.-Fehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>PAT-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>PAT-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>PMT-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>PMT-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>PID-Fehler Gesamt</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>PID-Fehler Akt.</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID-Fehler Max.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>IP-Adr.</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Pkt Vrlst</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Pak.-Verlust Max.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Pak.-Jit.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Pak.-Jit. Max.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation># Datenströme</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Rx Mbit/s, Akt. L1</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Prog Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Prog Mbit/s Min. </translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>Transport-ID</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog.-Nr.</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Sync-Verluste</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>Ges. Sync-Byte-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Sync-Byte-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Sync-Byte-Fehl. Max.</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>Ges. PAT-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>PAT Fehler</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>PAT-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR Jitter</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>PCR-Jitter Max. </translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>Gesamt CC-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>CC Fehler</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>CC-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>Ges. PMT-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>PMT Fehler</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>PMT-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>Ges. PID-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>PID Fehler</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>PID-Fehler Max.</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>Ges. Transp.-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Transp.-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Transp.-Fehl. Max.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation># Analysierte Datenströme</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Gesamt L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>IP-Prüfsummenfehler</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>UDP-Prüfsummenfehler</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>MPEG-Historie</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Pak.-Verlust Akt.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Pak.-Verlust Ges.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Pak.-Verlust Spitze</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Pak.-Jitter Akt.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Pak.-Jitter Max</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Peak-Peak</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos-Peak</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Peak</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Peak-Peak</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Pos-Peak</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Neg-Peak</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informationen zum Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Code</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Pfad</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Kanal</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>nicht benutzt</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>Optik Gesamttestergebnis </translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Optik Selbsttest</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Details</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Gestapelt</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>Gestapeltes VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>VLAN-Stapeltiefe</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>SVLAN %1 DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>SVLAN %1 Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>Anwender-SVLAN %1 TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>Es wurden keine Rahmen definiert</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Wert</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Trib Port</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Konfig.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>OTN Check-Test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>Umfassendes Testergebnis OTN-Check</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN-Check</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test unvollständig</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>POH-Byteaufzeichnung</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binär</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Keine zutreffenden Konfigurationen...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>Overhead-Bytes</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>CDMA Empfänger</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Ausgabefehler!</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>PTP Test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>Geladenes Profil</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>PTP-Gesamttestergebnis</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP-Check</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test unvollständig</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>ALLE ERGEBNISSE OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Erweiterte FC-Test</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>Erweiterter RFC 2544 Test</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>Gesamtergebnis</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modus</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>Gesamttestergebnisse</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Laufzeit</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Systemerholung</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Ausgedehnte Last</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Nicht ausgeführt - Der vorherige Test wurde vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Nicht ausgeführt - Der vorherige Test wurde fehlerhaft beendet</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Nicht ausgeführt - Der vorherige Test ist fehlgeschlagen und Stoppen bei Fehler wurde aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Nicht erlauben</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>Symmetrischer Loopback</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>Symmetrisches Upstream und Downstream</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>Asymmetrischer Upstream und Downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Laufzeit</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Paket-Jitter</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Burst (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Systemerholung</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Ausgedehnte Last</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Auszuführende Tests</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>Gewählte Frame Längen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>Gewählte Paketlängen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>Gewählte Upstream Frame  Längen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>Gewählte Upstream Paket Längen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>Gewählte Downstream Frame  Längen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>Gewählte Downstream Paket Längen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>Downstream Frame Längen (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 Byte Frame Verlust Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 Byte Upstream Frame Verlust Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 Byte Downstream Frame Verlust Testdiagramm</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 Byte Zwischenspeicher Kreditdurchlauf Testdiagramm</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dauer (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Überlauf</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dauer (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>Dauer (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Überlauf</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Längster</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Kürzester</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Letzter</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Durchschnitt</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Unterbrechungen</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Gesamt</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Anschluss</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Nominelle Wellenlänge (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Nennbitrate (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Wellenlänge (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>Minimale Bitrate (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>Maximale Bitrate (Mbits/s)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Leistungspegeltyp</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Anbieter</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>Anbieter PN</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Anbieter Vers.</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Max. Rx-Pegel (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Max. Tx-Pegel (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>Anbieter SN</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Datumscode</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Chargencode</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Diagnoseüberwachung</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Diagnosebyte</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Transceiver</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW-/SW-Versionsnummer</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA-HW-Spez. Rev-Nr.</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA-Verwaltung I/F Rev-Nr.</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Leistungsklasse</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Rx Leistungspegeltyp</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>Max. Lambda-Stärke (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>Anzahl aktiver Fasern</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Wellenlängen pro Faser </translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL pro Faserbereich (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Max. Netzspur-Bitrate (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Unterstützte Raten</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Dauer</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Ungültiger</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Frameverzögerung (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Frameverzögerung (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Einweg-Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>Zeile</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Konfig.:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>Wechsel</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>Fehlerbehebung</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modus</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Symmetrisch</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asymmetrisch</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>Durchsatzsymmetrie</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Pfad MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>Erweiterte TCP</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>Auszuführende Schritte</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Nicht ausgeführt - Der vorherige Test wurde vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Nicht ausgeführt - Der vorherige Test wurde fehlerhaft beendet</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Nicht ausgeführt - Der vorherige Test ist fehlgeschlagen und Stoppen bei Fehler wurde aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Nicht erlauben</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Geschwindigkeit VNF Test</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test unvollständig</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>Der Test wurde durch den Anwender abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>Der Test wurde nicht gestartet.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>Upstream Erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>Der Durchlauf beträgt mehr als 90% der Zielvorgabe.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>Upstream Nicht Erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>Der Durchlauf liegt unter 90% der Zielvorgabe.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>Downstream Erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>Downstream nicht erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informationen zum Testbericht</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Name des Technikers</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Unternehmen</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Email</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Telefon</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Testidentifizierung</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Testname</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Bestätigungscode</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Bestätigungscode Erstellung</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Test Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Kommentare</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Trace</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>Sequenz</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Protokoll:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>Strom-IP:Port</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Stromname</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dauer/Wert</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Prog.-Name</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>Fortschritt</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>VLAN Scan Gesamttestergebnis</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN Scan Test</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Konfig:</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Resultate:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>TrueSAM-Gesamttestergebnis</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>Der Test konnte nicht beendet werden und wurde fehlerhaft abgebrochen. Die Ergebnisse des Berichts sind möglicherweise unvollständig.</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>Test vom Benutzer gestoppt. Die Ergebnisse des Berichts sind möglicherweise unvollständig.</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>Vor-Prüfungsergebnisse</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>J-QuickCheck-Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>RFC 2544-Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>SAMComplete Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>J-Proof-Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>TrueSpeed-Testergebnis</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Nicht ausgeführt - Der vorherige Test wurde vom Benutzer gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Nicht ausgeführt - Der vorherige Test wurde fehlerhaft beendet</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Nicht ausgeführt - Der vorherige Test ist fehlgeschlagen und Stoppen bei Fehler wurde aktiviert</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Nicht erlauben</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Nachrichtenaufzeichnung</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>Mitteilungslog (Fortsetzung)</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Name des Kunden</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>Techniker-ID</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ort des Tests</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Arbeitsauftrag</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Anmerkungen/Hinweise</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Instrument</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Seriennummer</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>SW Version</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>SAMComplete-Gesamttestergebnis</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Startdatum</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Enddatum</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>Testergebnisse Gesamtkonfiguration </translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>Testergebnisse Gesamtleistung</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Rahmenverlust</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Verzögerungsvariation</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Durchsatz</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>Berichtsgruppen</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>Der bericht konnte nicht erstellt werden: </translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>nicht genügend Festplattenspeicherplatz</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Bericht konnte nicht erstellt werden</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>Port-Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Keine</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>Ungerade</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>Gerade</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>Baudrate</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>Parität</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Flusskontrolle</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Aus</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>Datenbits</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>Stoppbits</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>Terminal-Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>Eingabetaste</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>Lokales Echo</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>Reservierte Tasten aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>Deaktivierte Tasten</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Strg+U, Strg+Y, Strg+P, Strg+M, Strg+R, Strg+F, Strg+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>Deaktivieren</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Aktivieren</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>EXPORT, DATEI, SETUP, ERGEBNISSE, SCRIPT, START/STOPP, Bildschirmtasten</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>DRUCKEN, DATEI, KONFIG., RESULTATE, MAKRO, START/STOP, Bedienfeld-SoftKeys</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>DATEI, KONFIG., RESULTATE, MAKRO, START/STOP, Bedienfeld-SoftKeys</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, SETUP, Ergebnisse, EXPORT, START/STOP, Panel Soft Keys</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>Terminal&#xA;Fenster</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>Standardwerte&#xA;setzen</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>VT100&#xA;Konfig.</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>Löschen&#xA;Bildschirm</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>Anzeigen&#xA;Tastatur</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>Verschieben&#xA;Tastatur</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>automatische Datenratenanpassung</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Erfassen&#xA;Bildschirm</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>Ausblenden&#xA;Tastatur</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>Loop Progress:</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>Resultat:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Unerwarteter Fehler aufgetreten</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Skript&#xA;ausführen</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA;Bei diesem Vorgang wird die Leitung erneut nach Datenströmen durchsucht, und die Prüfung wird erneut durchgeführt.&#xA;&#xA;Es werden nicht mehr nur die Datenströme angezeigt,&#xA; die von der Explorer-Anwendung übertragen wurden.&#xA;&#xA;Möchten Sie fortfahren?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>Fortschritt autom. Konfiguration:</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>Detail:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>Autom. Modus läuft. Bitte warten...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>Auto fehlgeschlagen.</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>Auto abgeschlossen.</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>Auto vorzeitig beendet.</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Unbekannt - Status</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>Erkennen </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Scannen...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fehlgeschlagen</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abgebrochen</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>Unerwarteter Fehler aufgetreten</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>Details gekoppeltes Gerät</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>Datei senden</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Verbinden</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Vergessen</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Datei auswählen</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Senden</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Verbinden...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>Verbindung wird getrennt...</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Trennen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>Bündelname: </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>Bundle Name eingeben:</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>Zertifikate und Schlüssel (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>Ungültiger  Bundle Name</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>Ein "%1" genanntes Bundle existiert schon.&#xA;Bitte wählen Sie einen anderen Namen.</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>Dateien hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>Bundle erstellen</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>Bundle neu bezeichnen</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>Bundle modifizieren</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>Offener Ordner</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>Neue Bundle hinzufügen ...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>Zertifikate hinzufügen zu  %1 ...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>Löschen %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>%1 von %2 löschen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>Virtuelle Spur ID</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>Injektierte Schiefe (Bits)</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>Injektierte Schiefe (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>Physikalische Bahn #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Bereich:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>Datum konnte nicht festgelegt werden</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Ziffern</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>Zuflussstelle</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>Aktivierung</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Default</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gb/s</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>Bandbreite</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>Änderungen sind noch nicht umgesetzt.</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>Zu viele Zuflussstellen sind gewählt.</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>Zu wenige Zuflussstellen sind gewählt.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>Weiteres...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> Zeichen</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> Ziffern</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>Heute</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>Datum eingeben: tt/mm/jjjj</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>Datum eingeben: mm/tt/jjjj</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>bis zu </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> Ziffern</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> Ziffern</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Sekunden</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Minuten</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Stunden</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Tage</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>tt/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh:mm</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>Dauer: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>Konfigurieren Sie die Zieladresse auf der Seite "Alle Dienste".</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>Konfigurieren Ziel-MAC-Adresse auf der Seite "Alle Datenströme".</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>Ziel-MAC</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Zieltyp</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Loop Type</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>Dieser HOP Quellen-IP</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>Next Hop Ziel-IP-Adr.</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>Next Hop Subnetzmaske</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>Bitte Quelladresse für alle Rahmen  im Reiter "Ethernet" konfigurieren</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Quellentyp</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>Default MAC</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>Benutzer-MAC</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>Konfigurieren Quellen-MAC-Adresse auf der Seite "Alle Diensten".</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>Konfigurieren Quellen-MAC-Adresse auf der Seite "Alle Datenströme".</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>Quellen-MAC</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN Benutzerpri.</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>PBit-Inkrement</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>Anwender-SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>Nicht im Loopback-Modus konfigurierbar.</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>VLAN-ID angeben</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Länge</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>Datenlänge (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Control</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>Ether-Typ</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>Typ/&#xA;Länge</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>Tunnel&#xA;Label</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>B-Tag VLAN-ID-Filter</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>B-Tag Priorität</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>B-Tag DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>Tunnel-Label</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>Tunnelpriorität</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>B-Tag Ether-Typ</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>Tunnel-TTL</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC&#xA;Wert</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>I-Tag Priorität</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>I-Tag DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>I-Tag UCA-Bit</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>I-Tag Service-ID-Filter</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>I-Tag Service-ID</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>VC-Label</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>VC-Priorität</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>I-Tag Ether-Typ</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA;Label</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>MPLS1 Kennung</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>MPLS1 Priorität</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA;Label</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>MPLS2 Kennung</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>MPLS2 Priorität</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Daten</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>Kundenspezifischer Rahmen:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Verkapselung</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Rahmentyp</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>Kunden Frame Size</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Benutzerrahmengröße</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>Zu kleine Größe</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>Jumbo-Größe</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>Datenenthalten IP-Pakete. Paket kann auf der IP-Seite konfiguriert werden.</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>Configure data filtering on the IP Filter tab.</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Tx Payload</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>RTD-Einrichtung</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>Payloadanalyse</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Rx Payload</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>LPAC Timer</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Rx BERT-Muster</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Tx BERT-Muster</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>Benutzermuster</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>Configure incoming frames:</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>Configure outgoing frames:</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>Länge/Typ-Feld ist 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>Zufällige Datenlänge</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Benutzerpriorität Anfangsindex</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>Dateityp:</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Dateiname:</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Folgendes wirklich löschen&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>Möchten Sie wirklich alle Dateien in diesem Ordner löschen?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Ziffern</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation> Byte</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>bis zu </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Bereich:  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> (Hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> Ziffern</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> ist ungültig - Quell-IPs von Port 1 und 2 dürfen nicht gleich sein. Vorherige IP-Einstellung wiederhergestellt.</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>Ungültige IP-Adresse&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Bereich:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>Die angegebene IP-Adresse ist für diese Konfiguration nicht geeignet.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>2 Zeichen pro Byte, bis zu </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Bytes</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>2 Zeichen pro Byte, </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>Loop-Code name</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>Bitmuster</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>Auslieferung</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>Eingebunden</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>Out of Band</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Loop Up</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Loop Down</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Weiteres</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>Loop-Code Name</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Lö.</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>Hinzufügen/Entfernen</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Tx Trace</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>Bereich:  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>Kanal auswählen</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>KLM-Wert eingeben</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>Bereich:  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>VCG-Mitglied auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>PSI-Byte</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>Bytewert</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>ODU Typ</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>Tributary Port #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Bereich:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>VCG-Mitglied auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Bereich:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>Overhead-Byte Editor</translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>Wählen Sie ein gültiges Byte zur Bearbeitung aus.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>Nicht erkennbare Optik</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Die Seite wird erstellt. Bitte warten...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>Die Seite ist leer.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>Element hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>Element ändern</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>Element löschen</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>Zeile hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>Zeile ändern</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>Zeile löschen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>Aus</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 Traceability Unknown</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Nicht bestückt</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Nicht bestückt</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Nicht bestückt</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Nicht bestückt</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Länge:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> Zeichen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Länge:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> Zeichen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>Jetzt</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>Zeit eingeben: hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Kein auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>Byte %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation>Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Dienstname</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>Beide Richtungen</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>Servicetyp</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>Servicetyp wurde auf 'Daten' zurückgesetzt, da sich die Framelänge oder der CIR-Wert geändert hat.</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>Abtastrate (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Anrufe</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Rahmengröße</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Paketlänge</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># Kanäle</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Komprimierung</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>VCG&#xA;erstellen</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>VCG-Mitglieder mit Standardkanalnummerierung definieren:</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Tx VCG definieren</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Rx VCG definieren</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>Nutzlast-Bitrate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>Anzahl Mitglieder</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>Verteilen&#xA;Mitglieder</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>VCG-Mitglieder verteilen</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>Kundenspezifische Verteilung von VCG-Mitgliedern definieren</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>Beispiel</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Schritt</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>VCG-Mitglieder&#xA;bearbeiten</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>Adressformat</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>Neues Mitglied</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Default</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>Angewählte Ströme wurden aus dem Explorer importiert.&#xA;Drücken Sie [Ströme anwählen...] zum anwählen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Adressbuch</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Nichts auswählen</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>Finde nächsten</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>Entfernen</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Nichts auswählen</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>Datenströme verknüpfen...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>Hinzuf</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>Quellen-IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>IPs wurden zum Adressbuch hinzugefügt.</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Zu verknüpfen</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>Schon hinzugefügt</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>verknüpfen</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Datenstrom verlassen</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Quellen-IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>Ziel-IP-Adresse</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Datenstrom verlassen...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Kein auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>Direktkonfiguration</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>Hinweis: Die aktuelle Rahmenkonfiguration wird überschrieben.</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Schnell (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>Voll (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensität</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Familie</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Verkapselung</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>PBit-Inkrement</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Benutzerpriorität</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (Hex)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>Anwender TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Anwenden</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Benutzerpriorität Anfangsindex</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN Benutzerpri.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>Laden...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Langes Datum:</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Kurzes Datum:</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Langen Uhrzeit:</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Kurzen Uhrzeit:</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Zahlenwerte:</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>Nicht zugeordnet</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>Zugewiesen</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>Belegt10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reserviert11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reserved01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Reserviert00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Speichern...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Test auf Standardeinstellungen zurücksetzen</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>Konfiguriere&#xA; Ströme...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>Lastverteilung</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Alle&#xA;auswählen</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>Alle&#xA;löschen</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>Autom.&#xA;Verteilung</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Strom</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Rahmengröße</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>Rahmenrate</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% der Line-Rate</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>Ramp startet bei</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>Konstant</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>Max Lastschwelle</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>Gesamt (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>Gesamt (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>Gesamt (kbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>Gesamt (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>Hinweis:</translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>Mindestens ein Datenstrom muss aktiviert sein.</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>Die maximale Lastschwelle ist </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>Die maximal zulässige Last ist </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>Angegebene Gesamtlast darf nicht höher sein.</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>Prozentsatz eingeben:  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>Rahmenverhältnis eingeben:  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>Bitrate eingeben:  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>Hinweis:&#xA;Bitrate wurde nicht festgestellt. Bitte klicken Sie auf Abbrechen&#xA; und versuchen Sie es erneut, wenn die Bitrate festgestellt wurde.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>Triple Play Einstellungen definieren</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>Abtastrate&#xA;(ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation># Anrufe</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>Rate pro&#xA;Anruf (Kbits)</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>Gesamtrate&#xA;(Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>Gesamt Basic-&#xA;rahmengröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>Ruhe-Ausblendung</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>Jitterpuffer (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation># Kanäle</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Komprimierung</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Rate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>Gesamt Basicrahmengröße (Bytes)</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>Startrate (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>Lasttyp</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>Zeitschritt (sec)</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>Laststufe (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Gesamt L1 Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>Simuliert</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Daten 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Daten 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>Daten 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>Daten 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Zufall</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Sprache</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>Video</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Daten</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>Hinweis:</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>Mindestens ein Dienst muss aktiviert sein.</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>Die maximal zulässige Last ist </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>Angegebene Gesamtlast darf nicht höher sein.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>Verteilung: </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>Bandbreite: </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>Struktur: </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>Standard</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Schritt</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Adressbuch</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>Neuer Eintrag</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>Quellen IP (0.0.0.0 = "irgendwelche")</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>erforderlich</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>Name (max 16 Zeichen)</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>Eintrag hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>Import/Export</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Import</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>"Importiere Einträge von USB</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportieren</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>Einträge auf ein USB-Laufwerk exportieren</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>Speichern und verlasssen</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>Optional</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>Importiere Einträge von USB</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>Importiere Einträge</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>Comma Separated (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Eintrag wird hinzugefügt</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>Jeder Eintrag muss folgende 4 Felder haben( durch Komma getrennt) : Quell-IP, Ziel-IP, PID, Name.  Zeile wird übersprungen</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>Fortsetzen</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>dies ist keine gültige Quell-IP-Adresse; Zeile wird übergangen</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>dies ist keine gültige Ziel-IP-Adresse; Zeile wird übergangen</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>dies ist keine gültige PID; Zeile wird übergangen</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>Ein Eintrag benötigt einen Namen (bis zu 16 Zeichen). Zeile wird übersprungen</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>Der Name eines Eintrages darf nicht mehr als 16 Zeichen haben. Zeile wird übersprungen</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>Ein Eintrag mit gleicher, Quell-, Zieladresse und PID existiert bereits unter einem anderen Namen</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>Existierenden Eintrag überschreiben oder Import dieses Eintrags übergehen </translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Überspringen</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>Überschreiben</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>Einer der Eintröge hat bereits eine PID. Einträge mit einer PID werden nur in MPTS Applikationen verwendet</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>Einträge auf USB exportieren</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>Einträge exportieren</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>IPTV_ Adressbuch</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>wird bereits verwendet. Bitte verwenden sie einen neunen Namen oder efitieren die den existierenden Eintrag</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>Eintrag mit diesen Werten existiert bereits</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>Sind sie sicher das sie alle Einträge löschen möchten?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>Quellen-IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>Ziel-IP</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Eintrag wird hinzugefügt</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>Ein Eintrag mit gleichem Namen existiert bereits</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>Was möchetn sie tun ?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Datum: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Zeit: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Stunde</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Tag</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zu Ereignissen anzuzeigen.</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Ein</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Code</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Kanal</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Pfad</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zu K1/K2-Bytes anzuzeigen.</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Rahmen</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binär</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zur Einweg-Verzögerung anzuzeigen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>Verlust der Quelle</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>B1 Error</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>REI-L Fhlr</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>MS-REI Fhlr</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>B2 Error</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>REI-P Fhlr</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>B2 Fehler</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>HP-REI Fhlr</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>B3 Error</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>REI-V Fhlr</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>BIP-V Error</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>B3 Fehler</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>LP-REI Err</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>LP-BIP Err</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>STL FAS Fehl</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Bit/TSE Fhlr</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>Signal-Verlust</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Rahmensync-Verlust</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Rahmenwortfehler</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>FAS-Fehler</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dauer (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Startzeit</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Stoppzeit</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Längster</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Kürzester</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Letzter</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Durchschnitt</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Unterbrechungen</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zu Diensttrennung anzuzeigen.</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Gesamt</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Überlauf</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD Nr.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dauer (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zu Diensttrennung anzuzeigen.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Fehler</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Erfolg</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Überlauf</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>START</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>Stopp</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>SD Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dauer (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Überlauf</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zu Anrufergebnissen anzuzeigen.</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>Wählton</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  WAHR</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Typ</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Dauer</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Ungültiger</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Datum: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Zeit: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Stunde</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Tag</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Prog.-Name</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Ereignis</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>Strom-IP:Port</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Stromname</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dauer/Wert</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>Exportiere gespeicherte kundenspezifische Resultatskategorie...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Gespeicherte kundenspezifische Resultatskategorie...</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportieren</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Kann USB Flash-Laufwerk nicht finden.&#xA;Bitte neues Flash-Laufwerk einstecken oder dieses entfernen und wieder einstecken.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>PTP-Daten an USB-Gerät exportieren</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>PTP Datendateien (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>Bericht auf USB exportieren</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Text (*.txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>Bildschirmaufnahmen an USB-Gerät exportieren</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>Gespeicherte Bildschirmaufnahmen (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>Ausgewählte Dateien komprimieren unter:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Dateinamen eingeben: max. 60 Zeichen</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Komprimieren&#xA;&amp;&amp; exportieren</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Exportieren</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Geben Sie einen Namen für die Zip-Datei ein.</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Kann USB Flash-Laufwerk nicht finden.&#xA;Bitte neues Flash-Laufwerk einstecken oder dieses entfernen und wieder einstecken.</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>Beim Komprimieren der Dateien ist ein Fehler aufgetreten.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>TIE-Daten an USB-Gerät exportieren</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>Wander TIE Datendateien (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>Timing-Daten auf USB exportieren</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>Alle Dateien mit Timing-Daten (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>Dateityp:</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Öffnen</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>Angepasste Ergebniskategorie von USB-Gerät importieren</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Gespeicherte kundenspezifische Resultatskategorie...</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>Importiere &#xA;kundenspezifische Kategorie</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>Logo Import von USB</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Bilddateien (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>Logo&#xA;importieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>Kurzreferenz von USB-Gerät importieren</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>PDF-Dateien (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>Kurzreferenz&#xA;importieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>Test&#xA;importieren</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Dekomprimieren&#xA; &amp;&amp; importieren</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>Fehler - Es ist nicht möglich, bei der Datei Nicht-TAR durchzuführen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>Nicht genug freier Speicherplatz auf der Zieleinheit.&#xA;Kopiervorgang abgebrochen.</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>Kopiere Dateien ...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>Abgeschlossen. Datei wurde kopiert.</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>Fehler: Die folgenden Punkte konnten nicht kopiert werden: &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>PTP Daten speichern</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>PTP Dateien (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Kopieren</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>Ungenügneder Festplattenspeicher. Löschen Sie andere gespeicherte Dateien oder exporiteren Sie zu USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%1 %2 freier</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>Keine Dateien ausgewählt</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>%1 in der ausgewählten Datei</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>Insgesamt %1 in %2 ausgewählten Dateien</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Dateiname:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Dateinamen eingeben: max. 60 Zeichen</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>Alle Dateien in diesem Ordner löschen?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Schreibgeschützte Dateien werden nicht gelöscht.)</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>Dateien werden gelöscht...</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Folgendes wirklich löschen&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>Soll dieser Eintrag wirklich gelöscht werden?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>Keine empfohlene Optik</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Fibre Channel</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137.6M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/A&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>Zeigt die Resultate der Signalstrukturermittlung und des Scans an.</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>Sortieren nach:</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>Sortieren</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>Erneutes Sortieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>Zeigt eine Auswahl vorhandener Test an, die benutzt werden können, um den ausgewählten Kanal zu analysieren.</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Test starten</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>Bitte warten...ausgewählter Kanal wird konfiguriert....</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>Details anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>Details ausblenden</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>Bereich: %1 bis %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Keine</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>Suchen</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>Original</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>An Breite anpassen</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>An Höhe anpassen</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>Auswahl Dualprüfungsansicht</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>Weiteres...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(Forts.)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Warnung</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>Protokollpuffer ist voll</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>Aufzeichnung gestoppt</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>Zeile bearbeiten</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>Byte auswählen</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>Screenshot kann nicht erstellt werden</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>nicht genügend Festplattenspeicherplatz</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>Screenshot erstellt: </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>Informationen zum Datenstrom</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>DP wählen</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>MF wählen</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>DTMF wählen</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reserviert</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Einseitig</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Inaktiv</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>Extra Traffic</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>Gesamt</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>Navigieren zu externen Verbindungen nicht möglich</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>Nicht gefunden</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>Seitenende erreicht, Fortsetzung am Anfang</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>Tool auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Automatische Berichterstellung ausschalten, bevor Skript gestarted wird.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Zuvor ausgeschaltete automatische Berichterstellung einschalten.</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Erstellen</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Default</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Senden</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>Wiederholen</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Ansicht</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS (Senke)</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS (Quelle)</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>Container</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Kanal</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>Signal-Label</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Trace</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>Trace Format</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Unbekannt</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>Dies stellt nur den momentanen Pegel dar, wobei Kanäle niedrigerer oder höherer Ordnung nicht berücksichtigt werden. Nur der zurzeit ausgewählte Kanal wird Live-Updates erhalten.</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>Status des Kanals, durch ein Bildsymbol dargestellt.</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>Keine Überwachungsalarme vorliegend</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>Alarme vorliegend</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>Überwachung, während keine Überwachungsalarme vorliegen</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>Überwachung, während Alarme vorliegen</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>Der Name des Kanalcontainers. Ein 'c'-Suffix weist darauf hin, dass der Kanal verkettet wurde.</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>Die N KLM-Nummer des Kanals ist durch RFC 4606 spezifiziert</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>Die Signalbeschriftung des Kanals.</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>Letzter bekannter Status des Kanals.</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>Der Kanal ist ungültig.</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI vorhanden</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>AIS vorhanden</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP vorhanden</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>Überwachung</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Nein</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>Status aktualisiert: </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>noch nie</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>Die Nachverfolgungsinfo des Kanals</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>Die Nachverfolgungsformatinfo des Kanals</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>nicht unterstützt</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Scannen</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>Alarm</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Ungültiger</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>Unformatiert</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>Einzelnes Byte</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>Terminiert mit CR/LF</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>Gepaarte Geräte</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>Entdeckte Geräte</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>CA Zert</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>Mandanten Zert</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>Mandantenschlüssel</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Format</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Freier Speicherplatz</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Gesamtkapazität</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Anbieter</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Beschriftung</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Version des Upgrades</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Installierte Version</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>Kategorien</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>Liste der Namen, die den kundenspezifischen Kategorien gegeben sind. Klicken auf einen Namen aktiviert/deaktiviert die kundenspezifische Kategorie.</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>Ermöglicht das Konfigurieren einer kundenspezifischen Kategorie durch Anklicken.</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>Ermöglicht das Löschen einer kundenspezifischen Kategorie durch Umschalten der Kategorien, die gelöscht werden sollen.</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>Name, der der kundenspezifischen Kategorien gegeben ist. Klicken auf den Namen aktiviert/deaktiviert diese kundenspezifische Kategorie.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>Klicken auf das Konfigurationssymbol startet einen Konfigurationsdialog für die kundenspezifische Kategorie.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>Klicken auf das Löschsymbol markiert die kundenspezifische Kategorie zum Löschen bzw. hebt die Markierung wieder auf.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Bericht erstellen</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Text (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Erstellen</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Siehe Bericht nach Fertigstellung.</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Fehler - Dateiname kann nicht leer sein.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>Werden die Einstellung geändert, wird die Anzeige aktualisiert.</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS-Name</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS-Name</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Nicht in Subnetz</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>Auf Leitungsaktivität warten...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>Auf DHCP warten...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>Quell-IP wird rekonfiguriert...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modus</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>Quellen-IP</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS-Name</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS-Name</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>Systemname</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>Nicht in Subnetz</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Nicht in Subnetz</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS-Name</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS-Name</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Dienste</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Nicht in Subnetz</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC Adresse</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Dienste</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>DNS-Name</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Entdeckung</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>VLAN Priorität</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>GERÄTE</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>Netzwerk-IP</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>GERÄTE</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>NetBIOS-Name</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>GERÄTE</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>IP-Netzwerke</translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>Domänen</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>Server</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>Hosts</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>Switches</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>Router</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>Infrastruktur</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>Kern</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>Verteilung</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>Zugriff</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Entdeckung</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>Erkannte IP-Netzwerke</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>Erkannte NetBIOS-Domänen</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>Erkannte VLANs</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>Erkannte Router</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>Erkannte Switches</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>Erkannte Hosts</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>Erkannte Server</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>Netzwerkerkennungsbericht</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Bericht konnte nicht erstellt werden</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>nicht genügend Festplattenspeicherplatz</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Generiert von Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Generiert von Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Generiert von Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Erstellt von dem Viavi-Prüfgerät </translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Start</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>Allgemein</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Datum</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Zeit</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>Die Platte ist voll; alle Grafiken wurden gestoppt.&#xA;Bitte Speicherplatz freigeben und Test neu starten.&#xA;&#xA;Alternativ können Grafiken über Werkzeuge->Anpassen in&#xA;der Menüleiste deaktiviert werden.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>Antippen, um Zeitskala zu zentrieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>Grafikmerkmale</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>Mittl.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max.</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Fenster</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Fenstersättigung</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>Grafikdaten speichern</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>Nicht genügend freier Speicher auf dem Zielgerät.</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>Der Export kann direkt an USB erfolgen, wenn eine Flash-Einheit angeschlossen ist.</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>Diagrammdaten werden gespeichert. Dies kann einige Zeit dauern, bitte warten Sie...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>Diagrammdaten werden gespeichert</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>Nicht genügend freier Speicher auf dem Gerät. Die exportierten Diagrammdaten sind unvollständig.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>Ein Fehler ist beim Export der Grafikdaten aufgetreten. Die Daten können unvollständig sein.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>Masstab</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 Tag</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 Stunden</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 Stunde</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 Minuten</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 Minute</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 Sekunde</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Daten werden gezeichenet</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>Zum Zoomen antippen und ziehen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>Sättigung</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Fenster</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Verb.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>Nr.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Zeit (s)</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src IP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>Dest IP</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Nachricht</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Src-Port</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>Ziel-Port</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Löschen...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>Bestätigen...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>Die ausgewählten Kategorien werden aus allen zurzeit laufenden Tests entfernt.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>Möchten Sie wirklich die ausgewählten Elemente löschen?</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>Neu...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>Öffnet einen Dialog zum Konfigurieren einer neuen kundenspezifischen Resultatskategorie</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>Wenn sie diese Taste drücken, können Sie alle kundenspezifischen Kategorien, die aus der Einheit gelöscht werden sollen, markieren. Drücken Sie die Taste erneut, wenn Sie mit der Auswahl der zu löschenden Dateien fertig sind.</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>Drücken Sie bitte&#xA;"%1"&#xA;um zu beginnen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>Configure Custom Results Category</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>Mit einem '*' gekennzeichnete Ergebnisse gelten nicht für die aktuelle Prüfkonfiguration und werden nicht im Ergebnisfenster angezeigt.</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>Kategoriename:</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>Bitte geben Sie einen kundenspezifischen Kategorienamen ein: max %1 Zeichen</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Dateiname:</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>Speichern als</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>Neu speichern</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>Die Datei %1, welche die &#xA;Kategorie "%2" enthält,</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>Ausgewählte Resultate: </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   (Max. Auswahlmöglichkeiten </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Konfigurieren...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Übersicht</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>Übersicht</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>Max (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>Wert (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Hilfeschaltfläche Start...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Hilfeschaltfläche Zurück...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Hilfeschaltfläche Weiter...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Hilfeschaltfläche Ende...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>Kein Anrufverlauf</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Hilfeschaltfläche Start...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Hilfeschaltfläche Zurück...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Hilfeschaltfläche Weiter...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Hilfeschaltfläche Ende...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Peak-Peak</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos-Peak</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Peak</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Stunde</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Tag</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Stunde</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Tag</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Peak-Peak</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos-Peak</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Peak</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>Latenz (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Anzahl</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NICHT&#xA;VERFÜGBAR</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>Status: ERFOLGREICH</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>Status: FEHLGESCHLAGEN</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>Status: Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>Erweitern um Filterauswahl zu sehen</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation>Anzahl erkannter MEPs </translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>Als Peer setzen</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>Anzeige filtern</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>Filter ein</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>Filterwert eingeben: max. %1 Zeichen </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>CCM Typ</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>CCM Bitrate</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>ID des Peer-MEP </translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>Wartungsdomänenstufe</translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>Domänen-ID angeben</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>Wartungsdomänen-ID</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>Wartungsverband-ID</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>Testset konfiguriert. Die hervorgehobene Zeile wurde für dieses Testset als Peer-MEP festgelegt.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>Die Einstellung des Testsets als hervorgehobenen Peer-MEP ist fehlgeschlagen.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>Kann %1 nicht auf %2 setzen.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NICHT&#xA;VERFÜGBAR</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>GRAFIKERSTELLUNG&#xA;DEAKTIVIERT</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>Ergebnisfenster umschalten, um Vollbildschirm anzuzeigen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>NICHT VERFÜGBAR</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>Kundenspezifisch</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Alle</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 Byte Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 Byte Upstream Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 Byte Downstream Frame Verlust Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 Byte Zwischenspeicher Kreditdurchlauf Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 Byte Upstream Zwischenspeicher Kreditdurchlauf Testergebnisse</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 Byte Downstream Zwischenspeicher Kreditdurchlauf Testergebnisse</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>Textdatei wird exportiert...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>Protokoll exportiert nach</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>Datenstrom-&#xA;details</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>Alle Ergebnisbäume in diesem Fenster zuklappen.</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>Alle Ergebnisbäume in diesem Fenster aufklappen.</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>RESULTATE-&#xA;KATEGORIE&#xA;IST LEER</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NICHT&#xA;VERFÜGBAR</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>NICHT&#xA;VERFÜGBAR</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>ALLE&#xA;ERGEBNISSE&#xA;OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>Spalten</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>Spalten anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Kein auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>Nur Fehlerhafte anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Spalten...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>Nur &#xA;fehler&#xA; Zeilen anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>Nur fehlerhafte Zeilen anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>Raten des Datenaufkommens (L1, kbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>Raten des Datenaufkommens (L1 Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation>Anz. analys. Ströme</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>Gruppiert nach</translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>Gesamtlast</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>Last Ströme 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>Last Ströme >128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Spalten...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>Hop</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>Verzögerung (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP-Adresse</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>Verwenden Sie den Menübefehl "Ansicht->Ergebnisfenster->Einzeln", um weitere Daten zu Traceroute anzuzeigen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>Ideale Übertragungszeit</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>Tatsächliche Übertragungszeit</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>Gruppe:</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Strom</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>Programme</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>Paketverlust</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Paket-Jitter</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Abstandsfehler</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Periodenfehl.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Sync-Byte-Fehl.</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>Nur fehlerhafte Programme anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Spalten...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>Gesamt Prog. Mbit/s</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>Nur fehl&#xA;Prog anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>Nur fehlerhafte Programme anzeigen</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>n.v.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>Nur fehl&#xA;Strm anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>Nur fehlerhafte Datenströme anzeigen</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analysieren</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Name</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation># Analysierte&#xA;Datenströme</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>Gesamt&#xA;L1 (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>IP-Prüfsummen&#xA;fehler</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>UDP-Prüfsummen&#xA;fehler</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>Analyzer&#xA;starten</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Spalten...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>Bitte warten..Analyzer-Anwendung wird gestartet...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Stunde</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Tag</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Sek.</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Stunde</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Tag</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>Skript auswählen.. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>Skript:</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>Status:</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>Gegenwärtige Zustand:</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>Stoppuhr:</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>Skript abgelaufene Zeit:</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>Stopuhreinstellung</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>Ausgang:</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>Script beendet in:  </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>Skript&#xA;auswählen</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Skript ausführen</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>Ausgabe&#xA;löschen</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>Stoppe Script</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>LÄUFT...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>Skript abgelaufene Zeit:</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>Bitte ein anderes Script auswählen.. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>Fehler.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>LÄUFT.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>Läuft..</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>Testliste anpassen</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>Beim Start Ergebnisse anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>Nach oben</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>Nach unten</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Umbenennen</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>Separator</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>Verknüpfung hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>Gespeicherten Test hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>Alle Favoriten löschen?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>Die Favoritenliste ist die Standardeinstellung.</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>Alle benutzerdefinierten Favoriten werden gelöscht und die Liste dieser Einheit wird auf die Standardwerte zurückgesetzt. Wollen Sie fortfahren?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>Testkonfigurierungen (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>Dual Testkonfigurierungen (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>Gespeicherten Test wählen</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>Bitte warten...&#xA;Starte leere Testansicht</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>Test anheften</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>Angehefteten Test umbenennen</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>An Testsliste anheften</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>Testkonfiguration speichern</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Name des Tests:</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>Anzuzeigenden Namen eingeben</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>Dieser Test ist mit %1 identisch</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>Dies ist eine Verknüpfung, um eine Testanwendung zu starten.</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>Beschreibung: %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>Dies ist eine gespeicherte Testkonfigurierung.</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>Dateiname: %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>Ersetzen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>Test konnte nicht gestartet werden...&#xA;Die Optionen sind abgelaufen.&#xA;Beenden Sie BERT über die Seite 'System' und starten Sie den Test erneut.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>Dieser Test kann im Moment nicht gestartet werden. Möglicherweise wird er von der aktuellen Hardwarekonfiguration nicht unterstützt oder erfordert mehr Ressourcen.</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>Versuchen Sie, auf anderen Registerkarten ausgeführte Tests zu entfernen.</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>Dieser Test wird an einem anderen Port ausgeführt. Wollen Sie zu diesem Test wechseln? (Auf Registerkarte %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>Ein anderer Test (auf Registerkarte %1) kann für den ausgewählten Test rekonfiguriert werden. Wollen Sie den Test rekonfigurieren?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>Liste ist leer.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>Der Test kann nicht gestartet werden...&#xA;Optische Jitterfunktion OFF (AUS).&#xA;Optische Jitterfunktion von der Home/System Seite starten.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>Test kann nicht hinzugefügt werden...&#xA;Die Performanz des Testmoduls MSAM ist dafür zu gering.&#xA;Geladenen Test entfernen oder ändern.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>Test kann nicht hinzugefügt werden...&#xA;Unzureichende Stromversorgung.&#xA;Bitte anderes Modul inaktivieren oder&#xA;geladenen Test entfernen oder ändern.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>Bitte warten...&#xA;Test wird hinzugefügt </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>Bitte warten...&#xA;Test wird konfiguriert </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>Test kann nicht hinzugefügt werden...&#xA;Erforderliche Ressourcen sind evtl. nicht verfügbar.&#xA;&#xA;Bitte technischen Support informieren.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>UI-Objekte werden konstruiert</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>UI-Synchronisierung mit Anwendungsmodul</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>UI-Ansichten werden initialisiert</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Zurück</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Test&#xA;auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Copyright Viavi Solutions</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>Geräteinformationen</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>Optionen</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>Gespeicherte Datei</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>Zugriffsmodus für Benutzeroberfläche</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>Fernsteuerung wird zurzeit verwendet.&#xA;&#xA;Im schreibgeschützten Zugangsmodus können Benutzer keine Einstellungen ändern, &#xA;die die Fernsteuerung beeinträchtigen könnten.</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>Zugriffsmodus</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>Das MSAM Modul wurde aufgrund einer PIM&#xA;Konfiguartionsänderung neu gestartet.</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>Ein PIM wurde eingesteckt oder herausgenommen. Wollen Sie weitere Änderung&#xA;vornehmen tun sie das bitte jetzt.  Das MSAM Modul wird nun neu gestartet.&#xA;Dies kann bis zu 2 Minuten dauern. Bitte warten Sie...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>Das BERT Modul wird zu heiß und wird automatisch&#xA;heruntergefahren, wenn die interne Temperatur weiter&#xA;steigt. Bitte speichern Sie Ihre Daten, fahren das BERT&#xA;Modul herunter und rufen Sie den technischen Support.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>BERT Module wurde aufgrund Überhitzung gezwungen abzuschalten.</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>XFP PIM im falschen Steckplatz. Bitte XFP PIM an Anschluss #1 verschieben.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>Das BERT-Modul XFP PIM befindet sich im falschen Steckplatz. &#xA;Bitte verschieben Sie XFP PIM zu Anschluss #1.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>Sie haben einen Test an einem elektrischen Interface gewählt&#xA; es scheint allerdings ein optisches SFP gesteckt zu sein.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>Bitte stellen Sie sicher dass Sie ein elektrisches SFP benutzen.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>Das BERT-Modul hat einen möglichen Fehler in Anwendung %1 erkannt. Sie haben eine elektrischen Test ausgewählt, bei dem SFP handelt es sich jedoch wahrscheinlich um ein optisches SFP.  Bitte ersetzen Sie das SFP oder wählen Sie ein anderes aus.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>Sie haben einen Test an einem optisches Interface gewählt&#xA; es scheint allerdings ein elektrischen SFP gesteckt zu sein.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>Bitte stellen Sie sicher dass Sie ein optisches SFP benutzen.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>Das Prüfgerät hat einen möglichen Fehler in Anwendung %1 erkannt.&#xA;&#xA;Sie haben eine optische Prüfung ausgewählt, aber das SFP-&#xA;Modul scheint ein elektrisches zu sein. Wechseln Sie es aus oder wählen Sie ein anderes SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>Sie haben eine 10G-Prüfung ausgewählt, bei dem eingelegten Empfänger handelt es sich jedoch wahrscheinlich um kein SFP+. </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>Stellen Sie sicher, dass ein SFP+ verwendet wird.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>Das Prüfgerät hat einen möglichen Fehler erkannt. Für diese Prüfung wird ein SFP+-Modul benötigt, aber der Transceiver wurde nicht als SFP+ erkannt. Tauschen Sie das Modul gegen ein SFP+ aus.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>Automatische Berichte - Einstellungen</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>Datei überschreiben</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoReport</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Achtung: Das ausgewählte Laufwerk ist voll. Es kann entweder manuell Platz geschaffen oder die &#xA;letzten 5 Berichte können automatisch gelöscht werden.</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>Automatische Berichte aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>Berichtszeitraum</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>Min.:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>Max.:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>Test nach Berichterstellung neu starten</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modus</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>Separate Datei erstellen</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>Berichtsname</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>Erstellungsdatum/-zeit automatisch an Name angehängt</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format:</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>Text</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>Die automatische Berichterstellung wird im schreibgeschützten Zugriffsmodus nicht unterstützt.</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>Die automatischen Berichte werden auf der Festplatte gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>Für automatische Berichte ist eine Festplatte erforderlich. In diesem Gerät scheint jedoch keine installiert zu sein.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>Automatische Berichte können bei laufendem automatischem Skript nicht aktiviert werden.</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>Automatischer Bericht wird erstellt</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>Vorbereitung läuft...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>Erstellung wird ausgeführt </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>Vorheriger Bericht wird gelöscht...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>Fertig.</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** UNZUREICHENDE STROMVERSORGUNG. BITTE ANDERES MODUL DEAKTIVIEREN ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>Serielle Verbindung erfolgreich</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>Applikation überprüft Verfügbarkeit von Upgrades</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>Applikation bereit für Kommunikation</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** FEHLER BEIM KERNEL-UPGRADE *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** Ethernet-Sicherheit Einstellung = Standard einstellen und/oder BERT-Software erneut installieren *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>*** FEHLER BEIM UPGRADE DER ANWENDUNG. Installieren Sie die Modulsoftware erneut ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>**** UPGRADE FEHLGESCHLAGEN. UNZUREICHENDE STROMVERSORGUNG ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** Fehler beim Startvorgang: Schalten Sie das BERT Modul aus und wieder ein ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>Ansicht</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Berichte</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>Werkzeuge</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>Bericht erstellen...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>Automatischer Bericht...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Test starten</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>Test stoppen</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Individualisieren...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>Zugriffsmodus...</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Test auf Standardeinstellungen zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>Historie löschen</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>Skripts ausführen...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>VT100 Emulation</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Modem Konfiguration...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>Standardlayout wiederherstellen</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>Resultatfenster</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Ein</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>Links/Rechts teilen</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>Oben/Unten teilen</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>2 x 2 Grid</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>unten zusammenfassen</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>links zusammenfassen</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>Nur Ergebnisse anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>Teststatus</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LEDs</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>Konfigurationsanzeige</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Aktionsanzeige</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>Skript auswählen</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>Skriptdateien (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>Skript&#xA;auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Signalverbindungen</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Bericht erstellen</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Text (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>Inhalt&#xA;auswählen</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Erstellen</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Format:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Siehe Bericht nach Fertigstellung.</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Fehler - Dateiname kann nicht leer sein.</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>Wählen Sie Inhalt für</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Format</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dateiname</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Auswählen...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Siehe Bericht nach Fertigstellung.</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>Mitteilungslog einschließen</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>Bericht&#xA;erstellen</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>Bericht&#xA;ansehen</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Text (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Fehler - Dateiname kann nicht leer sein.</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>Bericht gespeichert</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>Bitte das Signal dämpfen.</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>Ereignisprotokoll und Histogramm sind voll.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>Die K1/K2-Protokolle sind voll.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>Das Protokoll ist voll.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>Der Test wird fortgesetzt, ohne derartige Aktionen&#xA;weiter zu protokollieren. Ein Neustart des Tests&#xA;löscht alle Protokolle und Histogramme.</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>Opt. Empf. &#xA;zurücksetzen</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>Test beenden</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Empfängerüberlast</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>Protokoll ist voll</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Achtung</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Kein Test aktiv</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>Kundenspezifische Benutzeroberfläche</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>Test speichern</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>Dualen Test speichern</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>Test laden</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>Alle Dateien (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>Gespeicherte Tests (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>Gespeicherte duale Tests (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>Setup laden</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>Dualen Test laden</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>Gespeicherten Test von USB importieren</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>Alle Dateien (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>Gespeicherte klassische RFC Testkonfigurationen (*.classic_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>Gespeicherte RFC-Prüfungskonfigurationen (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>FC Test Konfigurierungen (*.fc_test) gespeichert</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>Gespeicherte TrueSpeed-Konfigurationen (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>Gespeicherte Realgeschwindigkeit VNF Konfiguration (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>Gespeicherte SAMComplete-Konfigurationen (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>SAMComplete-Konfigurationen wurden gespeichert (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>Konfiguration für OTN-Check gespeichert (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>Gespeicherte Optics Selbsttest Konfigurations (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>Gespeicherte Konfigurationen der CPRI-Überprüfung (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>Gespeicherte Konfigurationen der PTP-Überprüfung (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>Gespeicherte Zip-Dateien (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>Gespeicherten Test auf USB exportieren</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>Alle Dateien (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>Vom Benutzer gespeicherte Mehrfachprüfungen</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>Gespeicherte Benutzertests</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>Fernsteuerung wird zurzeit verwendet.&#xA;&#xA;Diese Operation ist im schreibgeschützten Zugangsmodus nicht verfügbar.&#xA; Wählen Sie die Option "Extras"->"Zugangsmodus", um den Vollzugriff zu aktivieren.</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>Optionen für das BERT-Modul sind abgelaufen.&#xA;Bitte beenden Sie BERT über die Seite 'System' und starten Sie die Prüfung erneut.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>Ausblenden</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>Beide Tests neu starten</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Neu&#xA;starten</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>Werkzeuge:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>Aktionen</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>Maximiertes Ergebnisfenster für Prüfung: </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>Gesamtansicht</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Test hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Betriebsanleitung</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Empfohlene Optiken</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Frequenzraster</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Was ist das?</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>Dualprüfungskonfiguration speichern unter...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>Dualprüfungskonfiguration laden...</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Ansicht</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>Prüfauswahl ändern...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>Gesamtansicht der Prüfung anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Berichte</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>Dualprüfbericht erstellen...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Bericht anzeigen...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Hilfe</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Warnung</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>Datei wird gespeichert...</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>Neuer Name:</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>Geben Sie den neuen Dateinamen ein.</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Umbenennen</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>Die Datei konnte nicht umbenannt werden, da bereits eine Datei mit dem Zielnamen vorhanden ist.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Neu&#xA;starten</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>Benutzerinformation ändern</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Keine ausgewählt...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Max Zeichenzahl: </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Logo auswählen...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Vorschau nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Hilfediagramme</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Betriebsanleitung</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Konfig</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Zurück</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Pos1</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>Vorwärts</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>QuickLaunch</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** FEHLER BEIM JITTER-UPGRADE. Installieren Sie die Modulsoftware erneut ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** FEHLER IM JITTER-OPTIK-MODUL. STROMVERSORGUNG UNZUREICHEND. ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>Modul für optischen Jitter läuft</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1-Profile (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>Profile auswählen</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Alle auswählen</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>Alle deselektieren</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>Anmerkung: Das Laden des "Connect"-Profils verbindet den Kommunikationskanal mit der Fernbedienung, wenn notwendig</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>Inkompatibles Profil</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>Profile&#xA;laden</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Wollen Sie %1 wirklich löschen?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Sind Sie sicher, dass Sie alle %1 Profile löschen möchten?&#xA;&#xA;Dieser Vorgang kann nicht rückgängig gemacht werden.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Schreibgeschützte Dateien werden nicht gelöscht.)</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>Das Laden der Profile aus der folgenden Quelle ist fehlgeschlagen:</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>Profile wurden geladen aus:</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>Manche Konfigurationen wurden nicht korrekt geladen, weil sie in dieser Anwendung nicht gefunden wurden.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>Profile wurden erfolgreich geladen aus:</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>Dualprüfung aktivieren</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>Applikationen werden markiert</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>Überprüfung der Optionen</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>Für die installierte Hardware bzw. die optionalen Komponenten sind keine Prüfungen verfügbar.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>Kann keine Tests starten. Prüfen Sie die installierten Optionen und die aktuelle Hardwarekonfiguration.</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>Widerherstellung der zuletzt benutzen Applikation</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>Anwendung läuft</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>Es ist nicht möglich, einen internen USB-Flash-Speicher zu installieren.&#xA;&#xA;Stellen Sie bitte sicher, dass die Einheit bei der Batterie fest eingesteckt wird. Nach dem Einstecken führen Sie bitte einen Neustart des BERT Moduls aus.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>Das Dateisystem des internen USB-Flashs scheit beschädigt zu sein. Falls Sie Hilfe benötigen, wenden Sie sich bitte an den technischen Support-</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>Die interne USB-Flash-Kapazität ist kleiner als die empfohlene Größe von 1 G. &#xA;&#xA;Stellen Sie bitte sicher, dass die Einheit bei der Batterie fest eingesteckt wird. Nach dem Einstecken führen Sie bitte einen Neustart der Einheit aus.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>Beim Wiederherstellen der Einstellung ist ein Fehler aufgetreten.&#xA;Versuchen Sie es erneut.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>Bevor die gespeicherte Konfiguration geladen wird, werden die aktuellen Tests beendet.</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>Bitte warten...&#xA;Gespeicherte Prüfungen werden geladen</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>Testdokumentname oder Dateipfad ist ungültig.&#xA;Zum Lokalisieren des Dokuments "Gesp. Testkonfig. laden" verwenden.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>Bei der Wiederherstellung einer Prüfung ist ein Fehler aufgetreten.&#xA;Wiederholen Sie den Vorgang.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>Ladevorgang der Batterie aktiviert </translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>Hinweis: Ladevorgang der Batterie ist in diesem Modus nicht möglich. Ladevorgang der Batterie wird automatisch aktiviert wenn ein Test geladen wird der dies ermöglicht </translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>Dieser Modul wird momentan ferngesteuert und&#xA;daher wurde die lokale Anzeige deaktiviert</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>Meldungen wurden protokolliert. Klicken...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Mitteilungslog für %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>Keine Mitteilungen</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 Mitteilung</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 Mitteilungen</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Mitteilungslog für %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Nachrichtenaufzeichnung</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Modem Konfiguration </translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>Bitte eine IP als Adresse dieses Servers und eine IP, die dem Einwahlclient zugewiesen werden soll, auswählen</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Server IP</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>Client IP</translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>Aktueller Standort (Ländercode)</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>Modem nicht auffindbar. Bitte korrekten Anschluss des Modems sicherstellen und erneut versuchen.</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>Gerät ist besetzt. Alle Einwahlsitzungen trennen und erneut versuchen.</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>Modem kann nicht aktualisiert werden. Bitte alle Einwahlsitzungen trennen und erneut versuchen.</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>Test(s) werden aufgrund von Zeitänderung erneut gestartet.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Geben Sie den Schlüssel der neuen Option ein, um diese zu installieren.</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Optionsschlüssel</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Installieren</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Import</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Softwareoptionen können über Viavi erworben werden.</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>Option Challenge-ID: </translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>Schlüssel akzeptiert! Bitte neu booten, um die neue Option zu aktivieren.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Schlüssel zurückgewiesen - Plätze für Ablaufoptionen sind vollständig belegt.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Schlüssel zurückgewiesen - Ablaufoptionen war bereits installiert.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Ungültiger Schlüssel - Bitte erneut versuchen.</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%1 von %2 Schlüssel(n) akzeptiert! Ein Neustart ist erforderlich, damit die neue(n) Option(en) wirksam werden.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>%1' kann nicht auf einem UBS-Flashlaufwerk geöffnet werden.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>Problem beim Abrufen der Optioneninformationen...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Ausgewähltes Byte</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Voreinstellung</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Legende</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>Legende:</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI (nicht verwendet)</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Reserviert</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>Informationen zum BERT Modul</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>Transportmodul</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Eine Kurzreferenz importieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Eine Kurzreferenz importieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation>Menü ausblenden</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> Alle Tests</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Anpassen</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>Mindestens einer der ausgewählten Screenshots wurde vor dem Start des aktuellen &#xA;Tests erstellt.  Stellen Sie sicher, dass die richtige Datei ausgewählt ist.</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Alle&#xA;auswählen</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>Alle&#xA;deselektieren</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>Screenshots&#xA;auswählen</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>Screenshots&#xA;deselektieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>RS-FEC-Kalibrierung</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Kalibrieren</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Kalibriere...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>Die Kalibrierung ist nicht vollständig. Die Kalibrierung benötigt RS-FEC.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(Die Kalibrierung kann von der Registerkarte RS-FEC in den Setupseiten ausgeführt werden)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>Kalibrierung ernuet versuchen</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>Kalibrierung verlassen</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>Kalibrierung unvollständig</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC wird normalerweise mit SR4, PSM4, CWDM4 verwendet.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Führen Sie zur PS-FEC-Kalibrierung folgendes aus (gilt auch für CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Führen Sie einen QSFP28-Adapter in das CSAM ein</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Führen Sie einen QSFP28-Transreceiver in den Adapter ein</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Führen Sie ein Fiber-Loopback-Gerät in den Transceiver ein</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Klicken Sie auf Kalibrieren</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Status</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Resync</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Der Transreceiver muss nun mit dem Gerät unter Test (DUT) neu synchronisiert werden.</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Entfernen Sie das Fiber-Loopback-Gerät vom Transreceiver</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Bauen Sie eine Verbindung zum Gerät unter Test auf (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Schalten Sie den DUT-Laser AN</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Bestätigen Sie, dass das Signal vorhanden LED auf Ihrem CSAM grün leuchtet</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Klicken Sie auf Lane Resync</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>Resync fertiggestellt.  Der Dialog kann nun geschlossen werden.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Als schreibgeschützt&#xA;speichern</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>An Testliste anheften</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Dateiname</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Auswählen...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Als schreibgeschützt&#xA;speichern</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>Profile&#xA;speichern</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>%1-Profile (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>Profile </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation> ist eine schreibgeschützte Datei. Kann nicht ersetzt werden.</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>Profile gespeichert</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>Logo auswählen</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>Bilddateien (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>Datei ist zu groß. Bitte wählen Sie eine andere Datei.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Ausgewähltes Byte</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Voreinstellung</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Legende</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>Test&#xA;starten</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Test&#xA;stoppe </translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Läuft</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Gestoppt</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Verzögerung</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>Vollzugriff</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>Schreibgeschützt</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>Dunkel</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>Hell</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>Schnellstart</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>Ergebnisse anzeigen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Nicht verfügbar</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>Geschätzter TCP-Gesamtdurchsatz (Mbit/s) auf Basis von:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>Max. verfügbare Bandbreite (Mbit/s)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>Durchschnittliche Round-Trip-Verzögerung (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>Anzahl der parallelen TCP sessions die benötigt werden um den maximalen Durchsatz zu erreichen:</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Bitrate</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>Fenstergröße</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>Sitzungen</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>Geschätzter TCP-Gesamtdurchsatz (kbit/s) auf Basis von:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>Max. verfügbare Bandbreite (kbit/s)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Durchsatz</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>Beim Speichern der Testkonfiguration ist ein Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>Beim Laden der Testkonfiguration ist ein Fehler aufgetreten.</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>Das selektierte Speichermedieum ist voll.&#xA;Bitte einige Dateien entfernen und nochmals versuchen zu speichern.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>Folgende gespeicherte Dateien spezieller Resultatskategorien weichen von den zurzeit geladenen ab:</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... %1 andere</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>Ein Überschreiben kann sich auf andere Tests auswirken.</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>Überschreiben fortsetzen?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Nein</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>Bitte warten...&#xA;Stelle Setups wieder her...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>Nicht genügend Ressourcen, um Test %1 jetzt zu laden.&#xA;Menü "Test" enthält eine Liste der mit der aktuellen Konfiguration verfügbaren Tests.</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>Testeinstellungen konnten nicht alle wiederhergestellt werden.&#xA;Dateiinhalt ist möglicherweise alt oder beschädigt.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>Der Zugriffsmodus ist "Schreibgeschützt". Sie können dem Modus unter "Extras"->"Zugriffsmodus" ändern.</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Kein Test aktiv</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>Alle Einstellungen werden zurückgesetzt.&#xA;&#xA;Weiter?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>Der Test wird beendet und neu gestartet.&#xA;Testeinstellungen werden zurückgesetzt.&#xA;&#xA;Weiter?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>Dieser Ablauf läuft zur Zeit. Wollen Sie ihn beenden und einen neuen starten?&#xA;&#xA;Klicken Sie Cancel, um den vorhergehenden Ablauf weiterlaufen zu lassen.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>Wollen Sie diesen Prozess abbrechen?&#xA;&#xA;Klicken Sie Cancel(Abbrechen) um ihn laufen zu lassen.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>VT100 konnte nicht gestartet werden. Das serielle Gerät wird bereits verwendet.</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation> Modul </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>Achtung: Beim Drücken von "Neu starten" werden Ergebnisse an *beiden* Ports gelöscht.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Test&#xA;auswählen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>Test laden...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>Test speichern unter...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>Lade nur Setups...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Test hinzufügen</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>Test entfernen</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Hilfe</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Hilfediagramme</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Bericht anzeigen...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>Bericht exportieren...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>Benutzerinformation ändern...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>Berichtslogo importieren...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Von USB importieren</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>Gespeicherter Test...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>Benutzerdefinierte Kategorie wurde gespeichert ...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>Auf USB exportieren</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>Screenshot...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>Messe Daten...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>Optionen überprüfen/installieren...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>Bildschirmkopie</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Betriebsanleitung</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Empfohlene Optiken</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Frequenzraster</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Signalverbindungen</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Was ist das?</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>Kurzreferenzen</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>Bitte warten...&#xA;Test wird entfernt</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Port 1&#xA;ausgewählt</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Port 2&#xA;ausgewählt</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Keine ausgewählt...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>Datei auswählen...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>Paketaufzeichnung importieren von USB</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>Datei&#xA;auswählen</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Gespeicherte Paketaufzeichnung (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>Bericht anzeigen</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Alle Dateien (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Text (*.txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Protokoll (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Ansicht</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>Diesen Bericht&#xA;anzeigen</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>Andere Berichte&#xA;anzeigen</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>&#xA;Bericht umbenennen</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Auswählen</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> Existiert schon.&#xA;Ersetzen?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>Bericht umbenannt</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Fehler - Dateiname kann nicht leer sein.</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>Bericht wurde gespeichert unter</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>Es wurde kein Bericht gespeichert.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Gehe</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>TIE Daten speichern...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>Als Typ speichern: </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>HRD-Datei</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>CHRD Datei</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>Speichern </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>Dies kann einige Minuten in Anspruch nehmen ...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>Fehler: HRD-Datei konnte nicht geöffnet werden. Bitte speichern Sie erneut.</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>Wird beendet...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>TIE Daten gespeichert.</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>Fehler: Datei konnte nicht gespeichert werden. Bitte versuchen Sie es erneut.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Warnung</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Beim Schließen der Wander-Analyse gehen alle Analyseergebnisse verloren.&#xA;Zum Fortsetzen der Analyse auf "Analyse fortsetzen" klicken.</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>Analyse schließen</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>Analyse fortsetzen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Wander analyse</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>Aktualisieren&#xA;TIE-Daten</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>TIE-&#xA;Aktualisierung stoppen</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>Berechnen&#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>Stoppen&#xA;Berechnung</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>Screenshot&#xA;erstellen</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>Last&#xA;TIE Daten</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>TIE&#xA;Last anhalten</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>Analyse&#xA;schließen</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>Last TIE Daten</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Laden</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>Alle Wanderdateien(*.chrd *.hrd);;Hrd Dateien (*.hrd);;Chrd Dateien (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>Zweimal tippen, um das Rechteck zu definieren</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Alle löschen</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>Profil laden</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Wollen Sie %1 wirklich löschen?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Sind Sie sicher, dass Sie alle %1 Profile löschen möchten?&#xA;&#xA;Dieser Vorgang kann nicht rückgängig gemacht werden.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(Schreibgeschützte Dateien werden nicht gelöscht.)</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 Profile (*%2.%3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>Das Profil kann nicht geladen werden.</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>Laden fehlgeschlagen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Weiter</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Nachricht</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Fehler</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Gehe</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>Warnung</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>Welchen Schritt möchten Sie als nächstes durchführen?</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>Sind Sie sicher, dass Sie die Anwendung beenden möchten?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>Einrichtungen beim Beenden wiederherstellen</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>Zu Ergebnissen hin verlassen</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Verlassen</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Zurück</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>Schrittweise:</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Weiter</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>Anleitung</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Resultate</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Anderer Port</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Wiederholen</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>Gehe zu...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Anderer Port</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Test wird gestartet, Bitte warten...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Prüfung wird beendet. Bitte warten...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Nachrichtenaufzeichnung</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>Haupt</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>Schritte anzeigen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>Antwort: </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>Läuft</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Keine ausgewählt...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Berichtslogo</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Logo auswählen...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Vorschau nicht verfügbar</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Test wird gestartet, Bitte warten...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Prüfung wird beendet. Bitte warten...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>Test läuft...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>Restzeit:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Kein Test aktiv</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test unvollständig</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test abgeschlossen</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test abgebrochen</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Automatische Berichterstellung ausschalten, bevor Skript gestarted wird.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Zuvor ausgeschaltete automatische Berichterstellung einschalten.</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>Haupt</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>Bildschirmfoto gespeichert</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>Bildschirmfoto gespeichert:</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>Profilauswahl</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>Betriebsschicht</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>Gespeichertes Profil laden</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>Wie würden Sie TrueSAM gerne konfigurieren?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>Konfigurationen aus einem gespeicherten Profil laden</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>Gehe</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>Neues Profil starten</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>Auf welcher Schicht wird Ihr Service ausgeführt?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>Ebene 2: Test unter Anwendung des MAC adresses, z. B. 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>Ebene 3: Test unter Anwendung des IP adresses, z. B.  192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>Bitte warten...wechsele zum hervorgehobenen Schritt.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Test auswählen</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>Kommunikations-Installierungen</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>Konfigurieren Sie das Enhanced RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>Konfigurieren Sie das SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>Konfigurieren Sie das J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>Konfigurieren Sie das TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>Speichern der Konfiguration</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>Fügen Sie einen Info-Bericht hinzu</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation> Die ausgewählten Tests werden ausgeführt</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Bericht</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Bericht anzeigen</translation>
        </message>
    </context>
</TS>
