<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>Capacité d'application mobile</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>Bluetooth et WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>Uniquement WiFi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>Veuillez redémarrer l'équipement de test pour que les changement prennent effet </translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>Coller</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copier</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>Couper</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Supprimer</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Sélectionner</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Simple</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>Multiple</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tous</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucun</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Ouvrir</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Renommer</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>Nouveau dossier</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>Disque</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>Coller écrasera le fichier existant.</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>Écraser fichier</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>Nous ne pouvons pas ouvrir le fichier du à une erreur inconnue.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>Impossible d'ouvrir le fichier.</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>Le fichier sélectionné sera définitivement supprimé.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>Les %1 fichiers sélectionnés seront définitivement supprimés.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>Supprimer fichier</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>Veuillez entrer le nom du dossier :</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>Créer dossier</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>Impossible de créer le dossier "%1"</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>Le dossier "%1" existe déjà.</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>Créer dossier</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>Collage ...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>Suppression ...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>Terminé.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 a échoué.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>Échec dans le collage du fichier.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>Échec dans le collage de %1 fichiers.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>Pas d'espace libre suffisant.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>Échec dans la  suppression du fichier.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>Échec dans la suppression de %1 fichiers.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Retour</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Option</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>Mettre à niveau URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nom de fichier</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>Action</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>Module</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>Autre réseau sans fil</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>Entrez l'information du réseau sans fil ci-dessous.</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>Nom (SSID) :</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>Type de chiffrement :</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>Mise à niveau disponible</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync a déterminé qu'une mise à niveau est disponible.&#xA;Voulez-vous mettre à niveau maintenant ?&#xA;&#xA;La mise à niveau provoquera la fermeture de tous tests en cours. A la fin du processus l'ensemble de test va redémarrer pour terminer la mise à niveau.</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Une erreur inconnue a été reçue. Veuillez essayer à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>La mise à niveau a échoué</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>Information instrument :</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>Options de base:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>Réinitialisation de l'instrument&#xA;aux valeurs par défaut</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>Exporter les journaux ~ ~ pour clé usb</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>Copie de l'information&#xA;système au fichier</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>L'information système a été copiée&#xA;à ce fichier :</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>Ceci nécessite un redémarrage et va réinitialiser les réglages système et les réglages de test aux valeurs par défaut.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulation</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>Redémarrage et réinitialisation</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>Connexion exportation a réussi .</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>Impossible d' exporter des journaux . S'il vous plaît vérifier qu'une clé USB est insérée correctement et essayez à nouveau .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1 version %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Recommencer</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>Réinitialiser au défaut</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>Sélectionnez vos méthodes de mise à niveau</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>Mise à niveau à partir de fichiers stockés sur un lecteur flash USB.</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Réseau</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>Téléchargement de la mise à niveau à partir d'un serveur web sur le réseau.</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>Entrez l'adresse internet du serveur de mise à niveau :</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Adresse internet du serveur</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>Entrez l'adresse du serveur proxy, si l'accès au serveur de mise à niveau est nécessaire :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>Adresse proxy</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connecte</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>Interroger le serveur pour les mises à niveau disponibles.</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>Précédent</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Suivant</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>Démarrer la mise à niveau</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>Démarrer dégradation</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>Pas de mises à niveau trouvées</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>La mise à niveau a échoué</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>La mise à niveau terminera tous les test en cours. A la fin du processus l'équipement de test se réinitialisera pour compléter la mise à niveau.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>Confirmation de mise à niveau</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>Dégrader l'ensemble de test est possible mais non recommandé. SI vous avez besoin de faire cela on vous conseille d'appeler Viavi TAC.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>La dégradation terminera tout test en fonctionnement. A la fin du processus l'ensemble de test redémarrera lui-même pour compléter la dégradation.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>Dégradation non recommandée</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>Une erreur inconnue a été reçue. Veuillez essayer à nouveau.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>Système</translation>
        </message>
    </context>
</TS>
