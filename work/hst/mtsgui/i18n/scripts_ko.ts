<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>{1} 바이트 프레임 :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>{1} 바이트 프레임 :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>{1} 바이트 패킷 :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>{1} 바이트 패킷</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} 에러 : {2} 를 검색하려고 시도할 때 타임아웃이 발생하였습니다 . 연결을 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} 프레임 버스트 : 실패</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} 프레임 버스트 : 통과</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{2} 의 {1}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} 패킷 버스트 : 실패</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} 패킷 버스트 : 통과</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} 검색 중 {2} ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   이것을 교체하시겠습니까 ?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        다시 시도하기 위해 OK 를 누르세요</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{3} 을 위한 VLAN ID {2} {1} 테스팅 ...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} (us)</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} 대기 중 ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       새로운 설정을 생성하기 위해 이름을 변경할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>50 개 최대 사용처 ( 총 {1} 개 IP 대화 중 )</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>50 개 상위 TCP 재전송 대화 ( 총 {1} 개 대화 중 )</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>중단</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>테스트 취소</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>액티브</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>액티브 루프</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>액티브 루프가 성공적이지 않습니다 . VLAN ID 에 대한 테스트가 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>실제 테스트</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>범위 추가</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>설정된 프레임 손실 한계를 초과하는 프레임 손실률이</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>수동 테스트를 완료하거나 필요한 때는 언제든지 Expert RFC 2544 테스트 버튼을 클릭하여  RFC 2544 사용자 인터페이스로 돌아갈 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>하드웨어 루프가 발견되었습니다</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>하드웨어 루프가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>모든 테스트</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>루프백 애플리케이션은 호환 가능한 애플리케이션이 아닙니다 .</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>최대 처리량 측정은 없습니다</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>액티브 루프가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>분석</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>분석 중</translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>그리고</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>및 RFC 2544 테스트</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>LBM/LBR 루프가 발견되었습니다</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>LBM/LBR 루프가 발견되었습니다</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>영구적인 루프가 발견되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>진행 로그를 보고서의 끝에 추가함</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>애플리케이션 이름</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>대략적인 총 시간 :</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>이론적 FTP 처리량 값의 범위가 링크의 실제 측정된 값을 근거로 계산될 것입니다 .  측정된 링크 대역폭 , 왕복 지연 및 캡슐화를 입력하세요 .</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>응답 타임아웃이 발생하였습니다 .&#xA;{1} 초 내에 마지막 명령에 대한 응답이 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> 하드 루프가 있는 것으로 추정됩니다 .        </translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>비대칭</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>비대칭은 업스트림 모드에서는 근단에서 원단으로 전송하고 , 다운스트림 모드에서는 원단에서 근단으로 전송합니다 . 결합 모드는 테스트를 두 번 실행하여 , 연속적으로 로컬 셋업을 사용하여 업스트림 방향으로 전송하고 , 그 다음 원격 셋업을 사용하여 다운스트림 방향으로 전송합니다 . 현재의 로컬 셋업으로 원격 셋업을 덮어 쓰기 위해 버튼을 사용하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>시도 중</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>루프 업 시도 중</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>서버에 로그온하려고 시도하는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>루프 업 시도가 실패하였습니다 . 테스트가 중단됩니다</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>루프 업 시도가 성공적이지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>자동 협상</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>자동 협상 완료</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>자동 협상 설정</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>자동 협상 상태</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>사용 가능</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>평균</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>평균 버스트</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>평균 패킷 전송률</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>평균 패킷 크기</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>평균</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>평균 및 최대 평균 Pkt 지터 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>평균 지연 (RTD):</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>평균 지연 (RTD): N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>평균 패킷 지터 :</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>평균 패킷 지터 : N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>평균 Pkt 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>평균 속도</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>백투백</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>백투백 프레임 입도 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>백투백 프레임 입도</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>백투백 프레임 테스트</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>백투백 프레임 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>백투백 최대 시도 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>백투백 최대 시도 시간</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>백투백 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>요약으로 돌아가기</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$ballon::msg</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>대역폭 입도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>대역폭 입도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>대역폭 측정 정확도 :</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>대역폭 측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>기본 로드 테스트</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>범위의 시작 :</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation> 비트</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>둘 다</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx 와 Tx 모두</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>로컬 및 원격 소스 IP 주소 모두 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>상향식</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>버퍼</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>버퍼 크레딧 &#xA;( 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>버퍼 크레딧 시도 지속 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>버퍼 크레딧 시도 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>버퍼 크레딧 테스트</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>버퍼 크레딧 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>버퍼 크레딧 처리량</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>버퍼 크레딧 처리량 {1} 바이트 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>버퍼 크레딧 처리량 &#xA;( 버퍼 크레딧 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>버퍼 크레딧 처리량 테스트</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>버퍼 크레딧 처리량 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>버퍼 크기</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>버스트</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>버스트 입도 ( 프레임 )</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>시간에 따른 TCP 재전송 대 네트워크 이용률을 확인하여 성능이 떨어지는 네트워크를 폭주와 같은 손실이 많은 네트워크 조건과 서로 관련시킬 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>IP 대화 표를 검토하여 바이트나 프레임으로 최대 사용처를 파악할 수 있습니다 .  S &lt;- D 와 S -> D 명칭은 바이트와 프레임의 소스 to 목적지와 목적지 to 소스 트래픽 방향을 의미합니다 .</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation> 바이트</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>바이트 &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>바이트 S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>계산된 &#xA; 프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>계산된 &#xA; 패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>계산 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>진행할 수 없습니다 !</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>분석 요약 캡쳐</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>캡쳐 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>스크린 &#xA; 캡쳐</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>주의 !&#xA;&#xA; 이 설정을 완전히 &#xA; 삭제하시겠습니까 ?&#xA;{1}...</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>Cfg</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>Cfg 속도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>Cfg 율</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>Cfg 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>섀시 ID</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>필터 소스 셋업을 설정하기 위해 체크된 Rx 아이템이 사용될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>Tx 목적지 셋업을 설정하기 위해 체크된 Tx 아이템이 사용될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>액티브 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>하드웨어 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>액티브 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>LBM/LBR 루프 확인 중 .</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>영구적인 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>반이중 포트 탐지 확인</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>ICMP 프레임 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>가능한 재전송이나 고 대역폭 이용률을 확인하는 중</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>하드 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>LBM/LBR 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>영구적인 루프 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>프로토콜 계층 통계를 확인하는 중</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>소스 주소의 유효성을 확인하고 있습니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>이 박스를 체크하면 테스트를 나갈 때 테스트 셋업이 원래의 설정으로 복구될 것입니다 . 비대칭 테스팅의 경우 , 이들은 로컬와 원격 사이드 모두에서 복구될 것입니다 . 셋업을 복구하면 링크가 리셋될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>소스 IP 와 연결되어 있는 장치 사이의 포트 설정을 확인하세요 ; 반이중 조건이 존재하지 않는지 확인하세요 .  애널라이저를 목적지 IP 에 더 가까이 옮김으로 추가적인 구획화도 달성할 수 있습니다 ; 잘못된 링크를 분리하기 위해 재전송이 제거되었는지 살피세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>분석하기 위해 캡쳐 파일 선택</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>PCAP 파일 &#xA; 선택</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>원하는 대역폭 측정 정확도를 선택하세요 &#xA;( 더 짧은 테스트 시간의 경우 1% 를 권장합니다 ).</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>플로우 컨트롤 로그인 타입을 선택하세요</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>프레임 또는 패킷 크기를 선택하세요</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>백투백 테스트를 실행하길 원하는 입도를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>프레임 손실 테스트를 실행하길 원하는 입도를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>회로가 사용할 수 있는 최대 대역폭을 선택하세요 .  장치는 이 수를 전송하는 최대 대역폭으로 사용하여 테스트의 길이를 줄일 것입니다 :</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>최대 버퍼 크레딧 크기를 선택하세요 .&#xA; 테스트가 이 수로 시작하여 테스트 길이를 줄일 것입니다 :&#xA; 주의 :  트래픽을 루핑하고 있는 원격 장치를 &#xA; 다음의 파라미터로 설정해야 합니다 :&#xA;&#xA;1. 플로우 컨트롤 ON&#xA;2.  {1}&#xA;3.  위에서 입력한 것과 동일한 값으로 설정된 {2} 버퍼 크레딧 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>백투백 테스트를 위한 최대 시도 시간을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>'하향식' 또는 '상향식' 테스트 절차와 함께 사용할 최소 및 최대 로드 값을 선택하세요</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>각 프레임 크기에 대해 백투백 테스트를 실행하기를 원하는 시도 횟수를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>각 프레임 크기에 대해 지연 (RTD) 테스트를 실행하기를 원하는 시도 횟수를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>각 프레임 크기에 대해 패킷 지터 테스트를 실행하기를 원하는 시도 횟수를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>허용되는 처리량 프레임 손실 오차 비율을 선택하세요 .&#xA; 주의 : > 0.00 의 설정은 RFC2544 와는 맞지 않습니다</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>각 지연 (RTD) 시도가 지속될 시간을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>각 패킷 지터 시도가 지속될 시간을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>처리량 테스트를 통과하기 위해 에러 없이 전송되어야 하는 속도에 대한 시간을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>각 프레임 손실 시도가 지속되기를 원하는 시간을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>버퍼 크레딧 테스트를 위한 시도 시간을 선택하세요</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>프레임 손실 테스트에서 사용할 절차를 선택하세요 .&#xA; 주의 : RFC2544 절차는 최대 대역폭으로부터 실행되고 각 시도마다 대역폭 입도 만큼 감소하며 , 어떠한 프레임도 손실되지 않고 연속적으로 두 번을 시도한 후에 종료합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>시스코 디스커버리 프로토콜</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>이 네트워크에서 시스코 디스커버리 프로토콜 (CDP) 메시지가 발견되었으며 표는 반이중 설정을 통지한 MAC 주소와 포트를 나열합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>모두 삭제</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>표준 사용자 인터페이스로 전환하기 위해 결과 버튼을 클릭하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>닫기</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>코드 위반</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>결합</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation>설명</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>설명</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>원단과의 통신이 성공적으로 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>원단과의 통신을 설정할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>원단과의 통신이 끊어졌습니다</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>완료</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>완료 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation>설정 이름</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>설정 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>설정 이름</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>설정 이름이 필요합니다</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>설정 읽기 전용</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>설정 요약</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>체크된 아이템 설정</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>{1}가 트래픽을 얼마나 오래 전송할 지를 설정합니다.</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>설정 교체 확인</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>삭제 확인</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNECTED</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>연결 중</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>테스트 측정 애플리케이션과 연결</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>반이중에서 계속</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>반이중 모드로 계속 작업함</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>로컬 셋업을 &#xA; 원격 셋업으로 복사</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>복사 &#xA; 선택</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>원단을 루핑 업 할 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>보고서 생성</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>크레딧</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>( 크레딧 )</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA; 현재 스크립트 : {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>현재 선택</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation>고객</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>고객</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>고객 이름</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>데이터 비트 전송률</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>데이터 바이트 전송률</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>데이터 레이어 정지</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>데이터 모드</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>데이터 모드를 PPPoE 로 설정했음 </translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>데이터 크기</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>날짜</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>날짜 &amp; 시간</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>일</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>지연 , 현재 (us):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>지연 , 현재 (us)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>목적지 주소</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>목적지 설정</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>목적지 ID</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>목적지 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>목적지 IP&#xA; 주소</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>IP 주소 {1} 를 위한 목적지 MAC 가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>목적지 MAC 를 찾았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>목적지 MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>상세 라벨</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>세부사항</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>발견됨</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>탐색</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>탐지된 링크 대역폭</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       {1} 대역폭으로 전송된 것보다 더 많은 프레임이 발견되었습니다 – 유효하지 않은 테스트 .</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>대칭 처리량 결정 중</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>장치 세부사항</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>장치 ID</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>DHCP 파라미터를 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>DHCP 파라미터를 찾았습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>발견된 장치</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>발견</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>원단 루프 종류 발견 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>발견을 &#xA; 현재 &#xA; 할 &#xA; 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>표시 :</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>다운스트림</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>다운스트림 방향</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> 어쨌든 진행하시겠습니까 ? </translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>이중</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>활성화됨</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>확장 레이어 2 트래픽 테스트 활성화</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>캡슐화 :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>캡슐화</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>종료</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>종료 일자</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>범위의 끝 :</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>종료 시간</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>FTP 테스트를 실행하고자 하는 IP 주소나 서버 이름을 입력하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>연결하고자 하는 서버를 위한 로그인 이름을 입력하세요</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>사용하고자 하는 계정에 대한 비밀번호를 입력하세요</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>새로운 설정 이름을 입력하세요 &#xA;( 글자 , 숫자 , 스페이스 , 대시 및 밑줄만 사용할 수 있습니다 ):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA; 에러 : {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>에러 : 응답 타임아웃이 발생하였습니다 &#xA; 내에 반응이 없었습니다</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>에러 : 연결을 설정할 수 없었습니다</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>에러 카운트</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>에러 프레임</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>PCAP 파일 로딩 중 에러 발생</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>에러 : 1 차 DNS 가 이름 확인을 실패했습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>에러 : 사이트를 찾을 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>남은 예상 시간</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>남아 있는 예상 시간</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     이더넷 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>이벤트</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>이벤트 로그가 가득 찼습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>과도한 재전송 발견</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>J-QuickCheck 나가기</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>예상 처리량</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>예상 처리량은</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>예상 처리량을 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>전문가 RFC 2544 테스트 버튼 .</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>명시적 (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>명시적 ( 패브릭 /N-Port)</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> 명시적 로그인을 완료하지 못했습니다 . </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>실패</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>원단이 JDSU 스마트 클래스 이더넷 테스트 세트입니다.</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>원단은 Viavi 스마트 클래스 이더넷 테스트 세트입니다 .</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC 테스트</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>Acterna 테스트 페이로드를 사용하여 FC 테스트를 실행합니다</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>FDX 사용 가능</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>파이버 채널 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>파일 설정</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>파일 이름</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>파일</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>파일 크기</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>파일 크기 :</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>파일 크기</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>파일 크기 : {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>파일 크기 :</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>파일 크기</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>파일이 너무 빨리 전송되었습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>예상 처리량을 찾는 중</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>최대 사용처를 찾는 중</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>처음 50 개 반이중 포트 ( 총 {1} 개 중 )</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>처음 50 개 ICMP 메시지 ( 총 {1} 개 중 )</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>플로우 컨트롤</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>플로우 컨트롤 로그인 타입</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>폴더</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation> 파이버의 두 배 길이를 보상하기 위해 각 프레임이 반으로 감소합니다 .</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>찾았음</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>액티브 루프가 발견되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>하드웨어 루프가 발견되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>프레임 &#xA; 길이</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>프레임 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>프레임 길이 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>테스트하려는 프레임 길이</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>프레임 손실 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>프레임 손실</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>프레임 손실 {1} 바이트 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>프레임 손실 {1} 바이트</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>프레임 손실 대역폭 입도 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>프레임 손실 대역폭 입도</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>프레임 손실 최대 대역폭</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>프레임 손실 최소 대역폭</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>프레임 손실률</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>프레임 손실 비율</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>프레임 손실 테스트</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>프레임 손실 테스트 절차 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>프레임 손실 테스트 절차</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>프레임 손실 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>프레임 손실 오차 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>프레임 손실 시도 지속 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>프레임 손실 시도 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>프레임 또는 패킷</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>프레임</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>프레임 크기</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>프레임 크기 :  {1} 바이트</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>프레임 &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>프레임 S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>프레이밍</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(frms)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(frms/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>FTP 처리량</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>FTP 처리량 테스트 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>FTP 처리량 테스트 완료 !</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>FTP 처리량 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>전체</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>PCAP 정보 얻기</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>절반</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>반이중 포트</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>하드웨어</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>하드웨어 루프</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>( 하드웨어 &#xA; 또는 액티브 )</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>( 하드웨어 ,&#xA; 영구적 &#xA; 또는 액티브 )</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>HDX 사용 가능</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>고 이용률</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>홈</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>HTTP 처리량 테스트</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>HTTP 처리량 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>ICMP&#xA; 코드</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>ICMP 메시지</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>만약 산발적인 방법으로 에러 카운터가 증가하고 있다면 , 더 낮은 다른 트래픽 속도에서 매뉴얼 테스트를 실행하세요 . 만약 더 낮은 속도에서 여전히 에러가 발생한다면 , 최대 로드와는 관련이 없는 일반적인 문제가 링크에 있을 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>만약 문제가 지속되면 , 도구 메뉴에서 ‘기본값로 테스트 리셋’하세요 .</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>만약 산발적 에러와 관련된 문제를 해결할 수 없다면 , 작은 프레임 손실률을 허용하도록 프레임 손실 오차 한계를 설정할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>함축적 ( 투명 링크 )</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>정보</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>통신 초기화 중</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>에서 대역폭을 결정하기 위해</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>로컬과 원격 사이드에 대한 입력률이 맞지 않습니다</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>라인 상에 문제가 간헐적으로 나타나고 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>내부 에러</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>내부 에러 – PPPoE 재시작</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>유효하지 않은 설정</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>유효하지 않은 IP 설정</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>IP 주소</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>IP 대화</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>나가고 있음</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>시작하고 있습니다</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck 이 완료되었습니다</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck 이 링크를 분실했거나 링크를 설정할 수 없었습니다</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kbytes</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>킬</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>L2 트래픽 테스트는 J-QuickCheck 를 다시 실행하여 재시작할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>지연</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>대기 시간 (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>지연 (RTD) 와 패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>지연 (RTD) 평균 : N/A</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>지연 (RTD) 통과 한계 :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>지연 (RTD) 통과 한계</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>지연 (RTD)&#xA;( 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>지연 (RTD) 결과</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>지연 (RTD) 테스트</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>지연 (RTD) 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>지연 (RTD) 테스트 결과 : 중단   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>지연 (RTD) 테스트 결과 : 실패</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>지연 (RTD) 테스트 결과 : 통과</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>지연 (RTD) 테스트 결과 건너뜀</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>지연 (RTD) 테스트 건너뜀</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> 지연 (RTD) 한계 : {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>지연 (RTD) 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>지연 (RTD) 시도 지속 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>지연 (RTD) 시도 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>지연 (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>레이어 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>레이어 1 / 2&#xA; 이더넷 Health</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>레이어 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>레이어 2 링크를 찾았습니다</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>레이어 2 퀵 테스트</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>레이어 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>레이어 3&#xA;IP Health</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>레이어 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>레이어 4&#xA;TCP Health</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR 루프</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>길이</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>링크 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>링크 레이어 디스커버리 프로토콜</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>링크 손실 </translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>캡쳐 파일에서 탐지된 링크 속도</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>리슨 포트</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>로드 포맷</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>로딩 중 ... 기다려주세요</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>로컬</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>로컬 목적지 IP 주소가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>로컬 목적지 MAC 주소가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>로컬 목적지 포트가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>로컬 루프 타입이 유니캐스트로 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>로컬 포트</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>로컬 원격 IP 주소가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> 로컬 시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>로컬 셋업</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> 로컬 소프트웨어 개정</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>로컬 소스 IP 필터가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>로컬 소스 MAC 필터가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>로컬 소스 포트 필터가 다음과 같이 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>로컬 요약</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> 로컬 테스트 장치 이름</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>표에 나열된 소스 MAC 주소와 포트가 있는 장치를 찾아서 이중 설정이 자동이 아닌 전체로 설정되어 있는지 확인하세요 .  호스트가 자동으로 설정되고 네트워크 장치도 자동으로 설정되어 링크가 반이중으로 잘못 협상되는 것이 드문 일이 아닙니다 .</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation>위치</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>위치</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>로그인 :</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>로그인</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>로그인 이름</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>로그인 이름</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>루프 실패</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>원단 장치 루핑 다운 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>원단 장치 루핑 업 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>루프 상태 알 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>루프 업 실패</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>루프 업 성공</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>루프 업 성공</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>레이어 2 링크의 손실이 발견되었습니다 !</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>손실된</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>손실된 프레임</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC 주소</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>관리 주소</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>MAU 종류</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>최대</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>최대</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( 최대 {1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>최대 평균</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>최대 평균 패킷 지터 :</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>최대 평균 패킷 지터 : N/A</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>최대 평균 Pkt 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>최대 대역폭 (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>최대 대역폭 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>최대 버퍼 크기</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>지연 (RTD) 테스트를 통과하기 위해 필요한 최대 지연 , 평균</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>패킷 지터 테스트를 통과하기 위해 필요한 최대 패킷 지터 , 평균</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>최대 RX 버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>최대 테스트 대역폭 :</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>최대 테스트 대역폭</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>측정된 최대 처리량 :</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>최대 시간 제한 VLAN ID 당 {1}</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>최대 시간 제한 VLAN ID 당 7 일</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>최대 시도 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>최대 TX 버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>최대 속도</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>최대 재전송 시도에 도달했습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>측정된</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>측정된 속도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>측정된 속도</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>측정된 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>측정 정확도</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>{1} 버퍼 크레딧에서 처리량 측정 중</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>메시지</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>최소</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>최소 퍼센트 대역폭</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>처리량 테스트를 통과하기 위해 필요한 최소 퍼센트 대역폭 : </translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>최소 시간 제한 VLAN ID 당 5 초</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>최소 속도</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>분</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>모델</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>수정</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>MPLS/VPLS 캡슐화는 현재 지원되지 않습니다 ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>N/A ( 하드 루프 )</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>네트워크 업</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>네트워크 이용률</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>네트워크 이용률이 과도한 것으로 탐지되지는 않았지만 , 이 차트는 네트워크 이용률 그래프를 제공합니다</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>네트워크 이용률이 과도한 것으로 탐지되지는 않았지만 , 이 표는 IP 최대 사용처 목록을 제공합니다</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>New</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>새로운 설정 이름</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>새로운 URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>다음</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>아니오.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>아니오</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>호환 가능한 애플리케이션이 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt; 사용 가능한 설정이 없음 ></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>테스트를 하기 위한 파일이 선택되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>하드웨어 루프가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>발견된&#xA;JDSU&#xA;장치가&#xA;없습니다</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>레이어 2 링크가 발견되지 않았습니다 !</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>어떠한 루프도 설정할 수 없었습니다</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>어떠한 루프도 설정하거나 발견하지 못했습니다</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>영구적인 루프가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>실행 중인 애플리케이션이 탐지되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>RFC2544 와 맞지 않습니다</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>연결되지 않음</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>결정되지 않음</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>주의 :  > 0.00 설정은</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>주의 : 버퍼 크레딧이 2 미만인 하드 루프를 가정합니다 .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA; 주의 : 버퍼 크레딧이 2 미만인 하드 루프를 가정합니다 .&#xA; 이 테스트는 유효하지 않습니다 .&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>주의 : 하드 루프 가정을 근거로 , 각 프레임을 위해 계산된 최소 버퍼 크레딧이 파이버의 두 배 길이를 보상하기 위해 절반으로 감소될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>주의 : 하드 루프 가정을 근거로 , 파이버의 두 배 길이를 보상하기 위해 각 단계에서 버퍼 크레딧의 수의 두배로 처리량 측정이 이루어집니다 .</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>주의 : 일단 프레임 손실 오차를 사용하면 , 테스트는  RFC 2544 권장사항을 따르지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>주의</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>선택되지 않음</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Viavi&#xA; 장치가 &#xA; 발견되지 &#xA; 않았음</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>지금 나가는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>지금 확인 중</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>지금 {1} 크레딧으로 확인 중입니다 .  이것은 {2} 초가 결릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>백투백 시도 수 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>백투백 시도 수</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>실패 횟수</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>테스트한 ID 의 수</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>지연 (RTD) 시도 횟수 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>지연 (RTD) 시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>패킷 지터 시도 횟수 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>패킷 지터 시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>패킷 수</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>성공 횟수</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>시도 횟수 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>시도 횟수</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>의</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>꺼짐</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>프레임이 1 초 내에 손실되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>J-QuickCheck 예상 처리량의</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>( 라인 율의 %)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>라인 율의 %</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>라인율의</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>On</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * {1} 시도만 사용할 수 있는 데이터를 산출하였습니다 *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>(ON 또는 OFF)</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS 프레임</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>발신자 ID</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>범위 초과</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        전체 테스트 결과 : {1}        </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    전체 테스트 결과 : 중단   </translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>초과 범위</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>L2 필터 Rx Acterna 프레임 카운트가 계속 증가하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>패킷</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>패킷 지터</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>패킷 지터 , 평균</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>패킷 지터 통과 한계 :</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>패킷 지터 통과 한계</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>패킷 지터 &#xA;( 처리량 필요 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>패킷 지터 결과</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>패킷 지터 테스트 결과 : 중단   </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>패킷 지터 테스트 결과 : 실패</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>패킷 지터 테스트 결과 : 통과</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> 패킷 지터 한계 : {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>패킷 지터 한계 (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>패킷 지터 시도 지속 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>패킷 지터 시도 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>패킷 &#xA; 길이</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>패킷 길이 ( 바이트 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>패킷 길이 :</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>테스트하려는 패킷 길이</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>패킷 크기</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>패킷 크기</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>성공</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>통과 / 실패</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>통과 / 실패</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>통과 속도 (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>통과율 (frm/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>통과 속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>통과율 (pkts/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>비밀번호 :</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>비밀번호</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>Advrt 정지</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>정지 사용 가능 </translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>정지 Det</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>정지 프레임 발견됨</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>정지 프레임 발견됨 – 유효하지 않은 테스트</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>PCAP 파일 분석 에러</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>PCAP 파일</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>펜딩</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>클린업 실행 중</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>영구적</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>영구적인 루프</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>( 영구적 &#xA; 또는 액티브 )</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>Pkt</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>Pkt 지터 (us)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation>Pkt 길이</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Pkt 손실</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(pkts)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(pkts/ 초 )</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>플랫폼</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>Sync 와 링크가 있는지 확인하세요 ,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>올바르게 연결되었는지 확인하세요 ,</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>올바르게 연결되었는지 확인하세요 ,</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>다른 설정 이름을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>리포트를 저장하기 위해 파일명을 입력하세요 ( 최대 %{1} 자 ) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>설명을 입력하세요 ( 최대 {1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>설명을 입력하세요 최대 %{1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>설명(최대 {1} 자)을 입력하세요&#xA;(글자, 숫자, 스페이스, 대시 및 밑줄만 사용할 수 있습니다)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>고객 이름을 입력하세요 최대 %{1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>고객 이름을 입력하세요 ( 최대 %{1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>고객의 이름&#xA;(최대 {1} 자)을 입력하세요&#xA;(글자, 숫자, 스페이스, 대시 및 밑줄만 사용할 수 있습니다)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>고객의 이름(최대 {1} 자)을 입력하세요&#xA;(글자, 숫자, 스페이스, 대시 및 밑줄만 사용할 수 있습니다)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>고객의 이름(최대 {1} 자)을 입력하세요&#xA;(글자, 숫자, 스페이스, 대시 및 밑줄만 사용할 수 있습니다)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>기술자 이름을 입력하세요 ( 최대 {1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>기술자 이름을 입력하세요 최대 %{1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>기술자의 이름(최대 {1} 자)을 입력하세요&#xA;(글자, 숫자, 스페이스, 대시 및 밑줄만 사용할 수 있습니다)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>테스트 위치를 입력하세요 ( 최대 {1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>테스트 위치를 입력하세요 최대 %{1} 자 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>테스트 위치(최대 {1} 자)를 입력하세요&#xA;(글자, 숫자, 스페이스, 대시 및 밑줄만 사용할 수 있습니다)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>원격으로 연결 버튼을 누르세요</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>수동 트래픽 테스트로 링크의 성능을 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>로컬 및 원격 소스 IP 주소를 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>로컬 소스 IP 주소를 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>원격 IP 주소를 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>원격 소스 IP 주소를 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>기다려주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>기다려 주세요 ...</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>PDF 파일이 작성될 때까지 기다려주세요 .</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>PDF 파일이 작성될 때까지 기다리세요 .&#xA; 이 작업은 최대 90 초 정도 걸릴 수 있습니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>포트 :</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>포트</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>포트 {1}: 테스트 보고서를 저장하시겠습니까 ?&#xA;&#xA; 예나 아니오를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>포트 ID</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>포트 :				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>포트 :				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP 액티브</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP 인증 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP 실패 알려지지 않음</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>PPP IPCP 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP 실패</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE 액티브</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE 실패</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE 비활성</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE 시작됨</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>PPPoE 상태 : </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE 업</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>PPPoE 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP 타임아웃</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP 알려지지 않은 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>PPP UP 실패</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP 실패</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>메인 화면으로 돌아가기 위해 닫기를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>메인 화면으로 돌아가기 위해 나가기를 누르거나 다시 실행하기 위해 테스트 실행을 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>발견하기 &#xA; 위해 &#xA; 새로 고침 &#xA; 버튼을 &#xA; 누르세요</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>J-QuickCheck 를 나가려면 J-QuickCheck 나가기 버튼을 누르세요</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>현재 서브넷 상에 있는 Viavi 장치를 발견하기 위해 아래의 새로 고침 버튼을 누르세요 . 오른쪽 테이블에서 세부사항을 보기 위해 장치를 선택하세요 . 만약 새로 고침을 사용할 수 없다면 , 발견을 사용할 수 있는지 , 그리고 sync 와 링크가 있는지 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>로컬 및 원격 테스트 셋업과 사용 가능한 대역폭을 확인하기 위해 &#xA;J-QuickCheck 실행 버튼을 누르세요</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>이전</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>진행</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>특성</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>다음 단계 제안</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>공급자</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>종료</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>랜덤</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> 범위</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>범위</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>속도</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>속도 , 최대 로드와 관련이 없는 일반적인 문제가 링크에 있을 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>{2} 에서 {1} 바이트 검색됨</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>수신된 프레임</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>권장사항</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>권장 수동 테스트 설정 :</translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>새로 고침</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>원격</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>원격 IP 주소</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>원격 루프</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation>원격 시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation>원격 시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>원격 셋업</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>원격 셋업을 복구할 수 없었습니다 .</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> 원격 소프트웨어 개정</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>원격 소프트웨어 버전</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>원격 요약</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation>원격 테스트 장치 이름</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation>원격 테스트 장치 이름</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>범위 제거</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>응답자 ID</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>응답 중 &#xA; 라우터 IP</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>J-QuickCheck 재시작</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>나가기 전에 프리 테스트 설정 복구</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>원격 테스트 세트 설정 복구 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>로 제한된 RFC</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>기본 로드 테스트의 결과가 없습니다 , 가능한 해결책을 보기위해 다음 단계 제안을 클릭하세요</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>결과</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>모니터링 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>재전송</translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>재전송이 발견되었습니다 &#xA; 시간 경과에 따른 재전송 발생 분석 중</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>재전송이 발견되었습니다 . 추가 분석을 위해 USB 로 파일을 내보내기 하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>{1} 의 검색이 사용자에 의해 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>다음을 클릭하여 RFC 2544 사용자 인터페이스로 돌아갑니다 . </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation> RFC 2544 이더넷 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation> RFC 2544 모드</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation> RFC 2544 모드</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation> RFC 2544 표준</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation> RFC 2544 테스트</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>Acterna 테스트 페이로드를 사용하여 RFC2544 테스트를 실행합니다</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>다중 스트림 그래픽 결과를 실행하는 중에는 RFC/FC 테스트를 실행할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>Rfc 모드</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY Det</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>실행</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>FC 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>J-QuickCheck 실행</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>$l2quick::testLongName 실행</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>실행 중</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>{1}{2} 로드에서 테스트 실행 중 .</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>{1}{2} 로드에서 테스트 실행 중 .  이것은 {3} 초가 결릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation> RFC 2544 테스트 실행</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>스크립트 실행</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Rx Mbps, 현재 L1</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx 만</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>저장</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>FTP 처리량 테스트 보고서 저장</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>HTTP 처리량 테스트 보고서 저장</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>VLAN 스캔 테스트 보고서 저장</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> 축소된 대역폭</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>스크린샷 캡쳐됨</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>스크립트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>( 초 )</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>초</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>복사된 설정을 위한 이름을 선택하세요</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>새로운 설정을 위한 이름을 선택하세요</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>VLAN ID 의 범위 선택</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>선택한 Tx 방향 :</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>선택한 Tx 방향 &#xA; 다운스트림</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>선택한 Tx 방향 &#xA; 업스트림</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>선택 경고</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>설정을 수정하려면 OK 를 선택하세요 &#xA; 새로운 설정을 생성하려면 이름을 편집하세요 &#xA;( 글자 , 숫자 , 스페이스 , 대시 및 밑줄 만 사용하세요 )</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>테스트 설정 선택 :</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>발견된 장치 목록을 정렬하기 위해 원하는 특성을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>실행하고자 하는 테스트를 선택하세요 :</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>URL 선택</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>로드 관련 설정을 위해 사용할 포맷을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>목적지 MAC 에 대한 ARP 요청을 전송하는 중</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>트래픽 전송 중</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>트래픽 전송 , J-QuickCheck 가 발견한 예상 처리량은 이 값으로 스케일될 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>시퀀스 ID</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>시리얼 번호</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>서버 ID:</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>서버 ID</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>서비스 이름</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>설정</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>통과 / 실패 상태 보기</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA; 다음에 대한 통과 / 실패 상태 보기 :</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>크기</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>건너뛰기</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>지연 (RTC) 테스트를 건너뛰고 계속하는 중</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>소프트웨어 Rev</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> 소프트웨어 개정</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>소스 주소</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>소스 주소를 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>소스 유효성이 설정되었습니다 ...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>소스 ID</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>소스 IP</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>소스 IP&#xA; 주소</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>소스 IP 주소가 목적지 주소와 동일합니다</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>소스 MAC</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>소스 MAC&#xA; 주소</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>링크 대역폭 지정</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>속도</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>속도 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>시작</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>START</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>시작 일자</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>기본 로드 테스트 시작</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>시도 시작</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>시작 시간</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>상태 알 수 없음</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>정지</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOP</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>TCP 재전송이 성능이 저하된 네트워크 이용률과 관련이 있는지 결정하기 위해 그래프를 검토하세요 .  상당한 양의 TCP 재전송을 야기하고 있는 소스 IP 를 결정하기 위해 TCP 재전송 탭을 확인하세요 . 소스 IP 와 연결되어 있는 장치 사이의 포트 설정을 확인하세요 ; 반이중 조건이 존재하지 않는지 확인하세요 .  애널라이저를 목적지 IP 에 더 가까이 옮김으로 추가적인 구획화도 달성할 수 있습니다 ; 잘못된 링크를 분리하기 위해 재전송이 제거되었는지 살피세요 .</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>성공 !</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>성공</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>요약</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>측정 값 요약 :</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>페이지 {1} 의 요약 :</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN 사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>대칭</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>대칭 모드는 루프백을 사용하여 근단에서 송수신합니다 . 비대칭은 업스트림 모드에서는 근단에서 원단으로 전송하고 , 다운스트림 모드에서는 원단에서 근단으로 전송합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>대칭</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      선택하려는 설정 이름을 클릭하세요 </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	{1} MB 파일을 취합니다 ....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	{1} MB 파일을 둡니다 ...</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   속도 : {1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   속도 : {1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Rx 프레임 {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		 경고 &#xA;&#xA;	 이 설정을 영구적으로 &#xA;	 삭제하시겠습니까 ?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   글자 , 숫자 , 스페이스 , 대시 및 밑줄 만 &#xA;		 사용하세요 !</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   이 설정은 읽기 전용이며 &#xA;		   삭제할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         키패드를 사용하여 새로운 설정을 위한 이름을 &#xA;		 입력해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   지정한 설정이 이미 존재합니다 .</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   시간 : {1} 초 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Tx 프레임 {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>TCP 호스트가 연결을 설정하는데 실패하였습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>TCP 호스트에서 에러가 발생했습니다 . 테스트가 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>TCP 재전송</translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation>기술자</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>기술자</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>기술자 이름</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>종료</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>테스트가 취소되었음</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>테스트가 사용자에 의해 중단되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>설정 - 모든 테스트 탭에서 설정된 최고 대역폭으로 테스트</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>다른 더 낮은 트래픽 속도로 테스트합니다 . 더 낮은 속도에서도 에러가 발생할 경우</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>테스트 완료</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>테스트 설정 :</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>테스트 설정</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>테스트 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>테스트 지속 시간 : 설정된 테스트 지속 시간의 적어도 3 배 .</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>테스트된 대역폭</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### 테스트 실행 완료 ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>{1} 크레딧 테스트 중 </translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>{1} 크레딧 테스트 중 </translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>에서 테스팅 중 ... </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>연결을 테스트하고 있습니다 ... </translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation> 테스트 장치 이름</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>테스트가 시작하고 있습니다</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>테스트 위치</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>테스트 로그 :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>테스트 이름 :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>테스트 이름 :			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>테스트 이름 :			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>테스트 절차</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>테스트 진행 로그</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>테스트 범위 (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>테스트 범위 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>테스트 세트 셋업</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>실행할 테스트 :</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>실행할 테스트</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>테스트가 중단되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>액티브 루프가 실패했으며 하드 루프가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>액티브 루프가 실패했으며 하드웨어 또는 영구적인 루프가 발견되지 않았습니다</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>{1} 에 대한 평균 속도는 {2} kbps 였습니다</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>{1} 에 대한 평균 속도는 {2} Mbps 였습니다</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>파일이 JMentor 에 대한 50000 패킷 제한을 넘었습니다</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>작은 프레임 손실율을 허용하기 위한 프레임 손실 오차 한계 .</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>인터넷 컨트롤 메시지 프로토콜 (ICMP) 는 ICMP 핑의 구문에서 가장 널리 알려져있습니다 . ICMP Destination Unreachable 메시지는 라우터나 네트워크 장치가 목적지에 도달할 수 없다는 것을 의미합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>트래픽이 정지되어야 하지만 지난 10 초 동안</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>LBM/LBR 루프 실패 .</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>로컬 및 원격 소스 IP 주소가 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation> 로컬 셋업 설정이 원격 셋업으로 성공적으로 복사되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>로컬 소스 IP 주소를 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>측정된 MTU 는 계속하기에 너무 작습니다 . 테스트가 취소되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>선택하신 이름은 이미 사용 중입니다 .</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>연결된 네트워크 요소 포트는 반이중으로 제공됩니다 . 이것이 정확하다면 , 반이중에서 계속 버튼을 누르세요 . 그렇지 않은 경우 J-QuickCheck 나가기를 누르고 포트를 다시 설정하세요 .</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>네트워크 이용률 차트는 캡쳐 지속 시간 동안 캡쳐 파일에서 모든 패킷이 소비한 대역폭을 표시합니다 .  만약 TCP 재전송도 역시 탐지되었다면 , 메인 분석 화면으로 돌아가서 레이어 TCP 층 결과를 검토하는 것이 좋습니다 .</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>파이버의 두 배 길이를 보상하기 위한 각 단계에서의 버퍼 크레딧의 수 .</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>이론적인 계산</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>이론적 &amp; 측정된 값 :</translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>파트너 포트 ( 네트워크 요소 ) 가 AutoNeg OFF 을 가지고 있고 예상 처리량은 사용할 수 없으므로 , 파트너 포트는 아마도 반이중 모드일 것입니다 . 만약 파트너 포트에 반이중이 맞지 않다면 , 파트너 포트에서 설정을 전이중으로 변경하시고 J-QuickCheck 를 다시 실행하세요 . 그 다음 , 만약 측정된 예상 처리량이 더 합리적이라면 ,  RFC 2544 테스트를 실행할 수 있습니다 . 만약 예상 처리량을 여전히 사용할 수 없다면 , 원격 사이드에서 포트 설정을 확인하세요 . HD to FD 포트 모드 불일치가 있을 가능성이 있습니다 .&#xA;&#xA; 만약 파트너 포트에서 반이중이 정확하다면 , 결과 -> 셋업 -> 인터페이스 -> 물리층으로 가서 이중 설정을 전체에서 절반으로 변경하세요 . 그 다음 RFC2544 스크립트로 돌아가서 ( 결과 -> 전문가 RFC2544 테스트 ), J-QuickCheck 를 나가 , 처리량 탭으로 가서 제로 - 인 프로세스  RFC 2544 표준 ( 반이중 ) 을 선택하고  RFC 2544 테스트를 실행하세요 .</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>원단과의 통신 문제가 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>원격 루핑 장치는 Viavi 루프백 명령에 응답하지 않지만 , 소스와 목적지 주소 필드를 교환하여 전송된 프레임을 로컬 장치로 다시 돌려보냅니다</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>원격 루핑 장치는 Viavi 루프백 명령에 응답하고 소스와 목적지 주소 필드를 교환하여 전송된 프레임을 로컬 장치로 다시 돌려보냅니다</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>원격 루핑 장치는 전송된 프레임을 변경없이 로컬 장치로 다시 돌려 보냅니다</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>원격 루핑 장치는 OAM LBM 을 지원하며 수신된 LBM 프레임에 대한 응답으로 상응하는 LBR 프레임을 로컬 장치로 다시 전송합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>원격 사이드는 MPLS 캡슐화를 위해 설정되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>원격 사이드는 루프백 애플리케이션인 것 같습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>원격 소스 IP 주소를 사용할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA; 보고서가 PDF 포맷으로 {1}{2} 의 이름으로 저장되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA; 보고서가 PDF 포맷으로 {1}{2} 의 이름으로 저장되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation>보고서가 PDF, TXT, LOG 포맷으로 {1}{2} 의 이름으로 저장되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>보고서가 TXT 와 LOG 포맷으로 {1}{2} 의 이름으로 저장되었습니다</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>응답 라우터 IP 는 패킷을 목적지 IP 주소로 전송할 수 없으며 , 따라서 응답 라우터 IP 와 목적지 사이에서 트러블슈팅을 실행해야 합니다 .</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation> RFC 2544 테스트는 MPLS 캡슐화를 지원하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>전송 레이저가 켜졌습니다 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>전송 레이저가 꺼져 있습니다 !&#xA; 레이저를 켜시겠습니까 ?&#xA; 레이저를 켜려면 예 , 취소하려면 아니오를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>전송 레이저가 꺼져 있습니다 !&#xA; 레이저를 켜시겠습니까 ?&#xA; 레이저를 켜려면 예 , 취소하려면 아니오를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>VLAN 스캔 테스트는 OAM CCM On 으로 완료될 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>VLAN 스캔 테스트를 정상적으로 설정할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;    이 설정은 읽기 전용이며 수정할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>비대칭 모드를 사용할 때는 이것이 원단의 IP 주소여야 합니다</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>이 표는 TCP 재전송을 경험하고 있는 IP 소스 주소를 보여줍니다 . TCP 재전송이 탐지되면 , 이것은 ( 목적지 사이드로 향하는 ) 다운스트림 패킷 손실 때문일 수 있습니다 .  그것은 또한 반이중 포트 문제가 있다는 것을 나타낼 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>이 테스트는 Acterna 테스트 페이로드를 사용하여 실행됩니다</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>이 테스트는 유효하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>이 테스트에는 트래픽이 VLAN 캡슐화를 가지고 있어야 합니다 . 연결된 네트워크가 이 설정을 위한 IP 주소를 제공할 것인지 확인하세요 .</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>이것은 {1} 초가 결릴 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>처리량 (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>처리량</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>처리량 ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>처리량 및 레이턴스 (RTD) 테스트</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>처리량과 패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>처리량 프레임 손실 오차 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>처리량 프레임 손실 오차</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>처리량 , 지연 (RTD) 와 패킷 지터 테스트</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>처리량 통과 한계 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>처리량 통과 한계</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>처리량 스케일링</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>처리량 스케일링 인수</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>처리량 테스트</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>처리량 테스트 지속 시간은 {1} 초였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>처리량 테스트 결과 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>처리량 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>처리량 테스트 결과 : 중단   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>처리량 테스트 결과 : 실패</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>처리량 테스트 결과 : 통과</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>처리량 한계 (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> 처리량 한계 : {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>처리량 한계 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>처리량 시도 지속 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>처리량 시도 지속 시간</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>처리량 제로 - 인 프로세스 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>처리량 제로 - 인 프로세스</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation>시간 끝</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>시간 끝</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>ID 당 시간 :</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>시간 &#xA;( 초 )</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation>시간 시작</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>시간 시작</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>방문한 횟수</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>계속하려면 ,  케이블 연결을 확인하고 J-QuickCheck 를 다시 시작하세요</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>최대 처리량을 결정하기 위해서 tx 와 rx 프레임 카운트를 맞추는 표준  RFC 2544 방법이나 측정된 L2 평균 % Util 을 사용하는 Viavi 강화 방법을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>하향식</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>시간을 절약하기 위해 비대칭 모드에서 지연 (RTD) 를 한 방향으로만 실행해야 합니다</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>산발적 또는 지속적 프레임 손실이 발생하는지 확인합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>총 바이트</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>총 &#xA; 프레임</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>총 수</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>총 유틸 {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>총 Util (kbps):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>총 Util (Mbps):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>보고서를 보려면 {1} 을 나간 후 보고서 메뉴에서 보고서 보기를 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>이내로</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>트래픽 : {1} 로 일정함</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>트래픽 결과</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>원단으로부터 트래픽이 여전히 발생하고 있었습니다</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>전송 레이저 Off!</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>전송된 프레임</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>다운스트림 전송 중</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>업스트림 전송 중</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>시도</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>시도 {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>시도 {1} 완료 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>{2} 시도 중에 {1} :</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>시도 지속 시간 ( 초 )</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>시도</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>두 번째 시도 중</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>tshark 에러</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>TX 버퍼 크레딧</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Tx 방향</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Tx 레이저 Off</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Tx Mbps, 현재 L1</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx 만</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation> 원단을 자동적으로 루핑 업 할 수 없습니다 . </translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>테스트 측정 애플리케이션과 연결할 수 없습니다 !</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>테스트 측정 에플리케이션과 연결할 수 없습니다 !&#xA; 다시 시도하려면 예를 누르세요 . 중단하려면 아니오를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>DHCP 주소를 얻을 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>로컬 루프백을 사용하는 상태에서는 RFC2544 테스트를 실행할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>테스트를 실행할 수 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>로컬 루프백을 사용하는 상태에서는 VLAN 스캔 테스트를 실행할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>사용 불가</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>장치 식별자</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>UP</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>( 업 또는 다운 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>업스트림</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>업스트림 방향</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>(us)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>사용자 중단 테스트</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>사용자 취소 테스트</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>사용자 이름</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>사용자 우선순위</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>사용자 요청 비활성</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>사용자 선택 &#xA;( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>사용자 선택      ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>에러 이벤트를 검색하기 위해 요약 상태 화면을 사용하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>사용 중</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>프레임 사이즈 사용</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>이용률 및 TCP 재전송</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>값</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>파란색으로 표시된 값은 실제 테스트에서 나온 값입니다 .</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>링크가 활성화되었는지 확인하는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>원격 IP 주소를 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>원격 IP 주소를 확인하고 다시 시도하세요</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi 강화</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>테스트 할 VLAN ID 범위</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN 스캔 테스트</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>VLAN 스캔 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>VLAN 스캔 테스트 결과</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>VLAN 테스트 보고서</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>VTP/DTP/PAgP/UDLD 프레임 발견 !</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>자동 협상이 완료되기를 기다리는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>IP 주소를 위한 &#xA; 목적지 MAC 를 기다리는 중</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>DHCP 파라미터를 기다리는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>레이어 2 링크를 기다리는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>링크를 기다리고 있음</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>활성화될 OWD CDMA Sync 와 1PPS Sync 를 기다리는 중</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>성공적인 ARP 를 기다리는 중 ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>마지막 초에서 발견되었습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>웹사이트 크기</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>액티브 루프이 있습니다</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>에러가 발생하였습니다 !!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>반이중 링크를 테스트할 때 ,  RFC 2544 표준을 선택하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>윈도우 크기 / 용량</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>RFC 2544 권장사항 사용</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>테스트 보고서를 저장하시겠습니까 ?&#xA;&#xA; 예나 아니오를 누르세요 .</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>예</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>산발적이거나 지속적인 프레임 손실 이벤트가 있는지 확인하기 위해 그래픽 결과 프레임 손실률 현재 그래프도 사용할 수 있습니다 .</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>{1} 로부터 이 스크립트를 실행할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>다시 연결하기 위해 정지할 때까지 기다릴 필요가 있을 수 있습니다</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>최대 처리량 속도에 제로 - 인</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>최적 크레딧 버퍼에 제로 - 인</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>제로 - 인 프로세스</translation>
        </message>
    </context>
</TS>
