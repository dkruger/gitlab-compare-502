<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>Visualizzare download</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>Zoom avanti</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>Allontana zoom</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>Reimposta zoom</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>Download in corso...</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Aperta</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>Tempo rimanente: %1 minuti </translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>Tempo rimanente: %1 secondi</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>Download</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>Salva con nome...</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>Directory:</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nome file:</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
    </context>
</TS>
