<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>모바일 앱 성능</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>블루투스와 WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>WiFi 전용</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>설정하신 것이 적용되기 위해 테스트를 다시 시작하세요.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>붙여넣기</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>복사</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>잘라내기</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>삭제</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>선택</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>싱글</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>다중</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>전체</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>열기</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>이름바꾸기</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>새 폴더</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>디스크</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>붙여넣기는 기존의 파일을 덮어쓰기 할 것입니다 . </translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>파일 덮어쓰기</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>알려지지 않은 에러로 인해 파일을 열 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>파일을 열 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>선택한 파일이 영구적으로 삭제될 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>선택한 %1 파일이 영구적으로 삭제될 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>파일 삭제</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>폴더 이름을 입력하세요:</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>폴더 생성</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>폴더 %1 을 생성할 수 없습니다 .</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>폴더 %1 이 이미 존재합니다 .</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>폴더 생성</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>붙여넣기 중…</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>삭제 중…</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>완료.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 실패 .</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>파일 붙여넣기를 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>%1 파일 붙여넣기를 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>빈 공간이 충분하지 않습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>파일 삭제를 실패했습니다.</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>%1 파일 삭제를 실패했습니다.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>뒤로</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>옵션</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>시간</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>업그레이드 URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>파일 이름</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>작업</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>모듈</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>다른 무선 네트워크</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>아래의 무선 네트워크 정보를 입력하세요 .</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>이름 (SSID):</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>암호화 종류 :</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>없음</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>업그레이드 가능</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync 가 업그레이드가 가능하다고 결정하였습니다 .&#xA; 지금 업그레이드 하시겠습니까 ?&#xA;&#xA; 업그레이드를 하면 실행되고 있는 테스트가 종료될 것입니다 . 프로세스 완료시 테스트 세트는 업그레이드를 완료하기 위해 재부팅할 것입니다 .</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>알려지지 않은 에러가 발생했습니다. 다시 시도하세요.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>업그레이드가 실패했습니다</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>장치 정보:</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>베이스 옵션:</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>장치를&#xA;기본값로 리셋</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>로그를 &#xA;USB 스틱으로 내보내기</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>시스템&#xA;정보를 파일로 복사</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>시스템 정보가 이 파일로&#xA;복사되었습니다:</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>이것은 재부팅이 필요하며 시스템 설정과 테스트 설정을 기본값로 리셋할 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>취소</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>재부팅 및 리셋</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>로그 가져오기가 성공하였습니다 .</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>로그를 내보내기 할 수 없습니다 . USB 스틱이 안전하게 삽입되었는지 확인하고 다시 시도하세요 .</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1버전 %2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>스타트 오버</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>기본값로 리셋</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>업그레이드 방법 선택:</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>USB 플래시 드라이브에 저장된 파일로부터 업그레이드합니다.</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>네트워크</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>네트워크 상에서 웹서버로부터 업그레이드를 다운로드합니다.</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>업그레이드 서버의 주소를 입력하세요 :</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>서버 주소</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>업그레이드 서버에 접속하는데 필요하다면 프록시 서버의 주소를 입력하세요 :</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>종류</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>프록시 주소</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>연결</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>사용할 수 있는 업그레이드를 위해 서버를 질의합니다.</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>이전</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>다음</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>업그레이드 시작</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>다운그레이드 시작</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>찾은 업그레이드가 없습니다</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>업그레이드가 실패했습니다</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>업그레이드를 하면 실행되는 테스트가 종료될 것입니다. 프로세스 완료시 테스트 세트는 업그레이드를 완료하기 위해 재부팅할 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>업그레이드 확인</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>테스트 세트를 다운그레이드 할 수 있지만, 추천하지는 않습니다. 이 작업이 필요하다면 Viavi TAC로 전화하셔서 문의하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>다운그레이드를 하면 실행되는 테스트가 종료될 것입니다. 프로세스 완료시 테스트 세트는 다운그레이드를 완료하기 위해 재부팅할 것입니다.</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>다운그레이드는 추천하지 않습니다.</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>알려지지 않은 에러가 발생했습니다. 다시 시도하세요.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>시스템</translation>
        </message>
    </context>
</TS>
