<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>TextLabel</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Zoom in</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Overlay</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analyzing...</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tip</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Auto-center</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Test Button:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Testler</translation>
        </message>
        <message utf8="true">
            <source>Freezes</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Other settings...</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>scxgui::ImportProfilesDialog</name>
        <message utf8="true">
            <source>Import microscope profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Import&#xA;Profile</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Microscope profiles (*.pro)</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>scxgui::microscope</name>
        <message utf8="true">
            <source>Test</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Live</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save PNG</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save PDF</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Image</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Save Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Analysis failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Could not analyze the fiber. Please check that the live video shows the fiber end and that it is focused before testing again.</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seç</translation>
        </message>
    </context>
    <context>
        <name>HTMLGen</name>
        <message utf8="true">
            <source>Fiber Inspection and Test Report</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Fiber Information</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>No extra fiber information defined</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Profile:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Tip:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Inspection Summary</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>DEFECTS</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>SCRATCHES</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>or</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Criteria</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Result</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>any</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Failed generating inspection summary.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>LOW MAGNIFICATION</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>HIGH MAGNIFICATION</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Scratch testing not enabled</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>scxgui::SavePdfReportDialog</name>
        <message utf8="true">
            <source>Company:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Technician:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Customer:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Location:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Job:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Cable:</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Connector:</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
