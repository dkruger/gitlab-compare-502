<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>Archivos</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Almacenamiento desmontable</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>No se detectaron dispositivos.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>Dando formato a este dispositivo USB, todos los datos existentes se borrarán. Esto incluye todos los archivos y particiones.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Expulsar</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Buscar...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Si</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NINGUNO</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>SI</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NO</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Se solicitó par Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>Especifique el PIN para el emparejamiento</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>Par</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>Petición de emparejamiento</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>Petición de emparejamiento desde:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>Para emparejar con el dispositivo, asegúrese de que el siguiente código coincide con el código de dicho dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>Emparejamiento iniciado</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>Petición de emparejamiento enviada a:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>Iniciar exploración</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>Detener exploración</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Parámetros</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Activar Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>Permitir que otros dispositivos se emparejen con este</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>Nombre del dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>Aplicación móvil habilitada</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Red</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>Modo IP</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Estática</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>Dirección MAC</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>Dirección IP</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Máscara de subred</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Puerta de enlace.  </translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>Servidor DNS</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>Modo IPv6</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Deshabilitado</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manual</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>Dirección IPv6</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Long prefijo Subred</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>Servidor DNS</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Dirección Local Enlace</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Dirección sin estado</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Dirección con estado</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Módulos no cargados</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>Presente</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>No presente</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Iniciando</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Activando</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Inicializando</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Listo</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Explorando</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Deshabilitando</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Deteniendo</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>Asociando.""</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>Asociado</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>Activar adaptador inalámbrico</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>Conectado</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Desconectado</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Fallado</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>Conecte a WPA Enterprise Network</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>Nombre de red</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>método de autenticación externo</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>Método de autentificación interna</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>Nombre de usuario</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>Identidad Anónima</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Contraseña</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>Certificados</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>Contraseña privada</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>Conectar a la red personal WPA</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>Frase secreta</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>No se encontró un dispositivo inalámbrico USB.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Estado</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>Detener conexión</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>Olvidar red</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>No se pudo conectar a la red.</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>Servicio 3G </translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>Habilitando modem</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>Modem habilitado</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>Modem deshabilitado</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>No se detecta modem </translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>Error: el modem no responde</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>Error: el controlador de dispositivo no se ha iniciado</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>Error: no se ha podido configurar el modem</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>Atención: el modem no responde. Se está reiniciando el modem...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>Error: no se ha insertado SIM</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>Error: acceso a la red denegado</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Conectar</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Desconectar</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Conectando</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>Desconectando</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>No está conectado</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>Conexión fallida</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>Configuración de Modem </translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>Habilitar modem</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>No se encuentra o se ha habilitado ningún dispositivo de modem.</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>Network APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>Estado de conexión</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>Compruebe APN y vuelva a intentarlo.</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Pri DNS Server</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Sec DNS Server</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Proxy y seguridad</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>Tipo de proxy</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Servidor proxy</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Seguridad de red</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>Desactivar FTP, telnet y auto StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>Control de la alimentación</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>Está conectado a la alimentación de CA</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Operando con batería</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>No se detectó una batería</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>Se desconectó el cargador por su consumo de energía</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Cargando batería</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>La batería está totalmente cargada</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>La batería no carga</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>No se puede cargar la batería por falla de energía</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>Estado desconocido de la batería</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>La batería está demasiado caliente y no cargará</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>La batería está demasiado caliente. Enchufe la alimentación de CA</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>La batería está demasiado caliente. No desenchufe la alimentación de CA</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>La batería está demasiado caliente. La desconexión de la alimentación de CA forzará una parada</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>La batería está demasiado fría y no cargará</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>La batería está en peligro de sobrecalentamiento</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>La temperatura de la batería es normal</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>Activar desconexión automático al funcionar con batería</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>Tiempo de inactividad (minutos)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Fecha y hora</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Zona horaria</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Región</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>África</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>América</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Antártica</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Asia</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Océano Atlántico</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Australia</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Europa</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Océano Índico</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Océano Pacífico</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>País</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ninguno</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Afganistán</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Islas Åland</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Albania</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Argelia</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Samoa Americana</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Andorra</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Angola</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Anguila</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Antigua y Barbuda</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Argentina</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Armenia</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Aruba</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Austria</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Azerbaiyán</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Bahamas</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Bahréin</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Bangladesh</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Barbados</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Belarús</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Bélgica</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Belice</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Benin</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Bermuda</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Bhután</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Bolivia</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Bosnia y Herzegovina</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Botswana</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Isla Bouvet</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Brasil</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>Territorio Británico del Océano Índico</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Brunei Darussalam</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Bulgaria</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Burkina Faso</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Burundi</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Camboya</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>Camerún</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Canadá</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Cabo Verde</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Las Islas Caimán</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>República Centroafricana</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Chad</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Chile</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>China</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Isla de Navidad</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Islas Cocos (Keeling)</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Colombia</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Comoras</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Congo</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>Congo, República Democrática del</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Islas Cook</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Costa Rica</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Côte d'Ivoire</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Croacia</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Cuba</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Chipre</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>República Checa</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Dinamarca</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Djibouti</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Dominica</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>República Dominicana</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Ecuador</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Egipto</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>El Salvador</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Guinea Ecuatorial</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Eritrea</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Estonia</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Etiopía</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Islas Malvinas (Falkland)</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Islas Feroe</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Fiji</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Finlandia</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>Francia</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Guayana Francesa</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Polinesia francesa</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Territorios Australes Franceses</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Gabón</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Gambia</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Georgia</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Alemania</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Ghana</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Gibraltar</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Grecia</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Groenlandia</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Granada</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>Guadalupe</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Guam</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Guatemala</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>Guernsey</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Guinea</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Guinea-Bissau</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Guyana</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Haití</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Islas Heard y McDonald</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Honduras</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Hong Kong</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Hungría</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>Islandia</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>India</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Indonesia</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>Irán</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Iraq</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>Irlanda</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Isla de Man</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>Israel</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>Italia</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Jamaica</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Japón</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Jersey</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Jordania</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Kazajstán</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Kenia</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Kiribati</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Corea , República Popular Democrática de</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Corea del Sur</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Kuwait</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Kirguistán</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>República Democrática Popular Lao</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Letonia</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Líbano</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Lesoto</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Liberia</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Libia</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Liechtenstein</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Lituania</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Luxemburgo</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Macao</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>Macedonia, Antigua República Yugoslava de</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Madagascar</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Malawi</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Malasia</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>Maldivas</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Malí</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Malta</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Las Islas Marshall</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Martinica</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Mauritania</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>República de Mauricio</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Mayotte</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>México</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Micronesia , Estados Federados de</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>Moldova, República de</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>Mónaco</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Mongolia</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Montenegro</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Monserrate</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Marruecos</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Mozambique</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Myanmar</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Namibia</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Nauru</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Nepal</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Holanda</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Antillas Holandesas</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>Nueva Caledonia</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>Nueva Zelanda</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Nicaragua</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Níger</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Nigeria</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Niue</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Isla Norfolk</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Islas Marianas del Norte</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Noruega</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Omán</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Pakistán</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Palau</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Territorio Palestino</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Panamá</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Papua Nueva Guinea</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Paraguay</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Perú</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Filipinas</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>Pitcairn</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Polonia</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Portugal</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Puerto Rico</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Katar</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>La Reunión</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Rumania</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>FEDERACIÓN RUSA</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Ruanda</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>San Bartolomé</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Santa Elena , Ascensión y Tristán da Cunha</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>Saint Kitts y Nevis</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>Santa Lucía</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>San Martín</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>San Pedro y Miquelón</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>San Vicente y las Granadinas</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Samoa</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>San Marino</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>Santo Tomé y Príncipe</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Arabia Saudita</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Senegal</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Serbia</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Seychelles</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Sierra Leona</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Singapur</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Eslovaquia</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Eslovenia</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Islas Solomon</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Somalia</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>Sur África</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Georgia del Sur e Islas Sandwich del Sur</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>España</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Sri Lanka</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Sudán</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Suriname</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Svalbard y Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Swazilandia</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>Suecia</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>Suiza</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>República Arábica Siria</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Taiwán</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Tajikistan</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>Tanzania, República Unida de</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Tailandia</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Timor-Leste</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Togo</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Tokelau</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Tonga</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Trinidad y Tobago</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Tunisia</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Turquía</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Turkmenistán</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Islas Turcas y Caicos</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Tuvalu</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Uganda</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Ucrania</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Emiratos Árabes Unidos</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Reino Unido</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>Estados Unidos</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>Islas Menores Alejadas de los EE.UU.</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Uruguay</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Uzbekistán</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Vanuatu</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Ciudad del Vaticano</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>Venezuela</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Vietnam</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Islas Vírgenes Británicas</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Islas Vírgenes , EE.UU.</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Wallis y Futuna</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Sahara Occidental</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Yemen</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Zambia</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Zimbabwe</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Área</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Casey</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Davis</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Dumont d'Urville</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Mawson</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>McMurdo</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Palmer</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Rothera</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Polo Sur</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Syowa</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Vostok</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Territorio Capital de Australia</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>Norte</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>Nueva Gales del Sur</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Queensland</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>Sur</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Tasmania</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Victoria</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>Oeste</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Brasilia</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Brasilia - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Brasilia + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Alaska</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>Arizona</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Atlántico</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>Central</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>Oriental</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Hawai</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Montaña</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>Terranova</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Pacífico</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Saskatchewan</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Isla de Pascua</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Kinshasa</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Lubumbashi</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Galápagos</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Gambier</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Marquesas</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Tahití</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>Occidental</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Danmarkshavn</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>Este</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Islas Fénix</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Islas de la Línea</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Islas Gilbert</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>Noroeste</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Kosrae</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Truk</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Azores</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Madeira</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Irkutsk</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Kaliningrado</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Krasnoyarsk</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Magadan</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Moscú</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Omsk</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Vladivostok</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Yakutsk</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Ekaterimburgo</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Islas Canarias</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Svalbard</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Johnston</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Midway</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Despertar</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Ajusta automáticamente el horario de verano</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Fecha y hora actual</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 horas</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 horas</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>Configure reloj con NTP</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>verdadero</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>falso</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>Use el formato de 24 horas</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN NTP Server</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi-Fi NTP Server</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>Servidor NTP 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>Servidor NTP 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Fecha</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (Inglés)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (Alemán)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (Francés)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (Chino simplificado)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (Japonés)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 (Coreano)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (Ruso)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (Portugués)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (Italiano)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk (Turco)</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Idioma:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Cambiar formato del estándar:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Muestras para formato seleccionado:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personalizar</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Mostrar</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Brillo</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Protector de pantallas</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Activar protector automático de pantalla</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Mensaje</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retardo</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Contraseña del protector de pantalla</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Calibrar pantalla táctil ...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>Activar acceso VNC</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>Conmutar acceso VNC o protección con contraseña desconectará las conexiones existentes.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Contraseña de acceso remoto</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>Esta contraseña se utiliza para todos los accesos remotos, como vnc, ftp, ssh.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>Hubo un error iniciando VNC.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>Requiere contraseña para acceso VNV</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Smart Access Anywhere</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>Código de acceso</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>Dirección IP LAN</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Dirección IP de Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>Número de conexiones VNC</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Actualización</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>No ha sido posible actualizar porque la alimentación de CA está desconectada. Enchufe la alimentación de CA y vuelva a intentarlo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>No se pudo detectar una memoria USB. Por favor, confirme que esté insertada con seguridad e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>No se pudo actualizar.  Por favor, especifique el servidor de actualización e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>La conexión de red no está disponible. Por favor, verifique la conexión de red e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>No se puede hacer contacto con el servidor de actualización. Por favor, verifique la dirección del servidor e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>Se encontró un error buscando actualizaciones disponibles.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>No ha sido posible ejecutar la actualización porque en la actualización seleccionada faltan archivos o contiene archivos dañados.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>No ha sido posible ejecutar la actualización porque la actualización seleccionada contiene archivos dañados.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>No se pudieron generar los datos de actualización USB.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>No hay actualizaciones disponibles, por favor, revise sus configuraciones e inténtelo de nuevo.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>Falló el intento de actualización, por favor, inténtelo de nuevo. Si el problema persiste, póngase en contacto con su representante de ventas.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Bloqueo del instrumento de prueba</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Bloquear el instrumento de prueba evita un acceso no autorizado.  Se muestra el salva pantallas para asegurar la pantalla.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Bloquear el instrumento de prueba</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>No se ha establecido la contraseña requerida.  Sin ella los usuarios podrán desbloquear el instrumento de prueba.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Configuraciones de contraseña</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Estas configuraciones se comparten con el protector de pantallas.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Requiere contraseña</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Audio</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Volumen del parlante</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Silenciar</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Volumen del micrófono</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>Buscando satélites</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>Latitud</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>Longitud</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>Altitud</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>Timestamp</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>Número de satélites fijos</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>Media SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>Información de Satélite Individual</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Información del sistema</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Estableciendo conexión con el servidor...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Conexión establecida...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Descargando archivos...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Cargando archivos...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Descargando información de actualización...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Cargando archivo de registro...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Sincronizado con éxito con el servidor</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>Papelera de entrada. Esperando a añadirse al inventario...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>No se ha podido sincronizar con el servidor.</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Conexión perdida durante la sincronización. Compruebe la configuración de red.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>No se ha podido sincronizar con el servidor. El servidor está ocupado.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>No se ha podido sincronizar con el servidor. Se ha detectado un error del sistema.</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>No se ha podido establecer una conexión. Compruebe la configuración de red o ID de cuenta.</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Sincronización abortada.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Configuración de datos completa</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Informes completos.</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Introducir ID de cuenta</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Iniciar sincronización</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Detener sincronización</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Dirección del servidor</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>ID de cuenta</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID del técnico</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuración</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Informe</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opción</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Reiniciar a por defecto</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Reproductor de vídeo</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Explorador Web</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Depurar</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Dispositivo serie","</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Permitir a Getty controlar el dispositivo en serie</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>Contenidos de /root/debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Pruebas</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Configurar prueba Screening</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>Administrador de tareas</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>Información del trabajo</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nombre del Cliente</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>Número de trabajo</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Ubicación de la Prueba</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>La información del trabajo se agrega automáticamente a los informes de pruebas.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>Historia</translation>
        </message>
    </context>
</TS>
