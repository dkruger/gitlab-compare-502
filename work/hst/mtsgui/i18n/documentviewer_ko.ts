<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>나가기</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>로딩 </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>PDF를 로딩하는데 실패했습니다</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>페이지를 렌더링하는데 실패했습니다: </translation>
        </message>
    </context>
</TS>
