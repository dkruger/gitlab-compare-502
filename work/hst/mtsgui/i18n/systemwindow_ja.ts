<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>ﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>ﾘﾑｰﾊﾞﾌﾞﾙ ｽﾄﾚｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>ﾃﾞﾊﾞｲｽが検出されませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>初期化</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>この USB ﾃﾞﾊﾞｲｽをﾌｫｰﾏｯﾄすると、すべての既存のﾃﾞｰﾀが消去されます。これには、すべてのﾌｧｲﾙおよびﾊﾟｰﾃｨｼｮﾝが含まれます。</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>取り出し</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>ﾌﾞﾗｳｽﾞ...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>いいえ</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NO</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Bluetooth ﾍﾟｱﾘﾝｸﾞが要求されています</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>ﾍﾟｱﾘﾝｸﾞ用の PIN を入力してください</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>ﾍﾟｱ</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>ﾍﾟｱﾘﾝｸﾞ要求</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>ﾍﾟｱﾘﾝｸﾞ要求元:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>ﾃﾞﾊﾞｲｽとのﾍﾟｱﾘﾝｸﾞを行う場合は、そのﾃﾞﾊﾞｲｽのｺｰﾄﾞと下記のｺｰﾄﾞが一致していることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>ﾍﾟｱﾘﾝｸﾞが開始されました</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>ﾍﾟｱﾘﾝｸﾞ要求先:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>ｽｷｬﾝの開始</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>ｽｷｬﾝの停止</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>設定 </translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Bluetooth を有効にする</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>このﾃﾞﾊﾞｲｽとのﾍﾟｱﾘﾝｸﾞを可能にする</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>ﾃﾞﾊﾞｲｽ名</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>ﾓﾊﾞｲﾙ ｱﾌﾟﾘ有効</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>ﾈｯﾄﾜｰｸ</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>IP ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>固定</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>ｻﾌﾞﾈｯﾄ ﾏｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>ｹﾞｰﾄｳｪｲ</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>DNS ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>IPv6 ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>無効</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>手動</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>ｵｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>IPv6 ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>ｻﾌﾞﾈｯﾄ ﾌﾚﾌｨｸｽ長</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>DNS ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>ﾘﾝｸ - ﾛｰｶﾙ ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>ｽﾃｰﾄﾚｽ ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>ｽﾃｰﾄﾌﾙ ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>ﾓｼﾞｭｰﾙが読み込まれていません</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>存在</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>存在しない</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>開始中</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>有効化中</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>初期化中</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>準備完了</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>ｽｷｬﾝ中</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>無効化中</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>停止中</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>関連付け中</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>関連付け済み</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>ﾜｲﾔﾚｽ ｱﾀﾞﾌﾟﾀーを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>接続完了</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>切断されました</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>WPA ｴﾝﾀｰﾌﾟﾗｲﾂﾞ ﾈｯﾄﾜｰｸへの接続</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>ﾈｯﾄﾜｰｸ名</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>外部認証方法</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>内部認証方法</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>ﾕｰｻﾞー名</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>匿名 ID</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>ﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>証明書</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>ﾌﾟﾗｲﾍﾞｰﾄ ｷｰ ﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>WPA 個人用ﾈｯﾄﾜｰｸへの接続</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>ﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>USB ﾜｲﾔﾚｽ ﾃﾞﾊﾞｲｽは見つかりませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>状態</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>接続を停止</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>ネットワークの削除</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>ﾈｯﾄﾜｰｸに接続できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>3G ｻｰﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>ﾓﾃﾞﾑを有効にしています</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>ﾓﾃﾞﾑが有効になりました</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>ﾓﾃﾞﾑが無効になりました</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>ﾓﾃﾞﾑが検出されませんでした</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>ｴﾗｰ - ﾓﾃﾞﾑが応答していません。</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>ｴﾗｰ - ﾃﾞﾊﾞｲｽ ｺﾝﾄﾛｰﾗーが起動していません。</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>ｴﾗｰ - ﾓﾃﾞﾑを構成できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>警告 - ﾓﾃﾞﾑが応答していません。ﾓﾃﾞﾑがﾘｾｯﾄされています ...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>ｴﾗｰ - SIM が挿入されていません。</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>ｴﾗｰ - ﾈｯﾄﾜｰｸ登録が拒否されました。</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>接続</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>切断</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>接続中</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>切断しています</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>接続されていません</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>接続に失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>ﾓﾃﾞﾑのｾｯﾄｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>ﾓﾃﾞﾑを有効にする</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>ﾓﾃﾞﾑ ﾃﾞﾊﾞｲｽが見つからないか、有効になっていません。</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>ﾈｯﾄﾜｰｸ APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>接続の状態</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>APN を確認してから再試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Pri DNS ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>ｾｶﾝﾀﾞﾘ DNS ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>ﾌﾟﾛｷｼとｾｷｭﾘﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>ﾌﾟﾛｷｼ</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>ﾌﾟﾛｷｼの種類</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>ﾌﾟﾛｷｼ ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>ﾈｯﾄﾜｰｸ ｾｷｭﾘﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>FTP 、 Telnet 、自動 StrataSync を無効にする</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>電源管理</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>AC 電源が接続されています</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>ﾊﾞｯﾃﾘー電力で動作中</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>ﾊﾞｯﾃﾘーが検出されませんでした</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>電力消費により、充電はされていません</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>ﾊﾞｯﾃﾘーを充電中</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>ﾊﾞｯﾃﾘは完全に充電されました</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>ﾊﾞｯﾃﾘーは充電されていません</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>電力切断によりﾊﾞｯﾃﾘーに充電できません</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>不明のﾊﾞｯﾃﾘー状態</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎることにより、充電されていない可能性があります</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎます&#xA;AC 電源を接続してください</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎます&#xA;AC 電源の接続を外さないでください。</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>ﾊﾞｯﾃﾘーの温度が高すぎます&#xA;AC 電源を外すと、強制的にｼｬｯﾄ ﾀﾞｳﾝされます</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>ﾊﾞｯﾃﾘーの温度が低すぎることにより、充電されていない可能性があります</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>ﾊﾞｯﾃﾘーが過熱している危険があります</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>ﾊﾞｯﾃﾘーの温度は正常です</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>充電</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>ﾊﾞｯﾃﾘー使用中の自動ｵﾌを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>非ｱｸﾃｨﾌﾞ ﾀｲﾑ (分)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>日付と時刻</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>ﾀｲﾑ ｿﾞｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>地域</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>ｱﾌﾘｶ</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>南北ｱﾒﾘｶ</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>南極</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>ｱｼﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>大西洋</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>ｵｰｽﾄﾗﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>ﾖｰﾛｯﾊﾟ</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>ｲﾝﾄﾞ洋</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>太平洋</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>国</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>ｱﾌｶﾞﾆｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>ｵｰﾗﾝﾄﾞ諸島</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>ｱﾙﾊﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>ｱﾙｼﾞｪﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>米領ｻﾓｱ</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>ｱﾝﾄﾞﾗ</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>ｱﾝｺﾞﾗ</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>ｱﾝｷﾞﾗ</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>ｱﾝﾃｨｸﾞｱ･ﾊﾞｰﾌﾞｰﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>ｱﾙｽﾞﾝﾁﾝ</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>ｱﾙﾒﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>ｱﾙﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>ｵｰｽﾄﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>ｱｽﾞﾙﾊﾞｲｼﾞｬﾝ</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>ﾊﾞﾊﾏ</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>ﾊﾞｰﾚｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>ﾊﾞﾝｸﾞﾗﾃﾞｼｭ</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>ﾊﾞﾙﾊﾞﾄﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>ﾍﾞﾗﾙｰｼ</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>ﾍﾞﾙｷﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>ﾍﾞﾘｰﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>ﾍﾞﾆﾝ</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>ﾊﾞﾐｭｰﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>ﾌﾞｰﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>ﾎﾞﾘﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>ﾎﾞｽﾆｱ･ﾍﾙﾂｪｺﾞﾋﾞﾅ</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>ﾎﾞﾂﾜﾅ</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>ﾌﾞｰﾍﾞ島</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>ﾌﾞﾗｼﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>英領ｲﾝﾄﾞ洋地域</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>ﾌﾞﾙﾈｲ･ﾀﾞﾙｻﾗｰﾑ国</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>ﾌﾞﾙｶﾞﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>ﾌﾞﾙｷﾅﾌｧｿ</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>ﾌﾞﾙﾝｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>ｶﾝﾎﾞｼﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>ｶﾒﾙｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>ｶﾅﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>ｶｰﾎﾞﾍﾞﾙﾃﾞ</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>ｹｲﾏﾝ諸島</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>中央ｱﾌﾘｶ共和国</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>ﾁｬﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>ﾁﾘ</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>中国</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>ｸﾘｽﾏｽ島</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>ｺｺｽ諸島</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>ｺﾛﾝﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>ｺﾓﾛ諸島</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>ｺﾝｺﾞ</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>ｺﾝｺﾞ民主共和国</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>ｸｯｸ諸島</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>ｺｽﾀﾘｶ</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>ｺｰﾄｼﾞﾎﾞｱｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>ｸﾛｱﾁｱ</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>ｷｭｰﾊﾞ</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>ｷﾌﾟﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>ﾁｪｺ共和国</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>ﾃﾞﾝﾏｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>ｼﾞﾌﾞﾁ</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>ﾄﾞﾐﾆｶ</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>ﾄﾞﾐﾆｶ共和国</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>ｴｸｱﾄﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>ｴｼﾞﾌﾟﾄ</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>ｴﾙｻﾙﾊﾞﾄﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>赤道ｷﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>ｴﾘﾄﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>ｴｽﾄﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>ｴﾁｵﾋﾟｱ</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>ﾌｫｰｸﾗﾝﾄﾞ (ﾏﾙﾋﾞﾅｽ) 諸島</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>ﾌｪﾛー諸島</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>ﾌｨｼﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>ﾌｨﾝﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>ﾌﾗﾝｽ</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>仏領ｷﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>仏領ﾎﾟﾘﾈｼｱ</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>仏領極南諸島</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>ｶﾞﾎﾞﾝ</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>ｶﾞﾝﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>ｸﾞﾙｼﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>ﾄﾞｲﾂ</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>ｶﾞｰﾅ</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>ｼﾞﾌﾞﾗﾙﾀﾙ</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>ｷﾞﾘｼｬ</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>ｸﾞﾘｰﾝﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>ｸﾞﾚﾅﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>ｸﾞｱﾄﾞﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>ｸﾞｱﾑ</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>ｸﾞｱﾃﾏﾗ</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>ｶﾞｰﾝｼﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>ｷﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>ｷﾞﾆｱﾋﾞｻｳ</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>ｶﾞｲｱﾅ</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>ﾊｲﾁ</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>ﾊｰﾄﾞ･ﾏｸﾄﾞﾅﾙﾄﾞ諸島</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>ﾎﾝｼﾞｭﾗｽ</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>香港</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>ﾊﾝｶﾞﾘｰ</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>ｱｲｽﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>ｲﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>ｲﾝﾄﾞﾈｼｱ</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>ｲﾗﾝ</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>ｲﾗｸ</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>ｱｲﾙﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>ﾏﾝ島</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>ｲｽﾗｴﾙ</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>ｲﾀﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>ｼﾞｬﾏｲｶ</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>日本</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>ｼﾞｬｰｼﾞー島</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>ﾖﾙﾀﾞﾝ</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>ｶｻﾞﾌｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>ｹﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>ｷﾘﾊﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>朝鮮民主主義人民共和国</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>大韓民国</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>ｸｳｪｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>ｷﾙｷﾞｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>ﾗｵｽ人民民主共和国</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>ﾗﾄﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>ﾚﾊﾞﾉﾝ</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>ﾚｿﾄ</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>ﾘﾍﾞﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>ﾘﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>ﾘﾋﾃﾝｼｭﾀｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>ﾘﾄｱﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>ﾙｸｾﾝﾌﾞﾙｸ</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>ﾏｶｵ</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>ﾏｹﾄﾞﾆｱ旧ﾕｰｺﾞｽﾗﾋﾞｱ共和国</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>ﾏﾀﾞｶﾞｽｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>ﾏﾗｳｲ</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>ﾏﾚｰｼｱ</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>ﾓﾙﾃﾞｨﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>ﾏﾘ</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>ﾏﾙﾀ</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>ﾏｰｼｬﾙ諸島</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>ﾏﾙﾁﾆｰｸ島</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>ﾓｰﾘﾀﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>ﾓｰﾘｼｬｽ</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>ﾏｲﾖｯﾄ島</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>ﾒｷｼｺ</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>ﾐｸﾛﾈｼｱ連邦</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>ﾓﾙﾄﾞﾊﾞ共和国</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>ﾓﾅｺ</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>ﾓﾝｺﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>ﾓﾝﾃﾈｸﾞﾛ</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>ﾓﾝｾﾗｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>ﾓﾛｯｺ</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>ﾓｻﾞﾝﾋﾞｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>ﾐｬﾝﾏｰ</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>ﾅﾐﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>ﾅｳﾙ</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>ﾈﾊﾟｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>ｵﾗﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>ｵﾗﾝﾀﾞ領ｱﾝﾃｨﾙ</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>ﾆｭｰｶﾚﾄﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>ﾆｭｰｼﾞｰﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>ﾆｶﾗｸﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>ﾆｼﾞｪｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>ﾅｲｼﾞｪﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>ﾆｳｴ</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>ﾉｰﾌｫｰｸ島</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>北ﾏﾘｱﾅ諸島</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>ﾉﾙｳｪｰ</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>ｵﾏｰﾝ</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>ﾊﾟｷｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>ﾊﾟﾗｵ</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>ﾊﾟﾚｽﾁﾅ自治区</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>ﾊﾟﾅﾏ</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>ﾊﾟﾌﾟｱﾆｭｰｷﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>ﾊﾟﾗｸﾞｱｲ</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>ﾍﾟﾙｰ</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>ﾌｨﾘﾋﾟﾝ</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>ﾋﾟﾄｹｱﾝ</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>ﾎﾟｰﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>ﾎﾟﾙﾄｶﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>ﾌﾟｴﾙﾄﾘｺ</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>ｶﾀｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>ﾚﾕﾆｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>ﾙｰﾏﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>ﾛｼｱ連邦</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>ﾙﾜﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>ｻﾝ･ﾊﾞﾙﾃﾙﾐー島</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>ｾﾝﾄﾍﾚﾅ、ｱｾﾝｼｮﾝおよびﾄﾘｽﾀﾝﾀﾞｸｰﾆｬ</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>ｾﾝﾄｸﾘｽﾄﾌｧｰ･ﾈｲﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>ｾﾝﾄﾙｼｱ</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>ｻﾝﾏﾙﾀﾝ島</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>ｻﾝﾋﾟｴｰﾙ･ﾐｸﾛﾝ</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>ｾﾝﾄﾋﾞﾝｾﾝﾄおよびｸﾞﾚﾅﾃﾞｨｰﾝ諸島</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>ｻﾓｱ</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>ｻﾝﾏﾘﾉ</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>ｻﾝﾄﾒ･ﾌﾟﾘﾝｼﾍﾟ</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>ｻｳｼﾞｱﾗﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>ｾﾈｶﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>ｾﾙﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>ｾｰｼｪﾙ</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>ｼｴﾗﾚｵﾈ</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>ｼﾝｶﾞﾎﾟｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>ｽﾛﾊﾞｷｱ</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>ｽﾛﾍﾞﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>ｿﾛﾓﾝ諸島</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>ｿﾏﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>南ｱﾌﾘｶ</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>ｻｳｽｼﾞｮｰｼﾞｱ･ｻｳｽｻﾝﾄﾞｳｨｯﾁ諸島</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>ｽﾍﾟｲﾝ</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>ｽﾘﾗﾝｶ</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>ｽｰﾀﾞﾝ</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>ｽﾘﾅﾑ</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>ｽﾊﾞｰﾙﾊﾞﾙ諸島･ﾔﾝﾏｲｴﾝ島</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>ｽﾜｼﾞﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>ｽｳｪｰﾃﾞﾝ</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>ｽｲｽ</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>ｼﾘｱ･ｱﾗﾌﾞ共和国</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>台湾</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>ﾀｼﾞｷｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>ﾀﾝｻﾞﾆｱ連合共和国</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>ﾀｲ</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>東ﾃｨﾓｰﾙ</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>ﾄｰｺﾞ</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>ﾄｹﾗｳ諸島</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>ﾄﾝｶﾞ</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>ﾄﾘﾆﾀﾞｰﾄﾞ･ﾄﾊﾞｺﾞ</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>ﾁｭﾆｼﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>ﾄﾙｺ</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>ﾄﾙｸﾒﾆｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>ﾀｰｸｽ諸島･ｶｲｺｽ諸島</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>ﾂﾊﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>ｳｶﾞﾝﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>ｳｸﾗｲﾅ</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>ｱﾗﾌﾞ首長国連邦</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>ｲｷﾞﾘｽ</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>ｱﾒﾘｶ</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>その他の米領諸島</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>ｳﾙｸﾞｱｲ</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>ｳﾂﾞﾍﾞｷｽﾀﾝ</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>ﾊﾞﾇｱﾂ</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>ﾊﾞﾁｶﾝ市国</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>ﾍﾞﾈﾂﾞｴﾗ</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>ﾍﾞﾄﾅﾑ</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>英領ﾊﾞｰｼﾞﾝ諸島</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>米領ﾊﾞｰｼﾞﾝ諸島</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>ﾜﾘｽ･ﾌﾃｭﾅ諸島</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>西ｻﾊﾗ</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>ｲｴﾒﾝ</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>ｻﾞﾝﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>ｼﾞﾝﾊﾞﾌﾞｴ</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>地域</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>ｹｰｼｰ</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>ﾃﾞｲﾋﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>ﾃﾞｭﾓﾝ･ﾃﾞｭﾙﾋﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>ﾓｰｿﾝ</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>ﾏｸﾏｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>ﾊﾟｰﾏｰ</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>ﾛｽﾞﾗ</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>南極</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>昭和</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>ﾎﾞｽﾄｰｸ</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>ｵｰｽﾄﾗﾘｱ首都特別区</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>北</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>ﾆｭｰｻｳｽｳｪｰﾙﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>ｸｲｰﾝﾂﾞﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>南</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>ﾀｽﾏﾆｱ</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>ﾋﾞｸﾄﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>西</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>ﾌﾞﾗｼﾞﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>ﾌﾞﾗｼﾞﾘｱ - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>ﾌﾞﾗｼﾞﾘｱ + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>ｱﾗｽｶ</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>ｱﾘｿﾞﾅ</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>大西洋</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>中部</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>東部</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>ﾊﾜｲ</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>山岳部</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>ﾆｭｰﾌｧﾝﾄﾞﾗﾝﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>太平洋</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>ｻｽｶﾁｪﾜﾝ</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>ｲｰｽﾀー島</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>ｷﾝｼｬｻ</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>ﾙﾌﾞﾝﾊﾞｼ</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>ｶﾞﾗﾊﾟｺﾞｽ</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>ｶﾞﾝﾋﾞｱ</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>ﾏﾙｷｰﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>ﾀﾋﾁ</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>西部</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>ﾃﾞﾝﾏｰｸｼｬｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>東</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>ﾌｪﾆｯｸｽ諸島</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>ﾗｲﾝ諸島</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>ｷﾞﾙﾊﾞｰﾄ諸島</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>北西</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>ｺｽﾗｴ島</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>ﾁｭｰｸ諸島</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>ｱｿﾞﾚｽ諸島</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>ﾏﾃﾞｲﾗ</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>ｲﾙｸｰﾂｸ</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>ｶﾘｰﾆﾝｸﾞﾗｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>ｸﾗｽﾉﾔﾙｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>ﾏｶﾞﾀﾞﾝ</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>ﾓｽｸﾜ</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>ｵﾑｽｸ</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>ｳﾗｼﾞｵｽﾄｸ</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>ﾔｸｰﾂｸ</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>ｴｶﾃﾘﾝﾌﾞﾙｸ</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>ｶﾅﾘｱ諸島</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>ｽﾊﾞｰﾙﾊﾞﾙ諸島</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>ﾔﾝﾏｲｴﾝ島</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>ｼﾞｮﾝｽﾄﾝ島</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>ﾐｯﾄﾞｳｪｲ島</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>ｳｪｰｸ</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>自動的に夏時間を調整する</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>現在の日付と時間</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 時間</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 時間</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>NTP を使用してｸﾛｯｸを設定</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>24 時間表記を使用する</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>LAN NTP ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Wi-Fi NTP ｻｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>NTP サーバー 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>NTP サーバー 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (英語)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (ﾄﾞｲﾂ語)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Fran ç ais ( ﾌﾗﾝｽ語 )</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (簡体字中国語)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (日本語)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 ( 韓国語 )</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (ﾛｼｱ語)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Portugu ê s ( ﾎﾟﾙﾄｶﾞﾙ語 )</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (ｲﾀﾘｱ語)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk ( ﾄﾙｺ語 )</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>言語:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>標準の書式を変更する:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>選択した書式ののｻﾝﾌﾟﾙ:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>ｶｽﾀﾏｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>画面</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>明るさ</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>ｽｸﾘｰﾝ ｾｰﾊﾞｰ</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>自動的にｽｸﾘｰﾝｾｰﾊﾞｰを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>ﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>待ち時間</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>ｽｸﾘｰﾝ ｾｰﾊﾞーのﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>ﾀｯﾁｽｸﾘｰﾝの調整...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>VNC ｱｸｾｽを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>VNC ｱｸｾｽあるいはﾊﾟｽﾜｰﾄﾞ保護を切り替えると、既存の接続が切断されます。</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>ﾘﾓｰﾄ ｱｸｾｽ ﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>このﾊﾟｽﾜｰﾄﾞはすべてのﾘﾓｰﾄ ｱｸｾｽ (例: vnc、ftp、ssh) に使用されます。</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>VNC を開始するときにｴﾗーが発生しました。</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>VNC ｱｸｾｽにﾊﾟｽﾜｰﾄﾞを設定</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Smart Access Anywhere</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>ｱｸｾｽ ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>LAN IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Wi-Fi IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>VNC 接続数</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>AC 電源が接続されておらず、ｱｯﾌﾟｸﾞﾚｰﾄﾞできません。AC 電源を接続して、再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>USB ﾌﾗｯｼｭ ﾃﾞﾊﾞｲｽを検出できません。USB ﾌﾗｯｼｭ ﾃﾞﾊﾞｲｽが確実に挿入されていることを確認し、再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞできません。ｱｯﾌﾟｸﾞﾚｰﾄﾞ ｻｰﾊﾞーを指定して、再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>ﾈｯﾄﾜｰｸ接続が使用できません。ﾈｯﾄﾜｰｸ接続を確認して、再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ ｻｰﾊﾞーと通信できません。ｻｰﾊﾞｰ ｱﾄﾞﾚｽを確認して、再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>使用可能なｱｯﾌﾟｸﾞﾚｰﾄﾞを探している際にｴﾗーが発生しました。</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞできません。選択したｱｯﾌﾟｸﾞﾚｰﾄﾞのﾌｧｲﾙが欠落または破損しています。</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞできません。選択したｱｯﾌﾟｸﾞﾚｰﾄﾞのﾌｧｲﾙが破損しています。</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>USB ｱｯﾌﾟｸﾞﾚｰﾄﾞ ﾃﾞｰﾀの生成ができませんでした。</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞが見つかりません。設定を確認して、再度、試行してください。</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞできませんでした。再度、試行してください。問題が解決しない場合は、代理店に問い合わせてください。</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>ﾃｽﾄ ｾｯﾄのﾛｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>ﾃｽﾄ ｾｯﾄをﾛｯｸすることにより、許可されていないｱｸｾｽを防ぐことができます。&#xA;ｾｷｭﾘﾃｨの観点から、画面にｽｸﾘｰﾝ ｾｰﾊﾞーが表示されます。</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>ﾃｽﾄ ｾｯﾄのﾛｯｸ</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>必要なﾊﾟｽﾜｰﾄﾞが設定されていません。ﾕｰｻﾞーは、ﾊﾟｽﾜｰﾄﾞを入力せずにﾃｽﾄ ｾｯﾄをﾛｯｸ解除することができます。</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>ﾊﾟｽﾜｰﾄﾞ設定</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>これらの設定は、ｽｸﾘｰﾝ ｾｰﾊﾞーと共有されます。</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>ﾊﾟｽﾜｰﾄﾞが必要</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>ｵｰﾃﾞｨｵ</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>ｽﾋﾟｰｶー音量</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>ﾐｭｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>ﾏｲｸ音量</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>衛星の探索</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>緯度</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>経度</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>高度</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>ﾀｲﾑｽﾀﾝﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>測位中の衛星の数</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>平均 SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>個別の衛星情報</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>ｼｽﾃﾑ情報</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>ｻｰﾊﾞーとの接続を確立しています ...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>接続が確立されました ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>ﾌｧｲﾙをﾀﾞｳﾝﾛｰﾄﾞしています ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>ﾌｧｲﾙをｱｯﾌﾟﾛｰﾄﾞしています ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>ｱｯﾌﾟｸﾞﾚｰﾄﾞ情報をﾀﾞｳﾝﾛｰﾄﾞしています ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>ﾛｸﾞ ﾌｧｲﾙをｱｯﾌﾟﾛｰﾄﾞしています ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>ｻｰﾊﾞーとの同期に成功しました。</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>保留中の bin 内にあります。 ｲﾝﾍﾞﾝﾄﾘに追加されるまでお待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>ｻｰﾊﾞーとの同期に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>同期中に接続が切断されました。 ﾈｯﾄﾜｰｸ構成を確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>ｻｰﾊﾞーとの同期に失敗しました。 ｻｰﾊﾞーがﾋﾞｼﾞーです。</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>ｻｰﾊﾞーとの同期に失敗しました。 ｼｽﾃﾑ ｴﾗーが検出されました。</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>接続の確立に失敗しました。 ﾈｯﾄﾜｰｸ構成またはｱｶｳﾝﾄ ID を確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>同期が中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>構成ﾃﾞｰﾀが完了しました。</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>ﾚﾎﾟｰﾄが完了しました。</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>ｱｶｳﾝﾄ ID を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>同期の開始</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>同期の停止</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>ｻｰﾊﾞーのｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>ｱｶｳﾝﾄ ID</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>技術者 ID</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>ｵﾌﾟｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>ﾃﾞﾌｫﾙﾄに戻す</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>ﾋﾞﾃﾞｵ ﾌﾟﾚｰﾔｰ</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Web ﾌﾞﾗｳｻﾞ</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>ﾃﾞﾊﾞｯｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>ｼﾘｱﾙ ﾃﾞﾊﾞｲｽ</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Getty でｼﾘｱﾙ ﾃﾞﾊﾞｲｽを制御できるようにする</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>/root/debug.txt のｺﾝﾃﾝﾂ</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>ｽｸﾘｰﾆﾝｸﾞ ﾃｽﾄのｾｯﾄ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>ジョブ マネージャー</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>ジョブ情報</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>ジョブ番号</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>ジョブ情報はテスト レポートに自動的に追加されます。</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>履歴</translation>
        </message>
    </context>
</TS>
