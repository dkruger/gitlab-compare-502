<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CDocViewerMainWin</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Выход</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CPdfDocumentViewer</name>
        <message utf8="true">
            <source>Loading: </source>
            <translation>Загрузка : </translation>
        </message>
        <message utf8="true">
            <source>Failed to load PDF</source>
            <translation>Ошибка загрузки файла PDF</translation>
        </message>
        <message utf8="true">
            <source>Failed to render page: </source>
            <translation>Ошибка при отображении страницы : </translation>
        </message>
    </context>
</TS>
