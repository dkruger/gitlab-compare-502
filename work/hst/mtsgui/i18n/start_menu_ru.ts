<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Логотип отчета</translation>
        </message>
        <message utf8="true">
            <source>Device Under Test</source>
            <translation>Испытываемое устройство</translation>
        </message>
        <message utf8="true">
            <source>Owner</source>
            <translation>Владелец</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>Software Revision</source>
            <translation>Версия программного обеспечения</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Серийный номер</translation>
        </message>
        <message utf8="true">
            <source>Comment</source>
            <translation>Комментарий</translation>
        </message>
        <message utf8="true">
            <source>Tester Comments</source>
            <translation>Комментарии испытателя</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Результаты испытаний</translation>
        </message>
        <message utf8="true">
            <source>Tester</source>
            <translation>Испытатель</translation>
        </message>
        <message utf8="true">
            <source>Test Instrument</source>
            <translation>Средство испытания</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>Модель</translation>
        </message>
        <message utf8="true">
            <source>Viavi -BASE_MODEL- -MODULE_NAME-</source>
            <translation>Viavi -BASE_MODEL- -MODULE_NAME-</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Версия ПО</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SOFTWARE_VERSION-</source>
            <translation>-MODULE_SOFTWARE_VERSION-</translation>
        </message>
        <message utf8="true">
            <source>BERT Serial Number</source>
            <translation>Серийный номер устройства BERT</translation>
        </message>
        <message utf8="true">
            <source>-MODULE_SERIAL-</source>
            <translation>-MODULE_SERIAL-</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom</source>
            <translation>HS Datacom</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT</source>
            <translation>Модуль BERT передачи данных HS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Terminate</source>
            <translation>Заверш.</translation>
        </message>
        <message utf8="true">
            <source>HS Datacom BERT Term</source>
            <translation>Заверш. модул. BERT передачи данных HS</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Diphase</source>
            <translation>Двухфазн.</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT</source>
            <translation>Модуль BERT двухфазных сигналов</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Diphase BERT Term</source>
            <translation>Заверш. модул. BERT двухфазных сигналов</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1/DS3</source>
            <translation>DS1/DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1</source>
            <translation>DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT</source>
            <translation>DS1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Term</source>
            <translation>Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Thru</source>
            <translation>Сквозн.</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Thru</source>
            <translation>Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Single Monitor</source>
            <translation>Одиночн. монитор</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Single Mon</source>
            <translation>Одиночн. монитор модуля BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Monitor</source>
            <translation>Сдвоенный монитор</translation>
        </message>
        <message utf8="true">
            <source>DS1 BERT Dual Mon</source>
            <translation>Сдвоен. монитор модуля BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander</source>
            <translation>Отклонение DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Wander Single Mon</source>
            <translation>DS1 Wander Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling</source>
            <translation>Сигнализация DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Term</source>
            <translation>Заверш. сигнализации DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Signaling Dual Mon</source>
            <translation>Сдвоен. монитор сигнализации DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI</source>
            <translation>DS1 ISDN PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Term</source>
            <translation>Заверш. PRI ISDN в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 ISDN PRI Dual Mon</source>
            <translation>Сдвоен. монитор PRI ISDN в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF</source>
            <translation>DS1 VF</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Term</source>
            <translation>Заверш. модул. VF в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 VF Dual Mon</source>
            <translation>Сдвоен. монитор модуля VF в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC</source>
            <translation>DS1 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 HDLC Dual Mon</source>
            <translation>Сдвоен. монитор модуля HDLC в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP</source>
            <translation>DS1 PPP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Выдать команду Ping</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 PPP Ping Term</source>
            <translation>Время выполнения  проверки связи DS1 PPP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 Jitter</source>
            <translation>джиттера DS1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Jitter Term</source>
            <translation>Заверш. джиттера BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Rx Jitter Term</source>
            <translation>DS1 BERT Заверш. джиттера в Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS1 BERT Wander Term</source>
            <translation>Заверш. отклонения BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3</source>
            <translation>DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT</source>
            <translation>DS3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Term</source>
            <translation>Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Thru</source>
            <translation>Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Single Mon</source>
            <translation>Одиночн. монитор модуля BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Dual Mon</source>
            <translation>Сдвоен. монитор модуля BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT</source>
            <translation>E1 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Term</source>
            <translation>DS3 : Заверш. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Thru</source>
            <translation>DS3 : Сквозн. режим BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Dual Mon</source>
            <translation>DS3 : Сдвоен. монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Term</source>
            <translation>DS3 : Заверш. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Thru</source>
            <translation>DS3 : Сквозн. режим BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Dual Mon</source>
            <translation>DS3 : Сдвоен. монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC</source>
            <translation>DS3 HDLC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 HDLC Dual Mon</source>
            <translation>Сдвоен. монитор модуля HDLC в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Jitter</source>
            <translation>джиттера DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Jitter Term</source>
            <translation>Заверш. джиттера BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Rx Jitter Term</source>
            <translation>DS3 BERT Заверш. джиттера в Rx</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Jitter Term</source>
            <translation>DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 BERT Rx Jitter Term</source>
            <translation>DS3 : E1 BERT Rx Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Jitter Term</source>
            <translation>DS3 : Заверш. джиттера BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Rx Jitter Term</source>
            <translation>DS3 : DS1 BERT Rx Jitter Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 Wander</source>
            <translation>Отклонение DS3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>DS3 BERT Wander Term</source>
            <translation>Заверш. отклонения BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : E1 Wander Term</source>
            <translation>DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>DS3 : DS1 BERT Wander Term</source>
            <translation>DS3 : Заверш. отклонения BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1/E3/E4</source>
            <translation>E1/E3/E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1</source>
            <translation>E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Term</source>
            <translation>Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Thru</source>
            <translation>Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Single Mon</source>
            <translation>Одиночн. монитор модуля BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Dual Mon</source>
            <translation>Сдвоен. монитор модуля BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander</source>
            <translation>Отклонение E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 Wander Single Mon</source>
            <translation>E1 Wander Single Mon</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI</source>
            <translation>E1 ISDN PRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 ISDN PRI Term</source>
            <translation>E1 ISDN PRI Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 Jitter</source>
            <translation>джиттера E1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Jitter Term</source>
            <translation>Заверш. джиттера BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E1 BERT Wander Term</source>
            <translation>Заверш. отклонения BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3</source>
            <translation>E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT</source>
            <translation>E3 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Term</source>
            <translation>Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Thru</source>
            <translation>Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor</source>
            <translation>Монитор.</translation>
        </message>
        <message utf8="true">
            <source>E3 BERT Mon</source>
            <translation>Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Term</source>
            <translation>E3 : Заверш. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Thru</source>
            <translation>E3 : Сквозн. режим BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Mon</source>
            <translation>E3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Jitter</source>
            <translation>джиттера E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Jitter Term</source>
            <translation>Заверш. джиттера BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Jitter Term</source>
            <translation>E3 : Заверш. джиттера BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 Wander</source>
            <translation>Отклонение E3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E3 BERT Wander Term</source>
            <translation>Заверш. отклонения BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E3 : E1 BERT Wander Term</source>
            <translation>E3 : Заверш. отклонения BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4</source>
            <translation>E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT</source>
            <translation>E4 BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Term</source>
            <translation>Заверш. модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Thru</source>
            <translation>Сквозн. режим модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Mon</source>
            <translation>Монитор BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Term</source>
            <translation>E4 : Заверш. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Thru</source>
            <translation>E4 : Сквозн. режим BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Mon</source>
            <translation>E4 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Term</source>
            <translation>E4 : Заверш. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Thru</source>
            <translation>E4 : Сквозн. режим BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Mon</source>
            <translation>E4 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Jitter</source>
            <translation>джиттера E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Jitter Term</source>
            <translation>Заверш. джиттера BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Jitter Term</source>
            <translation>E4 : Заверш. джиттера BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Jitter Term</source>
            <translation>E4 : Заверш. джиттера BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 Wander</source>
            <translation>Отклонение E4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>E4 BERT Wander Term</source>
            <translation>Заверш. отклонения BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E3 BERT Wander Term</source>
            <translation>E4 : Заверш. отклонения BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>E4 : E1 BERT Wander Term</source>
            <translation>E4 : Заверш. отклонения BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SONET</source>
            <translation>SONET</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>J-Scan</source>
            <translation>J-Scan</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 J-Scan</source>
            <translation>J-Scan в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Bulk BERT</source>
            <translation>Массов. BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Drop+Insert</source>
            <translation>Удаление и вставка</translation>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT Dual Mon</source>
            <translation>Сдвоенн. монитор массов. BERT в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Term</source>
            <translation>Заверш. BERT DS3 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Thru</source>
            <translation>Сквозн. режим модул. BERT в DS3 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Mon</source>
            <translation>Монитор BERT в DS3 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в DS3 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Term</source>
            <translation>STS-1 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Thru</source>
            <translation>STS-1 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 BERT Mon</source>
            <translation>STS-1 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Term</source>
            <translation>STS-1 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Thru</source>
            <translation>STS-1 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 BERT Mon</source>
            <translation>STS-1 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VT-1.5 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VT-1.5 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Single Mon</source>
            <translation>Одиночн. монитор массов. BERT в VT-1.5 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk BERT Dual Mon</source>
            <translation>Сдвоен. монитор массов. BERT в VT-1.5 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Term</source>
            <translation>STS-1 VT-1.5 Заверш. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Thru</source>
            <translation>STS-1 VT-1.5 Сквозн. режим BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Single Mon</source>
            <translation>Одиночн. монитор модуля BERT в DS1 VT-1.5 STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 BERT Dual Mon</source>
            <translation>STS-1 VT-1.5 Сдвоен. монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Jitter</source>
            <translation>джиттера STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Jitter Term</source>
            <translation>Заверш. джиттера DS3 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Jitter Term</source>
            <translation>STS-1 DS3 Заверш. джиттера DS1 в :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Jitter Term</source>
            <translation>STS-1 DS3 Заверш. джиттера E1 в :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VT-1.5 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Jitter Term</source>
            <translation>STS-1 VT-1.5 Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Wander</source>
            <translation>Отклонение STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 Wander Term</source>
            <translation>Заверш. отклонения DS3 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : DS1 Wander Term</source>
            <translation>STS-1 DS3 Заверш. отклонения DS1 в :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 DS3 : E1 Wander Term</source>
            <translation>STS-1 DS3 Заверш. отклонения E1 в :</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VT-1.5 в STS-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 VT-1.5 DS1 Wander Term</source>
            <translation>STS-1 VT-1.5 Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3</source>
            <translation>OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 J-Scan</source>
            <translation>J-Scan в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c Bulk BERT</source>
            <translation>Массов. BERT в STS-3c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-3c в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-3c в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-3c в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Term</source>
            <translation>OC-3 STS-1 Заверш. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Thru</source>
            <translation>OC-3 STS-1 Сквозн. режим BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Mon</source>
            <translation>OC-3 STS-1 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в DS3 в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-3 STS-1 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-3 STS-1 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-3 STS-1 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-3 STS-1 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VT-1.5 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VT-1.5 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VT-1.5 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Term</source>
            <translation>OC-3 VT-1.5 Заверш. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-3 VT-1.5 Сквозн. режим BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-3 VT-1.5 Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-3c-Xv</source>
            <translation>STS-3c-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Term</source>
            <translation>Заверш. BERT STS-3c-Xv в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-3c-Xv в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>GFP Ethernet</source>
            <translation>GFP Ethernet</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Traffic</source>
            <translation>Трафик уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-3 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-3 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traffic</source>
            <translation>Трафик уровня 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-3 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-3 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Ping</source>
            <translation>Команда Ping уровня 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-3 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-3 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1-Xv</source>
            <translation>STS-1-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Term</source>
            <translation>Заверш. BERT STS-1-Xv в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-1-Xv в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-3 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-3 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-3 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-3 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-3 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-3 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VT-1.5-Xv</source>
            <translation>VT-1.5-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Term</source>
            <translation>Заверш. BERT VT-1.5-Xv в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv BERT Mon</source>
            <translation>Монитор BERT в VT-1.5-Xv в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-3 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-3 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-3 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-3 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-3 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-3 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Jitter</source>
            <translation>джиттера OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-3c в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Jitter Term</source>
            <translation>OC-3 STS-1 Заверш. джиттера в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-3 STS-1 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VT-1.5 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-3 VT-1.5 Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 Wander</source>
            <translation>Отклонение OC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-3c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-3c в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-1 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 Wander Term</source>
            <translation>OC-3 STS-1 Заверш. отклонен. в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-3 STS-1 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-3 STS-1 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VT-1.5 в OC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-3 VT-1.5 DS1 Wander Term</source>
            <translation>OC-3 VT-1.5 Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12</source>
            <translation>OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 J-Scan</source>
            <translation>J-Scan в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-12c Bulk BERT</source>
            <translation>Массов. BERT в STS-12c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-12c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-12c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-12c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-3c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-3c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-3c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Term</source>
            <translation>OC-12 STS-1 Заверш. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Thru</source>
            <translation>OC-12 STS-1 Сквозн. режим BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Mon</source>
            <translation>OC-12 STS-1 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в DS3 в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-12 STS-1 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-12 STS-1 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-12 STS-1 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-12 STS-1 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VT-1.5 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VT-1.5 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VT-1.5 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Term</source>
            <translation>OC-12 VT-1.5 Заверш. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-12 VT-1.5 Сквозн. режим BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-12 VT-1.5 Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Term</source>
            <translation>Заверш. BERT STS-3c-Xv в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-3c-Xv в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-12 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-12 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-12 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-12 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-12 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-12 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Term</source>
            <translation>Заверш. BERT STS-1-Xv в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-1-Xv в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-12 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-12 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-12 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-12 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-12 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-12 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Term</source>
            <translation>Заверш. BERT VT-1.5-Xv в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv BERT Mon</source>
            <translation>Монитор BERT в VT-1.5-Xv в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-12 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-12 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-12 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-12 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-12 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-12 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Jitter</source>
            <translation>джиттера OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-12c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-3c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Jitter Term</source>
            <translation>OC-12 STS-1 Заверш. джиттера в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-12 STS-1 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VT-1.5 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-12 VT-1.5 Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 Wander</source>
            <translation>Отклонение OC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-12c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-12c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-3c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-3c в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-1 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 Wander Term</source>
            <translation>OC-12 STS-1 Заверш. отклонен. в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-12 STS-1 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-12 STS-1 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VT-1.5 в OC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-12 VT-1.5 DS1 Wander Term</source>
            <translation>OC-12 VT-1.5 Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48</source>
            <translation>OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 J-Scan</source>
            <translation>J-Scan в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48c Bulk BERT</source>
            <translation>Массов. BERT в STS-48c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-48c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-48c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-48c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-48c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-12c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-12c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-12c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-3c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-3c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-3c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Term</source>
            <translation>OC-48 STS-1 Заверш. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Thru</source>
            <translation>OC-48 STS-1 Сквозн. режим BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Mon</source>
            <translation>OC-48 STS-1 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в DS3 в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-48 STS-1 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-48 STS-1 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-48 STS-1 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-48 STS-1 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VT-1.5 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VT-1.5 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VT-1.5 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Term</source>
            <translation>OC-48 VT-1.5 Заверш. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-48 VT-1.5 Сквозн. режим BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-48 VT-1.5 Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Term</source>
            <translation>Заверш. BERT STS-3c-Xv в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-3c-Xv в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-48 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-48 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-48 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-48 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-48 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-48 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Term</source>
            <translation>Заверш. BERT STS-1-Xv в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-1-Xv в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-48 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-48 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-48 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-48 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-48 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-48 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Term</source>
            <translation>Заверш. BERT VT-1.5-Xv в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv BERT Mon</source>
            <translation>Монитор BERT в VT-1.5-Xv в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-48 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-48 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-48 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-48 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-48 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-48 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Jitter</source>
            <translation>джиттера OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-48c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-12c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-3c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Jitter Term</source>
            <translation>OC-48 STS-1 Заверш. джиттера в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Jitter Term</source>
            <translation>OC-48 STS-1 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VT-1.5 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Jitter Term</source>
            <translation>OC-48 VT-1.5 Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 Wander</source>
            <translation>Отклонение OC-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-48c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-48c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-12c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-12c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-3c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-3c в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в STS-1 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 Wander Term</source>
            <translation>OC-48 STS-1 Заверш. отклонен. в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : DS1 Wander Term</source>
            <translation>OC-48 STS-1 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 STS-1 DS3 : E1 Wander Term</source>
            <translation>OC-48 STS-1 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VT-1.5 в OC-48</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-48 VT-1.5 DS1 Wander Term</source>
            <translation>OC-48 VT-1.5 Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192</source>
            <translation>OC-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 J-Scan</source>
            <translation>J-Scan в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192c Bulk BERT</source>
            <translation>Массов. BERT в STS-192c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-192c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-192c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-192c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-192c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-192c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-48c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-48c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-48c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-48c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-48c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-12c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-12c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-12c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-12c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-3c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-3c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-3c в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-1 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-1 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в STS-1 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Term</source>
            <translation>OC-192 STS-1 Заверш. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Thru</source>
            <translation>OC-192 STS-1 Сквозн. режим BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Mon</source>
            <translation>OC-192 STS-1 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в DS3 в STS-1 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Term</source>
            <translation>OC-192 STS-1 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : DS1 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Term</source>
            <translation>OC-192 STS-1 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Thru</source>
            <translation>OC-192 STS-1 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1 DS3 : E1 BERT Mon</source>
            <translation>OC-192 STS-1 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VT-1.5 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VT-1.5 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VT-1.5 в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Term</source>
            <translation>OC-192 VT-1.5 Заверш. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Thru</source>
            <translation>OC-192 VT-1.5 Сквозн. режим BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5 DS1 BERT Mon</source>
            <translation>OC-192 VT-1.5 Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Term</source>
            <translation>Заверш. BERT STS-3c-Xv в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-3c-Xv в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-192 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-192 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-192 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-192 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-192 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-3c-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-192 STS-3c-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Term</source>
            <translation>Заверш. BERT STS-1-Xv в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv BERT Mon</source>
            <translation>Монитор BERT в STS-1-Xv в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-192 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-192 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-192 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-192 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-192 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 STS-1-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-192 STS-1-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Term</source>
            <translation>Заверш. BERT VT-1.5-Xv в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv BERT Mon</source>
            <translation>Монитор BERT в VT-1.5-Xv в OC-192</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OC-192 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 OC-192 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OC-192 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 OC-192 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 OC-192 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-192 VT-1.5-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 OC-192 VT-1.5-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768</source>
            <translation>OC-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Самопроверка оптики</translation>
        </message>
        <message utf8="true">
            <source>OC-768 Optics Self-Test</source>
            <translation>Самопроверка оптики в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL BERT</source>
            <translation>STL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STL в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STL Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STL в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768c Bulk BERT</source>
            <translation>Массов. BERT в STS-768c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-768c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-768c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-768c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-768c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-192c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-192c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-192c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-192c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-48c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-48c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-48c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-48c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-12c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-12c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-12c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-3c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-3c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-3c в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-1 Bulk BERT</source>
            <translation>Массов. BERT в STS-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в STS-1 в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OC-768 STS-1 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в STS-1 в OC-768</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SDH</source>
            <translation>SDH</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e</source>
            <translation>STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e J-Scan</source>
            <translation>J-Scan в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-4</source>
            <translation>AU-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>VC-4</source>
            <translation>VC-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 Заверш. модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 Сквозн. режим модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 Монитор BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>STM-1e AU-4 VC-4 Удаление и вставка модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-4 E4 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-1e AU-4 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-1e AU-4 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-1e AU-4 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-3 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 Удаление и вставка модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-1e AU-3 VC-3 Удаление и вставка модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-1e AU-3 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-1e AU-3 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-1e AU-3 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Jitter</source>
            <translation>джиттера STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Jitter Term</source>
            <translation>Заверш. джиттера массов. BERT в VC-4 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Заверш. джиттера BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Заверш. джиттера BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Jitter Term</source>
            <translation>Заверш. джиттера массов. BERT в VC-3 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Заверш. джиттера BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Заверш. джиттера BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E3 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : Заверш. джиттера BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Jitter Term</source>
            <translation>Заверш. джиттера массов. BERT в VC-12 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Jitter Term</source>
            <translation>STM-1e AU-4 VC-12 E1 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Jitter Term</source>
            <translation>Заверш. джиттера массов. BERT в VC-3 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Заверш. джиттера BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Заверш. джиттера BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E3 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : Заверш. джиттера BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Jitter Term</source>
            <translation>Заверш. джиттера массов. BERT в VC-12 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Jitter Term</source>
            <translation>STM-1e AU-3 VC-12 E1 Заверш. джиттера в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e Wander</source>
            <translation>Отклонение STM-1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 Bulk BERT Wander Term</source>
            <translation>Заверш. отклонен. массов. BERT в VC-4 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Заверш. отклонения BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-4 E4 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-4 E4 : Заверш. отклонения BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 Bulk BERT Wander Term</source>
            <translation>Заверш. отклонен. массов. BERT в VC-3 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Заверш. отклонения BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 DS3 : Заверш. отклонения BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E3 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-3 E1 : Заверш. отклонения BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 Bulk BERT Wander Term</source>
            <translation>Заверш. отклонен. массов. BERT в VC-12 в AU-4 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-4 VC-12 E1 BERT Wander Term</source>
            <translation>STM-1e AU-4 VC-12 E1 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 Bulk BERT Wander Term</source>
            <translation>Заверш. отклонен. массов. BERT в VC-3 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Заверш. отклонения BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 DS3 : DS1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 DS3 : Заверш. отклонения BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E3 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-3 E3 : E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-3 E1 : Заверш. отклонения BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 Bulk BERT Wander Term</source>
            <translation>Заверш. отклонен. массов. BERT в VC-12 в AU-3 в STM-1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1e AU-3 VC-12 E1 BERT Wander Term</source>
            <translation>STM-1e AU-3 VC-12 E1 Заверш. отклонен. в BERT</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1</source>
            <translation>STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 J-Scan</source>
            <translation>J-Scan в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 Заверш. модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 Сквозн. режим модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 Монитор BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-4 E4 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-1 AU-4 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-1 AU-4 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-1 AU-4 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-Xv</source>
            <translation>VC-4-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-1 AU-4 Заверш. BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-1 AU-4 Монитор BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-1 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-1 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-1 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-1 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-1 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-1 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3-Xv</source>
            <translation>VC-3-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-4 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-4 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-1 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-1 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-1 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-1 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-1 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-1 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-12-Xv</source>
            <translation>VC-12-Xv</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-4 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-4 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-1 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-1 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-1 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-1 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-1 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-1 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-3 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 Удаление и вставка модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-1 AU-3 VC-3 Удаление и вставка модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-1 AU-3 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-1 AU-3 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-1 AU-3 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-1 AU-3 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-1 AU-3 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-1 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-1 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-1 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-1 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-1 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-1 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-1 AU-3 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-1 AU-3 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-1 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-1 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-1 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-1 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-1 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-1 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Jitter</source>
            <translation>джиттера STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-1 AU-4 Заверш. джиттера E4 в VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-3 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-4 Заверш. джиттера DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-4 Заверш. джиттера E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-12 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-4 Заверш. джиттера E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-3 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-1 AU-3 Заверш. джиттера DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-1 AU-3 Заверш. джиттера E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-12 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-1 AU-3 Заверш. джиттера E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 Wander</source>
            <translation>Отклонение STM-1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-4 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-1 AU-4 Заверш. отклонения E4 в VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-4 E4 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-3 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-4 Заверш. отклонения DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-4 Заверш. отклонения E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-4 VC-3 E1 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-12 в AU-4 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-4 Заверш. отклонения E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-3 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-1 AU-3 Заверш. отклонения DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-1 AU-3 Заверш. отклонения E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-1 AU-3 VC-3 E1 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-12 в AU-3 в STM-1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-1 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-1 AU-3 Заверш. отклонения E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4</source>
            <translation>STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 J-Scan</source>
            <translation>J-Scan в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-4c Bulk BERT</source>
            <translation>Массов. BERT в VC-4-4c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-4c в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-4c в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-4c в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4-4c в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 Заверш. модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 Сквозн. режим модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 Монитор BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-4 E4 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-4 AU-4 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-4 AU-4 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-4 AU-4 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-4 AU-4 Заверш. BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-4 AU-4 Монитор BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-4 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-4 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-4 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-4 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-4 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-4 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-4 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-4 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-4 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-4 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-4 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-4 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-4 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-4 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-4 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-4 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-4 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-4 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-4 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-4 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-4 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-4 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-3 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 Удаление и вставка модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-4 AU-3 VC-3 Удаление и вставка модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-4 AU-3 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-4 AU-3 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-4 AU-3 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-4 AU-3 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-4 AU-3 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-4 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-4 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-4 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-4 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-4 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-4 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-4 AU-3 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-4 AU-3 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-4 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-4 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-4 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-4 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-4 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-4 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Jitter</source>
            <translation>джиттера STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-4-4c в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-4 AU-4 Заверш. джиттера E4 в VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-3 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-4 Заверш. джиттера DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-4 Заверш. джиттера E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-12 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-4 Заверш. джиттера E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-3 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-4 AU-3 Заверш. джиттера DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-4 AU-3 Заверш. джиттера E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-12 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-4 AU-3 Заверш. джиттера E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 Wander</source>
            <translation>Отклонение STM-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-4-4c в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-4 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-4 AU-4 Заверш. отклонения E4 в VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-4 E4 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-3 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-4 Заверш. отклонения DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-4 Заверш. отклонения E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-4 VC-3 E1 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-12 в AU-4 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-4 Заверш. отклонения E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-3 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-4 AU-3 Заверш. отклонения DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-4 AU-3 Заверш. отклонения E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-4 AU-3 VC-3 E1 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-12 в AU-3 в STM-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-4 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-4 AU-3 Заверш. отклонения E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16</source>
            <translation>STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 J-Scan</source>
            <translation>J-Scan в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-16c Bulk BERT</source>
            <translation>Массов. BERT в VC-4-16c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-16c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-16c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-16c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4-16c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-4c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-4c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-4c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4-4c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 Заверш. модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 Сквозн. режим модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 Монитор BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-4 E4 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-16 AU-4 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-16 AU-4 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-16 AU-4 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-16 AU-4 Заверш. BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-16 AU-4 Монитор BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-16 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-16 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-16 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-16 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-16 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-16 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-4 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-4 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-16 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-16 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-16 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-16 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-16 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-16 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-4 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-4 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-16 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-16 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-16 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-16 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-16 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-16 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-3 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 Удаление и вставка модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-16 AU-3 VC-3 Удаление и вставка модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-16 AU-3 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-16 AU-3 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-16 AU-3 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-16 AU-3 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-16 AU-3 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-16 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-16 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-16 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-16 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-16 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-16 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-16 AU-3 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-16 AU-3 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-16 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-16 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-16 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-16 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-16 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-16 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Jitter</source>
            <translation>джиттера STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Jitter Term</source>
            <translation>STM-16 AU-4 Заверш. джиттера в VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-4-4c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Jitter Term</source>
            <translation>STM-16 AU-4 Заверш. джиттера E4 в VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-3 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-4 Заверш. джиттера DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-4 Заверш. джиттера E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-12 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-4 Заверш. джиттера E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-3 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Jitter Term</source>
            <translation>STM-16 AU-3 Заверш. джиттера DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Заверш. джиттера в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Заверш. джиттера в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Jitter Term</source>
            <translation>STM-16 AU-3 Заверш. джиттера E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Jitter Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : Заверш. джиттера в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Jitter Term</source>
            <translation>Заверш. массов. джиттера в VC-12 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Jitter Term</source>
            <translation>STM-16 AU-3 Заверш. джиттера E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 Wander</source>
            <translation>Отклонение STM-16</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-16c Wander Term</source>
            <translation>STM-16 AU-4 Заверш. отклонен. в VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4-4c Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-4-4c в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-4 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 Wander Term</source>
            <translation>STM-16 AU-4 Заверш. отклонения E4 в VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E3 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-4 E4 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-4 E4 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-3 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-4 Заверш. отклонения DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-4 Заверш. отклонения E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-4 VC-3 E1 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-12 в AU-4 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-4 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-4 Заверш. отклонения E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-3 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 Wander Term</source>
            <translation>STM-16 AU-3 Заверш. отклонения DS3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Заверш. отклонен. в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 DS3 : DS1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 DS3 : Заверш. отклонен. в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 Wander Term</source>
            <translation>STM-16 AU-3 Заверш. отклонения E3 в VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-3 E3 : E1 Wander Term</source>
            <translation>STM-16 AU-3 VC-3 E1 : Заверш. отклонен. в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 Bulk Wander Term</source>
            <translation>Заверш. массов. отклонения в VC-12 в AU-3 в STM-16</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-16 AU-3 VC-12 E1 Wander Term</source>
            <translation>STM-16 AU-3 Заверш. отклонения E1 в VC-12</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64</source>
            <translation>STM-64</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 J-Scan</source>
            <translation>J-Scan в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-64c Bulk BERT</source>
            <translation>Массов. BERT в VC-4-64c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-64c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-64c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-64c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-64c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4-64c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-16c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-16c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-16c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-16c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4-16c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-4c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-4c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-4c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-4c Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4-4c в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-4 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 Заверш. модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 Сквозн. режим модул. BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 Монитор BERT в E4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 BERT Drop+Insert</source>
            <translation>Удаление и вставка модул. BERT в VC-4 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-4 E4 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-4 E4 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4 E4 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-4 E4 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-4 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Term</source>
            <translation>STM-64 AU-4 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Thru</source>
            <translation>STM-64 AU-4 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12 E1 BERT Mon</source>
            <translation>STM-64 AU-4 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Term</source>
            <translation>STM-64 AU-4 Заверш. BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv BERT Mon</source>
            <translation>STM-64 AU-4 Монитор BERT в VC-4-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-64 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-64 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-64 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-64 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-64 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-4-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-64 AU-4 VC-4-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-4 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-4 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-64 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-64 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-64 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-64 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-64 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-64 AU-4 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-4 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-4 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-64 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-64 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-64 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-64 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-64 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-4 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-64 AU-4 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 Bulk BERT Drop+Insert</source>
            <translation>Удаление и вставка массов. BERT в VC-3 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 Заверш. модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 Сквозн. режим модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 Монитор BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 Удаление и вставка модул. BERT в DS3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Заверш. модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Сквозн. режим модул. BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 DS3 : DS1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 DS3 : Монитор BERT в DS1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 BERT Drop+Insert</source>
            <translation>STM-64 AU-3 VC-3 Удаление и вставка модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-3 E1 : Заверш. модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-3 E1 : Сквозн. режим модул. BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3 E3 : E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-3 E1 : Монитор BERT в E3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-12 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-12 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-12 в AU-3 в STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Term</source>
            <translation>STM-64 AU-3 VC-12 Заверш. модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Thru</source>
            <translation>STM-64 AU-3 VC-12 Сквозн. режим модул. BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12 E1 BERT Mon</source>
            <translation>STM-64 AU-3 VC-12 Монитор BERT в E1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Term</source>
            <translation>STM-64 AU-3 Заверш. BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv BERT Mon</source>
            <translation>STM-64 AU-3 Монитор BERT в VC-3-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-64 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-64 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-64 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-64 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-64 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-3-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-64 AU-3 VC-3-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Term</source>
            <translation>STM-64 AU-3 Заверш. BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv BERT Mon</source>
            <translation>STM-64 AU-3 Монитор BERT в VC-12-Xv</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 STM-64 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 2 Traffic Mon</source>
            <translation>Монитор трафика уровня 2 STM-64 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 STM-64 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traffic Mon</source>
            <translation>Монитор трафика уровня 3 STM-64 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Ping</source>
            <translation>Команда Ping уровня 3 STM-64 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-64 AU-3 VC-12-Xv GFP Layer 3 Traceroute</source>
            <translation>Контрол. прохожд. сигнала уровня 3 STM-64 AU-3 VC-12-Xv GFP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256</source>
            <translation>STM-256</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 Optics Self-Test</source>
            <translation>Самопроверка оптики в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STL Bert</source>
            <translation>STL Bert</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Term</source>
            <translation>Заверш . BERT в STL STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 STL BERT Mon</source>
            <translation>Монитор BERT в STL в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4-256c Bulk BERT</source>
            <translation>Массов. BERT в VC-4-256c</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-256c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-256c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-256c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-256c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-64c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-64c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-64c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-64c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-16c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-16c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-16c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-16c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4-4c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4-4c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4-4c Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4-4c в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-4 Bulk BERT</source>
            <translation>Массов. BERT в VC-4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-4 в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-4 в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-4 VC-4 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-4 в AU-4 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VC-3 Bulk BERT</source>
            <translation>Массов. BERT в VC-3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в VC-3 в AU-3 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Thru</source>
            <translation>Сквозн. режим массов. BERT в VC-3 в AU-3 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STM-256 AU-3 VC-3 Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в VC-3 в AU-3 в STM-256</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1G</source>
            <translation>10/100/1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth TrueSAM</source>
            <translation>Устройство распределения по профилям TrueSAM в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>L2 Traffic</source>
            <translation>Трафик L2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic QuickCheck</source>
            <translation>10/100/1000 L2 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic</source>
            <translation>Трафик L3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic QuickCheck</source>
            <translation>10/100/1000 L3 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic RFC 2544</source>
            <translation>10/100/1000 L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv4</source>
            <translation>Трафик L3 IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 RFC 2544</source>
            <translation>10/100/1000 L3 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Traffic IPv6</source>
            <translation>Трафик L3 IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 RFC 2544</source>
            <translation>10/100/1000 L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv4</source>
            <translation>Трафик L4 IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv4 RFC 2544</source>
            <translation>10/100/1000 L4 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 Traffic IPv6</source>
            <translation>Трафик L4 IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 Traffic IPv6 RFC 2544</source>
            <translation>10/100/1000 L4 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Traffic SAMComplete</source>
            <translation>10/100/1000 L2 трафик SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L2 Multiple Streams</source>
            <translation>Несколько потоков L2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L2 Streams SAMComplete</source>
            <translation>10/100/1000 L2 потоки SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv4 SAMComplete</source>
            <translation>10/100/1000 L3 Протокол IPv4 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Traffic IPv6 SAMComplete</source>
            <translation>10/100/1000 L3 Протокол IPv6 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv4</source>
            <translation>L3 множественные потоки IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv4 SAMComplete</source>
            <translation>10/100/1000 L3 потоки IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L3 Multiple Streams IPv6</source>
            <translation>L3 множественные потоки IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L3 Streams IPv6 SAMComplete</source>
            <translation>10/100/1000 L3 потоки IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>L4 TCP Wirespeed</source>
            <translation>Скорость физического соединения L4 TCP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed SAMComplete</source>
            <translation>10/100/1000 L4 TCP скорость физического соединения SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed</source>
            <translation>10/100/1000 L4 TCP скорость физического соединения TrueSpeed</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VNF Test</source>
            <translation>Тест VNF</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>10/100/1000 L4 TCP Скорость в среде передачи данных TrueSpeed VNF</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Mon</source>
            <translation>Монитор. трафика уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Контур обратной связи</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Loopback</source>
            <translation>Проверка по шлейфу уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Dual Thru</source>
            <translation>Сквозн. сдвоен. режим</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Traffic Dual Thru</source>
            <translation>Сквозн. сдвоен. режим трафика уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Multiple Streams</source>
            <translation>Множеств. потоки уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Triple Play</source>
            <translation>Передача видеоинформации , данных и речи уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 Triple Play</source>
            <translation>Передача видеоинформации , данных и речи уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MiM Traffic</source>
            <translation>Трафик MiM уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Term</source>
            <translation>Заверш. трафика MiM уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MiM Traffic Mon</source>
            <translation>Монитор. трафика MiM уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 MPLS-TP Traffic</source>
            <translation>Трафик MPLS-TP уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 MPLS-TP Traffic Term</source>
            <translation>Заверш. трафика MPLS-TP уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 PTP/1588</source>
            <translation>PTP/1588 уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 2 PTP/1588 Term</source>
            <translation>Заверш. PTP/1588 уровня 2 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожд. сигнала уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожд. сигнала уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon</source>
            <translation>Монитор. трафика уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback</source>
            <translation>Проверка по шлейфу уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Dual Thru</source>
            <translation>Сквозн. сдвоен. режим трафика уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Traffic Mon IPv6</source>
            <translation>IPv6 монитор. трафика уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Multiple Streams</source>
            <translation>Множеств. потоки уровня 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Streams Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу потоков уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 3 Triple Play</source>
            <translation>Передача видеоинформации данных и речи уровня 3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 3 Triple Play</source>
            <translation>Передача видеоинформации данных и речи уровня 3 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Traffic</source>
            <translation>Трафик уровня 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term</source>
            <translation>Заверш. трафика уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback</source>
            <translation>Проверка по шлейфу уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 Multiple Streams</source>
            <translation>Множеств. потоки уровня 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term</source>
            <translation>Заверш. потоков уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 Streams Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу потоков уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 PTP/1588</source>
            <translation>PTP/1588 уровня 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 PTP/1588 Term</source>
            <translation>Заверш. PTP/1588 уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 4 TCP Wirespeed</source>
            <translation>Скорость среды передачи данных TCP уровня 4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth Layer 4 TCP Wirespeed Term</source>
            <translation>Заверш. скорост. среды передачи данных TCP уровня 4 в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>IP Video</source>
            <translation>IP Видео</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Explorer</source>
            <translation>Проводник MPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Explorer</source>
            <translation>Проводник MPTS в IP Видео в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Explorer</source>
            <translation>Проводник SPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Explorer</source>
            <translation>Проводник SPTS в IP Видео в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>MPTS Analyzer</source>
            <translation>Анализатор MPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video MPTS Analyzer</source>
            <translation>Анализатор MPTS в IP Видео в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SPTS Analyzer</source>
            <translation>Анализатор SPTS</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth IP Video SPTS Analyzer</source>
            <translation>Анализатор SPTS в IP Видео в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth VoIP Term</source>
            <translation>Заверш. VoIP в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>J-Profiler</source>
            <translation>Устройство распределения по профилям J-Profiler</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10/100/1000 Eth J-Profiler</source>
            <translation>Устройство распределения по профилям J-Profiler в 10/100/1000 Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical</source>
            <translation>100M Оптич.</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth TrueSAM</source>
            <translation>Устройство распределения по профилям TrueSAM в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic QuickCheck</source>
            <translation>100M Optical Eth L2 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic QuickCheck</source>
            <translation>100M Optical Eth L3 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic RFC 2544</source>
            <translation>100M Optical Eth L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 RFC 2544</source>
            <translation>100M Optical Eth L3 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 RFC 2544</source>
            <translation>100M Optical Eth L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv4 RFC 2544</source>
            <translation>100M Optical Eth L4 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L4 Traffic IPv6 RFC 2544</source>
            <translation>100M Optical Eth L4 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Traffic SAMComplete</source>
            <translation>100M Optical Eth L2 трафик SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L2 Streams SAMComplete</source>
            <translation>100M Optical Eth L2 потоки SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv4 SAMComplete</source>
            <translation>100M Optical Eth L3 трафик IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Traffic IPv6 SAMComplete</source>
            <translation>100M Optical Eth L3 трафик IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv4 SAMComplete</source>
            <translation>100M Optical Eth L3 потоки IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth L3 Streams IPv6 SAMComplete</source>
            <translation>100M Optical Eth L3 потоки IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Monitor/Thru</source>
            <translation>Монитор / Сквозн. режим</translation>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Loopback Term</source>
            <translation>Заверш. проверки по шлейфу уровня 2 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 2 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 Triple Play</source>
            <translation>Передача видеоинформации , данных и речи уровня 2 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Term</source>
            <translation>Заверш. трафика MiM уровня 2 в 100M Оптич.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MiM Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика MiM уровня 2 в 100M Оптич.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Layer 2 MPLS-TP Traffic Term</source>
            <translation>Заверш. трафика MPLS-TP уровня 2 в 100M Оптич.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 2 PTP/1588 Term</source>
            <translation>Режим завершен. PTP/1588 уровня Optical Eth 2 при 100 М</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term</source>
            <translation>Заверш. контроля прохождения сигнала уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контроля прохождения сигнала уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback</source>
            <translation>Проверка по шлейфу уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор./ сквозн. режим. трафика уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Streams Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу потоков уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 3 Triple Play</source>
            <translation>Передача видеоинформации данных и речи уровня 3 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term</source>
            <translation>Заверш. трафика уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback</source>
            <translation>Проверка по шлейфу уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term</source>
            <translation>Заверш. потоков уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 Streams Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу потоков уровня 4 в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth Layer 4 PTP/1588 Term</source>
            <translation>Режим завершен. PTP/1588 уровня Optical Eth 4 при 100 М</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Explorer</source>
            <translation>Проводник MPTS в IP Видео в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Explorer</source>
            <translation>Проводник SPTS в IP Видео в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video MPTS Analyzer</source>
            <translation>Анализатор MPTS в IP Видео в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth IP Video SPTS Analyzer</source>
            <translation>Анализатор SPTS в IP Видео в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth VoIP Term</source>
            <translation>Заверш. VoIP в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100M Optical Eth J-Profiler</source>
            <translation>Устройство распределения по профилям J-Profiler в 100M Оптич. Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical</source>
            <translation>1GigE Оптич.</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE</source>
            <translation>1GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical TrueSAM</source>
            <translation>1GigE Оптический TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic QuickCheck</source>
            <translation>1GigE L2 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic QuickCheck</source>
            <translation>1GigE L3 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic RFC 2544</source>
            <translation>1GigE L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>1GigE L3 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>1GigE L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv4 RFC 2544</source>
            <translation>1GigE L4 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 Traffic IPv6 RFC 2544</source>
            <translation>1GigE L4 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Traffic SAMComplete</source>
            <translation>1GigE L2 трафик SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L2 Streams SAMComplete</source>
            <translation>1GigE L2 потоки SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>1GigE L3 Протокол IPv4 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>1GigE L3 Протокол IPv6 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv4 SAMComplete</source>
            <translation>1GigE L3 потоки IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L3 Streams IPv6 SAMComplete</source>
            <translation>1GigE L3 потоки IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed SAMComplete</source>
            <translation>1GigE L4 TCP скорость физического соединения SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed</source>
            <translation>1GigE L4 TCP скорость физического соединения TrueSpeed</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 TCP Wirespeed TrueSpeed VNF</source>
            <translation>1GigE L4 TCP Скорость в среде передачи данных TrueSpeed VNF</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Проверка PTP</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE L4 PTP Check</source>
            <translation>Проверка 1GigE L4 PTP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>SyncE Wander</source>
            <translation>SyncE вандер</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Optical SyncE Wander Mon/Thru</source>
            <translation>1GigE Оптчиеский SyncE отклонение Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 BERT</source>
            <translation>BERT уровня 1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим  модул. BERT уровня 1 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 Patterns</source>
            <translation>Последовательности уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Patterns Term</source>
            <translation>Заверш. последовательн. уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Loopback</source>
            <translation>Проверка по шлейфу уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 Triple Play</source>
            <translation>Передача видеоинформации , данных и речи уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Term</source>
            <translation>Заверш. трафика MiM уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MiM Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика MiM уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 MPLS-TP Traffic Term</source>
            <translation>Заверш. трафика MPLS-TP уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Term</source>
            <translation>Заверш. модул. PTP/1588 уровня 2 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 2 PTP/1588 Dual Mon</source>
            <translation>1GigE уровень 2 PTP/1588 дв. пон.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожд. сигнала уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожд. сигнала уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback</source>
            <translation>Проверка по шлейфу уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор./ сквозн. режим. трафика уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Streams Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу потоков уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 3 Triple Play</source>
            <translation>Передача видеоинформации данных и речи уровня 3 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term</source>
            <translation>Заверш. трафика уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback</source>
            <translation>Проверка по шлейфу уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term</source>
            <translation>Заверш. потоков уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 Streams Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу потоков уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 PTP/1588 Term</source>
            <translation>Заверш. модул. PTP/1588 уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Layer 4 TCP Wirespeed Term</source>
            <translation>Заверш. скорост. среды передачи данных TCP уровня 4 в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Explorer</source>
            <translation>Проводник MPTS в IP Видео в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Explorer</source>
            <translation>Проводник SPTS в IP Видео в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video MPTS Analyzer</source>
            <translation>Анализатор MPTS в IP Видео в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE IP Video SPTS Analyzer</source>
            <translation>Анализатор SPTS в IP Видео в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE Eth VoIP Term</source>
            <translation>Заверш. VoIP в 1GigE Eth</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1GigE J-Profiler</source>
            <translation>Устройство распределения по профилям J-Profiler в 1GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN</source>
            <translation>10GigE LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Optical TrueSAM</source>
            <translation>10GigE Оптический TrueSAM</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic QuickCheck</source>
            <translation>10GigE LAN L2 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic QuickCheck</source>
            <translation>10GigE LAN L3 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic RFC 2544</source>
            <translation>10GigE LAN L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 RFC 2544</source>
            <translation>10GigE LAN L3 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 RFC 2544</source>
            <translation>10GigE LAN L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv4 RFC 2544</source>
            <translation>10GigE LAN L4 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 Traffic IPv6 RFC 2544</source>
            <translation>10GigE LAN L4 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Traffic SAMComplete</source>
            <translation>10GigE LAN L2 трафик SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L2 Streams SAMComplete</source>
            <translation>10GigE LAN L2 потоки SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv4 SAMComplete</source>
            <translation>10GigE LAN L3 трафик IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Traffic IPv6 SAMComplete</source>
            <translation>10GigE LAN L3 трафик IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv4 SAMComplete</source>
            <translation>10GigE LAN L3 потоки IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L3 Streams IPv6 SAMComplete</source>
            <translation>10GigE LAN L3 потоки IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed SAMComplete</source>
            <translation>10GigE LAN L4 TCP скорость физического соединения SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN L4 TCP Wirespeed TrueSpeed</source>
            <translation>10GigE LAN L4 TCP скорость физического соединения TrueSpeed</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим  модул. BERT уровня 1 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Loopback</source>
            <translation>Проверка по шлейфу уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 2 Triple Play</source>
            <translation>Передача видеоинформации данных и речи уровня 2 в 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Term</source>
            <translation>Заверш. трафика MiM уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MiM Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика MiM уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 MPLS-TP Traffic Term</source>
            <translation>Заверш. трафика MPLS-TP уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 2 PTP/1588 Term</source>
            <translation>Заверш . модул . PTP/1588 уровня 2 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожден. сигнала уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожден. сигнала уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback</source>
            <translation>Проверка по шлейфу уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор./ сквозн. режим. трафика уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 3 Streams Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу потоков уровня 3 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE Layer 3 Triple Play</source>
            <translation>Передача видеоинформации данных и речи уровня 3 в 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term</source>
            <translation>Заверш. трафика уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback</source>
            <translation>Проверка по шлейфу уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term</source>
            <translation>Заверш. потоков уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 Streams Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу потоков уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 PTP/1588 Term</source>
            <translation>Заверш . модул . PTP/1588 уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN Layer 4 TCP Wirespeed Term</source>
            <translation>Заверш. скорост. среды передачи данных TCP уровня 4 в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Explorer</source>
            <translation>Проводник MPTS в IP Видео в 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Explorer</source>
            <translation>Проводник SPTS в IP Видео в 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video MPTS Analyzer</source>
            <translation>Анализатор MPTS в IP Видео в 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE IP Video SPTS Analyzer</source>
            <translation>Анализатор SPTS в IP Видео в 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE LAN VoIP Term</source>
            <translation>Заверш. VoIP в LAN 10GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN</source>
            <translation>10GigE WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. модул. BERT уровня 1 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Loopback</source>
            <translation>Проверка по шлейфу уровня 2 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 2 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 2 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохождения сигнала уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохождения сигнала уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback</source>
            <translation>Проверка по шлейфу уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор./ сквозн. режим. трафика уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN OC-192c L3 Streams Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу потоков уровня 3 в 10GigE WAN OC-192c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. модул. BERT уровня 1 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Loopback</source>
            <translation>Проверка по шлейфу уровня 2 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 2 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 2 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохождения сигнала уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохождения сигнала уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback</source>
            <translation>Проверка по шлейфу уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор./ сквозн. режим. трафика уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Loopback IPv6</source>
            <translation>IPv6 проверки по шлейфу уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Loopback</source>
            <translation>Проверка по шлейфу потоков уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10GigE WAN STM-64 L3 Streams Loopback IPv6</source>
            <translation>IPv6 проверк. по шлейфу потоков уровня 3 в 10GigE WAN STM-64</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE</source>
            <translation>40GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Optics Self-Test</source>
            <translation>Самопроверка оптики в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic QuickCheck</source>
            <translation>40GigE L2 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic QuickCheck</source>
            <translation>40GigE L3 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic RFC 2544</source>
            <translation>40GigE L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>40GigE L3 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>40GigE L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Traffic SAMComplete</source>
            <translation>40GigE L2 трафик SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L2 Streams SAMComplete</source>
            <translation>40GigE L2 потоки SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>40GigE L3 Протокол IPv4 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>40GigE L3 Протокол IPv6 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv4 SAMComplete</source>
            <translation>40GigE L3 потоки IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE L3 Streams IPv6 SAMComplete</source>
            <translation>40GigE L3 потоки IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 1 PCS</source>
            <translation>PCS уровня 1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Term</source>
            <translation>Заверш . модул . PCS уровня 1 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 1 PCS Mon/Thru</source>
            <translation>Монитор./ сквозн. режим  модул. PCS уровня 1 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Term</source>
            <translation>Заверш . трафика уровня 2 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 2 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 2 Streams Term</source>
            <translation>Заверш . потоков уровня 2 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term</source>
            <translation>Заверш . команды Ping уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш . команды Ping уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожд. сигнала уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожд. сигнала уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term</source>
            <translation>Заверш . трафика уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш . трафика уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор ./ сквозн . режим . трафика уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term</source>
            <translation>Заверш . потоков уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>40GigE Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш . потоков уровня 3 в 40GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE</source>
            <translation>100GigE</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Optics Self-Test</source>
            <translation>Самопроверка оптики в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic QuickCheck</source>
            <translation>100GigE L2 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic QuickCheck</source>
            <translation>100GigE L3 быстрая проверка трафика</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic RFC 2544</source>
            <translation>100GigE L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 RFC 2544</source>
            <translation>100GigE L3 трафик IPv4 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 RFC 2544</source>
            <translation>100GigE L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Traffic SAMComplete</source>
            <translation>100GigE L2 трафик SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L2 Streams SAMComplete</source>
            <translation>100GigE L2 потоки SAMComplete.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv4 SAMComplete</source>
            <translation>100GigE L3 Протокол IPv4 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Traffic IPv6 SAMComplete</source>
            <translation>100GigE L3 Протокол IPv6 Поддержка SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv4 SAMComplete</source>
            <translation>100GigE L3 потоки IPv4 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE L3 Streams IPv6 SAMComplete</source>
            <translation>100GigE L3 потоки IPv6 SAMComplete</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Term</source>
            <translation>Заверш . модул . PCS уровня 1 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 1 PCS Mon/Thru</source>
            <translation>Монитор./ сквозн. режим  модул. PCS уровня 1 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term</source>
            <translation>Заверш . трафика уровня 2 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 2 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term</source>
            <translation>Заверш . потоков уровня 2 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term</source>
            <translation>Заверш . команды Ping уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш . команды Ping уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожд. сигнала уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожд. сигнала уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term</source>
            <translation>Заверш . трафика уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш . трафика уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>IPv6 монитор ./ сквозн . режим . трафика уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term</source>
            <translation>Заверш . потоков уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш . потоков уровня 3 в 100GigE</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE RS-FEC</source>
            <translation>100GigE RS-FEC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Term RS-FEC</source>
            <translation>100GigE уровень 2 срок потока RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Traffic Mon RS-FEC</source>
            <translation>100GigE уровень 2 трафик Пон. RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 2 Streams Term RS-FEC</source>
            <translation>100GigE уровень 2 срок потоков RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term RS-FEC</source>
            <translation>100GigE уровень 3 срок проверки связи RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Ping Term IPv6 RS-FEC</source>
            <translation>100GigE уровень 3 срок проверки связи RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term RS-FEC</source>
            <translation>100GigE уровень 3 срок трассировки RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traceroute Term IPv6 RS-FEC</source>
            <translation>100GigE уровень 3 срок трассировки IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term RS-FEC</source>
            <translation>100GigE уровень 3 срок потока RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon RS-FEC</source>
            <translation>100GigE уровень 3 трафик Пон. RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Term IPv6 RS-FEC</source>
            <translation>100GigE уровень 3 срок трафика IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Traffic Mon IPv6 RS-FEC</source>
            <translation>100GigE уровень 3 трафик пон. IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term RS-FEC</source>
            <translation>100GigE уровень 3 срок потоков RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>100GigE Layer 3 Streams Term IPv6 RS-FEC</source>
            <translation>100GigE уровень 3 срок потоков IPv6 RS-FEC</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Волоконно - оптический канал</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig</source>
            <translation>1Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1G FC</source>
            <translation>1G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в волоконно - оптическом канале 1Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим модул. BERT уровня 1 в волоконно - оптическом канале 1Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Заверш. последовательн. уровня 2 в волоконно - оптическом канале 1Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в волоконно - оптическом канале 1Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в волоконно - оптическом канале 1Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig</source>
            <translation>2Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2G FC</source>
            <translation>2G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в волоконно - оптическом канале 2Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим модул. BERT уровня 1 в волоконно - оптическом канале 2Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Заверш. последовательн. уровня 2 в волоконно - оптическом канале 2Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в волоконно - оптическом канале 2Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в волоконно - оптическом канале 2Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig</source>
            <translation>4Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4G FC</source>
            <translation>4G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в волоконно - оптическом канале 4Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим модул. BERT уровня 1 в волоконно - оптическом канале 4Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Заверш. последовательн. уровня 2 в волоконно - оптическом канале 4Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в волоконно - оптическом канале 4Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в волоконно - оптическом канале 4Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig</source>
            <translation>8Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8G FC</source>
            <translation>8G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Patterns Term</source>
            <translation>Заверш. последовательн. уровня 2 в волоконно - оптическом канале 8Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в волоконно - оптическом канале 8Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>8Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в волоконно - оптическом канале 8Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig</source>
            <translation>10Gig</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10G FC</source>
            <translation>10G FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в волоконно - оптическом канале 10Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим модул. BERT уровня 1 в волоконно - оптическом канале 10Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в волоконно - оптическом канале 10Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим трафика уровня 2 в волоконно - оптическом канале 10Gig</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig</source>
            <translation>16 Гбит/с</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16G FC</source>
            <translation>16 Гбит/с FC</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Term</source>
            <translation>16 Гбит/с Оптоволоконный канал Layer 1 BERT Монит. / Сквозн.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 1 BERT Mon/Thru</source>
            <translation>16 Гбит/с Оптоволоконный канал Layer 1 BERT Монит. / Сквозн.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Term</source>
            <translation>16 Гбит/с Оптоволоконный канал Layer 2 Оконечн. передача</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>16Gig Fibre Channel Layer 2 Traffic Mon/Thru</source>
            <translation>16 Гбит/с Оптоволоконный канал Layer 2 BERT Монит. / Сквозн.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI</source>
            <translation>614.4M Радиоинтерфейс общего пользования</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Проверка CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Check</source>
            <translation>Проверка 614.4M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Term</source>
            <translation>614.4M Слой радиоинтерфейса общего пользования 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>614.4M Слой радиоинтерфейса общего пользования 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Layer 2 BERT</source>
            <translation>BERT уровня 2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 614.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>614.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 614.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI</source>
            <translation>1228.8M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Check</source>
            <translation>Проверка 1228.8M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Term</source>
            <translation>1228.8M Слой радиоинтерфейса общего пользования 1 BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>1228.8M Слой радиоинтерфейса общего пользования 1 BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 1228.8M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1228.8M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 1228.8M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI</source>
            <translation>2457.6M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Check</source>
            <translation>Проверка 2457.6M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в CPRI 2457.6M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в CPRI 2457.6M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 2457.6M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2457.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 2457.6M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI</source>
            <translation>3072.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Check</source>
            <translation>Проверка 3072.0M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в CPRI 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в CPRI 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI</source>
            <translation>4915.2M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Check</source>
            <translation>Проверка 4915.2M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в CPRI 4915.2M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в CPRI 4915.2M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 4915.2M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>4915.2M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 4915.2M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI</source>
            <translation>6144.0M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Check</source>
            <translation>Проверка 6144.0M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в CPRI 6144.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в CPRI 6144.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 6144.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144.0M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 6144.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI</source>
            <translation>9830.4M CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Check</source>
            <translation>Проверка 9830.4M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в CPRI 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в CPRI 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в CPRI 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в CPRI 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137,6 Мбит/с</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI</source>
            <translation>10137,6 Мбит/с CPRI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Check</source>
            <translation>Проверка 10137.6M CPRI</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Term</source>
            <translation>10137,6 Мбит/с CPRI Layer 2 BERT Оконечн.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10137.6M CPRI Layer 2 BERT Mon/Thru</source>
            <translation>10137,6 Мбит/с CPRI Layer 2 BERT Монит. / Сквозн.</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI</source>
            <translation>768M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в OBSAI 768M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в OBSAI 768M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в OBSAI 768M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>768M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в OBSAI 768M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI</source>
            <translation>1536M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в OBSAI 1536M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в OBSAI 1536M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в OBSAI 1536M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>1536M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в OBSAI 1536M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI</source>
            <translation>3072M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в OBSAI 3072M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в OBSAI 3072M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в OBSAI 3072M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в OBSAI 3072M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI</source>
            <translation>6144M OBSAI</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 1 в OBSAI 6144M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в OBSAI 6144M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Term</source>
            <translation>Заверш . модул . BERT уровня 2 в OBSAI 6144M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>6144M OBSAI Layer 2 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 2 в OBSAI 6144M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1</source>
            <translation>OTU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Проверка OTN</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G OTN Check</source>
            <translation>Проверка OTU1 2.7G OTN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-48</source>
            <translation>STS-48</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-48c в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-48c в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-12c в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-3c в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-1 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU1 2.7G STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-16c в AU-4 в STM-16 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU1 2.7G STM-16 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-4c в AU-4 в STM-16 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU1 2.7G STM-16 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4 в AU-4 в STM-16 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU1 2.7G STM-16 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-3 в AU-4 в STM-16 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU1 2.7G STM-16 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G STM-16 AU-3 VC-3 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU0</source>
            <translation>ODU0</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU0 в 2.7G в OTU1</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU1 2.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 GMP уровень 2 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU1 2.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>OTU1 2.7G ODU0 GMP уровень 3 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Jitter</source>
            <translation>Джиттер в OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Jitter BERT Term</source>
            <translation>Заверш. модул. BERT массов. джиттера в OTU1 2.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Jitter BERT Term</source>
            <translation>Заверш. модул. BERT массов. джиттера в OTU1 2.7G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Jitter BERT Term</source>
            <translation>Заверш. модул. BERT массов. джиттера в OTU1 2.7G STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Wander</source>
            <translation>Отклонение в OTU1 2.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G Bulk Wander BERT Term</source>
            <translation>Заверш. модул. BERT массов. отклонения в OTU1 2.7G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STS-48c Bulk Wander BERT Term</source>
            <translation>Заверш. модул. BERT массов. отклонения в OTU1 2.7G STS-48c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G STM-16 AU-4 VC-4-16c Bulk Wander BERT Term</source>
            <translation>Заверш. модул. BERT массов. отклонения в OTU1 2.7G STM-16 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2</source>
            <translation>OTU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G OTN Check</source>
            <translation>Проверка OTU2 10.7G OTN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-192</source>
            <translation>STS-192</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-192c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-192c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-192c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-48c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-48c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-48c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-12c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-12c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-3c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-3c в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STS-1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-1 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2 10.7G STM-64 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-64c в AU-4 в STM-64 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2 10.7G STM-64 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-16c в AU-4 в STM-64 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2 10.7G STM-64 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-4c в AU-4 в STM-64 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2 10.7G STM-64 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4 в STM-64 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2 10.7G STM-64 AU-4 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-4 VC-3 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT  в VC-3 в AU-4 в STM-64 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2 10.7G STM-64 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G STM-64 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-3 в AU-3 в STM-64 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU1</source>
            <translation>ODU1</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU1 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU1 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU0 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU0 в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU2 10.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 2 Traffic Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 GMP уровень 2 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU2 10.7G ODU0 GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODU0 GMP Layer 3 Traffic Mon/Thru</source>
            <translation>OTU2 10.7G ODU0 GMP уровень 3 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODUflex</source>
            <translation>ODUflex</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODUflex в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex Bulk BERT Mon</source>
            <translation>Монитор массов. BERT в ODUflex в 10.7G в OTU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU2 10.7G ODUflex GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e</source>
            <translation>OTU1e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G OTN Check</source>
            <translation>Проверка OTU1e 11.05G OTN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU1e 11.05G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в 11.05G в OTU1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в OTU1e 11.05G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в 11.05G OTU1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в OTU1e 11.05G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 2 в 11.05G OTU1e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e</source>
            <translation>OTU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G OTN Check</source>
            <translation>Проверка OTU2e 11.1G OTN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU2e 11.1G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в 11.1G в OTU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в OTU2e 11.1G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 1 BERT Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим  модул . BERT уровня 1 в 11.1G OTU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в OTU2e 11.1G</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 2 в 11.1G OTU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3</source>
            <translation>OTU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G Optics Self-Test</source>
            <translation>Самопроверка оптики в 43.02G в OTL3.4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3.4 43.02G OTN Check</source>
            <translation>Проверка OTU3.4 43.02G OTN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L2 Traffic RFC 2544</source>
            <translation>OTL3.4 43.02G L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic RFC 2544</source>
            <translation>OTL3.4 43.02G L3 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G L3 Traffic IPv6 RFC 2544</source>
            <translation>OTL3.4 43.02G L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL BERT</source>
            <translation>OTL BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Term</source>
            <translation>OTL3.4 43.02G Заверш. BERT в OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL3.4 43.02G OTL BERT Mon</source>
            <translation>OTL3.4 43.02G Монитор BERT в OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Term</source>
            <translation>Заверш . массов . BERT в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Traffic Monitor/Thru</source>
            <translation>OTU3 43.02G Layer 2 Traffic Monitor/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожден. сигнала уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожден. сигнала уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Монитор./ сквозн. режим уровня 3 трафика IPv6 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 43.02G OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>STS-768</source>
            <translation>STS-768</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-768c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-768c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-768c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-192c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-192c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-192c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-48c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-48c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-48c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-12c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-12c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-12c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-3c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-3c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-3c в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в STS-1 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STS-1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в STS-1 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G STM-256 AU4 VC4-256c Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU3 43.02G STM-256 AU-4 VC-4-64c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-64c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-64c в AU-4 в STM-256 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU3 43.02G STM-256 AU-4 VC-4-16c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-16c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-16c в AU-4 в STM-256 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU3 43.02G STM-256 AU-4 VC-4-4c</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4-4c Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4-4c в AU-4 в STM-256 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU3 43.02G STM-256 AU-4 VC-4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-4 VC-4 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-4 в AU-4 в STM-256 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в OTU3 43.02G STM-256 AU-3 VC-3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G STM-256 AU-3 VC-3 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в VC-3 в AU-3 в STM-256 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2e</source>
            <translation>ODU2e</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU2e в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU2e в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в OTU3 43.02G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. модул. BERT уровня 1 в OTU3 43.02G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU3 43.02G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в OTU3 43.02G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU2</source>
            <translation>ODU2</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU2 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU2 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 3 в OTU3 43.02G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU1 в ODU2 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU2 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU0 в ODU2 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU3 43.02G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 уровень 2 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU3 43.02G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU2 ODU0 уровень 3 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU1 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU1 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU3 43.02G ODU1 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 Bulk BERT Mon/Thru</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU3 43.02G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 уровень 2 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU3 43.02G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU3 43.02G ODU1 ODU0 уровень 3 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU0 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU0 в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU3 43.02G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в OTU3 43.02G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU3 43.02G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 3 в OTU3 43.02G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODUflex в 43.02G в OTU3</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU3 43.02G ODUflex GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4</source>
            <translation>OTU4</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G Optics Self-Test</source>
            <translation>Самопроверка оптики в 111.8G в OTL4.10</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G OTN Check</source>
            <translation>Проверка OTU4 111.8G OTN</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L2 Traffic RFC 2544</source>
            <translation>OTU4 111.8G L2 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic RFC 2544</source>
            <translation>OTU4 111.8G L3 трафик RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G L3 Traffic IPv6 RFC 2544</source>
            <translation>OTU4 111.8G L3 трафик IPv6 RFC 2544</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Term</source>
            <translation>OTL4.10 111.8G Заверш. BERT в OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTL4.10 111.8G OTL BERT Mon</source>
            <translation>OTL4.10 111.8G Монитор BERT в OTL</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Term</source>
            <translation>Заверш . массов . BERT в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Term</source>
            <translation>Заверш . трафика уровня 2 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 2 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 2 Streams Term</source>
            <translation>Заверш. потоков уровня 2 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term</source>
            <translation>Заверш. команды Ping уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Ping Term IPv6</source>
            <translation>IPv6 заверш. команды Ping уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term</source>
            <translation>Заверш. контрол. прохожден. сигнала уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traceroute Term IPv6</source>
            <translation>IPv6 заверш. контрол. прохожден. сигнала уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор ./ сквозн . режим трафика уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Term IPv6</source>
            <translation>IPv6 заверш. трафика уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Traffic Mon/Thru IPv6</source>
            <translation>Монитор./ сквозн. режим уровня 3 трафика IPv6 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term</source>
            <translation>Заверш. потоков уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G Layer 3 Streams Term IPv6</source>
            <translation>IPv6 заверш. потоков уровня 3 в 111.8G OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>ODU3</source>
            <translation>ODU3</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU3 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU3 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU3 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU2e в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU2e в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Term</source>
            <translation>Заверш. модул. BERT уровня 1 в OTU4 111.8G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. модул. BERT уровня 1 в OTU4 111.8G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU4 111.8G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2e Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в OTU4 111.8G ODU2e</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU2 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU2 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 3 в OTU4 111.8G ODU2</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU1 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU1 в ODU2 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU2 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU0 в ODU2 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU4 111.8G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 уровень 2 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU4 111.8G ODU2 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU2 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU2 ODU0 уровень 3 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU1 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU1 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Term</source>
            <translation>OTU4 111.8G ODU1 ODU0 Bulk BERT Term</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU0 в ODU1 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU4 111.8G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 уровень 2 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU4 111.8G ODU1 ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU1 ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>OTU4 111.8G ODU1 ODU0 уровень 3 трафик пон ./ четв .</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODU0 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Bulk BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим BERT в ODU0 в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU4 111.8G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 2 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 2 в OTU4 111.8G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Term</source>
            <translation>Заверш. трафика уровня 3 OTU4 111.8G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODU0 Layer 3 Traffic Mon/Thru</source>
            <translation>Монитор./ сквозн. режим. трафика уровня 3 в OTU4 111.8G ODU0</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex Bulk BERT Term</source>
            <translation>Заверш. массов. BERT в ODUflex в 111.8G в OTU4</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G ODUflex GMP Layer 2 Traffic Term</source>
            <translation>Заверш. трафика уровня 2 OTU4 111.8G ODUflex GMP</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Optical BERT</source>
            <translation>Оптический BERT</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Term</source>
            <translation>Режим завершен. устройства BERT оптич. уровня 1 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>3072.0M Optical Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим устройства BERT оптич. уровня 1 3072.0M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Term</source>
            <translation>Режим завершен. устройства BERT оптич. уровня 1 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>9830.4M Optical Layer 1 BERT Mon/Thru</source>
            <translation>Монитор./ сквозн. режим устройства BERT оптич. уровня 1 9830.4M</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing</source>
            <translation>Синхронизация</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>1PPS</source>
            <translation>1PPS</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Анализ</translation>
        </message>
        <message utf8="true">
            <source>1PPS Analysis</source>
            <translation>Анализ 1PPS </translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Wander</source>
            <translation>2 мГц отклонение</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>2MHz Clock Wander Analysis</source>
            <translation>2 мГц анализ отклонения по времени</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Wander</source>
            <translation>Отклонение 10MHz</translation>
            <comment>This text appears in the main Test menu.</comment>
        </message>
        <message utf8="true">
            <source>10MHz Clock Wander Analysis</source>
            <translation>10 мГц анализ отклонения по времени</translation>
            <comment>This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Benchmark</source>
            <translation>Ориентир</translation>
            <comment>This text appears in the main Test menu.&#xA;This is an application name.</comment>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Модуль синхронизации</translation>
        </message>
    </context>
</TS>
