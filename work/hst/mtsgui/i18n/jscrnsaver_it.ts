<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>Questo apparecchio di prova è stato bloccato da un utente per impedire l'accesso non autorizzato.</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>Password non richiesta , premere OK per sbloccare.</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>Sbloccare l'apparecchio di prova?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>Inserire la password per sbloccare:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>Inserire la password</translation>
        </message>
    </context>
</TS>
