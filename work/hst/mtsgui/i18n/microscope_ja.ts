<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi 顕微鏡</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>読み込み</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>ｽﾅｯﾌﾟｼｮｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>ﾋﾞｭｰ 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>ﾋﾞｭｰ 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>ﾋﾞｭｰ 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>ﾋﾞｭｰ 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>ﾋﾞｭｰ FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>顕微鏡</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>ｷｬﾌﾟﾁｬ</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>固定</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>画像</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>明るさ</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>ｺﾝﾄﾗｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>画面ﾚｲｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>全画面表示</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>並べて表示</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>終了</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>画像の保存</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>画像の選択</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>画像ﾌｧｲﾙ (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>すべてのﾌｧｲﾙ (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>選択</translation>
        </message>
    </context>
</TS>
