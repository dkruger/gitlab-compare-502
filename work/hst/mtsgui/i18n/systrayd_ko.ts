<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>systrayd::CAudioSystemTrayDataItem</name>
        <message utf8="true">
            <source>Audio</source>
            <translation>오디오</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBatterySystemTrayDataItem</name>
        <message utf8="true">
            <source>Battery/Charger</source>
            <translation>배터리/충전장치</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please plug in AC power to continue using the test instrument.</source>
            <translation>배터리가 너무 뜨겁습니다. 테스트 장치를 계속 사용하기 위해 AC 전원은 연결하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please keep the AC power plugged in to continue using the test instrument.</source>
            <translation>배터리가 너무 뜨겁습니다. AC 전원은 테스트 장치를 계속 사용하기 위해 연결된 상태를 유지하시기 바랍니다.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and cannot be used until it cools down. The AC power must remain plugged in to continue using the test instrument.</source>
            <translation>배터리가 너무 뜨거워서 냉각되기 전까지 사용할 수 없습니다. AC 전원은 테스트 장치를 계속 사용하기 위해 연결된 상태를 유지해야 합니다.</translation>
        </message>
        <message utf8="true">
            <source>Battery charging is not possible at this time.</source>
            <translation>지금은 배터리를 충전할 수 없습니다.</translation>
        </message>
        <message utf8="true">
            <source>A power failure has occurred, the battery is currently not charging.</source>
            <translation>전기 고장이 발생했습니다. 지금 배터리가 충전되지 않고 있습니다.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBeepingWarning</name>
        <message utf8="true">
            <source>System Warning</source>
            <translation>시스템 경고</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBluetoothSystemTrayDataItem</name>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>블루투스</translation>
        </message>
        <message utf8="true">
            <source>File received and placed in the bluetooth inbox.&#xA;Filename: %1</source>
            <translation>파일이 수신되어 블루투스 받은 편지함에 저장되었습니다.&#xA;파일명: %1</translation>
        </message>
        <message utf8="true">
            <source>Pair requested. You may go to the Bluetooth system page to complete the pair by clicking on the icon above.</source>
            <translation>페어 요청. 위의 아이콘을 클릭하여 블루투스 시스템 페이지로 가서 페어를 완성할 수 있습니다.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CNtpSystemTrayDataItem</name>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CP5000iSystemTrayDataItem</name>
        <message utf8="true">
            <source>P5000i</source>
            <translation>P5000i</translation>
        </message>
        <message utf8="true">
            <source>The P5000i Microscope will not function in this USB port.  Please use the other USB port.</source>
            <translation>P5000i 현미경이 이 USB 포트에서 작동하지 않습니다 . 다른 USB 포트를 사용하시기 바랍니다 .</translation>
        </message>
        <message utf8="true">
            <source>Microscope Error</source>
            <translation>현미경 에러</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CStrataSyncSystemTrayDataItem</name>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CUsbFlashSystemTrayDataItem</name>
        <message utf8="true">
            <source>USB Flash</source>
            <translation>USB 플래시</translation>
        </message>
        <message utf8="true">
            <source>Format complete. Please remember to eject the device from the USB system page before removing it.</source>
            <translation>포맷이 완료되었습니다. 장치를 제거하기 전에 USB시스템 페이지에서 장치를 해제하세요.</translation>
        </message>
        <message utf8="true">
            <source>Formatting USB device. Do not remove device until process is completed.</source>
            <translation>USB 장치를 포맷하고 있는 중입니다. 작업이 완료될 때까지 장치를 제거하지 마세요.</translation>
        </message>
        <message utf8="true">
            <source>The USB device was removed without being ejected. Data may be corrupted.</source>
            <translation>USB 장치가 해제되지 않고 제거되었습니다. 데이터가 손상되었을 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>The USB device has been ejected and is safe to remove.</source>
            <translation>USB 장치가 해제되었으므로 안전하게 제거할 수 있습니다.</translation>
        </message>
        <message utf8="true">
            <source>Please remember to eject the device from the USB system page before removing it.</source>
            <translation>장치를 제거하기 전에 USB시스템 페이지에서 장치를 해제하세요.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CVncSystemTrayDataItem</name>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CWifiSystemTrayDataItem</name>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
    </context>
</TS>
