<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscópio Viavi</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carregar</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salvar</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>Foto instantânea</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>Vista 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>Vista 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>Vista 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>Vista 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Vista FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscópio</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Capturar</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Congelar</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>Imagem</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Brilho</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>Contraste</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>Layout da tela</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>Tela cheia</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>Mosaico</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Desistir</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Salvar imagem</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Selecionar imagem</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Arquivos de imagem (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos os arquivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Selecionar</translation>
        </message>
    </context>
</TS>
