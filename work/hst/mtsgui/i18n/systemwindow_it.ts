<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>File</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Archivi rimovibili</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>Non sono stati rilevati dispositivi.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>Formattando questo dispositivo USB, tutti i dati esistenti saranno eliminati. Ciò vale per tutti i file e le partizioni.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Espelli</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Sfoglia...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sì</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NESSUNO</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>SÌ</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>NO</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Richiesta accoppiamento Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>Inserire PIN per associazione</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>Coppia</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>Richiesta di associazione</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>Mittente richiesta di associazione:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>Per creare l'associazione al dispositivo, accertarsi che il codice di seguito mostrato corrisponda al codice indicato sul dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>Associazione avviata</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>Destinatario richiesta di associazione:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>Avviare scansione</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>Arrestare scansione</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Impostazioni</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Abilità Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>Consentire ad altri dispositivi di associarsi al dispositivo in uso</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>Nome dispositivo</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>Applicazione mobile attivata</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Rete</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>Modo OP</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statico</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Subnet mask</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Gateway</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>server DNS</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>Modo IPv6</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Disabilitato</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manuale</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>Indirizzo IPv6</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Lung. prefisso subnet</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>Server DNS</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Indirizzo link-locale</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Indirizzo Stateless</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Indirizzo Stateful</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Moduli non caricati</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>Presente</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>Non presente</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Avviamento</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Abilitante</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Inizializzazione</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Pronto</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Analisi</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Disattivare</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Arresto</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>Associazione</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>Associato</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>Abilita scheda di rete wireless</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNESSO</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Disconnesso</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>Connettersi alla rete aziendale WPA</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>Nome rete</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>Metodo di autenticazione esterno</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>Metodo di autenticazione interno</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>Nome utente</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>Identità anonima</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Password</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>Certificati</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>Password chiave privata</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>Connetti a rete personale WPA</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>Passphrase</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>Nessun dispositivo wireless USB rilevato.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>Arresta connessione</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>Dimentica Rete</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>Connessione alla rete non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>Servizio 3G</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>Abilitazione modem in corso...</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>Modem abilitato</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>Modem disabilitato</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>Nessun modem rilevato</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>Errore: il modem non risponde.</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>Errore: controller dei dispositivi non avviato.</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>Errore: impossibile configurare il modem.</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>Attenzione: il modem non risponde e verrà reimpostato...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>Errore: SIM non inserita.</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>Errore: registrazione rete negata.</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connetti</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Disconnetti</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Connessione in corso</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>Disconnessione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Non collegato</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>Connessione non riuscita</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>Configurazione modem</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>Abilita modem</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>Nessun dispositivo modem rilevato o abilitato.</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>Rete APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>Stato della connessione</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>Controllare APN e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Server DNS principale</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Server DNS secondario</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Proxy e Protezione</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>Tipo di proxy</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Server Proxy</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Network Security</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>Disattivare FTP, telnet, e auto StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>Risparmio energia</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>Alimentazione da rete elettrica collegata</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Funzionamento a batteria</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>Nessuna batteria rilevata</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>La ricarica è stata disattivata a causa di consumo di energia</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Ricarica della batteria</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>La batteria è completamente carica</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>Nessuna batteria in caricamento</translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>Impossibile ricaricare la batteria a causa di mancanza di corrente</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>Stato batteria sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>La batteria è troppo calda e potrebbe non caricarsi</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>Batteria troppo calda&#xA;Collegarsi alla rete elettrica</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>Batteria troppo calda&#xA;Non scollegarsi dalla rete elettrica</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>Batteria troppo calda&#xA;Lo scollegamento dalla rete elettrica comporterà una chiusura forzata</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>La batteria è troppo fredda e potrebbe non caricarsi</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>La batteria è a rischio di surriscaldamento</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>La temperatura della batteria è normale</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>Attivare autospegnimento con l'utilizzo della batteria</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>Tempo di inattività (minuti)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Data e ora</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Fuso orario</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Regione</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>Africa</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>Americhe</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Antartide</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Asia</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Oceano Atlantico</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Australia</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Europa</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Oceano Indiano</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Oceano Pacifico</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>Paese</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Afghanistan</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Isole Aland</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Albania</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Algeria</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Samoa Americane</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Andorra</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Angola</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Anguilla</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Antigua e Barbuda</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Argentina</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Armenia</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Aruba</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Austria</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Azerbaigian</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Bahamas</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Bahrein</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Bangladesh</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Barbados</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Bielorussia</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Belgio</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Belize</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Benin</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Bermuda</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Bhutan</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Bolivia</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Bosnia ed Erzegovina</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Botswana</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Isola Bouvet</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Brasile</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>Territorio britannico dell'Oceano indiano</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Brunei Darussalam</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Bulgaria</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Burkina Faso</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Burundi</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Cambogia</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>Camerun</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Canada</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Capo Verde</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Cayman, Isole Cayman</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>Repubblica Centrafricana</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Ciad</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Cile</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>Cina</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Christmas, Isola</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Isole Cocos (Keeling)</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Colombia</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Comore</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Congo</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>Congo, Repubblica Democratica del</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Isole Cook</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Costa Rica</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Costa d'Avorio</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Croazia</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Cuba</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Cipro</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>Repubblica Ceca</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Danimarca</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Gibuti</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Dominica</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>Repubblica dominicana</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Ecuador</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Egitto</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>El Salvador</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Guinea Equatoriale</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Eritrea</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Estonia</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Etiopia</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Falkland (o Malvine), Isole</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Fær Øer</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Fiji</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Finlandia</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>Francia</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Guayana francese</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Polinesia Francese</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Territori francesi meridionali</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Gabon</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Gambia</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Georgia</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Germania</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Ghana</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Gibilterra</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Grecia</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Groenlandia</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Grenada</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>Guadalupa</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Guam</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Guatemala</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>Guernsey</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Guinea</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Guinea-Bissau</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Guyana</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Haiti</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Heard e McDonald</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Honduras</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Hong Kong</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Ungheria</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>Islanda</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>India</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Indonesia</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>Iran</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Iraq</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>Irlanda</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Isola di Man</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>Israele</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>Italia</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Giamaica</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Giappone</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Jersey</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Giordania</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Kazakstan</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Kenya</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Kiribati</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Corea, Repubblica democratica popolare di</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Corea, Repubblica di</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Kuwait</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Kyrgyzstan</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>Lao, Repubblica Popolare Democratica di</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Lettonia</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Libano</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Lesotho</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Liberia</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Libia</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Liechtenstein</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Lituania</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Lussemburgo</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Macao</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>Macedonia, Ex-Repubblica Jugoslava di</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Madagascar</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Malawi</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Malaysia</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>Maldive</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Mali</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Maltese</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Marshall, Isole</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Martinica</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Mauritania</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>Mauritius</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Mayotte</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>Messico</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Micronesia, Stati Federati della</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>Moldavia, Repubblica di</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>Monaco</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Mongolia</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Montenegro</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Montserrat</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Marocco</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Mozambico</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Myanmar</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Namibia</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Nauru</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Nepal</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Paesi Bassi</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Antille Olandesi</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>Nuova Caledonia</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>Nuova Zelanda</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Nicaragua</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Niger</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Nigeria</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Niue</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Norfolk, Isola</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Marianne Settentrionali</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Norvegia</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Oman</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Pakistan</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Palau</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Autorità Nazionale Palestinese</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Panama</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Papua Nuova Guinea</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Paraguay</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Perù</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Filippine</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>Pitcairn</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Polonia</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Portogallo</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Portorico</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Qatar</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>Réunion</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Romania</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>Federazione Russa</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Ruanda</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>Saint Barthélemy</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Saint Helena, Ascension e Tristan da Cunha</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>Saint Christopher e Nevis</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>Santa Lucia</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>Saint Martin</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>Saint Pierre e Miquelon</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>Saint Vincent e Grenadine</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Samoa</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>San Marino</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>Sao Tomè e Principe</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Arabia Saudita</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Senegal</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Serbia</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Seychelles</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Sierra Leone</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Singapore</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Slovacchia</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Slovenia</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Salomone, Isole</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Somalia</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>Sud Africa</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Isole di South Georgia e South Sandwich</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>Spagna</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Sri Lanka</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Sudan</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Suriname</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Svalbard e Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Swaziland</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>Svezia</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>Svizzera</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>Siria, Repubblica Araba di</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Taiwan</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Tajikistan</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>Tanzania, Repubblica Unita di</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Thailandia</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Timor-Leste</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Togo</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Tokelau</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Tonga</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Trinidad e Tobago</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Tunisia</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Turchia</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Turkmenistan</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Isole Turks e Caicos</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Tuvalu</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Uganda</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Ucraina</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Emirati arabi uniti</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Regno Unito</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>Stati Uniti d'America</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>Altre isole americane del Pacifico</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Uruguay</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Uzbekistan</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Vanuatu</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Città del Vaticano</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>Venezuela</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Viet Nam</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Vergini Britanniche, Isole</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Isole Vergini Americane</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Wallis e Futuna</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Sahara Occidentale&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Yemen</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Zambia</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Zimbabwe</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Area</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Casey</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Davis</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Dumont d'Urville</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Mawson</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>McMurdo</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Palmer</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Rothera</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Polo sud</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Syowa</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Vostok</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Territorio della capitale australiana</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>Nord</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>Nuovo Galles del sud</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Queensland</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>Sud</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Tasmania</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Victoria</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>Ovest</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Brasilia</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Brasilia - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Brasilia + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Alaska</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>Arizona</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Atlantico</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>Centrale</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>Orientale</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Hawaii</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Montagna</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>New Foundland</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Pacifico</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Saskatchewan</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Isola di Pasqua</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Kinshasa</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Lubumbashi</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Galapagos</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Gambier</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Marchesi</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Tahiti</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>Occidentale</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Danmarkshavn</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>Est</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Isole Phoenix</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Sporadi equatoriali</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Isole Gilbert</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>Nordovest</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Kosrae</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Truk</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Azzorre</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Madeira</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Irkutsk</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Kaliningrad</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Krasnoyarsk</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Magadan</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Mosca</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Omsk</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Vladivostok</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Yakutsk</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Yekaterinburg</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Isole Canarie</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Svalbard</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Jan Mayen</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Johnston</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Midway</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Wake</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Regolazione automatica per l'ora legale</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Data e ora correnti</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24 ore</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12 ore</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>Impostare orologio con NTP</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>true</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>false</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>Usa formato orario 24 ore</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>Server NTP LAN</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Server NTP Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>Server NTP 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>Server NTP 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (Inglese)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (Tedesco)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (Spagnolo)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (Francese)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (Cinese semplificato)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (Giapponese)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어 (Coreano)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский (Russo)</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (Portoghese)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk (Turco)</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Lingua:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Cambia standard di formato:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Esempi per il formato selezionato:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personalizza</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Visualizza</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Luminosità</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Salvaschermo</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Abilita salvaschermo automatico</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Messaggio</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>di ritardo</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Password salvaschermo</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Calibra schermo tattile...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>Abilita accesso VNC</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>La commutazione tra accesso VNC e protezione password causa la disconnessione delle connessioni correnti.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Password di accesso remoto</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>Questa password viene utilizzata per tutti gli accessi remoti, ad esempio vnc, ftp, ssh.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>Si è verificato un errore nell'avvio di VNC.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>Richiedi password per accesso VNC.</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Accesso intelligente ovunque</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>Codice di accesso</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>Indirizzo IP della LAN</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>Indirizzo IP Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>Numero di connessioni VNC</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Aggiorna</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>Impossibile effettuare l'upgrade. Rete elettrica non collegata. Collegarsi alla rete elettrica e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>Impossibile rilevare un dispositivo flash USB. Accertarsi che il dispositivo flash USB sia fermamente inserito e riprovare ad eseguire l'aggiornamento.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>Impossibile eseguire l'aggiornamento. Specificare il server di aggiornamento e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>La connessione alla rete non è disponibile. Verificare la connessione di rete e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>Impossibile contattare il server di aggiornamento. Controllare l'indirizzo del server e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>Errore rilevato durante la ricerca degli aggiornamenti disponibili.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>Impossibile effettuare l'upgrade. L'upggrade selezionato contiene file danneggiati o mancano file nell'upgrade.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>Impossibile effettuare l'upgrade. L'upggrade selezionato contiene file danneggiati.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>Generazione dei dati di aggiornamento USB non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>Non sono stati rilevati aggiornamenti, controllare le impostazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>Tentativo di aggiornamento non riuscito. Riprovare. Se il problema persiste, contattare il proprio rivenditore.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Blocco apparecchio di prova</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Bloccando l'apparecchio di prova si impedisce l'accesso non autorizzato. Per proteggere lo schermo viene visualizzata la schermata salvaschermo.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Blocca apparecchio di prova</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>Una password richiesta non è stata impostata. Gli utenti saranno in grado di sbloccare l'apparecchio di prova senza inserire una password.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Impostazioni password</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Queste impostazioni sono condivise con il salvaschermo.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Richiedi password</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Audio</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Volume altoparlante</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Disattiva microfono</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Volume microfono</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>Ricerca satelliti in corso...</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>Latitudine</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>Longitudine</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>Altitudine</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>Data/ora</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>Numero di satelliti aggancio</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>SNR medio</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>Informazioni singolo satellite</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Informazioni di sistema</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Connessione al server in corso...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Connessione effettuata...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Download dei file...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Caricamento file...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Download delle informazioni di aggiornamento...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Caricamento file di registro...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Sincronizzazione con il server riuscita.</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>In contenitore. In attesa di essere aggiunto all'inventario...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>Sincronizzazione con server non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Connessione persa durante la sincronizzazione. Verificare la configurazione di rete.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>Sincronizzazione con server non riuscita. Il server è occupato.</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>Sincronizzazione con server non riuscita. Rilevato errore di sistema.</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>Connessione non riuscita. Verificare la configurazione di rete o l'ID dell'account.</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Sincronizzazione interrotta.</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Dati di configurazione completati.</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Rapporti completati.</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Immettere l'ID dell'account.</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Avvia sinc</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Arresta sinc</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Indirizzo server</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>ID account</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configurazione</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Opzione</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Ripristina configurazione predefinita</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Video Player</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Browser Web</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Debug</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Periferica seriale</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Consenti controllo dispositivo seriale da Getty</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>Contenuto di /root/debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Impostazione test di screening</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>Gestore Attività</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>Informazioni Attività</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>Numero Attività</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>Le informazioni sull'attività sono aggiunte automaticamente ai resoconti dei test.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>Cronologia</translation>
        </message>
    </context>
</TS>
