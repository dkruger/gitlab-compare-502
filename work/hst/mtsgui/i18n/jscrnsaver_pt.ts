<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>Esse equipamento foi bloqueado pelo usuário para evitar o acesso sem autorização</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>Nenhuma senha exigida, pressione OK para desbloquear.</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>Desbloquear o equipamento?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>Por favor, entre com a senha para desbloquear:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>Insira a senha</translation>
        </message>
    </context>
</TS>
