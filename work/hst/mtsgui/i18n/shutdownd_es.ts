<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CPowerOffProcess</name>
        <message utf8="true">
            <source>Power off</source>
            <translation>Apagar</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set shuts down.</source>
            <translation>Por favor, espere mientras se apaga el instrumento de prueba.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CRebootProcess</name>
        <message utf8="true">
            <source>Reboot</source>
            <translation>Reiniciar</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set reboots.</source>
            <translation>Por favor, espere mientras se reinicia el instrumento de prueba.</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CShutdownPrompt</name>
        <message utf8="true">
            <source>Power options</source>
            <translation>Opciones de energía</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Cancelar</translation>
        </message>
    </context>
</TS>
