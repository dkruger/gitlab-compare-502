<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>Проверка CPRI</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>Конфигур.</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>Внести изменения в предыдущие настройки</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>Загрузить конфигурацию из профиля</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>Начать новую конфигурацию ( значения по умолчанию )</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>Вручную</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>Настроить параметры испытаний вручную</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>Параметры тестирования</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>Сохранить профили</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>Конец : настр. вручную</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>Выполнить испытания</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>Сохранено</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>Загрузить профили</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>Конец : загрузить профили</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>Измен. конфигурац.</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Испытание</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Выполнить</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>Запуск проверки CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>Проверка SFP</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>Конец : испытание</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Создать отчет</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>Повторить испытание</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>Ознакомиться с подробными результатами</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>Завершение проверки CPRI</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>Ознакомиться</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Результаты</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>Интерфейс</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Уровень 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>Конец : просмотр результатов</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>Информация об отчете</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>Конец : создать отчет</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>Ознакомиться с подробными результатами</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Испытание прервано</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>НЕ ЗАВЕРШЕНО</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>ЗАВЕРШЕНО  </translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ОШИБКА</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>ПРОЙДЕНО</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>НЕТ</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>Проверка CPRI</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>*** Запуск проверки CPRI (интерфейс общественного радио) ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** Тест завершен ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** Тест прерван ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>Пропустить сохранение профилей</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>Вы можете сохранить конфигурацию , используемую для выполнения данного испытания.&#xA;&#xA; Она может быть использована ( целиком или посредством выбора отдельных &#xA; профилей ) для настройки будущих испытаний. </translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>Пропустить загрузку профилей</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>Проверка локального SFP</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Разъем</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>Пожалуйста, вставьте модуль SFP.</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>SFP длина волны (нм)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>Поставщик SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>Номер редакции поставщика SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>P/N SFP </translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>Рекомендуемая скорость</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>Отображение дополнительных данных модуля SFP </translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>Модуль SFP в порядке.</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>Не удалось проверить SFP для этой скорости.</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>Работа SFP неприемлема.</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Настройки</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Длительность теста</translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>Устройство на дальнем конце</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Другое</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>Аппаратная петля</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Да</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>Макс. лимит оптического Rx уровня (дб/мВт)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>Опт.l Rx предел мин. знач. (дБм)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>Макс. лимит времени передачи по шлейфу (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>Пропустить проверку CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>Проверка SFP</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>Испытание клавиши состояния</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Завершен .</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Прошел.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>По графику</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>Запустить &#xA; Тест</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Остановить &#xA; испытание</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>Результаты локального SFP</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>Модуль SFP не обнаружен.</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Длина волны ( нм )</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Максимальный уровень передачи (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Максимальный уровень приема (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Приемопередатчик</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>Результаты интерфейса</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>CPRI проверка вердиктов наборов тестовых данных</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>Тестирование интерфейса</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>Уровень 2 Test</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>Испытание RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>Испытание BERT</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>Сигнал</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Синхр. достигнута</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Максимальное отклонение частоты приема (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Нарушения кода</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>Уровень оптического приема (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>Результаты уровня 2:</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>Состояние запуска </translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>Кадровая синхр.</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>Результаты RTD</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>Двусторонняя задержка, средн. (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>Результаты BERT</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>Синхр. образца</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>Потери образца</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Ошибки Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>Настройки</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>Тип оборудования</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>L1 Синхронизация</translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>Установка протокола</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>Установка платы C&amp;M</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>Операция</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>Пассивная связь</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>Пропустить создание отчетов</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Информация об отчете по проведению теста</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>Имя абонента :</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>ID- Техник :</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>Местоположение испытания :</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>Заказ на выполнение работ :</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>Комментарии / Примечания :</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>Радио:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>Диапазон:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>Общий статус</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Расширен испытан FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>Испытание FC</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Подключиться</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Симметричн .</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>Локальные настройки</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>Подключиться к удаленной стороне</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Сеть</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>Настройки волоконнооптического канала</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>Тесты FC</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>Шаблоны конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Выбрать испытания</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>Использование</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Длина кадра</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Испытание производительности</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Испытание потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>Последовательный тест</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Испытание кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>Управления</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>Длительность тестов</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>Пороги тестов</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>Изменить конфигурацию</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>Расширенные настройки волоконнооптического канала</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>Расширенные настройки использования</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>Расширенные пропускные настройки времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>Расширенные последовательные настройки теста</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>Расширенные настройки теста потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>Выполнить испытания активации сервиса</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>Изменить конфигурацию и повторить испытание</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Потеря кадров</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Взаимн .</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Кредит буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Производительность кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>Завершить тест FC</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>Создать другой отчет</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>Титульная страница</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>Синхронизация источника времени с односторонней задержкой была утеряна во время выполнения испытания в локальном устройстве.  Проверьте соединения аппаратных средств источника времени OWD.  Испытание будет продолжено , если оно не завершено , но при этом результаты задержки кадров будут недоступны.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>Синхронизация источника времени с односторонней задержкой была утеряна во время выполнения испытания в удаленном устройстве.  Проверьте соединения аппаратных средств источника времени OWD.  Испытание будет продолжено , если оно не завершено , но при этом результаты задержки кадров будут недоступны.</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>Найден активный шлейф</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>Попытка получения адреса соседнего узла неуспешна.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>Услуга № #1: Отправка ARP запроса r для MAC.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>Сервис #1 на локальной стороне : Передача запроса ARP для MAC- адр. назначения</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>Сервис #1 на удаленной стороне : Передача запроса ARP для MAC- адр. назначения</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Передача запроса ARP для MAC- адр. назначения.</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>Локальная сторона передает запрос ARP для MAC- адреса назначения.</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>Удаленная сторона передает запрос ARP для MAC- адреса назначения.</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>Порт сетевого устройства используется в полудуплексном режиме работы. Для продолжения нажмите "Продолжить в полудуплексном режиме". Для отмены нажмите "Отменить тест".</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>Идет попытка установления восходящ. петли. Проверка активной петли.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>Проверка наличия аппаратной петли.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Проверка на наличие петли LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>Проверка наличия постоянной петли.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>Время запроса DHCP параметров истекло . Получение DHCP параметров оказалось невозможным .</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>В результате выбора режима проверки по шлейфу произошло отключение от удаленного устройства.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>Превышен лимит времени испытания SAMComplete, поскольку невозможно определить окончательное количество принятых кадров. Измеренное количество принятых кадров продолжает непредвиденно увеличиваться.</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>Найдена жесткая петля</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>Проверка J-QuickCheck не сможет выполнить испытание соединений для передачи трафика , пока не будет установлено соединение с удаленным устройством. </translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>Найдена петля LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>Утерян локальный канал связи , в связи с чем произошло нарушение соединения с удаленным устройством.  После повторного установления канала связи может быть выполнена повторная попытка соединения с удаленным концом.</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>Канал связи в настоящий момент неактивен.</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>IP- адрес источника совпадает с IP- адресом назначения. Измените IP- адрес источника или IP- адрес назначения.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>Локальное и удаленное приложения несовместимы. Локальное приложение является приложением , относящимся к Трафику , в то время как удаленное приложение по IP- адресу #1 является приложением , относящимся к Потокам.</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>Невозможно установить петлю.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>Не удалось установить соединение с удаленным устройством. Перейдите на страницу Соединение , проверьте IP- адрес назначения и нажмите кнопку Выполнить соединение с удаленным устройством. </translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Пожалуйста, перейдите на вкладку «Сеть», проверьте конфигурацию и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>Ожидание готовности оптического канала или его отсутствие.</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>Найдена постоянная петля</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>Время ожидания подключения PPPoE . Проверьте настройки PPPoE  и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>Произошла неисправимая ошибка PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Идет попытка входа в систему на сервере ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>Обнаружена проблема удаленного соединения.  Отсутствует доступ к удаленному устройству </translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>Не удалось установить соединение с удаленным устройством по IP- адресу #1. Проверьте IP- адрес удаленного источника и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>Удаленное приложение по IP-адресу #1 является приложением Loopback. Оно не совместимо с усовершенствованной версией RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>Локальное и удаленное приложения несовместимы. Удаленное приложение по IP- адресу #1 не является приложением , относящимся к TCP WireSpeed. </translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>Версия программного обеспечения на удаленном блоке новее . Если вы продолжите тестирование , некоторая функциональность может быть ограничена . Для оптимального тестирования рекомендуется обновить программное обеспечение на этом блоке .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>Версия программного обеспечения на удаленном блоке старее . Если вы продолжите тестирование , некоторая функциональность может быть ограничена . Для оптимального тестирования рекомендуется обновить программное обеспечение на блоке .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>Не удалось определить версию программного обеспечения на удаленном блоке . Если вы продолжите тестирование , некоторая функциональность может быть ограничена . Рекомендуется тестировать блоки с одинаковыми версиями программного обеспечения .</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>Неудачная попытка установлен. восходящ. петли. Идет повторная попытка.</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>Настройки для выбранного шалбона успешно применены .</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>Версия программного обеспечения на удаленном модуле не может быть определена или не соответствует версии на данном устройстве. При дальнейшем тестировании некоторые функциональные возможности могут быть ограничены. При тестировании модулей необходимо использовать идентичные версии ПО.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>Не удалось войти в систему.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>Локальное и удаленное приложения несовместимы . Локальное приложение является приложением Уровня #1, в то время как удаленное приложение по IP- адресу #2 является приложением уровня #3.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>Линейная скорость местного блока (#1 Мбит/с) не совпадает со скоростью удаленного устройства (#2 Мбит/с). Выполните перенастройку , на Asymmetric на странице "Connect".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>Линейная скорость местного блока (#1 кбит/с) не совпадает со скоростью удаленного устройства (#2 кбит/с). Выполните перенастройку , на Asymmetric на странице "Connect".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>Линейная скорость местного блока (#1 Мбит/с) не совпадает со скоростью удаленного устройства (#2 Мбит/с). Выполните перенастройку , на Asymmetric на странице "Connect" и перезапустите тест.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>Линейная скорость местного блока (#1 кбит/с) не совпадает со скоростью удаленного устройства (#2 кбит/с). Выполните перенастройку , на Asymmetric на странице "Connect" и перезапустите тест.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>Линейная скорость местного блока (#1 Мбит/с) не совпадает со скоростью удаленного устройства (#2 Мбит/с). Выполните перенастройку , на Asymmetric на странице RFC2544 "Symmetry".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>Линейная скорость местного блока (#1 кбит/с) не совпадает со скоростью удаленного устройства (#2 кбит/с). Выполните перенастройку , на Asymmetric на странице RFC2544 "Symmetry".</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Необходимо выбрать минимум один тест . Выберите хотя бы один тест и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>Вы не выбрали ни одного размера кадров для тестирования . Выберите хотя бы один размер кадра перед тем , как начать .</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>Скорость потери кадров , превышающая установленный порог потери кадров была обнаружена в последнюю секунду . Проверьте производительность ссылки с помощью ручной проверки трафика . После того как вы выполнили ручные тесты можно повторно запустить RFC 2544&#xA;Рекомендуемая ручная настройка теста : .&#xA;Размер кадра : #1 байт&#xA;трафика : постоянный с #2 Мбит&#xA;Продолжительность теста : Минимум в 3 раза больше настроенной продолжительности теста . Пропускная продолжительность теста была #3 секунды&#xA;Результаты для наблюдения : &#xA;Используйте экран Summary Status для поиска событий ошибок . Если счетчики ошибок увеличивая спорадической образом , запустите ручной тест с более низкими значениями трафика . Если вы все еще получаете ошибки даже на низких значениях , возможно , существуют общие проблемы , не связанные с максимальной нагрузкой . Вы также можете использовать графические результаты кадров , представленные на графике потери трафика , чтобы увидеть , есть ли спорадические или постоянные случаи потери кадров . Если вы не можете решить проблему с спорадическими ошибками , можно установить пороговое значение потери кадров , чтобы допустить небольшие уровни потерь кадров . Примечание : после использования порога потери кадров тест не будет соответствовать рекомендации RFC 2544 .</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>Примечание :  Ввиду разности глубины стеков VLAN для  передатчика и приемника , установленную скорость необходимо отрегулировать для компенсации разницы между портами .</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>#1 байтовые кадры</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>#1 байтовые пакеты</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>Счетчик кадров фильтра L2 Rx Acterna продолжает увеличиваться в течение последних 10 секунд , даже если трафик должен быть остановлен</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Обнуление при максимальной производительности</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>Попытка #1 L1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>Попытка #1 L2 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>Попытка #1 L1 кбит/с</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>Попытка #1 L2 кбит/с</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>Идет попытка #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>Производится верификация #1 L1 Мбит/с. Это займет #2 секунды .</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>Производится верификация #1 L2 Мбит/с. Это займет #2 секунды .</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>Выполняется проверка #1 L1 кбит/с. Это займет #2 секунды</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>Производится верификация #1 L2 Мбит/с . Это займет #2 секунды .</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>Выполняется проверка #1 %. Это займет #2 секунды</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>Максимальная определенная пропускная способность : #1 L1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>Максимальная определенная пропускная способность : #1 L2 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>Максимальная измеренная пропускная способность : #1 L1 кбит/с</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>Максимальная измеренная пропускная способность : #1 L2 кбит/с</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>Измерен . макс . производительность : #1</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Измерение максимальной производительности не предусмотрено</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>Тест потери кадров ( стандарт RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>Тест потери кадров ( сверху вниз )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>Тест потери кадров ( снизу вверх )</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>Проведение теста при загрузке #1 L1 Мбит / с . Это займет #2 секунды</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>Проведение теста при загрузке #1 L2 Мбит / с . Это займет #2 секунды</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>Выполняется тест при загрузке #1 L1 кбит / с Это займет #2 секунды</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>Выполняется тест при загрузке #1 L2 кбит / с Это займет #2 секунды</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>Идет выполнение испытания при нагрузке #1 %.  Этой займет #2 секунд</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Испытание взаимных кадров</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>Попытка #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Обнаружены кадры с паузой</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 – пакетный комплект : ошибка</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 – кадровый пакет : ошибка</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 – пакетный комплект : успешн</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 – кадровый пакет : успешн</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>Тест поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>Попытка пакета #1 кБ</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>Больше , чем  #1 кБ</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>Менее #1 кБ</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>Размер буфера - #1 kB</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>Размер буфера менее #1 кБ</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>Размер буфера более или равен #1 кБ</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>Отправлен #1 кадр</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>Принятые кадры #1</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>Потерянные кадры #1</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>Тест пакета (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>Предполагаемое CBS: #1 кБ</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>Предполагаемый CBS: недоступно</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>Тест CBS будет пропущен . Размер пакета теста слишком большой для корректного тестирования этой конфигурации .</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>Тест CBS будет пропущен . Длительность теста недостаточна для корректного тестирования этой конфигурации .</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>Разбивка пакета : не удалась</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>Разбивка кадров : не удалось</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>Разбивка пакета : успешно</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>Разбивка кадров : пройдено</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>Проверка ограничения импульсов #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>Испытание восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>Для этого размера пакета тест неверный</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>Для этого размера кадра тест неверный</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>Плпытка #1 из #2:</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>Вызвать потерю кадров не представляется возможным , поскольку испытание производительности было успешно выполнено при максимальной полосе пропускания без потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>Невозможно вызвать события потери . Для этого размера пакета тест неверный</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>Невозможно вызвать события потери . Для этого размера кадра тест неверный</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>Среднее время восстановления : Больше , чем  #1 секунд</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>Больше , чем  #1 секунд</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>Среднее время восстановления : Недоступно</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>Среднее время восстановления : #1 мкс</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 мы</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>Выверка по оптимальному буферу данных Тестирование #1 данных .</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>Испытание #1 кредитов</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>Идет проверка с #1 кредитами .  Этой займет #2 секунд</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Испытание производительности кредитов буфера</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>Примечание : предполагает замкнутую цепь с буферными кредитами менее 2. Этот тест является недействительным</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>Примечание : Исходя из предположения о наличии аппаратной петли , измерения производительности выполняются с использованием кредитов буфера , количество которых в два раза больше на каждой стадии для компенсации двойной длины волоконно - оптического канала связи .</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>Измерение производительности при #1 кредитах буфера</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>Тесты пропускной способности и времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Испытания производительности и джиттера пакета .</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>Jitter тесты полосы пропускания , времени задержки и пакета</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>Испытание задержки и искажение пакета #1. Это займет #2 секунды .</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>Тест на искажение пакета #1. Это займет #2 секунды .</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>Задержка тестовых испытаний #1.  Это займет #2 секунды .</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>Задержка тестовых испытаний #1 при #2% проверки пропускной нагрузки . Это займет #3 секунды</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 Запуск RFC 2544 теста #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 Запуск FC теста #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>Тест завершен</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Подождите, сохранение результатов.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>Тест длительной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>Испытание FC:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>Конфигурация сети</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Тип кадра</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>Режим испытан.</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>Уст . Уровень домена</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>Отправитель TLV</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Инкапсуляция</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Глубина яруса сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Приоритет пользователя сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>Бит DEI SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>TPID (hex) сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID сети SVLAN пользователя</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>Приоритет пользователя сети CVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Приоритет пользователя</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 7 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 6 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 5 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 4 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 3 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 2 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>Приоритет пользователя сети SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>Идентификационный код TPID (hex) сети SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>Пользовательский идентификационный код TPID (hex) 1 сети SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>Бит DEI сети SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Тип шлейфа</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>Тип Ether</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC источника</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>Авто - приращение SA MAC</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation>Количество MAC в последовательности</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>Отключить P EtherType</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>Отключить результаты OoS</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>Тип DA</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC назначения</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Режим данных</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>Аутентификация использования</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Имя пользователя</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Пароль</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>Поставщик услуг</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Имя службы</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>Тип IP- адреса источника</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>Шлюз по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>Маска подсети</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Срок службы ( переходы )</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>Приращение IP ID</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>Получить адрес локальной ссылки</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>Получить глобальный адрес</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Длина префикса подсети</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>Класс трафика</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>Метка потока</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>Ограничение переходов</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>Режим. трафика</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>Получить тип сервиса порта</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>Порт источника</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>Тип сервиса порта назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>Порт назначения</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>Тип IP- адреса прослушивания ATP</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation> IP- адрес прослушивания ATP</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>Идентификационный код последовательности</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>Идентифкационный код исходящей стороны</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>Идентифкационный код отвечающей стороны</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Конфигурация испытания</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Версия полезной нагрузки Acterna</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>Точность измерения задержки</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>Единица пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>Максимальная пропускная способность испытания ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>Высходящая максимальная пропускная способность испытания ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>Максимальная тестовая полоса пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>Верхняя максимальная тестовая полоса пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>Нисходящая максимальная пропускная способность испытания ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>Нисходящая максимальная тестовая полоса пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>Максимальная тестовая полоса пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>Разрешить верный 100% трафик</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>Точность измерения полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>Верхняя точность измерения полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>Нисходящая точность измерения полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Процесс обнуления при испытании производительности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>Допустимая потеря кадров при испытании производительности (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Верхняя погрешность потери кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Нисходящая погрешность потери кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>Любая длительность тестов</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>Все номера тестов попыток</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>Длительность полосы пропускания ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Порог успешного прохождения испытания производительности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>Порог пропускной способности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>Высходящий порог пропускной способности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>Порог полосы пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>Верхний порог полосы пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>Нисходящий порог пропускной способности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>Нисходящий порог полосы пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>Порог успешного прохождения испытания производительности (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>ККоличество попыток время задержки</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>Длительность попытки времени задержки ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>Ширина полосы задержки (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>Порог времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>Порог времени задержки ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>Верхний порог времени задержки ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>Нисходящий порог времени задержки ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Количество попыток испытания джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>Длительность попытки пакета Jitter ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Порог успешного прохождения испытания джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>Порог прохождения пакета Jitter ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>Верхний порог пакета прохождения Jitter ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>Нисходящий порог пакета прохождения Jitter ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Процедура испытания потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>Длительность попытки потери кадров ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Неравномерная потеря фрейма пропускной способности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Высходящая неравномерная потеря фрейма пропускной способности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Потеря кадров грануляции полосы пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Верхняя потеря кадров грануляции полосы пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Нисходящая потеря кадров грануляции полосы пропускания ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Нисходящая неравномерная потеря фрейма пропускной способности ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>Гранулярность полосы пропускания при испытании потери кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>Минимальный показатель потери фрейма ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>Область минимальной потери кадров ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>Область минимальной потери кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>Максимальный показатель потери фрейма ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>Область максимальной потери кадров ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>Область максимальной потери кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Высходящий минимальный показатель потери фрейма ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>Верхняя область минимальной потери кадров ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Высходящий максимальный показатель потери фрейма ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>Верхняя область максимальной потери кадров ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Нисходящий минимальный показатель потери фрейма ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>Нисходящая область минимальной потери кадров ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Нисходящий максимальный показатель потери фрейма ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>Нисходящая область максимальной потери кадров ( кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>Количество шагов потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>Количество последовательных попыток</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>Последовательная грануляция ( кадры )</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>Максимальная длительность при последовательной передачи пакетов ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>Верхняя длительность при последовательной передачи пакетов ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>Нисходящая длительность при последовательной передачи пакетов ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>Игнорировать кадры с паузой</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>Тип теста пакета</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>Размер пакета CBS  (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>Верхний размер пакета CBS  (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>Нисходящий размер пакета CBS  (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>Минимальный размер поиска пакета (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>Максимальный размер поиска пакета (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>Верхний минимальный размер пакета CBS  (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>Верхний максимальный размер поиска пакета CBS  (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>Нисходящий минимальный размер пакета CBS  (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>Нисходящий максимальный размер поиска пакета CBS (кБ)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>CBS продолжительность (сек)  </translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>Тестирование пакета . Количество испытаний</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>Показать результаты пройдено / не пройдено для пакета CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>Показать результаты пройдено / не пройдено для  поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>Пороговый размер поиска пакета ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>Порог верхнего размера поиска пакета ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>Порог нисходящего размера поиска пакета ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>Количество попыток восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>Длительность перегрузки восстановления системы ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>Время загрузки (с)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>Измерение производительности при загрузке (%)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>Увеличенная длина пакета</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>Увеличенная длина кадра</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>Тип входа данных буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>Максимальный размер буфера данных буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>Шаги пропускной способности данных буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>Длительность данных буфера</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>Успешн. удален. нисх. петля : устройство **#1** вне режима LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>Успешн. удален. нисх. петля : устройство **#1** вне прозрачного режима LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>Удален. устройство **#1** было направлено через нисх. петлю</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>Удаленный Loop Down из - за изменения конфигурации</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>Успешн. удален. восх. петля : устройство **#1** в режиме LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>Успешн. удален. восх. петля : устройство **#1** в прозрачном режиме LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>Удален. устройство **#1** уже находилось в режиме LLB</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>Удален. устройство **#1** уже находилось в прозрачном режиме LLB</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>Зацикливание или петля с другим портом не поддерживаются</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Поток #1: Неудачн. удаленн. нисх. петля : превышение лимита времени подтверждения</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Поток #1: Неудачн. удаленн. восх. петля : превышение лимита времени подтверждения</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Неудачн. удаленн. нисх. петля : превышение лимита времени подтверждения</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Неудачн. удаленн. прозрачн. нисх. петля : превышение лимита времени подтверждения</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Неудачн. удаленн. восх. петля : превышение лимита времени подтверждения</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Неудачн. удаленн. прозрачн. восх. петля : превышение лимита времени подтверждения</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>Запущено обнаружение повторяющегося адреса в рамках глобального адреса.</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>Ошибка настройки глобального адреса ( проверьте настройки ).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>Ожидание обнаружения повторяющегося адреса в рамках глобального адреса.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>Успешн. обнаружение повторяющ. адреса. Глобальный адрес является действительным.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>Неудачн. обнаружение повторяющ. адреса. Глобальный адрес является недействительным.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>Запущено обнаружение повторяющегося адреса в рамках локального адреса канала связи.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>Ошибка настройки локального адреса канала связи ( проверьте настройки ).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>Ожидание обнаружения повторяющегося адреса в рамках локального адреса канала связи.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>Успешн. обнаружение повторяющ. адреса. Локальный адрес канала связи является действительным.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>Неудачн. обнаружение повторяющ. адреса. Локальный адрес канала связи является недействительным.</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>Запущено получение IP- адреса без состояния.</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>Невозможно получить IP- адрес без состояния.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>IP- адрес без состояния получен успешно.</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>Запущено получение IP- адреса с состоянием.</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>Не обнаружены маршрутизаторы , способные предоставить IP- адрес с состоянием.</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>Идет повторная попытка получить IP- адрес с состоянием.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>IP- адрес с состоянием получен успешно.</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>Невозможно достигнуть сети</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>Невозможно достигнуть хост - устройства</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>Невозможно достигнуть протокола</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>Невозможно достигнуть порта</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>Слишком длинное сообщение</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>Неизвестная сеть назначения</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>Неизвестное хост - устройство назначения</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>Запрещенная сеть назначения</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>Запрещенное хост - устройство назначения</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>Недоступная сеть для TOS</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>Недоступное хост - устройство для TOS</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Превышение времени транзита</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>Превышение времени повторной сборки</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>Неизвестная ошибка ICMP</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>Невозможно достигнуть пункта назначения</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>Невозможно достигнуть адреса</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>Отсутствие пути к пункту назначения</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>Пункт назначения не является смежным пунктом</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>Связь с пунктом назначения запрещена администратором</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>Слишком большой пакет</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Превышен лимит переходов при транзите</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>Превышен. времени повторн. сборки фрагмента</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>Обнаружен. поле заголовка с ошибкой</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>Обнаружен неизвестн. тип следующ. заголовка</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>Обнаружен неизвестн. параметр IPv6</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>Неактивн.</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>Активн. PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>Активн. PPP</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>Вкл. сети</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Неактивн. запрашиваемый пользователь</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Уровень данных остановлен</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Превышение лимита времени PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Превышение лимита времени PPP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>Отказ PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>Ошибка PPP LCP</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Ошибка проверки подлинности PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>Неизвестн. ошибка PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>Ошибка PPP UP</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Недействительн. конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Внутренняя ошибка</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>ARP успешн. Получен MAC назначения</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>Ожидание службы ARP...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>Отсутствие ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Прервать</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Продолжить в полудуплексном режиме</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>Завершить испытание J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>Испытание RFC2544</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Восходящ.</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Нисходящ.</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>Испытание J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>Состояние удален. петли</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>Состояние локальной петли</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>Состояние PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>Состояние IPv6</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>Состояние ARP</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>Состояние DHCP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>Состояние ICMP</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>Соседний статус обнаружения</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>Состояние автоматической настройки</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>Состояние адреса</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>Обнаружение блока</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>Поиск блоков</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Поиск</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>Использовать выбранный блок</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>Выход из</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Закрыть</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>Соединен.&#xA; с удаленным устройством</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>Соединен.&#xA; с удаленным устройств.</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Разъединить</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>Подключение&#xA;к удаленному</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ВКЛ</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Идет подключен.</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Выкл .</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Симметрия</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Асимметричный</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>Направленный</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Контур обратной связи</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>Downstream и Upstream ( каналы приема и передачи данных )</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>В обеих направлениях</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>Пропускная способность :</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>Пропускная способность выходного и обратного потока одинакова .</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>Пропускная способность Downstream и Upstream разная .</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>Проверка сети только в одном направлении .</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>Измерения :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>Трафик генерируется и оценивается локально .</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>Трафик генерируется и оценивается локально и удаленно .</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>Направление измерения :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>Трафик генерируется локально и оценивается удаленным испытательным прибором .</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>Трафик генерируется удаленно и оценивается локальным испытательным прибором .</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>Задержка полного обхода</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Односторонняя задержка</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Можно выполнить только измерения задержки полного хода . &#xA; Удаленный блок не может выполнить одностороннюю задержку .</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Можно выполнить только однонаправленные измерения задержки . &#xA; Удаленный блок не может выполнить двунаправленный RTD.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>Нельзя выполнить измерения с задержкой . &#xA; Удаленный блок не может выполнить двунаправленный RTD или одностороннюю задержку .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Нельзя выполнить измерения с задержкой . &#xA; Удаленный блок не может выполнить одностороннюю задержку .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Нельзя выполнить измерения с задержкой . &#xA; Удаленный блок не может выполнить двунаправленный RTD.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Можно выполнить только однонаправленные измерения задержки . &#xA;RTD не поддерживается при однонаправленном тестировании .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Нельзя выполнить измерения с задержкой . &#xA;RTD не поддерживается при однонаправленном тестировании .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Нельзя выполнить измерения с задержкой . &#xA; Удаленный блок не может выполнить одностороннюю задержку .&#xA;RTD не поддерживается при  однонаправленном тестировании .</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>Нельзя выполнить измерения с задержкой .</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>Тип измерения времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Локальн.</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Дистанцион.</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>Требуется использование на удаленной стороне средства для проведения испытаний Viavi. </translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>Версия 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>Версия 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Калибровка RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>Выбор оптики</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>IP настройки для канала связи с дальним концом</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>Для теста Loopback нет необходимых настроек локального IP адреса .</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Статическ.</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>Статич. - на каждый сервис</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>Статич. - отдельный</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Ручн.</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>С внутрен. состоянием</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Без внутрен. состоян.</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>Локальн. адр. канала связи источника</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>Глобальный адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>Автоматич. прием</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>Пользователем</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC- адрес</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>Режим ARP</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>Тип адреса источника</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>Режим PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>Расширенный</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Включен.</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Выключен</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>MAC источника абонента</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>MAC источника по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>MAC абонента по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>MAC источника пользователя</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>MAC пользователя абонента</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>Пропустить соединение</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>Ярусная сеть VLAN</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>Сколько виртуальных локальных сетей используется на местном сетевом порту ?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>Нет VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ виртуальные локальные сети ( виртуальные локальные сети со стеком )</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>Ввести настройки идентификатора VLAN:</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>Глубина стека</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>Пользователь</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>Настройки сервера обнаружения</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>ПРИМЕЧАНИЕ: Link-Local IP назначения не может использоваться для «Подключения к удаленному устройству»</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>IP глобального назначения</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Передача команды Ping</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Выдать команду Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>Помогите мне найти  &#xA;IP назначения</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Локальн . порт</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Неизвестн. состоян.</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>UP (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>UP (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>UP (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>UP (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>UP (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>UP (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>UP (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>UP (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>UP (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Потеря канала связи</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>Локальный порт :</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Автосогласование</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Идет анализ</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>ВЫКЛ</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Автосогласование :</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>Ожидание подключения</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Идет подключен....</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>Повторить ...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>ПОДКЛЮЧЕН</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>Подключение не установлено</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>Соединение прервано</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>Канал связи:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>Расширенные настройки сервера обнаружения</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>Получить IP назначения из сервера обнаружения</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>IP- адрес сервера</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>Порт сервера</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>Фраза - пароль к серверу</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>Запрашиваемое время аренды ( мин .)</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>Время предоставляемой аренды ( мин .)</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>Расширенные настройки сети L2 - местные</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>Настройки локального устройства</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>Трафик</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>Трафик LBM</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 ( самый низкий )</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 ( самый высокий )</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>TPID сети SVLAN пользователя</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>Задать тип цикла, EtherType и MAC-адреса</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>Задать MAC адреса, EtherType и LBM</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>Установить MAC- адреса</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>Задать MAC- адреса и ARP Режим</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Тип назначения</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>Одноадресн.</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>Многоадресн.</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>Широковещат.</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>Приоритет пользователя сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>Удаленное устройство не подключено.</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>Расширенные настройки сети L2 - удаленные</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>Настройки удаленного устройства</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>Расширенные настройки сети L3 - местные</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>Задайте класс передаваемых данных, метку канала и максимальный сетевой сегмент</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>Какой тип приоретизации IP используется вашей сетью ?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>Установить время на Live (Актуальное), IP ID приращение и режим PPPoE </translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>Установить на режим реального времени и PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>Установить время на Live (Актуальное) и IP ID приращение</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>Установить на режим реального времени</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>Какая приоретизация IP используется вашей сетью ?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>Расширенные настройки сети L3 - удаленные</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>Расширенные настройки сети L4 - местные</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>Тип службы источника</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>DHCP- клиент</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>DHCP- сервер</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>Указат.,</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Время</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>Тип службы назначения</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>Установить ATP слушающий IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>Расширенные настройки сети L4 - удаленные</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>Установить тип петли и идентификаторы последовательности , ответчика и отправителя</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>Расширенные настройки L2 - местные</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Тип источника</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC пользователя</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>Конфигурация LBM</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>Тип LBM</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>Включить&amp;#xA;отправитель TLV</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Вкл</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>Расширенные настройки L2 - удаленные</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>Расширенные настройки L3 - местные</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Срок службы ( переходы )</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>Расширенные настройки L3 - удаленные</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>Расширенные настройки L4 - местные</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>Расширенные настройки L4 - удаленные</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>Расширенные настройки сети – локальное устройство</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>Шаблоны</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>Вы хотите использовать этот шаблон конфигурации ?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Повышеная скорость доступа к пакетам Viavi > Скорость транспортировки</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - лучшая попытка</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - конфиденциально</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - глобально</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - метро</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - мобильное транзитное соединение H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - регионально</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - эмуляция данных IP- телефонии</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>RFC2544 бит прозрачно</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>RFC2544 скорость доступа к пакету  > скорость транспортировки</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>RFC2544 скорость доступа к пакету = скорость транспортировки</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>Применить шаблон</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>Конфигурации шаблоны :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>Потеря кадров полосы пропускания (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>Порог времени задержки ( мкс ): #1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation> Порог джиттера пакета ( мкс ): #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>Размеры кадров : По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>Максимальная пропускная способность (верхняя) #1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>Максимальная пропускная способность (нисходящая) #1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>Максимальная пропускная способность : #1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>Тесты : тесты пропускной способности и времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>Тесты : пропускная способность , время задержки , потеря кадров и последовательность</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>Длительность и попытки #1 секунд с 1 попыткой</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>Алгоритм пропускной способности : улучшенный Viavi</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>Алгоритм пропускной способности : стандарт RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>Порог полосы пропускания (верхний) #1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>Порог полосы пропускания (нисходящий) #1 Мбит/с</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>Порог производительности: #1 (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>Алгоритм потери кадров : Стандарт RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>Время при последовательной передачи пакетов : 1 сек</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>Последовательная грануляция 1 кадр</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Испытания</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>Установить расширенные настройки использования</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>Максим. полоса пропуск</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>Максимальная нисходящая частота обновления (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>Максимальная нисходящая частота обновления (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>Максимальная нисходящая частота обновления (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>Максимальная нисходящая частота обновления (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>Нисходящая максимальная полоса пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>Максимальная восходящая частота обновления (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>Максимальная восходящая частота обновления (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>Максимальная восходящая частота обновления (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>Максимальная восходящая частота обновления (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>Верхняя максимальная полоса пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>Максим. полоса пропуск (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>Максим. полоса пропуск (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>Максим. полоса пропуск (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>Максим. полоса пропуск (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Максим. полоса пропуск (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>Выбранные кадры</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>Тип длины</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Кадр</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>Сброс</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Очистить все</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Выбрать все</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Длина кадра</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Длина пакета</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>Высходящая &#xA; длина фрейма</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>Удаленное устройство не подключено</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>Нисходящая &#xA; Длина кадров</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>Удаленное &#xA; устройство &#xA; не &#xA; подключено</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>Выбранные тесты</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>Тесты RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>Время задержки ( требуется полоса пропускания )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>Кредит буфера ( требуется Производительность )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>Производительн . кредита буфера ( требуется Кредит буфера )</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>Восстановление системы ( только обратная петля и требуется пропускная способность )</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>Дополнительные тесты</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>Джиттер пакета ( требуется Производительность )</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>Тестирование пакета </translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>Длительная нагрузка ( только обратная петля )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>Конфигурация пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Процесс обнуления</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>Стандартн . RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Расширен . Viavi</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>Задать усовершенствованные настройки измерения пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>Нисходящая точность измерения</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Точность низходящего измерения (L1 Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Нисходящая точность измерения  (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>Нисходящая точность измерения (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>Нисходящая точность измерения (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>Нисходящая точность измерения (%)</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>до 1.0%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>до 0.1%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>до 0.01%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>С точностью до 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>в рамках 400 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>в рамках 40 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>в рамках 4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>в рамках .4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>в рамках 1000 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>в рамках 100 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>в рамках 10 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>в рамках 1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>в рамках .1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>в рамках .01 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>в рамках .001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>С точностью д .0001 Мбит / с </translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>в рамках 92.942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>С точностью д 9.2942 Мбит / с </translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>в рамках .9294 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>в рамках .0929 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>в рамках 10000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>в рамках 1000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>в рамках 100 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>в рамках 10 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>в рамках 1 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>в рамках .1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>Верхняя точность измерения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Точность восходящего измерения (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Высходящая точность измерения  (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>Верхняя точность измерения (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>Верхняя точность измерения (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>Верхняя точность измерения (%)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Точность измерения</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>Точность измерений (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>Точность измерений (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>Точность измерения (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>Точность измерения (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>Точность измерения (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>Дополнительная информация</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>Поиск и устранение неисправностей</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>Пуско-наладочные работы</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>Усовершенствованные настройки пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>Продолжительность теста задержки / искажения пакета конфигурируется отдельно</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>Продолжительность теста задержки конфигурируется отдельно</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>Продолжительность теста искажения пакета конфигурируется отдельно</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>Конфигурация потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Процедура испытания</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Сверху вниз</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Снизу вверх</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>Количество шагов</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 Мбит/с за один этап</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1  кбит/с за шаг</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % строчного тарифа за шаг</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>Нисходящий диапазон проверок (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>Нисходящий диапазон проверок (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>Нисходящий диапазон проверок (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>Нисходящий диапазон проверок (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>Нисходящий диапазон теста (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Мин.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Макс.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Нисходящая грануляция полосы пропускания (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Нисходящая грануляция полосы пропускания (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Нисходящая грануляция полосы пропускания (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Нисходящая грануляция полосы пропускания (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>Нисходящая грануляция полосы пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>Восходящий диапазон проверок (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>Восходящий диапазон проверок (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>Восходящий диапазон проверок (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>Восходящий диапазон проверок (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>Верхний диапазон теста (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Верхняя грануляция полосы пропускания (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Верхняя грануляция полосы пропускания (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Верхняя грануляция полосы пропускания (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Верхняя грануляция полосы пропускания (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>Верхняя грануляция полосы пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>Тестовый интервал (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>Тестовый интервал (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>Диапазон теста (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>Диапазон теста (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Диапазон испытаний (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>Гранулярность полосы пропускания (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>Гранулярность полосы пропускания (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>Детализация пропускной способности (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>Детализация пропускной способности (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Гранулярность полосы пропускания (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>Установить расширенные настройки измерения потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>Расширенные настройки потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>Оциональные тестовые измерения</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>Измерение времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>Измерение пакета Jitter</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>Последовательность Cfg</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>Нисходящая максимальная длительность пакетов ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>Верхняя максимальная длительность пакетов ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>Максимальная длительность пакетов ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>Гранулярность пакетов ( кадры )</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>Установить расширенные последовательные настройки</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>Правила обработки кадров с паузой</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>Конфигурация данных буфера</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Тип входа в систему управления потоком</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>Внутренний ( прозрачная ссылка )</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Явный (E- порт )</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Макс размер буфера</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>Шаги пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>Средства управления испытаниями</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>Настроить длительность тестов отдельно ?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>Длительность</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Количество попыток</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Джиттер пакета</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>Задержка / искажение пакета </translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Пакет (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Восстановление системы</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Все испытания</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>Показать результаты&#xA;пройдено / не пройдено</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>Высходящая &#xA; пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>Нисходящий &#xA; Порог</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Порог</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>Порог пропускной способности (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>Порог пропускной способности (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>Порог пропускной способности (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>Порог пропускной способности (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation> Порог производительности (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>Удаленное устройство &#xA; не подключено.</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>Задержка RTD (мкс)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>Задержка OWD (мкс)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>Джиттер пакета ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>Размер поиска пакета (кБ) </translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>Пакет (CBS ограничения )</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>Высокое разрешение – низкая задержка</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>Низкое разрешение – высокая задержка</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>Запустить FC тесты</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>Пропустить тесты FC</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>вкл</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>выкл</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>Производительность &#xA; кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>График теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Верхний график теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Результаты испытания производительности :</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Верхние результаты теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Нисходящий график теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Нисходящие результаты теста полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>Отклонения пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>Верхние отклонения пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>Нисходящие отклонения пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>Результаты полосы пропускания</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>Пропускная способность (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>Пропускная способность (кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Производительность (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Кадр 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>L3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>L4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Кадр 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Кадр 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Кадр 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Кадр 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Кадр 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Кадр 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Кадр 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Кадр 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Кадр 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>Успешн. / Неудачн.</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>Длина кадра &#xA;( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>Длина пакета &#xA;( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>Измеренная &#xA; скорость (Мбит/ с)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>Измеренная &#xA; скорость (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>Измеренная &#xA; скорость (кадров/с)</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>Обнаружение &#xA;R_RDY</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Скорость конфигурац &#xA;(Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>Измеренный L1&#xA; скорость (кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>Измеренный L1&#xA; показатель (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>Измеренный L1&#xA;(% линейной скорости)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>Измеренный L2&#xA; показатель (кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>Измеренный L2&#xA; показатель (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>Измеренный L2&#xA;(% линейной скорости )</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>Измеренный L3&#xA; показатель (кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>Измеренный L3&#xA; показатель (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>Измеренный L3&#xA;(% линейной скорости )</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>Измеренный L4&#xA; показатель (кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>Измеренный L4&#xA; показатель (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>Измеренный L4&#xA;(% линейной скорости )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>Измеренная скорость &#xA; (кадров/с)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>Измеренная скорость &#xA; (пакета/с)</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>Обнаружение &#xA; паузы</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>CFG показатель &#xA;(L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>CFG показатель (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Скорость конфигурации &#xA;(L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Скорость конфигурации &#xA;(L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Отклонения от нормы</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>Обнаружен счет кадров &#xA;OoS</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Полезная нагрузка Acterna&#xA; Обнаружена ошибка</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>FCS&#xA; Обнаружена ошибка</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>Обнаружена ошибка &#xA; контрольной суммы IP</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>Обнаружена ошибка &#xA; контрольной суммы TCP/UDP</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>Тест времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>График теста  времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Верхний график теста времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Результаты теста времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Верхние результаты времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Нисходящий график теста времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Нисходящие результаты времени задержки</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>Задержка &#xA;RTD (мкс)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>Задержка &#xA;OWD (мкс)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>Измеренный &#xA;% линейной скорости</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>Обнаружение &#xA; паузы</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>Результаты испытаний потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>Результаты тестирования верхней потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>Результаты тестирования нисходящей потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>Результаты потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Кадр 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>CFG показатель ( настройки ) (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>CFG показатель настройки (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>Настроенная скорость (L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>Настроенная скорость (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>Настроенная скорость (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Потеря кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>Показатель пропускной способности &#xA;(L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>Показатель пропускной способности (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>Скорость полосы пропускания &#xA;(L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>Показатель пропускной способности (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>Скорость полосы пропускания &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>Частота потери кадров &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>Потерянные кадры</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>Макс . ср . пакет &#xA;Jitter ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>Обнаружение &#xA; Ошибки</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>Обнаружение &#xA;OoS</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Настр . скорость &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Результаты испытания взаимного процесса</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Верхние последовательные настройки теста</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Нисходящие последовательные настройки теста</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>Средние &#xA; кадры пакета</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>Средние &#xA; секудны пакета</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>Результаты испытаний кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Внимание</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>Минимальный размер буфера &#xA;( данные )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>Результаты испытания производительности кредита буфера</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>Результаты пропускной способности данных буфера</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>Пропускная способность (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>Данные  0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>Данные  1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>Данные  2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>Данные  3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>Данные  4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>Данные  5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>Данные  6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>Данные  7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>Данные  8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>Данные  9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>Размер буфера &#xA;( данные )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>Измеренная скорость &#xA;( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>Измеренная скорость &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>Измеренная скорость &#xA; ( кадров / с )</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>Контрольная проверка J - Тест на проницаемость Ethernet L2</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>Кадры J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>Выполнить испытание</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>Выполнить испытание J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>Конец : подробные результаты</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>Контрольное испытание выхода  J</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Примен.</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>Конфигурация типов форматов для проведения теста на проницаемость Уровня канала передачи данных</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Название</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   Протокол   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>Протокол</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> Тип кадра </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> Инкапсуляция </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>В стеке</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>Размер &#xA; кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Размер кадра</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Количество</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>Скорость &#xA;( кадры / сек )</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Частота</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>Превышение лимита времени &#xA;( мс )</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>Тайм-аут</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Управление</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Тип</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>Бит&#xA;DEI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>TPID сети SVLAN</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>TPID сети SVLAN пользователя</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>Автоматич. увелич. бита CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>Автоматич. увелич. бит CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>Автоматич. увелич. бита SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>Автоматич. увелич. бит SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>Инкапсуляц.</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>Инкапсуляц.</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>Тип &#xA; кадра</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>Идент. номер сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Увеличен. бита Pbit</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>Быстрая &#xA; настройка</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>Добавить &#xA; кадр</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>Удалить &#xA; кадр</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>Адресс источника общий для всех фреймов . Настройка осуществляется на странице Local Settings ( Локальные настройки ).</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Увеличение бита PBit </translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>Ярус сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>Увеличение SPbit</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Длина</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Данные</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>Ok</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>Конфигурация J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Интенсивность</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Быстр. (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>Полн. (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Семейство</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Все</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>Связующее дерево</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>Лазер &#xA; вкл.</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>Лазер &#xA; выкл.</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>Запустить последовательность &#xA; кадров</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>Остановить последовательность &#xA; кадров</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>Испытание J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>ОСТАНОВЛЕН.</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>ВЫПОЛНЯЕТСЯ</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>Ошибки загрузки</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>Ошибки заголовка</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>Несовпаден. колич.</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Остановлен</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>Итог результатов</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Незанят.</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>Выполняется</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>Несовпадение полезной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>Несовпадение заголовка</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>Результаты J-Proof</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>Название</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Статус</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>Сброс &#xA; оптич. приема</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>Выполнить QuickCheck Тест</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>Настройки QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>QuickCheck расширенные результаты нагрузочного тестирования</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Сведения</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Отдельн.</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>На один поток</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>ИЗ_ТЕСТА</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>Испытание производительности:</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>Результаты QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Результаты тестов длительной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Счет кадров Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Количество кадров приема</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>Количество кадров с ошибками</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>Счет кадров OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>Счет потерянных кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Коэффициент потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Оборудование</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Постоян.</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Активн.</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Скорость: %1 Мбит/с (L1), продолжительность: %2 секунд, размер кадра: %3 байт</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Пропускная способность : %1 кбит / с (L1), Продолжительность : %2 секунд , Размер кадра : %3 байт</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>Это не то , что вы хотели ?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Остановить</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Начать</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Успешн .</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>Поиск целей</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>Состояние ARP:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Петля удален . сторон .</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Проверка активной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Проверка аппаратной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Проверка наличия постоянной петли</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Проверка петли LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Активная петля</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Постоян. петля</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Петля LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>Ошибка петли</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>Удаленная петля :</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>Измеренная пропускная способность (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Не определен .</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 L2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Недоступно</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>Измеренная пропускная способность CIR (L1 кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>Информация об ошибках</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>Нагрузка (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>Загрузить (L1 кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>Длительность теста на пропускную способность (в секундах)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>Размер кадра (байт)</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Скорость передачи битов</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>Выполнить тест длительности</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 секунд</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 секунд</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 минута</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 минут</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 минут</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 минут</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 час</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 часов</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 часов</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 часов</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>Определенный пользователем</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>Длительность теста (сек)</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>Длительность испытания производительности ( минуты )</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Длина кадра ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>увелич. размера</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>Пользовательская длина</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>Большая длина</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  Порт: 10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>Длина волны лазера</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>Электрич. разъем</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>Оптич. разъем</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>Детали ...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Акт. связи</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Результаты трафика</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Подсчет ошибок</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Кадры с ошибками</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Кадры OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Потерянные кадры</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>Информация об интерфейсе</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>Сведения о модуле SFP/XFP</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>Длина волны (нм): Настраиваемое устройство SFP, см. порядок настройки длины канала.</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>Длина кабеля (м)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>Настройка поддерживается</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>Длина волны ; Канал</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Длина волны</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Канал</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>Рекомендуемая скорость&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>Номинальная скорость (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>Миним. скорость (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>Максим. скорость (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Поставщик</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>PN поставщика</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Номер редакции поставщика</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Тип уровня мощности</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Диагностический контроль</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Диагностический байт</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>Конфигурация JMEP</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>Длина волны (нм): Настраиваемое устройство, см. порядок настройки длины канала.</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>Сведения о модуле CFP/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>Сведения о модуле CFP2/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>Информация об интерфейсе CFP4/QSFP</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN поставщика</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Код даты</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Код участка</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>HW / SW Версия №</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>MSA HW Spec. Rev#</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>MSA Mgmt. I/F Rev#</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Категория мощности</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Тип уровня мощности Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Rx Макс . мощность Lambda ( дБм )</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Макс. мощность TX лямбды (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>Количество активных волокон</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Длина волны для волокна</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>Diagnositc Byte</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL для диапазона волокна (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Макс . Скорость передачи полосы Ntwk (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>Модуль ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Поддерживаемые скорости</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Номинальная длина волны (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Номинальная скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** Рекомендуется использовать для приложений 100GigE RS-FEC ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>CFP Expert</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>CFP2 Expert</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>CFP4 Expert</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Активировать обратную петлю Viavi</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>Включить замыкание на себя CFP Viavi</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>Включить экспертный режим CFP</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>Включить экспертный режим CFP2</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>Включить Экспертный режим CFP4 </translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>Предварительная коррекция</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>CFP Tx предыскажение</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>По умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>Низк .</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>Номинальный</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>Высок.</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>Делитель частоты</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>CFP Tx делитель частоты</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>Отклонение офсета ( байты )</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>CFP Tx смещенная расфазировка</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>Инвертировать полярность</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>CFP Тx инвертировать полярность</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Возврат Tx FIFO</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>CFP4 Rx</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>Выравнивание</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>CFP Rx выравнивание</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>CFP Rx инвертировать полярность</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>Игнорировать LOS</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>CFP Rx игнорировать LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Возврат Rx FIFO</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>QSFP Expert</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>Включить экспертный режим QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx игнорировать LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>CDR пропуск</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>Прием</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR получение обхода</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>Передача</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR передача обхода</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>CDR передавать и получать контроль над пропуском будет возможно, если модуль QSFP поддерживает его.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>Разработка</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>XCVR core шлейф</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>Замыкание на себя ядра CFP XCVR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>XCVR Line шлейф</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>Замыкание на себя линии CFP XCVR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>Модуль узла замыкания</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>CFP Module Host Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>Модуль сетевого замыкания</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>CFP Module Network Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>CFP Gearbox System Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>CFP Gearbox Line Loopback</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>Идентификатор чипа коробки передач </translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Rev чипа коробки передач</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Ucode Ver коробки передач</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>Ucode CRC коробки передач</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Полосы системы коробки передач</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Полосы линии коробки передач</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>Полосы узла CFPn</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>Полосы сети CFPn</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>Замыкание на себя ядра QSFP XCVR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>Замыкание на себя линии QSFP XCVR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO Peek DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO Peek PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO Poke DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO Poke PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>MDIO Poke Value</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>Активировать / деактивировать регистрацию органов управления A013 для канала лазера.</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C Peek PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C Peek DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C Poke PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C Poke DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>I2C Poke Value</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>Рег. 0x56 контроля для полос. лазера включение/отключение.</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP+ подробные данные</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Сигнал</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Такт. сигнал. передачи</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>Источник тактовых импульсов</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>Внутрен.</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>Восстановлен.</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>Внешн.</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>Внешн. 1.5M</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>Внешн. 2M</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>Внешн. 10M</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>Восстановлен. удален.</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Модуль синхронизации</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>Источник VC-12</translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>Внутрен. – сдвиг частоты (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>Удален. тактовые сигналы</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>Настраиваемое устройство</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>Режим настройки</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>Частота</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>Частота ( ГГц )</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>Первая настраиваемая частота ( ГГц )</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>Последняя настраиваемая частота ( ГГц )</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>Шаг сетки ( ГГц )</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>Сигнал сдвига</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>Порог сигнала сдвига ( нс )</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>Требуется повторная синхронизация</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>Повторная синхронизация завершена</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>Калибровка RS-FEC закончена</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>Калибровка RS-FEC не удалась</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC обычно используется с оптикой SR4, PSM4, CWDM4</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Для выполнения калибровки RS-FEC выполните следующие действия (применительно к CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Вставить адаптер QSFP28 в CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Вставить трансивер QSFP28 в адаптер</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Вставить оптоволоконное петлевое устройство в трансивер</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Нажать КАЛИБРОВАТЬ</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Откалибровать</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Идет калибровка...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Повторная синхронизация</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Необходимо произвести повторную синхронизацию трансивера к тестируемому устройству (ТУ).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Изъять оптоволоконное петлевое устройство из трансивера</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Установление соединения с тестируемым устройством (TУ)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Включить лазер TУ</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Убедится в том, что присутствует зеленый сигнал светодиода CSAM</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Нажать Lane Resync</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>Lane&#xA;Resync</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>Результаты J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>Нажмите кнопку Пуск для запуска испытания J-QuickCheck роверка.</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>Продолжение в полудуплексном режиме.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>Проверка соединений для передачи трафика в восходящем направлении. </translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>Проверка соединений для передачи трафика в нисходящем направлении. </translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>Проверка соединений для передачи трафика успешно выполнена для всех сервисов.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>Не удалось успешно выполнить проверку соединений для передачи трафика в восходящем направлении для следующих сервисов : #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>Не удалось успешно выполнить проверку соединений для передачи трафика в нисходящем направлении для следующих сервисов : #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>Не удалось успешно выполнить проверку соединений для передачи трафика в восходящем направлении для следующих сервисов : #1 и в нисходящем направлении для следующих сервисов : #2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>Не удалось успешно выполнить проверку соединений для передачи трафика в восходящем направлении.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>Не удалось успешно выполнить проверку соединений для передачи трафика в нисходящем направлении.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>Не удалось успешно выполнить проверку соединений для передачи трафика в восходящем или нисходящем направлениях.</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>Проверка соединений</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>Соединения при передаче трафика :</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>Запустить QC</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>Остановить QC</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Самопроверка оптики</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>Конец: Сохранить профили</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Отчеты</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>Оптика</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>Выход из самопроверки оптики</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>Оптический сигнал потерян Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>Обнаружен низкий уровень мощности . Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>Обнаружен высокий уровень мощности . Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>Обнаружена ошибка. Тестирование было прервано.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>Тест НЕ ПРОЙДЕН , потому что было обнаружено чрезмерное отклонение .</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>Тест НЕ ПРОЙДЕН , потому что было обнаружено чрезмерное отклонение . Тест прерван</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>Обнаружена ошибка в разряде Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>СБОЙ тестирования вследствие превышения BER заданного порогового значения.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>СБОЙ при тестировании из-за потери синхронизации.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>Не удается прочесть вставленный QSFP+.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>Не удается прочесть вставленный CFP.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>Не удается прочесть вставленный CFP2.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>Не удается прочесть встав. CFP4.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>Обнаружена ошибка корректируемого бита RS-FEC.  Тестирование было прервано.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>Обнаружена ошибка некорректируемого бита RS-FEC.  Тестирование было прервано.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>Калибровка RS-FEC не удалась</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>Обнаружена ошибка отклонения от частоты ввода</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>Обнаружена ошибка отклонения от частоты вывода</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Синхронизация потеряна</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>Обнаружены нарушения ключа</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>Потеряна блокировка путевого указателя кривых</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>Обнаружены неверные маркеры выравнивания</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>Обнаружены ошибки в разряде BIP 8 AM</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>Обнаружены ошибки блока BIP</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>Обнаружен сдвиг</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>Обнаружены ошибки блока</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>Обнаружены кадры маленького размера</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>Обнаружен удаленный дефект</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1  Обнаруженные ошибки в разряде</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>Потеря синхронизации образца</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>Порог BER  нельзя точно измерить из - за потери синхронизации образца</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>Измеренный BER превысил порог выбранного BER</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>Обнаружен  LSS</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>Обнаружены ошибки FAS</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>Обнаружен выход из кадра</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>Обнаружена потеря кадра</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>Потеря синхронизации кадра</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>Обнаружен выход из логического маркера дорожки</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>Обнаружены ошибки логических разделительных линий</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>Обнаружена потеря выравнивания дорожки</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>Потеряно выравнивание дорожки</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>Обнаружены ошибки MFAS</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>Обнаружен выход из дорожки</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>Обнаружен  OOMFAS</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>Обнаружен выход из восстановления</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>Обнаружен чрезмерный сдвиг</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>Калибровка RS-FEC прошла успешно</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>Тест оптики</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>Тип испытания</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>Тест использует 100GE RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>Тип оптики</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>Общее заключение теста</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>Тест присутствия сигнала</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>Тест уровня оптического сигнала</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>Тест чрезмерного сдвига</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>Тест на ошибки в разряде</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>Тест на общие ошибки</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>Тестирование порога BER</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>Оптическая мощность</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Rx Уровень Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Rx Уровень Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Rx Уровень Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Rx Уровень Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Rx Уровень Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Rx Уровень Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Rx Уровень Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Rx Уровень Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Rx Уровень Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Rx Уровень Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Rx Уровень Sum (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Tx Уровень Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Tx Уровень Lambda #2(dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Tx Уровень Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Tx Уровень Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Tx Уровень Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Tx Уровень Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Tx Уровень Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Tx Уровень Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Tx Уровень Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Tx Уровень Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Tx Level Sum (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>Настройки</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>Подсоедините короткий , чистый коммутационный кабель между Tx и Rx терминалами соединителя , который вы хотите протестировать .</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>Тесты оптики &#xA;CFP</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>Проверка&#xA;оптической системы CFP2/слота</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Прервать испытание</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>Тесты оптики &#xA;QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>Тест. QSFP28&#xA;оптики</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Настройки:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>Рекомендуемое</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 минут</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 минут</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 часов</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 часов</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>Длительность пользователя ( минуты )</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>Рекомендуемая продолжительность (в минутах)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>Рекомендуемое время для этой конфигурации - &lt;b>%1&lt;/b> минут .</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>Тип порога BER</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Post-FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Pre-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>Порог BER</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>Активировать линейное смещение PPM</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>Максимальный сдвиг PPM (+/-)</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>Ошибка остановки</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>Обзор результатов</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>Тип оптики/слота</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>Текущее смещение PPM</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>Текущ. BER</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>Оптическая мощность (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Rx Уровень Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Rx Level Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Rx Level Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Rx Level Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Rx Level Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Rx Level Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Rx Level Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Rx Level Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Rx Level Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Rx Level Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Rx Уровень Sum</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Tx Level Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Tx Level Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Tx Level Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Tx Level Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Tx Level Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Tx Level Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Tx Level Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Tx Level Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Tx Level Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Tx Level Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Tx Level Sum</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>Информация об интерфейсе CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>Информация об интерфейсе CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>Информация об интерфейсе CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>Информация об интерфейсе QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>QSFP+ информация об интерфейсе</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>Информация об интерфейсе QSFP28</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>Нет QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>Невозможно распознать модуль QSFP – переустановите модуль QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>Ошибка контрольной суммы QSFP</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>Невозможно опросить требуемые регистры QSFP.</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>Невозможно подтвердить пользователя QSFP.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>Проверка OTN</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>Длительность и полезная нагрузка BERT тест</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>Транспарентность GCC</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>Выбрать и выполнить испытания</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>Дополнительные параметры</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>Выполнение тестов  OTN</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>BERT полезной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>Проверка выхода OTN</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>Частота измерения</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Состояние автосогласования</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>Конфигурация RTD</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>Все полосы</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>Проверка OTN</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** Starting OTN Check Test ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>Обнаружена битовая ошибка полезной нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>Более 100 000 битовых ошибок полезной нагрузки BERT обнаружено </translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>Превышен порог BER полезной нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 Обнаружены битовые ошибки полезной нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>коэффициент биовых ошибок полезной нагрузки BERT: #1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>Превышен порог RTD </translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: Min: #3, макс. значение: #4, среднее значение: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD недоступен</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>Выполнение теста полезной нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>Выполнение теста RTD</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>Выполнение теста транспарентности GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>Обнаружена битовая ошибка GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>Превышен порог BER GCC</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 GCC обнаруженные ошибки битов</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>Обнаружено более 100 000 битовых ошибока GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>Коэффициент битовых ошибок GCC: #1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>*** Запуск эхо-контроля ***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>*** Пропустить эхо-контроль ***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>*** Эхо-контроль завершен ***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>Обнаружено замыкание на себя</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>Замыкание на себя не обнаружено</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>Канал #1 недоступен</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>Канал #1 доступен</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>Потеря синхронизации шаблона</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>Потеря синхронизации GCC шаблона</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>Тест прерван: Обнаружена ошибка двоичного сигнала.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>Ошибка: BER превышает установленный порог.</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>Ошибка диагностики RTD превышает максимальный порог.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>Тест прерван: Замыкание на себя не обнаруженою</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>Тест прерван: Оптический сигнал отсутствует.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>Тест прерван: |Потеря кадра.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>Тест прерван: Потеря синхронизации кадров</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>Тест прерван: Потеря синхронизации шаблона</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>Тест прерван: Потеря синхронизации GCC шаблона</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>Тест прерван: Потеря выравнивания полосы.</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>Тест прерван: Потеря блокировки маркера.</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>Должен быть выбран хотя бы один канал.</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>Замыкание на себя не проверено</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>Замыкание на себя не обнаружено</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>Выбор теста</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>Запланированная продолжительность теста</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>Время выполнения тестирования</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>Проверка OTN требует шлейфа трафика; шлейф требуется на удаленном конце цепи OTN.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>Тесты сети OTN</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>Смещение оптики, структура сигнала</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>Структура сигнала</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>Структура сигнала и выбор оптики</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>Дополнительные настройки</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>Структура сигнала</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP Optics RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>CFP Optics RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 Optics RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>CFP4 сдвиг оптики RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>Настройка длительности и полезной нагрузки BERT тест</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>Настройка длительности и полезной нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>Настройка нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>Уровень распознавания (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>Учитывая скорость линии, порог BER и уровень распознавания, рекомендованное время теста составляет &lt;b>%1&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>образца</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>Последовательность BERT</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>Порог ошибок</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>Показать Годен/Не Годен</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>Насткройки Round Trip Delay</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>Настройка RTD</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>Включить</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>Порог (мс)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>Частота измерения (с)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>Настройка GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>Настройки транспарентности GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>Канал GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>Порог GCC BER</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>Шаблон BERT 2^23-1 используется в GCC.</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>Проверка петли</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>Пропустить эхоконтроль</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>Нажмите кнопку «Пуск», чтобы начать тест замыкания на себя.</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>Пропустить выполнение тестов сети OTN</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>Эхо-контроль</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>Обнаружено замыкание на себя</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>Пропустить эхоконтроль</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>Обнаружение замыкания на себя</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>Обнаружение замыкания на себя</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>Результаты  нагрузки BERT</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>Измеренный BER</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>Счетчик битовых ошибок</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>Решение</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>Конфигурация  нагрузки BERT </translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>Результаты Round Trip Delay</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>Мин ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>Макс ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>Среднее значение (мс)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>Примечание: Состояние сбоя возникает, когда среднее значение RTD превышает пороговое значение.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>Настройка Round Trip Delay</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>Результаты транспарентности GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>GCC BERT частота повторения цифровых ошибок</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>GCC BERT цифровые ошибки</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>Конфигурация транспарентности GCC</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>Анализ протокола</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Захватить</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>Захват и анализ CDP</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>Захват и анализ</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>Выбрать протокол для анализа</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>Начать &#xA; анализ</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>Прекращение &#xA; Анализ</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Анализ</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>Эксперт PTP</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>Настроить параметры испытаний вручную </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>Предельные значения</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>Запуск быстрой проверки</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>Завершение экспертного теста PTP</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>Ссылка не больше не является активной.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>Одностороннее время задержки источника синхронизации утеряно.</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>Сеанс подчиненного устройства PTP неожиданно остановлен.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>Синхронизация источника времени отсутствует. Пожалуйста, убедитесь, что ваш источник времени правильно настроен и подключен.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>Не удается установить сеанс подчиненного устройства PTP.</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>GPS приемник не обнаружен .</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>Тип TOS</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Объявить превышение лимита времени приема</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>Объявить</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 в секунду</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 в секунду</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 в секунду</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 в секунду</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 в секунду</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 в секунду</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 в секунду</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 в секунду</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Синхр.</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>Запрос задержки</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>Длительность владения ( секунды )</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Включить</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Активировать макс. порог длительности ошибки</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Макс. искажение времени (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Макс. порог искажения времени (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>Длительность испытания ( минуты )</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>Быстрая проверка</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>Master IP</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>Домен PTP</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>Начало&#xA;Проверка</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>Запуск&#xA;сессии</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>Сессия&#xA;установлена</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Макс. порог искажения времени (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Макс. искажение времени (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>Тек. искажение времени (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>Тек. искажение времени (us) к врем.</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Результаты испытаний</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>Продолжительность (минуты)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>Проверка PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>Тест сети PTP</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>Конец: Проверка PTP</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>Начать еще один тест</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>Проверить PTP выход</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>Расширение RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>Неверные настройки  </translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>Расширенные настройки IP - местные</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>Расширенные настройки времени задержки выполнения</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>Расширенные пакетные (CBS) настройки теста</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Выполнить операцию J-QuickCheck проверка</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>Настройки J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>Джиттер</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Длительная нагрузка</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>Выход из теста RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>Местная конфигурация сети</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>Удаленная конфигурация сети</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>Статус местного автоматического сообщения</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Скорость ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Дуплексн .</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Управление потоком</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Возможность FDX</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Возможность HDX</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Возможн . паузы</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>Статус удаленного автоматического сообщения</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Половин .</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Полн .</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Ни одно</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx и Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Только Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Только Rx</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>Частота кадров RTD</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 кадр / с</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 кадров / с</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>Конфигурация пакета</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>Выделенный размер пакета</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>Политика CBS (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>Поиск пакета</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>Нисходящий CBS  ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>Нисходящие размеры пакета ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Миним .</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>Максим.</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>Верхний CBS  ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>Верхние размеры пакета ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (кб)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>Размеры пакета (кБ) </translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>Установить расширенные настройки CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>Установить расширенные настройки политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>Установить расширенные настройки пакета поиска</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>Отклонение</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>Конфигурация длительной нагрузки</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>Масштабирование пропускной способности (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 имеет следующие недопустимые параметры конфигурации :</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>Подключение трафика успешно проверно . Выполнение нагрузочного теста .</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>Нажмите "Начать" для проведения теста с линейной скоростью.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>Нажмите "Начать" для проведения теста с базовыми настройками согласно RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>Измеренная пропускная способность НЕ будет использована для тестов RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>Измеренная пропускная способность БУДЕТ использована для тестов RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>Оазмер кадров при нагрузочном тестировании : %1 байт .</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>Размер пакета загрузки теста : %1 байт .</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>Верхний размер кадров при нагрузочном тестировании : %1 байт .</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>Нисходящий размер кадров при нагрузочном тестировании : %1 байт .</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>Измеренная пропускная способность :</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>Тест с использованием настроенной максимальной пропускной способности RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>Используйте измеряемой полосы пропускания как максимальную пропускную способность RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>Размер кадров при нагрузочном теста (байты)</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>Размер пакета при нагрузочном теста (байты)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>Верхний размер кадров при нагрузочном тестировании ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>Нисходящий размер кадров при нагрузочном тестировании ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>Запустить тесты RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>Пропустить RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>Проверка наличия аппаратной петли</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Испытание джиттера пакета</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>График теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Верхний график теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Результаты теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Верхние результаты теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Нисходящий график теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Нисходящие результаты теста Jitter</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>Макс . средн . джиттер (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>Макс . ср . Jitter</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>Макс . ср . &#xA;Jitter ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Результаты теста CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Верхние результаты теста CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Нисходящие результаты теста CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Результаты теста поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Верхние результаты теста поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Нисходящие результаты теста поиска пакета</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Результаты теста политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Верхние результаты теста политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Нисходящие результаты теста политики CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>Тестирование пакета (CBS ограничения )</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(L1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>CFG тест &#xA; размер (КБ)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Tx тест &#xA; Размер (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>Средний размер&#xA;Rx пакета (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>Фреймы &#xA; Отправлено</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>Кадров&#xA;получено</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>Кадров&#xA;потеряно</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>Скорость передачи&#xA;(кБ)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>Задержка &#xA;(мкс)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>Jitter&#xA;(мкс)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>Фактический &#xA; размер импульса (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Tx тест &#xA; Ограничения размера (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>Предполагаемое &#xA;CBS (кБ)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>График теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Верхний график теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Результаты испытания восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Верхние результаты теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Нисходящий график теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Нисходящие результаты теста восстановления системы</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>Время восстановления (us)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>Перезагрузка показателей &#xA;(L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>Перезагрузка показателей (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>Скорость перегрузки &#xA;(L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>Скорость перегрузки &#xA;(L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>Частота перегрузки &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>Показатель скорости возврата &#xA; (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>Показатель скорости возврата  (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>Скорость восстановления &#xA;(L1 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>Скорость восстановления &#xA;(L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>Частота восстановления &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>Среднее восстановление &#xA; время ( мкс )</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Испытание истинной скорости</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed </translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Режим</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>Средства управления</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>Средства управления TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>Обработка</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>Конфигурация шага</translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>Выбрать шаги</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Путь MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Оконный режим</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Производительность TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>Дополнительные параметры TCP</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Сохранить</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>Настройки соединения</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>TrueSpeed Ctl ( Расширено )</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Выбрать модули Вс</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>окно Walk</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>Завершить тест TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>Хост - устройство TCP не удалось установить соединение . Текущей тест был отменен .</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>При выборе режима «Диагностика» удаленный блок будет отключен .</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>Неверный выбор теста : Для выполнения TrueSpeed необходимо выбрать хотя бы один тест .</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>Измеренный MTU слишком мал для продолжения . Тест прерван .</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>Файл передан слишком быстро ! Выбрать размер файла для пропускной способности TCP, чтобы тест выполнялся не менее 5 секунд . Рекомендуемое время выполнения теста составляет , по крайней мере , 10 секунд .</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation> Достигнуто максимальное количество повторных попыток . Проверка прервана .</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>Обнаружена ошибка Хост - устройство TCP. Текущей тест был отменен .</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>Запуск испытания Истинная скорость.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>Обнуление размера MTU.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>Тестирование #1 байтов MTU с #2 байтов MSS.</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>Определенный Путь MTU равен #1 байтов . Он приравнен к MSS  #2 байтов .</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>Выполнить тест RTT. Это займет #1 секунд .</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>Определенное время двойного прохода (RTT) равняется #1 мсек .</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>Выполнение проверки окна обратного потока .</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>Выполнение проверки окна выходного ключевого потока .</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>Отправка #1 байт трафика TCP.</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>Локальный исходящий трафик: Несформированный</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>Удаленный исходящий трафик: Несформированный</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>Локальный исходящий трафик: Сформированный</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>Удаленный исходящий трафик: Сформированный</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>Длительность (сек): #1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>Подключения: #1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>Размер окна (Кб): #1 </translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>Выполнение проверки пропускной способности обратного потока TCP.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>Выполнение проверки пропускной способности выходного ключевого потока TCP.</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>Выполнение расширенного тестирования TCP займет #1 секунд.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>Тип локального IP- адреса</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>Локальный IP адрес</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>Локальный шлюз по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>Локальная маска подсети</translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>Локальная инкапсуляция</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Дистанционный IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>Выбрать режим</translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>Какой тип теста выполняется ?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>Я &lt;b> устанавливаю новую цепь &lt;/b> или &lt;b>turning-up&lt;/b>.*</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>Я &lt;b> устраняю неполадки &lt;/b> существующей цепи .</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>* Необходим удаленный тестовый инструмент MTS/T-BERD</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>Как настроить пропускная способность ?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>Пропускная способность выходного и обратного потока &lt;b>the одинакова &lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>Пропускная способность выходного и обратного потока &lt;b> различна &lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Задать узкое место пропускной способности для соответствия макс . пропускной способности SAMComplete CIR при загрузке Truespeed&#xA; конфигурации .</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Задать узкое место пропускной способности для соответствия макс . пропускной способности RFC 2544 при загрузке Truespeed&#xA; конфигурации .</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Сервер Iperf</translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>Испытательный прибор T-BERD/MTS</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>Тип IP</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP- адрес</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>Удаленные настройки</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>Настройки хост - сервера TCP</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>Этот шаг произведет настройку общих параметров для всех последующих ​​шагов TrueSpeed. Это включает CIR ( гарантированную скорость передачи данных ) и TCP Pass %, процент CIR необходимый для выполнения теста на пропускную способность .</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>Запуск теста окна</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>Общее время теста ( с )</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>Автоматически найти размер MTU</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>Размер MTU ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>Локальный идентификатор VLAN</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>Приоритет</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>Локальный приоритет</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>Локальн. TOS- адрес</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>Локальн. DSCP- адрес</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>Гарантированная скорость передачи выходного ключевого потока ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>Гарантированная скорость передачи данных обратного потока ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>Удаленный &#xA;TCP узел</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>Удаленный идентификатор VLAN</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>Удаленный приоритет</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>Удаленный TOS- адрес</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>Удаленный DSCP- адрес</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>Установить расширенные настройки TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Выбрать модуль Вс</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>Формирование трафика</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Модуль Вс</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>кбит</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Мбит</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>кб</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>Формировочный профиль</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>Локальный и удаленный исходящий формированный трафик</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>Только локальный исходящий трафик сформирован</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>Только удаленный исходящий трафик сформирован</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>Локальный и удаленный исходящий трафик не сформирован</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>Формирование трафика (выполнить тестирование окон и пропускной способности)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>Тестирование выполняется с формированием и без него</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>TC (мс) локальный</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Вс (кбит) локальный</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Вс (кбит) локальный</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Вс (Мбит) локальный</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Вс (Мбит) локальный</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>TC (мс) удаленный</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>TC (удаленный)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Вс (кбит) удаленный</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Вс (кбит) удаленный</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Вс (Мбит) удаленный</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Вс (Мбит) удаленный</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>Вы хотите сформировать трафик TCP?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>Несформированном трафике</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>Сформированный трафик</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>Выполнить тест без формирования</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>ЗАТЕМ</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>Выполнить тест с формированием</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>Выполнить тест с формированием на локальном</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>Выполнить с формированием на локальном и удаленном</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>Выполнить без какого-либо формирования</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>Выполнить с формированием на локальном и без формирования на удаленном</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>Выполнить с формированием на локальном</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>Выполнить без формирования на локальном и с формированием на удаленном</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>Сформировать локальный трафик</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (мс)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Вс (кбит)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Вс (Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>Удаленный модуль не подключен</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>Сформировать удаленный трафик</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>Показать дополнительные параметры формирования</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>Элементы управления TrueSpeed ( расшыренные )</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>Подключиться к порту</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>Передача TCP %</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>Максимальное значение MTU (в байтах)</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>Использовать несколько соединений</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>Активация окна насыщения</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>Окно повышения скорости (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>Соединения с повышенной скоростью (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>Конфигурация шага</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>TrueSpeed шаги</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>Для выполнения теста необходимо выбрать хотя бы один шаг .</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>Этот шаг использует процедуру , определенную в RFC4821 для автоматического определения максимального блока передачи пути сети конец - конец . Клиент испытательной установки TCP будет отправлять сегменты TCP с разным размером пакетов . Он определяет MTU без ICMP ( необходимо при обычном нахождении Пути MTU).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>Во время данной операции проводится передача TCP с низкой интенсивностью и выдается отчет о  базовом значении времени подтверждения приема (RTT), на основе которого будут получены последующие результатов испытаний .  Базовое значение RTT представляет собой собственную задержку сети , не включающую в себя дополнительные задержки , вызванные  перегруженностью сети .</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>Длительность ( секунды )</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>Данный тест проводит сканирование окна TCP и отсылает результаты пропускной способности TCP, имеющие до 4 размеров окна TCP и сочетаний соединения .   Тест также сообщает фактическую пропускную способность TCP для каждого размера окна .</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>Размер окна</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>Размер окна 1 ( байты )</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># Conn.</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>Соединения окна 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>Размер окна 2 ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>Соединения окна 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>Размер окна 3 ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>Соединения окна 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>Размер окна 4 ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>Соединения окна 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>Максим . размер сегмента ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>Обнаружены использования MSS в &#xA; маршруте MTU step</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>Макс . размер сегмента ( байты )</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>Основана на пропускной способности ссылок &lt;b>%1&lt;/b> Мбит / с и RTT &lt;b>%2&lt;/b> мс , в идеале TCP окно будет &lt;b>%3&lt;/b> байт . С &lt;b>%4&lt;/b> соединения ( ми ) &lt;b>%5&lt;/b> байт размер окна TCP для каждого соединения , примерная идеальная скорость будет &lt;b>%6&lt;/b> кбит / с и &lt;b>%7&lt;/b> MB файл , передаваемый на каждое соединение должен занимать &lt;b>%8&lt;/b> секунд .</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>В зависимости от скорости канала &lt;b>%1&lt;/b> Мб/с и результатов RTT &lt;b>%2&lt;/b> мс оптимальное TCP-окно составляет  &lt;b>%3&lt;/b> байт. &lt;font color="red"> При &lt;b>%4&lt;/b> подключении(ях) и TCP-окне размером  &lt;b>%5&lt;/b> байт для каждого подключения пропускная способность сети может быть превышена. Полученные результаты могут быть ниже ожидаемых вследствие потери пакетов и дополнительной задержки. Необходимо уменьшить количество подключений и/или размер TCP-окна для проведения теста в соответствии с пропускной способностью сети. &lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>Размер окна ( байты )</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>Размер файла на одно соединение ( МБ )</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>Автоматически найти размер файла для 30 секундной передачи</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 МБ )</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>Количество соединений</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>Использовать RTT, который найден &#xA; на шаге RTT&#xA;(%1 мс )</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>Использовать MSS, который найден &#xA; на шаге Путь MTU&#xA;(%1 байтов )</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>Данная операция выполняет множество передач в рамках соединения TCP, чтобы узнать  надлежащим образом канал связи разделяет полосу пропускания с другими объектами ( формирование трафика ) или ненадлежащим образом ( контроль трафика ).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>Данная операция выполняет множество передач в рамках соединения TCP, чтобы узнать  надлежащим образом канал связи разделяет полосу пропускания с другими объектами ( формирование трафика ) или ненадлежащим образом ( контроль трафика ).    Чтобы размер окна и количество соединений вычислялись  автоматически , выполните операцию по вычислению RTT.</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Размер окна ( КБ )</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>Автоматически вычисляется при проведении &#xA;RTT</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 байты</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>Запустить TrueSpeed тесты</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>Пропустить TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>Максимальная единица передачи (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>Максимальный размер сегмента (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>Данный шаг определил , что максимальный блок передачи (MTU) равен &lt;b>%1&lt;/b> байтов для этой линии связи ( конец - конец ). Данное значение , минус 3/4 издержки уровня , будет использоваться в качестве максимального размера сегмента (MSS) TCP для последующих шагов . В данном случае , MSS равен &lt;b>%2&lt;/b> байта .</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Время ( с ) </translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>Сводка результатов RTT</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>Ср . RTT ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>Мин . RTT ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>Max. RTT ( мс )</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Измеренное время двойного прохода (RTT) равняется &lt;b>%1&lt;/b> мсек . Используется минимальный RTT для представления наследуемой задержки сети . Последующие шаги используются в качестве основы для ожидаемой производительности TCP.</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Время прохождения сигнала в обоих направлениях измерено с точностью %1 мс. Используется среднее (базовое) время прохождения сигнала в обоих направлениях, так как этот параметр наиболее точно отражает внутреннюю задержку сети. На следующих этапах этот параметр используется для определения пропускной способности TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>Окно обратного потока</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>Окно выходного ключевого потока</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>Верхнее окно 1 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>Нисходящее окно 1 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>Верхнее окно 2 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>Нисходящее окно 2 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>Верхнее окно 3 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>Нисходящее окно 3 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>Верхнее окно 4 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>Нисходящее окно 4 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>Верхнее окно 5 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>Нисходящее окно 5 размер ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>Окно обратного потока 1 соединения</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>Окно выходного ключевого потока 1 соединения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>Окно обратного потока 2 соединения</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>Окно выходного ключевого потока 2 соединения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>Окно обратного потока 3 соединения</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>Окно выходного ключевого потока 3 соединения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>Окно обратного потока 4 соединения</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>Окно выходного ключевого потока 4 соединения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>Окно обратного потока 5 соединения</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>Окно выходного ключевого потока 5 соединения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>Окно обратного потока 1 Фактическое ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>Окно 1 фактическое, несформированное (Мбит) в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Окно 1 фактическое, сформированное (Мбит) в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>Окно обратного потока 1 расчетный ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>Окно обратного потока 2 Фактическое ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>Окно 2 фактическое, несформированное (Мбит) в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Окно 2 фактическое, сформированное (Мбит) в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>Окно обратного потока 2 расчетный ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>Окно обратного потока 3 Фактическое Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>Окно 3 фактической необработанной восходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Окно 3 фактической модели восходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>Окно обратного потока 3 расчетный Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>Окно обратного потока 4 Фактическое Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Окно 4 фактическое, несформированное (Мбит) в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Окно 4 фактическое, сформированное (Мбит) в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>Окно обратного потока 4 расчетный Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>Окно 5 фактической восходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>Окно 5 фактическое, несформированное в восходящем потоке (Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Окно 5 фактическое, сформированное в восходящем потоке (Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>Окно 5 расчетной восходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>Окно выходного ключевого потока 1 фактическое ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Окно 1 фактическое, сформированное (Мбит) в направлении основного трафика </translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>Окно выходного ключевого потока 1 расчетный ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>Окно выходного ключевого потока 2 фактическое ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Окно 2 фактическое, сформированное (Мбит) в направлении основного трафика</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>Окно выходного ключевого потока 2 расчетный ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>Окно выходного ключевого потока 3 фактическое Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Окно 3 фактической обработанной нисходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>Окно выходного ключевого потока 3 расчетный Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>Окно выходного ключевого потока 4 фактическое Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Окно 4 фактическое, сформированное (Мбит). нисходящий трафик</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>Окно выходного ключевого потока 4 расчетный Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>Окно 5 фактической нисходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Окно 5 фактическое, сформированное в нисходящем потоке (Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>Окно 5 расчетной нисходящей передачи (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Передача Мбит / с , средн ..</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>Окно 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>Фактич .</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>Несформированный фактический</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>Сформированный фактический</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>Идеальн .</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>Окно 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>Окно 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>Окно 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>Окно 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>Результаты шага TCP Walk the Window показывают фактическую пр . идеальной пропускную способность для каждого проверенного размера окна / соединения . Фактическая меньше идеальной может быть из - за потерь или перегруженности . Если фактическая больше идеальной , то базовая линия RTT слишком высока . Шаг пропускной способности TCP предоставляет полный анализ передач TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>Пропускная способность обратого потока TCP – фактическая против идеальной</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>Графики пропускной способности обратного потока TCP</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>Пропускная способность TCP в восходящем потоке (повторная передача диаграмм)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>Пропускная способность TCP в восходящем потоке </translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>Фактическая пропускная способность выходного ключевого потока TCP против идеальной</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>Графики фактической пропускной спрособности выходного ключевого потока TCP</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>Пропускная способность TCP в направлении основного трафика (повторная передача диаграмм)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>Пропускная способность TCP диаграмм RTT в направлении основного трафика</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>Идеальное время передачи обратного потока ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>Фактическое время передачи обратного потока ( сек )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>Upstream настоящая пропускная способность L4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Восходящая фактическая несформированная пропускная способность L4 (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>Восходящая фактическая сформированная пропускная способность L4 (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>Восходящая фактическая идеальная пропускная способность L4 (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>Upstream производительность TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>Несформированная эффективность TCP в восходящем потоке (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>Сформированная эффективность TCP в восходящем потоке (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>Upstream задержка в буфере (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>Несформированная задержка в буфере в восходящем потоке (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>Сформированная задержка в буфере в восходящем потоке (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>Действительная пропускная способность L4 (Mbps) в направлении Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Фактическая неформированная пропускная способность L4 в нисходящем направлении (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>Фактическая сформированная пропускная способность в направлении основного трафика (Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>Фактическая идеальная пропускная способность L4 (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>Downstream производительность TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>Несформированная эффективность TCP в направлении основного трафика (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>Сформированная эффективность TCP в направлении основного трафика (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>Downstream задержка в буфере (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>Несформированная задержка в буфере в направлении основного трафика (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>Сформированная задержка в буфере в направлении основного трафика (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>Результаты пропускной способности TCP</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>Фактический против Идеал</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>Восходящие результаты теста могут быть указаны :</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>Рекомендаций нет .</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>Тест не производился долгое время</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>Необходима настройка сетевого буфера / формирователя</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>Потерянны пакеты Policer из - за пакетов TCP.</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>Пропускная способность была хорошей , но обнаружены повторные передачи .</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>Сеть перегружена или формируется трафик </translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>Возможно , ваш CIR может настроен неверно </translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>Файл передан слишком быстро !&#xA; Проверьте ожидаемое время передачи файла .</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>&lt;b>%1&lt;/b> соединение ( я ), каждое с размером окна &lt;b>%2&lt;/b> байт использовалось для передачи файла &lt;b>%3&lt;/b> МБ через соединение ( всего &lt;b>%4&lt;/b> МБ ).</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b> байт TCP- окна передано посредством &lt;b>%2&lt;/b> соединения (- й ).</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>Фактическ. L4 ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>Идеальн . L4 ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>Показатели передачи</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>Эффективность TCP&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>Задержка буфера &#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>Низходящие результаты теста могут быть указаны :</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>Фактический и идеальный поток к коммутирующему узлу</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>Производительность TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>Задержка буфера (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>Несформированные результаты теста могут свидетельствовать о:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>Фактическая и идеальный поток в направлении основного трафика</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>Графики производительности</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Ретрансляция Frm</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>Базовый показатель RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>Данные графики используются для корреляции возможных проблем производительности TCP из - за ретрансляций и / или эффектов перегруженности сети (RTT превышение базовой линии ).</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>Диаграммы повторной передачи</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>Диаграммы RTT</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>Идеальная производительность на одно соединение</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>Для линии сформированного трафика , каждое подключение должно получить относительно одинаковую часть пропускной способности . Для линии ограниченного трафика , каждое подключение будет колебаться при появлении ретрансляций из - за ограничений . Для подключений &lt;b>%1&lt;/b>, каждое подключение должно составлять &lt;b>%2&lt;/b> Мбит / с пропускной способности .</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>L1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>L2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Произвольн.</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Тест TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>Параметры тестирования</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>Расширенные настройки подключения к серверу</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>Расширенные настройки тестирования</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>Создание локального отчета</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Идентификация теста</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>Завершение: Просмотр подробных результатов</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>Завершение: Создание локального отчета</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>Создание следующего локального отчета</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>Испытание MSS</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>Испытание RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>Тестирование восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>Тестирование нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>Требуется следующий параметр конфигурации.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>Следующий параметр находится вне пределов допустимого диапазона. Введите число от #1 до #2.</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>Следующий параметр имеет недопустимое значение. Выберите новое значение.</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>Отсутствие лицензии на тестирование канала клиент-сервер.</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>Отсутствие лицензии на тестирование канала сервер-сервер.</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>Слишком большое число активных тестов (максимум один #1).</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>Локальный сервер не резервирован.</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>Удаленный сервер не резервирован.</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>Копия теста уже существует.</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>Ошибка считывания из базы данных тестирования.</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>Тест не был обнаружен в базе данных тестов.</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>Тест прекращен с истечением срока.</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>Тип теста не поддерживается.</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>Сервер тестирования не выбран.</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>Сервер тестирования резервирован.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>Режим Bad request при тестировании сервера.</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>Тестирование на удаленном сервере не разрешается.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>Отказ запроса HTTP на удаленном сервере</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>Удаленный сервер не имеет достаточно ресурсов.</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>Клиент тестирования не выбран.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>Порт тестирования не поддерживается.</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>Чрезмерно большое число попыток тестирования за истекший час.</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>Ошибка создания копии теста.</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>Ошибка создания последовательности теста.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>Неуспешный запрос HTTP последовательности.</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>Отказ запроса HTTP последовательности.</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>Дистанционное тестирование не разрешено.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>В менеджере ресурсов возникла ошибка.</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>Копия теста не обнаружена.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>Статус теста имеет конфликт.</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>Статус теста недействителен.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>Ошибка создания теста.</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>Ошибка обновления теста.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>Тест был успешно создан.</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>Тест был успешно обновлен.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>Отказ запроса HTTP: #1 / #2 / #3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>Версия сервера VNF (#2) может быть не совместима с версией инструмента (#1).</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>Введите имя пользователя и ключ авторизации для сервера на устройстве #1.</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>Ошибка инициализации тестирования: #1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>Тестирование прекращено: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>Подключение сервера</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>Отсутствие данных для подключения к серверу.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>Канал для осуществления передачи данных в сети не установлен.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>Отсутствие действительного адреса IP источника сигнала для осуществления сетевого подключения.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>Команда ping не выполнена для заданного адреса IP.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>Невозможно определить устройство по заданному адресу IP.</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>Не выполнена идентификация сервера с указанным адресом IP.</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>Ошибка идентификация сервера с указанным адресом IP.</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>Выполнена идентификация сервера, но ошибка авторизации сервера.</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>Ошибка авторизации сервера.</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>Авторизация неуспешна, попытка идентификации.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>Отсутствие авторизации или идентификации, попробуйте выполнить команду ping.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>Выполнена авторизация сервера, сервер доступен для тестирования.</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>Сервер подключен, выполняется тестирование.</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>Идентификация</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>Идентифицировать</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>Версия прокси-сервера TCP</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>Версия контроллера тестирования</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>Канал активен:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID- адрес сервера:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>Статус сервера:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>Расширенные настройки</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>Расширенные настройки соединения</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>Ключ авторизации</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>Запомнить имена пользователей и ключи</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>Локальное устройство</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>Сервер</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Длительность передачи окна (с) </translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Авто</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>Автоматическая длительность</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>Параметры тестирования (Расширенные)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>Дополнительные параметры тестирования</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>Порт TCP</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>Автоматический выбор порта TCP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Число передач окон</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>Счетчик попыток подключения</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>Подсчет числа попыток автоматического соединения</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Окно насыщения</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>Запуск окна насыщения</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>Соединение с повышенной скоростью (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>Данные отчета сервера.</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Наименование теста</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Имя специалиста</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>Имя абонента*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Компания</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>Эл. почта*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Телефон</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Комментарии</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>Показать идентификационный номер теста</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>Пропустить тест TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>Сервер не подключен.</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>Средняя скорость передачи данных (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>Ожидание готовности тестируемого устройства. Вы являетесь #%1 в списке ожидания.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Окно</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>Окно&#xA;насыщения</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>Размер окна (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>Подключения</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>Диагностика восходящей передачи:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>Отсутствие данных для отчета</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>Слишком низкая пропускная способность</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>Несогласующееся время RTT</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>Низкая пропускная способность TCP</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>Высокая задержка буфера</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>Пропускная способность менее 85% CIR</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>Максимальный размер пакета (MTU) менее 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>Диагностика нисходящей передачи:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Код авторизации</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Дата создания учетной записи</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>Дата завершения</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>Время изменения</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Время прекращения тестирования</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>Последнее изменение</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Эл. почта</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>Суммарные данные пропускной способности восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>Фактический уровень по сравнению с заданным</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>Фактический уровень восходящей передачи по сравнению с заданным</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>Суммарные данные для восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>Пиковая скорость передачи данных TCP (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>Максимальный размер пакета (MSS) TCP (байт)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>Время прохождения сигнала в обоих направлениях (мс)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>Суммарные данные для восходящей передачи (Окно максимальной пропускной способности канала)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>Размер окна для соединения (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>Итоговое окно (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>Заданная пропускная способность TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>Средняя скорость передачи данных TCP (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>Производительность TCP ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>Целевой параметр</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>Окно 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>Окно 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>Окно 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>Окно 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>Окно 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>Окно 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>Окно максимальной пропускной способности:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>%1 Кбайт Окно: %2 подключ. x %3 Кбайт</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Среднее</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>Графическое представление пропускной способности восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>Подробные данные восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>Окно 1 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>Размер окна для соединения (кБ)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>Фактическая производительность ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>Целевая скорость передачи данных (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>Полная ретрансляция</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>Окно 2 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>Окно 3 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>Окно 4 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>Окно 5 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>Окно 6 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>Окно 7 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>Окно 8 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>Окно 9 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>Окно 10 пропускной способности канала восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>Окно пропускной способности канала при насыщении восходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>Средняя скорость передачи данных (Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>Суммарные данные пропускной способности нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>Фактический уровень нисходящей передачи по сравнению с заданным</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>Суммарные данные нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>Суммарные данные для нисходящей передачи (Окно максимальной пропускной способности канала)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>Графическое представление пропускной способности нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>Подробные данные нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>Окно 1 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>Окно 2 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>Окно 3 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>Окно 4 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>Окно 5 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>Окно 6 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>Окно 7 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>Окно 8 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>Окно 9 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>Окно 10 данных пропускной способности канала нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>Окно данных пропускной способности канала при насыщении нисходящей передачи</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation># Количество передач окон</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>Перенасыщение окон (%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>Перенасыщение подключений (%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>Сканиров. VLAN</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 Начало проверки # 2 сканирования виртуальной локальной сети VLAN</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>Тестирование VLAN ID #1 в течение #2 секунд</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1: ПОДТВЕРЖДЕН</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1: НЕ ПОДТВЕРЖДЕН</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>Активный шлейф не удался. Ошибка тестирования VLAN ID #1</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>Общее количество идентификаторов VLAN, которые подлежат тестированию</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>Количество верных идентификаторов</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>Количество неверных идентификаторов</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>Продолжительность для идентификатора (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>Количество диапазонов</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>Выбранные диапазоны</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>Мин. ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID, макс.</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Старт Теста</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>Всего идентификаторов</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>Правильность идентификаторов</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>Ошибка при вводе идентификаторов</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>Дополнительные параметры VLAN сканирования</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>Пропускная способность (L1 Мбит)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>Критерии соответствия</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>Ни один кадр не утерян</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>Некоторые из полученных кадров</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>Конфигурация TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>Запустить TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>Оценка времени выполнения</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Остановка при сбое</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Ознакомиться с отчетом</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>Просмотр отчета TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Операция прервана</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Завершено</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Пройден</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Остановлен пользователем</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>Потеря сигнала</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>Потеря связи.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>Потеря связи с удаленной испытательной установкой . Восстановите канал связи и попробуйте еще раз .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Возникла проблема при настройке локальной испытательной установки . Потеря связи с удаленной испытательной установки . Восстановите канал связи и попробуйте еще раз .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Возникла проблема при настройке удаленной контрольно-измерительной аппаратуры; соединение с испытательным пультом было прервано. Установить соединение и повторить попытку.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>Возникла проблема при настройке локальной испытательной установки . Завершение работы TrueSAM.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Возникла проблема при настройке локальной испытательной установки . Проверка была остановлена , потерян канал связи с удаленной испытательной установкой .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Возникла проблема при настройке удаленной испытательной установки . Проверка была остановлена , потерян канал связи с удаленной испытательной установкой .</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>Хранитель экрана был отключен для предотвращения помех во время теста .</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>Возникла проблема локальной испытательной установки . Завершение работы TrueSAM.</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>Запрос параметров DHCP.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>Параметры DHCP получены.</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>Идет инициализация интерфейса Ethernet. Подождите.</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>Северная Америка</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>Северная Америка и Корея</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>Северная Америка PCS</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Индия</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>Потеря сигнала Time of Day с внешнего источника приведет к исчезновению 1PPS синх .</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>Начать синхронизацию с сигналом Time of Day из внешнего источника времени ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>Синхронизация с сигналом Time of Day из внешнего источника времени успешно завершена .</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>Петля &#xA; неактивна</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>Титульные страницы</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>Выберите тест для загрузки</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>Сквозная проверка прохождения трафика</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>Расширение RFC 2544 / SAM Завершить</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>Набор эталонных тестов Ethernet</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Настройка теста проверки производительности и конфигурации служб Ethernet (стандарт Y.1564)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>Уровень 2. Проверка прохождения для протоколов управления (CDP, STP и т.д.).</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>RFC 6349. Проверка производительности и пропускной способности TCP</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>L3- Тип источника</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>Установки для канала связи ( используя Услугу 1)</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>Настройки канала связи</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>Сервис 1 должен быть сконфигурирован таким образом , чтобы он согласовывался с потоком 1 в средстве для проведения испытаний на удаленной стороне Viavi. </translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>Локальный статус</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>Синхр. ToD</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>Синхр. 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>Подключиться&#xA;к каналу</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>Физический уровень</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>Длительность паузы ( кванты )</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>Длительность паузы ( время - мс )</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>Запустить &#xA; Тесты</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>Начать &#xA; Тесты</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>Остановка &#xA; тесто</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>Расчетное время выполнения тестов</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Всего</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>Завершение по отказу</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>Критерии прохождения / непрохождения для выбранных тестов</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>Вверх или вниз</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>Он не связан с принципом прохождения / непрохождения . Данные настройки не будут применяться .</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>Пройден , если соответствует следующему порогу :</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>Нет заданного порога - не будет отчета «Прохождение / непрохождение» .</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>Локальн.:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Дистанцион.:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>Местные и удаленные :</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>Пропускная способность (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>Пропускная способность (L1 кбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>Пропускная способность (L2 кбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Допустимая потеря кадров (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>Задержка (мкс)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>Пройден , если соответствует следующим параметрам SLA:</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>Восходящ.:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>Нисходящ.:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>Пропускная способность CIR ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>Пропускная способность CIR (L1 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>Пропускная способность CIR (L2 Мбит)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>Избыточная скорость передачи информации – пропускная мощность ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>Пропускная способность EIR (L1 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>Пропускная способность передачи избыточной информации (EIR) (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M - Отклонение ( мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - Допуск (L1 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>M - допуск (L2 Мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>Задержка кадра ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>Колебание задержки ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>Прохождение / непрохождение будет определено внутри .</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>Пройден , если измеренная пропускная способность TCP соответствует следующему порогу :</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>Процент ожидаемой пропускной способности</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>Тест пропускной способности TCP не включен - не будет отчета «Прохождение / непрохождение» .</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>Остановить тесты</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>Это остановит запущенный тест и дальнейшее выполнение теста . Хотите остановить ?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная общая нагрузка для сервисов #1  равна 0 или превышает линейную скорость #2 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная общая нагрузка для верхнего направления для сервисов #1  равна 0 или превышает линейную скорость #2 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная общая нагрузка для нисходящего направления для сервисов #1  равна 0 или превышает линейную скорость #2 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная общая нагрузка для сервисов #1  равна 0 или превышает соответствующую линейную скорость L2 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная общая нагрузка для верхнего направления для сервисов #1  равна 0 или превышает линейную скорость L2 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная общая нагрузка для нисходящего направления для сервисов #1  равна 0 или превышает линейную скорость L2 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная нагрузка превышает линейную скорость #1 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная нагрузка превышает линейную скорость #1 L1 мбит / с в верхнем направлении .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Максимальная нагрузка превышает линейную скорость #1 L1 мбит / с в нисходящем направлении .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>Недопустимая конфигурация :&#xA;&#xA; Максимальная загрузка превышает показатель линейной скорости #1 L2 M бит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>Недопустимая конфигурация :&#xA;&#xA; Максимальная загрузка превышает показатель линейной скорости #1 L2 M бит / с в высходящем направлении .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>Недопустимая конфигурация :&#xA;&#xA; Максимальная загрузка превышает показатель линейной скорости #1 L2 M бит / с в нисходящем направлении .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Значения CIR, EIR и Контроль трафика не могут все одновременно равняться 0. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Значения CIR, EIR и Контроль трафика не могут все одновременно равняться 0 в восходящем направлении. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Значения CIR, EIR и Контроль трафика не могут все одновременно равняться 0 в нисходящем направлении. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Самый малый шаг загрузки (#1% от величины CIR) не может быть достигнут при использовании установочного значения CIR  #2 Мбит / с. Самый малый шаг загрузки при использовании текущего значения CIR для сервиса #3 составляет #4%.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Общее CIR (L1 мбит / с ) равно 0 или превышает линейную скорость #1 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Общее CIR (L1 мбит / с ) для верхнего направления равно 0 или превышает линейную скорость #1 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Неверная конфигурация :&#xA;&#xA; Общее CIR (L1 мбит / с ) для нисходящего направления равно 0 или превышает линейную скорость #1 L1 мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Недопустимая конфигурация :&#xA;&#xA; Общий показатель CIR (L2 M бит / с ) равен 0 либо превышает показатель линейной скорости #1 L2 Мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Недопустимая конфигурация :&#xA;&#xA; Общий показатель CIR (L2 M бит / с ) в восходящем направлении равен 0 либо превышает показатель линейной скорости #1 L2 Мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Недопустимая конфигурация :&#xA;&#xA; Общий показатель CIR (L2 M бит / с ) в нисходящем направлении равен 0 либо превышает показатель линейной скорости #1 L2 Мбит / с .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>Недействительн. конфигурац.:&#xA;&#xA; Необходимо выбрать либо испытание конфигурации сервиса , либо испытание производительности сервиса.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Необходимо выбрать как минимум один сервис.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>Неверная конфигурация:&#xA;&#xA;Общее количество выбранных услуг CIR (или EIR для тех служб, которые имеют CIR 0) не может превышать скорость линии, если вы хотите выполнить тест производительности службы.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Максимальное указанное значение измерения производительности (RFC 2544) не может быть меньше , чем сумма значений CIR для выбранных сервисов при выполнении испытания производительности сервисов. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Максимальное указанное значение измерения производительности восходящего потока (RFC 2544) не может быть меньше , чем сумма значений CIR для выбранных сервисов при выполнении испытания производительности сервисов. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Максимальное указанное значение измерения производительности нисходящего потока (RFC 2544) не может быть меньше , чем сумма значений CIR для выбранных сервисов при выполнении испытания производительности сервисов. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Максимальное указанное значение измерения производительности (RFC 2544) не может быть меньше , чем значение CIR при выполнении испытания производительности сервиса. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Максимальное указанное значение измерения производительности в восходящем направлении (RFC 2544) не может быть меньше , чем значение CIR при выполнении испытания производительности сервиса. </translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Максимальное указанное значение измерения производительности нисходящего потока (RFC 2544) не может быть меньше , чем значение CIR при выполнении испытания производительности сервиса. </translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>Испытание SAMComplete прекращено в связи с тем , что были выбраны команды Стоп или Отказ и как минимум одно значение KPI не удовлетворяет параметрам Соглашения об уровне обслуживания для сервиса #1.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>Испытание SAMComplete прекращено в связи с тем , что данные сервиса #1 не были возвращены в течение 10 секунд после запуска трафика.  Дальний конец может больше не производить проверку по шлейфу.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>Испытание SAMComplete прекращено в связи с тем , что данные сервиса #1 не были возвращены в течение 10 секунд после запуска трафика. Дальний конец может больше не осуществлять передачу трафика.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>Испытание SAMComplete прекращено в связи с тем , что данные не были возвращены в течение 10 секунд после запуска трафика. Дальний конец может больше не производить проверку по шлейфу.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>Испытание SAMComplete прекращено в связи с тем , что данные не были возвращены в течение 10 секунд после запуска трафика. Дальний конец может больше не осуществлять передачу трафика.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>Локальное и удаленное приложения несовместимы. Локальное приложение является приложением , относящимся к Потокам , в то время как удаленное приложение по IP- адресу #1 является приложением , относящимся к Трафику.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>Локальное и удаленное приложения несовместимы. Локальное приложение является приложением , относящимся к Потокам , в то время как удаленное приложение по IP- адресу #1 является приложением , относящимся к TCP WireSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>Локальное и удаленное приложения несовместимы. Локальное приложение является приложением Уровня 3, в то время как удаленное приложение по IP- адресу #1 является приложением уровня 2.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>Локальное и удаленное приложения несовместимы. Локальное приложение является приложением Уровня 2, в то время как удаленное приложение по IP- адресу #1 является приложением уровня 3.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>Удаленное приложение по IP-адресу #1 установлено для WAN IP. Оно не совместимо с SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>Удаленное приложение по IP адресу #1 установлено в режим инкапсуляции пакетированного VLAN. Оно не совместимо с SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>Удаленное приложение по IP- адресу #1 установлено для инкапсуляции VPLS. Оно несовместимо с SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>Удаленное приложение по IP- адресу #1 установлено для инкапсуляции MPLS. Оно несовместимо с SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>Удаленное приложение поддерживает только услугу #1. </translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Для испытания односторонней задержки требуется , чтобы локальное и удаленное устройства для испытаний достигли синхронизации с источником времени OWD.  Не удалось достигнуть синхронизации односторонней задержки в локальном устройстве.  Проверьте все соединения с аппаратными средствами источника времени OWD.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Для испытания односторонней задержки требуется , чтобы локальное и удаленное устройства для испытаний достигли синхронизации с источником времени OWD.  Не удалось достигнуть синхронизации односторонней задержки в удаленном устройстве.  Проверьте все соединения с аппаратными средствами источника времени OWD.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>Недействительная конфигурация :&#xA;&#xA; Испытание времени подтверждения приема (RTT) должно быть выполнено до проведения испытания SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>�евозможно организовать сеанс TrueSpeed. Перейдите на страницу " Сеть</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Не удалось  установить сеанс  TrueSpeed для направления Upstream. Перейтиде к странице "Network", проверьте конфигурацию и попробуйте еще раз . </translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Не удалось  установить сеанс  TrueSpeed для направления Downstream. Перейтиде к странице "Network", проверьте конфигурацию и попробуйте еще раз . </translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>Испытание времени подтверждения приема (RTT) оказалось недействительным в результате изменения выбранных в текущий момент сервисов , подлежащих испытанию. Перейдите на страницу " Средства управления TrueSpeed" и повторно выполните испытание RTT.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Неправильная конфигурация :&#xA;&#xA; Число CIR (Mbps) для направления Downstream для Услуг ( и ) #1 не должно = 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Неправильная конфигурация :&#xA;&#xA; Число CIR (Mbps) для направления Upstream для Услуг ( и ) № #1 не должно = 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>Неверная конфигурация:&#xA;&#xA;CIR (Мбит/с) для услуг #1 не может быть равно 0.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Трафик не получен . Перейдите на страницу «Сеть» , проверьте конфигурацию и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>Просмотр главных результатов</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>Остановить &#xA; тесты</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>   Отчет создан , для просмотра нажмите «Далее»</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>Пропустить просмотр отчетов</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAMComplete - испытание активации сервиса Ethernet</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>с TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>Параметры локальной сети</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>Локальному устройству не требуется настройка.</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>Настройки удаленной сети</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>Удаленному устройству не требуется настройка.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>Локальные параметры IP</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>Удаленные параметры IP</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Службы</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>Тегирование</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>Тегирование не используется .</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>Производительность SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>SLA поддержание обеспечения</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>Тесты ограничения не выбраны .</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>Пакетный сигнал SLA</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>Тестирование пакета невозможно из - за отсутствия требуемой функциональности .</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>Производительность SLA</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>�ервис TrueSpeed в настоящий момент отключен.&#xA;&#xA; Сервис TrueSpeed может быть включен на странице настройки " Сеть</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>Местные расширенные настройки</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>Расширенные настройки трафика</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>Расширенные настройки LBM</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>Расширенные настройки SLA</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>Размер произвольный / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>Дополнительные параметры IP</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>L4 расширенный</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>Расширенная маркировка</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>Испытание конфигурации сервиса</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>Испытание производительн. сервиса</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Выйти из испытания Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>Статус сервера обнаружения</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>Сообщение сервера обнаружения</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>Адрес MAC и режим ARP</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>Выбор SFP</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>IP- адрес источника</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>Стат. адрес IP WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>Шлюз WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>Маска подсети WAN</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>Адрес IP устройства передачи данных</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>Маска подсети данных</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>Порт локальной сети использует VLAN- тегирование ?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-in-Q ( многоуровневая VLAN)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>Идет ожидание</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>Доступ к серверу ...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>Невозможно зайти на сервер</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP получен</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>Предоставляемая аренда : #1 мин .</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>Сниженная аренда : #1 мин .</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>Разрешенная аренда</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>Сервер обнаружения :</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 L2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 кБ CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 мс FTD, #3 мс FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>Служба 1 VoIP - 25% трафика - кадры по #1 и #2 байт</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>Служба 2 Видеотелефония - 10% трафика - кадры по #1 и #2 байт</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>Служба 3 Приоритетные данные - 15% трафика - кадры по #1 и #2 байт</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>Служба 4 BE данные - 50% трафика - кадры по #1 и #2 байт</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>Все службы</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации , данных и речи сервиса 1</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Голос.</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>Функция вида U 56K G.711</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>Функция вида A 56K G.711</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>Функция вида U 64K G.711</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>Функция вида A 64K G.711</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>При 10GE уровень детализации пропускной способности равен 0,1 мбит / с . В результате , для данного уровня детализации есть несколько значений CIR</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>На 40GE уровень гранулярности полосы пропускания составляет 0,4 Мбит / с , в результате , значение гарантированной скорости передачи данных кратно этому уровню гранулярности</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>При 100GE уровень детализации пропускной способности равен 1 мбит / с . В результате , для данного уровня детализации есть несколько значений CIR</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>Настроить размеры</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>Размер кадра ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>Определен. длина</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>Длина цикла EMIX</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Длина пакета ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Расчет. длин. кадра</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>Расчетный размер кадра определяется путем использования длины пакета и инкапсуляции.</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 2</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 3</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 4</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 5</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 6</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 7</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 8</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 9</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>Свойства функции передачи видеоинформации данных и речи сервиса 10</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>Неполномерн.</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>Количество сервисов</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>Уровень</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>Настройки LBM</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>Настроить передачу видеоинформации , данных и речи…</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>Свойства сервиса</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>Настройки DA MAC и размера кадра</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC, Настройка размера пакета и типа сети Ethernet</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>Настройки DA MAC, длины пакета и настройки TTL</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>DA MAC и настройки длины пакета</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>Настройки сети ( расширенные )</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>Несовместимость локального / удаленного модуля требует длину пакета &lt;b>%1&lt;/b> или более.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>Служба</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Настроить...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation>  Размер , определен. пользователем</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL ( скачки )</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>MAC- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>Показать оба.</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>Только удаленная</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>Только локальная</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>Настройки LBM ( расширенные )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>Параметры сети: размер произвольный / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 1 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>Размер пакета 1</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>Длина пакета 1</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>Размер пользователя 1</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>Размер пакета Jumbo 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>Размер пакета 2</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>Длина пакета 2</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>Размер пользователя 2</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>Размер пакета Jumbo 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>Размер пакета 3</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>Длина пакета 3</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>Размер пользователя 3</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>Размер пакета Jumbo 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>Размер пакета 4</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>Длина пакета 4</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>Размер пользователя 4</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>Размер пакета Jumbo 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>Размер пакета 5</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>Длина пакета 5</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>Размер пользователя 5</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>Размер пакета Jumbo 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>Размер пакета 6</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>Длина пакета 6</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>Размер пользователя 6</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>Размер пакета Jumbo 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>Размер пакета 7</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>Длина пакета 7</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>Размер пользователя 7</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>Размер пакета Jumbo 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>Размер пакета 8</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>Длина пакета 8</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>Размер пользователя 8</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>Размер пакета Jumbo 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 1 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>Сервер 1 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 2 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 2 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>Сервер 2 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 3 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 3 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>Сервер 3 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 4 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 4 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>Сервер 4 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 5 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 5 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>Сервер 5 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 6 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 6 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>Сервер 6 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 7 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 7 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>Сервер 7 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 8 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 8 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>Сервер 8 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 9 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 9 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>Сервер 9 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>Сервер 10 Длины произвольные / EMIX (дистанц.)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>Сервер 10 Длины произвольные / EMIX (локальн.)</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>Сервер 10 Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>У служб другие настройки идентификаторы VLAN или приоритеты пользователей ?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>Нет , все службы используют одинаковые настройки VLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Бит DEI</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>Расширенный…</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>Приоритет пользователя.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>SVLAN Pri.</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>Настройки VLAN- тегирования ( инкапсуляции )</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>Приоритет SVLAN</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>Все службы для передачи данных используют одинаковый адрес IP приемного устройства.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>Адрес IP устройства получения данных</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP- адрес назначения</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>Адрес IP устройства получения данных</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>Установить приращение IP ID</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>Параметры IP-адреса (только локальные)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>Настройки IP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>Параметры IP-адреса (только удаленные)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>Настройки IP ( расширенные )</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>Расширенные настройки IP</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>У служб другие настройки TOS или DSCP?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>Нет , все службы имеют одинаковые TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>Совокупные SLAs</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>Совокупный CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>Совокупные CIR в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>Совокупный CIR в направлении основного трафика</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>Совокупный EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>Совокупные EIR в восходящем потоке</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>Совокупный EIR в направлении основного трафика</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>Совокупный режим</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>Макс. испытание производительности локальн. устройства ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>Макс. испытание производительности удален. устройства ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>Активировать совокупный режим</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>ВНИМАНИЕ! Выбранные значения веса в сумме составляют &lt;b>%1&lt;/b>%, а не 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>Производительность SLA, Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>SLA пропускная способность , L2 Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>Полоса пропускания SLA, L1 мбит / с</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>Полоса пропускания SLA, L1 мбит / с ( односторонняя )</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>SLA пропускная способность , L2 Мбит / с ( в одном направлении )</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>Вес (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Контроль трафика</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>Максим. нагрузка</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>Только нисходящ.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>Только восходящ.</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>Установить расширенные настройки трафика</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>Контроль трафика SLA, Мбит / с</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>Макс. контроль трафика</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>Выполнить тестирование пакета</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>Отклонение (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>Отклонение (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>Вы желаете произвести проверку пакетного сигнала ?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>Тип теста пакета :</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>Установить расширенные настройки пакета</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>Точность задержки кадра</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>Включить задержку пакета, как требование SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>Включая изменение длительности задержки пакета, как требование SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>Производительность SLA ( односторон.)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Задержка фрейма (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Задержка кадра (OWD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>Установить расширенные настройки SLA</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>Настройка сервиса</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>Количество шагов ниже CIR</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>Длительность шага ( сек.)</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>шаг 1 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>шаг 2 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>шаг 3 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>шаг 4 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>шаг 5 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>шаг 6 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>шаг 7 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>шаг 8 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>шаг 9 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>шаг 10 % CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>Проценты шага</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>Производительность сервиса</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>Каждое направление тестируется отдельно , поэтому общая продолжительность теста будет вдвое больше введенного значения .</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Остановка теста при сбое</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>Расширенные пакетные настройки теста</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>Пропустить J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Выбрать испытания Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>Выбрать сервисы для испытания</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>Установить все</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  Всего :</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>Испытание настройки сервиса</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>Испытание производительности сервиса</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>Дополнительные измерения</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>Производительность (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>Макс. ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>Макс . (L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>Макс . (L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>Дополнительные измерения не могут выполняться при активном сервисе TrueSpeed. </translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Выполнить испытания Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Пропустить Y.1564</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>Задержка , колебан.</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>Итоговый обзор отказов при испытаниях :</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>Решения</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Решение Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>Конфиг. решения об испытаниях</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>Конфиг. решения 1 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>Конфиг. решения 2 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>Конфиг. решения 3 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>Конфиг. решения 4 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>Конфиг. решения 5 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>Конфиг. решения 6 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>Конфиг. решения 7 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>Конфиг. решения 8 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>Конфиг. решения 9 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>Конфиг. решения 10 сервиса испытаний</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Решение об испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Решение о сервисе 1 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Решение о сервисе 2 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Решение о сервисе 3 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Решение о сервисе 4 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Решение о сервисе 5 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Решение о сервисе 6 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Решение о сервисе 7 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Решение о сервисе 8 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Решение о сервисе 9 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Решение о сервисе 10 при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Решение о значении IR при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Решение о потере кадров при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Решение о задержке при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Решение о колебании задержки при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Решение о значении TrueSpeed при испытании производительн.</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>Результаты настройки сервиса</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>Результаты настройки сервиса 1</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>Максимальная пропускная способность (L1 мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>Нисходящее максимальная пропускная способность (L1 мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>Верхнее максимальная пропускная способность (L1 мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>Максимальная пропускная способность (L2 мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>Нисходящее максимальная пропускная способность (L2 мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>Верхнее максимальная пропускная способность (L2 мбит/с)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>Решение CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>Нисходящее решение CIR</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>Колебан. задержки кадра ( мс )</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>Количество&#xA;OoS</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>Обнаружение ошибки кадра</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>Обнаружение паузы</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>Верхнее решение CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>Решение CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>Нисходящее решение CBS</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>Фактический размер импульса ( кб )</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Tx размер пакета ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>Ср . размер импульса Rx ( кб )</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>Предполагаемое CBS ( кБ )</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>Верхнее решение CBS</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>Решение EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>Нисходящее решение EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>Верхнее решение EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>Решение о контроле трафика</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>Нисходящее решение о контроле трафика</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>Верхнее решение о контроле трафика</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>Решение о шаге 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>Нисходящее решение о шаге 1</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>Верхнее решение о шаге 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>Решение о шаге 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>Нисходящее решение о шаге 2</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>Верхнее решение о шаге 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>Решение о шаге 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>Нисходящее решение о шаге 3</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>Верхнее решение о шаге 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>Решение о шаге 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>Нисходящее решение о шаге 4</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>Верхнее решение о шаге 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>Решение о шаге 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>Нисходящее решение о шаге 5</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>Верхнее решение о шаге 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>Решение о шаге 6</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>Нисходящее решение о шаге 6</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>Верхнее решение о шаге 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>Решение о шаге 7</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>Нисходящее решение о шаге 7</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>Верхнее решение о шаге 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>Решение о шаге 8</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>Нисходящее решение о шаге 8</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>Верхнее решение о шаге 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>Решение о шаге 9</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>Нисходящее решение о шаге 9</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>Верхнее решение о шаге 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>Решение о шаге 10</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>Нисходящее решение о шаге 10</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>Верхнее решение о шаге 10</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>Макс . полоса пропускания (L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>Максимальная пропускная способность (L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>Макс. производительность ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>Операции</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Пакет</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>Ключ</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>Нажать на панелях для ознакомления с результатами по каждому этапу.</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (Производительность Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (Производительность L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (Производительность L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>Обнаружение ошибки</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>Шаг 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>Шаг 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>Шаг 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>Шаг 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>Шаг 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>Шаг 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>Шаг 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>Шаг 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>Шаг 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>Шаг 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>Пороги SLA</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>Результаты настройки сервиса 2</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>Результаты настройки сервиса 3</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>Результаты настройки сервиса 4</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>Результаты настройки сервиса 5</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>Результаты настройки сервиса 6</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>Результаты настройки сервиса 7</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>Результаты настройки сервиса 8</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>Результаты настройки сервиса 9</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>Результаты настройки сервиса 10</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>Результаты производительности сервиса</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Решение о сервисе</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR, текущ.</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR, максим.</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR, миним.</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR, средн.</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>Секунды с потерями кадров</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>Количество потерянных кадров</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>Задержка текущ.</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>Задержка макс.</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>Задержка мин.</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>Задержка средн.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>Задержка , колебан. текущ.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>Задержка , колебан. макс.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>Задержка , колебан. мин.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>Задержка , колебан. средн.</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>Наличие</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Доступн.</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>Доступн. секунд.</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>Секунды состояния недоступности</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>Секунды с серьезными ошибками</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>Производительность сервиса</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Обзор</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Колебание задержки</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>Сервис TrueSpeed. Ознакомьтесь с результатами на странице результатов сервиса TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR, средн .&#xA;(Производительность&#xA;L1 Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR, средн .&#xA;(Производительность&#xA;L2 Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>Потеря кадров &#xA; коэффициент</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>Односторонняя задержка &#xA; средн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>Задержка , колебан.&#xA; макс. &#xA;( мс )</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>Обнаруженные ошибки</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>Задержка &#xA; средн. &#xA;(RTD, мс )</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, текущ .&#xA;(L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, текущ .&#xA;(L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, макс .&#xA;(L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, макс .&#xA;(L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, мин .&#xA;(L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, мин .&#xA;(L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>Потеря кадров &#xA; количество</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>Порог &#xA; коэффициента &#xA; потери кадров</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>Задержка &#xA; текущ. &#xA;(OWD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>Задержка &#xA; макс. &#xA;(OWD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>Задержка &#xA; мин. &#xA;(OWD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>Задержка &#xA; средн. &#xA;(OWD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>Задержка &#xA; Порог &#xA;(OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>Задержка &#xA;Cur &#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>Задержка &#xA;Max. &#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>Задержка &#xA; мин. &#xA;(RTD, мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>Задержка &#xA; Порог &#xA;(RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>Задержка , колебан.&#xA; текущ. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>Задержка , колебан.&#xA; средн. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>Задержка , колебан.&#xA; порог. ( мс )</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>Коэффиц.&#xA; доступн.&#xA; секунд</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>Секунды состояния &#xA; недоступности</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>Секунды &#xA; с серьезными &#xA; ошибками</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Конфигурация сервиса &#xA; Пропускная способность &#xA;(L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Конфигурация услуги &#xA; Пропускная способность &#xA;(L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Работа сервиса &#xA; Пропускная способность &#xA;(L1 мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Эксплуатационная характеристика &#xA; Пропускная способность &#xA;(L2 Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>Настройки сети</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Размер пользователя</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>Кадр увелич. размера</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>Длина пакета пользователя</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>Длина пакета увелич. размера</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>Расчетн. размер кадра ( в байтах )</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>Параметры сети (Размер произвольный / EMIX)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>Длины произвольные / EMIX (удаленное устройство)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>Длины произвольные / EMIX (локальное устройство)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>Длины произвольные / EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>Расширенные настройки IP - удаленные</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>Downstream CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>Downstream EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>Downstream поддержка обеспечения </translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>Downstream M</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>Upstream CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>Upstream EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>Upstream поддержание обеспечения</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>Upstream M</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>Расширенный пакет SLA</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>Downstream коэффициент потери фреймов</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>Downstream задержка фрейма (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>Downstream задержка фрейма (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>Downstream отклонение задержки (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>Upstream коэффициент потерь фрейма</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>Upstream задержка фрейма (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>Upstream задержка фрейма (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>Upstream отклонение задержки (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>Включить как требование SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>Включая задержку пакета</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>Включая изменение длительности задержки пакета</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete имеет следующие недопустимые параметры конфигурации :</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>Результаты настройки сервиса</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>Выполнить испытание истин. скорости TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>Установить длину пакета TTL</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>Установить порты TCP/UDP</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>Тип источника </translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Порт источника</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>Тип назначен. </translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>Порт назначен.</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>Порог пропускной способности TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>Рекомендуемый общий размер окна ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>Общий размер окон с повышенной скоростью (байт)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>Рекомендуемые #  соединений</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>Число соединений с повышенной скоростью</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>Верхний рекомендуемый общий размер окна ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>Общий размер окон с повышенной скоростью восходящей передачи (байт)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>Верхние рекомендуемые #  соединений</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>Число восходящих соединений с повышенной скоростью</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>Нисходящий рекомендуемый общий размер окна ( байты )</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>Общий размер окон с повышенной скоростью нисходящей передачи (байт)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>Нисходящие рекомендуемые #  соединений</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>Число соединений передачи с повышенной нисходящей скоростью</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation>Рекомендуемое #  соединений</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>Производительность TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>нагрузка Acterna</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>Время задержки полного обхода ( мс )</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>Параметр RTT используется на следующих этапах для определения размера окна и число соединений.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>Результаты TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>Показатели передачи TCP</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>Целев. L4 ( Мбит / с )</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>Решение о сервисе TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>Upstream заключение об услуге TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>Действительная пропускная способность TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>Upstream настоящая пропускная способность TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>Upstream заданная пропускная способность TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>Заданная пропускная способность TCP (Mbps))</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>Downstream истинная скорость</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>Действительная пропускная способность TCP (Mbps) в направлении Downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>Downstream заданная пропускная способность TCP (Mbps)</translation>
        </message>
    </context>
</TS>
