<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>systrayd::CAudioSystemTrayDataItem</name>
        <message utf8="true">
            <source>Audio</source>
            <translation>Áudio</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBatterySystemTrayDataItem</name>
        <message utf8="true">
            <source>Battery/Charger</source>
            <translation>Bateria/Carregador</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please plug in AC power to continue using the test instrument.</source>
            <translation>A bateria está muito quente. Conecte a alimentação CA para continuar usando o instrumento de teste.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot. Please keep the AC power plugged in to continue using the test instrument.</source>
            <translation>A bateria está muito quente. Mantenha a alimentação CA conectada para continuar usando o instrumento de teste.</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and cannot be used until it cools down. The AC power must remain plugged in to continue using the test instrument.</source>
            <translation>A bateria está muito quente e não pode ser usada até esfriar. A alimentação CA deve permanecer conectada para continuar usando o instrumento de teste.</translation>
        </message>
        <message utf8="true">
            <source>Battery charging is not possible at this time.</source>
            <translation>Não se pode carregar a betaria nesse momento</translation>
        </message>
        <message utf8="true">
            <source>A power failure has occurred, the battery is currently not charging.</source>
            <translation>Ocorreu uma falha de energia, a bateria não está carregando no momento.</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBeepingWarning</name>
        <message utf8="true">
            <source>System Warning</source>
            <translation>Alerta de Sistema</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CBluetoothSystemTrayDataItem</name>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>File received and placed in the bluetooth inbox.&#xA;Filename: %1</source>
            <translation>Arquivo recebido e colocado na caixa de entrada bluetooth. &#xA;Nome do arquivo: %1</translation>
        </message>
        <message utf8="true">
            <source>Pair requested. You may go to the Bluetooth system page to complete the pair by clicking on the icon above.</source>
            <translation>Par requisitado. O usuário poderá ir à página do sistema Blurtooth para completar o par, clicando no ícone acima</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CNtpSystemTrayDataItem</name>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CP5000iSystemTrayDataItem</name>
        <message utf8="true">
            <source>P5000i</source>
            <translation>P5000i</translation>
        </message>
        <message utf8="true">
            <source>The P5000i Microscope will not function in this USB port.  Please use the other USB port.</source>
            <translation>O microscópico P5000i não funcionará na porta USB.  Use outra porta USB.</translation>
        </message>
        <message utf8="true">
            <source>Microscope Error</source>
            <translation>Erro do microscópico</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CStrataSyncSystemTrayDataItem</name>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CUsbFlashSystemTrayDataItem</name>
        <message utf8="true">
            <source>USB Flash</source>
            <translation>Memória USB</translation>
        </message>
        <message utf8="true">
            <source>Format complete. Please remember to eject the device from the USB system page before removing it.</source>
            <translation>Formatação completa. Por favor, lembre-se de ejetar o dispositivo antes de removê-lo</translation>
        </message>
        <message utf8="true">
            <source>Formatting USB device. Do not remove device until process is completed.</source>
            <translation>Formatando dispositivo USB. Não remova o dispositivo até o processo ser completado</translation>
        </message>
        <message utf8="true">
            <source>The USB device was removed without being ejected. Data may be corrupted.</source>
            <translation>O dispositivo USB foi removido sem ser ejetado. Os dados podem estar corrrompidos</translation>
        </message>
        <message utf8="true">
            <source>The USB device has been ejected and is safe to remove.</source>
            <translation>O dispositivo USB foi ejetado e pode ser retirado com segurança</translation>
        </message>
        <message utf8="true">
            <source>Please remember to eject the device from the USB system page before removing it.</source>
            <translation>Por favor, lembre-se de ejetar o dispositivo USB antes de retirá-lo</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CVncSystemTrayDataItem</name>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
    </context>
    <context>
        <name>systrayd::CWifiSystemTrayDataItem</name>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
    </context>
</TS>
