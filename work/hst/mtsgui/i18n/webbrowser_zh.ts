<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>查看下载资料</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>放大</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>缩小</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>重置变焦</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>下载</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>剩余  %1 分钟</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>剩余 %1 秒</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>资料下载</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>清除</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>关闭</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>另存为</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>目录：</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>文件名 :</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
    </context>
</TS>
