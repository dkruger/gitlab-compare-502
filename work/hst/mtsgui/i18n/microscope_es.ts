<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscopio Viavi</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Cargar</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Guardar</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>Foto instantánea</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>Vista 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>Vista 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>Vista 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>Vista 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Vista FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscopio</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Captura</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Congelar</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>Imagen</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Brillo</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>Contraste</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>Diseño de pantalla</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>Pantalla total</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>Mosaico</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Abandonar</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Guardar imagen</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Seleccionar imagen</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Archivos de imagen (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Todos los archivos (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleccionar</translation>
        </message>
    </context>
</TS>
