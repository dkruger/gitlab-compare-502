<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation type="unfinished"/>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation type="unfinished"/>
        </message>
    </context>
</TS>
