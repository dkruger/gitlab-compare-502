<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscope Viavi</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Charge</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauvegarde</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>Instantané</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>Vision 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>Vision 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>Vision 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>Vision 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Vision FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscope</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Capture</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Gel</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>Image</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Luminosité</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>Contraste</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>Disposition de l'écran</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>Plein écran</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>Mosaïque</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Quitter</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Sauvegarder image</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Sélectionner image</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>Fichiers image (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tous fichiers (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Sélectionner</translation>
        </message>
    </context>
</TS>
