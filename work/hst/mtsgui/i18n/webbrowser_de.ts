<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>CBrowser</name>
        <message utf8="true">
            <source>View Downloads</source>
            <translation>Downloads ansehen</translation>
        </message>
        <message utf8="true">
            <source>Zoom In</source>
            <translation>Heranzoomen</translation>
        </message>
        <message utf8="true">
            <source>Zoom Out</source>
            <translation>Verkleinern</translation>
        </message>
        <message utf8="true">
            <source>Reset Zoom</source>
            <translation>Resetzoom</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
    </context>
    <context>
        <name>CDownloadItem</name>
        <message utf8="true">
            <source>Downloading</source>
            <translation>Downloaden</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>Öffnen</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
        <message utf8="true">
            <source>%1 mins left</source>
            <translation>%1 Min übrig</translation>
        </message>
        <message utf8="true">
            <source>%1 secs left</source>
            <translation>%1 Sek übrig</translation>
        </message>
    </context>
    <context>
        <name>CDownloadManager</name>
        <message utf8="true">
            <source>Downloads</source>
            <translation>Downloads</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Löschen</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Schließen</translation>
        </message>
    </context>
    <context>
        <name>CSaveAsDialog</name>
        <message utf8="true">
            <source>Save as</source>
            <translation>Speichern unter</translation>
        </message>
        <message utf8="true">
            <source>Directory:</source>
            <translation>Verzeichnis:</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Dateiname:</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Speichern</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Abbrechen</translation>
        </message>
    </context>
</TS>
