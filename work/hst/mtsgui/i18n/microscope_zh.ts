<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Viavi显微镜</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>快照</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>视图1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>视图2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>视图3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>视图4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>视图FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>显微镜</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>捕捉</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>冻结</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>图像</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>亮度</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>对比度</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>屏幕布局</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>全屏</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>层叠</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>退出</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>保存图像</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>选择图像</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>图像文件（*.png）</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>所有文件（*）</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
    </context>
</TS>
