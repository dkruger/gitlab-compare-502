<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>guidata::CCaptureSaveActionItem</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
        <message utf8="true">
            <source> Are you sure you want to cancel?</source>
            <translation> Annullare?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWiresharkActionItem</name>
        <message utf8="true">
            <source>There was an error reading the file.</source>
            <translation>Si è verificato un problema durante la lettura del file.</translation>
        </message>
        <message utf8="true">
            <source>The file has too many frames (more than 50,000). Try saving a partial buffer,&#xA;or export it to a USB drive and load it on a different device.</source>
            <translation>Il file include un numero eccessivo di frame (più di 50.000). Provare a salvare un buffer parziale,&#xA;o esportarlo in un drive USB e caricarlo su un dispositivo diverso.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAutosaveAssocConfigItem</name>
        <message utf8="true">
            <source>Do you want to erase all stored data?</source>
            <translation>Vuoi cancellare tutti i dati memorizzati?</translation>
        </message>
        <message utf8="true">
            <source>Do you want to remove the selected item?</source>
            <translation>Vuoi rimuovere l'elemento selezionato?</translation>
        </message>
        <message utf8="true">
            <source>Name already exists.&#xA;Do you want to overwrite the old data?</source>
            <translation>Nome già esistente.&#xA;Vuoi sovrascrivere i vecchi dati?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CGraphResultStream</name>
        <message utf8="true">
            <source>Graphs are encountering errors writing to disk. Please save graphs now to preserve&#xA;your work. Graphs will stop and clear automatically after critical level reached.</source>
            <translation>I grafici incontrano errori di scrittura su disco.  Salvare ora i grafici per preservare&#xA;il proprio lavoro. I grafici vengono automaticamente arrestati ed eliminati dopo il raggiungimento del livello critico. </translation>
        </message>
        <message utf8="true">
            <source>Graphs encountered too many disk errors to continue, and have been stopped.&#xA;Graphing data cleared. You may restart graphs if you wish.</source>
            <translation>Riscontrati troppi errori su disco per continuare, i grafici sono stati arrestati.&#xA;Dati tracciamento grafici eliminati. È possibile riavviare i grafici, se desiderato.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CFlashDeviceListResultItem</name>
        <message utf8="true">
            <source>UsbFlash</source>
            <translation>UsbFlash</translation>
        </message>
        <message utf8="true">
            <source>Removable device</source>
            <translation>Dispositivo rimovibile</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLatencyDistrGraphResultItem</name>
        <message utf8="true">
            <source>&lt; %1 ms</source>
            <translation>&lt; %1 ms</translation>
        </message>
        <message utf8="true">
            <source>> %1 ms</source>
            <translation>> %1 ms</translation>
        </message>
        <message utf8="true">
            <source>%1 - %2 ms</source>
            <translation>%1 - %2 ms</translation>
        </message>
    </context>
    <context>
        <name>guidata::CLogResultItem</name>
        <message utf8="true">
            <source>Log is Full</source>
            <translation>Il registro è pieno</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMessageResultItem</name>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;</source>
            <translation>Configurazione non valida:&#xA;&#xA;</translation>
        </message>
    </context>
    <context>
        <name>guidata::CSonetSdhMapResultItem</name>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Non valido</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTriplePlayMessageResultItem</name>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voice</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Dati 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Dati 2</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertIFSpecHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>CFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Revisione specifica I/F gestione MSA CFP supportata dal software</translation>
        </message>
        <message utf8="true">
            <source>QSFP MSA Mgmt I/F Specification revision supported by SW</source>
            <translation>Revisione specifica I/F gestione MSA QSFP supportata dal software</translation>
        </message>
    </context>
    <context>
        <name>guidata::CBertUnitHardwareInfo</name>
        <message utf8="true">
            <source>DMC Info</source>
            <translation>Informazioni DMC</translation>
        </message>
        <message utf8="true">
            <source>S/N</source>
            <translation>S/N</translation>
        </message>
    </context>
    <context>
        <name>guidata::CDefaultInfoDocLayout</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>Bar Code</source>
            <translation>Codice a barre</translation>
        </message>
        <message utf8="true">
            <source>Manufacturing Date</source>
            <translation>Data di produzione</translation>
        </message>
        <message utf8="true">
            <source>Calibration Date</source>
            <translation>Data calibrazione</translation>
        </message>
        <message utf8="true">
            <source>SW Revision</source>
            <translation>Revisione SW</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID</source>
            <translation>ID sfida opzione</translation>
        </message>
        <message utf8="true">
            <source>Assembly Serial Number</source>
            <translation>Numero di serie dell'assieme</translation>
        </message>
        <message utf8="true">
            <source>Assembly Bar Code</source>
            <translation>Codice a barre assieme</translation>
        </message>
        <message utf8="true">
            <source>Rev</source>
            <translation>Rev</translation>
        </message>
        <message utf8="true">
            <source>SN</source>
            <translation>SN</translation>
        </message>
        <message utf8="true">
            <source>Installed Software</source>
            <translation>Software installato</translation>
        </message>
    </context>
    <context>
        <name>guidata::CUnitInfoDocGenerator</name>
        <message utf8="true">
            <source>Options:</source>
            <translation>Opzioni:</translation>
        </message>
    </context>
    <context>
        <name>guidata::CAnalysisRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting for packets...</source>
            <translation>Attesa per i pacchetti...</translation>
        </message>
        <message utf8="true">
            <source>Analyzing packets...</source>
            <translation>Analisi dei pacchetti...</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analisi</translation>
        </message>
        <message utf8="true">
            <source>Waiting for link...</source>
            <translation>In attesa di link...</translation>
        </message>
        <message utf8="true">
            <source>Press "Start Analysis" to begin...</source>
            <translation>Premere il tasto "Avvia analisi" per iniziare ...</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJProofController</name>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvataggio risultati, si prega di attendere.</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossibile salvare i risultati.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>I risultati sono salvati.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CJQuickCheckController</name>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
    </context>
    <context>
        <name>guidata::CMetaWizardRichTextLogUpdater</name>
        <message utf8="true">
            <source>Waiting to Start</source>
            <translation>In attesa di avvio</translation>
        </message>
        <message utf8="true">
            <source>Previous test was stopped by user</source>
            <translation>Test precedente arrestato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Previous test was aborted with errors</source>
            <translation>Test precedente interrotto con errori</translation>
        </message>
        <message utf8="true">
            <source>Previous test failed and stop on failure was enabled</source>
            <translation>Il test precedente è fallito ed era attivato l'arresto in caso di fallimento</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non eseguito</translation>
        </message>
        <message utf8="true">
            <source>Time remaining: </source>
            <translation>Tempo rimanente: </translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>In esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors</source>
            <translation>Il test non ha potuto giungere a termine ed è stata interrotto con errori</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Test completed</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Test completed with failing results</source>
            <translation>Test completato con risultati falliti</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Superato</translation>
        </message>
        <message utf8="true">
            <source>Test completed with all results passing</source>
            <translation>Test completato con tutti risultati passati</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrestato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user</source>
            <translation>Il test è stato interrotto dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Arresto</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Nessun test in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Avviamento</translation>
        </message>
        <message utf8="true">
            <source>Connection verified</source>
            <translation>Connessione verificata</translation>
        </message>
        <message utf8="true">
            <source>Connection lost</source>
            <translation>Connessione interrotta</translation>
        </message>
        <message utf8="true">
            <source>Verifying connection</source>
            <translation>Verifica della connessione</translation>
        </message>
    </context>
    <context>
        <name>guidata::CRfc2544Controller</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configurazione non valida</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvataggio risultati, si prega di attendere.</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossibile salvare i risultati.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>I risultati sono salvati.</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Grafico del test di throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Risultati del test di Throughput</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Grafico del test di throughput in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Risultati del test di throughput in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Grafico del test di throughput in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Risultati del test di throughput in downstream</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalie</translation>
        </message>
        <message utf8="true">
            <source>Upstream Anomalies</source>
            <translation>Anomalie in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Anomalies</source>
            <translation>Anomalie in downstream</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Grafico test di latenza</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Risultati test di Latenza</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Grafico del test di latenza in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Risultati del test di latenza in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Grafico del test di latenza in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Risultati del test di latenza in downstream</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Grafico del test di Jitter</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Risultati del test di Jitter</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Grafico del test di Jitter in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Risultati del test di Jitter in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Grafico del test di Jitter in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Risultati del test di Jitter in downstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 byte Grafico test di perdita frame</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 byte Risultati test di perdita frame</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 byte Grafico test di throughput perdita frame upstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 byte Risultati test di throughput perdita frame upstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 byte Grafico test di perdita frame in downstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 byte Risultati test di perdita frame downstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Risultati test CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Risultati del test di CBS in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Risultati del test di CBS in downstream</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Risultati del test CBS Policing</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Risultati del test di CBS Policing in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Risultati del test di CBS Policing in downstream</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Risultati del test di burst hunt</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Risultati del test di burst hunt in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Risultati del test di burst hunt in downstream</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Risultati test Back-to-back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Risultati test Back-to-back in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Risultati test Back-to-back in downstream</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Grafico test ripristino di sistema</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Risultati test Ripristino di sistema</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Grafico del test di ripristino sistema in upstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Risultati del test di ripristino sistema in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Grafico del test di ripristino sistema in downstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Risultati del test di ripristino sistema in downstream</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Risultati test carico esteso</translation>
        </message>
    </context>
    <context>
        <name>guidata::CTruespeedController</name>
        <message utf8="true">
            <source>Path MTU Step</source>
            <translation>Passo Path MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT Step</source>
            <translation>Passo RTT</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window Step</source>
            <translation>Passo Walk the Window in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window Step</source>
            <translation>Passo Walk the Window in downstream</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Step</source>
            <translation>Passo di throughput TCP in upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Step</source>
            <translation>Passo throughput TCP in downstream</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvataggio risultati, si prega di attendere.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossibile salvare i risultati.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>I risultati sono salvati.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangConnectMachine</name>
        <message utf8="true">
            <source>Unable to acquire sync. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Impossibile acquisire la sincronizzazione. Accertarsi che tutti i cavi siano collegati e la porta sia correttamente configurata.</translation>
        </message>
        <message utf8="true">
            <source>Could not find an active link. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Impossibile trovare un link attivo. Accertarsi che tutti i cavi siano collegati e la porta sia correttamente configurata.</translation>
        </message>
        <message utf8="true">
            <source>Could not determine the speed or duplex of the connected port. Please ensure that all of the cables are attached, and the port is configured properly.</source>
            <translation>Impossibile determinare la velocità o il duplex della porta collegata. Accertarsi che tutti i cavi siano collegati e la porta sia correttamente configurata.</translation>
        </message>
        <message utf8="true">
            <source>Could not obtain an IP address for the local test set. Please check your communication settings and try again.</source>
            <translation>Impossibile ottenere un indirizzo IP per l'apparecchio di prova locale. Controllare l'impostazione delle comunicazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a communications channel to the remote test set. Please check your communication settings and try again.</source>
            <translation>Impossibile stabilire un canale di comunicazione con l'apparecchiatura di prova remota. Controllare l'impostazione delle comunicazioni e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>The remote test set does not seem to have a compatible version of the BERT software. The minimum compatible version is %1.</source>
            <translation>La serie di test remoti non sembra avere una versione compatibile del software BERT. La minima versione compatibile è la %1.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangController</name>
        <message utf8="true">
            <source>The Layer 4 TCP Wirespeed application must be available on both the local unit and remote unit in order to run TrueSpeed</source>
            <translation>Per potere eseguire TrueSpeed l'applicazione TCP Wirespeed Layer 4 deve essere disponibile sul unità locale e sull'unità remota</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this encapsulation setting and will be removed.</source>
            <translation>La selezione del test TrueSpeed non è valida per questa impostazione di incapsulamento e sarà rimossa.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid for this frame type setting and will be removed.</source>
            <translation>La selezione del test TrueSpeed non è valida per questo tipo di frame e sarà rimossa.</translation>
        </message>
        <message utf8="true">
            <source>The TrueSpeed test selection is not valid with the remote encapsulation and will be removed.</source>
            <translation>La selezione di test TrueSpeed non è valida con l'incapsulamento remoto e sarà rimossa.</translation>
        </message>
        <message utf8="true">
            <source>The SAM-Complete test selection is not valid for this encapsulaton setting and will be removed.</source>
            <translation>La selezione del test SAM-Complete non è valida per questa impostazione di incapsulamento e sarà rimossa.</translation>
        </message>
    </context>
    <context>
        <name>guidata::CWizbangReportGenerator</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
    </context>
    <context>
        <name>guidata::CY156SamController</name>
        <message utf8="true">
            <source>Invalid Configuration</source>
            <translation>Configurazione non valida</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing</source>
            <translation>Policing CBS</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Policing</translation>
        </message>
        <message utf8="true">
            <source>Step #1</source>
            <translation>Passaggio #1</translation>
        </message>
        <message utf8="true">
            <source>Step #2</source>
            <translation>Passaggio #2</translation>
        </message>
        <message utf8="true">
            <source>Step #3</source>
            <translation>Passaggio #3</translation>
        </message>
        <message utf8="true">
            <source>Step #4</source>
            <translation>Passaggio #4</translation>
        </message>
        <message utf8="true">
            <source>Step #5</source>
            <translation>Passaggio #5</translation>
        </message>
        <message utf8="true">
            <source>Step #6</source>
            <translation>Passaggio #6</translation>
        </message>
        <message utf8="true">
            <source>Step #7</source>
            <translation>Passaggio #7</translation>
        </message>
        <message utf8="true">
            <source>Step #8</source>
            <translation>Passaggio #8</translation>
        </message>
        <message utf8="true">
            <source>Step #9</source>
            <translation>Passaggio #9</translation>
        </message>
        <message utf8="true">
            <source>Step #10</source>
            <translation>Passaggio #10</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Salvataggio risultati, si prega di attendere.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Failed to save results.</source>
            <translation>Impossibile salvare i risultati.</translation>
        </message>
        <message utf8="true">
            <source>Results are saved.</source>
            <translation>I risultati sono salvati.</translation>
        </message>
        <message utf8="true">
            <source>Local ARP failed.</source>
            <translation>ARP locale non riuscita.</translation>
        </message>
        <message utf8="true">
            <source>Remote ARP failed.</source>
            <translation>ARP remoto non riuscito</translation>
        </message>
    </context>
    <context>
        <name>report::CPdfDoc</name>
        <message utf8="true">
            <source> Table, cont.</source>
            <translation> Tabella, cont.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Screenshot</source>
            <translation>Schermata</translation>
        </message>
        <message utf8="true">
            <source>   - The Histogram is spread over multiple pages</source>
            <translation>   - L'istogramma si sviluppa su più pagine</translation>
        </message>
        <message utf8="true">
            <source>Time Scale</source>
            <translation>Scala temporale</translation>
        </message>
        <message utf8="true">
            <source>Not available</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>report::CPrint</name>
        <message utf8="true">
            <source>User Info</source>
            <translation>Informazioni utente</translation>
        </message>
        <message utf8="true">
            <source>Configuration Groups</source>
            <translation>Gruppi di configurazione</translation>
        </message>
        <message utf8="true">
            <source>Result Groups</source>
            <translation>Gruppo risultati</translation>
        </message>
        <message utf8="true">
            <source>Event Loggers</source>
            <translation>Registratori di eventi</translation>
        </message>
        <message utf8="true">
            <source>Histograms</source>
            <translation>Istogrammi</translation>
        </message>
        <message utf8="true">
            <source>Screenshots</source>
            <translation>Schermate</translation>
        </message>
        <message utf8="true">
            <source>Estimated TCP Throughput</source>
            <translation>Throughput TCP stimato</translation>
        </message>
        <message utf8="true">
            <source> Multiple Tests Report</source>
            <translation> Resoconto test multipli</translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source> Test Report</source>
            <translation> Resoconto del test</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Generato da Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Generato da Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Generato da Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Generato da strumento di prova Viavi</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintAMSTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>> Pass</source>
            <translation>> Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Scan Frequency (Hz)</source>
            <translation>Frequenza di scansione (Hz)</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>Riuscito / Non riuscito</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintBytePatternConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups: Filters /</source>
            <translation>Impostazioni: Filtri /</translation>
        </message>
        <message utf8="true">
            <source> (Pattern and Mask)</source>
            <translation> (Pattern e Maschera)</translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Pattern</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Maschera</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configurazione:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Nessuna configurazione applicabile...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintCpriTestStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI Check</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>Radio</source>
            <translation>Radio</translation>
        </message>
        <message utf8="true">
            <source>Band</source>
            <translation>Banda</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Overall Test Result</source>
            <translation>Risultati Complessivi Controllo CPRI</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test incompleto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>In progress</source>
            <translation>In corso</translation>
        </message>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Ora di fine</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>On</source>
            <translation>Acceso</translation>
        </message>
        <message utf8="true">
            <source>--</source>
            <translation>--</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>No.</translation>
        </message>
        <message utf8="true">
            <source>Event Name</source>
            <translation>Nome evento</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>Duration/Value</source>
            <translation>Durata/Valore</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Event Log</source>
            <translation>Registro eventi test carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Stato - Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>In Progress. Please Wait...</source>
            <translation>In corso. Attendere...</translation>
        </message>
        <message utf8="true">
            <source>Failed!</source>
            <translation>Non riuscito!</translation>
        </message>
        <message utf8="true">
            <source>Command Completed!</source>
            <translation>Comando Completato!</translation>
        </message>
        <message utf8="true">
            <source>Aborted!</source>
            <translation>Interrotto!</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Loop non attivo</translation>
        </message>
        <message utf8="true">
            <source>Arm</source>
            <translation>Inserire</translation>
        </message>
        <message utf8="true">
            <source>Disarm</source>
            <translation>Disinserire</translation>
        </message>
        <message utf8="true">
            <source>Power Down</source>
            <translation>Alimentazione mancante</translation>
        </message>
        <message utf8="true">
            <source>Send Loop Command</source>
            <translation>Invia comando di Loop</translation>
        </message>
        <message utf8="true">
            <source>Switch</source>
            <translation>Switch</translation>
        </message>
        <message utf8="true">
            <source>Switch Reset</source>
            <translation>Ripristino switch</translation>
        </message>
        <message utf8="true">
            <source>Issue Query</source>
            <translation>Interrogazione problema</translation>
        </message>
        <message utf8="true">
            <source>Loopback Query</source>
            <translation>Interrogazione Loopback</translation>
        </message>
        <message utf8="true">
            <source>Near End Arm</source>
            <translation>Inserisci estremità vicina</translation>
        </message>
        <message utf8="true">
            <source>Near End Disarm</source>
            <translation>Disinserisci estremità vicina</translation>
        </message>
        <message utf8="true">
            <source>Power Query</source>
            <translation>Interrogazione alimentazione</translation>
        </message>
        <message utf8="true">
            <source>Span Query</source>
            <translation>Interrogazione intervallo</translation>
        </message>
        <message utf8="true">
            <source>Timeout Disable</source>
            <translation>Disattiva Timeout</translation>
        </message>
        <message utf8="true">
            <source>Timeout Reset</source>
            <translation>Reimposta Timeout</translation>
        </message>
        <message utf8="true">
            <source>Sequential Loop</source>
            <translation>Loop sequenziale</translation>
        </message>
        <message utf8="true">
            <source>0001 Stratum 1 Trace</source>
            <translation>0001 Traccia Strato 1</translation>
        </message>
        <message utf8="true">
            <source>0010 Reserved</source>
            <translation>0010 Riservato</translation>
        </message>
        <message utf8="true">
            <source>0011 Reserved</source>
            <translation>0011 Riservato</translation>
        </message>
        <message utf8="true">
            <source>0100 Transit Node Clock Trace</source>
            <translation>0100 Traccia Clock Nodo di transito</translation>
        </message>
        <message utf8="true">
            <source>0101 Reserved</source>
            <translation>0101 Riservato</translation>
        </message>
        <message utf8="true">
            <source>0110 Reserved</source>
            <translation>0110 Riservato</translation>
        </message>
        <message utf8="true">
            <source>0111 Stratum 2 Trace</source>
            <translation>0111 Traccia Strato 2</translation>
        </message>
        <message utf8="true">
            <source>1000 Reserved</source>
            <translation>1000 Riservato</translation>
        </message>
        <message utf8="true">
            <source>1001 Reserved</source>
            <translation>1001 Riservato</translation>
        </message>
        <message utf8="true">
            <source>1010 Stratum 3 Trace</source>
            <translation>1010 Traccia Strato 3</translation>
        </message>
        <message utf8="true">
            <source>1011 Reserved</source>
            <translation>1011 Riservato</translation>
        </message>
        <message utf8="true">
            <source>1100 Sonet Min Clock Trace</source>
            <translation>1100 Traccia Clock Sonet Min</translation>
        </message>
        <message utf8="true">
            <source>1101 Stratum 3E Trace</source>
            <translation>1101 Traccia Strato 3E</translation>
        </message>
        <message utf8="true">
            <source>1110 Provision by Netwk Op</source>
            <translation>1110 Provisioning per Netwk Op</translation>
        </message>
        <message utf8="true">
            <source>1111 Don't Use for Synchronization</source>
            <translation>1111 Non usare per sincronizzazione</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-specific</source>
            <translation>Equipaggiato non specifico</translation>
        </message>
        <message utf8="true">
            <source>TUG Structure</source>
            <translation>Struttura TUG</translation>
        </message>
        <message utf8="true">
            <source>Locked TU</source>
            <translation>TU Bloccato</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 34M/45M</source>
            <translation>34M/45M asincrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous 140M</source>
            <translation>140M asincrono</translation>
        </message>
        <message utf8="true">
            <source>ATM Mapping</source>
            <translation>Mappatura ATM</translation>
        </message>
        <message utf8="true">
            <source>MAN (DQDB) Mapping</source>
            <translation>Mappatura MAN (DQDB)</translation>
        </message>
        <message utf8="true">
            <source>FDDI Mapping</source>
            <translation>Mappatura FDDI</translation>
        </message>
        <message utf8="true">
            <source>HDLC/PPP Mapping</source>
            <translation>Mappatura HDLC/PPP</translation>
        </message>
        <message utf8="true">
            <source>RFC 1619 Unscrambled</source>
            <translation>RFC 1619 Decodificato</translation>
        </message>
        <message utf8="true">
            <source>O.181 Test Signal</source>
            <translation>Segnale di prova O.181</translation>
        </message>
        <message utf8="true">
            <source>VC-AIS</source>
            <translation>VC-AIS</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous</source>
            <translation>Asincrono</translation>
        </message>
        <message utf8="true">
            <source>Bit Synchronous</source>
            <translation>Bit Sincrono</translation>
        </message>
        <message utf8="true">
            <source>Byte Synchronous</source>
            <translation>Byte sincrono</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Riservato</translation>
        </message>
        <message utf8="true">
            <source>VT-Structured STS-1 SPE</source>
            <translation>STS-1 SPE strutturato VT</translation>
        </message>
        <message utf8="true">
            <source>Locked VT Mode</source>
            <translation>Modo VT bloccato</translation>
        </message>
        <message utf8="true">
            <source>Async. DS3 Mapping</source>
            <translation>Mappatura DS3 asinc.</translation>
        </message>
        <message utf8="true">
            <source>Async. DS4NA Mapping</source>
            <translation>Mappatura DS4NA asinc.</translation>
        </message>
        <message utf8="true">
            <source>Async. FDDI Mapping</source>
            <translation>Mappatura FDDI asinc.</translation>
        </message>
        <message utf8="true">
            <source>1 VT Payload Defect</source>
            <translation>1 Mancanza di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>2 VT Payload Defects</source>
            <translation>2 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>3 VT Payload Defects</source>
            <translation>3 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>4 VT Payload Defects</source>
            <translation>4 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>5 VT Payload Defects</source>
            <translation>5 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>6 VT Payload Defects</source>
            <translation>6 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>7 VT Payload Defects</source>
            <translation>7 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>8 VT Payload Defects</source>
            <translation>8 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>9 VT Payload Defects</source>
            <translation>9 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>10 VT Payload Defects</source>
            <translation>10 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>11 VT Payload Defects</source>
            <translation>11 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>12 VT Payload Defects</source>
            <translation>12 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>13 VT Payload Defects</source>
            <translation>13 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>14 VT Payload Defects</source>
            <translation>14 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>15 VT Payload Defects</source>
            <translation>15 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>16 VT Payload Defects</source>
            <translation>16 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>17 VT Payload Defects</source>
            <translation>17 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>18 VT Payload Defects</source>
            <translation>18 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>19 VT Payload Defects</source>
            <translation>19 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>20 VT Payload Defects</source>
            <translation>20 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>21 VT Payload Defects</source>
            <translation>21 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>22 VT Payload Defects</source>
            <translation>22 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>23 VT Payload Defects</source>
            <translation>23 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>24 VT Payload Defects</source>
            <translation>24 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>25 VT Payload Defects</source>
            <translation>25 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>26 VT Payload Defects</source>
            <translation>26 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>27 VT Payload Defects</source>
            <translation>27 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>28 VT Payload Defects</source>
            <translation>28 Mancanze di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm</source>
            <translation>%dg %dh:%02dm</translation>
        </message>
        <message utf8="true">
            <source>%dd %dh:%02dm:%02ds</source>
            <translation>%dg %dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm:%02ds</source>
            <translation>%dh:%02dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dh:%02dm</source>
            <translation>%dh: %02dm</translation>
        </message>
        <message utf8="true">
            <source>%dm:%02ds</source>
            <translation>%dm:%02ds</translation>
        </message>
        <message utf8="true">
            <source>%dm</source>
            <translation>%dm</translation>
        </message>
        <message utf8="true">
            <source>%ds</source>
            <translation>%ds</translation>
        </message>
        <message utf8="true">
            <source>Format?</source>
            <translation>Formato?</translation>
        </message>
        <message utf8="true">
            <source>Out Of Range</source>
            <translation>Fuori gamma</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>HISTORY</source>
            <translation>CRONOLOGIA</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Overflow</translation>
        </message>
        <message utf8="true">
            <source> + HISTORY</source>
            <translation> + CRONOLOGIA</translation>
        </message>
        <message utf8="true">
            <source>Space</source>
            <translation>Spazio</translation>
        </message>
        <message utf8="true">
            <source>Mark</source>
            <translation>Segno</translation>
        </message>
        <message utf8="true">
            <source>GREEN</source>
            <translation>VERDE</translation>
        </message>
        <message utf8="true">
            <source>YELLOW</source>
            <translation>GIALLO</translation>
        </message>
        <message utf8="true">
            <source>RED</source>
            <translation>ROSSO</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>NESSUNO</translation>
        </message>
        <message utf8="true">
            <source>ALL</source>
            <translation>TUTTO</translation>
        </message>
        <message utf8="true">
            <source>REJECT</source>
            <translation>RIFIUTA</translation>
        </message>
        <message utf8="true">
            <source>UNCERTAIN</source>
            <translation>INCERTO</translation>
        </message>
        <message utf8="true">
            <source>ACCEPT</source>
            <translation>ACCETTA</translation>
        </message>
        <message utf8="true">
            <source>UNKNOWN</source>
            <translation>SCONOSCIUTO</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>FALLITO</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>In esecuzione</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test incompleto</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Stato sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Identified</source>
            <translation>Identificato</translation>
        </message>
        <message utf8="true">
            <source>Cannot Identify</source>
            <translation>Identificazione non riuscita</translation>
        </message>
        <message utf8="true">
            <source>Identity Unknown</source>
            <translation>Identità sconosciuta</translation>
        </message>
        <message utf8="true">
            <source>%1 hours and %2 minutes remaining</source>
            <translation>%1 ore e %2 minuti rimanenti</translation>
        </message>
        <message utf8="true">
            <source>%1 minutes remaining</source>
            <translation>%1 minuti rimanenti</translation>
        </message>
        <message utf8="true">
            <source>TOO LOW</source>
            <translation>TROPPO BASSO</translation>
        </message>
        <message utf8="true">
            <source>TOO HIGH</source>
            <translation>TROPPO ALTO</translation>
        </message>
        <message utf8="true">
            <source>(TOO LOW) </source>
            <translation>(TROPPO BASSO) </translation>
        </message>
        <message utf8="true">
            <source>(TOO HIGH) </source>
            <translation>(TROPPO ALTO) </translation>
        </message>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valore</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Trib Port</source>
            <translation>Porta Trib</translation>
        </message>
        <message utf8="true">
            <source>Undef</source>
            <translation>Indef</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>ODTU02</source>
            <translation>ODTU02</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
        <message utf8="true">
            <source>ms</source>
            <translation>ms</translation>
        </message>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non equipaggiato</translation>
        </message>
        <message utf8="true">
            <source>Mapping Under Development</source>
            <translation>Mappatura in corso di sviluppo</translation>
        </message>
        <message utf8="true">
            <source>HDLC over SONET</source>
            <translation>HDLC su SONET</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (SDH)</source>
            <translation>Mappatura Simple Data Link (SDH)</translation>
        </message>
        <message utf8="true">
            <source>HCLC/LAP-S Mapping</source>
            <translation>Mappatura HCLC/LAP-S</translation>
        </message>
        <message utf8="true">
            <source>Simple Data Link Mapping (set-reset)</source>
            <translation>Mappatura Simple Data Link (imposta-reimposta)</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Ethernet Frames Mapping</source>
            <translation>Mappatura Frame Ethernet 10 Gbit/s </translation>
        </message>
        <message utf8="true">
            <source>GFP Mapping</source>
            <translation>Mappatura GFP</translation>
        </message>
        <message utf8="true">
            <source>10 Gbit/s Fiber Channel Mapping</source>
            <translation>Mappatura Fiber Channel 10 Gbit/s </translation>
        </message>
        <message utf8="true">
            <source>Reserved - Proprietary</source>
            <translation>Riservato - Proprietario</translation>
        </message>
        <message utf8="true">
            <source>Reserved - National</source>
            <translation>Riservato - Nazionale</translation>
        </message>
        <message utf8="true">
            <source>Test Signal O.181 Mapping</source>
            <translation>Mappatura Segnale di prova O.181</translation>
        </message>
        <message utf8="true">
            <source>Reserved (%1)</source>
            <translation>Reserato (%1)</translation>
        </message>
        <message utf8="true">
            <source>Equipped Non-Specific</source>
            <translation>Equipaggiato non specifico</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS3 Mapping</source>
            <translation>Mappatura DS3 asincrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous DS4NA Mapping</source>
            <translation>Mappatura DS4NA asincrono</translation>
        </message>
        <message utf8="true">
            <source>Asynchronous FDDI Mapping</source>
            <translation>Mappatura FDDI asincrono</translation>
        </message>
        <message utf8="true">
            <source>HDLC Over SONET</source>
            <translation>HDLC su SONET</translation>
        </message>
        <message utf8="true">
            <source>%1 VT Payload Defect</source>
            <translation>%1 Mancanza di Payload VT</translation>
        </message>
        <message utf8="true">
            <source>TEI Unassgn.</source>
            <translation>TEI non assegn.</translation>
        </message>
        <message utf8="true">
            <source>Await. TEI</source>
            <translation>Attesa TEI</translation>
        </message>
        <message utf8="true">
            <source>Est. Await. TEI</source>
            <translation>Stima Attesa TEI </translation>
        </message>
        <message utf8="true">
            <source>TEI Assigned</source>
            <translation>TEI assegnato</translation>
        </message>
        <message utf8="true">
            <source>Await. Est.</source>
            <translation>Attesa stim.</translation>
        </message>
        <message utf8="true">
            <source>Await. Rel.</source>
            <translation>Attesa rel.</translation>
        </message>
        <message utf8="true">
            <source>Mult. Frm. Est.</source>
            <translation>Mult. Frm. Est.</translation>
        </message>
        <message utf8="true">
            <source>Timer Recovery</source>
            <translation>Recupero Timer</translation>
        </message>
        <message utf8="true">
            <source>Link Unknown</source>
            <translation>Link sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>AWAITING ESTABLISHMENT</source>
            <translation>ATTESA INSTAURAZIONE</translation>
        </message>
        <message utf8="true">
            <source>MULTIFRAME ESTABLISHED</source>
            <translation>MULTIFRAME INSTAURATO</translation>
        </message>
        <message utf8="true">
            <source>ONHOOK</source>
            <translation>ONHOOK</translation>
        </message>
        <message utf8="true">
            <source>DIALTONE</source>
            <translation>SEGNALE DI CENTRALE</translation>
        </message>
        <message utf8="true">
            <source>ENBLOCK DIALING</source>
            <translation>SBLOCCA COMPOSIZIONE</translation>
        </message>
        <message utf8="true">
            <source>RINGING</source>
            <translation>SQUILLO</translation>
        </message>
        <message utf8="true">
            <source>CONNECTED</source>
            <translation>CONNESSO</translation>
        </message>
        <message utf8="true">
            <source>CALL RELEASING</source>
            <translation>RILASCIO CHIAMATA</translation>
        </message>
        <message utf8="true">
            <source>Speech</source>
            <translation>Parlato</translation>
        </message>
        <message utf8="true">
            <source>3.1 KHz</source>
            <translation>3.1 KHz</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dati</translation>
        </message>
        <message utf8="true">
            <source>Fax G4</source>
            <translation>Fax G4</translation>
        </message>
        <message utf8="true">
            <source>Teletex</source>
            <translation>Teletex</translation>
        </message>
        <message utf8="true">
            <source>Videotex</source>
            <translation>Videotex</translation>
        </message>
        <message utf8="true">
            <source>Speech BC</source>
            <translation>BC Dialogo</translation>
        </message>
        <message utf8="true">
            <source>Data BC</source>
            <translation>Dati BC</translation>
        </message>
        <message utf8="true">
            <source>Data 56Kb</source>
            <translation>Dati 56Kb</translation>
        </message>
        <message utf8="true">
            <source>Fax 2/3</source>
            <translation>Fax 2/3</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Ricerca in corso</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponibile</translation>
        </message>
        <message utf8="true">
            <source>>=</source>
            <translation>>=</translation>
        </message>
        <message utf8="true">
            <source>&lt; -70.0</source>
            <translation>&lt; -70.0</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Join Request</source>
            <translation>Richiesta di partecipazione</translation>
        </message>
        <message utf8="true">
            <source>Retry Request</source>
            <translation>Prova richiesta</translation>
        </message>
        <message utf8="true">
            <source>Leave</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Ack Burst Complete</source>
            <translation>Ack Burst completato</translation>
        </message>
        <message utf8="true">
            <source>Join Response</source>
            <translation>Risposta partecipazione</translation>
        </message>
        <message utf8="true">
            <source>Burst Complete</source>
            <translation>Burst completato</translation>
        </message>
        <message utf8="true">
            <source>Status Response</source>
            <translation>Risposta stato</translation>
        </message>
        <message utf8="true">
            <source>Know Hole in Stream</source>
            <translation>Conoscere Hole in Stream</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Errore</translation>
        </message>
        <message utf8="true">
            <source>Err: Service Not Buffered Yet</source>
            <translation>Err: Servizio non ancora bufferizzato</translation>
        </message>
        <message utf8="true">
            <source>Err: Retry Packet Request is not Valid</source>
            <translation>Err: Riprova richiesta pacchetto non valido</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Service</source>
            <translation>Err: Servizio inesistente</translation>
        </message>
        <message utf8="true">
            <source>Err: No Such Section</source>
            <translation>Err: Sezione inesistente</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Error</source>
            <translation>Err: Errore sessione</translation>
        </message>
        <message utf8="true">
            <source>Err: Unsupported Command and Control Version</source>
            <translation>Err: Comando e controllo versione non supportato</translation>
        </message>
        <message utf8="true">
            <source>Err: Server Full</source>
            <translation>Err: Server pieno</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Join</source>
            <translation>Err: Partecipazione duplicata</translation>
        </message>
        <message utf8="true">
            <source>Err: Duplicate Session IDs</source>
            <translation>Err: ID sessioni duplicati</translation>
        </message>
        <message utf8="true">
            <source>Err: Bad Bit Rate</source>
            <translation>Err: Bit Rate errato</translation>
        </message>
        <message utf8="true">
            <source>Err: Session Destroyed by Server</source>
            <translation>Err: Sessione distrutta da Server</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>short</source>
            <translation>breve</translation>
        </message>
        <message utf8="true">
            <source>open</source>
            <translation>aperto</translation>
        </message>
        <message utf8="true">
            <source>MDI</source>
            <translation>MDI</translation>
        </message>
        <message utf8="true">
            <source>MDIX</source>
            <translation>MDIX</translation>
        </message>
        <message utf8="true">
            <source>10M</source>
            <translation>10M</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1000M</source>
            <translation>1000M</translation>
        </message>
        <message utf8="true">
            <source>normal</source>
            <translation>normale</translation>
        </message>
        <message utf8="true">
            <source>reversed</source>
            <translation>inverso</translation>
        </message>
        <message utf8="true">
            <source>1,2</source>
            <translation>1,2</translation>
        </message>
        <message utf8="true">
            <source>3,6</source>
            <translation>3,6</translation>
        </message>
        <message utf8="true">
            <source>4,5</source>
            <translation>4,5</translation>
        </message>
        <message utf8="true">
            <source>7,8</source>
            <translation>7,8</translation>
        </message>
        <message utf8="true">
            <source>Level Too Low</source>
            <translation>Livello troppo basso</translation>
        </message>
        <message utf8="true">
            <source>%1 bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>%1 GB</source>
            <translation>%1 GB</translation>
        </message>
        <message utf8="true">
            <source>%1 MB</source>
            <translation>%1 MB</translation>
        </message>
        <message utf8="true">
            <source>%1 KB</source>
            <translation>%1 KB</translation>
        </message>
        <message utf8="true">
            <source>%1 Bytes</source>
            <translation>%1 Bytes</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sì</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Selected</source>
            <translation>Selezionato</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>Non selezionato</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>AVVIA</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOP</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Frame errati</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Frame OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Frame persi</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violazioni del codice</translation>
        </message>
        <message utf8="true">
            <source>Event log is full</source>
            <translation>Il registro eventi è pieno</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
        <message utf8="true">
            <source>No USB key found. Please insert one and try again.&#xA;</source>
            <translation>Chiave USB non trovata. Inserire una chiava e riprovare.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Help not provided for this item.</source>
            <translation>Guida non fornita per questo elemento.</translation>
        </message>
        <message utf8="true">
            <source>Unit Id</source>
            <translation>ID unità</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>No Signal</source>
            <translation>Nessun Segnale</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Segnale</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Pronto</translation>
        </message>
        <message utf8="true">
            <source>Used</source>
            <translation>Utilizzato</translation>
        </message>
        <message utf8="true">
            <source>C/No (dB-Hz)</source>
            <translation>C/No (dB-Hz)</translation>
        </message>
        <message utf8="true">
            <source>Satellite ID</source>
            <translation>ID Satellite</translation>
        </message>
        <message utf8="true">
            <source>GNSS ID</source>
            <translation>ID GNSS</translation>
        </message>
        <message utf8="true">
            <source>S = SBAS</source>
            <translation>S = SBAS</translation>
        </message>
        <message utf8="true">
            <source>B = BeiDou</source>
            <translation>B = BeiDou</translation>
        </message>
        <message utf8="true">
            <source>R = GLONASS</source>
            <translation>R = GLONASS</translation>
        </message>
        <message utf8="true">
            <source>G = GPS</source>
            <translation>G = GPS</translation>
        </message>
        <message utf8="true">
            <source>Res</source>
            <translation>Res</translation>
        </message>
        <message utf8="true">
            <source>Stat</source>
            <translation>Stat</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Framing</translation>
        </message>
        <message utf8="true">
            <source>Exp</source>
            <translation>Exp.</translation>
        </message>
        <message utf8="true">
            <source>Mask</source>
            <translation>Maschera</translation>
        </message>
        <message utf8="true">
            <source>MTIE Mask</source>
            <translation>MTIE Mask</translation>
        </message>
        <message utf8="true">
            <source>TDEV Mask</source>
            <translation>TDEV Mask</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV (s)</source>
            <translation>MTIE/TDEV (s)</translation>
        </message>
        <message utf8="true">
            <source>MTIE results</source>
            <translation>Risultati MTIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE mask</source>
            <translation>Maschera MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV results</source>
            <translation>Risultati TDEV</translation>
        </message>
        <message utf8="true">
            <source>TDEV mask</source>
            <translation>Maschera TDEV</translation>
        </message>
        <message utf8="true">
            <source>TIE (s)</source>
            <translation>TIE (s)</translation>
        </message>
        <message utf8="true">
            <source>Orig. TIE data</source>
            <translation>Orig. dati TIE</translation>
        </message>
        <message utf8="true">
            <source>Offset rem. data</source>
            <translation>Offset dati rem.</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV Curve Style</source>
            <translation>Stile curva MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Line + Dots</source>
            <translation>Linea + punti</translation>
        </message>
        <message utf8="true">
            <source>Dots only</source>
            <translation>Solo punti</translation>
        </message>
        <message utf8="true">
            <source>MTIE only</source>
            <translation>Solo MTIE</translation>
        </message>
        <message utf8="true">
            <source>TDEV only</source>
            <translation>Solo TDEV</translation>
        </message>
        <message utf8="true">
            <source>MTIE+TDEV</source>
            <translation>MTIE+TDEV</translation>
        </message>
        <message utf8="true">
            <source>Mask Type</source>
            <translation>Tipo di maschera</translation>
        </message>
        <message utf8="true">
            <source>ANSI</source>
            <translation>ANSI</translation>
        </message>
        <message utf8="true">
            <source>ETSI</source>
            <translation>ETSI</translation>
        </message>
        <message utf8="true">
            <source>GR253</source>
            <translation>GR253</translation>
        </message>
        <message utf8="true">
            <source>ITU-T</source>
            <translation>ITU-T</translation>
        </message>
        <message utf8="true">
            <source>MTIE Passed</source>
            <translation>MTIE Rius.</translation>
        </message>
        <message utf8="true">
            <source>MTIE Failed</source>
            <translation>MTIE Non rius.</translation>
        </message>
        <message utf8="true">
            <source>TDEV Passed</source>
            <translation>TDEV Rius.</translation>
        </message>
        <message utf8="true">
            <source>TDEV Failed</source>
            <translation>TDEV Non rius.</translation>
        </message>
        <message utf8="true">
            <source>Observation Interval (s)</source>
            <translation>Intervallo di osservazione (s)</translation>
        </message>
        <message utf8="true">
            <source>Calculating </source>
            <translation>Calcolo in corso </translation>
        </message>
        <message utf8="true">
            <source>Calculation canceled</source>
            <translation>Calcolo annullato</translation>
        </message>
        <message utf8="true">
            <source>Calculation finished</source>
            <translation>Calcolo terminato</translation>
        </message>
        <message utf8="true">
            <source>Updating TIE data </source>
            <translation>Aggiornamento dati TIE </translation>
        </message>
        <message utf8="true">
            <source>TIE data loaded</source>
            <translation>Dati TIE caricati</translation>
        </message>
        <message utf8="true">
            <source>No TIE data loaded</source>
            <translation>Non sono stati caricati dati TIE</translation>
        </message>
        <message utf8="true">
            <source>Insufficient memory for running Wander Analysis locally.&#xA;256 MB ram are required. Use external analysis software instead.&#xA;See the manual for details.</source>
            <translation>Memoria insufficiente per l'esecuzione locale di Wander Analysis.&#xA;Sono richiesti 256 MB di ram. Usare un software di analisi esterno.&#xA;Per ulteriori dettagli consultare il manuale.</translation>
        </message>
        <message utf8="true">
            <source>Freq. Offset (ppm)</source>
            <translation>Offset freq. (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Drift Rate (ppm/s)</source>
            <translation>Tasso deriva (ppm/s)</translation>
        </message>
        <message utf8="true">
            <source>Samples</source>
            <translation>Campioni</translation>
        </message>
        <message utf8="true">
            <source>Sample Rate (per sec)</source>
            <translation>Freq. campion. (per sec)</translation>
        </message>
        <message utf8="true">
            <source>Blocks</source>
            <translation>Blocchi</translation>
        </message>
        <message utf8="true">
            <source>Current Block</source>
            <translation>Blocco corrente</translation>
        </message>
        <message utf8="true">
            <source>Remove Offset</source>
            <translation>Rimuovi offset</translation>
        </message>
        <message utf8="true">
            <source>Curve Selection</source>
            <translation>Selezione linea</translation>
        </message>
        <message utf8="true">
            <source>Both curves</source>
            <translation>Entrambi</translation>
        </message>
        <message utf8="true">
            <source>Offs.rem.only</source>
            <translation>Offs.solo.rem</translation>
        </message>
        <message utf8="true">
            <source>Capture Screenshot</source>
            <translation>Acquisici schermata</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>MTIE/TDEV</source>
            <translation>MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Locale</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Remoto</translation>
        </message>
        <message utf8="true">
            <source>Unlabeled</source>
            <translation>Senza etichetta</translation>
        </message>
        <message utf8="true">
            <source>Port 1</source>
            <translation>Porta 1</translation>
        </message>
        <message utf8="true">
            <source>Port 2</source>
            <translation>Porta 2</translation>
        </message>
        <message utf8="true">
            <source>Rx 1</source>
            <translation>Rx 1</translation>
        </message>
        <message utf8="true">
            <source>Rx 2</source>
            <translation>Rx 2</translation>
        </message>
        <message utf8="true">
            <source>DTE</source>
            <translation>DTE</translation>
        </message>
        <message utf8="true">
            <source>DCE</source>
            <translation>DCE</translation>
        </message>
        <message utf8="true">
            <source>Toolbar</source>
            <translation>Barra strumenti</translation>
        </message>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro messaggi</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGeneralInfoGroupDescriptor</name>
        <message utf8="true">
            <source>General Info:</source>
            <translation>Informazioni generali:</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintGraphGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Graphs Disabled</source>
            <translation>Grafici disattivati</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintHistogramGroupDescriptor</name>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Errore di stampa!</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsProgramTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps Min</source>
            <translation>Mbps Min</translation>
        </message>
        <message utf8="true">
            <source>Mbps Max</source>
            <translation>Mbps Max</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR Jitter</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR Jitter Max</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Tot err. di CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err. di CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Max err. di CC</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Tot err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Max err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Tot err. di PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err. di PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Max err. di PID</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerMptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation>N° Flussi</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Errori ChkSum OP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Errori ChkSum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest OP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID flusso di trasp.</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP Presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Tot Perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perdita pacchetti corr.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Picco perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Pkt Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Max Pkt Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot.</source>
            <translation>Tot OoS pacchetti</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS pacchetti corr.</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS Pkts Max</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Tot. err. dist.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Err. dist. corr.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Err. dist. max</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Tot err. periodo</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Err. periodo corr.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Max err. periodo</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Periodo perdita max</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist</source>
            <translation>Min. perdita dist.</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Tot. perdite sinc.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Tot. err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Err. di sinc byte corr.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Max err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF Corr</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF Max</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR Corr</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR Max</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Tot err. trasp.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Err. trasp. corr.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Max err. trasp.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Tot err. di PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Err. PAT corr.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Max err. di PAT</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerPidsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams  Analyzed</source>
            <translation>N° flussi analizzati</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Totale Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Errori ChkSum OP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Errori ChkSum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest OP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Transport Strm ID</source>
            <translation>ID flusso di trasp.</translation>
        </message>
        <message utf8="true">
            <source>Prog No</source>
            <translation>N° prog.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source># PIDs</source>
            <translation># PIDs</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Cur</source>
            <translation>Mbps prog. corr.</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min</source>
            <translation>Min. Mbps prog.</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Max</source>
            <translation>Max Mbps prog.</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP Presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Tot Perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perdita pacchetti corr.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Picco perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (ms)</source>
            <translation>Pkt Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max (ms)</source>
            <translation>Max Pkt Jitter (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Tot</source>
            <translation>Totale pacchetti OoS</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Cur</source>
            <translation>OoS pacchetti corr.</translation>
        </message>
        <message utf8="true">
            <source>OoS Pkts Max</source>
            <translation>OoS Pkts Max</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Tot</source>
            <translation>Tot. err. dist.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Cur</source>
            <translation>Err. dist. corr.</translation>
        </message>
        <message utf8="true">
            <source>Dist Err Max</source>
            <translation>Err. dist. max</translation>
        </message>
        <message utf8="true">
            <source>Period Err Tot</source>
            <translation>Tot err. periodo</translation>
        </message>
        <message utf8="true">
            <source>Period Err Cur</source>
            <translation>Err. periodo corr.</translation>
        </message>
        <message utf8="true">
            <source>Period Err Max</source>
            <translation>Max err. periodo</translation>
        </message>
        <message utf8="true">
            <source>Max Loss Period</source>
            <translation>Periodo perdita max</translation>
        </message>
        <message utf8="true">
            <source>Min Loss Dist </source>
            <translation>Min. perdita dist. </translation>
        </message>
        <message utf8="true">
            <source>Sync Losses Tot</source>
            <translation>Tot. perdite sinc.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Tot</source>
            <translation>Tot. err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Cur</source>
            <translation>Err. di sinc byte corr.</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Max err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Cur</source>
            <translation>MDI DF Corr</translation>
        </message>
        <message utf8="true">
            <source>MDI DF Max</source>
            <translation>MDI DF Max</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Cur</source>
            <translation>MDI MLR Corr</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR Max</source>
            <translation>MDI MLR Max</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Cur</source>
            <translation>PCR Jitter corr.</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max</source>
            <translation>PCR Jitter Max</translation>
        </message>
        <message utf8="true">
            <source>CC Err Tot</source>
            <translation>Tot err. di CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Cur</source>
            <translation>Err. CC corr.</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Max err. di CC</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Tot</source>
            <translation>Tot err. trasp.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Cur</source>
            <translation>Err. trasp. corr.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Max err. trasp.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Tot</source>
            <translation>Tot err. di PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Cur</source>
            <translation>Err. PAT corr.</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Max err. di PAT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Tot</source>
            <translation>Tot err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Cur</source>
            <translation>Err. PMT corr.</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Max err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>PID Err Tot</source>
            <translation>Tot err. di PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Cur</source>
            <translation>Err. PID corr.</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Max err. di PID</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsTransportTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>IP Addr</source>
            <translation>Ind. OP</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>Perdita pacch.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Max</source>
            <translation>Max perdita pacch.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit</source>
            <translation>Jit pacch.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jit Max</source>
            <translation>Max jit pacch.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvAnalyzerSptsVideoTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams</source>
            <translation>N° Flussi</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps,Cur L1</source>
            <translation>Mbps Rx, L1 corr.</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest OP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps</source>
            <translation>Prog Mbps</translation>
        </message>
        <message utf8="true">
            <source>Prog Mbps Min </source>
            <translation>Min. Mbps prog.</translation>
        </message>
        <message utf8="true">
            <source>Transport ID</source>
            <translation>ID trasporto</translation>
        </message>
        <message utf8="true">
            <source>Prog #</source>
            <translation>Prog #</translation>
        </message>
        <message utf8="true">
            <source>Sync Losses</source>
            <translation>Perdite di sinc.</translation>
        </message>
        <message utf8="true">
            <source>Tot Sync Byte Err</source>
            <translation>Tot err. sinc byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err Max</source>
            <translation>Max err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>Tot PAT Err</source>
            <translation>Tot err. PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err</source>
            <translation>Err. di PAT</translation>
        </message>
        <message utf8="true">
            <source>PAT Err Max</source>
            <translation>Max err. di PAT</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter</source>
            <translation>PCR Jitter</translation>
        </message>
        <message utf8="true">
            <source>PCR Jitter Max </source>
            <translation>PCR Jitter Max</translation>
        </message>
        <message utf8="true">
            <source>Total CC Err</source>
            <translation>Tot errori di CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err</source>
            <translation>Err. di CC</translation>
        </message>
        <message utf8="true">
            <source>CC Err Max</source>
            <translation>Max err. di CC</translation>
        </message>
        <message utf8="true">
            <source>Tot PMT Err</source>
            <translation>Tot err. PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err</source>
            <translation>Err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>PMT Err Max</source>
            <translation>Max err. di PMT</translation>
        </message>
        <message utf8="true">
            <source>Tot PID Err</source>
            <translation>Tot err. PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err</source>
            <translation>Err. di PID</translation>
        </message>
        <message utf8="true">
            <source>PID Err Max</source>
            <translation>Max err. di PID</translation>
        </message>
        <message utf8="true">
            <source>Tot Transp Err</source>
            <translation>Tot err. di trasp.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err</source>
            <translation>Err. di trasp.</translation>
        </message>
        <message utf8="true">
            <source>Transp Err Max</source>
            <translation>Max err. trasp.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIptvExplorerTableResultGroupDescriptor</name>
        <message utf8="true">
            <source># Streams Analyzed</source>
            <translation>N° flussi analizzati</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Totale Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>IP ChkSum Errs</source>
            <translation>Errori ChkSum OP</translation>
        </message>
        <message utf8="true">
            <source>UDP ChkSum Errs</source>
            <translation>Errori ChkSum UDP</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest OP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>L1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#Prgs</source>
            <translation>#Prgs</translation>
        </message>
        <message utf8="true">
            <source>MPEG</source>
            <translation>MPEG</translation>
        </message>
        <message utf8="true">
            <source>MPEG History</source>
            <translation>Cronologia MPEG</translation>
        </message>
        <message utf8="true">
            <source>RTP Present</source>
            <translation>RTP Presente</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Cur</source>
            <translation>Perdita pacchetti corr.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Tot</source>
            <translation>Tot Perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss Peak</source>
            <translation>Picco perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Cur</source>
            <translation>Pkt Jitter Corr.</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter Max</source>
            <translation>Max Pkt Jitter</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintIsdnCallHistoryResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJittWandOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Picco-Picco</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos-Picco</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Picco</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPeakPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Peak Peak</source>
            <translation>Picco-Picco</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterRMSOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterPosPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Pos Peak</source>
            <translation>Picco Pos</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJitterNegPeakOvTimeGroupDescpriptor</name>
        <message utf8="true">
            <source>Neg Peak</source>
            <translation>Picco Neg</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintJQuickCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informazioni rapporto test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintK1K2LogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Codice</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Percorso</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Can.</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>unused</source>
            <translation>Non usato</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL1OpticsStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Optics Overall Test Result</source>
            <translation>Risultato complessivo del test sulle reti ottiche</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Autotest delle reti ottiche</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintL2TransparencyConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Impostazioni:</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Dettagli</translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Stacked</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Frame</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>Stacked VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profondità stack VLAN</translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 ID</source>
            <translation>SVLAN %1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 DEI Bit</source>
            <translation>DEI Bit SVLAN %1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 User Priority</source>
            <translation>Priorità utente SVLAN %1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN %1 TPID (hex)</source>
            <translation>TPID SVLAN %1 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN %1 TPID (hex)</source>
            <translation>TPID SVLAN utente %1 (hex)</translation>
        </message>
        <message utf8="true">
            <source>No frames have been defined</source>
            <translation>Non sono stati definiti frame</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintLoopCodeTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintMsiTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>Valore</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Trib. Port</source>
            <translation>Porta Trib</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOtnCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Test</source>
            <translation>Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Overall Test Result</source>
            <translation>Risultato Complessivo del Test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test incompleto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadCaptureLogGroupDescriptor</name>
        <message utf8="true">
            <source>POH Byte Capture</source>
            <translation>Acquisizione POH byte</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Frame</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binario</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Impostazioni:</translation>
        </message>
        <message utf8="true">
            <source>No applicable setups...</source>
            <translation>Nessuna configurazione applicabile...</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOverheadResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Overhead Bytes</source>
            <translation>Overhead Bytes</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintOwdEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>CDMA Receiver</source>
            <translation>Ricevitore CDMA</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPlotGroupDescriptor</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Print error!</source>
            <translation>Errore di stampa!</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintPtpCheckStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>PTP Test</source>
            <translation>Test PTP</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Loaded Profile</source>
            <translation>Profilo Caricato</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>PTP Overall Test Result</source>
            <translation>Risultato complessivo del test PTP</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP Check</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Test incomplete</source>
            <translation>Test incompleto</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Riepilogo</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY RESULTS OK</source>
            <translation>TUTTI RISULTATI DI RIEPILOGO OK</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544CoverPageDescriptor</name>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Test FC Avanzato</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 Test</source>
            <translation>Test RFC 2544 enhanced</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Result</source>
            <translation>Risultato complessivo test</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modalità</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Results</source>
            <translation>Risultati test complessivo</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Throughput</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latenza</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Ripristino del sistema</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrestato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Non eseguito - Il test precedente è stato interrotto dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Non eseguito - Il test precedente si è interrotto con errori</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Non eseguito - Il Test precedente non è riuscito e l'arresto su mancata riuscita era attivato</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non eseguito</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Loopback</source>
            <translation>Loopback simmetrico</translation>
        </message>
        <message utf8="true">
            <source>Symmetric Upstream and Downstream</source>
            <translation>Simmetrico upstream e downstream</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric Upstream and Downstream</source>
            <translation>Upstream e downstream asimmetrici</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintRfc2544GroupDescriptor</name>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Throughput</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latenza</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Burst (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to back</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Ripristino del sistema</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>Carico esteso</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>Test da eseguire</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths Selected (bytes)</source>
            <translation>Lunghezze frame selezionate (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths Selected (bytes)</source>
            <translation>Lunghezze pacchetti selezionate (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Lengths Selected (bytes)</source>
            <translation>Lunghezze frame upstream selezionate (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Lengths Selected (bytes)</source>
            <translation>Lunghezze pacchetti in upstream selezionate (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths Selected (bytes)</source>
            <translation>Lunghezze frame downstream selezionate (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Lengths Selected (bytes)</source>
            <translation>Lunghezze pacchetti in downstream selezionate (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Lengths (bytes)</source>
            <translation>Lunghezze frame downstream (bytes)</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Graph</source>
            <translation>%1 byte Grafico test di perdita frame</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Graph</source>
            <translation>%1 byte Grafico test di throughput perdita frame upstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Graph</source>
            <translation>%1 byte Grafico test di perdita frame in downstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Graph</source>
            <translation>%1 byte Grafico test di throughput crediti buffer</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDBasicLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Overflow</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSDStatLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>Duration (ms)</source>
            <translation>Durata (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Ora di fine</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Overflow</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Più lungo</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Più breve</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Ultimo</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Interruzioni</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Totale</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSfpXfpDetailsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configurazione:</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Connettore</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Lunghezza d'onda nominale (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Bit Rate nominale (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Lunghezza d'onda (nm)</translation>
        </message>
        <message utf8="true">
            <source>Minimum Bit Rate (Mbits/sec)</source>
            <translation>Bit Rate minimo (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bit Rate (Mbits/sec)</source>
            <translation>Bit Rate massimo (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Tipo di livello potenza</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fornitore</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>PN fornitore</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Rev fornitore</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Liv. Rx max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Liv. Tx max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN fornitore</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Codice data</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Codice lotto</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Monitoraggio diagnostico</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Byte diagnostico</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Ricetrasmittente</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation>N. Versione HW/SW</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation>Spec. HW MSA  N. Rev</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation>Gestione MSA N. rev I/F</translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Power Class</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Tipo di livello potenza Rx</translation>
        </message>
        <message utf8="true">
            <source>Max Lambda Power (dBm)</source>
            <translation>Max Lambda Power (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation>N. di fibre attive</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Lunghezze d'onda per fibra</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL per gamma fibre (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Max tasso corsia rete (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>Velocità supportate</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintSigCallLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Durata</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Non valido</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTestModeAwareWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Ritardo frame (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Ritardo frame (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Ritardo unidirezionale (OWD) -</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintToeTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Row</source>
            <translation>Riga</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTracerouteResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTribSlotsConfigGroupDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Config:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TrueSpeed</source>
            <translation>RFC 6349 TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>Turn-up</source>
            <translation>Turn-up</translation>
        </message>
        <message utf8="true">
            <source>Troubleshoot</source>
            <translation>Risoluzione dei problemi</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modalità</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Simmetrico</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asimmetrico</translation>
        </message>
        <message utf8="true">
            <source>Throughput Symmetry</source>
            <translation>Simmetria throughput</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Percorso MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Throughput TCP</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avanzato</translation>
        </message>
        <message utf8="true">
            <source>Steps to Run</source>
            <translation>Passi da eseguire</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrestato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Non eseguito - Il test precedente è stato interrotto dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Non eseguito - Il test precedente si è interrotto con errori</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Non eseguito - Il Test precedente non è riuscito e l'arresto su mancata riuscita era attivato</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non eseguito</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintTrueSpeedVnfCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>Test VNF TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test incompleto</translation>
        </message>
        <message utf8="true">
            <source>The test was aborted by the user.</source>
            <translation>Test interrotto dall'utente.</translation>
        </message>
        <message utf8="true">
            <source>The test was not started.</source>
            <translation>Test non avviato.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Pass</source>
            <translation>Upstream riuscito</translation>
        </message>
        <message utf8="true">
            <source>The throughput is more than 90% of the target.</source>
            <translation>La velocità effettiva è superiore al 90% della destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Fail</source>
            <translation>Upstream non riuscito</translation>
        </message>
        <message utf8="true">
            <source>The throughput is less than 90% of the target.</source>
            <translation>La velocità effettiva è inferiore al 90% della destinazione.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Pass</source>
            <translation>Downstream riuscito</translation>
        </message>
        <message utf8="true">
            <source>Downstream Fail</source>
            <translation>Downstream non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Informazioni rapporto test</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nome del Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Società</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>E-mail</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Telefono</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identificazione test</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nome test</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Codice di autorizzazione</translation>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Data di creazione dell'autorizzazione</translation>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Ora di arresto del test</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Commenti</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Ora di fine</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableConfigGroupDescriptor</name>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Trace</translation>
        </message>
        <message utf8="true">
            <source>Sequence</source>
            <translation>Sequenza</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configurazione:</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVcgTableResultGroupDescriptor</name>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVideoEventLogGroupDescriptor</name>
        <message utf8="true">
            <source>Log:</source>
            <translation>Log:</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>StrmIP:Port</source>
            <translation>IPflusso:Porta</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nome flusso</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>Stop Time</source>
            <translation>Ora di fine</translation>
        </message>
        <message utf8="true">
            <source>Dur/Val</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Nome prog.</translation>
        </message>
        <message utf8="true">
            <source>In progress</source>
            <translation>In corso</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintVlanScanStatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Overall Test Result</source>
            <translation>Risultato test generale scansione VLAN</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>Test VLAN Scan</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWidgetsDescriptor</name>
        <message utf8="true">
            <source>Setup:</source>
            <translation>Configurazione:</translation>
        </message>
        <message utf8="true">
            <source>Results:</source>
            <translation>Risultati:</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWizbangCoverPageDescriptor</name>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM Overall Test Result</source>
            <translation>Risultato complessivo del test TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Test could not complete and was aborted with errors. Results in the report may be incomplete.</source>
            <translation>Il test non ha potuto giungere a termine ed è stata interrotto con errori. I risultati del report possono essere incompleti.</translation>
        </message>
        <message utf8="true">
            <source>Test was stopped by user. Results in the report may be incomplete.</source>
            <translation>Il test è stato interrotto dall'utente. I risultati del report possono essere incompleti.</translation>
        </message>
        <message utf8="true">
            <source>Sub-test Results</source>
            <translation>Risultati dei sotto-test</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test Result</source>
            <translation>Risultato del Test J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test Result</source>
            <translation>Risultati del test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Test Result</source>
            <translation>Risultato del Test SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test Result</source>
            <translation>Risultato del Test J-Proof</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test Result</source>
            <translation>Risultato del Test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Completato</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrestato dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was stopped by user</source>
            <translation>Non eseguito - Il test precedente è stato interrotto dall'utente</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test was aborted with errors</source>
            <translation>Non eseguito - Il test precedente si è interrotto con errori</translation>
        </message>
        <message utf8="true">
            <source>Not Run - Previous test failed and stop on failure was enabled</source>
            <translation>Non eseguito - Il Test precedente non è riuscito e l'arresto su mancata riuscita era attivato</translation>
        </message>
        <message utf8="true">
            <source>Not Run</source>
            <translation>Non eseguito</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowGroupDescriptor</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintWorkflowLogGroupDescriptor</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro messaggi</translation>
        </message>
        <message utf8="true">
            <source>Message Log (continued)</source>
            <translation>Registro dei messaggi (seguito)</translation>
        </message>
    </context>
    <context>
        <name>report::CPrintY1564StatusDescriptor</name>
        <message utf8="true">
            <source>Overview</source>
            <translation>Panoramica</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nome cliente</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID Tecnico</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Posizione del test</translation>
        </message>
        <message utf8="true">
            <source>Work Order</source>
            <translation>Ordine di lavoro</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes</source>
            <translation>Commenti/Note</translation>
        </message>
        <message utf8="true">
            <source>Instrument</source>
            <translation>Strumento</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>Numero di serie</translation>
        </message>
        <message utf8="true">
            <source>SW Version</source>
            <translation>Versione SW</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete Overall Test Result</source>
            <translation>Risultato complessivo del test SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 SAMComplete</source>
            <translation>Y.1564 SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>Data di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>Data di scadenza</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>Overall Configuration Test Results</source>
            <translation>Risultati complessivi dei test di configurazione</translation>
        </message>
        <message utf8="true">
            <source>Overall Performance Test Results</source>
            <translation>Risultati complessivi dei test delle prestazioni</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Perdita frame</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variazione ritardo</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Throughput</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
    </context>
    <context>
        <name>report::CReportFilterViewModel</name>
        <message utf8="true">
            <source>Report Groups</source>
            <translation>Gruppi report</translation>
        </message>
    </context>
    <context>
        <name>report::CReportGenerator</name>
        <message utf8="true">
            <source>Report could not be created: </source>
            <translation>Creazione resoconto non riuscita:</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space.</source>
            <translation>spazio su disco insufficiente.</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Il report non può essere creato</translation>
        </message>
    </context>
    <context>
        <name>report::CXmlDoc</name>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsPage</name>
        <message utf8="true">
            <source>Port Settings</source>
            <translation>Impostazioni porta</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Nessuno</translation>
        </message>
        <message utf8="true">
            <source>Odd</source>
            <translation>Dispari</translation>
        </message>
        <message utf8="true">
            <source>Even</source>
            <translation>Pari</translation>
        </message>
        <message utf8="true">
            <source>Baud Rate</source>
            <translation>Baud Rate</translation>
        </message>
        <message utf8="true">
            <source>Parity</source>
            <translation>Parità</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Controllo di flusso</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Spento</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Hardware</translation>
        </message>
        <message utf8="true">
            <source>XonXoff</source>
            <translation>XonXoff</translation>
        </message>
        <message utf8="true">
            <source>Data Bits</source>
            <translation>Bit di dati</translation>
        </message>
        <message utf8="true">
            <source>Stop Bits</source>
            <translation>Stop Bits</translation>
        </message>
        <message utf8="true">
            <source>Terminal Settings</source>
            <translation>Impostazioni terminale</translation>
        </message>
        <message utf8="true">
            <source>Enter/Return</source>
            <translation>Tasto invio</translation>
        </message>
        <message utf8="true">
            <source>Local Echo</source>
            <translation>Eco locale</translation>
        </message>
        <message utf8="true">
            <source>Enable Reserved Keys</source>
            <translation>Attiva tasti riservati</translation>
        </message>
        <message utf8="true">
            <source>Disabled Keys</source>
            <translation>Tasti disattivati</translation>
        </message>
        <message utf8="true">
            <source>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</source>
            <translation>F1-F12, Ctrl+U, Ctrl+Y, Ctrl+P, Ctrl+M, Ctrl+R, Ctrl+F, Ctrl+S</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Acceso</translation>
        </message>
        <message utf8="true">
            <source>Disable</source>
            <translation>Disattiva</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Abilita</translation>
        </message>
        <message utf8="true">
            <source>EXPORT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>ESPORTA, FILE, SETUP, RISULTATI, SCRIPT, START/STOP, Pannello tasti configurabili</translation>
        </message>
        <message utf8="true">
            <source>PRINT, FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>STAMPA, FILE, SETUP, RESULTATI, SCRIPT, START/STOP, Pannello tasti configurabili</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, SCRIPT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, SETUP, RESULTATI, SCRIPT, START/STOP, Pannello tasti configurabili</translation>
        </message>
        <message utf8="true">
            <source>FILE, SETUP, RESULTS, EXPORT, START/STOP, Panel Soft Keys</source>
            <translation>FILE, IMPOSTAZIONE, RISULTATI, SCRIPT, START/STOP, Pannello tasti configurabili</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100SettingsView</name>
        <message utf8="true">
            <source>Terminal&#xA;Window</source>
            <translation>Terminale&#xA;Finestra</translation>
        </message>
        <message utf8="true">
            <source>Restore&#xA;Defaults</source>
            <translation>Ripristina&#xA;Impostazioni predefinite</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100TerminalPage</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
    </context>
    <context>
        <name>ui::CVt100View</name>
        <message utf8="true">
            <source>VT100</source>
            <translation>VT100</translation>
        </message>
        <message utf8="true">
            <source>VT100&#xA;Setup</source>
            <translation>VT100&#xA;Impostazione</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Screen</source>
            <translation>Cancella&#xA;Schermo</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Keyboard</source>
            <translation>Mostra&#xA;Tastiera</translation>
        </message>
        <message utf8="true">
            <source>Move&#xA;Keyboard</source>
            <translation>Sposta&#xA;Tastiera</translation>
        </message>
        <message utf8="true">
            <source>Autobaud</source>
            <translation>Autobaud</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>Acquisizione&#xA;Schermo</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Hide&#xA;Keyboard</source>
            <translation>Nascondi&#xA;Tastiera</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoopProgressView</name>
        <message utf8="true">
            <source>Loop Progress:</source>
            <translation>Avanzamento loop:</translation>
        </message>
        <message utf8="true">
            <source>Result:</source>
            <translation>Risultato:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>È stato rilevato un errore inatteso</translation>
        </message>
    </context>
    <context>
        <name>ui::CTclScriptActionPushButton</name>
        <message utf8="true">
            <source>Run&#xA;Script</source>
            <translation>Esegui&#xA;Script</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAnalyzerFilterDialog</name>
        <message utf8="true">
            <source>&#xA;This will rescan the link for streams and restart the test.&#xA;&#xA;You will no longer see only the streams that were&#xA;transferred from the Explorer application.&#xA;&#xA;Continue?&#xA;</source>
            <translation>&#xA;Questo rieseguirà la scansione dei link per i flussi e riavvierà il test.&#xA;&#xA;Non sarà più possibile vedere solo i flussi che sono&#xA;stati trasferiti dall'applicazione Explorer.&#xA;&#xA;Continuare?&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutoProgressView</name>
        <message utf8="true">
            <source>Auto Progress:</source>
            <translation>Avanzamento automatico:</translation>
        </message>
        <message utf8="true">
            <source>Detail:</source>
            <translation>Dettaglio:</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Auto In Progress. Please Wait...</source>
            <translation>Automatico in corso. Attendere...</translation>
        </message>
        <message utf8="true">
            <source>Auto Failed.</source>
            <translation>Automatico non riuscito.</translation>
        </message>
        <message utf8="true">
            <source>Auto Completed.</source>
            <translation>Automatico completato.</translation>
        </message>
        <message utf8="true">
            <source>Auto Aborted.</source>
            <translation>Automatico annullato.</translation>
        </message>
        <message utf8="true">
            <source>Unknown - Status</source>
            <translation>Stato - Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Detecting </source>
            <translation>Rilevamento in corso... </translation>
        </message>
        <message utf8="true">
            <source>Scanning...</source>
            <translation>Scansione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Interrotto</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>Unexpected error encountered</source>
            <translation>È stato rilevato un errore inatteso</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesTableWidget</name>
        <message utf8="true">
            <source>Paired Device Details</source>
            <translation>Dettagli dispositivo associato</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>Send File</source>
            <translation>Inviare file</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connetti</translation>
        </message>
        <message utf8="true">
            <source>Forget</source>
            <translation>Dimentica</translation>
        </message>
        <message utf8="true">
            <source>Select file</source>
            <translation>Seleziona file</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Invia</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Connessione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting...</source>
            <translation>Disconnessione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Disconnetti</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundleSelectionDialog</name>
        <message utf8="true">
            <source>Bundle Name: </source>
            <translation>Nome raggruppamento: </translation>
        </message>
        <message utf8="true">
            <source>Enter bundle name:</source>
            <translation>Immettere il nome del raggruppamento:</translation>
        </message>
        <message utf8="true">
            <source>Certificates and Keys (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</source>
            <translation>Certificati e chiavi (*.pem *.der *.crt *.cert *.p12 *.pfx *.key *.prv *.priv)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Bundle Name</source>
            <translation>Nome raggruppamento non valido</translation>
        </message>
        <message utf8="true">
            <source>A bundle by the name of "%1" already exists.&#xA;Please select another name.</source>
            <translation>Esiste già una raggruppamento chiamato "%1".&#xA;Scegliere un altro nome.</translation>
        </message>
        <message utf8="true">
            <source>Add Files</source>
            <translation>Aggiungi file</translation>
        </message>
        <message utf8="true">
            <source>Create Bundle</source>
            <translation>Crea raggruppamento</translation>
        </message>
        <message utf8="true">
            <source>Rename Bundle</source>
            <translation>Rinomina raggruppamento</translation>
        </message>
        <message utf8="true">
            <source>Modify Bundle</source>
            <translation>Modifica raggruppamento</translation>
        </message>
        <message utf8="true">
            <source>Open Folder</source>
            <translation>Apri cartella</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesManagementWidget</name>
        <message utf8="true">
            <source>Add new bundle ...</source>
            <translation>Aggiunta nuovo raggruppamento...</translation>
        </message>
        <message utf8="true">
            <source>Add certificates to %1 ...</source>
            <translation>Aggiunta certificati a %1...</translation>
        </message>
        <message utf8="true">
            <source>Delete %1</source>
            <translation>Elimina %1</translation>
        </message>
        <message utf8="true">
            <source>Delete %1 from %2</source>
            <translation>Elimina %1 da %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBinaryLineEditWidget</name>
        <message utf8="true">
            <source> Bits</source>
            <translation> Bit</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgBitSkewTableWidget</name>
        <message utf8="true">
            <source>Virtual Lane ID</source>
            <translation>ID lane virtuale</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (Bits)</source>
            <translation>Max Skew (Bits)</translation>
        </message>
        <message utf8="true">
            <source>Injected Skew (ns)</source>
            <translation>Skew iniettato (ns)</translation>
        </message>
        <message utf8="true">
            <source>Physical Lane #</source>
            <translation>Corsia fisica #</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervallo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCalendarWidget</name>
        <message utf8="true">
            <source>Unable to set date</source>
            <translation>Impossibile impostare la data</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgCallDigitRangeLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Cifre</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgChannelGridWidget</name>
        <message utf8="true">
            <source>Tributary Slot</source>
            <translation>Slot tributari</translation>
        </message>
        <message utf8="true">
            <source>Apply</source>
            <translation>Applica</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predefinito</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Gbps</source>
            <translation>Gbps</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth</source>
            <translation>Larghezza di banda</translation>
        </message>
        <message utf8="true">
            <source>Changes are not yet applied.</source>
            <translation>Le modifiche non sono state ancora applicate.</translation>
        </message>
        <message utf8="true">
            <source>Too many trib. slots are selected.</source>
            <translation>È stato selezionato un numero eccessivo di slot trib.</translation>
        </message>
        <message utf8="true">
            <source>Too few trib. slots are selected.</source>
            <translation>È stato selezionato un numero insufficiente di slot trib.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgComboLineEditWidget</name>
        <message utf8="true">
            <source>Other...</source>
            <translation>Altro...</translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caratteri.</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> cifre</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDateEditWidget</name>
        <message utf8="true">
            <source>Today</source>
            <translation>Oggi</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: dd/mm/yyyy</source>
            <translation>Immettere la data: gg/mm/aaaa</translation>
        </message>
        <message utf8="true">
            <source>Enter Date: mm/dd/yyyy</source>
            <translation>Immettere la data: mm/gg/aaaa</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDigitRangeHexLineEditWidget</name>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Byte</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Fino a </translation>
        </message>
        <message utf8="true">
            <source> bytes</source>
            <translation> byte</translation>
        </message>
        <message utf8="true">
            <source> Digits</source>
            <translation> Cifre</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> cifre</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgDurationEditWidget</name>
        <message utf8="true">
            <source>Seconds</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Minutes</source>
            <translation>Minuti</translation>
        </message>
        <message utf8="true">
            <source>Hours</source>
            <translation>Ore</translation>
        </message>
        <message utf8="true">
            <source>Days</source>
            <translation>Giorni</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm:ss</source>
            <translation>dd/hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>dd/hh:mm</source>
            <translation>gg/hh:mm</translation>
        </message>
        <message utf8="true">
            <source>hh:mm:ss</source>
            <translation>hh:mm:ss</translation>
        </message>
        <message utf8="true">
            <source>hh:mm</source>
            <translation>hh:mm</translation>
        </message>
        <message utf8="true">
            <source>mm:ss</source>
            <translation>mm:ss</translation>
        </message>
        <message utf8="true">
            <source>Duration: </source>
            <translation>Durata: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgEthernetFrameWidget</name>
        <message utf8="true">
            <source>B-DA</source>
            <translation>B-DA</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Services tab.</source>
            <translation>Configurare l'indirizzo di destinazione sulla scheda Tutti i servizi.</translation>
        </message>
        <message utf8="true">
            <source>Configure Destination Address on the All Streams tab.</source>
            <translation>Configurare l'indirizzo di destinazione sulla scheda Tutti i flussi.</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Tipo di destinazione</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Tipo di Loop</translation>
        </message>
        <message utf8="true">
            <source>This Hop Source IP</source>
            <translation>OP di origine di questo Hop</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Dest IP</source>
            <translation>OP di dest. prossimo Hop</translation>
        </message>
        <message utf8="true">
            <source>Next Hop Subnet Mask</source>
            <translation>Subnet Mask prossimo Hop</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the Ethernet tab for all frames.</source>
            <translation>Configurare l'indirizzo di origine della scheda Ethernet per tutti i frame.</translation>
        </message>
        <message utf8="true">
            <source>B-SA</source>
            <translation>B-SA</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Tipo di origine</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC predefinito</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC utente</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Services tab.</source>
            <translation>Configurare l'indirizzo di origine sulla scheda Tutti i servizi.</translation>
        </message>
        <message utf8="true">
            <source>Configure Source Address on the All Streams tab.</source>
            <translation>Configurare l'indirizzo di origine sulla scheda Tutti i flussi.</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC origine</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Priorità utente SVLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>Bit DEI</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>SVLAN TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Intervallo PBit</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID SVLAN utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>Not configurable in loopback mode.</source>
            <translation>Non configurabile in modalità loopback.</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Specify VLAN ID</source>
            <translation>Specifica ID VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Priorità utente</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>Data Length (Bytes)</source>
            <translation>Lunghezza dati (bytes)</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Controlla</translation>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>L/T</source>
            <translation>L/T</translation>
        </message>
        <message utf8="true">
            <source>Type/&#xA;Length</source>
            <translation>Tipo/&#xA;Lunghezza</translation>
        </message>
        <message utf8="true">
            <source>B-TAG</source>
            <translation>B-TAG</translation>
        </message>
        <message utf8="true">
            <source>Tunnel&#xA;Label</source>
            <translation>Tunnel&#xA;Etichetta</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID Filter</source>
            <translation>Filtro ID VLAN B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag VLAN ID</source>
            <translation>B-Tag VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>B-Tag Priority</source>
            <translation>Priorità B-Tag</translation>
        </message>
        <message utf8="true">
            <source>B-Tag DEI Bit</source>
            <translation>DEI Bit B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Label</source>
            <translation>Etichetta Tunnel</translation>
        </message>
        <message utf8="true">
            <source>Tunnel Priority</source>
            <translation>Priorità Tunnel</translation>
        </message>
        <message utf8="true">
            <source>B-Tag EtherType</source>
            <translation>EtherType B-Tag</translation>
        </message>
        <message utf8="true">
            <source>Tunnel TTL</source>
            <translation>TTL Tunnel</translation>
        </message>
        <message utf8="true">
            <source>I-TAG</source>
            <translation>I-TAG</translation>
        </message>
        <message utf8="true">
            <source>VC&#xA;Label</source>
            <translation>VC&#xA;Etichetta</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Priority</source>
            <translation>Priorità I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag DEI Bit</source>
            <translation>DEI Bit I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag UCA Bit</source>
            <translation>UCA Bit I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID Filter</source>
            <translation>ID filtro servizio I-Tag</translation>
        </message>
        <message utf8="true">
            <source>I-Tag Service ID</source>
            <translation>ID Servizio I-Tag</translation>
        </message>
        <message utf8="true">
            <source>VC Label</source>
            <translation>Etichetta VC</translation>
        </message>
        <message utf8="true">
            <source>VC Priority</source>
            <translation>Priorità VC</translation>
        </message>
        <message utf8="true">
            <source>I-Tag EtherType</source>
            <translation>EtherType I-Tag</translation>
        </message>
        <message utf8="true">
            <source>VC TTL</source>
            <translation>VC TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS1&#xA;Label</source>
            <translation>MPLS1&#xA;Etichetta</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Label</source>
            <translation>Etichetta MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 Priority</source>
            <translation>Priorità MPLS1</translation>
        </message>
        <message utf8="true">
            <source>MPLS1 TTL</source>
            <translation>MPLS1 TTL</translation>
        </message>
        <message utf8="true">
            <source>MPLS2&#xA;Label</source>
            <translation>MPLS2&#xA;Etichetta</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Label</source>
            <translation>Etichetta MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 Priority</source>
            <translation>Priorità MPLS2</translation>
        </message>
        <message utf8="true">
            <source>MPLS2 TTL</source>
            <translation>MPLS2 TTL</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dati</translation>
        </message>
        <message utf8="true">
            <source>Customer frame being carried:</source>
            <translation>Frame del cliente trasportata:</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Incapsulamento</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Tipo di frame</translation>
        </message>
        <message utf8="true">
            <source>Customer Frame Size</source>
            <translation>Dimensione frame cliente</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Dimensione frame utente</translation>
        </message>
        <message utf8="true">
            <source>Undersized Size</source>
            <translation>Dimensione sotto misura</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size</source>
            <translation>Dimensione jumbo</translation>
        </message>
        <message utf8="true">
            <source>Data section contains an IP packet. Configure this packet on the IP tab.</source>
            <translation>La sezione dati contiene un pacchetto OP. Configurare questo pacchetto nella scheda OP.</translation>
        </message>
        <message utf8="true">
            <source>Configure data filtering on the IP Filter tab.</source>
            <translation>Configurare il filtraggio dei dati nella scheda Filtro OP.</translation>
        </message>
        <message utf8="true">
            <source>Tx Payload</source>
            <translation>Payload Tx</translation>
        </message>
        <message utf8="true">
            <source>RTD Setup</source>
            <translation>Impostazione RTD</translation>
        </message>
        <message utf8="true">
            <source>Payload Analysis</source>
            <translation>Analisi payload</translation>
        </message>
        <message utf8="true">
            <source>Rx Payload</source>
            <translation>Payload Rx</translation>
        </message>
        <message utf8="true">
            <source>LPAC Timer</source>
            <translation>Timer LPAC</translation>
        </message>
        <message utf8="true">
            <source>BERT Rx&lt;=Tx</source>
            <translation>BERT Rx&lt;=Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx BERT Pattern</source>
            <translation>Pattern BERT Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx BERT Pattern</source>
            <translation>Pattern BERT Tx</translation>
        </message>
        <message utf8="true">
            <source>User Pattern</source>
            <translation>Pattern utente</translation>
        </message>
        <message utf8="true">
            <source>Configure incoming frames:</source>
            <translation>Configura frame in ingresso:</translation>
        </message>
        <message utf8="true">
            <source>Configure outgoing frames:</source>
            <translation>Configura frame in uscita:</translation>
        </message>
        <message utf8="true">
            <source>Length/Type field is 0x8870</source>
            <translation>Il campo Lunghezza / Tipo è 0x8870</translation>
        </message>
        <message utf8="true">
            <source>Data Length is Random</source>
            <translation>La lunghezza dei dati è casuale</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Indice avvio priorità utente</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgFileSelectorWidget</name>
        <message utf8="true">
            <source>File Type:</source>
            <translation>Tipo di file:</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nome file:</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Sicuro di volere eliminare&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all files within this folder?</source>
            <translation>Sicuro di volere eliminare tutti i file in questa cartella?</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgHexLineEditWidget</name>
        <message utf8="true">
            <source> Digits</source>
            <translation> Cifre</translation>
        </message>
        <message utf8="true">
            <source> Byte</source>
            <translation>Byte</translation>
        </message>
        <message utf8="true">
            <source>Up to </source>
            <translation>Fino a </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Byte</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervallo:  </translation>
        </message>
        <message utf8="true">
            <source> (hex)</source>
            <translation> (hex)</translation>
        </message>
        <message utf8="true">
            <source> digits</source>
            <translation> cifre</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPLineEditWidget</name>
        <message utf8="true">
            <source> is invalid - Source IPs of Ports 1 and&#xA;2 should not match. Previous IP restored.</source>
            <translation> non è valido - gli OP origine delle porte 1 e&#xA;2 non devono essere uguali. OP precedente restaurato.</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Address&#xA;</source>
            <translation>Indirizzo OP non valido&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervallo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgIPv6LineEditWidget</name>
        <message utf8="true">
            <source>The given IP Address is not suitable for this setup.&#xA;</source>
            <translation>L'indirizzo OP fornito non è idoneo per questa configurazione.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLongByteWidget</name>
        <message utf8="true">
            <source>2 characters per byte, up to </source>
            <translation>2 caratteri per byte, fino a </translation>
        </message>
        <message utf8="true">
            <source> Bytes</source>
            <translation> Byte</translation>
        </message>
        <message utf8="true">
            <source>2 characters per byte, </source>
            <translation>2 caratteri per byte, </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgLoopCodeEditWidget</name>
        <message utf8="true">
            <source>Loop-Code name</source>
            <translation>Nome codice Loop</translation>
        </message>
        <message utf8="true">
            <source>Bit Pattern</source>
            <translation>Bit Pattern</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delivery</source>
            <translation>Consegna</translation>
        </message>
        <message utf8="true">
            <source>In Band</source>
            <translation>In banda</translation>
        </message>
        <message utf8="true">
            <source>Out of Band</source>
            <translation>Fuori banda</translation>
        </message>
        <message utf8="true">
            <source>Loop Up</source>
            <translation>Loop attivo</translation>
        </message>
        <message utf8="true">
            <source>Loop Down</source>
            <translation>Loop non attivo</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Altro</translation>
        </message>
        <message utf8="true">
            <source>Loop-Code Name</source>
            <translation>Nome codice Loop</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
        <message utf8="true">
            <source>3 .. 16 Bits</source>
            <translation>3 .. 16 Bits</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberEditWidgetBase</name>
        <message utf8="true">
            <source>KLM</source>
            <translation>KLM</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>STS-N</source>
            <translation>STS-N</translation>
        </message>
        <message utf8="true">
            <source>Del.</source>
            <translation>Rit.,''''</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>State</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Add/Remove</source>
            <translation>Aggiungi/Rimuovi</translation>
        </message>
        <message utf8="true">
            <source>Def.</source>
            <translation>Def.</translation>
        </message>
        <message utf8="true">
            <source>Tx Trace</source>
            <translation>Tracciamento Tx</translation>
        </message>
        <message utf8="true">
            <source>Range: </source>
            <translation>Intervallo:  </translation>
        </message>
        <message utf8="true">
            <source>Select channel</source>
            <translation>Seleziona canale</translation>
        </message>
        <message utf8="true">
            <source>Enter KLM value</source>
            <translation>Immettere il valore KLM</translation>
        </message>
        <message utf8="true">
            <source>Range:</source>
            <translation>Intervallo:  </translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Selezionare il membro del VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>Seq.</source>
            <translation>Seq.</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMembersTraceTableWidget</name>
        <message utf8="true">
            <source>TS</source>
            <translation>TS</translation>
        </message>
        <message utf8="true">
            <source>Ch</source>
            <translation>Ch</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMsiTableWidget</name>
        <message utf8="true">
            <source>PSI Byte</source>
            <translation>PSI Byte</translation>
        </message>
        <message utf8="true">
            <source>Byte Value</source>
            <translation>Valore del byte</translation>
        </message>
        <message utf8="true">
            <source>ODU Type</source>
            <translation>Tipo di ODU</translation>
        </message>
        <message utf8="true">
            <source>Tributary Port #</source>
            <translation>N° Porta Tributaria</translation>
        </message>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervallo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgMultiMemberSelectorWidget</name>
        <message utf8="true">
            <source>Select VCG Member</source>
            <translation>Selezionare il membro del VCG</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgNumericLineEditWidget</name>
        <message utf8="true">
            <source>Range:  </source>
            <translation>Intervallo:  </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgOhBytesGroupBoxWidget</name>
        <message utf8="true">
            <source>Overhead Byte Editor</source>
            <translation>Editor Overhead Byte </translation>
        </message>
        <message utf8="true">
            <source>Select a valid byte to edit.</source>
            <translation>Selezionare un bite idoneo da editare.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageOpticsLabelWidget</name>
        <message utf8="true">
            <source>Unrecognized optic</source>
            <translation>Ottica non riconosciuta</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPageSectionWidget</name>
        <message utf8="true">
            <source>Building page.  Please wait...</source>
            <translation>Elaborazione pagina.  Attendere...</translation>
        </message>
        <message utf8="true">
            <source>Page is empty.</source>
            <translation>La pagina è vuota.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgPairTableWidget</name>
        <message utf8="true">
            <source>Add Item</source>
            <translation>Aggiungi elemento</translation>
        </message>
        <message utf8="true">
            <source>Modify Item</source>
            <translation>Modifica elemento</translation>
        </message>
        <message utf8="true">
            <source>Delete Item</source>
            <translation>Elimina elemento</translation>
        </message>
        <message utf8="true">
            <source>Add Row</source>
            <translation>Aggiungi riga</translation>
        </message>
        <message utf8="true">
            <source>Modify Row</source>
            <translation>Modifica riga</translation>
        </message>
        <message utf8="true">
            <source>Delete Row</source>
            <translation>Elimina riga</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgProfileWidget</name>
        <message utf8="true">
            <source>Off</source>
            <translation>Spento</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgS1SyncStatusWidget</name>
        <message utf8="true">
            <source>0000 Traceability Unknown</source>
            <translation>0000 Tracciabilità sconosciuta</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhHPLPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non equipaggiato</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSdhLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non equipaggiato</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetHPC2Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non equipaggiato</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgSonetLPV5Widget</name>
        <message utf8="true">
            <source>Unequipped</source>
            <translation>Non equipaggiato</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgStringLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Lunghezza:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caratteri.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTextEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
        <message utf8="true">
            <source>Length:  </source>
            <translation>Lunghezza:  </translation>
        </message>
        <message utf8="true">
            <source> characters</source>
            <translation> caratteri.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeEditWidget</name>
        <message utf8="true">
            <source>Now</source>
            <translation>Adesso</translation>
        </message>
        <message utf8="true">
            <source>Enter Time: hh:mm:ss</source>
            <translation>Immettere l'ora: hh:mm:ss</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTimeslotWidget</name>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Deseleziona tutto</translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTraceLineEditWidget</name>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgTracePartialEditWidget</name>
        <message utf8="true">
            <source>Byte %1</source>
            <translation>Byte %1</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
    </context>
    <context>
        <name>ui::CCfgXBitBinaryLineEditWidget</name>
        <message utf8="true">
            <source>Bits</source>
            <translation> Bit</translation>
        </message>
    </context>
    <context>
        <name>ui::CConfigureServiceDialog</name>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nome servizio</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Upstream</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Downstream</translation>
        </message>
        <message utf8="true">
            <source>Both Directions</source>
            <translation>Bidirezionale</translation>
        </message>
        <message utf8="true">
            <source>Service Type</source>
            <translation>Tipo di servizio</translation>
        </message>
        <message utf8="true">
            <source>Service Type has been reset to Data because of changes to Frame Length and/or CIR.</source>
            <translation>Il tipo di servizio Service è stato reimpostato su Dati a causa di modifiche nella lunghezza dei Frame o nel CIR.</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling Rate (ms)</source>
            <translation>Frequenza di campionamento (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation>N°. di chiamate</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Dimensioni frame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Lungh. pacchetto</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>N°. di canali</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compressione</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateVCGDialog</name>
        <message utf8="true">
            <source>Create VCG</source>
            <translation>Crea VCG</translation>
        </message>
        <message utf8="true">
            <source>Define VCG members with default channel numbering:</source>
            <translation>Definire i membri del VCG con numerazione predefinita dei canali:</translation>
        </message>
        <message utf8="true">
            <source>Define Tx VCG</source>
            <translation>Definisci VCG Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Define Rx VCG</source>
            <translation>Definisci VCG Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>Payload bandwidth (Mbps)</source>
            <translation>Payload di banda (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Number of Members</source>
            <translation>Numero di membri</translation>
        </message>
        <message utf8="true">
            <source>Distribute&#xA;Members</source>
            <translation>Distribuisci&#xA;Membri</translation>
        </message>
    </context>
    <context>
        <name>ui::CDistributeMembersDialog</name>
        <message utf8="true">
            <source>Distribute VCG Members</source>
            <translation>Distribuisco membri di VCG</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Define custom distribution of VCG members</source>
            <translation>Definisci distribuzione personalizzata dei membri di VCG</translation>
        </message>
        <message utf8="true">
            <source>Instance</source>
            <translation>Istanza</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Fase</translation>
        </message>
    </context>
    <context>
        <name>ui::CEditVCGDialog</name>
        <message utf8="true">
            <source>Edit VCG Members</source>
            <translation>Modifica membri di VCG</translation>
        </message>
        <message utf8="true">
            <source>Address Format</source>
            <translation>Formato indirizzo</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx => Rx</source>
            <translation>Tx => Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx => Tx</source>
            <translation>Rx => Tx</translation>
        </message>
        <message utf8="true">
            <source>New member</source>
            <translation>Nuovo membro</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predefinito</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpInfo</name>
        <message utf8="true">
            <source>Joined streams have been imported from Explorer. &#xA;Press [Join Streams...] button to join.</source>
            <translation>I flussi partecipati sono stati importati da Explorer. &#xA;Premere il pulsante [Partecipa a flussi...] per partecipare.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src OP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>OP dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CAddressBookWidget</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Rubrica indirizzi</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Deseleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Find Next</source>
            <translation>Trova successivo</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedStreamsWidget</name>
        <message utf8="true">
            <source>Remove</source>
            <translation>Rimuovi</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Select None</source>
            <translation>Deseleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src OP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>OP dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpJoinDialog</name>
        <message utf8="true">
            <source>Join Streams...</source>
            <translation>Partecipa a flussi...</translation>
        </message>
        <message utf8="true">
            <source>Add</source>
            <translation>Aggiungi</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>OP origine</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>OP di destinazione</translation>
        </message>
        <message utf8="true">
            <source>IPs entered are added to the Address Book.</source>
            <translation>Gli IP inseriti sono aggiunti alla Rubrica indirizzi.</translation>
        </message>
        <message utf8="true">
            <source>To Join</source>
            <translation>Per partecipare</translation>
        </message>
        <message utf8="true">
            <source>Already Joined</source>
            <translation>Già partecipato</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src OP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>OP dest</translation>
        </message>
        <message utf8="true">
            <source>Join</source>
            <translation>Partecipa</translation>
        </message>
    </context>
    <context>
        <name>ui::CIPAddressTableModel</name>
        <message utf8="true">
            <source>Leave Stream</source>
            <translation>Lascia flusso</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Indirizzo OP di origine</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP Address</source>
            <translation>Indirizzo OP di dest.</translation>
        </message>
    </context>
    <context>
        <name>ui::CIgmpLeaveDialog</name>
        <message utf8="true">
            <source>Leave Streams...</source>
            <translation>Lascia flussi...</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Deseleziona tutto</translation>
        </message>
    </context>
    <context>
        <name>ui::CL2TranspQuickCfgDialog</name>
        <message utf8="true">
            <source>Quick Config</source>
            <translation>Config. rapida</translation>
        </message>
        <message utf8="true">
            <source>Note: This will override the current frame configuration.</source>
            <translation>Nota: questo sostituirà la configurazione frame corrente.</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rapido (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (20)</source>
            <translation>Completo (20)</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensità</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Famiglia</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Incapsulamento</translation>
        </message>
        <message utf8="true">
            <source>ID</source>
            <translation>ID</translation>
        </message>
        <message utf8="true">
            <source>PBit Increment</source>
            <translation>Intervallo PBit</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Priorità utente</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)  </source>
            <translation>TPID (hex)  </translation>
        </message>
        <message utf8="true">
            <source>User TPID (hex)</source>
            <translation>TPID utente (hex)</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Applicazione</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>User Priority Start Index</source>
            <translation>Indice avvio priorità utente</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Priorità utente SVLAN</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadValueButton</name>
        <message utf8="true">
            <source>Load...</source>
            <translation>Carica...</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
    </context>
    <context>
        <name>ui::CLocaleSampleWidget</name>
        <message utf8="true">
            <source>Long date:</source>
            <translation>Data estesa:</translation>
        </message>
        <message utf8="true">
            <source>Short date:</source>
            <translation>Data abbreviata:</translation>
        </message>
        <message utf8="true">
            <source>Long time:</source>
            <translation>Tempo lungo:</translation>
        </message>
        <message utf8="true">
            <source>Short time:</source>
            <translation>Tempo breve:</translation>
        </message>
        <message utf8="true">
            <source>Numbers:</source>
            <translation>Numeri:</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnMsiHelper</name>
        <message utf8="true">
            <source>Unallocated</source>
            <translation>Non allocato</translation>
        </message>
        <message utf8="true">
            <source>Allocated</source>
            <translation>Allocato</translation>
        </message>
        <message utf8="true">
            <source>ODTU13</source>
            <translation>ODTU13</translation>
        </message>
        <message utf8="true">
            <source>ODTU23</source>
            <translation>ODTU23</translation>
        </message>
        <message utf8="true">
            <source>Reserved10</source>
            <translation>Reserved10</translation>
        </message>
        <message utf8="true">
            <source>Reserved11</source>
            <translation>Reserved11</translation>
        </message>
        <message utf8="true">
            <source>ODTU3ts</source>
            <translation>ODTU3ts</translation>
        </message>
        <message utf8="true">
            <source>ODTU12</source>
            <translation>ODTU12</translation>
        </message>
        <message utf8="true">
            <source>Reserved01</source>
            <translation>Reserved01</translation>
        </message>
        <message utf8="true">
            <source>ODTU2ts</source>
            <translation>ODTU2ts</translation>
        </message>
        <message utf8="true">
            <source>Reserved00</source>
            <translation>Riservato00</translation>
        </message>
        <message utf8="true">
            <source>ODTU01</source>
            <translation>ODTU01</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveValueButton</name>
        <message utf8="true">
            <source>Save...</source>
            <translation>Salva...</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
    </context>
    <context>
        <name>ui::CSetupPagesView_WSVGA</name>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Ripristina test su valori predefiniti</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionButton</name>
        <message utf8="true">
            <source>Configure&#xA; Streams...</source>
            <translation>Configura&#xA;flussi...</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsLoadDistributionDialog</name>
        <message utf8="true">
            <source>Load Distribution</source>
            <translation>Distribuzione del carico</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Seleziona&#xA;Tutto</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;All</source>
            <translation>Cancella&#xA;Tutto</translation>
        </message>
        <message utf8="true">
            <source>Auto&#xA;Distribute</source>
            <translation>Auto&#xA;Distribuisci</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Flusso</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Dimensioni frame</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>Frame Rate</source>
            <translation>Velocità frame</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>% della velocità della linea</translation>
        </message>
        <message utf8="true">
            <source>Ramp starting at</source>
            <translation>Rampa a partire da</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Constant</source>
            <translation>Costante</translation>
        </message>
        <message utf8="true">
            <source>Max Util Threshold</source>
            <translation>Soglia max util.</translation>
        </message>
        <message utf8="true">
            <source>Total (%)</source>
            <translation>Totale (%)</translation>
        </message>
        <message utf8="true">
            <source>Total (Mbps)</source>
            <translation>Totale (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (kbps)</source>
            <translation>Totale (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total (fps)</source>
            <translation>Totale (fps)</translation>
        </message>
        <message utf8="true">
            <source>Note: </source>
            <translation>Nota: </translation>
        </message>
        <message utf8="true">
            <source>At least one stream must be enabled.</source>
            <translation>Si deve attivare almeno un flusso.</translation>
        </message>
        <message utf8="true">
            <source>The maximum utilization threshold is </source>
            <translation>La soglia massimo di utilizzo è </translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>Il carico massimo è </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>Carico totale specificato non deve superare tale valore.</translation>
        </message>
        <message utf8="true">
            <source>Enter percent:  </source>
            <translation>Immetti percentuale:  </translation>
        </message>
        <message utf8="true">
            <source>Enter frame rate:  </source>
            <translation>Inserire frequenza frame:  </translation>
        </message>
        <message utf8="true">
            <source>Enter bit rate:  </source>
            <translation>Immetti bit rate:  </translation>
        </message>
        <message utf8="true">
            <source>Note:&#xA;Bit rate not detected. Please press Cancel&#xA;and retry when the bit rate has been detected.</source>
            <translation>Nota:&#xA;bit rate non rilevato. Premere Annulla&#xA;riprovare quando il bit rate è stato rilevato</translation>
        </message>
    </context>
    <context>
        <name>ui::CTriplePlayTrafficSettingsDialog</name>
        <message utf8="true">
            <source>Define Triple Play Services</source>
            <translation>Definizione servizi Triple Play...</translation>
        </message>
        <message utf8="true">
            <source>Codec</source>
            <translation>CODEC</translation>
        </message>
        <message utf8="true">
            <source>Sampling&#xA;Rate (ms)</source>
            <translation>Frequenza di&#xA;campionamento (ms)</translation>
        </message>
        <message utf8="true">
            <source># Calls</source>
            <translation>N°. di chiamate</translation>
        </message>
        <message utf8="true">
            <source>Per Call&#xA;Rate (kbps)</source>
            <translation>Rate per &#xA;chiamata (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Rate&#xA;(Mbps)</source>
            <translation>Rate totale&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame&#xA;Size (Bytes)</source>
            <translation>Totale Basic Frame&#xA;Dimensioni (byte)</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>Silence Suppression</source>
            <translation>Soppressione silenzio</translation>
        </message>
        <message utf8="true">
            <source>Jitter Buffer (ms)</source>
            <translation>Jitter Buffer (ms)</translation>
        </message>
        <message utf8="true">
            <source># Channels</source>
            <translation>N°. di canali</translation>
        </message>
        <message utf8="true">
            <source>Compression</source>
            <translation>Compressione</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>Rate (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Basic Frame Size (Bytes)</source>
            <translation>Dimensione totale Basic Frame (bytes)</translation>
        </message>
        <message utf8="true">
            <source>Start Rate (Mbps)</source>
            <translation>Rate iniziale (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load Type</source>
            <translation>Tipo di carico</translation>
        </message>
        <message utf8="true">
            <source>Time Step (Sec)</source>
            <translation>Incremento temporale (sec)</translation>
        </message>
        <message utf8="true">
            <source>Load Step (Mbps)</source>
            <translation>Incremento di carico (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total L1 Mbps</source>
            <translation>Totale Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>Simulated</source>
            <translation>Simulato</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>Data 1</source>
            <translation>Dati 1</translation>
        </message>
        <message utf8="true">
            <source>Data 2</source>
            <translation>Dati 2</translation>
        </message>
        <message utf8="true">
            <source>Data 3</source>
            <translation>Dati 3</translation>
        </message>
        <message utf8="true">
            <source>Data 4</source>
            <translation>Dati 4</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Casuale</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voice</translation>
        </message>
        <message utf8="true">
            <source>Video</source>
            <translation>Video</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Dati</translation>
        </message>
        <message utf8="true">
            <source>Note:</source>
            <translation>Nota:</translation>
        </message>
        <message utf8="true">
            <source>At least one service must be enabled.</source>
            <translation>Si deve attivare almeno un servizio.</translation>
        </message>
        <message utf8="true">
            <source>The maximum possible load is </source>
            <translation>Il carico massimo è </translation>
        </message>
        <message utf8="true">
            <source>Total load specified must not exceed this.</source>
            <translation>Carico totale specificato non deve superare tale valore.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVCGBandwidthStructureWidget</name>
        <message utf8="true">
            <source>Distribution: </source>
            <translation>Distribuzione: </translation>
        </message>
        <message utf8="true">
            <source>Bandwidth: </source>
            <translation>Larghezza di banda: </translation>
        </message>
        <message utf8="true">
            <source>Structure: </source>
            <translation>Struttura: </translation>
        </message>
        <message utf8="true">
            <source> Mbps</source>
            <translation> Mbps</translation>
        </message>
        <message utf8="true">
            <source>default</source>
            <translation>predefinita</translation>
        </message>
        <message utf8="true">
            <source>Step</source>
            <translation>Fase</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookDialog</name>
        <message utf8="true">
            <source>Address Book</source>
            <translation>Rubrica indirizzi</translation>
        </message>
        <message utf8="true">
            <source>New Entry</source>
            <translation>Nuovo valore</translation>
        </message>
        <message utf8="true">
            <source>Source IP (0.0.0.0 = "Any")</source>
            <translation>OP origine (0.0.0.0 = "Qualsiasi")</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>Dest OP</translation>
        </message>
        <message utf8="true">
            <source>Required</source>
            <translation>Obbligatorio</translation>
        </message>
        <message utf8="true">
            <source>Name (max length 16 characters)</source>
            <translation>Nome (lunghezza max 16 caratteri)</translation>
        </message>
        <message utf8="true">
            <source>Add Entry</source>
            <translation>Aggiungi voce</translation>
        </message>
        <message utf8="true">
            <source>Import/Export</source>
            <translation>Import/Export</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importa</translation>
        </message>
        <message utf8="true">
            <source>Import entries from USB drive</source>
            <translation>Importa voci da unità USB</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Esporta</translation>
        </message>
        <message utf8="true">
            <source>Export entries to a USB drive</source>
            <translation>Esporta voci su unità USB</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Elimina tutto</translation>
        </message>
        <message utf8="true">
            <source>Save and Close</source>
            <translation>Salva e chiudi</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Optional</source>
            <translation>Facoltativo</translation>
        </message>
        <message utf8="true">
            <source>Import Entries From USB</source>
            <translation>Importa voci da USB</translation>
        </message>
        <message utf8="true">
            <source>Import Entries</source>
            <translation>Importa voci</translation>
        </message>
        <message utf8="true">
            <source>Comma Separated (*.csv)</source>
            <translation>Delimitato da virgole (*.csv)</translation>
        </message>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Aggiunta di una voce</translation>
        </message>
        <message utf8="true">
            <source>Each entry must have 4 fields: &lt;Src. IP>,&lt;Dest. IP>,&lt;PID>,&lt;Name> separated by commas.  Line skipped.</source>
            <translation>Ogni voce deve avere 4 campi: &lt;OP orig.>,&lt;OP dest.>,&lt;PID>,&lt;Nome> separati da virgole.  Riga ignorata.</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Termina</translation>
        </message>
        <message utf8="true">
            <source>Continue</source>
            <translation>Continua</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Source IP Address.  Line skipped.</source>
            <translation>non è un Indirizzo OP origine valido.  Riga ignorata.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid Destination IP Address.  Line skipped.</source>
            <translation>non è un Indirizzo OP di destinazione valido.  Riga ignorata.</translation>
        </message>
        <message utf8="true">
            <source>is not a valid PID.  Line skipped.</source>
            <translation>non è un PID valido.  Riga ignorata.</translation>
        </message>
        <message utf8="true">
            <source>An entry must have a name (up to 16 characters).  Line skipped</source>
            <translation>Una voce deve avere un nome (fino a 16 caratteri ).  Riga ignorata</translation>
        </message>
        <message utf8="true">
            <source>An entry name must not be longer than 16 characters.  Line skipped</source>
            <translation>Il nome della voce non deve essere più lungo di 16 caratteri.  Riga ignorata</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same Src. Ip, Dest. IP and PID&#xA;already exists and has a name</source>
            <translation>Una voce con identici Ip orig., OP dest. e PID è già&#xA;presente con un nome</translation>
        </message>
        <message utf8="true">
            <source>OVERWRITE the name of existing entry&#xA;or&#xA;SKIP importing this item?</source>
            <translation>SOSTITUIRE il nome della voce esistente&#xA;oppure&#xA;IGNORARE l'importazione di questa voce?</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>Ignora</translation>
        </message>
        <message utf8="true">
            <source>Overwrite</source>
            <translation>Sovrascrivi</translation>
        </message>
        <message utf8="true">
            <source>One of the imported entries had a PID value.&#xA;Entries with PID values are only used in MPTS applications.</source>
            <translation>Una delle voci importate aveva un valore PID.&#xA;Le voci con valori PID vengono utilizzate solo in applicazioni MPTS.</translation>
        </message>
        <message utf8="true">
            <source>Export Entries To USB</source>
            <translation>Esporta voci su USB</translation>
        </message>
        <message utf8="true">
            <source>Export Entries</source>
            <translation>Esporta voci</translation>
        </message>
        <message utf8="true">
            <source>IPTV_Address_Book</source>
            <translation>Rubrica_Indirizzi_IPTV</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>already in use. Choose a different name or edit existing entry.</source>
            <translation>già in uso. Scegliere un nome diverso o modificare la voce esistente.</translation>
        </message>
        <message utf8="true">
            <source>Entry with these parameters already exists.</source>
            <translation>È già presente una voce con questi parametri.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all entries?</source>
            <translation>Eliminare tutte le voci?</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>OP origine</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>OP di destinazione</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoAddressBookImportOverwriteDialog</name>
        <message utf8="true">
            <source>Adding entry</source>
            <translation>Aggiunta di una voce</translation>
        </message>
        <message utf8="true">
            <source>SRC</source>
            <translation>SRC</translation>
        </message>
        <message utf8="true">
            <source>DST</source>
            <translation>DST</translation>
        </message>
        <message utf8="true">
            <source>PID</source>
            <translation>PID</translation>
        </message>
        <message utf8="true">
            <source>An entry with the same name already exists with</source>
            <translation>È già presente una voce con lo stesso nome con.</translation>
        </message>
        <message utf8="true">
            <source>What would you like to do?</source>
            <translation>Cosa si desidera fare?</translation>
        </message>
    </context>
    <context>
        <name>ui::CCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Data: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Ora: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Ora/e</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Giorno</translation>
        </message>
    </context>
    <context>
        <name>ui::CEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>To view more Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare più dati Evento, selezionare Vista -> Finestre -> Singolo nel menu.</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Acceso</translation>
        </message>
    </context>
    <context>
        <name>ui::CK1K2TableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>K1</source>
            <translation>K1</translation>
        </message>
        <message utf8="true">
            <source>Code</source>
            <translation>Codice</translation>
        </message>
        <message utf8="true">
            <source>Chan</source>
            <translation>Can.</translation>
        </message>
        <message utf8="true">
            <source>K2</source>
            <translation>K2</translation>
        </message>
        <message utf8="true">
            <source>Brdg</source>
            <translation>Brdg</translation>
        </message>
        <message utf8="true">
            <source>MSP</source>
            <translation>MSP</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Dest</source>
            <translation>Dest</translation>
        </message>
        <message utf8="true">
            <source>Src</source>
            <translation>Src</translation>
        </message>
        <message utf8="true">
            <source>Path</source>
            <translation>Percorso</translation>
        </message>
        <message utf8="true">
            <source>To view more K1/K2 byte data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare più dati byte K1/K2 , selezionare Vista -> Finestre esito -> Singolo nel menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Frame</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Hex</source>
            <translation>Hex</translation>
        </message>
        <message utf8="true">
            <source>ASCII</source>
            <translation>ASCII</translation>
        </message>
        <message utf8="true">
            <source>Binary</source>
            <translation>Binario</translation>
        </message>
    </context>
    <context>
        <name>ui::COwdEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>To view more One Way Delay Event data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare più Dati evento One Way Delay, selezionare Vista -> Finestre -> Singolo nel menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDEventName</name>
        <message utf8="true">
            <source>Source Loss</source>
            <translation>Perdita origine</translation>
        </message>
        <message utf8="true">
            <source>AIS</source>
            <translation>AIS</translation>
        </message>
        <message utf8="true">
            <source>RAI</source>
            <translation>RAI</translation>
        </message>
        <message utf8="true">
            <source>RDI</source>
            <translation>RDI</translation>
        </message>
        <message utf8="true">
            <source>MF-LOF</source>
            <translation>MF-LOF</translation>
        </message>
        <message utf8="true">
            <source>MF-AIS</source>
            <translation>MF-AIS</translation>
        </message>
        <message utf8="true">
            <source>MF-RDI</source>
            <translation>MF-RDI</translation>
        </message>
        <message utf8="true">
            <source>SEF</source>
            <translation>SEF</translation>
        </message>
        <message utf8="true">
            <source>OOF</source>
            <translation>OOF</translation>
        </message>
        <message utf8="true">
            <source>B1 Err</source>
            <translation>Err. di B1</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>REI-L Err</source>
            <translation>Err. di REI-L</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-REI Err</source>
            <translation>Err. di MS-REI</translation>
        </message>
        <message utf8="true">
            <source>B2 Err</source>
            <translation>Err. di B2</translation>
        </message>
        <message utf8="true">
            <source>LOP-P</source>
            <translation>LOP-P</translation>
        </message>
        <message utf8="true">
            <source>AIS-P</source>
            <translation>AIS-P</translation>
        </message>
        <message utf8="true">
            <source>RDI-P</source>
            <translation>RDI-P</translation>
        </message>
        <message utf8="true">
            <source>REI-P Err</source>
            <translation>Err. di REI-P</translation>
        </message>
        <message utf8="true">
            <source>B2 Error</source>
            <translation>Errore B2</translation>
        </message>
        <message utf8="true">
            <source>AU-LOP</source>
            <translation>AU-LOP</translation>
        </message>
        <message utf8="true">
            <source>AU-AIS</source>
            <translation>AU-AIS</translation>
        </message>
        <message utf8="true">
            <source>HP-RDI</source>
            <translation>HP-RDI</translation>
        </message>
        <message utf8="true">
            <source>HP-REI Err</source>
            <translation>Err. di HP-REI</translation>
        </message>
        <message utf8="true">
            <source>B3 Err</source>
            <translation>Err. di B3</translation>
        </message>
        <message utf8="true">
            <source>LOP-V</source>
            <translation>LOP-V</translation>
        </message>
        <message utf8="true">
            <source>LOM-V</source>
            <translation>LOM-V</translation>
        </message>
        <message utf8="true">
            <source>AIS-V</source>
            <translation>AIS-V</translation>
        </message>
        <message utf8="true">
            <source>RDI-V</source>
            <translation>RDI-V</translation>
        </message>
        <message utf8="true">
            <source>REI-V Err</source>
            <translation>Err. di REI-V</translation>
        </message>
        <message utf8="true">
            <source>BIP-V Err</source>
            <translation>Err. di BIP-V</translation>
        </message>
        <message utf8="true">
            <source>B3 Error</source>
            <translation>Errore B3</translation>
        </message>
        <message utf8="true">
            <source>TU-LOP</source>
            <translation>TU-LOP</translation>
        </message>
        <message utf8="true">
            <source>TU-LOM</source>
            <translation>TU-LOM</translation>
        </message>
        <message utf8="true">
            <source>TU-AIS</source>
            <translation>TU-AIS</translation>
        </message>
        <message utf8="true">
            <source>LP-RDI</source>
            <translation>LP-RDI</translation>
        </message>
        <message utf8="true">
            <source>LP-REI Err</source>
            <translation>Err. di LP-REI</translation>
        </message>
        <message utf8="true">
            <source>LP-BIP Err</source>
            <translation>Err. di LP-BIP</translation>
        </message>
        <message utf8="true">
            <source>OTU1 LOM</source>
            <translation>OTU1 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-IAE</source>
            <translation>OTU1 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU1 SM-BIAE</source>
            <translation>OTU1 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU1 AIS</source>
            <translation>ODU1 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 LCK</source>
            <translation>ODU1 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU1 OCI</source>
            <translation>ODU1 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BDI</source>
            <translation>ODU1 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU1 OOM</source>
            <translation>OTU1 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU1 MFAS</source>
            <translation>OTU1 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BIP</source>
            <translation>ODU1 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU1 PM-BEI</source>
            <translation>ODU1 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 LOM</source>
            <translation>OTU2 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-IAE</source>
            <translation>OTU2 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU2 SM-BIAE</source>
            <translation>OTU2 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU2 AIS</source>
            <translation>ODU2 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 LCK</source>
            <translation>ODU2 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU2 OCI</source>
            <translation>ODU2 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BDI</source>
            <translation>ODU2 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU2 OOM</source>
            <translation>OTU2 OOM</translation>
        </message>
        <message utf8="true">
            <source>OTU2 MFAS</source>
            <translation>OTU2 MFAS</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BIP</source>
            <translation>ODU2 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU2 PM-BEI</source>
            <translation>ODU2 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 LOM</source>
            <translation>OTU3 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-IAE</source>
            <translation>OTU3 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU3 SM-BIAE</source>
            <translation>OTU3 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU3 AIS</source>
            <translation>ODU3 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU3 LCK</source>
            <translation>ODU3 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU3 OCI</source>
            <translation>ODU3 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BDI</source>
            <translation>ODU3 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU3 OOM</source>
            <translation>OTU3 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BIP</source>
            <translation>ODU3 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU3 PM-BEI</source>
            <translation>ODU3 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 LOM</source>
            <translation>OTU4 LOM</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-IAE</source>
            <translation>OTU4 SM-IAE</translation>
        </message>
        <message utf8="true">
            <source>OTU4 SM-BIAE</source>
            <translation>OTU4 SM-BIAE</translation>
        </message>
        <message utf8="true">
            <source>ODU4 AIS</source>
            <translation>ODU4 AIS</translation>
        </message>
        <message utf8="true">
            <source>ODU4 LCK</source>
            <translation>ODU4 LCK</translation>
        </message>
        <message utf8="true">
            <source>ODU4 OCI</source>
            <translation>ODU4 OCI</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BDI</source>
            <translation>ODU4 PM-BDI</translation>
        </message>
        <message utf8="true">
            <source>OTU4 OOM</source>
            <translation>OTU4 OOM</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BIP</source>
            <translation>ODU4 PM-BIP</translation>
        </message>
        <message utf8="true">
            <source>ODU4 PM-BEI</source>
            <translation>ODU4 PM-BEI</translation>
        </message>
        <message utf8="true">
            <source>STL AIS</source>
            <translation>STL AIS</translation>
        </message>
        <message utf8="true">
            <source>STL FAS Err</source>
            <translation>Err. FAS STL</translation>
        </message>
        <message utf8="true">
            <source>STL OOF</source>
            <translation>STL OOF</translation>
        </message>
        <message utf8="true">
            <source>STL SEF</source>
            <translation>STL SEF</translation>
        </message>
        <message utf8="true">
            <source>STL LOF</source>
            <translation>STL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL LLM</source>
            <translation>OTL LLM</translation>
        </message>
        <message utf8="true">
            <source>OTL FAS</source>
            <translation>OTL FAS</translation>
        </message>
        <message utf8="true">
            <source>OTL LOF</source>
            <translation>OTL LOF</translation>
        </message>
        <message utf8="true">
            <source>OTL MFAS</source>
            <translation>OTL MFAS</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Err</source>
            <translation>Err. di Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>LOF</source>
            <translation>LOF</translation>
        </message>
        <message utf8="true">
            <source>CV</source>
            <translation>CV</translation>
        </message>
        <message utf8="true">
            <source>R-LOS</source>
            <translation>R-LOS</translation>
        </message>
        <message utf8="true">
            <source>R-LOF</source>
            <translation>R-LOF</translation>
        </message>
        <message utf8="true">
            <source>SDI</source>
            <translation>SDI</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss</source>
            <translation>Perdita segnale</translation>
        </message>
        <message utf8="true">
            <source>Frm Syn Loss</source>
            <translation>Frm Syn Loss</translation>
        </message>
        <message utf8="true">
            <source>Frm Wd Err</source>
            <translation>Err Frm Wd</translation>
        </message>
        <message utf8="true">
            <source>LOS</source>
            <translation>LOS</translation>
        </message>
        <message utf8="true">
            <source>FAS Error</source>
            <translation>Errore FAS</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDStatTableWidget</name>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Start Time</source>
            <translation>Ora di inizio</translation>
        </message>
        <message utf8="true">
            <source>End Time</source>
            <translation>Orario finale</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Longest</source>
            <translation>Più lungo</translation>
        </message>
        <message utf8="true">
            <source>Shortest</source>
            <translation>Più breve</translation>
        </message>
        <message utf8="true">
            <source>Last</source>
            <translation>Ultimo</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Medio</translation>
        </message>
        <message utf8="true">
            <source>Disruptions</source>
            <translation>Interruzioni</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare ulteriori dati di Interruzioni servizi, selezionare Visualizza -> Finestre -> Singolo dal menu.</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Totale</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Overflow</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableBasicWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>N° SD</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>To view more Service Disruption data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare ulteriori dati di Interruzioni servizi, selezionare Visualizza -> Finestre -> Singolo dal menu.</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Non riuscito</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>Riuscito</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Overflow</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>AVVIA</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>STOP</translation>
        </message>
    </context>
    <context>
        <name>ui::CSDTableWidget</name>
        <message utf8="true">
            <source>SD No.</source>
            <translation>N° SD</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur. (ms)</source>
            <translation>Dur. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Overflow</source>
            <translation>Overflow</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWidget</name>
        <message utf8="true">
            <source>To view more Call Results data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare più dati Risultato chiamata, selezionare Vista -> Finestre -> Singolo nel menu.</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>dtmf </source>
            <translation>dtmf </translation>
        </message>
        <message utf8="true">
            <source>mf </source>
            <translation>mf </translation>
        </message>
        <message utf8="true">
            <source>dp </source>
            <translation>dp </translation>
        </message>
        <message utf8="true">
            <source>dial tone</source>
            <translation>segnale di linea</translation>
        </message>
        <message utf8="true">
            <source>  TRUE</source>
            <translation>  TRUE</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Tipo</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>Durata</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Non valido</translation>
        </message>
    </context>
    <context>
        <name>ui::CSignalingTableWindow</name>
        <message utf8="true">
            <source>DS0</source>
            <translation>DS0</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgCombinedHistogramWidget</name>
        <message utf8="true">
            <source>Date: </source>
            <translation>Data: </translation>
        </message>
        <message utf8="true">
            <source>Time: </source>
            <translation>Ora: </translation>
        </message>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Ora/e</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Giorno</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>N KLM</source>
            <translation>N KLM</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>PMT PID</source>
            <translation>PMT PID</translation>
        </message>
        <message utf8="true">
            <source>Prog Name</source>
            <translation>Nome prog.</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsEventTableWidget</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>Evento</translation>
        </message>
        <message utf8="true">
            <source>Strm IP:Port</source>
            <translation>OP flus:Porta</translation>
        </message>
        <message utf8="true">
            <source>Strm Name</source>
            <translation>Nome flusso</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Dur.(ms)/Val.</source>
            <translation>Dur.(ms)/Val.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Export Saved Custom Result Category</source>
            <translation>Esporta Categoria risultati personalizzata salvata</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Categoria risultati personalizzata salvata</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportFileDialogBase</name>
        <message utf8="true">
            <source>Export</source>
            <translation>Esporta</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Impossibile trovare il flash drive USB.&#xA;Si prega di inserire un flash drive , o rimuovere e reinserire di nuovo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportPtpDataFileDialog</name>
        <message utf8="true">
            <source>Export PTP Data to USB</source>
            <translation>Esporta dati PTP su USB</translation>
        </message>
        <message utf8="true">
            <source>PTP Data Files (*.ptp)</source>
            <translation>File Dati PTP (*.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportReportFileDialog</name>
        <message utf8="true">
            <source>Export Report to USB</source>
            <translation>Esporta rapporto su USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Testo (*.Txt)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportScreenshotMgr</name>
        <message utf8="true">
            <source>Export Screenshots to USB</source>
            <translation>Esporta schermate su USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Screenshots (*.png *.jpg *.jpeg)</source>
            <translation>Schermate salvate (*.png *.jpg *.jpeg)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTestFileDialog</name>
        <message utf8="true">
            <source>Zip selected files as:</source>
            <translation>Zip file selezionati come:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Immettere il nome del file: max. 60 caratteri</translation>
        </message>
        <message utf8="true">
            <source>Zip&#xA;&amp;&amp; Export</source>
            <translation>Zip&#xA;&amp;&amp; Esporta</translation>
        </message>
        <message utf8="true">
            <source>Export</source>
            <translation>Esporta</translation>
        </message>
        <message utf8="true">
            <source>Please Enter a Name for the Zip File</source>
            <translation>Inserire un nome per il file Zip</translation>
        </message>
        <message utf8="true">
            <source>Unable to locate USB flash drive.&#xA;Please insert a flash drive, or remove and re-insert again.</source>
            <translation>Impossibile trovare il flash drive USB.&#xA;Si prega di inserire un flash drive , o rimuovere e reinserire di nuovo.</translation>
        </message>
        <message utf8="true">
            <source>Unable to zip the file(s)</source>
            <translation>Impossibile comprimere il/i file</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTieDataFileDialog</name>
        <message utf8="true">
            <source>Export TIE Data to USB</source>
            <translation>Esporta dati TIE su USB</translation>
        </message>
        <message utf8="true">
            <source>Wander TIE Data Files (*.hrd *.chrd)</source>
            <translation>File dati Wander TIE (*.hrd *.chrd)</translation>
        </message>
    </context>
    <context>
        <name>ui::CExportTimingDataFileDialog</name>
        <message utf8="true">
            <source>Export Timing Data to USB</source>
            <translation>Esportare i dati di sincronizzazione su USB</translation>
        </message>
        <message utf8="true">
            <source>All timing data files (*.hrd *.chrd *.ptp)</source>
            <translation>Tutti i file dei dati di sincronizzazione (*.hrd *.chrd *.ptp)</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileFolderWidget</name>
        <message utf8="true">
            <source>File type:</source>
            <translation>Tipo di file:</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileNameDialog</name>
        <message utf8="true">
            <source>Open</source>
            <translation>Aperta</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportCustomCategoryFileDialog</name>
        <message utf8="true">
            <source>Import Saved Custom Result Category from USB</source>
            <translation>Importa categoria personalizzata risultati salvata da USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Result Category</source>
            <translation>Categoria risultati personalizzata salvata</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Custom Category</source>
            <translation>Importa&#xA;Categoria personalizzata</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportLogoFileDialog</name>
        <message utf8="true">
            <source>Import Report Logo from USB</source>
            <translation>Importa Logo rapporto da USB</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>File immagine (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Logo</source>
            <translation>Importa&#xA;Logo</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportQuickCardMgr</name>
        <message utf8="true">
            <source>Import Quick Card from USB</source>
            <translation>Importa Scheda rapida da USB</translation>
        </message>
        <message utf8="true">
            <source>Pdf files (*.pdf)</source>
            <translation>File pdf (*.pdf)</translation>
        </message>
        <message utf8="true">
            <source>Import&#xA;Quick Card</source>
            <translation>Importa&#xA;Scheda rapida</translation>
        </message>
    </context>
    <context>
        <name>ui::CImportTestFileDialog</name>
        <message utf8="true">
            <source>Import&#xA;Test</source>
            <translation>Importa&#xA;Test</translation>
        </message>
        <message utf8="true">
            <source>Unzip&#xA; &amp;&amp; Import</source>
            <translation>Unzip&#xA; &amp;&amp;Importa...</translation>
        </message>
        <message utf8="true">
            <source>Error - Unable to unTAR one of the files.</source>
            <translation>Errore: impossibile decomprimere uno dei file nel TAR.</translation>
        </message>
    </context>
    <context>
        <name>ui::CLegacyBatchFileCopier</name>
        <message utf8="true">
            <source>Insufficient free space on destination device.&#xA;Copy operation cancelled.</source>
            <translation>Spazio libero Insufficiente sul dispositivo di destinazione.&#xA;Operazione di copia annullata.</translation>
        </message>
        <message utf8="true">
            <source>Copying files...</source>
            <translation>Copia dei file in corso...</translation>
        </message>
        <message utf8="true">
            <source>Done. Files copied.</source>
            <translation>Terminato. File copiati.</translation>
        </message>
        <message utf8="true">
            <source>Error: The following items failed to copy: &#xA;</source>
            <translation>Errore: la copia dei seguenti elementi non è riuscita: &#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavePtpDataFileDialog</name>
        <message utf8="true">
            <source>Save PTP Data</source>
            <translation>Salva Dati PTP</translation>
        </message>
        <message utf8="true">
            <source>PTP files (*.ptp)</source>
            <translation>File PTP (*.ptp)</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>Copia</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space. Delete other saved files or export to USB.</source>
            <translation>Spazio su disco insufficiente. Eliminare altri file salvati o esportarli su USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedDeviceStatsWidget</name>
        <message utf8="true">
            <source>%1 of %2 free</source>
            <translation>%1 di %2 liberi</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectedFileStatsWidget</name>
        <message utf8="true">
            <source>No files selected</source>
            <translation>Non sono stati selezionati file</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in selected file</source>
            <translation>%1 totali nel file selezionato</translation>
        </message>
        <message utf8="true">
            <source>Total %1 in %2 selected files</source>
            <translation>%1 totali in %2 file selezionati</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformFileDialog</name>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
        <message utf8="true">
            <source>File Name:</source>
            <translation>Nome file:</translation>
        </message>
        <message utf8="true">
            <source>Enter file name: 60 chars max</source>
            <translation>Immettere il nome del file: max. 60 caratteri</translation>
        </message>
        <message utf8="true">
            <source>Delete all files within this folder?</source>
            <translation>Eliminare tutti i file all'interno di questa cartella?</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(I file di sola lettura non saranno cancellati.)</translation>
        </message>
        <message utf8="true">
            <source>Deleting files...</source>
            <translation>Eliminazione file in corso...</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete&#xA;</source>
            <translation>Sicuro di volere eliminare&#xA;</translation>
        </message>
        <message utf8="true">
            <source> ?</source>
            <translation> ?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete this item?</source>
            <translation>Eliminare questa voce?</translation>
        </message>
    </context>
    <context>
        <name>ui::CRecommendedOpticRatesFormatter</name>
        <message utf8="true">
            <source>Not a recommended optic</source>
            <translation>Fibra Ottica Non Raccomandata</translation>
        </message>
        <message utf8="true">
            <source>SONET/SDH</source>
            <translation>SONET/SDH</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel</source>
            <translation>Fibre Channel</translation>
        </message>
        <message utf8="true">
            <source>CPRI</source>
            <translation>CPRI</translation>
        </message>
        <message utf8="true">
            <source>OBSAI</source>
            <translation>OBSAI</translation>
        </message>
        <message utf8="true">
            <source>OTN</source>
            <translation>OTN</translation>
        </message>
        <message utf8="true">
            <source>10G LAN/WAN</source>
            <translation>10G LAN/WAN</translation>
        </message>
        <message utf8="true">
            <source>STS-1/STM-0</source>
            <translation>STS-1/STM-0</translation>
        </message>
        <message utf8="true">
            <source>OC-3/STM-1</source>
            <translation>OC-3/STM-1</translation>
        </message>
        <message utf8="true">
            <source>OC-12/STM-4</source>
            <translation>OC-12/STM-4</translation>
        </message>
        <message utf8="true">
            <source>OC-48/STM-16</source>
            <translation>OC-48/STM-16</translation>
        </message>
        <message utf8="true">
            <source>OC-192/STM-64</source>
            <translation>OC-192/STM-64</translation>
        </message>
        <message utf8="true">
            <source>OC-768/STM-256</source>
            <translation>OC-768/STM-256</translation>
        </message>
        <message utf8="true">
            <source>10/100/1000</source>
            <translation>10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>100M</source>
            <translation>100M</translation>
        </message>
        <message utf8="true">
            <source>1G</source>
            <translation>1G</translation>
        </message>
        <message utf8="true">
            <source>10G LAN</source>
            <translation>10G LAN</translation>
        </message>
        <message utf8="true">
            <source>10G WAN</source>
            <translation>10G WAN</translation>
        </message>
        <message utf8="true">
            <source>40G</source>
            <translation>40G</translation>
        </message>
        <message utf8="true">
            <source>100G</source>
            <translation>100G</translation>
        </message>
        <message utf8="true">
            <source>2G</source>
            <translation>2G</translation>
        </message>
        <message utf8="true">
            <source>4G</source>
            <translation>4G</translation>
        </message>
        <message utf8="true">
            <source>8G</source>
            <translation>8G</translation>
        </message>
        <message utf8="true">
            <source>10G</source>
            <translation>10G</translation>
        </message>
        <message utf8="true">
            <source>16G</source>
            <translation>16G</translation>
        </message>
        <message utf8="true">
            <source>614.4M</source>
            <translation>614.4M</translation>
        </message>
        <message utf8="true">
            <source>1228.8M</source>
            <translation>1228.8M</translation>
        </message>
        <message utf8="true">
            <source>2457.6M</source>
            <translation>2457.6M</translation>
        </message>
        <message utf8="true">
            <source>3072.0M</source>
            <translation>3072.0M</translation>
        </message>
        <message utf8="true">
            <source>4915.2M</source>
            <translation>4915.2M</translation>
        </message>
        <message utf8="true">
            <source>6144.0M</source>
            <translation>6144.0M</translation>
        </message>
        <message utf8="true">
            <source>9830.4M</source>
            <translation>9830.4M</translation>
        </message>
        <message utf8="true">
            <source>10137.6M</source>
            <translation>10137,6 M</translation>
        </message>
        <message utf8="true">
            <source>768M</source>
            <translation>768M</translation>
        </message>
        <message utf8="true">
            <source>1536M</source>
            <translation>1536M</translation>
        </message>
        <message utf8="true">
            <source>3072M</source>
            <translation>3072M</translation>
        </message>
        <message utf8="true">
            <source>6144M</source>
            <translation>6144M</translation>
        </message>
        <message utf8="true">
            <source>OTU0 1.2G</source>
            <translation>OTU0 1.2G</translation>
        </message>
        <message utf8="true">
            <source>OTU1 2.7G</source>
            <translation>OTU1 2.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU2 10.7G</source>
            <translation>OTU2 10.7G</translation>
        </message>
        <message utf8="true">
            <source>OTU1e 11.05G</source>
            <translation>OTU1e 11.05G</translation>
        </message>
        <message utf8="true">
            <source>OTU2e 11.1G</source>
            <translation>OTU2e 11.1G</translation>
        </message>
        <message utf8="true">
            <source>OTU3 43.02G</source>
            <translation>OTU3 43.02G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e1 44.57G</source>
            <translation>OTU3e1 44.57G</translation>
        </message>
        <message utf8="true">
            <source>OTU3e2 44.58G</source>
            <translation>OTU3e2 44.58G</translation>
        </message>
        <message utf8="true">
            <source>OTU4 111.8G</source>
            <translation>OTU4 111.8G</translation>
        </message>
    </context>
    <context>
        <name>ui::CArrayComponentTableWidget</name>
        <message utf8="true">
            <source>&lt;b>N/A&lt;/b></source>
            <translation>&lt;b>N/D&lt;/b></translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTableWidget</name>
        <message utf8="true">
            <source>Displays the results of the signal structure discovery and scan.</source>
            <translation>Visualizza i risultati della scoperta della struttura del segnale e della scansione.</translation>
        </message>
        <message utf8="true">
            <source>Sort by:</source>
            <translation>Ordina per:</translation>
        </message>
        <message utf8="true">
            <source>Sort</source>
            <translation>Ordina</translation>
        </message>
        <message utf8="true">
            <source>Re-sort</source>
            <translation>Riordina</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryTestMenuButton</name>
        <message utf8="true">
            <source>Presents a selection of available tests that may be utilized to analyze the selected channel</source>
            <translation>Presenta una selezione di test disponibili che possono essere utilizzati per analizzare il canale selezionato</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Avvia test</translation>
        </message>
        <message utf8="true">
            <source>Please wait..configuring selected channel...</source>
            <translation>Attendere: configurazione di canale selezionato...</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertSplashWidget</name>
        <message utf8="true">
            <source>Show details</source>
            <translation>Mostra dettagli</translation>
        </message>
        <message utf8="true">
            <source>Hide details</source>
            <translation>Nascondi dettagli</translation>
        </message>
    </context>
    <context>
        <name>ui::CCalendarNavigationBar</name>
        <message utf8="true">
            <source>Range: %1 to %2</source>
            <translation>Intervallo: %1 a %2</translation>
        </message>
    </context>
    <context>
        <name>ui::CComponentLabelWidget</name>
        <message utf8="true">
            <source>Unavail</source>
            <translation>Indisp.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCompositeLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>ui::CDocumentViewerBase</name>
        <message utf8="true">
            <source>Find</source>
            <translation>Trova</translation>
        </message>
        <message utf8="true">
            <source>Original</source>
            <translation>Originale</translation>
        </message>
        <message utf8="true">
            <source>Fit Width</source>
            <translation>Adatta larghezza</translation>
        </message>
        <message utf8="true">
            <source>Fit Height</source>
            <translation>Adatta altezza</translation>
        </message>
        <message utf8="true">
            <source>50%</source>
            <translation>50%</translation>
        </message>
        <message utf8="true">
            <source>75%</source>
            <translation>75%</translation>
        </message>
        <message utf8="true">
            <source>150%</source>
            <translation>150%</translation>
        </message>
        <message utf8="true">
            <source>200%</source>
            <translation>200%</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestSelectionDialog</name>
        <message utf8="true">
            <source>Dual Test View Selection</source>
            <translation>Selezione vista test duale</translation>
        </message>
    </context>
    <context>
        <name>ui::CFormattedComponentLabelWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericComponentTableCell</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceLinkWidget</name>
        <message utf8="true">
            <source>More...</source>
            <translation>Maggiori informazioni...</translation>
        </message>
    </context>
    <context>
        <name>ui::CInferenceResultWidget</name>
        <message utf8="true">
            <source>(Continued)</source>
            <translation>(seguito)</translation>
        </message>
    </context>
    <context>
        <name>ui::CKeypad</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Ins</source>
            <translation>Ins</translation>
        </message>
        <message utf8="true">
            <source>Ctrl</source>
            <translation>Ctrl</translation>
        </message>
        <message utf8="true">
            <source>Esc</source>
            <translation>Esc</translation>
        </message>
        <message utf8="true">
            <source>F1</source>
            <translation>F1</translation>
        </message>
        <message utf8="true">
            <source>F2</source>
            <translation>F2</translation>
        </message>
        <message utf8="true">
            <source>F3</source>
            <translation>F3</translation>
        </message>
        <message utf8="true">
            <source>F4</source>
            <translation>F4</translation>
        </message>
        <message utf8="true">
            <source>F5</source>
            <translation>F5</translation>
        </message>
        <message utf8="true">
            <source>F6</source>
            <translation>F6</translation>
        </message>
        <message utf8="true">
            <source>F7</source>
            <translation>F7</translation>
        </message>
        <message utf8="true">
            <source>F8</source>
            <translation>F8</translation>
        </message>
        <message utf8="true">
            <source>F9</source>
            <translation>F9</translation>
        </message>
        <message utf8="true">
            <source>F10</source>
            <translation>F10</translation>
        </message>
        <message utf8="true">
            <source>F11</source>
            <translation>F11</translation>
        </message>
        <message utf8="true">
            <source>F12</source>
            <translation>F12</translation>
        </message>
        <message utf8="true">
            <source>&amp;&amp;123</source>
            <translation>&amp;&amp;123</translation>
        </message>
        <message utf8="true">
            <source>abc</source>
            <translation>abc</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Avviso</translation>
        </message>
    </context>
    <context>
        <name>ui::COverheadCaptureViewWidget</name>
        <message utf8="true">
            <source>Log buffer full</source>
            <translation>Registro buffer pieno</translation>
        </message>
        <message utf8="true">
            <source>Capture stopped</source>
            <translation>Acquisizione fermata</translation>
        </message>
    </context>
    <context>
        <name>ui::CPairEditDialog</name>
        <message utf8="true">
            <source>Edit Row</source>
            <translation>Modifica riga</translation>
        </message>
    </context>
    <context>
        <name>ui::CPohButtonGroup</name>
        <message utf8="true">
            <source>Select Byte:</source>
            <translation>Seleziona byte:</translation>
        </message>
        <message utf8="true">
            <source>HP</source>
            <translation>HP</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
    </context>
    <context>
        <name>ui::CScreenGrabber</name>
        <message utf8="true">
            <source>Unable to capture screenshot</source>
            <translation>Impossibile acquisire schermata</translation>
        </message>
        <message utf8="true">
            <source>insufficient disk space</source>
            <translation>spazio su disco insufficiente</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured: </source>
            <translation>Schermata acquisita: </translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsDialog</name>
        <message utf8="true">
            <source>About Stream</source>
            <translation>Info su flusso</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
    </context>
    <context>
        <name>ui::CToeShowDetailsDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableRowDetailsDialogModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDtmfDialog</name>
        <message utf8="true">
            <source>DP Dial</source>
            <translation>DP Dial</translation>
        </message>
        <message utf8="true">
            <source>MF Dial</source>
            <translation>MF Dial</translation>
        </message>
        <message utf8="true">
            <source>DTMF Dial</source>
            <translation>DTMF Dial</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
    </context>
    <context>
        <name>ui::CSmallProgressDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK1Interpreter</name>
        <message utf8="true">
            <source>NR</source>
            <translation>NR</translation>
        </message>
        <message utf8="true">
            <source>DnR</source>
            <translation>DnR</translation>
        </message>
        <message utf8="true">
            <source>RR</source>
            <translation>RR</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>EXER</source>
            <translation>EXER</translation>
        </message>
        <message utf8="true">
            <source>WTR</source>
            <translation>WTR</translation>
        </message>
        <message utf8="true">
            <source>MS</source>
            <translation>MS</translation>
        </message>
        <message utf8="true">
            <source>SD-L</source>
            <translation>SD-L</translation>
        </message>
        <message utf8="true">
            <source>SD-H</source>
            <translation>SD-H</translation>
        </message>
        <message utf8="true">
            <source>SF-L</source>
            <translation>SF-L</translation>
        </message>
        <message utf8="true">
            <source>SF-H</source>
            <translation>SF-H</translation>
        </message>
        <message utf8="true">
            <source>FS</source>
            <translation>FS</translation>
        </message>
        <message utf8="true">
            <source>LP</source>
            <translation>LP</translation>
        </message>
        <message utf8="true">
            <source>RR-R</source>
            <translation>RR-R</translation>
        </message>
        <message utf8="true">
            <source>RR-S</source>
            <translation>RR-S</translation>
        </message>
        <message utf8="true">
            <source>EXER-R</source>
            <translation>EXER-R</translation>
        </message>
        <message utf8="true">
            <source>EXER-S</source>
            <translation>EXER-S</translation>
        </message>
        <message utf8="true">
            <source>MS-R</source>
            <translation>MS-R</translation>
        </message>
        <message utf8="true">
            <source>MS-S</source>
            <translation>MS-S</translation>
        </message>
        <message utf8="true">
            <source>SD-R</source>
            <translation>SD-R</translation>
        </message>
        <message utf8="true">
            <source>SD-S</source>
            <translation>SD-S</translation>
        </message>
        <message utf8="true">
            <source>SD-P</source>
            <translation>SD-P</translation>
        </message>
        <message utf8="true">
            <source>SF-R</source>
            <translation>SF-R</translation>
        </message>
        <message utf8="true">
            <source>SF-S</source>
            <translation>SF-S</translation>
        </message>
        <message utf8="true">
            <source>FS-R</source>
            <translation>FS-R</translation>
        </message>
        <message utf8="true">
            <source>FS-S</source>
            <translation>FS-S</translation>
        </message>
        <message utf8="true">
            <source>LP-S</source>
            <translation>LP-S</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetSdhK2Interpreter</name>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Riservato</translation>
        </message>
        <message utf8="true">
            <source>Unidir</source>
            <translation>Unidir</translation>
        </message>
        <message utf8="true">
            <source>Bidir</source>
            <translation>Bidir</translation>
        </message>
        <message utf8="true">
            <source>RDI-L</source>
            <translation>RDI-L</translation>
        </message>
        <message utf8="true">
            <source>AIS-L</source>
            <translation>AIS-L</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>Inattivo</translation>
        </message>
        <message utf8="true">
            <source>Br</source>
            <translation>Br</translation>
        </message>
        <message utf8="true">
            <source>Br+Sw</source>
            <translation>Br+Sw</translation>
        </message>
        <message utf8="true">
            <source>Extra Traffic</source>
            <translation>Traffico extra</translation>
        </message>
        <message utf8="true">
            <source>MS-RDI</source>
            <translation>MS-RDI</translation>
        </message>
        <message utf8="true">
            <source>MS-AIS</source>
            <translation>MS-AIS</translation>
        </message>
    </context>
    <context>
        <name>ui::CStreamsInfoWidget</name>
        <message utf8="true">
            <source>Total</source>
            <translation>Totale</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestMenuButton</name>
        <message utf8="true">
            <source>None Available</source>
            <translation>Nessuno disponibile</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextDocumentViewer</name>
        <message utf8="true">
            <source>Cannot navigate to external links</source>
            <translation>Impossibile passare a link esterni</translation>
        </message>
        <message utf8="true">
            <source>Not Found</source>
            <translation>Non trovato</translation>
        </message>
        <message utf8="true">
            <source>Reached bottom of page, continued from top</source>
            <translation>Fine pagina raggiunto, continua da inizio pagina</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolChooserDialog</name>
        <message utf8="true">
            <source>Select Tool</source>
            <translation>Seleziona utensile</translation>
        </message>
    </context>
    <context>
        <name>ui::CToolkitItemScriptAction</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Disattivazione dei rapporti automatici prima di avviare lo script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Attivazione dei rapporti automatici precedentemente disabilitati.</translation>
        </message>
    </context>
    <context>
        <name>ui::CUniformDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Crea</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Predefinito</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Elimina tutto</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>Send</source>
            <translation>Invia</translation>
        </message>
        <message utf8="true">
            <source>Retry</source>
            <translation>Riprova</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Visualizza</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgAnalysisWidget</name>
        <message utf8="true">
            <source>AU-3</source>
            <translation>AU-3</translation>
        </message>
        <message utf8="true">
            <source>VCAT</source>
            <translation>VCAT</translation>
        </message>
        <message utf8="true">
            <source>VC-12</source>
            <translation>VC-12</translation>
        </message>
        <message utf8="true">
            <source>STM-N</source>
            <translation>STM-N</translation>
        </message>
        <message utf8="true">
            <source>VC-3</source>
            <translation>VC-3</translation>
        </message>
        <message utf8="true">
            <source>STS-3c</source>
            <translation>STS-3c</translation>
        </message>
        <message utf8="true">
            <source>STS-1</source>
            <translation>STS-1</translation>
        </message>
        <message utf8="true">
            <source>VT-1.5</source>
            <translation>VT-1.5</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Sink)</source>
            <translation>LCAS (Sink)</translation>
        </message>
        <message utf8="true">
            <source>LCAS (Source)</source>
            <translation>LCAS (Origine)</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModelRow</name>
        <message utf8="true">
            <source>Container</source>
            <translation>Contenitore</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Canale</translation>
        </message>
        <message utf8="true">
            <source>Signal Label</source>
            <translation>Etichetta segnale</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Trace</source>
            <translation>Trace</translation>
        </message>
        <message utf8="true">
            <source>Trace Format</source>
            <translation>Formato Traccia</translation>
        </message>
        <message utf8="true">
            <source>Unknown</source>
            <translation>Sconosciuto</translation>
        </message>
        <message utf8="true">
            <source>This represents only the current level, and does not take into account any lower or higher order channels. Only the currently selected channel will receive live updates.</source>
            <translation>Questo rappresenta solo il livello corrente , e non tiene conto di eventuali canali di ordine inferiore o superiore. Solo il canale selezionato riceverà aggiornamenti in tempo reale.</translation>
        </message>
        <message utf8="true">
            <source>The status of the channel represented by an icon.</source>
            <translation>Lo stato del canale rappresentato da un'icona.</translation>
        </message>
        <message utf8="true">
            <source>No monitored alarms present</source>
            <translation>Non è presente nessun allarme monitorato</translation>
        </message>
        <message utf8="true">
            <source>Alarms present</source>
            <translation>Allarmi presenti</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ No monitored alarms present</source>
            <translation>Monitoraggio con nessun allarme monitorato presente</translation>
        </message>
        <message utf8="true">
            <source>Monitoring w/ Alarms present</source>
            <translation>Monitoraggio con allarmi presenti</translation>
        </message>
        <message utf8="true">
            <source>The name of the channel's container. A 'c' suffix indicates a concatenated channel.</source>
            <translation>Il nome del contenitore del canale. Il suffisso 'c ' indica un canale concatenato.</translation>
        </message>
        <message utf8="true">
            <source>The N KLM number of the channel as specified by RFC 4606</source>
            <translation>Il numero N KLM del canale , come specificato da RFC 4606</translation>
        </message>
        <message utf8="true">
            <source>The channel's signal label</source>
            <translation>L'etichetta di segnale del canale</translation>
        </message>
        <message utf8="true">
            <source>The last known status of the channel.</source>
            <translation>L'ultimo stato noto del canale.</translation>
        </message>
        <message utf8="true">
            <source>The channel is invalid.</source>
            <translation>Canale non valido.</translation>
        </message>
        <message utf8="true">
            <source>RDI Present</source>
            <translation>RDI Presente</translation>
        </message>
        <message utf8="true">
            <source>AIS Present</source>
            <translation>AIS Presente</translation>
        </message>
        <message utf8="true">
            <source>LOP Present</source>
            <translation>LOP Presente</translation>
        </message>
        <message utf8="true">
            <source>Monitoring</source>
            <translation>Monitoraggio</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sì</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Status updated at: </source>
            <translation>Stato aggiornato a: </translation>
        </message>
        <message utf8="true">
            <source>never</source>
            <translation>mai</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace info</source>
            <translation>Informazioni sulla traccia del canale</translation>
        </message>
        <message utf8="true">
            <source>The channel's trace format info</source>
            <translation>Informazioni sul formato traccia del canale</translation>
        </message>
        <message utf8="true">
            <source>Unsupported</source>
            <translation>Non supportato</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Analisi</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Alarm</source>
            <translation>Allarme</translation>
        </message>
        <message utf8="true">
            <source>Invalid</source>
            <translation>Non valido</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutodiscoveryModel</name>
        <message utf8="true">
            <source>Unformatted</source>
            <translation>Non formattato</translation>
        </message>
        <message utf8="true">
            <source>Single Byte</source>
            <translation>Byte singolo</translation>
        </message>
        <message utf8="true">
            <source>CR/LF Terminated</source>
            <translation>Terminante con CR/LF</translation>
        </message>
        <message utf8="true">
            <source>ITU-T G.707</source>
            <translation>ITU-T G.707</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>ui::CBluetoothDevicesModel</name>
        <message utf8="true">
            <source>Paired devices</source>
            <translation>Dispositivi accoppiati</translation>
        </message>
        <message utf8="true">
            <source>Discovered devices</source>
            <translation>Dispositivi individuati</translation>
        </message>
    </context>
    <context>
        <name>ui::CCertificateBundlesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>CA Cert</source>
            <translation>CA Cert</translation>
        </message>
        <message utf8="true">
            <source>Client Cert</source>
            <translation>Cert. client</translation>
        </message>
        <message utf8="true">
            <source>Client Key</source>
            <translation>Chiave client</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
    </context>
    <context>
        <name>ui::CFlashDevicesModel</name>
        <message utf8="true">
            <source>Free space</source>
            <translation>Spazio libero</translation>
        </message>
        <message utf8="true">
            <source>Total capacity</source>
            <translation>Capienza totale</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Fornitore</translation>
        </message>
        <message utf8="true">
            <source>Label</source>
            <translation>Etichetta</translation>
        </message>
    </context>
    <context>
        <name>ui::CRpmUpgradesModel</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Upgrade Version</source>
            <translation>Versione aggiornamento</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Installed Version</source>
            <translation>Versione installata</translation>
        </message>
    </context>
    <context>
        <name>ui::CSavedCustomCategoriesModel</name>
        <message utf8="true">
            <source>Categories</source>
            <translation>Categorie</translation>
        </message>
        <message utf8="true">
            <source>Lists the names given to the custom categories. Clicking a name will enable/disable that custom category.</source>
            <translation>Elenca i nomi dati alle categorie personalizzate. Facendo clic su un nome si abilita/disabilita la categoria personalizzata.</translation>
        </message>
        <message utf8="true">
            <source>Allows for configuration of a custom category when clicked.</source>
            <translation>Consente la configurazione di una categoria personalizzata quando si fa clic.</translation>
        </message>
        <message utf8="true">
            <source>Allows for deletion of a custom category by toggling the desired categories to delete.</source>
            <translation>Consente per la cancellazione di una categoria personalizzata commutando i contrassegni delle categorie da eliminare.</translation>
        </message>
        <message utf8="true">
            <source>The name given to the custom category. Clicking the name will enable/disable the custom category.</source>
            <translation>Il nomi dati alla categoria personalizzata. Facendo clic su un nome si abilita/disabilita la categoria personalizzata.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the configure icon will launch a configuration dialog for the custom category.</source>
            <translation>Facendo clic sull'icona configura si apre una finestra di configurazione per la categoria personalizzata.</translation>
        </message>
        <message utf8="true">
            <source>Clicking on the delete icon will mark or unmark the custom category for deletion.</source>
            <translation>Facendo clic sull'icona elimina si seleziona o deseleziona la categoria personalizzata per l'eliminazione.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateDiscoveryReportDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crea rapporto</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Testo (*.Txt)</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Crea</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Formato:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Visualizza rapporto dopo la creazione</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Errore: nome file non può essere vuoto.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryConfigFrame</name>
        <message utf8="true">
            <source>Modification of the settings will refresh current results.</source>
            <translation>La modifica delle impostazioni aggiornerà i risultati correnti.</translation>
        </message>
    </context>
    <context>
        <name>ui::HostsOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Non in Subnet</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryMessageBar</name>
        <message utf8="true">
            <source>Waiting for Link Active...</source>
            <translation>In attesa di Link Active...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP...</source>
            <translation>In attesa di DHCP...</translation>
        </message>
        <message utf8="true">
            <source>Reconfiguring the Source IP...</source>
            <translation>Riconfigurazione dell'OP origine...</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modalità</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>OP origine</translation>
        </message>
    </context>
    <context>
        <name>ui::PrintersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>System Name</source>
            <translation>Nome sistema</translation>
        </message>
        <message utf8="true">
            <source>Not on Subnet</source>
            <translation>Non su Subnet</translation>
        </message>
    </context>
    <context>
        <name>ui::RoutersOverviewModel</name>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Non in Subnet</translation>
        </message>
    </context>
    <context>
        <name>ui::ServersOverviewModel</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Servizi</translation>
        </message>
        <message utf8="true">
            <source>Not in Subnet</source>
            <translation>Non in Subnet</translation>
        </message>
    </context>
    <context>
        <name>ui::SwitchesOverviewModel</name>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Indirizzo MAC</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Servizi</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiscoveryTablePanelBase</name>
        <message utf8="true">
            <source>DNS Name</source>
            <translation>Nome DNS</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Rilevazione</translation>
        </message>
    </context>
    <context>
        <name>ui::VlanModel</name>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN Priority</source>
            <translation>Priorità VLAN</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivi</translation>
        </message>
    </context>
    <context>
        <name>ui::IpNetworksModel</name>
        <message utf8="true">
            <source>Network IP</source>
            <translation>OP rete</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivi</translation>
        </message>
    </context>
    <context>
        <name>ui::NetbiosModel</name>
        <message utf8="true">
            <source>NetBIOS Name</source>
            <translation>Nome NetBIOS</translation>
        </message>
        <message utf8="true">
            <source>Devices</source>
            <translation>Dispositivi</translation>
        </message>
    </context>
    <context>
        <name>ui::CNetworkDiscoveryView</name>
        <message utf8="true">
            <source>IP Networks</source>
            <translation>Reti OP </translation>
        </message>
        <message utf8="true">
            <source>Domains</source>
            <translation>Domini</translation>
        </message>
        <message utf8="true">
            <source>Servers</source>
            <translation>Server</translation>
        </message>
        <message utf8="true">
            <source>Hosts</source>
            <translation>Host</translation>
        </message>
        <message utf8="true">
            <source>Switches</source>
            <translation>Interruttori</translation>
        </message>
        <message utf8="true">
            <source>VLANs</source>
            <translation>VLANs</translation>
        </message>
        <message utf8="true">
            <source>Routers</source>
            <translation>Router</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Impostazioni</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
        <message utf8="true">
            <source>Infrastructure</source>
            <translation>Infrastrutture</translation>
        </message>
        <message utf8="true">
            <source>Core</source>
            <translation>Nucleo</translation>
        </message>
        <message utf8="true">
            <source>Distribution</source>
            <translation>Distribuzione</translation>
        </message>
        <message utf8="true">
            <source>Access</source>
            <translation>Accesso</translation>
        </message>
        <message utf8="true">
            <source>Discovery</source>
            <translation>Rilevazione</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
        <message utf8="true">
            <source>Discovered IP Networks</source>
            <translation>Reti OP scoperte</translation>
        </message>
        <message utf8="true">
            <source>Discovered NetBIOS Domains</source>
            <translation>Domini NetBIOS scoperti</translation>
        </message>
        <message utf8="true">
            <source>Discovered VLANS</source>
            <translation>VLAN scoperte</translation>
        </message>
        <message utf8="true">
            <source>Discovered Rounters</source>
            <translation>Router scoperti</translation>
        </message>
        <message utf8="true">
            <source>Discovered Switches</source>
            <translation>Switch scoperti</translation>
        </message>
        <message utf8="true">
            <source>Discovered Hosts</source>
            <translation>Host scoperti</translation>
        </message>
        <message utf8="true">
            <source>Discovered Servers</source>
            <translation>Server scoperti</translation>
        </message>
        <message utf8="true">
            <source>Network Discovery Report</source>
            <translation>Report di rilevamento di rete</translation>
        </message>
        <message utf8="true">
            <source>Report could not be created</source>
            <translation>Il report non può essere creato</translation>
        </message>
        <message utf8="true">
            <source>Insufficient disk space</source>
            <translation>Spazio su disco insufficiente</translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 8000 </source>
            <translation>Generato da Viavi 8000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 6000 </source>
            <translation>Generato da Viavi 6000 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi 5800 </source>
            <translation>Generato da Viavi 5800 </translation>
        </message>
        <message utf8="true">
            <source>Generated by Viavi Test Instrument </source>
            <translation>Generato da strumento di prova Viavi</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopDiscoveryPushButton</name>
        <message utf8="true">
            <source>Stop</source>
            <translation>Stop</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Avvia</translation>
        </message>
    </context>
    <context>
        <name>ui::ReportBuilder</name>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source>General</source>
            <translation>Generalità</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Data</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Tempo</translation>
        </message>
    </context>
    <context>
        <name>ui::CDiskSpaceNotifier</name>
        <message utf8="true">
            <source>The disk is full, all graphs have been stopped.&#xA;Please free up space and restart the test.&#xA;&#xA;Alternatively, Graphs may be disabled from Tools->Customize in&#xA;the menu bar.</source>
            <translation>Il disco è pieno , tutti i grafici sono stati fermati.&#xA;Liberare dello spazio e riavviare il test.&#xA;&#xA;In alternativa , i Grafici possono essere disabilitati da Strumenti ->Personalizza&#xA;nella barra dei menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotCurve</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotFixedZoom</name>
        <message utf8="true">
            <source>Tap to center time scale</source>
            <translation>Toccare per centrare la scala temporale</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotPropertyDialog</name>
        <message utf8="true">
            <source>Graph properties</source>
            <translation>Proprietà grafico</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotStrategyChooser</name>
        <message utf8="true">
            <source>Mean</source>
            <translation>medio</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max.</translation>
        </message>
    </context>
    <context>
        <name>ui::CThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Finestra</translation>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Finestra di saturazione</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotExportDialog</name>
        <message utf8="true">
            <source>Save Plot Data</source>
            <translation>Salva dati Plot</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on destination device.</source>
            <translation>Spazio libero insufficiente sul dispositivo di destinazione.</translation>
        </message>
        <message utf8="true">
            <source>You can export directly to USB if a USB flash device is inserted.</source>
            <translation>È possibile esportare direttamente in USB se è inserito un dispositivo flash USB.</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data. This may take a while, please wait...</source>
            <translation>Salvataggio dei dati del grafico. Questo processo può richiedere tempo, si prega di attendere...</translation>
        </message>
        <message utf8="true">
            <source>Saving graph data</source>
            <translation>Salvataggio dei dati del grafico</translation>
        </message>
        <message utf8="true">
            <source>Insufficient free space on device. The exported graph data is incomplete.</source>
            <translation>Spazio libero insufficiente sul dispositivo. I dati grafico esportati sono incompleti.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while exporting the graph data. The data may be incomplete.</source>
            <translation>Si è verificato un errore durante l'esportazione dei dati del grafico. I dati potrebbero essere incompleti.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTimePlotWidget</name>
        <message utf8="true">
            <source>Scale</source>
            <translation>Scala</translation>
        </message>
        <message utf8="true">
            <source>1 Day</source>
            <translation>1 giorno</translation>
        </message>
        <message utf8="true">
            <source>10 Hours</source>
            <translation>10 ore</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 ore</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 minuti</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 minuto</translation>
        </message>
        <message utf8="true">
            <source>10 Seconds</source>
            <translation>10 secondi</translation>
        </message>
        <message utf8="true">
            <source>Plot_Data</source>
            <translation>Plot_Data</translation>
        </message>
        <message utf8="true">
            <source>Tap and drag to zoom</source>
            <translation>Toccare e trascinare per variare lo zoom</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedThroughputBarGraphWidget</name>
        <message utf8="true">
            <source>Saturation</source>
            <translation>Saturazione</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Finestra</translation>
        </message>
        <message utf8="true">
            <source>Conn.</source>
            <translation>Conn.</translation>
        </message>
    </context>
    <context>
        <name>ui::CCCMLogResultModel</name>
        <message utf8="true">
            <source>No.</source>
            <translation>N°</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Tempo</translation>
        </message>
        <message utf8="true">
            <source>Src IP</source>
            <translation>Src OP</translation>
        </message>
        <message utf8="true">
            <source>Dest IP</source>
            <translation>OP dest</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Messaggio</translation>
        </message>
        <message utf8="true">
            <source>Src Port</source>
            <translation>Porta orig</translation>
        </message>
        <message utf8="true">
            <source>Dest Port</source>
            <translation>Porta dest</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomCategoriesSelectionWindow</name>
        <message utf8="true">
            <source>Delete...</source>
            <translation>Elimina...</translation>
        </message>
        <message utf8="true">
            <source>Confirm...</source>
            <translation>Conferma...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>The selected categories will be removed from all tests currently running.</source>
            <translation>Le categorie selezionate saranno rimosse da tutti i test attualmente in esecuzione.</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete the selected items?</source>
            <translation>Eliminare gli elementi selezionati?</translation>
        </message>
        <message utf8="true">
            <source>New...</source>
            <translation>Nuovo ...</translation>
        </message>
        <message utf8="true">
            <source>Opens a dialog for configuring a new custom results category.</source>
            <translation>Apre una finestra di dialogo per la configurazione di una nuova categoria di risultati personalizzata.</translation>
        </message>
        <message utf8="true">
            <source>When pressed this allows you to mark custom categories to delete from the unit. Press the button again when you are done with your selection to delete the files.</source>
            <translation>Se premuto questo permette di contrassegnare categorie personalizzate da eliminare dall'unità. Premere nuovamente il pulsante al termine della selezione per eliminare i file.</translation>
        </message>
        <message utf8="true">
            <source>Press "%1"&#xA;to begin</source>
            <translation>Premere "%1"&#xA;per iniziare</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryDialog</name>
        <message utf8="true">
            <source>Configure Custom Results Category</source>
            <translation>Configura categoria risultati personalizzata</translation>
        </message>
        <message utf8="true">
            <source>Selected results marked by a '*' do not apply to the current test configuration, and will not appear in the results window.</source>
            <translation>I risultati selezionati contrassegnati da un asterisco '*' non si applicano alla configurazione di prova corrente, e non verranno visualizzati nella finestra dei risultati.</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>Category name:</source>
            <translation>Nome categoria:</translation>
        </message>
        <message utf8="true">
            <source>Enter custom category name: %1 chars max</source>
            <translation>Immettere il nome della categoria personalizzata: massimo %1 caratteri</translation>
        </message>
        <message utf8="true">
            <source>File name:</source>
            <translation>Nome file:</translation>
        </message>
        <message utf8="true">
            <source>n/a</source>
            <translation>n/a</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>Save As</source>
            <translation>Salva con nome</translation>
        </message>
        <message utf8="true">
            <source>Save New</source>
            <translation>Salva nuovo</translation>
        </message>
        <message utf8="true">
            <source>The file %1 which contains the&#xA;category "%2"</source>
            <translation>Il file %1 che contiene la&#xA;categoria "%2"</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
        <message utf8="true">
            <source>Selected Results: </source>
            <translation>Risultati selezionati: </translation>
        </message>
        <message utf8="true">
            <source>   (Max Selections </source>
            <translation>   (Selezioni max </translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomResultCategoryWindow</name>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configura...</translation>
        </message>
    </context>
    <context>
        <name>ui::CLedResultCategoryWindow</name>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Riepilogo</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestResultWindow</name>
        <message utf8="true">
            <source>Summary</source>
            <translation>Riepilogo</translation>
        </message>
        <message utf8="true">
            <source>LED</source>
            <translation>LED</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdMarkerEvaluation</name>
        <message utf8="true">
            <source>Max (%1):</source>
            <translation>Max (%1):</translation>
        </message>
        <message utf8="true">
            <source>Value (%1):</source>
            <translation>Valore (%1):</translation>
        </message>
    </context>
    <context>
        <name>ui::CHrdTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnCallHistoryResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Pulsante strumenti guida home...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Pulsante strumenti guida indietro...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Pulsante strumenti guida avanti...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Pulsante strumenti guida fine...</translation>
        </message>
        <message utf8="true">
            <source>No Call History</source>
            <translation>Nessuna cronologia chiamate</translation>
        </message>
    </context>
    <context>
        <name>ui::CIsdnDecodesResultCategoryWindow</name>
        <message utf8="true">
            <source>Help toolButton home...</source>
            <translation>Pulsante strumenti guida home...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton reverse...</source>
            <translation>Pulsante strumenti guida indietro...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton forward...</source>
            <translation>Pulsante strumenti guida avanti...</translation>
        </message>
        <message utf8="true">
            <source>Help toolButton end...</source>
            <translation>Pulsante strumenti guida fine...</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceCurveSelection</name>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Picco-Picco</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos-Picco</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Picco</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Ora/e</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Giorno</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Ora/e</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Giorno</translation>
        </message>
        <message utf8="true">
            <source>Peak-Peak</source>
            <translation>Picco-Picco</translation>
        </message>
        <message utf8="true">
            <source>RMS</source>
            <translation>RMS</translation>
        </message>
        <message utf8="true">
            <source>Pos-Peak</source>
            <translation>Pos-Picco</translation>
        </message>
        <message utf8="true">
            <source>Neg-Peak</source>
            <translation>Neg-Picco</translation>
        </message>
        <message utf8="true">
            <source>UI --></source>
            <translation>UI --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.2 UI</source>
            <translation>0 .. 0.2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 0.4 UI</source>
            <translation>0 .. 0.4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1 UI</source>
            <translation>0 .. 1 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 2 UI</source>
            <translation>0 .. 2 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 4 UI</source>
            <translation>0 .. 4 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 10 UI</source>
            <translation>0 .. 10 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 20 UI</source>
            <translation>0 .. 20 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 40 UI</source>
            <translation>0 .. 40 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 100 UI</source>
            <translation>0 .. 100 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 200 UI</source>
            <translation>0 .. 200 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 400 UI</source>
            <translation>0 .. 400 UI</translation>
        </message>
        <message utf8="true">
            <source>0 .. 1000 UI</source>
            <translation>0 .. 1000 UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterTraceWidget</name>
        <message utf8="true">
            <source>UI</source>
            <translation>UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CLatencyDistriBarGraphWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Latency (ms)</source>
            <translation>Latenza (ms)</translation>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Conteggio</translation>
        </message>
    </context>
    <context>
        <name>ui::CMemberResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RISULTATI&#xA;NON DISPONIBILE</translation>
        </message>
    </context>
    <context>
        <name>ui::CMTJResultTableWidget</name>
        <message utf8="true">
            <source>Status: PASS</source>
            <translation>Stato: RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Status: FAIL</source>
            <translation>Stato: NON RIUSCITO</translation>
        </message>
        <message utf8="true">
            <source>Status: N/A</source>
            <translation>Stato: N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::COamMepDiscoveryTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Expand to view filter options</source>
            <translation>Espandi per visualizzare le opzioni filtro</translation>
        </message>
        <message utf8="true">
            <source># MEPs discovered</source>
            <translation>N. di MEP scoperti</translation>
        </message>
        <message utf8="true">
            <source>Set as Peer</source>
            <translation>Rendi Peer</translation>
        </message>
        <message utf8="true">
            <source>Filter the display</source>
            <translation>Filtra la visualizzazione</translation>
        </message>
        <message utf8="true">
            <source>Filter on</source>
            <translation>Applica filtro</translation>
        </message>
        <message utf8="true">
            <source>Enter filter value: %1 chars max</source>
            <translation>Immettere il valore del filtro: massimo %1 caratteri</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>CCM Type</source>
            <translation>Tipo CCM</translation>
        </message>
        <message utf8="true">
            <source>CCM Rate</source>
            <translation>CCM Rate</translation>
        </message>
        <message utf8="true">
            <source>Peer MEP Id</source>
            <translation>ID Peer MEP</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain Level</source>
            <translation>Livello dominio manutenzione </translation>
        </message>
        <message utf8="true">
            <source>Specify Domain ID</source>
            <translation>Specifica ID dominio</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Domain ID</source>
            <translation>ID dominio manutenzione</translation>
        </message>
        <message utf8="true">
            <source>Maintenance Association ID</source>
            <translation>ID associazione manutenzione</translation>
        </message>
        <message utf8="true">
            <source>Test set configured. Highlighted row has been set as the peer MEP for this test set.&#xA;</source>
            <translation>Insieme di test configurato. La riga evidenziata è stata impostata come peer MEP per questa serie di test.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Setting the test set as the highlighted peer MEP failed.&#xA;</source>
            <translation>Impostazione del set di prova come peer MEP evidenziato non riuscita.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to set %1 to %2.&#xA;</source>
            <translation>Impossibile impostare %1 su %2.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CPidResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CPlotResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RISULTATI&#xA;NON DISPONIBILE</translation>
        </message>
        <message utf8="true">
            <source>GRAPHING&#xA;DISABLED</source>
            <translation>GRAFICI&#xA;DISATTIVATI</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultCategoryWindowBase</name>
        <message utf8="true">
            <source>Toggle this result window to take the full screen.</source>
            <translation>Commuta questa finestra dei risultati di prendere l'intero schermo.</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultTableWidget</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindow</name>
        <message utf8="true">
            <source>NO RESULTS&#xA;AVAILABLE</source>
            <translation>NON SONO DISPONIBILI&#xA;RISULTATI</translation>
        </message>
    </context>
    <context>
        <name>ui::CResultWindowView</name>
        <message utf8="true">
            <source>Custom</source>
            <translation>Personalizzato</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tutto</translation>
        </message>
    </context>
    <context>
        <name>ui::CRfc2544ResultTableWidget</name>
        <message utf8="true">
            <source>%1 Byte Frame Loss Test Results</source>
            <translation>%1 byte Risultati test di perdita frame</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Frame Loss Test Results</source>
            <translation>%1 byte Risultati test di throughput perdita frame upstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Frame Loss Test Results</source>
            <translation>%1 byte Risultati test di perdita frame downstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Buffer Credit Throughput Test Results</source>
            <translation>%1 byte Risultati test di throughput crediti buffer</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Upstream Buffer Credit Throughput Test Results</source>
            <translation>%1 byte Risultati test di throughput crediti buffer upstream</translation>
        </message>
        <message utf8="true">
            <source>%1 Byte Downstream Buffer Credit Throughput Test Results</source>
            <translation>%1 byte Risultati test di throughput crediti buffer downstream</translation>
        </message>
    </context>
    <context>
        <name>ui::CRichTextLogWidget</name>
        <message utf8="true">
            <source>Export Text File...</source>
            <translation>Esporta file di testo...</translation>
        </message>
        <message utf8="true">
            <source>Exported log to</source>
            <translation>Log Esportato in</translation>
        </message>
    </context>
    <context>
        <name>ui::CShowDetailsButton</name>
        <message utf8="true">
            <source>Stream&#xA;Details</source>
            <translation>Dettagli&#xA;Flusso</translation>
        </message>
    </context>
    <context>
        <name>ui::CStandardResultCategoryWindow</name>
        <message utf8="true">
            <source>Collapse all result trees in this window.</source>
            <translation>Comprimi tutte le strutture ad albero dei risultati in questa finestra.</translation>
        </message>
        <message utf8="true">
            <source>Expand all result trees in this window.</source>
            <translation>Espandi tutte le strutture ad albero dei risultati in questa finestra.</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;CATEGORY&#xA;IS EMPTY</source>
            <translation>LA CATEGORIA&#xA;RISULTATI&#xA;È VUOTA</translation>
        </message>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RISULTATI&#xA;NON DISPONIBILE</translation>
        </message>
    </context>
    <context>
        <name>ui::CSummaryResultCategoryWindow</name>
        <message utf8="true">
            <source>RESULTS&#xA;UNAVAILABLE</source>
            <translation>RISULTATI&#xA;NON DISPONIBILE</translation>
        </message>
        <message utf8="true">
            <source>ALL SUMMARY&#xA;RESULTS&#xA;OK</source>
            <translation>RISULTATI&#xA;RIEPILOGO&#xA;TUTTI OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryCustomizeDialog</name>
        <message utf8="true">
            <source>Columns</source>
            <translation>Colonne</translation>
        </message>
        <message utf8="true">
            <source>Show Columns</source>
            <translation>Mostra colonne</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Deselect All</source>
            <translation>Deseleziona tutto</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow</name>
        <message utf8="true">
            <source>Show Only Errored</source>
            <translation>Mostra solo con errore</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonne...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultCategoryWindow_v2</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Rows</source>
            <translation>Mostra&#xA;solo Err&#xA;Righe</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Rows</source>
            <translation>Mostra solo righe con errore</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 kbps)</source>
            <translation>Tassi di traffico(kbps L1)</translation>
        </message>
        <message utf8="true">
            <source>Traffic Rates (L1 Mbps)</source>
            <translation>Tassi di traffico(Mbps L1)</translation>
        </message>
        <message utf8="true">
            <source># Analyzed Streams</source>
            <translation>N° flussi analizzati</translation>
        </message>
        <message utf8="true">
            <source>Traffic grouped by</source>
            <translation>Traffico raggruppato per </translation>
        </message>
        <message utf8="true">
            <source>Total Link</source>
            <translation>Totale Link</translation>
        </message>
        <message utf8="true">
            <source>Displayed Streams 1-128</source>
            <translation>Flussi visualizzati 1-128</translation>
        </message>
        <message utf8="true">
            <source>Additional Streams >128</source>
            <translation>Flussi addizionali >128</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonne...</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CTableResultModel_v2</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestStateLabelWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Arrestato</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
    </context>
    <context>
        <name>ui::CTraceResultWidget</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
    </context>
    <context>
        <name>ui::CTracerouteWidget</name>
        <message utf8="true">
            <source>Hop</source>
            <translation>Hop</translation>
        </message>
        <message utf8="true">
            <source>Delay (ms)</source>
            <translation>Ritardo (ms)</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>Indirizzo OP</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>To view more Traceroute data, use the View->Result Windows->Single menu selection.</source>
            <translation>Per visualizzare più dati Traceroute, selezionare Vista -> Finestre -> Singolo nel menu.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTrafficResultCategoryWindow</name>
        <message utf8="true">
            <source>CH</source>
            <translation>CH</translation>
        </message>
    </context>
    <context>
        <name>ui::CTruespeedTransmitTimeWidget</name>
        <message utf8="true">
            <source>Ideal Transfer Time</source>
            <translation>Tempo di trasferimento ideale</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
        <message utf8="true">
            <source>Actual Transfer Time</source>
            <translation>Tempo effettivo di trasferimento</translation>
        </message>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CVcgResultCategoryWindow</name>
        <message utf8="true">
            <source>Group:</source>
            <translation>Gruppo:</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoProgramsResultCategoryWindow</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source>Stream</source>
            <translation>Flusso</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Programs</source>
            <translation>Programmi</translation>
        </message>
        <message utf8="true">
            <source>Packet Loss</source>
            <translation>Perdita pacchetti</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Packet Jitter</translation>
        </message>
        <message utf8="true">
            <source>MDI DF</source>
            <translation>MDI DF</translation>
        </message>
        <message utf8="true">
            <source>MDI MLR</source>
            <translation>MDI MLR</translation>
        </message>
        <message utf8="true">
            <source>Distance Err</source>
            <translation>Err distanza</translation>
        </message>
        <message utf8="true">
            <source>Period Err</source>
            <translation>Err periodo</translation>
        </message>
        <message utf8="true">
            <source>Sync Byte Err</source>
            <translation>Err. di sinc byte</translation>
        </message>
        <message utf8="true">
            <source>Show Only Errored Programs</source>
            <translation>Mostra solo programmi con errore</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonne...</translation>
        </message>
        <message utf8="true">
            <source>Total Prog. Mbps</source>
            <translation>Mbps prog. totali</translation>
        </message>
        <message utf8="true">
            <source>Show only&#xA;Err Programs</source>
            <translation>Mostra solo&#xA;Programmi con err.</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Programs</source>
            <translation>Mostra solo programmi con errori</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryModel</name>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/P</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoStreamsResultCategoryWindow</name>
        <message utf8="true">
            <source>Show&#xA;only Err&#xA;Streams</source>
            <translation>Mostra&#xA;solo flussi&#xA;con errori</translation>
        </message>
        <message utf8="true">
            <source>Show only Errored Streams</source>
            <translation>Mostra solo flussi con errori</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>Analizza</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nome</translation>
        </message>
        <message utf8="true">
            <source># Streams&#xA;Analyzed</source>
            <translation>N° Flussi&#xA;Analizzato</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;L1 Mbps</source>
            <translation>Totale &#xA;Mbps L1</translation>
        </message>
        <message utf8="true">
            <source>IP Chksum&#xA;Errors</source>
            <translation>Errori&#xA;checksum OP</translation>
        </message>
        <message utf8="true">
            <source>UDP Chksum&#xA;Errors</source>
            <translation>Errori&#xA;checksum UDP</translation>
        </message>
        <message utf8="true">
            <source>Launch&#xA;Analyzer</source>
            <translation>Apri&#xA;Analyzer</translation>
        </message>
        <message utf8="true">
            <source>Columns...</source>
            <translation>Colonne...</translation>
        </message>
        <message utf8="true">
            <source>Please wait..launching Analyzer application...</source>
            <translation>Attendere: apertura applicazione Analyzer...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceYAxisResolutionSelection</name>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceXAxisResolutionSelection</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Ora/e</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Giorno</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTrace</name>
        <message utf8="true">
            <source>Sec</source>
            <translation>Secondi</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min.</translation>
        </message>
        <message utf8="true">
            <source>Hour</source>
            <translation>Ora/e</translation>
        </message>
        <message utf8="true">
            <source>Day</source>
            <translation>Giorno</translation>
        </message>
        <message utf8="true">
            <source>TIE</source>
            <translation>TIE</translation>
        </message>
        <message utf8="true">
            <source>TIE (s) --></source>
            <translation>TIE (s) --></translation>
        </message>
        <message utf8="true">
            <source>Y-Auto</source>
            <translation>Y-Auto</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-9 s</source>
            <translation>+/- 1E-9 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-8 s</source>
            <translation>+/- 1E-8 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-7 s</source>
            <translation>+/- 1E-7 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-6 s</source>
            <translation>+/- 1E-6 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-5 s</source>
            <translation>+/- 1E-5 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-4 s</source>
            <translation>+/- 1E-4 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-3 s</source>
            <translation>+/- 1E-3 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-2 s</source>
            <translation>+/- 1E-2 s</translation>
        </message>
        <message utf8="true">
            <source>+/- 1E-1 s</source>
            <translation>+/- 1E-1 s</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderTraceWidget</name>
        <message utf8="true">
            <source>s</source>
            <translation>s</translation>
        </message>
    </context>
    <context>
        <name>ui::CScriptView</name>
        <message utf8="true">
            <source>Please Choose a Script.. </source>
            <translation>Scegliere uno script. </translation>
        </message>
        <message utf8="true">
            <source>Script:</source>
            <translation>Script:</translation>
        </message>
        <message utf8="true">
            <source>State:</source>
            <translation>Stato:</translation>
        </message>
        <message utf8="true">
            <source>Current State</source>
            <translation>Stato corrente</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Arrestato</translation>
        </message>
        <message utf8="true">
            <source>Timer:</source>
            <translation>Timer:</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:</source>
            <translation>Tempo script trascorso:</translation>
        </message>
        <message utf8="true">
            <source>Timer Amount</source>
            <translation>Valore timer</translation>
        </message>
        <message utf8="true">
            <source>Output:</source>
            <translation>Output:</translation>
        </message>
        <message utf8="true">
            <source>Script Finished in:  </source>
            <translation>Script completato in:  </translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Script</source>
            <translation>Scegli&#xA;Script</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>Esegui script</translation>
        </message>
        <message utf8="true">
            <source>Clear&#xA;Output</source>
            <translation>Cancella&#xA;Output</translation>
        </message>
        <message utf8="true">
            <source>Stop Script</source>
            <translation>Arresta script</translation>
        </message>
        <message utf8="true">
            <source>RUNNING...</source>
            <translation>ESECUZIONE...</translation>
        </message>
        <message utf8="true">
            <source>Script Elapsed Time:  </source>
            <translation>Tempo script trascorso:</translation>
        </message>
        <message utf8="true">
            <source>Please Choose a different Script.. </source>
            <translation>Scegliere un altro script. </translation>
        </message>
        <message utf8="true">
            <source>Error.</source>
            <translation>Errore.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING.</source>
            <translation>ESECUZIONE.</translation>
        </message>
        <message utf8="true">
            <source>RUNNING..</source>
            <translation>ESECUZIONE...</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeFavoritesDialog</name>
        <message utf8="true">
            <source>Customize Test List</source>
            <translation>Personalizza Elenco Test</translation>
        </message>
        <message utf8="true">
            <source>Show results at startup</source>
            <translation>Mostra risultati all'avvio</translation>
        </message>
        <message utf8="true">
            <source>Move Up</source>
            <translation>Sposta in alto</translation>
        </message>
        <message utf8="true">
            <source>Move Down</source>
            <translation>Sposta in basso</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Elimina tutto</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Rinomina</translation>
        </message>
        <message utf8="true">
            <source>Separator</source>
            <translation>Separatore</translation>
        </message>
        <message utf8="true">
            <source>Add Shortcut</source>
            <translation>Aggiungi collegamento</translation>
        </message>
        <message utf8="true">
            <source>Add Saved Test</source>
            <translation>Aggiungi test salvato</translation>
        </message>
        <message utf8="true">
            <source>Delete all favorites?</source>
            <translation>Cancellare tutti i preferiti?</translation>
        </message>
        <message utf8="true">
            <source>The favorites list is default.</source>
            <translation>L'elenco preferiti è predefinito.</translation>
        </message>
        <message utf8="true">
            <source>All custom favorites will be deleted and the list will be restored to the defaults for this unit.  Do you want to continue?</source>
            <translation>Tutti i preferiti personalizzati verranno eliminati e la lista verrà reimpostata con le impostazioni predefinite per questa unità.  Continuare?</translation>
        </message>
        <message utf8="true">
            <source>Test configurations (*.tst)</source>
            <translation>Configurazioni dei test (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Dual Test configurations (*.dual_tst)</source>
            <translation>Configurazioni test duali (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Select Saved Test</source>
            <translation>Seleziona test salvato</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
    </context>
    <context>
        <name>ui::CEmptyTestLaunchStrategy</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Launching Empty Test View</source>
            <translation>Attendere prego...&#xA;Avvio Vista Test Vuota</translation>
        </message>
    </context>
    <context>
        <name>ui::CFavoriteTestNameDialog</name>
        <message utf8="true">
            <source>Pin Test</source>
            <translation>Fissa Test</translation>
        </message>
        <message utf8="true">
            <source>Rename Pinned Test</source>
            <translation>Rinomina Test Fissati</translation>
        </message>
        <message utf8="true">
            <source>Pin to tests list</source>
            <translation>Fissa all'elenco dei test</translation>
        </message>
        <message utf8="true">
            <source>Save test configuration</source>
            <translation>Salva configurazione test</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>Nome test:</translation>
        </message>
        <message utf8="true">
            <source>Enter the name to display</source>
            <translation>Immettere il nome da visualizzare</translation>
        </message>
        <message utf8="true">
            <source>This test is the same as %1</source>
            <translation>Questo test è uguale a %1</translation>
        </message>
        <message utf8="true">
            <source>This is a shortcut to launch a test application.</source>
            <translation>Questo è un collegamento per l'avvio di un'applicazione di test.</translation>
        </message>
        <message utf8="true">
            <source>Description: %1</source>
            <translation>Descrizione: %1</translation>
        </message>
        <message utf8="true">
            <source>This is saved test configuration.</source>
            <translation>Questa è configurazione test salvata.</translation>
        </message>
        <message utf8="true">
            <source>File Name: %1</source>
            <translation>Nome file: %1</translation>
        </message>
        <message utf8="true">
            <source>Replace</source>
            <translation>Sostituisci</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLaunch</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Options have expired.&#xA;Exit and re-launch BERT from the System Page.</source>
            <translation>Impossibile avviare il test...&#xA;Le opzioni sono scadute.&#xA;Uscire e riavviare BERT dalla pagina di sistema.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestLinkWidget</name>
        <message utf8="true">
            <source>This test cannot be launched right now.  It may not be supported by the current hardware configuration, or may require more resources.</source>
            <translation>Questo test non può essere avviato ora.  Potrebbe non essere supportato dalla configurazione hardware corrente o richiedere più risorse.</translation>
        </message>
        <message utf8="true">
            <source>Try removing tests running on other tabs.</source>
            <translation>Provare a rimuovere i test in esecuzione su altre schede.</translation>
        </message>
        <message utf8="true">
            <source>This test is running on another port.  Do you want to go to that test? (on tab %1)</source>
            <translation>Questo test è in esecuzione su un'altra porta.  Andare a tale test? (nella scheda %1)</translation>
        </message>
        <message utf8="true">
            <source>Another test (on tab %1) can be reconfigured to the selected test.  Do you want to reconfigure that test?</source>
            <translation>È possibile riconfigurare un altro test (sulla scheda %1) per il test selezionato.  Riconfigurare tale test?</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestListWidget</name>
        <message utf8="true">
            <source>List is empty.</source>
            <translation>Elenco vuoto.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewLaunchStrategy</name>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Optical jitter function OFF.&#xA;Launch optical jitter function from Home/System Page.</source>
            <translation>Impossibile avviare il test...&#xA;Funzione jitter ottico OFF.&#xA;Lancio funzione jitter ottico da pagina di Home/Sistema.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Remove/reconfigure an existing test.&#xA;&#xA;</source>
            <translation>Impossibile avviare il test...&#xA;Risorse di alimentazione non disponibili.&#xA;Rimuovere/riconfigurare un test esistente.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Power resources not available.&#xA;Deselect another module or remove/reconfigure&#xA;an existing test.&#xA;&#xA;</source>
            <translation>Impossibile avviare il test...&#xA;Risorse di alimentazione non disponibili.&#xA;Deselezionare un altro modulo o rimuovere/riconfigurare&#xA;un test esistente.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Adding Test </source>
            <translation>Attendere...&#xA;Aggiunta di test </translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Reconfiguring Test to </source>
            <translation>Attendere...&#xA;Riconfigurazione del test per </translation>
        </message>
        <message utf8="true">
            <source>Unable to launch test...&#xA;Required resources may be in use.&#xA;&#xA;Please contact technical support.&#xA;</source>
            <translation>Impossibile avviare il test...&#xA;Le risorse necessarie potrebbero essere in uso.&#xA;&#xA;Contattare il servizio di supporto tecnico.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Constructing UI objects</source>
            <translation>Costruzione oggetti UI</translation>
        </message>
        <message utf8="true">
            <source>UI synchronizing with application module</source>
            <translation>Sinctronizzazione UI con modulo applicazione</translation>
        </message>
        <message utf8="true">
            <source>Initializing UI views</source>
            <translation>Inizializzazione viste UI</translation>
        </message>
    </context>
    <context>
        <name>ui::CMenuListViewWidget</name>
        <message utf8="true">
            <source>Back</source>
            <translation>Indietro</translation>
        </message>
    </context>
    <context>
        <name>ui::testview::CTestsTabBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Scegli&#xA;Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CAboutDialog</name>
        <message utf8="true">
            <source>Viavi 8000</source>
            <translation>Viavi 8000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 6000</source>
            <translation>Viavi 6000</translation>
        </message>
        <message utf8="true">
            <source>Viavi 5800</source>
            <translation>Viavi 5800</translation>
        </message>
        <message utf8="true">
            <source>Copyright Viavi Solutions</source>
            <translation>Copyright Viavi Solutions</translation>
        </message>
        <message utf8="true">
            <source>Instrument info</source>
            <translation>Informazioni strumento</translation>
        </message>
        <message utf8="true">
            <source>Options</source>
            <translation>Opzioni</translation>
        </message>
        <message utf8="true">
            <source>Saved file</source>
            <translation>File salvato</translation>
        </message>
    </context>
    <context>
        <name>ui::CAccessModeDialog</name>
        <message utf8="true">
            <source>User Interface Access Mode</source>
            <translation>Modalità di accesso interfaccia utente</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;Read-Only access mode prevents the user from changing settings&#xA;which may affect the remote control operations.</source>
            <translation>Il controllo remoto è in uso.&#xA;&#xA;La modalità di accesso in sola lettura impedisce all'utente di modificare le impostazioni&#xA;che possono influire sulle operazioni di controllo remoto.</translation>
        </message>
        <message utf8="true">
            <source>Access Mode</source>
            <translation>Modalità di accesso</translation>
        </message>
    </context>
    <context>
        <name>ui::CAppSvcMsgHandler</name>
        <message utf8="true">
            <source>MSAM was reset due to PIM configuration change.</source>
            <translation>Il MSAM è stato reimpostato a causa della modifica di configurazione PIM.</translation>
        </message>
        <message utf8="true">
            <source>A PIM has been inserted or removed. If swapping PIMs, continue to do so now.&#xA;MSAM will now be restarted. This may take up to 2 Minutes. Please wait...</source>
            <translation>Un PIM è stato inserito o rimosso. Se si scambiano i PIM, continuare a farlo ora.&#xA;Il MSAM sarà ora riavviato. Potrebbero essere necessari fino a 2 minuti. Attendere...</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module is running too hot and will&#xA;automatically shut down if internal temperature keeps&#xA;rising.  Please save your data, shut down BERT&#xA;module, and call technical support.</source>
            <translation>Il Modulo BERT in esecuzione è troppo caldo e&#xA;sarà automaticamente spegno se la temperatura interna&#xA;continuerà a salire.  Si prega di salvare i dati, spegnere il modulo &#xA;BERT e rivolgersi all'assistenza tecnica.</translation>
        </message>
        <message utf8="true">
            <source>BERT Module was forced to shut down due to overheating.</source>
            <translation>È stato necessario spegnere il modulo BERT a causa di un surriscaldamento.</translation>
        </message>
        <message utf8="true">
            <source>XFP PIM in wrong slot. Please move XFP PIM to Port #1.</source>
            <translation>PIM XFP in slot errato. Passare il PIM XFP sulla Porta #1.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module XFP PIM is in the wrong slot.&#xA; Please move the XFP PIM to Port #1.</source>
            <translation>Il PIM XFP del Modulo BERT è nello slot sbagliato.&#xA; Spostare il PIM XFP sulla Porta #1.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an electrical test but the selected SFP looks like an optical SFP.</source>
            <translation>È stato selezionato un test elettrico , ma l'SFP scelto sembra un SFP ottico.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an electrical SFP.</source>
            <translation>Assicurarsi di stare utilizzando un SFP elettrico.</translation>
        </message>
        <message utf8="true">
            <source>The BERT Module has detected a possible error on application %1.&#xA;&#xA;You have selected an electrical test but the SFP looks like an optical SFP.  Please replace or select another SFP.</source>
            <translation>Il modulo BERT ha rilevato un possibile errore nell'applicazione %1.&#xA;&#xA;È stato selezionato un test elettrico, ma l'SFP sembra un SFP ottico.  Sostituire o selezionare un altro SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected an optical test but the selected SFP looks like an electrical SFP.</source>
            <translation>È stato selezionato un test ottico, ma l'SFP scelto sembra un SFP elettrico.</translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an optical SFP.</source>
            <translation>Assicurarsi di stare utilizzando un SFP ottico.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error on application %1.&#xA;&#xA;You have selected an optical test but the SFP looks like an&#xA;electrical SFP.  Please replace or select another SFP.</source>
            <translation>L'assieme di prova ha rilevato un possibile errore nell'applicazione %1.&#xA;&#xA;È stato selezionato un test ottico, ma l'SFP sembra un SFP&#xA;elettrico.  Sostituire o selezionare un altro SFP.</translation>
        </message>
        <message utf8="true">
            <source>You have selected a 10G test but the inserted transceiver does not look like an SFP+. </source>
            <translation>È stato selezionato un test 10G ma il ricetrasmettitore inserito non sembra un SFP +. </translation>
        </message>
        <message utf8="true">
            <source>Make sure you are using an SFP+.</source>
            <translation>Assicurarsi di stare utilizzando un SFP+.</translation>
        </message>
        <message utf8="true">
            <source>The test set has detected a possible error.  This test requires an SFP+, but the transceiver does not look like one. Please replace with an SFP+.</source>
            <translation>L'assieme di prova ha rilevato un possibile errore.  Questo test richiede un SFP+ , ma il ricetrasmettitore non sembra esserlo. Sostituire con un SFP+.</translation>
        </message>
    </context>
    <context>
        <name>ui::CAutomaticReportSettingDialog</name>
        <message utf8="true">
            <source>Automatic Report Settings</source>
            <translation>Impostazioni report automatico</translation>
        </message>
        <message utf8="true">
            <source>Overwrite the same file</source>
            <translation>Sovrascrivi lo stesso file</translation>
        </message>
        <message utf8="true">
            <source>AutoReport</source>
            <translation>AutoReport</translation>
        </message>
        <message utf8="true">
            <source>Warning:    Selected drive is full. You can free up space manually, or let the 5 oldest reports be&#xA;deleted automatically.</source>
            <translation>Avviso:    L'unità selezionata è piena. È possibile liberare spazio manualmente , oppure lasciare che i 5 più rapporti più vecchi vegano&#xA;automaticamente eliminati.</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic reports</source>
            <translation>Attiva report automatici</translation>
        </message>
        <message utf8="true">
            <source>Reporting Period</source>
            <translation>Periodo di copertura report</translation>
        </message>
        <message utf8="true">
            <source>Min:</source>
            <translation>Min.:</translation>
        </message>
        <message utf8="true">
            <source>Max:</source>
            <translation>Max.:</translation>
        </message>
        <message utf8="true">
            <source>Restart test after report creation</source>
            <translation>Riavviare il test al termine della creazione del report</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Modalità</translation>
        </message>
        <message utf8="true">
            <source>Create a separate file</source>
            <translation>Crea un file separato</translation>
        </message>
        <message utf8="true">
            <source>Report Name</source>
            <translation>Nome del report</translation>
        </message>
        <message utf8="true">
            <source>Date and time of creation automatically appended to name</source>
            <translation>Data e ora di creazione aggiunte automaticamente al nome</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Formato:</translation>
        </message>
        <message utf8="true">
            <source>PDF</source>
            <translation>PDF</translation>
        </message>
        <message utf8="true">
            <source>CSV</source>
            <translation>CSV</translation>
        </message>
        <message utf8="true">
            <source>Text</source>
            <translation>Testo</translation>
        </message>
        <message utf8="true">
            <source>HTML</source>
            <translation>HTML</translation>
        </message>
        <message utf8="true">
            <source>XML</source>
            <translation>XML</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports not supported in Read-Only access mode.</source>
            <translation>Report automatici non è supportato nella modalità di accesso in Sola lettura.</translation>
        </message>
        <message utf8="true">
            <source>The Automatic Reports will be saved to the Hard Disk.</source>
            <translation>I report automatici verranno salvati sul disco rigido.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports require a hard disk.  It does not appear this unit has one installed.</source>
            <translation>I rapporti automatici richiedono un disco rigido.  Questa unità non sembra averne uno installato.</translation>
        </message>
        <message utf8="true">
            <source>Automatic Reports cannot be enabled when an automated script is running.</source>
            <translation>Impossibile abilitare i report automatici durante l'esecuzione di uno script automatico.</translation>
        </message>
        <message utf8="true">
            <source>Creating Automatic Report</source>
            <translation>Creazione di report automatici</translation>
        </message>
        <message utf8="true">
            <source>Preparing...</source>
            <translation>Preparazione...</translation>
        </message>
        <message utf8="true">
            <source>Creating </source>
            <translation>Creazione </translation>
        </message>
        <message utf8="true">
            <source> Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source>Deleting previous report...</source>
            <translation>Eliminazione report precedente...</translation>
        </message>
        <message utf8="true">
            <source>Done.</source>
            <translation>Terminato.</translation>
        </message>
    </context>
    <context>
        <name>ui::CBertApplication</name>
        <message utf8="true">
            <source>**** INSUFFICIENT POWER.  DESELECT ANOTHER MODULE ****</source>
            <translation>**** ENERGIA INSUFFICIENTE.  DESELEZIONARE UN ALTRO MODULO ****</translation>
        </message>
        <message utf8="true">
            <source>Serial connection successful</source>
            <translation>Connessione seriale riuscita</translation>
        </message>
        <message utf8="true">
            <source>Application checking for upgrades</source>
            <translation>Verifica aggiornamenti dell'applicazione</translation>
        </message>
        <message utf8="true">
            <source>Application ready for communications</source>
            <translation>Applicazione pronta per le comunicazioni</translation>
        </message>
        <message utf8="true">
            <source>***** ERROR IN KERNEL UPGRADE *****</source>
            <translation>***** ERRORE IN AGGIORNAMENTO KERNEL *****</translation>
        </message>
        <message utf8="true">
            <source>***** Make sure Ethernet Security=Standard and/or Reinstall BERT software *****</source>
            <translation>***** Accertarsi che Ethernet Security = Standard e / o reinstallare il software BERT *****</translation>
        </message>
        <message utf8="true">
            <source>*** ERROR IN APPLICATION UPGRADE.  Reinstall module software ***</source>
            <translation>***** ERRORE IN AGGIORNAMENTO APPLICAZIONE.  Reinstallare il modulo software ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN UPGRADE. INSUFFICIENT POWER. ****</source>
            <translation>***** ERRORE IN AGGIORNAMENTO. ENERGIA INSUFFICIENTE. ****</translation>
        </message>
        <message utf8="true">
            <source>*** Startup Error: Please deactivate BERT Module then reactivate ***</source>
            <translation>*** Errore di avvio: Disattivare il modulo BERT e riattivarlo ***</translation>
        </message>
    </context>
    <context>
        <name>ui::CBigappTestView</name>
        <message utf8="true">
            <source>View</source>
            <translation>Visualizza</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Report</translation>
        </message>
        <message utf8="true">
            <source>Tools</source>
            <translation>Strumenti</translation>
        </message>
        <message utf8="true">
            <source>Create Report...</source>
            <translation>Crea rapporto...</translation>
        </message>
        <message utf8="true">
            <source>Automatic Report...</source>
            <translation>Report automatico...</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Avvia test</translation>
        </message>
        <message utf8="true">
            <source>Stop Test</source>
            <translation>Arresta test</translation>
        </message>
        <message utf8="true">
            <source>Customize...</source>
            <translation>Personalizza...</translation>
        </message>
        <message utf8="true">
            <source>Access Mode...</source>
            <translation>Modalità di accesso...</translation>
        </message>
        <message utf8="true">
            <source>Reset Test to Defaults</source>
            <translation>Ripristina test su valori predefiniti</translation>
        </message>
        <message utf8="true">
            <source>Clear History</source>
            <translation>Cancella Cronologia</translation>
        </message>
        <message utf8="true">
            <source>Run Scripts...</source>
            <translation>Esegui script...</translation>
        </message>
        <message utf8="true">
            <source>VT100 Emulation</source>
            <translation>Emulazione VT100</translation>
        </message>
        <message utf8="true">
            <source>Modem Settings...</source>
            <translation>Impostazioni modem...</translation>
        </message>
        <message utf8="true">
            <source>Restore Default Layout</source>
            <translation>Ripristina layout predefinito</translation>
        </message>
        <message utf8="true">
            <source>Result Windows</source>
            <translation>Finestra risultati</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Singolo</translation>
        </message>
        <message utf8="true">
            <source>Split Left/Right</source>
            <translation>Suddivisa Destra/Sinistra</translation>
        </message>
        <message utf8="true">
            <source>Split Top/Bottom</source>
            <translation>Suddivisa Alto/Basso</translation>
        </message>
        <message utf8="true">
            <source>2 x 2 Grid</source>
            <translation>Griglia 2 x 2</translation>
        </message>
        <message utf8="true">
            <source>Join Bottom</source>
            <translation>Unisci base</translation>
        </message>
        <message utf8="true">
            <source>Join Left</source>
            <translation>Unisci sinistra</translation>
        </message>
        <message utf8="true">
            <source>Show Only Results</source>
            <translation>Mostra solo risultati</translation>
        </message>
        <message utf8="true">
            <source>Test Status</source>
            <translation>Stato test</translation>
        </message>
        <message utf8="true">
            <source>LEDs</source>
            <translation>LED</translation>
        </message>
        <message utf8="true">
            <source>Config Panel</source>
            <translation>Pannello config</translation>
        </message>
        <message utf8="true">
            <source>Actions Panel</source>
            <translation>Pannello Azioni</translation>
        </message>
    </context>
    <context>
        <name>ui::CChooseScriptFileDialog</name>
        <message utf8="true">
            <source>Choose Script</source>
            <translation>Scegli script</translation>
        </message>
        <message utf8="true">
            <source>Script files (*.tcl)</source>
            <translation>File Script (*.tcl)</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Script</source>
            <translation>Seleziona&#xA;script</translation>
        </message>
    </context>
    <context>
        <name>ui::CConnectionDialog</name>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Connessioni segnale</translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportFileDialog</name>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Crea rapporto</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Testo (*.Txt)</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;Contents</source>
            <translation>Scegli&#xA;Sommario</translation>
        </message>
        <message utf8="true">
            <source>Create</source>
            <translation>Crea</translation>
        </message>
        <message utf8="true">
            <source>Format:</source>
            <translation>Formato:</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Visualizza rapporto dopo la creazione</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Errore: nome file non può essere vuoto.</translation>
        </message>
        <message utf8="true">
            <source>Choose contents for</source>
            <translation>Scegliere i contenuti per </translation>
        </message>
    </context>
    <context>
        <name>ui::CCreateReportWidget</name>
        <message utf8="true">
            <source>Format</source>
            <translation>Formato</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome file</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Seleziona...</translation>
        </message>
        <message utf8="true">
            <source>View report after creation</source>
            <translation>Visualizza rapporto dopo la creazione</translation>
        </message>
        <message utf8="true">
            <source>Include message log</source>
            <translation>Includere registro dei messaggi</translation>
        </message>
        <message utf8="true">
            <source>Create&#xA;Report</source>
            <translation>Crea&#xA;Rapporto</translation>
        </message>
        <message utf8="true">
            <source>View&#xA;Report</source>
            <translation>Visualizza&#xA;Rapporto</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Testo (*.Txt)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Errore: nome file non può essere vuoto.</translation>
        </message>
        <message utf8="true">
            <source>Report saved</source>
            <translation>Report salvato</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalDialogView</name>
        <message utf8="true">
            <source>Please attenuate the signal.</source>
            <translation>Attenuare il segnale.</translation>
        </message>
        <message utf8="true">
            <source>The event log and histogram are full.&#xA;&#xA;</source>
            <translation>Il registro eventi e l'istogramma sono pieni.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The K1/K2 logs are full.&#xA;&#xA;</source>
            <translation>I log K1/K2 sono pieni.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The log is full.&#xA;&#xA;</source>
            <translation>Il registro è pieno.&#xA;&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The test will continue without logging&#xA;additional items of this kind.  Restarting&#xA;the test will clear all logs and histograms.</source>
            <translation>Il test continuerà senza registrare&#xA;elementi aggiuntivi di questo tipo.   Il riavvio&#xA;del test causerà la cancellazione di tutti i log e gli istogrammi.</translation>
        </message>
        <message utf8="true">
            <source>Optical&#xA;Reset</source>
            <translation>Ottico&#xA;Reimposta</translation>
        </message>
        <message utf8="true">
            <source>End Test</source>
            <translation>Termina test</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Receiver Overload</source>
            <translation>Sovraccarico ricevitore</translation>
        </message>
        <message utf8="true">
            <source>Log Is Full</source>
            <translation>Il registro è pieno</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>Attenzione</translation>
        </message>
    </context>
    <context>
        <name>ui::CCriticalErrorDialog</name>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Nessun test in esecuzione</translation>
        </message>
    </context>
    <context>
        <name>ui::CCustomizeDialog</name>
        <message utf8="true">
            <source>Customize User Interface Look and Feel</source>
            <translation>Personalizza aspetto interfaccia utente</translation>
        </message>
    </context>
    <context>
        <name>ui::CDialogMgr</name>
        <message utf8="true">
            <source>Save Test</source>
            <translation>Salva test</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test</source>
            <translation>Salva dual test</translation>
        </message>
        <message utf8="true">
            <source>Load Test</source>
            <translation>Carica test</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst)</source>
            <translation>Tutti i file (*.tst *.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Tests (*.tst)</source>
            <translation>Test salvati (*.tst)</translation>
        </message>
        <message utf8="true">
            <source>Saved Dual Tests (*.dual_tst)</source>
            <translation>Dual test salvati (*.dual_tst)</translation>
        </message>
        <message utf8="true">
            <source>Load Setup</source>
            <translation>Carica impostazione</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test</source>
            <translation>Carica test doppio</translation>
        </message>
        <message utf8="true">
            <source>Import Saved Test from USB</source>
            <translation>Importa test salvato da USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</source>
            <translation>Tutti i file (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams *.tar)</translation>
        </message>
        <message utf8="true">
            <source>Saved Classic RFC Test Configurations (*.classic_rfc)</source>
            <translation>Configurazioni di test RFC classico salvate (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved RFC Test Configurations (*.expert_rfc)</source>
            <translation>Configurazioni di test RFC salvate (*.expert_rfc)</translation>
        </message>
        <message utf8="true">
            <source>Saved FC Test Configurations (*.fc_test)</source>
            <translation>Configurazioni test FC salvate (*.fc_test)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed Configurations (*.truespeed)</source>
            <translation>Configurazioni TrueSpeed salvate (*.truespeed)</translation>
        </message>
        <message utf8="true">
            <source>Saved TrueSpeed VNF Configurations (*.vts)</source>
            <translation>Configurazioni VNF TrueSpeed salvate (*.vts)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.sam)</source>
            <translation>Configurazioni SAMComplete salvati (*.sam)</translation>
        </message>
        <message utf8="true">
            <source>Saved SAMComplete Configurations (*.ams)</source>
            <translation>Configurazioni SAMComplete salvate (*.ams)</translation>
        </message>
        <message utf8="true">
            <source>Saved OTN Check Configurations (*.otncheck)</source>
            <translation>Configurazioni OTN Check salvate (*.otncheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Optics Self-Test Configurations (*.optics)</source>
            <translation>Salva Configurazioni Auto-Diagnosi Ottiche (*.optics)</translation>
        </message>
        <message utf8="true">
            <source>Saved CPRI Check Configurations (*.cpri)</source>
            <translation>Configurazioni Controllo CPRI Salvate (*.cpri)</translation>
        </message>
        <message utf8="true">
            <source>Saved PTP Check Configurations (*.ptpCheck)</source>
            <translation>Configurazioni Controllo PTP Salvate (*.ptpCheck)</translation>
        </message>
        <message utf8="true">
            <source>Saved Zip Files (*.tar)</source>
            <translation>File Zip salvati (*.tar)</translation>
        </message>
        <message utf8="true">
            <source>Export Saved Test to USB</source>
            <translation>Esporta Test salvato su USB</translation>
        </message>
        <message utf8="true">
            <source>All files (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</source>
            <translation>Tutti i file (*.tst *.dual_tst *.classic_rfc *.expert_rfc *.fc_test *.truespeed *.vts *.sam *.otncheck *.optics *.ptpCheck *.cpri *.ams)</translation>
        </message>
        <message utf8="true">
            <source>User saved multi tests</source>
            <translation>Test multipli salvati da utente</translation>
        </message>
        <message utf8="true">
            <source>User saved test</source>
            <translation>Test salvato da utente</translation>
        </message>
        <message utf8="true">
            <source>Remote Control is in use.&#xA;&#xA;This operation is not allowed in Read-Only access mode.&#xA; Use Tools->Access Mode to enable Full Access.</source>
            <translation>Il controllo remoto è in uso.&#xA;&#xA;Questa operazione non è consentita in modalità di accesso in sola lettura.&#xA; Utilizzare Strumenti->Modalità di accesso per consentire l'Accesso completo.</translation>
        </message>
        <message utf8="true">
            <source>Options on the BERT Module have expired.&#xA;Please exit and re-launch BERT from the System Page.</source>
            <translation>Le opzioni sul modulo BERT sono scadute.&#xA;Uscire e riavviare BERT dalla pagina di Sistema.</translation>
        </message>
    </context>
    <context>
        <name>ui::CDualTestView</name>
        <message utf8="true">
            <source>Hide</source>
            <translation>Nascondi</translation>
        </message>
        <message utf8="true">
            <source>Restart Both Tests</source>
            <translation>Riavvia entrambi i test</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Riavvia</translation>
        </message>
        <message utf8="true">
            <source>Tools:</source>
            <translation>Strumenti:</translation>
        </message>
        <message utf8="true">
            <source>Actions</source>
            <translation>Azioni</translation>
        </message>
        <message utf8="true">
            <source>Config</source>
            <translation>Config.</translation>
        </message>
        <message utf8="true">
            <source>Maximized Result Window for Test : </source>
            <translation>Finestra risultati ingrandita per il test: </translation>
        </message>
        <message utf8="true">
            <source>Full View</source>
            <translation>Completi la vista</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Aggiungi prova</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manuale per l'utente</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Ottica consigliata</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Griglia di frequenza</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Guida rapida</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Save Dual Test Config As...</source>
            <translation>Salva configurazione test duale come...</translation>
        </message>
        <message utf8="true">
            <source>Load Dual Test Config...</source>
            <translation>Carica config test duale...</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Visualizza</translation>
        </message>
        <message utf8="true">
            <source>Change Test Selection ...</source>
            <translation>Cambia selezione test ...</translation>
        </message>
        <message utf8="true">
            <source>Go To Full Test View</source>
            <translation>Vai visualizza test completo</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Report</translation>
        </message>
        <message utf8="true">
            <source>Create Dual Test Report...</source>
            <translation>Crea rapporto test duale...</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Visualizza rapporto...</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Guida</translation>
        </message>
    </context>
    <context>
        <name>ui::CErrorDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Avviso</translation>
        </message>
    </context>
    <context>
        <name>ui::CFileMessageDialog</name>
        <message utf8="true">
            <source>Saving File</source>
            <translation>Salvataggio file</translation>
        </message>
        <message utf8="true">
            <source>New name:</source>
            <translation>Nuovo nome:</translation>
        </message>
        <message utf8="true">
            <source>Enter new file name</source>
            <translation>Inserire nome del nuovo file</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>Rinomina</translation>
        </message>
        <message utf8="true">
            <source>Cannot rename since a file exists with that name.&#xA;</source>
            <translation>Impossibile rinominare perché è già presente un file con tale nome.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenericView</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Restart</source>
            <translation>Riavvia</translation>
        </message>
    </context>
    <context>
        <name>ui::CGenInfoSettingDialog</name>
        <message utf8="true">
            <source>Edit User Info</source>
            <translation>Modifica Info Utente</translation>
        </message>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Nessuna selezione...</translation>
        </message>
        <message utf8="true">
            <source>Max characters: </source>
            <translation>Numero massimo di caratteri: </translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Seleziona logo ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Anteprima non disponibile.</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpDiagramsDialog</name>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Diagrammi di guida</translation>
        </message>
    </context>
    <context>
        <name>ui::CHelpViewerView</name>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manuale per l'utente</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>Config</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Indietro</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>Pagina iniziale</translation>
        </message>
        <message utf8="true">
            <source>Forward</source>
            <translation>Avanti</translation>
        </message>
    </context>
    <context>
        <name>ui::CIconLaunchView</name>
        <message utf8="true">
            <source>QuickLaunch</source>
            <translation>QuickLaunch</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
    </context>
    <context>
        <name>ui::CJitterMgr</name>
        <message utf8="true">
            <source>*** ERROR IN JITTER UPGRADE.  Reinstall module software ***</source>
            <translation>*** ERRORE IN AGGIORNAMENTO JITTER.  Reinstallare il modulo software ***</translation>
        </message>
        <message utf8="true">
            <source>**** ERROR IN OPTICAL JITTER FUNCTION. INSUFFICIENT POWER. ****</source>
            <translation>**** ERRORE NELLA FUNZIONE JITTER OTTICO. ENERGIA INSUFFICIENTE. ****</translation>
        </message>
        <message utf8="true">
            <source>Optical jitter function running</source>
            <translation>Funzione jitter ottico in esecuzione</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadProfileWidget</name>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>Profili %1 (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select Profiles</source>
            <translation>Seleziona profili</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Seleziona tutto</translation>
        </message>
        <message utf8="true">
            <source>Unselect All</source>
            <translation>Deseleziona tutti</translation>
        </message>
        <message utf8="true">
            <source>Note: Loading the "Connect" profile will connect the communications channel to the remote unit if necessary.</source>
            <translation>Nota: Caricando il profilo "Connect" si connetterà il canale di comunicazione all'unità remota se necessario.</translation>
        </message>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Elimina tutto</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Incompatible profile</source>
            <translation>Profilo incompatibile</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;Profiles</source>
            <translation>Carica&#xA;Profili</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Eliminare %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Eliminare tutti i profili di %1?&#xA;&#xA;Questa operazione non può essere annullata.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(I file di sola lettura non saranno cancellati.)</translation>
        </message>
        <message utf8="true">
            <source>Failed to load profiles from:</source>
            <translation>Caricamento profili non riuscito da:</translation>
        </message>
        <message utf8="true">
            <source>Loaded profiles from:</source>
            <translation>Profili caricati da:</translation>
        </message>
        <message utf8="true">
            <source>Some configurations were not loaded properly because they were not found in this application.</source>
            <translation>Alcune configurazioni non sono state caricate correttamente perché non sono state trovate in questa applicazione.</translation>
        </message>
        <message utf8="true">
            <source>Successfully loaded profiles from:</source>
            <translation>Profili correttamente caricati da:</translation>
        </message>
    </context>
    <context>
        <name>ui::CMainWindow</name>
        <message utf8="true">
            <source>Enable Dual Test</source>
            <translation>Abilita test duale</translation>
        </message>
        <message utf8="true">
            <source>Indexing applications</source>
            <translation>Indicizzazione applicazioni</translation>
        </message>
        <message utf8="true">
            <source>Validating options</source>
            <translation>Convalida opzioni</translation>
        </message>
        <message utf8="true">
            <source>No available tests for installed hardware or options.</source>
            <translation>Nessun test disponibile per l'hardware installato o per le opzioni.</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch any tests.  Check installed options and current hardware configuration.</source>
            <translation>Impossibile avviare qualsiasi test.  Controllare le opzioni installate e la configurazione hardware corrente.</translation>
        </message>
        <message utf8="true">
            <source>Restoring application running at power down</source>
            <translation>Ripristino di un'applicazione in esecuzione allo spegnimento</translation>
        </message>
        <message utf8="true">
            <source>Application running</source>
            <translation>Applicazione in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Unable to mount internal USB flash.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the BERT module.</source>
            <translation>Impossibile montare il flash USB interna.&#xA;&#xA;Assicurarsi che la periferica sia inserita saldamente accanto alla batteria. Una volta inserito riavviare il modulo BERT.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash filesystem appears to be corrupted. Please contact technical support for assistance.</source>
            <translation>Il file system interno del flash USB potrebbe essere danneggiato. Si prega di contattare il supporto tecnico per l' assistenza.</translation>
        </message>
        <message utf8="true">
            <source>The internal USB flash capacity is less than recommended size of 1G.&#xA;&#xA;Please ensure the device is securely inserted next to the battery. Once inserted please restart the unit.</source>
            <translation>La capacità interna della flash USB è inferiore a dimensione consigliata di 1G.&#xA;&#xA;Assicurarsi che la periferica sia inserita saldamente accanto alla batteria. Una volta inserito riavviare l'unità.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring setups.&#xA;Please try again.&#xA;</source>
            <translation>Si è verificato un errore durante il ripristino delle impostazioni.&#xA;Si prega di riprovare.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>All running tests will be terminated before loading saved tests.</source>
            <translation>Tutte le prove di esecuzione saranno terminate prima di caricare i test salvati.</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Loading Saved Tests</source>
            <translation>Attendere...&#xA;Caricamento test salvati</translation>
        </message>
        <message utf8="true">
            <source>The test document name or file path is not valid.&#xA;Use "Load Saved Test" to locate the document.&#xA;</source>
            <translation>Il percorso del file o il nome del documento del test non è valido.&#xA;Utilizzare "Carica test salvato" per individuare il documento.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while restoring a test.&#xA;Please try again.&#xA;</source>
            <translation>Si è verificato un errore durante il ripristino di un test.&#xA;Riprovare.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Battery charger enabled.</source>
            <translation>Caricabatterie abilitato.</translation>
        </message>
        <message utf8="true">
            <source>Please note that the battery charger will not be enabled in this mode. Charger will automatically be enabled when suitable tests are selected.</source>
            <translation>Si prega di notare che il caricabatterie non sarà attivato in questo modo. Caricabatterie verrà automaticamente attivato alla selezione dei test idonei.</translation>
        </message>
        <message utf8="true">
            <source>Remote control is in use for this module and&#xA;the display has been disabled.</source>
            <translation>Il controllo remoto è in uso per questo modulo e&#xA;il display è stato disattivato.</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBar</name>
        <message utf8="true">
            <source>Messages logged. Click to see...</source>
            <translation>Messaggi registrati. Clicca per vedere...</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Registro dei messaggi di %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageBarV2</name>
        <message utf8="true">
            <source>No messages</source>
            <translation>Nessun messaggio</translation>
        </message>
        <message utf8="true">
            <source>1 message</source>
            <translation>1 messaggio</translation>
        </message>
        <message utf8="true">
            <source>%1 messages</source>
            <translation>%1 messaggi</translation>
        </message>
        <message utf8="true">
            <source>Message log for %1</source>
            <translation>Registro dei messaggi di %1</translation>
        </message>
    </context>
    <context>
        <name>ui::CMessageLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro messaggi</translation>
        </message>
    </context>
    <context>
        <name>ui::CModemConfigDialog</name>
        <message utf8="true">
            <source>Modem Settings</source>
            <translation>Impostazioni modem</translation>
        </message>
        <message utf8="true">
            <source>Select an IP for this server's address and an IP to be assigned to the dial-in client</source>
            <translation>Selezionare un OP per l'indirizzo di questo server e un OP da assegnare al client chiamante</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>OP del server</translation>
        </message>
        <message utf8="true">
            <source>Client IP</source>
            <translation>OP del client </translation>
        </message>
        <message utf8="true">
            <source>Current Location (Country Code)</source>
            <translation>Posizione corrente (Codice Paese)</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect modem. Ensure the modem is plugged-in properly and try again.</source>
            <translation>Impossibile rilevare modem. Assicurarsi che il modem è collegato in modo corretto e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Device is busy. Disconnect all dial-in sessions and try again.</source>
            <translation>Il dispositivo è occupato. Scollegare tutte le sessioni di chiamata e riprovare.</translation>
        </message>
        <message utf8="true">
            <source>Unable to update modem. Disconnect all dial-in sessions and try again.</source>
            <translation>Impossibile aggiornare il modem. Scollegare tutte le sessioni di chiamata e riprovare.</translation>
        </message>
    </context>
    <context>
        <name>ui::CNtpSvcMsgHandler</name>
        <message utf8="true">
            <source>Restarting test(s) due to time change.</source>
            <translation>Riavvio del/i test causa cambiamento di ora.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionInputWidget</name>
        <message utf8="true">
            <source>Enter new option key below to install it.</source>
            <translation>Inserire sotto il nuovo tasto opzione per installarlo</translation>
        </message>
        <message utf8="true">
            <source>Option Key</source>
            <translation>Tasto di opzione</translation>
        </message>
        <message utf8="true">
            <source>Install</source>
            <translation>Installa</translation>
        </message>
        <message utf8="true">
            <source>Import</source>
            <translation>Importa</translation>
        </message>
        <message utf8="true">
            <source>Contact Viavi to purchase software options.</source>
            <translation>Contattare Viavi per l'acquisto di opzioni software.</translation>
        </message>
        <message utf8="true">
            <source>Option Challenge ID: </source>
            <translation>ID: sfida opzione</translation>
        </message>
        <message utf8="true">
            <source>Key Accepted! Reboot to activate new option.</source>
            <translation>Chiave accettata! Riavviare per attivare la nuova opzione.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option slots are full.</source>
            <translation>Chiave rifiutata  Gli slot opzioni scadenza sono pieni.</translation>
        </message>
        <message utf8="true">
            <source>Rejected Key - Expiring option was already installed.</source>
            <translation>Chiave rifiutata  Gli slot opzioni scadenza sono pieni.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Key - Please try again.</source>
            <translation>Chiave non valida. Riprovare.</translation>
        </message>
        <message utf8="true">
            <source>%1 of %2 key(s) accepted! Reboot to activate new option(s).</source>
            <translation>%1 di %2 tasto/i accettati! Riavviare per attivare la nuova o le nuove opzioni.</translation>
        </message>
        <message utf8="true">
            <source>Unable to open '%1' on USB flash drive.</source>
            <translation>Impossibile aprire '%1' su unità flash USB.</translation>
        </message>
    </context>
    <context>
        <name>ui::COptionsWidget</name>
        <message utf8="true">
            <source>There was a problem obtaining the Options information...</source>
            <translation>Si è verificato un problema nel recupero delle le informazioni sulle opzioni...</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Byte selezionato</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Impostazioni predefinite</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Legenda</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::COtnPsiWidget</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Legend:</source>
            <translation>Legenda:</translation>
        </message>
        <message utf8="true">
            <source>PT</source>
            <translation>PT</translation>
        </message>
        <message utf8="true">
            <source>MSI (Unused)</source>
            <translation>MSI (Non usato)</translation>
        </message>
        <message utf8="true">
            <source>Reserved</source>
            <translation>Riservato</translation>
        </message>
    </context>
    <context>
        <name>ui::CProductSpecific</name>
        <message utf8="true">
            <source>About BERT Module</source>
            <translation>Informazioni su Modulo BERT</translation>
        </message>
        <message utf8="true">
            <source>CSAM</source>
            <translation>CSAM</translation>
        </message>
        <message utf8="true">
            <source>MSAM</source>
            <translation>MSAM</translation>
        </message>
        <message utf8="true">
            <source>Transport Module</source>
            <translation>Modulo di trasporto</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardControl</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importa una Scheda rapida</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickCardMenu</name>
        <message utf8="true">
            <source>Import A Quick Card</source>
            <translation>Importa una Scheda rapida</translation>
        </message>
    </context>
    <context>
        <name>ui::CQuickLaunchView</name>
        <message utf8="true">
            <source> Hide Menu</source>
            <translation> Nascondi Menu</translation>
        </message>
        <message utf8="true">
            <source> All Tests</source>
            <translation> Tutti i test</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Personalizza</translation>
        </message>
    </context>
    <context>
        <name>ui::CReportSettingDialog</name>
        <message utf8="true">
            <source>One, or more, of the selected screenshots was captured prior to the start&#xA;of the current test.  Make sure you have selected the correct file.</source>
            <translation>Una o più delle schermate selezionate era stata acquisita prima di iniziare&#xA;il test corrente.  Verificare di avere selezionato il file corretto.</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;All</source>
            <translation>Seleziona&#xA;Tutto</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;All</source>
            <translation>Deseleziona&#xA;Tutto</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;Screenshots</source>
            <translation>Seleziona&#xA;Schermate</translation>
        </message>
        <message utf8="true">
            <source>Unselect&#xA;Screenshots</source>
            <translation>Deseleziona&#xA;Schermate</translation>
        </message>
    </context>
    <context>
        <name>ui::CRsFecCalibrationDialog</name>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Calibrazione FEC-RS</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Calibrare</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Calibrazione in corso...</translation>
        </message>
        <message utf8="true">
            <source>Calibration is not complete.  Calibration is required to use RS-FEC.</source>
            <translation>La calibrazione non è completa.  È richiesta la calibrazione per utilizzare RS-FEC.</translation>
        </message>
        <message utf8="true">
            <source>(Calibration can be run from the RS-FEC tab in the setup pages.)</source>
            <translation>(La calibrazione può essere eseguita dalla scheda RS-FEC nelle pagine di configurazione.)</translation>
        </message>
        <message utf8="true">
            <source>Retry Calibration</source>
            <translation>Riprova Calibrazione</translation>
        </message>
        <message utf8="true">
            <source>Leave Calibration</source>
            <translation>Abbandonare Calibrazione</translation>
        </message>
        <message utf8="true">
            <source>Calibration Incomplete</source>
            <translation>Calibrazione Incompleta</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC è in genere utilizzato con SR4, PSM4, CWDM4.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Per eseguire la calibrazione RS-FEC, effettuare le seguenti operazioni (vale anche per CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Inserire un adattatore QSFP28 nella CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Inserire un ricetrasmettitore QSFP28 nell'adattatore</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Inserire un dispositivo di loopback in fibra nel ricetrasmettitore</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Fare clic su Calibra</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Stato</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Risincronizza</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Ora è necessario risincronizzare il ricetrasmettitore con il dispositivo in prova (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Rimuovere il dispositivo di loopback in fibra dal ricetrasmettitore</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Stabilire una connessione verso il dispositivo in prova (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Accendere il laser DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Verificare che il LED di Segnale Presente sulla CSAM sia verde</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Fare clic su Risincronizza Pista</translation>
        </message>
        <message utf8="true">
            <source>Resync complete.  The dialog may now be closed.</source>
            <translation>Risincronizzazione completa.  La finestra può ora essere chiusa.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveFileDialog</name>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Salva come di sola lettura</translation>
        </message>
        <message utf8="true">
            <source>Pin to test list</source>
            <translation>Fissa all'elenco dei test</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveProfileWidget</name>
        <message utf8="true">
            <source>File Name</source>
            <translation>Nome file</translation>
        </message>
        <message utf8="true">
            <source>Select...</source>
            <translation>Seleziona...</translation>
        </message>
        <message utf8="true">
            <source>Save as read-only</source>
            <translation>Salva come di sola lettura</translation>
        </message>
        <message utf8="true">
            <source>Save&#xA;Profiles</source>
            <translation>Salva&#xA;profili</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*.%2)</source>
            <translation>Profili %1 (*.%2)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
        <message utf8="true">
            <source>Profiles </source>
            <translation>Profili </translation>
        </message>
        <message utf8="true">
            <source> is read-only file. It can't be replaced.</source>
            <translation>è un file di sola lettura e non può essere sostituito.</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
        <message utf8="true">
            <source>Profiles saved</source>
            <translation>Profili salvati</translation>
        </message>
    </context>
    <context>
        <name>ui::CSelectLogoFileDialog</name>
        <message utf8="true">
            <source>Select Logo</source>
            <translation>Seleziona logo</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png *.jpg *.jpeg)</source>
            <translation>File immagine (*.png *.jpg *.jpeg)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
        <message utf8="true">
            <source>File is too large. Please Select another file.</source>
            <translation>Il file è troppo grande. Selezionare un altro file.</translation>
        </message>
    </context>
    <context>
        <name>ui::CSonetOverheadWidget</name>
        <message utf8="true">
            <source>Selected Byte</source>
            <translation>Byte selezionato</translation>
        </message>
        <message utf8="true">
            <source>POH:</source>
            <translation>POH:</translation>
        </message>
        <message utf8="true">
            <source>TOH:</source>
            <translation>TOH:</translation>
        </message>
        <message utf8="true">
            <source>SOH:</source>
            <translation>SOH:</translation>
        </message>
        <message utf8="true">
            <source>Defaults</source>
            <translation>Impostazioni predefinite</translation>
        </message>
        <message utf8="true">
            <source>Legend</source>
            <translation>Legenda</translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx</source>
            <translation>Rx</translation>
        </message>
    </context>
    <context>
        <name>ui::CStartStopTestSoftkey</name>
        <message utf8="true">
            <source>Start&#xA;Test</source>
            <translation>Avvia&#xA;Test</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Arresta&#xA;Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CStatusBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>In esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Arrestato</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Ritardo</translation>
        </message>
    </context>
    <context>
        <name>ui::CSystemConfigs</name>
        <message utf8="true">
            <source>TestPad</source>
            <translation>TestPad</translation>
        </message>
        <message utf8="true">
            <source>ANT</source>
            <translation>ANT</translation>
        </message>
        <message utf8="true">
            <source>Full Access</source>
            <translation>Accesso completo</translation>
        </message>
        <message utf8="true">
            <source>Read-Only</source>
            <translation>Sola lettura</translation>
        </message>
        <message utf8="true">
            <source>Dark</source>
            <translation>Scuro</translation>
        </message>
        <message utf8="true">
            <source>Light</source>
            <translation>Luce</translation>
        </message>
        <message utf8="true">
            <source>Quick Launch</source>
            <translation>Avvio veloce</translation>
        </message>
        <message utf8="true">
            <source>Results View</source>
            <translation>Vista risultati</translation>
        </message>
    </context>
    <context>
        <name>ui::CTcpThroughputView</name>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponibile</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (Mbps) based on:</source>
            <translation>Throughput TCP totale stimato (Mbps ) sulla base di:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (Mbps)</source>
            <translation>Max larghezza di banda disponibile (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average Round Trip Delay (ms)</source>
            <translation>Ritardo medio roundtrip (ms)</translation>
        </message>
        <message utf8="true">
            <source>Number of parallel TCP sessions needed to achieve maximum throughput:</source>
            <translation>Numero di sessioni TCP parallele necessarie per ottenere la massima velocità effettiva:</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Velocità Bit</translation>
        </message>
        <message utf8="true">
            <source>Window Size</source>
            <translation>Dimensione finestra</translation>
        </message>
        <message utf8="true">
            <source>Sessions</source>
            <translation>Sessioni</translation>
        </message>
        <message utf8="true">
            <source>Estimated total TCP Throughput (kbps) based on:</source>
            <translation>Throughput TCP totale stimato (kbps sulla base di:</translation>
        </message>
        <message utf8="true">
            <source>Max available bandwidth (kbps)</source>
            <translation>Max larghezza di banda disponibile (kbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>Throughput TCP</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestSerializer</name>
        <message utf8="true">
            <source>An error occurred while saving test settings.</source>
            <translation>Si è verificato un errore durante il salvataggio delle impostazioni del test.</translation>
        </message>
        <message utf8="true">
            <source>An error occurred while loading test settings.</source>
            <translation>Si è verificato un errore durante il caricamento delle impostazioni del test.</translation>
        </message>
        <message utf8="true">
            <source>The selected disk is full.&#xA;Remove some files and try saving again.&#xA;</source>
            <translation>Il disco selezionato è pieno.&#xA;Rimuovere alcuni file e riprovare a salvare.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>The following saved custom result category files differ from those currently loaded:</source>
            <translation>I seguenti file categoria personalizzata salvati risultato sono diversi da quelli attualmente caricati:</translation>
        </message>
        <message utf8="true">
            <source>... %1 others</source>
            <translation>... %1 altri</translation>
        </message>
        <message utf8="true">
            <source>Overwriting them may affect other tests.</source>
            <translation>La loro sovrascrittura può influenzare altri test.</translation>
        </message>
        <message utf8="true">
            <source>Continue overwriting?</source>
            <translation>Continuare sovrascrittura?</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>No</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Sì</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...&#xA;Restoring Setups...</source>
            <translation>Attendere prego...&#xA;Ripristino impostazioni...</translation>
        </message>
        <message utf8="true">
            <source>Insufficient resources to load %1 test at this time.&#xA;See "Test" menu for a list of tests available with current configuration.</source>
            <translation>Risorse insufficienti per caricare il test %1 in questo momento.&#xA;Aprire il menu "Test" per una lista dei test disponibili con la configurazione corrente.</translation>
        </message>
        <message utf8="true">
            <source>Unable to restore all test settings.&#xA;File content could be old or corrupted.&#xA;</source>
            <translation>Impossibile ripristinare tutte le impostazioni dei test.&#xA;Il contenuto dei file potrebbe essere vecchio o danneggiato.&#xA;</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestView</name>
        <message utf8="true">
            <source>Access Mode is Read-Only. To change it use Tools->Access Mode</source>
            <translation>La modalità di accesso è Sola lettura. Per cambiarla usare Strumenti -> Modalità di accesso</translation>
        </message>
        <message utf8="true">
            <source>No Running Test</source>
            <translation>Nessun test in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>This will reset all setups to defaults.&#xA;&#xA;Continue?</source>
            <translation>In questo modo verranno ripristinate tutte le impostazioni di default.&#xA;&#xA;Continuare?</translation>
        </message>
        <message utf8="true">
            <source>This will shut down and restart the test.&#xA;Test settings will be restored to defaults.&#xA;&#xA;Continue?&#xA;</source>
            <translation>Questo spegne e riavvia il test.&#xA;Test impostazioni dei test saranno ripristinate sui valori predefiniti.&#xA;&#xA;Continuare?&#xA;</translation>
        </message>
        <message utf8="true">
            <source>This workflow is currently running. Do you want to end it and start the new one?&#xA;&#xA;Click Cancel to continue running the previous workflow.&#xA;</source>
            <translation>Questo flusso di elaborazione è attualmente in esecuzione. Arrestarlo e avviarne uno nuovo?&#xA;&#xA;Fare clic su Annulla per continuare l'esecuzione del flusso di elaborazione precedente.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Do you want to end this workflow?&#xA;&#xA;Click Cancel to continue running.&#xA;</source>
            <translation>Terminare questo flusso di elaborazione?&#xA;&#xA;Fare clic su Annulla per continuare l'esecuzione.&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Unable to launch VT100. Serial device already in use.</source>
            <translation>Impossibile avviare VT100. Dispositivo seriale già in uso.</translation>
        </message>
        <message utf8="true">
            <source>P</source>
            <translation>P</translation>
        </message>
        <message utf8="true">
            <source>Port </source>
            <translation>Porta</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>Modulo </translation>
        </message>
        <message utf8="true">
            <source>Please note that pressing "Restart" will clear out results on *both* ports.</source>
            <translation>Si noti che premendo il tasto "Riavvia" si cancelleranno i risultati su *entrambe* le porte.</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenuBar</name>
        <message utf8="true">
            <source>Select&#xA;Test</source>
            <translation>Scegli&#xA;Test</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewMenus</name>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Load Test...</source>
            <translation>Carica test...</translation>
        </message>
        <message utf8="true">
            <source>Save Test As...</source>
            <translation>Salva test con nome...</translation>
        </message>
        <message utf8="true">
            <source>Load Only Setups...</source>
            <translation>Impostazioni solo carico...</translation>
        </message>
        <message utf8="true">
            <source>Add Test</source>
            <translation>Aggiungi prova</translation>
        </message>
        <message utf8="true">
            <source>Remove Test</source>
            <translation>Rimuovi test</translation>
        </message>
        <message utf8="true">
            <source>Help</source>
            <translation>Guida</translation>
        </message>
        <message utf8="true">
            <source>Help Diagrams</source>
            <translation>Diagrammi di guida</translation>
        </message>
        <message utf8="true">
            <source>View Report...</source>
            <translation>Visualizza rapporto...</translation>
        </message>
        <message utf8="true">
            <source>Export Report...</source>
            <translation>Esportazione report in corso...</translation>
        </message>
        <message utf8="true">
            <source>Edit User Info...</source>
            <translation>Modifica Info Utente...</translation>
        </message>
        <message utf8="true">
            <source>Import Report Logo...</source>
            <translation>Importazione logo report in corso...</translation>
        </message>
        <message utf8="true">
            <source>Import from USB</source>
            <translation>Importa da USB</translation>
        </message>
        <message utf8="true">
            <source>Saved Test...</source>
            <translation>Test salvato...</translation>
        </message>
        <message utf8="true">
            <source>Saved Custom Category...</source>
            <translation>Categoria personalizzata salvata...</translation>
        </message>
        <message utf8="true">
            <source>Export to USB</source>
            <translation>Esporta su USB</translation>
        </message>
        <message utf8="true">
            <source>Screenshot...</source>
            <translation>Schermata...</translation>
        </message>
        <message utf8="true">
            <source>Timing Data...</source>
            <translation>Dati di Sincronizzazione...</translation>
        </message>
        <message utf8="true">
            <source>Review/Install Options...</source>
            <translation>Esamina/Installa opzioni...</translation>
        </message>
        <message utf8="true">
            <source>Take Screenshot</source>
            <translation>Copia schermata</translation>
        </message>
        <message utf8="true">
            <source>User Manual</source>
            <translation>Manuale per l'utente</translation>
        </message>
        <message utf8="true">
            <source>Recommended Optics</source>
            <translation>Ottica consigliata</translation>
        </message>
        <message utf8="true">
            <source>Frequency Grid</source>
            <translation>Griglia di frequenza</translation>
        </message>
        <message utf8="true">
            <source>Signal Connections</source>
            <translation>Connessioni segnale</translation>
        </message>
        <message utf8="true">
            <source>What's This?</source>
            <translation>Guida rapida</translation>
        </message>
        <message utf8="true">
            <source>Quick Cards</source>
            <translation>Schede rapide</translation>
        </message>
    </context>
    <context>
        <name>ui::CTestViewsModel</name>
        <message utf8="true">
            <source>Please Wait...&#xA;Removing Test</source>
            <translation>Attendere...&#xA;Rimozione del test</translation>
        </message>
    </context>
    <context>
        <name>ui::CTextViewerView</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
    </context>
    <context>
        <name>ui::CToggleSoftkey</name>
        <message utf8="true">
            <source>Port 1&#xA;Selected</source>
            <translation>Porta 1&#xA;Selected</translation>
        </message>
        <message utf8="true">
            <source>Port 2&#xA;Selected</source>
            <translation>Porta 2&#xA;Selected</translation>
        </message>
    </context>
    <context>
        <name>ui::CVideoFileSelector</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Nessuna selezione...</translation>
        </message>
        <message utf8="true">
            <source>Select File...</source>
            <translation>Seleziona file...</translation>
        </message>
        <message utf8="true">
            <source>Import Packet Capture from USB</source>
            <translation>Importa acquisizione pacchetti da USB</translation>
        </message>
        <message utf8="true">
            <source>Select&#xA;File</source>
            <translation>Seleziona&#xA;File</translation>
        </message>
        <message utf8="true">
            <source>Saved Packet Capture (*.pcap)</source>
            <translation>Acquisizione pacchetti salvata (*.pcap)</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportFileDialog</name>
        <message utf8="true">
            <source>View Report</source>
            <translation>Visualizza rapporto</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Text (*.txt)</source>
            <translation>Testo (*.Txt)</translation>
        </message>
        <message utf8="true">
            <source>Log (*.log)</source>
            <translation>Log (*.log)</translation>
        </message>
        <message utf8="true">
            <source>View</source>
            <translation>Visualizza</translation>
        </message>
    </context>
    <context>
        <name>ui::CViewReportWidget</name>
        <message utf8="true">
            <source>View This&#xA;Report</source>
            <translation>Visualizza questo&#xA;report</translation>
        </message>
        <message utf8="true">
            <source>View Other&#xA;Reports</source>
            <translation>Mostra altri&#xA;report</translation>
        </message>
        <message utf8="true">
            <source>Rename&#xA;Report</source>
            <translation>Rinomina&#xA;rapporto</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
        <message utf8="true">
            <source> already exists.&#xA;Do you want to replace it?</source>
            <translation> già esistente.&#xA;Sostituire?</translation>
        </message>
        <message utf8="true">
            <source>Report renamed</source>
            <translation>Rapporto rinominato</translation>
        </message>
        <message utf8="true">
            <source>Error - Filename cannot be empty.</source>
            <translation>Errore: nome file non può essere vuoto.</translation>
        </message>
        <message utf8="true">
            <source>A report has been saved as </source>
            <translation>È stato salvato un report come</translation>
        </message>
        <message utf8="true">
            <source>No report has been saved.</source>
            <translation>Non è stata salvato nessun report.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWorkspaceSelectorView</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Vada</translation>
        </message>
    </context>
    <context>
        <name>ui::CSaveTieDataDialog</name>
        <message utf8="true">
            <source>Save TIE Data...</source>
            <translation>Salva Dati TIE...</translation>
        </message>
        <message utf8="true">
            <source>Save as type: </source>
            <translation>Salva come tipo: </translation>
        </message>
        <message utf8="true">
            <source>HRD file</source>
            <translation>File HRD</translation>
        </message>
        <message utf8="true">
            <source>CHRD file</source>
            <translation>File CHRD</translation>
        </message>
    </context>
    <context>
        <name>ui::CTieFileSaver</name>
        <message utf8="true">
            <source>Saving </source>
            <translation>Risparmio </translation>
        </message>
        <message utf8="true">
            <source>This could take several minutes...</source>
            <translation>L'operazione può richiedere alcuni minuti...</translation>
        </message>
        <message utf8="true">
            <source>Error: Couldn't open HRD file. Please try saving again.</source>
            <translation>Errore: impossibile aprire il file HRD. Provare a salvare nuovamente.</translation>
        </message>
        <message utf8="true">
            <source>Canceling...</source>
            <translation>Annullamento in corso...</translation>
        </message>
        <message utf8="true">
            <source>TIE data saved.</source>
            <translation>Dati TIE salvati.</translation>
        </message>
        <message utf8="true">
            <source>Error: File could not be saved. Please try again.</source>
            <translation>Errore: impossibile salvare il file. Riprovare.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisCloseDialog</name>
        <message utf8="true">
            <source>Warning</source>
            <translation>Avviso</translation>
        </message>
        <message utf8="true">
            <source>When closing Wander Analysis, all analysis results will be lost.&#xA;For continuing the analysis, click on Continue Analysis.</source>
            <translation>Con la chiusura di Wander Analysis tutti i risultati delle analisi verranno persi.&#xA;Per continuare l'analisi fare clic su Continua analisi.</translation>
        </message>
        <message utf8="true">
            <source>Close Analysis</source>
            <translation>Chiudi analisi</translation>
        </message>
        <message utf8="true">
            <source>Continue Analysis</source>
            <translation>Continua analisi</translation>
        </message>
    </context>
    <context>
        <name>ui::CWanderAnalysisView</name>
        <message utf8="true">
            <source>Wander Analysis</source>
            <translation>Wander Analysis</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Update&#xA;TIE Data</source>
            <translation>Aggiorna&#xA;Dati TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Update</source>
            <translation>Arresta TIE&#xA;Aggiorna</translation>
        </message>
        <message utf8="true">
            <source>Calculate&#xA;MTIE/TDEV</source>
            <translation>Calcola&#xA;MTIE/TDEV</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Calculation</source>
            <translation>Arresta&#xA;Calcolo</translation>
        </message>
        <message utf8="true">
            <source>Take&#xA;Screenshot</source>
            <translation>Copia&#xA;Schermata</translation>
        </message>
        <message utf8="true">
            <source>Load&#xA;TIE Data</source>
            <translation>Carica&#xA;dati TIE</translation>
        </message>
        <message utf8="true">
            <source>Stop TIE&#xA;Load</source>
            <translation>Arresta carico&#xA;TIE</translation>
        </message>
        <message utf8="true">
            <source>Close&#xA;Analysis</source>
            <translation>Chiudi&#xA;Analisi</translation>
        </message>
        <message utf8="true">
            <source>Load TIE Data</source>
            <translation>Carica dati TIE</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>All Wander Files (*.chrd *.hrd);;Hrd files (*.hrd);;Chrd files (*.chrd)</source>
            <translation>Tutti i file Wander (*.chrd *.hrd);;file Hrd (*.hrd);;file Chrd (*.chrd)</translation>
        </message>
    </context>
    <context>
        <name>CWanderZoomer</name>
        <message utf8="true">
            <source>Tap twice to define the rectangle</source>
            <translation>Toccare due volte per definire il rettangolo</translation>
        </message>
    </context>
    <context>
        <name>ui::CLoadWizbangProfileWidget</name>
        <message utf8="true">
            <source>Delete All</source>
            <translation>Elimina tutto</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>Elimina</translation>
        </message>
        <message utf8="true">
            <source>Load Profile</source>
            <translation>Carica profilo</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete %1?</source>
            <translation>Eliminare %1?</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to delete all %1 profiles?&#xA;&#xA;This operation cannot be undone.</source>
            <translation>Eliminare tutti i profili di %1?&#xA;&#xA;Questa operazione non può essere annullata.</translation>
        </message>
        <message utf8="true">
            <source>(Read-only files will not be deleted.)</source>
            <translation>(I file di sola lettura non saranno cancellati.)</translation>
        </message>
        <message utf8="true">
            <source>%1 Profiles (*%2.%3)</source>
            <translation>%1 Profili (*%2.%3)</translation>
        </message>
    </context>
    <context>
        <name>ui::CMetaWizardView</name>
        <message utf8="true">
            <source>Unable to load the profile.</source>
            <translation>Impossibile caricare il profilo.</translation>
        </message>
        <message utf8="true">
            <source>Load failed</source>
            <translation>Impossibile eseguire il carico</translation>
        </message>
    </context>
    <context>
        <name>ui::CWfproxyMessageDialog</name>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Successivo</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Messaggio</translation>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Errore</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardConfirmationDialog</name>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionChoicePanel</name>
        <message utf8="true">
            <source>Go</source>
            <translation>Vada</translation>
        </message>
        <message utf8="true">
            <source>Warning</source>
            <translation>Avviso</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardDecisionPage</name>
        <message utf8="true">
            <source>What do you want to do next?</source>
            <translation>Scegliere un'opzione per proseguire</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardExitDialog</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Are you sure you want to exit?</source>
            <translation>Uscire?</translation>
        </message>
        <message utf8="true">
            <source>Restore Setups on Exit</source>
            <translation>Ripristina configurazioni all'uscita</translation>
        </message>
        <message utf8="true">
            <source>Exit to Results</source>
            <translation>Esci e passa ai risultati</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardFooterWidget</name>
        <message utf8="true">
            <source>Exit</source>
            <translation>Esci</translation>
        </message>
        <message utf8="true">
            <source>Back</source>
            <translation>Indietro</translation>
        </message>
        <message utf8="true">
            <source>Step-by-step:</source>
            <translation>Un passo alla volta</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>Successivo</translation>
        </message>
        <message utf8="true">
            <source>Guide Me</source>
            <translation>Guidami</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardGoToDialog</name>
        <message utf8="true">
            <source>Results</source>
            <translation>Risultati</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Altra Porta</translation>
        </message>
        <message utf8="true">
            <source>Start Over</source>
            <translation>Ricomincia</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annulla</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardHeaderWidget</name>
        <message utf8="true">
            <source>Go To...</source>
            <translation>Vai a...</translation>
        </message>
        <message utf8="true">
            <source>Other Port</source>
            <translation>Altra Porta</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLoadWizbangProfilePage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Il test si sta avviando, attendere...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Chiusura test in corso, attendere...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardLogDialog</name>
        <message utf8="true">
            <source>Message Log</source>
            <translation>Registro messaggi</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMainPage</name>
        <message utf8="true">
            <source>Main</source>
            <translation>Principale</translation>
        </message>
        <message utf8="true">
            <source>Show Steps</source>
            <translation>Mostra passi</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardMessageDialog</name>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
        <message utf8="true">
            <source>Response: </source>
            <translation>Risposta: </translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardProgressBar</name>
        <message utf8="true">
            <source>Running</source>
            <translation>In esecuzione</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardReportLogoWidget</name>
        <message utf8="true">
            <source>None selected...</source>
            <translation>Nessuna selezione...</translation>
        </message>
        <message utf8="true">
            <source>Report Logo</source>
            <translation>Logo del report</translation>
        </message>
        <message utf8="true">
            <source>Clear</source>
            <translation>Cancella</translation>
        </message>
        <message utf8="true">
            <source>Select logo...</source>
            <translation>Seleziona logo ...</translation>
        </message>
        <message utf8="true">
            <source>Preview not available.</source>
            <translation>Anteprima non disponibile.</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardSplashScreenPage</name>
        <message utf8="true">
            <source>Test is initializing, please wait...</source>
            <translation>Il test si sta avviando, attendere...</translation>
        </message>
        <message utf8="true">
            <source>Test is shutting down, please wait...</source>
            <translation>Chiusura test in corso, attendere...</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusDialog</name>
        <message utf8="true">
            <source>Test is in progress...</source>
            <translation>Test in corso...</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Chiudi</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardStatusPresenter</name>
        <message utf8="true">
            <source>Time remaining:</source>
            <translation>Tempo rimanente:</translation>
        </message>
        <message utf8="true">
            <source>Not Running</source>
            <translation>Nessun test in esecuzione</translation>
        </message>
        <message utf8="true">
            <source>Test Incomplete</source>
            <translation>Test incompleto</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>Test completato</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test interrotto</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizardView</name>
        <message utf8="true">
            <source>Turning OFF Automatic Reports before starting script.</source>
            <translation>Disattivazione dei rapporti automatici prima di avviare lo script.</translation>
        </message>
        <message utf8="true">
            <source>Turning ON previously disabled Automatic Reports.</source>
            <translation>Attivazione dei rapporti automatici precedentemente disabilitati.</translation>
        </message>
        <message utf8="true">
            <source>Main</source>
            <translation>Principale</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved</source>
            <translation>Screenshot salvato</translation>
        </message>
        <message utf8="true">
            <source>Screenshot Saved:</source>
            <translation>Screenshot salvato:</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTestWorkspaceView</name>
        <message utf8="true">
            <source>Profile selection</source>
            <translation>Selezione del profilo</translation>
        </message>
        <message utf8="true">
            <source>Operating layer</source>
            <translation>Layer operativo</translation>
        </message>
        <message utf8="true">
            <source>Load a saved profile</source>
            <translation>Carica un profilo salvato</translation>
        </message>
        <message utf8="true">
            <source>How would you like to configure TrueSAM?</source>
            <translation>Come si vuole configurare TrueSAM?</translation>
        </message>
        <message utf8="true">
            <source>Load configurations from a saved profile</source>
            <translation>Carica configurazioni da un profilo salvato</translation>
        </message>
        <message utf8="true">
            <source>Go</source>
            <translation>Vada</translation>
        </message>
        <message utf8="true">
            <source>Start a new profile</source>
            <translation>Avvia un nuovo profilo</translation>
        </message>
        <message utf8="true">
            <source>What layer does your service operate on?</source>
            <translation>Su quale layer opera il servizio?</translation>
        </message>
        <message utf8="true">
            <source>Layer 2: Test using MAC addresses, eg 00:80:16:8A:12:34</source>
            <translation>Layer 2: ﻿Test con gli indirizzi MAC, ad esempio 00:80:16:8A:12:34</translation>
        </message>
        <message utf8="true">
            <source>Layer 3: Test using IP addresses, eg 192.168.1.9</source>
            <translation>Layer 3: ﻿Test con gli indirizzi IP, ad esempio 192.168.1.9</translation>
        </message>
    </context>
    <context>
        <name>ui::CWizbangTransitionScreen</name>
        <message utf8="true">
            <source>Please wait...going to highlighted step.</source>
            <translation>Attendere prego ... passaggio al passo evidenziato.</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configurazione</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Seleziona i test</translation>
        </message>
        <message utf8="true">
            <source>Establish Communications</source>
            <translation>Connessione in corso</translation>
        </message>
        <message utf8="true">
            <source>Configure Enhanced RFC 2544</source>
            <translation>Configura Enahnced RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Configure SAMComplete</source>
            <translation>Configura SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Configure J-Proof</source>
            <translation>Configura J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSpeed</source>
            <translation>Configura TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Save Configuration</source>
            <translation>Salva configurazione</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Test</translation>
        </message>
        <message utf8="true">
            <source>Add Report Info</source>
            <translation>Aggiungi informazioni report</translation>
        </message>
        <message utf8="true">
            <source>Run Selected Tests</source>
            <translation>Esegui test selezionati</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Report</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Visualizza rapporto</translation>
        </message>
    </context>
</TS>
