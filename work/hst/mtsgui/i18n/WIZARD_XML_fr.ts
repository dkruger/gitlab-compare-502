<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>WIZARD_XML</name>
        <message utf8="true">
            <source>CPRI Check</source>
            <translation>CPRI Check</translation>
        </message>
        <message utf8="true">
            <source>Configure</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Edit Previous Configuration</source>
            <translation>Editer configuration précédente</translation>
        </message>
        <message utf8="true">
            <source>Load Configuration from a Profile</source>
            <translation>Charger la configuration à partir d'un profile</translation>
        </message>
        <message utf8="true">
            <source>Start a New Configuration (reset to defaults)</source>
            <translation>Démarrer une nouvelle configuration (par défaut)</translation>
        </message>
        <message utf8="true">
            <source>Manually</source>
            <translation>Manuellement</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually</source>
            <translation>Configuration des réglages de test manuellement</translation>
        </message>
        <message utf8="true">
            <source>Test Settings</source>
            <translation>Paramétrages de test</translation>
        </message>
        <message utf8="true">
            <source>Save Profiles</source>
            <translation>Sauvegarde des profiles</translation>
        </message>
        <message utf8="true">
            <source>End: Configure Manually</source>
            <translation>Fin : Configure manuellement</translation>
        </message>
        <message utf8="true">
            <source>Run Tests</source>
            <translation>Lancer tests</translation>
        </message>
        <message utf8="true">
            <source>Stored</source>
            <translation>Stocké</translation>
        </message>
        <message utf8="true">
            <source>Load Profiles</source>
            <translation>Charger profiles</translation>
        </message>
        <message utf8="true">
            <source>End: Load Profiles</source>
            <translation>Fin : Charger profiles</translation>
        </message>
        <message utf8="true">
            <source>Edit Configuration</source>
            <translation>Editer Configuration</translation>
        </message>
        <message utf8="true">
            <source>Test</source>
            <translation>Essai</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>Exécuter</translation>
        </message>
        <message utf8="true">
            <source>Run CPRI Check</source>
            <translation>Exécuter vérification CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Verification</source>
            <translation>Vérification SFP</translation>
        </message>
        <message utf8="true">
            <source>End: Test</source>
            <translation>Fin : Test</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>Créer Rapport</translation>
        </message>
        <message utf8="true">
            <source>Repeat Test</source>
            <translation>Répéter test</translation>
        </message>
        <message utf8="true">
            <source>View Detailed Results</source>
            <translation>Visualiser les résultats détaillés</translation>
        </message>
        <message utf8="true">
            <source>Exit CPRI Check</source>
            <translation>Quitter Vérification CPRI</translation>
        </message>
        <message utf8="true">
            <source>Review</source>
            <translation>Examen</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>Résultats</translation>
        </message>
        <message utf8="true">
            <source>SFP</source>
            <translation>SFP</translation>
        </message>
        <message utf8="true">
            <source>Interface</source>
            <translation>Interface</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>Couche 2</translation>
        </message>
        <message utf8="true">
            <source>RTD</source>
            <translation>RTD</translation>
        </message>
        <message utf8="true">
            <source>BERT</source>
            <translation>BERT</translation>
        </message>
        <message utf8="true">
            <source>End: Review Results</source>
            <translation>Fin: Résultats de l'examen</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Rapport</translation>
        </message>
        <message utf8="true">
            <source>Report Info</source>
            <translation>Information rapport</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report</source>
            <translation>Fin : Créer un autre rapport</translation>
        </message>
        <message utf8="true">
            <source>Review Detailed Results</source>
            <translation>Examen des résultats détaillés</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>en cours</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>Test annulé</translation>
        </message>
        <message utf8="true">
            <source>INCOMPLETE</source>
            <translation>INCOMPLET</translation>
        </message>
        <message utf8="true">
            <source>COMPLETE</source>
            <translation>COMPLET</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>ECHEC</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>AUCUN</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check:</source>
            <translation>CPRI Check</translation>
        </message>
        <message utf8="true">
            <source>*** Starting CPRI Check ***</source>
            <translation>***démarrage vérification CPRI***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Finished ***</source>
            <translation>*** Test Terminé ***</translation>
        </message>
        <message utf8="true">
            <source>*** Test Aborted ***</source>
            <translation>*** Test Annulé ***</translation>
        </message>
        <message utf8="true">
            <source>Skip Save Profiles</source>
            <translation>Passer la sauvegarde de profiles</translation>
        </message>
        <message utf8="true">
            <source>You may save the configuration used to run this test.&#xA;&#xA;It may be used (in whole or by selecting individual&#xA;"profiles") to configure future tests.</source>
            <translation>Vous pouvez sauvegarder la configuration utilisée pour exécuter ce test.&#xA;&#xA;Elle pourrait être utilisée (dans son intégralité ou en sélectionnant les&#xA;"profiles" individuels) pour configurer des tests futures.</translation>
        </message>
        <message utf8="true">
            <source>Skip Load Profiles</source>
            <translation>Passer profiles de charge</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Verification</source>
            <translation>Vérification locale SFP</translation>
        </message>
        <message utf8="true">
            <source>Connector</source>
            <translation>Connecteur</translation>
        </message>
        <message utf8="true">
            <source>SFP1</source>
            <translation>SFP1</translation>
        </message>
        <message utf8="true">
            <source>SFP2</source>
            <translation>SFP2</translation>
        </message>
        <message utf8="true">
            <source>Please insert an SFP.</source>
            <translation>Veuillez insérer un SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Wavelength (nm)</source>
            <translation>Longueur d'Onde SFP (nm)</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor</source>
            <translation>Vendeur SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor Rev</source>
            <translation>Rev Vendeur SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP Vendor P/N</source>
            <translation>P/N vendeur SFP</translation>
        </message>
        <message utf8="true">
            <source>Recommended Rates</source>
            <translation>Taux recommandé</translation>
        </message>
        <message utf8="true">
            <source>Show Additional SFP Data</source>
            <translation>Montrer données supplémentaires SFP</translation>
        </message>
        <message utf8="true">
            <source>SFP is good.</source>
            <translation>SFP est bon</translation>
        </message>
        <message utf8="true">
            <source>Unable to verify SFP for this rate.</source>
            <translation>Impossible de vérifier SFP pour ce taux</translation>
        </message>
        <message utf8="true">
            <source>SFP is not acceptable.</source>
            <translation>SFP n'est pas acceptable</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>Durée du test </translation>
        </message>
        <message utf8="true">
            <source>Far-end Device</source>
            <translation>Appareil Far-end</translation>
        </message>
        <message utf8="true">
            <source>ALU</source>
            <translation>ALU</translation>
        </message>
        <message utf8="true">
            <source>Ericsson</source>
            <translation>Ericsson</translation>
        </message>
        <message utf8="true">
            <source>Other</source>
            <translation>Autre</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop</source>
            <translation>Boucle dure</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Non</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Max. Limit (dBm)</source>
            <translation>Limite max de niveau Rx Optique (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level Min. Limit (dBm)</source>
            <translation>Limite min de niveau Rx optique (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Max. Limit (us)</source>
            <translation>Limite max de Latence en boucle (us)</translation>
        </message>
        <message utf8="true">
            <source>Skip CPRI Check</source>
            <translation>Ignorer vérification CPRI</translation>
        </message>
        <message utf8="true">
            <source>SFP Check</source>
            <translation>SFP Check</translation>
        </message>
        <message utf8="true">
            <source>Test Status Key</source>
            <translation>Clé de statut de test</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Pass</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Fail</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Scheduled</source>
            <translation>Planifié</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Test</source>
            <translation>Lancer&#xA;test</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Test</source>
            <translation>Arrêter&#xA;Test</translation>
        </message>
        <message utf8="true">
            <source>Local SFP Results</source>
            <translation>Résultats locaux SFP</translation>
        </message>
        <message utf8="true">
            <source>No SFP is detected.</source>
            <translation>aucun SFP détecté</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm)</source>
            <translation>Longueur d'Onde (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Tx Level (dBm)</source>
            <translation>Niveau Tx Max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Max Rx Level (dBm)</source>
            <translation>Niveau Rx Max (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Transceiver</source>
            <translation>Émetteur</translation>
        </message>
        <message utf8="true">
            <source>Interface Results</source>
            <translation>Résultats d'interface</translation>
        </message>
        <message utf8="true">
            <source>CPRI Check Test Verdicts</source>
            <translation>Verdicts de test de vérification CPRI</translation>
        </message>
        <message utf8="true">
            <source>Interface Test</source>
            <translation>Test d'interface</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Test</source>
            <translation>Test couche 2</translation>
        </message>
        <message utf8="true">
            <source>RTD Test</source>
            <translation>RTD Test</translation>
        </message>
        <message utf8="true">
            <source>BERT Test</source>
            <translation>BERT Test</translation>
        </message>
        <message utf8="true">
            <source>Signal Present</source>
            <translation>Signal présent</translation>
        </message>
        <message utf8="true">
            <source>Sync Acquired</source>
            <translation>Acquisition de Synchro</translation>
        </message>
        <message utf8="true">
            <source>Rx Freq Max Deviation (ppm)</source>
            <translation>Déviation Max de Fréq en Rx (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>Violation de Code</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx Level (dBm)</source>
            <translation>Niveau du Récepteur Optique (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Results</source>
            <translation>Résultats couche 2</translation>
        </message>
        <message utf8="true">
            <source>Start-up State</source>
            <translation>État de démarrage</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync</source>
            <translation>Synchro trame</translation>
        </message>
        <message utf8="true">
            <source>RTD Results</source>
            <translation>RTD Results</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Avg (us)</source>
            <translation>Moyenne Latence en boucle (us)</translation>
        </message>
        <message utf8="true">
            <source>BERT Results</source>
            <translation>BERT Results</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync</source>
            <translation>Sync séq.</translation>
        </message>
        <message utf8="true">
            <source>Pattern Losses</source>
            <translation>Pertes de séquences</translation>
        </message>
        <message utf8="true">
            <source>Bit/TSE Errors</source>
            <translation>Erreurs Bit/TSE</translation>
        </message>
        <message utf8="true">
            <source>Configurations</source>
            <translation>Configurations</translation>
        </message>
        <message utf8="true">
            <source>Equipment Type</source>
            <translation>Type d'équipement</translation>
        </message>
        <message utf8="true">
            <source>L1 Synchronization</source>
            <translation>Synchronisation L1 </translation>
        </message>
        <message utf8="true">
            <source>Protocol Setup</source>
            <translation>Configuration du protocole</translation>
        </message>
        <message utf8="true">
            <source>C&amp;M Plane Setup</source>
            <translation>C&amp;M Configuration de plan</translation>
        </message>
        <message utf8="true">
            <source>Operation</source>
            <translation>Opération</translation>
        </message>
        <message utf8="true">
            <source>Passive Link</source>
            <translation>Lien passif</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Creation</source>
            <translation>Passer la création de rapports</translation>
        </message>
        <message utf8="true">
            <source>Test Report Information</source>
            <translation>Information de rapport du test</translation>
        </message>
        <message utf8="true">
            <source>Customer Name:</source>
            <translation>Nom du client :</translation>
        </message>
        <message utf8="true">
            <source>Technician ID:</source>
            <translation>Technicien ID:</translation>
        </message>
        <message utf8="true">
            <source>Test Location:</source>
            <translation>Localisation du Test :</translation>
        </message>
        <message utf8="true">
            <source>Work Order:</source>
            <translation>Commande de travail:</translation>
        </message>
        <message utf8="true">
            <source>Comments/Notes:</source>
            <translation>Commentaires/Remarques:</translation>
        </message>
        <message utf8="true">
            <source>Radio:</source>
            <translation>Radio:</translation>
        </message>
        <message utf8="true">
            <source>Band:</source>
            <translation>Bande:</translation>
        </message>
        <message utf8="true">
            <source>Overall Status</source>
            <translation>Etat global</translation>
        </message>
        <message utf8="true">
            <source>Enhanced FC Test</source>
            <translation>Test FC amélioré</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC Test</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Connecte</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>Symétrie</translation>
        </message>
        <message utf8="true">
            <source>Local Settings</source>
            <translation>Réglages locaux</translation>
        </message>
        <message utf8="true">
            <source>Connect to Remote</source>
            <translation>Connexion au distant</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Réseau</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Settings</source>
            <translation>Réglages Fibre Channel</translation>
        </message>
        <message utf8="true">
            <source>FC Tests</source>
            <translation>Tests FC</translation>
        </message>
        <message utf8="true">
            <source>Configuration Templates</source>
            <translation>Configuration de gabarit</translation>
        </message>
        <message utf8="true">
            <source>Select Tests</source>
            <translation>Sélectionner tests</translation>
        </message>
        <message utf8="true">
            <source>Utilization</source>
            <translation>utilisation</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>Longueurs de Trame</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>Test du "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>Test "Trame perdue"</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test</source>
            <translation>Retour vers Test de retour</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>Test de Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Test Ctls</source>
            <translation>Test Ctls</translation>
        </message>
        <message utf8="true">
            <source>Test Durations</source>
            <translation>La durée de l'essai</translation>
        </message>
        <message utf8="true">
            <source>Test Thresholds</source>
            <translation>Seuils d'essai</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration</source>
            <translation>Changer configuration</translation>
        </message>
        <message utf8="true">
            <source>Advanced Fibre Channel Settings</source>
            <translation>Paramétrages avancés de canal de fibre</translation>
        </message>
        <message utf8="true">
            <source>Advanced Utilization Settings</source>
            <translation>Paramétrages avancés d'utilisation</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Latency Settings</source>
            <translation>Paramétrages avancés de latence de débit</translation>
        </message>
        <message utf8="true">
            <source>Advanced Back to Back Test Settings</source>
            <translation>Paramétrages avancés de retour vers test de retour</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Test Settings</source>
            <translation>Paramétrages avancés de test de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Run Service Activation Tests</source>
            <translation>Lancer les tests d'activation de service</translation>
        </message>
        <message utf8="true">
            <source>Change Configuration and Rerun Test</source>
            <translation>Changer configuration et lancer test</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>Latence</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>Trames Perdues</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>Buffer Credit</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Exit FC Test</source>
            <translation>Quitter le test FC</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report</source>
            <translation>Créer un autre rapport</translation>
        </message>
        <message utf8="true">
            <source>Cover Page</source>
            <translation>Page de couverture</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Local unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>La source de synchronisation du délai dans un sens a été perdue pendant le test sur l'unité locale.  Veuillez vérifier les connexions pour le matériel source de synchronisation du délai dans un sens.  Le test va continuer s'il n'est pas complété, toutefois les résultats du délai de trame ne seront pas disponibles.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost during the test on the Remote unit.  Please check the connections for OWD Time Source hardware.  The test will continue if not completed, however Frame Delay results will be unavailable.</source>
            <translation>La source de synchronisation du délai dans un sens a été perdue pendant le test sur l'unité distante.  Veuillez vérifier les connexions pour le matériel source de synchronisation du délai dans un sens.  Le test va continuer s'il n'est pas complété, toutefois les résultats du délai de trame ne seront pas disponibles.</translation>
        </message>
        <message utf8="true">
            <source>Active Loop Found</source>
            <translation>Activer boucle trouvée</translation>
        </message>
        <message utf8="true">
            <source>Neighbor address resolution not successful.</source>
            <translation>Echec de résolution d'adresse voisine.</translation>
        </message>
        <message utf8="true">
            <source>Service #1: Sending ARP request for destination MAC.</source>
            <translation>Service #1 : Envoi d'une demande ARP pour le MAC de destination.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Local side: Sending ARP request for destination MAC.</source>
            <translation>Service #1 sur le côté local : Envoie d'une requête ARP pour le MAC de destination.</translation>
        </message>
        <message utf8="true">
            <source>Service #1 on the Remote side: Sending ARP request for destination MAC.</source>
            <translation>Service #1 sur le côté distant : Envoie d'une requête ARP pour le MAC de destination.</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>Envoi d'un ARP Request pour l'adresse MAC Destination.</translation>
        </message>
        <message utf8="true">
            <source>Local side sending ARP request for destination MAC.</source>
            <translation>Le côté local envoie une demande ARP pour le MAC de destination.</translation>
        </message>
        <message utf8="true">
            <source>Remote side sending ARP request for destination MAC.</source>
            <translation>Le côté distant envoie des requêtes ARP pour le MAC de destination.</translation>
        </message>
        <message utf8="true">
            <source>The network element port is provisioned for half duplex operation. If you would like to proceed press the "Continue in Half Duplex" button. Otherwise, press "Abort Test".</source>
            <translation>Le port d'élément du réseau est fourni pour un fonctionnement semi duplex. Si vous souhaitez continuer appuyez sur le bouton "Continuer en semi duplex". Sinon, appuyez sur "Annuler le test".</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up. Checking for an active loop.</source>
            <translation>Tente une boucle Vérification d'une boucle active</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop.</source>
            <translation>Vérification une boucle de matériel.</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>Vérification d'une boucle LBM/LBR.</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop.</source>
            <translation>Vérification d'une boucle permanente.</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameter request timed out. DHCP parameters could not be obtained.</source>
            <translation>Demande de paramètre DHCP expirée. Les paramètres DHCP n'ont pas pu être obtenus.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Loopback mode, you have been disconnected from the Remote unit.</source>
            <translation>En choisissant le mode de boucle retour vous avez été déconnecté de l'unité distante.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has timed out because a final received frame count cannot be determined. The measured received frame count has continued to increment unexpectedly.</source>
            <translation>SAMComplete a expiré parce qu'un comptage de dernière trame reçue ne peut pas être déterminé. Le comptage mesuré de la trame reçue a continué inopinément à compter.</translation>
        </message>
        <message utf8="true">
            <source>Hard Loop Found</source>
            <translation>Difficile Boucle Trouvée</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck cannot perform the traffic connectivity test unless a connection to the remote unit is established.</source>
            <translation>J-QuickCheck ne peut pas exécuter le test de connectivité de trafic sauf si une connexion à l'unité distante est établie.</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop Found</source>
            <translation>LBM/ Boucle LBR trouvée</translation>
        </message>
        <message utf8="true">
            <source>Local link has been lost and the connection to the remote unit has been severed. Once link is reestablished you may attempt to connect to the remote end again.</source>
            <translation>La liaison locale est perdue et la connexion à l'unité distante a été rompue. Une fois la liaison est ré-établie vous pouvez tenter de vous reconnecter à l'unité distante.</translation>
        </message>
        <message utf8="true">
            <source>Link is not currently active.</source>
            <translation>Le lien n'esr pas actif en ce moment.</translation>
        </message>
        <message utf8="true">
            <source>The source and destination IP are identical. Please reconfigure your source or destination IP address.</source>
            <translation>Les adresses IP sources et destination sont identiques. Veuillez reconfigurer votre adresse IP source ou destination.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Traffic application and the remote application at IP address #1 is a Streams application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application locale est une application de trafic et l'application distante à l'adresse IP #1 est une application de flux.</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established.</source>
            <translation>Aucune boucle n'a pu être établie.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit has not been established. Please go to the "Connect" page, verify your destination IP Address and then press the "Connect to Remote" button.</source>
            <translation>Une connexion à l'unité distante n'a pas été effectuée. Veuillez aller à la page "Connecter", vérifier votre adresse IP destination et puis presser le bouton "Connecter au distant".</translation>
        </message>
        <message utf8="true">
            <source>Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Veuillez aller sur la page 'Réseau', vérifiez les configurations et essayez de nouveau</translation>
        </message>
        <message utf8="true">
            <source>Waiting for the optic to become ready or an optic is not present.</source>
            <translation>En attente de l'optique pour être prêt ou un optique n'est pas présent.</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop Found</source>
            <translation>Boucle permanente trouvée</translation>
        </message>
        <message utf8="true">
            <source>PPPoE connection timeout. Please check your PPPoE settings and try again.</source>
            <translation>Dépassement de la connexion PPPoE. S'il vous plaît vérifier vos paramètres PPPoE et réessayez.</translation>
        </message>
        <message utf8="true">
            <source>An unrecoverable PPPoE error was encountered</source>
            <translation>Une erreur irrécupérable PPPoE a été rencontrée</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>Essai de connexion au serveur ...</translation>
        </message>
        <message utf8="true">
            <source>A problem with the remote connection was detected. The remote unit may no longer be accessible</source>
            <translation>Un problème avec la connexion à distance a été détecté. L'unité distante pourrait ne plus être accessible.</translation>
        </message>
        <message utf8="true">
            <source>A connection to the remote unit at IP address #1 could not be established. Please check your remote source IP Address and try again.</source>
            <translation>Une connexion à l'unité distante à l'adresse IP #1 n'a pas pu être effectuée. Veuillez vérifier l'adresse IP de votre source distante et réessayer.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is a Loopback application. It is not compatible with Enhanced RFC 2544.</source>
            <translation>L'application distante à l'adresse IP #1 est une application avec boucle de retour. Elle n'est pas compatible avec RFC 2544 amélioré.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The remote application at IP address #1 is not a TCP WireSpeed application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application distante à l'adresse IP #1 n'est pas une application de vitesse filaire TCP.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be newer. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on this unit for optimal performance.</source>
            <translation>La version du logiciel de l'appareil à distance semble être plus récente. Si vous continuez à tester, certaines fonctionnalités peuvent être limitées. Il est recommandé de mettre à niveau le logiciel de cet appareil pour des performances optimales.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit appears to be older. If you continue to test, some functionality may be limited. It is recommended to upgrade the software on the remote unit for optimal performance.</source>
            <translation>La version du logiciel de l'appareil à distance semble être plus ancienne. Si vous continuez à tester, certaines fonctionnalités peuvent être limitées. Il est recommandé de mettre à jour le logiciel de l'appareil à distance pour des performances optimales.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit could not be determined. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>La version du logiciel sur l'appareil distant n'a pas pu être déterminée. Si vous continuez à tester, certaines fonctionnalités peuvent être limitées. Il est recommandé de tester entre les unités avec les versions équivalentes de logiciel.</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful. Trying again.</source>
            <translation>La tentative de boucler avec retour n'a pas réussi Nouvel essai</translation>
        </message>
        <message utf8="true">
            <source>The settings for the selected template have been successfully applied.</source>
            <translation>Les paramètres du modèle sélectionné ont été appliqués avec succès.</translation>
        </message>
        <message utf8="true">
            <source>The version of software on the remote unit either could not be determined or does not match the version on this unit. If you continue to test, some functionality may be limited. It is recommended to test between units with equivalent versions of software.</source>
            <translation>La version du logiciel sur l'unité distante ne peut pas soit déterminer ou ne correspond pas à la version sur cette unité. Si vous continuez de tester, certaines fonctionnalités peuvent être limitées. Il est recommandé de tester entre les unités avec des versions équivalentes de logiciel.</translation>
        </message>
        <message utf8="true">
            <source>Explicit login was unable to complete.</source>
            <translation>Connexion explicite impossible à réaliser.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer #1 application and the remote application at IP address #2 is a Layer #3 application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application locales est une application de la couche #1 et l'application distante à l'adresse IP #2 est une application de la couche #3.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>Le taux de la ligne de l'unité locale (#1 Mbps) ne correspond pas à celle de l'unité à distance (#2 Mbps). S'il vous plaît reconfigurer à asymétrique sur la page "Connecter".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page.</source>
            <translation>Le taux de la ligne de l'unité locale (#1 kbps) ne correspond pas à celle de l'unité à distance (#2 kbps). S'il vous plaît reconfigurer à asymétrique sur la page "Connecter".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>Le taux de la ligne de l'unité locale (#1 Mbps) ne correspond pas à celle de l'unité à distance (#2 Mbps). S'il vous plaît reconfigurer à asymétrique sur la page "Connect" et redémarrez le test.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the "Connect" page and restart the test.</source>
            <translation>Le taux de la ligne de l'unité locale (#1 kbps) ne correspond pas à celle de l'unité à distance (#2 kbps). S'il vous plaît reconfigurer à asymétrique sur la page "Connect" et redémarrez le test.</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 Mbps) does not match that of the remote unit (#2 Mbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>Le taux de la ligne de l'unité locale (#1 Mbps) ne correspond pas à celle de l'unité à distance (#2 Mbps). S'il vous plaît reconfigurer à asymétrique sur la page RFC2544 "Symétrie".</translation>
        </message>
        <message utf8="true">
            <source>The local unit line rate (#1 kbps) does not match that of the remote unit (#2 kbps). Please reconfigure to Asymmetric on the RFC2544 "Symmetry" page.</source>
            <translation>Le taux de la ligne de l'unité locale (#1 kbps) ne correspond pas à celle de l'unité à distance (#2 kbps). S'il vous plaît reconfigurer à asymétrique sur la page RFC2544 "Symétrie".</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one test must be selected. Please select at least one test and try again.</source>
            <translation>Configuration invalide : &#xA;&#xA;Au moins un essai doit être sélectionnée. S'il vous plaît sélectionner au moins un essai et essayer à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>You have not selected any frame sizes to test. Please select at least one frame size before starting.</source>
            <translation>Vous n'avez pas sélectionné de taille de trame pour test. Veuillez sélectionner au moins une taille de trame avant de commencer.</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold was detected in the last second. Please verify the performance of the link with a manual traffic test. After you have done your manual tests you can rerun RFC 2544.&#xA;Recommended manual test configuration:&#xA;Frame Size: #1 bytes&#xA;Traffic: Constant with #2 Mbps&#xA;Test duration: At least 3 times the configured test duration. Throughput test duration was #3 seconds.&#xA;Results to monitor:&#xA;Use the Summary Status screen to look for error events. If the error counters are incrementing in a sporadic manner run the manual test at different lower traffic rates. If you still get errors even on lower rates the link may have general problems not related to maximum load. You can also use the Graphical Results Frame Loss Rate Cur graph to see if there are sporadic or constant frame loss events. If you cannot solve the problem with the sporadic errors you can set the Frame Loss Tolerance Threshold to tolerate small frame loss rates. Note: Once you use a Frame Loss Tolerance the test does not comply with the RFC 2544 recommendation.</source>
            <translation>Une débit de perte de trame qui dépasse le seuil de perte de trame configuré a été détecté dans la dernière seconde. Veuillez vérifier la performance du lien avec u  test manuel de trafic. Une fois que vous avez fait vos tests manuels vous pouvez ré-exécuter RFC 2544.&#xA;Configuration de test manuel recommandée:&#xA;Taille de trame:#1octets&#xA;Trafic:Constant avec #2 Mbps&#xA;Durée de test: Au moins 3 fois la durée de test configurée. La durée de test de seuil était #3 secondes.&#xA;Les résultats sur moniteur:&#xA;Utilisez l'écran d'état résumé pour rechercher des évènements d'erreur. Si les compteurs d'erreur augmentent de manière sporadique exécutez le test manul à différents débits de trafic inférieur. Si vous obtenez toujours des erreurs même sur débits inférieurs le lien peut avoir des problèmes généraux non relatifs à la charge maximale. Vous pouvez également utiliser le graphique de débit de perte de trame pour voir s'il y a des évènements d'erreur de trame constants ou sporadiques. Si vous ne pouvez pas résoudre le problème avec des erreurs sporadiques vous pouvez définir le Seuil de tolérance de perte de trame pour tolérer de petits débits de perte de trame. Remarque: Une fois que vous utilisez la tolérance de perte de trame, le test n'est pas conforme à la recommandation RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Note:  Due to differing VLAN stack depths for the transmitter and the receiver, the configured rate will be adjusted to compensate for the rate difference between the ports.</source>
            <translation>Remarque :  En raison de différentes profondeurs de pile VLAN pour l'émetteur et le récepteur, le débit configuré sera ajusté pour compenser la différence de débit entre les ports.</translation>
        </message>
        <message utf8="true">
            <source>#1 byte frames</source>
            <translation>Trames de #1 octets</translation>
        </message>
        <message utf8="true">
            <source>#1 byte packets</source>
            <translation>Paquets de #1 octets</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment over the last 10 seconds even though traffic should be stopped</source>
            <translation>La C2 filtre Rx Acterna compte trame a continué à augmenter au cours des 10 dernières secondes, même si le trafic doit être arrêté</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>Détermination du débit max par répétition</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 Mbps</source>
            <translation>Tentative #1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 Mbps</source>
            <translation>Tentative #1 C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L1 kbps</source>
            <translation>Tentative #1 C1 kpbs</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 L2 kbps</source>
            <translation>Tentative #1 C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Attempting #1 %</source>
            <translation>Essais #1 %</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 Mbps. This will take #2 seconds</source>
            <translation>En cours de vérification #1 C1 Mbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 Mbps. This will take #2 seconds</source>
            <translation>En cours de vérification #1 C2 Mbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L1 kbps. This will take #2 seconds</source>
            <translation>A présent vérification #1 C1 kbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 L2 kbps. This will take #2 seconds</source>
            <translation>En cours de vérification #1 C2 kbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Now verifying #1 %. This will take #2 seconds</source>
            <translation>En cours de vérification #1.  Cela va prendre #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 Mbps</source>
            <translation>Maximum débit mesuré: #1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 Mbps</source>
            <translation>Maximum débit mesuré: #1 C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L1 kbps</source>
            <translation>Débit maximum mesuré:#1 C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 L2 kbps</source>
            <translation>Maximum débit mesuré: #1 C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured: #1 %</source>
            <translation>Débit maximum mesuré : #1 %</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>Une mesure de débit maximum n'est pas disponible</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (RFC 2544 Standard)</source>
            <translation>trame test de perte (RFC 2544 standard)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Top Down)</source>
            <translation>trame test de perte ( Top Down )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test (Bottom Up)</source>
            <translation>trame test de perte ( Bottom Up )</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 Mbps load. This will take #2 seconds</source>
            <translation>Test en cours pour charge #1 C1 Mbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 Mbps load. This will take #2 seconds</source>
            <translation>Test en cours pour charge #1 C2 Mbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L1 kbps load. This will take #2 seconds</source>
            <translation>Test en exécution pour charge #1 C1 kbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 L2 kbps load. This will take #2 seconds</source>
            <translation>Test en cours pour charge #1 C2 kbps. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Running test at #1 % load. This will take #2 seconds</source>
            <translation>Lancement du test à #1 % de charge. Cela va prendre #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>Test de Trames en "Back to Back"</translation>
        </message>
        <message utf8="true">
            <source>Trial #1</source>
            <translation>Essais #1</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>Trame Pause détectées</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: fail</source>
            <translation>#1 Burst de paquet : echec</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: fail</source>
            <translation>#1 Burst de trame : echec</translation>
        </message>
        <message utf8="true">
            <source>#1 packet burst: pass</source>
            <translation>#1 Burst de paquet : passe</translation>
        </message>
        <message utf8="true">
            <source>#1 frame burst: pass</source>
            <translation>#1 Burst de trame : passe</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test</source>
            <translation>Test de recherche salve</translation>
        </message>
        <message utf8="true">
            <source>Attempting a burst of #1 kB</source>
            <translation>Tentative de salve de #1 kO</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 kB</source>
            <translation>Supérieure #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Less than #1 kB</source>
            <translation>Moins que #1 kO</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is #1 kB</source>
            <translation>La taille du buffer est #1 kO</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is less than #1 kB</source>
            <translation>La taille du buffer est inférieure à #1 kO</translation>
        </message>
        <message utf8="true">
            <source>The buffer size is greater than or equal to #1 kB</source>
            <translation>La taille du buffer est supérieure ou égale à #1 kO</translation>
        </message>
        <message utf8="true">
            <source>Sent #1 frames</source>
            <translation>Envoyer trames #1</translation>
        </message>
        <message utf8="true">
            <source>Received #1 frames</source>
            <translation>Trames Reçus #1</translation>
        </message>
        <message utf8="true">
            <source>Lost #1 frames</source>
            <translation>Perte de Trames #1</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS) Test</source>
            <translation>Test de salve (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: #1 kB</source>
            <translation>Estimée CBS : #1 kB</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS: Unavailable</source>
            <translation>CBS estimé: indisponible</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test burst size is too large to accurately test this configuration.</source>
            <translation>Le test CBS sera ignoré. La dimension de salve est trop grande pour tester avec précision cette configuration.</translation>
        </message>
        <message utf8="true">
            <source>The CBS test will be skipped. Test duration is not long enough to accurately test this configuration.</source>
            <translation>Le test CBS sera ignoré. La durée du test n'est pas assez long pour tester avec précision cette configuration.</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: fail</source>
            <translation>Rafale paquets : échec</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: fail</source>
            <translation>Rafale trames : échec</translation>
        </message>
        <message utf8="true">
            <source>Packet burst: pass</source>
            <translation>Rafale paquets : succès</translation>
        </message>
        <message utf8="true">
            <source>Frame burst: pass</source>
            <translation>Rafale trames : succès</translation>
        </message>
        <message utf8="true">
            <source>Burst Policing Trial #1</source>
            <translation>Essai de contrôle de rafale #1</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test</source>
            <translation>Test de récupération du système</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this packet size</source>
            <translation>Test n'est pas valide pour cette taille de paquet</translation>
        </message>
        <message utf8="true">
            <source>Test is invalid for this frame size</source>
            <translation>Test n'est pas valide pour cette taille d'image</translation>
        </message>
        <message utf8="true">
            <source>Trial #1 of #2:</source>
            <translation>Test #1 de #2 :</translation>
        </message>
        <message utf8="true">
            <source>It will not be possible to induce frame loss because the Throughput Test passed at maximum bandwidth with no frame loss observed</source>
            <translation>Il ne sera pas possible d'induire une perte de trame car le test de débit a réussi à un débit maximum avec aucune perte de trame observée</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this packet size</source>
            <translation>Impossible de déclencher tout évènement de perte. Le test est invalide pour cette taille de paquet</translation>
        </message>
        <message utf8="true">
            <source>Unable to induce any loss events. Test is invalid for this frame size</source>
            <translation>Impossible de déclencher tout évènement de perte. Le test est invalide pour cette taille de trame</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Greater than #1 seconds</source>
            <translation>Durée de récupération moyenne: Supérieure à #1 secondes</translation>
        </message>
        <message utf8="true">
            <source>Greater than #1 seconds</source>
            <translation>Supérieure #1 seconde</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: Unavailable</source>
            <translation>Durée de récupération moyenne:Indisponible</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery Time: #1 us</source>
            <translation>Durée de récupération moyenne:#1 us</translation>
        </message>
        <message utf8="true">
            <source>#1 us</source>
            <translation>#1 us</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer. Testing #1 credits</source>
            <translation>Mise à zéro en cours dans buffer de crédit optimal. Test en cours #1 crédits</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 credits</source>
            <translation>Test de #1 de credit</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with #1 credits.  This will take #2 seconds</source>
            <translation>Vérification avec un credit de #1. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>Test de Buffer Credit Throughput</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2. This test is invalid</source>
            <translation>Remarque : Suppose une boucle physique avec un crédit de mémoire tampon inférieur à 2. Ce test n'est pas valide</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice the number of buffer credits at each step to compensate for the double length of fibre</source>
            <translation>Note: Basé sur l'hypothèse d'une boucle dure, les mesures de débit sont mis au double du nombre de crédits de la mémoire tampon à chaque étape pour compenser pour la double longueur de fibre optique</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at #1 Buffer Credits</source>
            <translation>Mesure du débit au Buffer credit #1</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency Tests</source>
            <translation>Tests de débit et de latence</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>Tests de débit et de gigue de paquets</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency and Packet Jitter Tests</source>
            <translation>Débit, la latence et de gigue Packet Tests</translation>
        </message>
        <message utf8="true">
            <source>Latency and Packet Jitter trial #1. This will take #2 seconds</source>
            <translation>Essai #1 de latence et gigue de paquet. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test trial #1. This will take #2 seconds</source>
            <translation>Essai #1 de test de gigue de paquet. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1. This will take #2 seconds</source>
            <translation>Essai #1 de test de latence. Cela prendra #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>Latency Test trial #1 at #2% of verified throughput load. This will take #3 seconds</source>
            <translation>Essai #1 de test de latence à #2% de charge de débit vérifié. Cela prendra #3 secondes</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting RFC 2544 Test #2</source>
            <translation>#1 Test RFC 2544 démarrant #2</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting FC Test #2</source>
            <translation>#1 Test FC démarrant #2</translation>
        </message>
        <message utf8="true">
            <source>Test complete.</source>
            <translation>Test complété.</translation>
        </message>
        <message utf8="true">
            <source>Saving results, please wait.</source>
            <translation>Sauvegarde des résultats en cours, veuillez patienter.</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test</source>
            <translation>Test de charge prolongée</translation>
        </message>
        <message utf8="true">
            <source>FC Test:</source>
            <translation>FC Test:</translation>
        </message>
        <message utf8="true">
            <source>Network Configuration</source>
            <translation>Configuration du réseau</translation>
        </message>
        <message utf8="true">
            <source>Frame Type</source>
            <translation>Type de Trame</translation>
        </message>
        <message utf8="true">
            <source>Test Mode</source>
            <translation>Mode Test</translation>
        </message>
        <message utf8="true">
            <source>Maint. Domain Level</source>
            <translation>Niveau du domaine de maint.</translation>
        </message>
        <message utf8="true">
            <source>Sender TLV</source>
            <translation>TLV émetteur</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>Encapsulation</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack Depth</source>
            <translation>Profondeur de la pile VLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>Bits de priorité SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN DEI Bit</source>
            <translation>Bit DEI du SVLAN</translation>
        </message>
        <message utf8="true">
            <source>SVLAN TPID (hex)</source>
            <translation>TPID SVLAN (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex)</source>
            <translation>TPID SVLAN (hex) utilisateur </translation>
        </message>
        <message utf8="true">
            <source>CVLAN ID</source>
            <translation>CVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>CVLAN User Priority</source>
            <translation>Priorité CVLAN choisie par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>Bits de priorité Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 ID</source>
            <translation>SVLAN 7 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 User Priority</source>
            <translation>SVLAN Bits de priorité 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 TPID (hex)</source>
            <translation>TPID SVLAN 7 (hex)</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 7 TPID (hex)</source>
            <translation>TPID SVLAN 7 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 7 DEI Bit</source>
            <translation>Bit DEI du SVLAN 7</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 ID</source>
            <translation>SVLAN 6 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 User Priority</source>
            <translation>SVLAN Bits de priorité 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 TPID (hex)</source>
            <translation>TPID SVLAN (hex) 6</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 6 TPID (hex)</source>
            <translation>TPID SVLAN 6 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 6 DEI Bit</source>
            <translation>Bit DEI du SVLAN 6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 ID</source>
            <translation>SVLAN 5 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 User Priority</source>
            <translation>SVLAN Bits de priorité 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 TPID (hex)</source>
            <translation>TPID SVLAN (hex) 5</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 5 TPID (hex)</source>
            <translation>TPID SVLAN 5 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 5 DEI Bit</source>
            <translation>Bit DEI du SVLAN 5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 ID</source>
            <translation>SVLAN 4 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 User Priority</source>
            <translation>SVLAN Bits de priorité 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 TPID (hex)</source>
            <translation>TPID SVLAN (hex) 4</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 4 TPID (hex)</source>
            <translation>TPID SVLAN 4 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 4 DEI Bit</source>
            <translation>Bit DEI du SVLAN 4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 ID</source>
            <translation>SVLAN 3 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 User Priority</source>
            <translation>SVLAN Bits de priorité 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 TPID (hex)</source>
            <translation>TPID SVLAN (hex) 3</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 3 TPID (hex)</source>
            <translation>TPID SVLAN 3 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 3 DEI Bit</source>
            <translation>Bit DEI du SVLAN 3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 ID</source>
            <translation>SVLAN 2 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 User Priority</source>
            <translation>SVLAN Bits de priorité 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 TPID (hex)</source>
            <translation>TPID SVLAN (hex) 2</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 2 TPID (hex)</source>
            <translation>TPID SVLAN 2 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 2 DEI Bit</source>
            <translation>Bit DEI du SVLAN 2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 ID</source>
            <translation>SVLAN 1 ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 User Priority</source>
            <translation>SVLAN Bits de priorité 1</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 TPID (hex)</source>
            <translation>TPID SVLAN (hex) 1</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN 1 TPID (hex)</source>
            <translation>TPID SVLAN 1 (hex) utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN 1 DEI Bit</source>
            <translation>Bit DEI du SVLAN 1</translation>
        </message>
        <message utf8="true">
            <source>Loop Type</source>
            <translation>Type de boucle</translation>
        </message>
        <message utf8="true">
            <source>EtherType</source>
            <translation>EtherType</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>MAC Source</translation>
        </message>
        <message utf8="true">
            <source>Auto-increment SA MAC</source>
            <translation>SA MAC auto-incrémenté</translation>
        </message>
        <message utf8="true">
            <source># MACs in Sequence</source>
            <translation>Numéro MAC en séquence</translation>
        </message>
        <message utf8="true">
            <source>Disable IP EtherType</source>
            <translation>Désactiver type Ethernet IP</translation>
        </message>
        <message utf8="true">
            <source>Disable OoS Results</source>
            <translation>Désactiver résultats OoS</translation>
        </message>
        <message utf8="true">
            <source>DA Type</source>
            <translation>Type de DA</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC</source>
            <translation>MAC Destination</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>Mode Données</translation>
        </message>
        <message utf8="true">
            <source>Use Authentication</source>
            <translation>utiliser l'authentification</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>Nom d'Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Mot de passe</translation>
        </message>
        <message utf8="true">
            <source>Service Provider</source>
            <translation>Fournisseur de service</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>Nom du service</translation>
        </message>
        <message utf8="true">
            <source>Source IP Type</source>
            <translation>Type d'IP Source</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address</source>
            <translation>Adresse IP Source</translation>
        </message>
        <message utf8="true">
            <source>Default Gateway</source>
            <translation>Gateway par défaut</translation>
        </message>
        <message utf8="true">
            <source>Subnet Mask</source>
            <translation>Masque de sous-réseau</translation>
        </message>
        <message utf8="true">
            <source>Destination IP Address</source>
            <translation>Adresse IP Destination</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Time to Live (hops)</source>
            <translation>Time To Live (bonds)</translation>
        </message>
        <message utf8="true">
            <source>IP ID Incrementing</source>
            <translation>Incrémentation d'ID IP</translation>
        </message>
        <message utf8="true">
            <source>Source Link-Local Address</source>
            <translation>Source adresse lien-local</translation>
        </message>
        <message utf8="true">
            <source>Source Global Address</source>
            <translation>Source adresses globale</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Longueur du préfixe de Sous-Réseau</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Adresse destination</translation>
        </message>
        <message utf8="true">
            <source>Traffic Class</source>
            <translation>Classe de Trafic</translation>
        </message>
        <message utf8="true">
            <source>Flow Label</source>
            <translation>Label de Flux</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit</source>
            <translation>Saut Limite</translation>
        </message>
        <message utf8="true">
            <source>Traffic Mode</source>
            <translation>Mode de Trafic</translation>
        </message>
        <message utf8="true">
            <source>Source Port Service Type</source>
            <translation>Source Port Type de service</translation>
        </message>
        <message utf8="true">
            <source>Source Port</source>
            <translation>Port Source</translation>
        </message>
        <message utf8="true">
            <source>Destination Port Service Type</source>
            <translation>Type de service du port de destination</translation>
        </message>
        <message utf8="true">
            <source>Destination Port</source>
            <translation>Port de Destination</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Type</source>
            <translation>Type d'IP écoute ATP</translation>
        </message>
        <message utf8="true">
            <source>ATP Listen IP Address</source>
            <translation>Adresse IP écoute ATP</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>ID Source</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>ID Destination</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>ID de Séquence</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>ID d'Origine</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>ID du répondeur</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>Configuration du test</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload Version</source>
            <translation>Acterna version charge utile</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Precision</source>
            <translation>Précision de mesure de latence</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Unit</source>
            <translation>Unité de largeur de bande</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (Mbps)</source>
            <translation>Largeur de bande de test max (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (Mbps)</source>
            <translation>Largeur de bande de test max en amont (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (kbps)</source>
            <translation>Largeur de bande de test max (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Test Bandwidth (kbps)</source>
            <translation>Largeur de bande de test max en amont (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (Mbps)</source>
            <translation>Largeur de bande de test max en aval (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Test Bandwidth (kbps)</source>
            <translation>Largeur de bande de test max en aval (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Test Bandwidth (%)</source>
            <translation>Largeur de bande de test max (%)</translation>
        </message>
        <message utf8="true">
            <source>Allow True 100% Traffic</source>
            <translation>Autoriser un trafic 100% véritable</translation>
        </message>
        <message utf8="true">
            <source>Throughput Measurement Accuracy</source>
            <translation>Débit Précision de mesure</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Measurement Accuracy</source>
            <translation>Précision de mesure de débit en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Measurement Accuracy</source>
            <translation>Précision de mesure de débit en aval</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>Processus de mise à zéro du débit</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance (%)</source>
            <translation>Tol. du trame perdues Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolérance de perte de trame de débit en amont (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Frame Loss Tolerance (%)</source>
            <translation>Tolérance de perte de trame de débit en aval (%)</translation>
        </message>
        <message utf8="true">
            <source>All Tests Duration (s)</source>
            <translation>Durée(s) de tous les tests</translation>
        </message>
        <message utf8="true">
            <source>All Tests Number of Trials</source>
            <translation>Nombre de tous les tests d'essais</translation>
        </message>
        <message utf8="true">
            <source>Throughput Duration (s)</source>
            <translation>Débit Durée (s)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>Seuil du test "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (Mbps)</source>
            <translation>Seuil de passage Débit (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (Mbps)</source>
            <translation>Seuil de passage de débit en amont (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (kbps)</source>
            <translation>Seuil de passage Débit (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Pass Threshold (kbps)</source>
            <translation>Seuil de passage de débit en amont (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (Mbps)</source>
            <translation>Seuil de passage de débit en aval (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Pass Threshold (kbps)</source>
            <translation>Seuil de passage de débit en aval (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold (%)</source>
            <translation>Seuil du test Throughput (%)</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency Trials</source>
            <translation>Nombre d'essais de latence</translation>
        </message>
        <message utf8="true">
            <source>Latency Trial Duration (s)</source>
            <translation>Durée(s) d'essai de latence</translation>
        </message>
        <message utf8="true">
            <source>Latency Bandwidth (%)</source>
            <translation>Largeur de bande de délai (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold</source>
            <translation>Seuil de passage de latence</translation>
        </message>
        <message utf8="true">
            <source>Latency Pass Threshold (us)</source>
            <translation>Seuil de passage de latence (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Pass Threshold (us)</source>
            <translation>Seuil (us) de passage de latence en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Pass Threshold (us)</source>
            <translation>Seuil (us) de passage de latence en aval</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>Nombre d'essais de Gigue de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration (s)</source>
            <translation>Durée (s) d'essai de gigue de paquet</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>Seuil de réussite Gigue de Paquets</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold (us)</source>
            <translation>Seuil de passage de gigue de paquet (us)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Packet Jitter Pass Threshold (us)</source>
            <translation>Seuil (us) de passage de gigue de paquet en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Packet Jitter Pass Threshold (us)</source>
            <translation>Seuil (us) de passage de gigue de paquet en aval</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>Procédure de Test de Perte de Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration (s)</source>
            <translation>trame Perte première instance Durée (s)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Perte trame granularité de bande passante ( Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularité de largeur de bande de perte de trame en amont (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Perte trame granularité de bande passante ( kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularité de largeur de bande de perte de trame en amont (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (kbps)</source>
            <translation>Granularité de largeur de bande de perte de trame en aval (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Bandwidth Granularity (Mbps)</source>
            <translation>Granularité de largeur de bande de perte de trame en aval (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity (%)</source>
            <translation>Gran. Bande passante Frame Loss (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (Mbps)</source>
            <translation>trame perte Range minimum ( Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (kbps)</source>
            <translation>trame perte Range minimum ( kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Minimum (%)</source>
            <translation>trame perte Range minimale (% )</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (Mbps)</source>
            <translation>Perte trame de portée maximale (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (kbps)</source>
            <translation>Perte trame de portée maximale (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Range Maximum (%)</source>
            <translation>Perte trame de portée maximale (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Minimum d'échelle de perte de trame en amont (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Minimum (kbps)</source>
            <translation>Minimum d'échelle de perte de trame en amont (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Maximum d'échelle de perte de trame en amont (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Range Maximum (kbps)</source>
            <translation>Maximum d'échelle de perte de trame en amont (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (Mbps)</source>
            <translation>Minimum d'échelle de perte de trame en aval (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Minimum (kbps)</source>
            <translation>Minimum d'échelle de perte de trame en aval (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (Mbps)</source>
            <translation>Maximum d'échelle de perte de trame en aval (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Range Maximum (kbps)</source>
            <translation>Maximum d'échelle de perte de trame en aval (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Number of Steps</source>
            <translation>Perte trame Nombre d'étapes</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Number of Trials</source>
            <translation>Retour vers nombre d'essais de retour</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity (Frames)</source>
            <translation>Retour à granularité de retour (trames)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Burst Duration (s)</source>
            <translation>Retour à durée (s) de salve max de retour</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Max Burst Duration (s)</source>
            <translation>Retour en amont vers Durée (s) de salve max de retour</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Max Burst Duration (s)</source>
            <translation>Retour en aval vers Durée (s) de salve max de retour</translation>
        </message>
        <message utf8="true">
            <source>Ignore Pause Frames</source>
            <translation>Ignorer les trames de pause</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type</source>
            <translation>Type de test de salve</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Size (kB)</source>
            <translation>Taille de salve CBS (kO)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst CBS Size (kB)</source>
            <translation>Taille CBS de salve en amont (kO)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst CBS Size (kB)</source>
            <translation>Taille CBS de salve en aval (kO)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Min Size (kB)</source>
            <translation>Taille min de recherche salve (kO)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Max Size (kB)</source>
            <translation>Taille max de recherche salve (kO)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Min Size (kB)</source>
            <translation>Taille min de recherche salve en amont (kO)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Max Size (kB)</source>
            <translation>Taille max de recherche salve en amont (kO)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Min Size (kB)</source>
            <translation>Taille min de recherche salve en aval (kO)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Max Size (kB)</source>
            <translation>Taille max de recherche salve en aval (kO)</translation>
        </message>
        <message utf8="true">
            <source>CBS Duration (s)</source>
            <translation>Durée (s) CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Number of Trials</source>
            <translation>Nombre de test de salve d'esssais</translation>
        </message>
        <message utf8="true">
            <source>Burst CBS Show Pass/Fail</source>
            <translation>Afficher passage/échec salve CBS</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Show Pass/Fail</source>
            <translation>Afficher passage/échec de recherche salve</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size Threshold (kB)</source>
            <translation>Seuil de taille de recherche salve (kO)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Size Threshold (kB)</source>
            <translation>Seuil de taille de recherche salve en amont (kO)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Size Threshold (kB)</source>
            <translation>Seuil de taille de recherche salve en aval (kO)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Number of Trials</source>
            <translation>System Recovery Nombre d'essais</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Overload Duration (s)</source>
            <translation>System Recovery surcharge Durée (s)</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Duration (s)</source>
            <translation>Durée(s) de charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Throughput Scaling (%)</source>
            <translation>Echelonnage (%) de seuil de charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Packet Length</source>
            <translation>Longueur de paquet de charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Frame Length</source>
            <translation>Longueur de trame de charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Login Type</source>
            <translation>Type de connexion crédit buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Max Buffer Size</source>
            <translation>Taille de buffer max de crédit buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Steps</source>
            <translation>Pas de début de crédit buffer</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Duration</source>
            <translation>Durée crédit buffer</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of LLB Mode</source>
            <translation>Boucle distante arrêtée avec succès : Unité **#1** hors du mode de boucle</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Successful:Unit **#1** Out of Transparent LLB Mode</source>
            <translation>Fin de Bouclage Distant Réussi : Equipement **#1** Sorti du Mode LLB Transparent</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already Looped Down</source>
            <translation>L'équipement distant **#1** est déjà hors du mode de boucle</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Due to Configuration Change</source>
            <translation>Boucle distante arrêtée en raison du changement de configuration</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in LLB Mode</source>
            <translation>Boucle distante activée avec succès : Unité **#1** en mode de boucle</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Successful:Unit **#1** in Transparent LLB Mode</source>
            <translation>Bouclage Distant Réussi : Equipement **#1** en Mode LLB Transparent</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in LLB Mode</source>
            <translation>L'équipement distant **#1** est déjà en mode de boucle</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit **#1** Was Already in Transparent LLB Mode</source>
            <translation>L'Equipement Distant **#1** était déjà en Mode LLB Transparent</translation>
        </message>
        <message utf8="true">
            <source>Selfloop or Loop Other Port Is Not Supported</source>
            <translation>Le bouclage propre ou sur un autre port n'est pas supporté</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Flux #1 : Boucle distante non arrêtée : Réception du "timeout"</translation>
        </message>
        <message utf8="true">
            <source>Stream #1: Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Flux #1 : Boucle distante non activée : Réception du "timeout"</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Boucle distante non arrêtée : Réception du "timeout"</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Down Unsuccessful:Acknowledgement Timeout</source>
            <translation>Echec de Fin de Bouclage Transparent Distant : Timeout Acquittement</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Boucle distante non activée : Réception du "timeout"</translation>
        </message>
        <message utf8="true">
            <source>Remote Transparent Loop Up Unsuccessful:Acknowledgement Timeout</source>
            <translation>Echec de Bouclage Transparent Distant : Timeout Acquittement</translation>
        </message>
        <message utf8="true">
            <source>Global address Duplicate Address Detection started.</source>
            <translation>Détection des adresses doublées d'adresse globale démarrée.</translation>
        </message>
        <message utf8="true">
            <source>Global address configuration failed (check settings).</source>
            <translation>Échec de la configuration de l'adresse globale (vérifiez les réglages).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on global address.</source>
            <translation>Attente de détection des adresses doublées d'adresse globale.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Global address is valid.</source>
            <translation>Détection des adresses doublées réussie. L'adresse globale est valide.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Global address is invalid.</source>
            <translation>Échec de la détection des adresses doublées. L'adresse globale est invalide.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address Duplicate Address Detection started.</source>
            <translation>Détection des adresses doublées d'adresse link-local démarrée.</translation>
        </message>
        <message utf8="true">
            <source>Link-Local address configuration failed (check settings).</source>
            <translation>Échec de la configuration de l'adresse link-local (vérifiez les réglages).</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Duplicate Address Detection on link-local address.</source>
            <translation>Attente de détection des adresses doublées d'adresse link-local.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection successful. Link-local address is valid.</source>
            <translation>Détection des adresses doublées réussie. L'adresse link-local est valide.</translation>
        </message>
        <message utf8="true">
            <source>Duplicate Address Detection failed. Link-local address is invalid.</source>
            <translation>Échec de la détection des adresses doublées. L'adresse link-local est invalide.</translation>
        </message>
        <message utf8="true">
            <source>Stateless IP retrieval started.</source>
            <translation>Récupération d'IP sans état démarrée.</translation>
        </message>
        <message utf8="true">
            <source>Failed to obtain stateless IP.</source>
            <translation>Échec de l'obtention d'une IP sans état.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateless IP.</source>
            <translation>Réussite de l'obtention d'une IP sans état.</translation>
        </message>
        <message utf8="true">
            <source>Stateful IP retrieval started.</source>
            <translation>Récupération d'IP avec état démarrée.</translation>
        </message>
        <message utf8="true">
            <source>No routers found to provide stateful IP.</source>
            <translation>Aucun routeur trouvé pour fournir une IP avec état.</translation>
        </message>
        <message utf8="true">
            <source>Retrying stateful IP retrieval.</source>
            <translation>Nouvel essai de récupération d'IP avec état.</translation>
        </message>
        <message utf8="true">
            <source>Successfully obtained stateful IP.</source>
            <translation>Réussite de l'obtention d'une IP avec état.</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable</source>
            <translation>Réseau Inaccessible</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable</source>
            <translation>Hôte Inaccessible</translation>
        </message>
        <message utf8="true">
            <source>Protocol Unreachable</source>
            <translation>Protocole Inaccessible</translation>
        </message>
        <message utf8="true">
            <source>Port Unreachable</source>
            <translation>Port Inaccessible</translation>
        </message>
        <message utf8="true">
            <source>Message too long</source>
            <translation>Message trop long</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Unknown</source>
            <translation>Réseau Destination Inconnu</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Unknown</source>
            <translation>Hôte Destination Inconnu</translation>
        </message>
        <message utf8="true">
            <source>Dest Network Prohibited</source>
            <translation>Réseau Destination Prohibé</translation>
        </message>
        <message utf8="true">
            <source>Dest Host Prohibited</source>
            <translation>Hôte Destination Prohibé</translation>
        </message>
        <message utf8="true">
            <source>Network Unreachable for TOS</source>
            <translation>Réseau non joignable pour le TOS</translation>
        </message>
        <message utf8="true">
            <source>Host Unreachable for TOS</source>
            <translation>Hôte non joignable pour le TOS</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Transit</source>
            <translation>Temps dépassé pendant le Transit</translation>
        </message>
        <message utf8="true">
            <source>Time Exceeded During Reassembly</source>
            <translation>Temps dépassé pendant le Réassemblage</translation>
        </message>
        <message utf8="true">
            <source>ICMP Unknown Error</source>
            <translation>Erreur ICMP inconnue</translation>
        </message>
        <message utf8="true">
            <source>Destination Unreachable</source>
            <translation>Destination non joingnable</translation>
        </message>
        <message utf8="true">
            <source>Address Unreachable</source>
            <translation>Adresse non valide</translation>
        </message>
        <message utf8="true">
            <source>No Route to Destination</source>
            <translation>Pas de route vers la destination</translation>
        </message>
        <message utf8="true">
            <source>Destination is Not a Neighbor</source>
            <translation>La destination n'est pas un voisin</translation>
        </message>
        <message utf8="true">
            <source>Communication with Destination Administratively Prohibited</source>
            <translation>La communication avec la destination est administrativement interdite</translation>
        </message>
        <message utf8="true">
            <source>Packet too Big</source>
            <translation>Paquet trop gros</translation>
        </message>
        <message utf8="true">
            <source>Hop Limit Exceeded in Transit</source>
            <translation>Saut Limite en transit dépassé</translation>
        </message>
        <message utf8="true">
            <source>Fragment Reassembly Time Exceeded</source>
            <translation>Temps de ré-assemblage dépassé</translation>
        </message>
        <message utf8="true">
            <source>Erroneous Header Field Encountered</source>
            <translation>Erreur sur le champ entête</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized Next Header Type Encountered</source>
            <translation>Type de champ après header inconnu</translation>
        </message>
        <message utf8="true">
            <source>Unrecognized IPv6 Option Encountered</source>
            <translation>Option IPv6 inconnue</translation>
        </message>
        <message utf8="true">
            <source>Inactive</source>
            <translation>Inactif</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE Actif</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP Actif</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>RESEAU Actif</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>Utilisateur Inactif</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>Couche données arrêtée</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>Time Out PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>Time Out PPP</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>Defaut PPPoE</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>Defaut LCP</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>Defaut Authentification PPP</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>Defaut PPP Inconnu</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>Defaut Activite PPP</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>Config Non Valide</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>Erreur Interne</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>ARP Successful. Destination MAC obtained</source>
            <translation>Succès ARP : Adresse MAC Destination obtenue</translation>
        </message>
        <message utf8="true">
            <source>Waiting for ARP Service...</source>
            <translation>Attente du service ARP...</translation>
        </message>
        <message utf8="true">
            <source>No ARP. DA = SA</source>
            <translation>No ARP. DA = SA</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Annuler</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>Continuer en semi-duplex</translation>
        </message>
        <message utf8="true">
            <source>Stop J-QuickCheck</source>
            <translation>Arrêt de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Test</source>
            <translation>RFC2544 Test</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>Ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>Descendant</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Test</source>
            <translation>Test J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop Status</source>
            <translation>Etat de la Boucle Distante</translation>
        </message>
        <message utf8="true">
            <source>Local Loop Status</source>
            <translation>Etat de la boucle en local</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status</source>
            <translation>Etat PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Status</source>
            <translation>Etat du IPv6</translation>
        </message>
        <message utf8="true">
            <source>ARP Status</source>
            <translation>Etat du ARP</translation>
        </message>
        <message utf8="true">
            <source>DHCP Status</source>
            <translation>Etat du DHCP</translation>
        </message>
        <message utf8="true">
            <source>ICMP Status</source>
            <translation>Etat du ICMP</translation>
        </message>
        <message utf8="true">
            <source>Neighbor Discovery Status</source>
            <translation>Statut Recouvrement voisin</translation>
        </message>
        <message utf8="true">
            <source>Autconfig Status</source>
            <translation>État de la configuration auto</translation>
        </message>
        <message utf8="true">
            <source>Address Status</source>
            <translation>État de l'adresse</translation>
        </message>
        <message utf8="true">
            <source>Unit Discovery</source>
            <translation>Recouvrement d'unité</translation>
        </message>
        <message utf8="true">
            <source>Search for units</source>
            <translation>Recherche d'unités</translation>
        </message>
        <message utf8="true">
            <source>Searching</source>
            <translation>Recherche</translation>
        </message>
        <message utf8="true">
            <source>Use selected unit</source>
            <translation>Utilisez unité sélectionnée</translation>
        </message>
        <message utf8="true">
            <source>Exiting</source>
            <translation>Sortant</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>Fermer</translation>
        </message>
        <message utf8="true">
            <source>Connecting&#xA;to Remote</source>
            <translation>Connexion&#xA;à l'unité distante</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Remote</source>
            <translation>Connexion&#xA;au distant</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Déconnecte</translation>
        </message>
        <message utf8="true">
            <source>Connected&#xA; to Remote</source>
            <translation>Connecté&#xA; au distant</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Connexion en cours</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>Inactif</translation>
        </message>
        <message utf8="true">
            <source>SFP3</source>
            <translation>SFP3</translation>
        </message>
        <message utf8="true">
            <source>SFP4</source>
            <translation>SFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP</source>
            <translation>CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2</source>
            <translation>CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4</source>
            <translation>CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP</source>
            <translation>QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP28</source>
            <translation>QSFP28</translation>
        </message>
        <message utf8="true">
            <source>XFP</source>
            <translation>XFP</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>Symétrique</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>Asymétrique</translation>
        </message>
        <message utf8="true">
            <source>Unidirectional</source>
            <translation>Unidirectionnel</translation>
        </message>
        <message utf8="true">
            <source>Loopback</source>
            <translation>Boucle avec retour</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream</source>
            <translation>Descendant et ascendant</translation>
        </message>
        <message utf8="true">
            <source>Both Dir</source>
            <translation>Les deux dir</translation>
        </message>
        <message utf8="true">
            <source>Throughput:</source>
            <translation>Débit :</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are the same.</source>
            <translation>Les débits descendants et ascendants sont les mêmes.</translation>
        </message>
        <message utf8="true">
            <source>Downstream and Upstream throughputs are different.</source>
            <translation>Les débits descendants et ascendants sont différents.</translation>
        </message>
        <message utf8="true">
            <source>Only test the network in one direction.</source>
            <translation>Seulement teste le réseau dans une direction.</translation>
        </message>
        <message utf8="true">
            <source>Measurements:</source>
            <translation>Mesures :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measurements are taken locally.</source>
            <translation>Le trafic est généré localement et les mesures sont prises localement.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and remotely and measurements are taken in each direction.</source>
            <translation>Le trafic est généré localement et à distance et les mesures sont prises dans chaque direction.</translation>
        </message>
        <message utf8="true">
            <source>Measurement Direction:</source>
            <translation>Direction de mesures :</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated locally and measured by the remote test instrument.</source>
            <translation>Le trafic est généré localement et mesurée par l'appareil de contrôle distant.</translation>
        </message>
        <message utf8="true">
            <source>Traffic is generated remotely and is measured by the local test instrument.</source>
            <translation>Le trafic est généré à distance et est mesurée par l'instrument de test local.</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay</source>
            <translation>Latence en boucle</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay</source>
            <translation>Délai dans un sens</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Measurements only.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Uniquement mesures de latence en boucle.&#xA;L'unité distante n'est pas capable de gérer le latence en boucle.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Uniquement mesures de délai à à sens unique.&#xA;L'unité distante n'est pas capable de gérer RTD bidirectionnel.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD or One Way Delay.</source>
            <translation>Les mesures de retard ne peuvent pas être faites. &#xA;L'unité à distance n'est pas capable de bidirectionnel RTD ou un délai unidirectionnel.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.</source>
            <translation>Les mesures de retard ne peuvent pas être faites. &#xA;L'unité à distance n'est pas capable de délai unidirectionnel.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of Bidirectional RTD.</source>
            <translation>Les mesures de retard ne peuvent pas être faites. &#xA;L'unité à distance n'est pas capable de bidirectionnel RTD.</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Measurements only.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Uniquement mesures de délai à sens unique.&#xA;RTD n'est pas pris en charge lors de test unidirectionnel.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Les mesures de retard ne peuvent pas être faites. &#xA;RTD n'est pas pris en charge lors de l'essai unidirectionnel.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.&#xA;The Remote unit is not capable of One Way Delay.&#xA;RTD is not supported when testing Unidirectionally.</source>
            <translation>Les mesures de retard ne peuvent paq être faites. &#xA;L'unité à distance n'est pas capable de délai unidrectionnel.&#xA;RTD n'est pas pris en charge lors de l'essai unidirectionnel.</translation>
        </message>
        <message utf8="true">
            <source>No Delay measurements can be made.</source>
            <translation>Aucune mesure de délai ne peut être faite.</translation>
        </message>
        <message utf8="true">
            <source>Latency Measurement Type</source>
            <translation>Type de mesure de latence</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>Local</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Distant</translation>
        </message>
        <message utf8="true">
            <source>Requires remote Viavi test instrument.</source>
            <translation>Nécessite l'équipement de test distant de Viavi.</translation>
        </message>
        <message utf8="true">
            <source>Version 2</source>
            <translation>Version 2</translation>
        </message>
        <message utf8="true">
            <source>Version 3</source>
            <translation>Version 3</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration</source>
            <translation>Etalonnage RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>DIX</source>
            <translation>DIX</translation>
        </message>
        <message utf8="true">
            <source>802.3</source>
            <translation>802.3</translation>
        </message>
        <message utf8="true">
            <source>Optics Selection</source>
            <translation>Sélection d'optiques</translation>
        </message>
        <message utf8="true">
            <source>IP Settings for Communications Channel to Far End</source>
            <translation>Réglages IP pour le canal de communications pour l'extrémité distante</translation>
        </message>
        <message utf8="true">
            <source>There are no Local IP Address settings required for the Loopback test.</source>
            <translation>Il n'existe pas de paramètres locaux de l'adresse IP requis pour le test de bouclage.</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Statique</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static - Per Service</source>
            <translation>Statique - Par service</translation>
        </message>
        <message utf8="true">
            <source>Static - Single</source>
            <translation>Statique - unique</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Manuel</translation>
        </message>
        <message utf8="true">
            <source>Stateful</source>
            <translation>État</translation>
        </message>
        <message utf8="true">
            <source>Stateless</source>
            <translation>Sans état</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>IP Source</translation>
        </message>
        <message utf8="true">
            <source>Src Link-Local Addr</source>
            <translation>Adresse source link-local</translation>
        </message>
        <message utf8="true">
            <source>Src Global Addr</source>
            <translation>Adresse source globale</translation>
        </message>
        <message utf8="true">
            <source>Auto Obtained</source>
            <translation>Obtenu Automatiquement</translation>
        </message>
        <message utf8="true">
            <source>User Defined</source>
            <translation>Utilisateur défini</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>Adresse MAC</translation>
        </message>
        <message utf8="true">
            <source>ARP Mode</source>
            <translation>Mode ARP</translation>
        </message>
        <message utf8="true">
            <source>Source Address Type</source>
            <translation>Type d'adresse source</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Mode</source>
            <translation>Mode PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Advanced</source>
            <translation>Avancée</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>Activé</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Désactivé</translation>
        </message>
        <message utf8="true">
            <source>Customer Source MAC</source>
            <translation>MAC Source Client</translation>
        </message>
        <message utf8="true">
            <source>Factory Default</source>
            <translation>Réglage d'usine</translation>
        </message>
        <message utf8="true">
            <source>Default Source MAC</source>
            <translation>MAC Source Défaut</translation>
        </message>
        <message utf8="true">
            <source>Customer Default MAC</source>
            <translation>MAC Défaut Client</translation>
        </message>
        <message utf8="true">
            <source>User Source MAC</source>
            <translation>MAC Util Source</translation>
        </message>
        <message utf8="true">
            <source>Cust. User MAC</source>
            <translation>MAC Util Client</translation>
        </message>
        <message utf8="true">
            <source>PPPoE</source>
            <translation>PPPoE</translation>
        </message>
        <message utf8="true">
            <source>IPoE</source>
            <translation>IPoE</translation>
        </message>
        <message utf8="true">
            <source>Skip Connect</source>
            <translation>Sauter la connexion</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Stacked VLAN</source>
            <translation>VLANs empilés</translation>
        </message>
        <message utf8="true">
            <source>How many VLANs are used on the Local network port?</source>
            <translation>Combien de VLAN sont utilisés sur le port de réseau local ?</translation>
        </message>
        <message utf8="true">
            <source>No VLANs</source>
            <translation>Pas de VLANs</translation>
        </message>
        <message utf8="true">
            <source>1 VLAN</source>
            <translation>1 VLAN</translation>
        </message>
        <message utf8="true">
            <source>2 VLANs (Q-in-Q)</source>
            <translation>2 VLANs (Q-in-Q)</translation>
        </message>
        <message utf8="true">
            <source>3+ VLANs (Stacked VLAN)</source>
            <translation>3+ VLANs (VLAN empilé)</translation>
        </message>
        <message utf8="true">
            <source>Enter VLAN ID settings:</source>
            <translation>Entrez les paramètres VLAN ID :</translation>
        </message>
        <message utf8="true">
            <source>Stack Depth</source>
            <translation>Empilez Profondeur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN7</source>
            <translation>SVLAN7</translation>
        </message>
        <message utf8="true">
            <source>TPID (hex)</source>
            <translation>TPID (hex)</translation>
        </message>
        <message utf8="true">
            <source>8100</source>
            <translation>8100</translation>
        </message>
        <message utf8="true">
            <source>9100</source>
            <translation>9100</translation>
        </message>
        <message utf8="true">
            <source>88A8</source>
            <translation>88A8</translation>
        </message>
        <message utf8="true">
            <source>User</source>
            <translation>Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>SVLAN6</source>
            <translation>SVLAN6</translation>
        </message>
        <message utf8="true">
            <source>SVLAN5</source>
            <translation>SVLAN5</translation>
        </message>
        <message utf8="true">
            <source>SVLAN4</source>
            <translation>SVLAN4</translation>
        </message>
        <message utf8="true">
            <source>SVLAN3</source>
            <translation>SVLAN3</translation>
        </message>
        <message utf8="true">
            <source>SVLAN2</source>
            <translation>SVLAN2</translation>
        </message>
        <message utf8="true">
            <source>SVLAN1</source>
            <translation>SVLAN1</translation>
        </message>
        <message utf8="true">
            <source>CVLAN</source>
            <translation>CVLAN</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server settings</source>
            <translation>Paramétrages du serveur Discovery</translation>
        </message>
        <message utf8="true">
            <source>NOTE: A Link-Local Destination IP can not be used to 'Connect to Remote'</source>
            <translation>Remarque: une adresse IP de destination liaison-locale ne peut pas être utilisée pour 'Se connecter à distance'</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>IP Destination</translation>
        </message>
        <message utf8="true">
            <source>Global Destination IP</source>
            <translation>Adresse IP globale de destination</translation>
        </message>
        <message utf8="true">
            <source>Pinging</source>
            <translation>Pinging</translation>
        </message>
        <message utf8="true">
            <source>Ping</source>
            <translation>Ping</translation>
        </message>
        <message utf8="true">
            <source>Help me find the &#xA;Destination IP</source>
            <translation>Aidez-moi à trouver la&#xA;Destination IP</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>Port local</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>Statut inconnu</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / FD)</source>
            <translation>UP (10 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / FD)</source>
            <translation>UP (100 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / FD)</source>
            <translation>UP (1000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10000 / FD)</source>
            <translation>UP (10000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (40000 / FD)</source>
            <translation>UP (40000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100000 / FD)</source>
            <translation>UP (100000 / FD)</translation>
        </message>
        <message utf8="true">
            <source>UP (10 / HD)</source>
            <translation>UP (10 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (100 / HD)</source>
            <translation>UP (100 / HD)</translation>
        </message>
        <message utf8="true">
            <source>UP (1000 / HD)</source>
            <translation>UP (1000 / HD)</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>Liaison perdue</translation>
        </message>
        <message utf8="true">
            <source>Local Port:</source>
            <translation>Port local :</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>Auto Négociation</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation:</source>
            <translation>Auto Négociation :</translation>
        </message>
        <message utf8="true">
            <source>Waiting to Connect</source>
            <translation>Attend connexion</translation>
        </message>
        <message utf8="true">
            <source>Connecting...</source>
            <translation>Connexion en cours...</translation>
        </message>
        <message utf8="true">
            <source>Retrying...</source>
            <translation>Nouvel essai...</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>CONNECTÉ</translation>
        </message>
        <message utf8="true">
            <source>Connection Failed</source>
            <translation>La connexion a échoué</translation>
        </message>
        <message utf8="true">
            <source>Connection Aborted</source>
            <translation>Annulation de connexion</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel:</source>
            <translation>Canal de communications:</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Advanced Settings</source>
            <translation>Paramétrages avancés du serveur Discovery</translation>
        </message>
        <message utf8="true">
            <source>Get destination IP from a Discovery Server</source>
            <translation>Obtenez destination IP d'un serveur Discovery (de recouvrement)</translation>
        </message>
        <message utf8="true">
            <source>Server IP</source>
            <translation>Serveur IP</translation>
        </message>
        <message utf8="true">
            <source>Server Port</source>
            <translation>Port du serveur</translation>
        </message>
        <message utf8="true">
            <source>Server Passphrase</source>
            <translation>Mot de passe du serveur</translation>
        </message>
        <message utf8="true">
            <source>Requested Lease Time (min.)</source>
            <translation>Durée d'allocation demandée (min.)</translation>
        </message>
        <message utf8="true">
            <source>Lease Time Granted (min.)</source>
            <translation>Durée d'allocation acquise (min.)</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Local</source>
            <translation>Paramétrages de réseau C2 - Local</translation>
        </message>
        <message utf8="true">
            <source>Local Unit Settings</source>
            <translation>Réglages de l'unité locale</translation>
        </message>
        <message utf8="true">
            <source>Traffic</source>
            <translation>Trafic</translation>
        </message>
        <message utf8="true">
            <source>LBM Traffic</source>
            <translation>Trafic LBM</translation>
        </message>
        <message utf8="true">
            <source>0 (lowest)</source>
            <translation>0 (le plus bas)</translation>
        </message>
        <message utf8="true">
            <source>1</source>
            <translation>1</translation>
        </message>
        <message utf8="true">
            <source>2</source>
            <translation>2</translation>
        </message>
        <message utf8="true">
            <source>3</source>
            <translation>3</translation>
        </message>
        <message utf8="true">
            <source>4</source>
            <translation>4</translation>
        </message>
        <message utf8="true">
            <source>5</source>
            <translation>5</translation>
        </message>
        <message utf8="true">
            <source>6</source>
            <translation>6</translation>
        </message>
        <message utf8="true">
            <source>7 (highest)</source>
            <translation>7 (le plus haut)</translation>
        </message>
        <message utf8="true">
            <source>0</source>
            <translation>0</translation>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID (hex) </source>
            <translation>TPID SVLAN (hex) utilisateur </translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type, EtherType, and MAC Addresses</source>
            <translation>Définir type de boucle, EtherType, et adresses MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses, EtherType, and LBM</source>
            <translation>Définir adresses MAC, EtherType et LBM</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses</source>
            <translation>Définir adresses MAC</translation>
        </message>
        <message utf8="true">
            <source>Set MAC Addresses and ARP Mode</source>
            <translation>Définir les adresses MAC et mode ARP</translation>
        </message>
        <message utf8="true">
            <source>Destination Type</source>
            <translation>Type Destination</translation>
        </message>
        <message utf8="true">
            <source>Unicast</source>
            <translation>Unicast</translation>
        </message>
        <message utf8="true">
            <source>Multicast</source>
            <translation>Multicast</translation>
        </message>
        <message utf8="true">
            <source>Broadcast</source>
            <translation>Diffusion</translation>
        </message>
        <message utf8="true">
            <source>VLAN User Priority</source>
            <translation>Bits de priorité VLAN</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected.</source>
            <translation>L'unité distante n'est pas connectée.</translation>
        </message>
        <message utf8="true">
            <source>L2 Network Settings - Remote</source>
            <translation>Paramétrages de réseau C2 - Distant</translation>
        </message>
        <message utf8="true">
            <source>Remote Unit Settings</source>
            <translation>Réglages de l'unité distante</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Local</source>
            <translation>Paramétrages de réseau C3 - Local</translation>
        </message>
        <message utf8="true">
            <source>Set Traffic Class, Flow Label and Hop Limit</source>
            <translation>Définir classe de trafic, label de flux et limite de saut</translation>
        </message>
        <message utf8="true">
            <source>What type of IP prioritization is used by your network?</source>
            <translation>Quel type de prioritisation d'IP est utilisée par votre réseau?</translation>
        </message>
        <message utf8="true">
            <source>EF(46)</source>
            <translation>EF(46)</translation>
        </message>
        <message utf8="true">
            <source>AF11(10)</source>
            <translation>AF11(10)</translation>
        </message>
        <message utf8="true">
            <source>AF12(12)</source>
            <translation>AF12(12)</translation>
        </message>
        <message utf8="true">
            <source>AF13(14)</source>
            <translation>AF13(14)</translation>
        </message>
        <message utf8="true">
            <source>AF21(18)</source>
            <translation>AF21(18)</translation>
        </message>
        <message utf8="true">
            <source>AF22(20)</source>
            <translation>AF22(20)</translation>
        </message>
        <message utf8="true">
            <source>AF23(22)</source>
            <translation>AF23(22)</translation>
        </message>
        <message utf8="true">
            <source>AF31(26)</source>
            <translation>AF31(26)</translation>
        </message>
        <message utf8="true">
            <source>AF32(28)</source>
            <translation>AF32(28)</translation>
        </message>
        <message utf8="true">
            <source>AF33(30)</source>
            <translation>AF33(30)</translation>
        </message>
        <message utf8="true">
            <source>AF41(34)</source>
            <translation>AF41(34)</translation>
        </message>
        <message utf8="true">
            <source>AF42(36)</source>
            <translation>AF42(36)</translation>
        </message>
        <message utf8="true">
            <source>AF43(38)</source>
            <translation>AF43(38)</translation>
        </message>
        <message utf8="true">
            <source>BE(0)</source>
            <translation>BE(0)</translation>
        </message>
        <message utf8="true">
            <source>CS1(8)</source>
            <translation>CS1(8)</translation>
        </message>
        <message utf8="true">
            <source>CS2(16)</source>
            <translation>CS2(16)</translation>
        </message>
        <message utf8="true">
            <source>CS3(24)</source>
            <translation>CS3(24)</translation>
        </message>
        <message utf8="true">
            <source>CS4(32)</source>
            <translation>CS4(32)</translation>
        </message>
        <message utf8="true">
            <source>CS5(40)</source>
            <translation>CS5(40)</translation>
        </message>
        <message utf8="true">
            <source>NC1 CS6(48)</source>
            <translation>NC1 CS6(48)</translation>
        </message>
        <message utf8="true">
            <source>NC2 CS7(56)</source>
            <translation>NC2 CS7(56)</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live, IP ID Incrementing, and PPPoE Mode</source>
            <translation>Définir durée de direct, IP ID incrémentant et Mode PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and PPPoE Mode</source>
            <translation>Définir durée de vie et mode PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live and IP ID Incrementing</source>
            <translation>Définir durée de direct et IP ID incrémentant</translation>
        </message>
        <message utf8="true">
            <source>Set Time to Live</source>
            <translation>Définir durée de vie</translation>
        </message>
        <message utf8="true">
            <source>What IP prioritization is used by your network?</source>
            <translation>Quelle prioritisation d'IP est utilisée par votre réseau?</translation>
        </message>
        <message utf8="true">
            <source>L3 Network Settings - Remote</source>
            <translation>Paramétrages de réseau C3 - Distant</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Local</source>
            <translation>Paramétrages de réseau C4 - Local</translation>
        </message>
        <message utf8="true">
            <source>TCP</source>
            <translation>TCP</translation>
        </message>
        <message utf8="true">
            <source>UDP</source>
            <translation>UDP</translation>
        </message>
        <message utf8="true">
            <source>Source Service Type</source>
            <translation>Type de Service Source</translation>
        </message>
        <message utf8="true">
            <source>AT-Echo</source>
            <translation>AT-Echo</translation>
        </message>
        <message utf8="true">
            <source>AT-NBP</source>
            <translation>AT-NBP</translation>
        </message>
        <message utf8="true">
            <source>AT-RTMP</source>
            <translation>AT-RTMP</translation>
        </message>
        <message utf8="true">
            <source>AT-ZIS</source>
            <translation>AT-ZIS</translation>
        </message>
        <message utf8="true">
            <source>AUTH</source>
            <translation>AUTH</translation>
        </message>
        <message utf8="true">
            <source>BGP</source>
            <translation>BGP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Client</source>
            <translation>Client-DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP-Server</source>
            <translation>Serveur-DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Client</source>
            <translation>DHCPv6-Client</translation>
        </message>
        <message utf8="true">
            <source>DHCPv6-Server</source>
            <translation>DHCPv6-Server</translation>
        </message>
        <message utf8="true">
            <source>DNS</source>
            <translation>DNS</translation>
        </message>
        <message utf8="true">
            <source>Finger</source>
            <translation>Finger</translation>
        </message>
        <message utf8="true">
            <source>Ftp</source>
            <translation>Ftp</translation>
        </message>
        <message utf8="true">
            <source>Ftp-Data</source>
            <translation>Ftp-Data</translation>
        </message>
        <message utf8="true">
            <source>GOPHER</source>
            <translation>GOPHER</translation>
        </message>
        <message utf8="true">
            <source>Http</source>
            <translation>Http</translation>
        </message>
        <message utf8="true">
            <source>Https</source>
            <translation>Https</translation>
        </message>
        <message utf8="true">
            <source>IMAP</source>
            <translation>IMAP</translation>
        </message>
        <message utf8="true">
            <source>IMAP3</source>
            <translation>IMAP3</translation>
        </message>
        <message utf8="true">
            <source>IRC</source>
            <translation>IRC</translation>
        </message>
        <message utf8="true">
            <source>KERBEROS</source>
            <translation>KERBEROS</translation>
        </message>
        <message utf8="true">
            <source>KPASSWD</source>
            <translation>KPASSWD</translation>
        </message>
        <message utf8="true">
            <source>LDAP</source>
            <translation>LDAP</translation>
        </message>
        <message utf8="true">
            <source>MailQ</source>
            <translation>MailQ</translation>
        </message>
        <message utf8="true">
            <source>SMB</source>
            <translation>SMB</translation>
        </message>
        <message utf8="true">
            <source>NameServer</source>
            <translation>NameServer</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-DGM</source>
            <translation>NETBIOS-DGM</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-NS</source>
            <translation>NETBIOS-NS</translation>
        </message>
        <message utf8="true">
            <source>NETBIOS-SSN</source>
            <translation>NETBIOS-SSN</translation>
        </message>
        <message utf8="true">
            <source>NNTP</source>
            <translation>NNTP</translation>
        </message>
        <message utf8="true">
            <source>NNTPS</source>
            <translation>NNTPS</translation>
        </message>
        <message utf8="true">
            <source>NTP</source>
            <translation>NTP</translation>
        </message>
        <message utf8="true">
            <source>POP2</source>
            <translation>POP2</translation>
        </message>
        <message utf8="true">
            <source>POP3</source>
            <translation>POP3</translation>
        </message>
        <message utf8="true">
            <source>POP3S</source>
            <translation>POP3S</translation>
        </message>
        <message utf8="true">
            <source>QMTP</source>
            <translation>QMTP</translation>
        </message>
        <message utf8="true">
            <source>RSYNC</source>
            <translation>RSYNC</translation>
        </message>
        <message utf8="true">
            <source>RTELNET</source>
            <translation>RTELNET</translation>
        </message>
        <message utf8="true">
            <source>RTSP</source>
            <translation>RTSP</translation>
        </message>
        <message utf8="true">
            <source>SFTP</source>
            <translation>SFTP</translation>
        </message>
        <message utf8="true">
            <source>SIP</source>
            <translation>SIP</translation>
        </message>
        <message utf8="true">
            <source>SIP-TLS</source>
            <translation>SIP-TLS</translation>
        </message>
        <message utf8="true">
            <source>SMTP</source>
            <translation>SMTP</translation>
        </message>
        <message utf8="true">
            <source>SNMP</source>
            <translation>SNMP</translation>
        </message>
        <message utf8="true">
            <source>SNPP</source>
            <translation>SNPP</translation>
        </message>
        <message utf8="true">
            <source>SSH</source>
            <translation>SSH</translation>
        </message>
        <message utf8="true">
            <source>SUNRPC</source>
            <translation>SUNRPC</translation>
        </message>
        <message utf8="true">
            <source>SUPDUP</source>
            <translation>SUPDUP</translation>
        </message>
        <message utf8="true">
            <source>TELNET</source>
            <translation>TELNET</translation>
        </message>
        <message utf8="true">
            <source>TELNETS</source>
            <translation>TELNETS</translation>
        </message>
        <message utf8="true">
            <source>TFTP</source>
            <translation>TFTP</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>Temps</translation>
        </message>
        <message utf8="true">
            <source>UUCP-PATH</source>
            <translation>UUCP-PATH</translation>
        </message>
        <message utf8="true">
            <source>WHOAMI</source>
            <translation>WHOAMI</translation>
        </message>
        <message utf8="true">
            <source>XDMCP</source>
            <translation>XDMCP</translation>
        </message>
        <message utf8="true">
            <source>Destination Service Type</source>
            <translation>Type de service de destination</translation>
        </message>
        <message utf8="true">
            <source>Set ATP Listen IP</source>
            <translation>Définir IP découte ATP</translation>
        </message>
        <message utf8="true">
            <source>L4 Network Settings - Remote</source>
            <translation>Paramétrages de réseau C4 - Distant</translation>
        </message>
        <message utf8="true">
            <source>Set Loop Type and Sequence, Responder, and Originator IDs</source>
            <translation>Définir type de boucle et séquence, répondeur, Ids d'initiateur</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Local</source>
            <translation>Paramétrages avancés C2 - Local</translation>
        </message>
        <message utf8="true">
            <source>Source Type</source>
            <translation>Type de Source</translation>
        </message>
        <message utf8="true">
            <source>Default MAC</source>
            <translation>MAC par défaut</translation>
        </message>
        <message utf8="true">
            <source>User MAC</source>
            <translation>MAC Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>LBM Configuration</source>
            <translation>Configuration LBM</translation>
        </message>
        <message utf8="true">
            <source>LBM Type</source>
            <translation>Type de LBM</translation>
        </message>
        <message utf8="true">
            <source>Enable Sender TLV</source>
            <translation>Activer émetteur TLV</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>Actif</translation>
        </message>
        <message utf8="true">
            <source>Advanced L2 Settings - Remote</source>
            <translation>Paramétrages avancés C2 - Distant</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Local</source>
            <translation>Paramétrages avancés C3 - Local</translation>
        </message>
        <message utf8="true">
            <source>Time To Live (hops)</source>
            <translation>Time To Live (bonds)</translation>
        </message>
        <message utf8="true">
            <source>Advanced L3 Settings - Remote</source>
            <translation>Paramétrages avancés C3 - Distant</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Local</source>
            <translation>Paramétrages avancés C4 - Local</translation>
        </message>
        <message utf8="true">
            <source>Advanced L4 Settings - Remote</source>
            <translation>Paramétrages avancés C4 - Distant</translation>
        </message>
        <message utf8="true">
            <source>Advanced Network Settings - Local</source>
            <translation>Paramétrages avancés de réseau - local</translation>
        </message>
        <message utf8="true">
            <source>Templates</source>
            <translation>modèles</translation>
        </message>
        <message utf8="true">
            <source>Do you want to use a configuration template?</source>
            <translation>Voulez-vous utiliser un gabarit de configuration?</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced Packet Access Rate > Transport Rate</source>
            <translation>Débit d'accès de paquet amélioré Viavi > Débit de transport</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Best Effort</source>
            <translation>MEF23.1 - meilleur Effort</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Continental</source>
            <translation>MEF23.1 - Continental</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Global</source>
            <translation>MEF23.1 - Mondial</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Metro</source>
            <translation>MEF23.1 - Metro</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Mobile Backhaul H</source>
            <translation>MEF23.1 - Liaison Mobile  H</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - Regional</source>
            <translation>MEF23.1 - Régional</translation>
        </message>
        <message utf8="true">
            <source>MEF23.1 - VoIP Data Emulation</source>
            <translation>MEF23.1 - Emulation de données VoIP</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Bit Transparent</source>
            <translation>Bit de RFC2544 transparent</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate > Transport Rate</source>
            <translation>Débit d'accès paquet RFC2544 > Débit de transport</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 Packet Access Rate = Transport Rate</source>
            <translation>Débit d'accès paquet RFC2544 = Débit de transport</translation>
        </message>
        <message utf8="true">
            <source>Apply Template</source>
            <translation>Appliquer le Gabarit</translation>
        </message>
        <message utf8="true">
            <source>Template Configurations:</source>
            <translation>Configurations de gabarits:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss (%): #1</source>
            <translation>La perte de trames débit (%): #1</translation>
        </message>
        <message utf8="true">
            <source>Latency Threshold (us): #1</source>
            <translation>Seuil de latence (us):#1</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us): #1</source>
            <translation>Seuil Gigue de Paquets (us) : #1</translation>
        </message>
        <message utf8="true">
            <source>Frame Sizes: Default</source>
            <translation>Formes de construction : Défaut</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Upstream): #1 Mbps</source>
            <translation>Largeur de bande maximale (en amont):#1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth (Downstream): #1 Mbps</source>
            <translation>Largeur de bande maximale (en aval):#1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Maximum Bandwidth: #1 Mbps</source>
            <translation>Largeur de bande maximale:#1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput and Latency Tests</source>
            <translation>Tests: le débit et tests de latence</translation>
        </message>
        <message utf8="true">
            <source>Tests: Throughput, Latency, Frame Loss and Back to Back</source>
            <translation>Tests: Débit, latence, perte de trame et retour vers retour</translation>
        </message>
        <message utf8="true">
            <source>Durations and Trials: #1 seconds with 1 trial</source>
            <translation>Durées et essais : #1 secondes avec 1 essai</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: Viavi Enhanced</source>
            <translation>Débit Algorithme: Viavi amélioré</translation>
        </message>
        <message utf8="true">
            <source>Throughput Algorithm: RFC 2544 Standard</source>
            <translation>Algorithme Débit: RFC 2544 standard</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Upstream): #1 Mbps</source>
            <translation>Seuil de débit (en amont): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Downstream): #1 Mbps</source>
            <translation>Seuil de débit (en aval): #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold: #1 Mbps</source>
            <translation>Seuil de débit : #1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Algorithm: RFC 2544 Standard</source>
            <translation>Perte trame Algorithme: RFC 2544 standard</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Burst Time: 1 sec</source>
            <translation>Retour vers Durée de salve de retour: 1 sec</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Granularity: 1 frame</source>
            <translation>Retour à granularité de retour: 1 trame</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Tests</translation>
        </message>
        <message utf8="true">
            <source>%</source>
            <translation>%</translation>
        </message>
        <message utf8="true">
            <source>L1 Mbps</source>
            <translation>C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Mbps</source>
            <translation>C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>L1 kbps</source>
            <translation>C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 kbps</source>
            <translation>C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Utilization settings</source>
            <translation>Définit paramétrages avancés d'utilisation</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth</source>
            <translation>Bande Passante Max</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 Mbps)</source>
            <translation>Largeur de bande max de flux descendant (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 Mbps)</source>
            <translation>Largeur de bande max de flux descendant (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L1 kbps)</source>
            <translation>Largeur de bande max de flux descendant (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (L2 kbps)</source>
            <translation>Largeur de bande max de flux descendant (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Bandwidth (%)</source>
            <translation>Largeur de bande max en aval (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 Mbps)</source>
            <translation>Largeur de bande max de flux ascendant (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 Mbps)</source>
            <translation>Largeur de bande max de flux ascendant (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L1 kbps)</source>
            <translation>Largeur de bande max de flux ascendant (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (L2 kbps)</source>
            <translation>Largeur de bande max de flux ascendant (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Bandwidth (%)</source>
            <translation>Largeur de bande max en amont (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 Mbps)</source>
            <translation>Bande Passante Max (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 Mbps)</source>
            <translation>Bande Passante Max (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L1 kbps)</source>
            <translation>Bande Passante Max (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (L2 kbps)</source>
            <translation>Bande Passante Max (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>Bande Passante Max (%)</translation>
        </message>
        <message utf8="true">
            <source>Selected Frames</source>
            <translation>trames sélectionnées</translation>
        </message>
        <message utf8="true">
            <source>Length Type</source>
            <translation>Type de longueur</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>Trame</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>Paquet</translation>
        </message>
        <message utf8="true">
            <source>Reset</source>
            <translation>Réinitialisation de la mémoire</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Select All</source>
            <translation>Toutes</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>Longueur Trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length</source>
            <translation>Long. de paquet</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Frame Length</source>
            <translation>Longueur de structure&#xA;en amont</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is not connected</source>
            <translation>L'unité distante n'est pas connectée</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Frame Length</source>
            <translation>Longueur de structure&#xA;en aval</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;unit&#xA;is not&#xA;connected</source>
            <translation>L'unité&#xA;distante&#xA;n'est pas&#xA;connectée</translation>
        </message>
        <message utf8="true">
            <source>Selected Tests</source>
            <translation>Tests sélectionnés</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Tests</source>
            <translation>Tests RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Latency (requires Throughput)</source>
            <translation>Latence ( nécessite débit)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit (requires Throughput)</source>
            <translation>Crédit de mémoire tampon (nécessite un débit)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput (requires Buffer Credit)</source>
            <translation>Débit du crédit de mémoire tampon (nécessite un crédit de mémoire tampon)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery (Loopback only and requires Throughput)</source>
            <translation>System Recovery (bouclage seulement et exige Throughput)</translation>
        </message>
        <message utf8="true">
            <source>Additional Tests</source>
            <translation>Tests supplémentaires</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (requires Throughput)</source>
            <translation>Gigue de paquet (nécessite Débit)</translation>
        </message>
        <message utf8="true">
            <source>Burst Test</source>
            <translation>Test de salve</translation>
        </message>
        <message utf8="true">
            <source>Extended Load (Loopback only)</source>
            <translation>Charge étendue ( bouclage uniquement)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Cfg</source>
            <translation>Cfg Débit</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>Processus de remise à zéro</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544 Standard</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi Enhanced</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Throughput measurement settings</source>
            <translation>Définissez les paramètres de mesure de débit de pointe</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy</source>
            <translation>Précision de mesure en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Précision de mesure de flux descendant (C1 Mpbs)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Précision de mesure de flux en aval (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L1 kbps)</source>
            <translation>Précision de mesure en aval (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (L2 kbps)</source>
            <translation>Précision de mesure de flux en aval (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Measurement Accuracy (%)</source>
            <translation>Précision (%) de mesure en aval</translation>
        </message>
        <message utf8="true">
            <source>To within 1.0%</source>
            <translation>Dans la limite de 1.0%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.1%</source>
            <translation>Dans la limite de 0.1%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.01%</source>
            <translation>Dans la limite de 0.01%</translation>
        </message>
        <message utf8="true">
            <source>To within 0.001%</source>
            <translation>dans 0.001%</translation>
        </message>
        <message utf8="true">
            <source>To within 400 Mbps</source>
            <translation>A une précision de 400 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 40 Mbps</source>
            <translation>A une précision de 40 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 4 Mbps</source>
            <translation>A une précision de 4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .4 Mbps</source>
            <translation>Dans la limite de .4 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 Mbps</source>
            <translation>A une précision de 1000 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 Mbps</source>
            <translation>A une précision de 100 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 Mbps</source>
            <translation>A une précision de 10 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 Mbps</source>
            <translation>A une précision de 1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 Mbps</source>
            <translation>Dans la limite de .1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .01 Mbps</source>
            <translation>Dans la limite de .01 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .001 Mbps</source>
            <translation>Dans la limite de .001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0001 Mbps</source>
            <translation>Dans la limite de .0001 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 92.942 Mbps</source>
            <translation>A une précision de 92.942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 9.2942 Mbps</source>
            <translation>Dans la limite de 9.2942 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .9294 Mbps</source>
            <translation>Dans la limite de .9294 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within .0929 Mbps</source>
            <translation>Dans la limite de .0929 Mbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10000 kbps</source>
            <translation>A une précision de 10000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1000 kbps</source>
            <translation>A une précision de 1000 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 100 kbps</source>
            <translation>A une précision de 100 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 10 kbps</source>
            <translation>A une précision de 10 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within 1 kbps</source>
            <translation>A une précision de 1 kbps</translation>
        </message>
        <message utf8="true">
            <source>To within .1 kbps</source>
            <translation>Dans la limite de .1 kbps</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy</source>
            <translation>Précision de mesure en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 Mbps)</source>
            <translation>Précision de mesure de flux ascendant (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 Mbps)</source>
            <translation>Précision de mesure de flux en amont (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L1 kbps)</source>
            <translation>Précision de mesure en amont (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (L2 kbps)</source>
            <translation>Précision de mesure de flux en amont (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Measurement Accuracy (%)</source>
            <translation>Précision (%) de mesure en amont</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>Précision de mesure</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 Mbps)</source>
            <translation>Précision de mesure (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 Mbps)</source>
            <translation>Précision de mesure (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L1 kbps)</source>
            <translation>Précision de mesure (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (L2 kbps)</source>
            <translation>Précision de mesure (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy (%)</source>
            <translation>Précision de mesure (%)</translation>
        </message>
        <message utf8="true">
            <source>More Information</source>
            <translation>Plus d'informations</translation>
        </message>
        <message utf8="true">
            <source>Troubleshooting</source>
            <translation>Dépannage</translation>
        </message>
        <message utf8="true">
            <source>Commissioning</source>
            <translation>Engagement</translation>
        </message>
        <message utf8="true">
            <source>Advanced Throughput Settings</source>
            <translation>Paramètres Débit avancés</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency / Packet Jitter test duration separately</source>
            <translation>Configurer Latence / Packet durée du test de gigue séparément</translation>
        </message>
        <message utf8="true">
            <source>Configure Latency test duration separately</source>
            <translation>Configurer la durée du test de latence séparément</translation>
        </message>
        <message utf8="true">
            <source>Configure Packet Jitter test duration separately</source>
            <translation>Configurer la durée du test de gigue de paquets séparément</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Cfg</source>
            <translation>Perte trame Cfg</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>Procédure de Test</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>Du haut vers le bas</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>Du bas vers le haut</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps</source>
            <translation>Nombre de pas</translation>
        </message>
        <message utf8="true">
            <source>#1 Mbps per step</source>
            <translation>#1 Mbps par pas</translation>
        </message>
        <message utf8="true">
            <source>#1 kbps per step</source>
            <translation>#1 kbps par pas</translation>
        </message>
        <message utf8="true">
            <source>#1 % Line Rate per step</source>
            <translation>#1 % débit de ligne par pas</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 Mbps)</source>
            <translation>La gamme de test de flux descendant (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 Mbps)</source>
            <translation>La gamme de test de flux descendant (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L1 kbps)</source>
            <translation>La gamme de test de flux descendant (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (L2 kbps)</source>
            <translation>La gamme de test de flux descendant (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test Range (%)</source>
            <translation>Echelle de test en aval (%)</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>Min</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularité (C1 Mbps) de largeur de bande en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularité (C2 Mbps) de largeur de bande en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularité (C1 Mbps) de largeur de bande en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularité (C2 kbps) de largeur de bande en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Bandwidth Granularity (%)</source>
            <translation>Granularité (%) de largeur de bande en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 Mbps)</source>
            <translation>La gamme de test de flux ascendant (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 Mbps)</source>
            <translation>La gamme de test de flux ascendant (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L1 kbps)</source>
            <translation>La gamme de test de flux ascendant (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (L2 kbps)</source>
            <translation>La gamme de test de flux ascendant (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test Range (%)</source>
            <translation>Echelle de test en amont (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularité (C1 Mbps) de largeur de bande en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularité (C2 Mbps) de largeur de bande en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularité (C1 kbps) de largeur de bande en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularité (C2 kbps) de largeur de bande en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Bandwidth Granularity (%)</source>
            <translation>Granularité (%) de largeur de bande en amont</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 Mbps)</source>
            <translation>Gamme de Test (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 Mbps)</source>
            <translation>Gamme de Test (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L1 kbps)</source>
            <translation>Gamme de Test (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (L2 kbps)</source>
            <translation>Gamme de Test (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>Gamme de Test (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 Mbps)</source>
            <translation>Granularité de bande passante (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 Mbps)</source>
            <translation>Granularité de la bande passante (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L1 kbps)</source>
            <translation>Granularité de la bande passante (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (L2 kbps)</source>
            <translation>Granularité de la bande passante (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>Granularité de Bandwidth (%)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Frame Loss measurement settings</source>
            <translation>Définir paramétrages avancés de mesure de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Advanced Frame Loss Settings</source>
            <translation>Paramétrages avancés de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Optional Test Measurements</source>
            <translation>Mesures de test optionnel</translation>
        </message>
        <message utf8="true">
            <source>Measure Latency</source>
            <translation>Latence de mesure</translation>
        </message>
        <message utf8="true">
            <source>Measure Packet Jitter</source>
            <translation>Gigue de paquet de mesure</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Cfg</source>
            <translation>Retour à Cfg de retour</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Burst Duration (s)</source>
            <translation>Durée (s) de salve max en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Burst Duration (s)</source>
            <translation>Durée (s) de salve max en amont</translation>
        </message>
        <message utf8="true">
            <source>Max Burst Duration (s)</source>
            <translation>Durée(s) de salve max</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (Frames)</source>
            <translation>Granularité du Burst (en Trames)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Back to Back Settings</source>
            <translation>Définir paramétrages avancés consécutifs</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame Policy</source>
            <translation>Politique de trame de pause</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Cfg</source>
            <translation>Cfg crédit buffer</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>Type de Login de contrôle de flux</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparenct Link)</source>
            <translation>Implicite ( Lien Transparent)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explicite (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>Taille Max de Buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput Steps</source>
            <translation>Etapes de débit</translation>
        </message>
        <message utf8="true">
            <source>Test Controls</source>
            <translation>Contrôles de test</translation>
        </message>
        <message utf8="true">
            <source>Configure test durations separately?</source>
            <translation>Configurer les durées de test séparément?</translation>
        </message>
        <message utf8="true">
            <source>Duration (s)</source>
            <translation>Durée (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>Nombre d'essais</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>Gigue Paquet</translation>
        </message>
        <message utf8="true">
            <source>Latency / Packet Jitter</source>
            <translation>Latence / gigue</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS)</source>
            <translation>Salve (CBS)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery</source>
            <translation>Récupération du système</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>Tous les tests</translation>
        </message>
        <message utf8="true">
            <source>Show&#xA;Pass/Fail</source>
            <translation>Montrez&#xA;Passage / Echecl</translation>
        </message>
        <message utf8="true">
            <source>Upstream&#xA;Threshold</source>
            <translation>Seuil&#xA;en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream&#xA;Threshold</source>
            <translation>Seuil&#xA;en aval</translation>
        </message>
        <message utf8="true">
            <source>Threshold</source>
            <translation>Seuil</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 Mbps)</source>
            <translation>Seuil de débit (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 Mbps)</source>
            <translation>Seuil de débit (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L1 kbps)</source>
            <translation>Seuil de débit (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (L2 kbps)</source>
            <translation>Seuil de débit (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>Seuil de débit (%)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected.</source>
            <translation>L'unité distante&#xA;n'est pas connectée.</translation>
        </message>
        <message utf8="true">
            <source>Latency RTD (us)</source>
            <translation>Latence RTD (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency OWD (us)</source>
            <translation>Latence OWD (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter (us)</source>
            <translation>Gigue Paquet (us)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Size (kB)</source>
            <translation>Taille de recherche salve (kO)</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing)</source>
            <translation>Salve (contrôle CBS)</translation>
        </message>
        <message utf8="true">
            <source>High Precision - Low Delay</source>
            <translation>Grande Précision - Délai Faible</translation>
        </message>
        <message utf8="true">
            <source>Low Precision - High Delay</source>
            <translation>Basse Précision - Délai important</translation>
        </message>
        <message utf8="true">
            <source>Run FC Tests</source>
            <translation>Lancer tests FC</translation>
        </message>
        <message utf8="true">
            <source>Skip FC Tests</source>
            <translation>Ignorez les tests FC</translation>
        </message>
        <message utf8="true">
            <source>on</source>
            <translation>marche</translation>
        </message>
        <message utf8="true">
            <source>off</source>
            <translation>arrêt</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;Throughput</source>
            <translation>Buffer Credit&#xA;Throughput</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Graph</source>
            <translation>Graphique de test de débit</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Graph</source>
            <translation>Graphique de test de débit en amont</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>Résultats du test de "Throughput"</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Test Results</source>
            <translation>Résultats de test de débit en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Graph</source>
            <translation>Graphique de test de débit en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Test Results</source>
            <translation>Résultats de test de débit en aval</translation>
        </message>
        <message utf8="true">
            <source>Throughput Anomalies</source>
            <translation>Les anomalies de débit</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Anomalies</source>
            <translation>Anomalies de débit en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Anomalies</source>
            <translation>Anomalies de débit en aval</translation>
        </message>
        <message utf8="true">
            <source>Throughput Results</source>
            <translation>Résultats de débit</translation>
        </message>
        <message utf8="true">
            <source>Throughput (Mbps)</source>
            <translation>Débit (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (kbps)</source>
            <translation>Débit (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>Débit (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame 1</source>
            <translation>Trame 1</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>C1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>C2</translation>
        </message>
        <message utf8="true">
            <source>L3</source>
            <translation>C3</translation>
        </message>
        <message utf8="true">
            <source>L4</source>
            <translation>C4</translation>
        </message>
        <message utf8="true">
            <source>Frame 2</source>
            <translation>Trame 2</translation>
        </message>
        <message utf8="true">
            <source>Frame 3</source>
            <translation>Trame 3</translation>
        </message>
        <message utf8="true">
            <source>Frame 4</source>
            <translation>Trame 4</translation>
        </message>
        <message utf8="true">
            <source>Frame 5</source>
            <translation>Trame 5</translation>
        </message>
        <message utf8="true">
            <source>Frame 6</source>
            <translation>Trame 6</translation>
        </message>
        <message utf8="true">
            <source>Frame 7</source>
            <translation>Trame 7</translation>
        </message>
        <message utf8="true">
            <source>Frame 8</source>
            <translation>Trame 8</translation>
        </message>
        <message utf8="true">
            <source>Frame 9</source>
            <translation>Trame 9</translation>
        </message>
        <message utf8="true">
            <source>Frame 10</source>
            <translation>Trame 10</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail</source>
            <translation>OK/Echec</translation>
        </message>
        <message utf8="true">
            <source>Frame Length&#xA;(Bytes)</source>
            <translation>Longueur de Trame&#xA;(Octets)</translation>
        </message>
        <message utf8="true">
            <source>Packet Length&#xA;(Bytes)</source>
            <translation>Longueur de Paquet&#xA;(octets)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (Mbps)</source>
            <translation>Débit&#xA;mesurée (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (%)</source>
            <translation>Débit&#xA;mesurée (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured&#xA;Rate (frms/sec)</source>
            <translation>Débit mesuré&#xA; (trame/sec)</translation>
        </message>
        <message utf8="true">
            <source>R_RDY&#xA;Detect</source>
            <translation>R_RDY&#xA;Détecter</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(Mbps)</source>
            <translation>Débit&#xA;Cfg (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (kbps)</source>
            <translation>Débit C1&#xA; mesuré (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;Rate (Mbps)</source>
            <translation>Débit C1&#xA; mesuré (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L1&#xA;(% Line Rate)</source>
            <translation>C1&#xA; mesuré (% débit de ligne)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (kbps)</source>
            <translation>Débit C2&#xA; mesuré (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;Rate (Mbps)</source>
            <translation>Débit C2&#xA; mesuré (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L2&#xA;(% Line Rate)</source>
            <translation>C2&#xA; mesuré (% débit de ligne)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (kbps)</source>
            <translation>Débit C3&#xA;mesuré (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;Rate (Mbps)</source>
            <translation>Débit C3&#xA; mesuré (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L3&#xA;(% Line Rate)</source>
            <translation>C3&#xA; mesuré (% débit de ligne)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (kbps)</source>
            <translation>Débit C4&#xA;mesuré (kbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;Rate (Mbps)</source>
            <translation>Débit C4&#xA; mesuré (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured L4&#xA;(% Line Rate)</source>
            <translation>C4&#xA; mesuré (% Débit de ligne)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (frms/sec)</source>
            <translation>Débit&#xA; mesuré (trame/sec)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA; (pkts/sec)</source>
            <translation>Débit&#xA;mesuré (paquets/sec)</translation>
        </message>
        <message utf8="true">
            <source>Pause&#xA;Detect</source>
            <translation>Pause&#xA;Détecter</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 Mbps)</source>
            <translation>Cfg Débit&#xA; (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 Mbps)</source>
            <translation>Cfg Débit&#xA; (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L1 kbps)</source>
            <translation>Cfg débit&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(L2 kbps)</source>
            <translation>Cfg Débit&#xA; (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Anomalies</source>
            <translation>Anomalies</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame(s)&#xA;Detected</source>
            <translation>OoS trame(s)&#xA;Détecté</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload&#xA;Error Detected</source>
            <translation>Charge utile Acterna&#xA;Erreur détectée</translation>
        </message>
        <message utf8="true">
            <source>FCS&#xA;Error Detected</source>
            <translation>FCS&#xA;erreur détectée</translation>
        </message>
        <message utf8="true">
            <source>IP Checksum&#xA;Error Detected</source>
            <translation>Checksum IP&#xA;Erreur détectée</translation>
        </message>
        <message utf8="true">
            <source>TCP/UDP Checksum&#xA;Error Detected</source>
            <translation>Checksum TCP/UDP&#xA;Erreur détectée</translation>
        </message>
        <message utf8="true">
            <source>Latency Test</source>
            <translation>Test de latence</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Graph</source>
            <translation>Graphique de test de latence</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Graph</source>
            <translation>Graphique de test de latence en amont</translation>
        </message>
        <message utf8="true">
            <source>Latency Test Results</source>
            <translation>Résultats de test de latence</translation>
        </message>
        <message utf8="true">
            <source>Upstream Latency Test Results</source>
            <translation>Résultats de test de latence en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Graph</source>
            <translation>Graphique de test de latence en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Latency Test Results</source>
            <translation>Résultats de test de latence en aval</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;RTD (us)</source>
            <translation>Latence&#xA;RTD(us)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;OWD (us)</source>
            <translation>Latence&#xA;OWD(us)</translation>
        </message>
        <message utf8="true">
            <source>Measured &#xA;% Line Rate</source>
            <translation>Débit de ligne mesuré &#xA;%</translation>
        </message>
        <message utf8="true">
            <source>Pause &#xA;Detect</source>
            <translation>Détecter Pause&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results</source>
            <translation>Résultats du Test Trame perdue</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Test Results</source>
            <translation>Résultats de test de perte de trame en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Test Results</source>
            <translation>Résultats de test de perte de trame en aval</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Results</source>
            <translation>trame des résultats de perte</translation>
        </message>
        <message utf8="true">
            <source>Frame 0</source>
            <translation>Trame 0</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 Mbps)</source>
            <translation>Débit configuré (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 Mbps)</source>
            <translation>Débit configuré (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L1 kbps)</source>
            <translation>Débit configuré (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (L2 kbps)</source>
            <translation>Débit configuré (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Configured Rate (%)</source>
            <translation>Débit configuré (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>Trame perdue (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 Mbps)</source>
            <translation>Débit&#xA; (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 Mbps)</source>
            <translation>Débit&#xA; (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L1 kbps)</source>
            <translation>Débit Tarif&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(L2 kbps)</source>
            <translation>Débit&#xA; (C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Rate&#xA;(%)</source>
            <translation>Débit&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate&#xA;(%)</source>
            <translation>Taux de perte de trame&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Frames Lost</source>
            <translation>Trames Perdues</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet&#xA;Jitter (us)</source>
            <translation>Moyenne de paquet&#xA;de gigue max (us)</translation>
        </message>
        <message utf8="true">
            <source>Error&#xA;Detect</source>
            <translation>Erreur&#xA;Detectée</translation>
        </message>
        <message utf8="true">
            <source>OoS&#xA;Detect</source>
            <translation>OoS&#xA;Détecter</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate&#xA;(%)</source>
            <translation>Débit Cfg&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results</source>
            <translation>Résultats de Test Back to Back</translation>
        </message>
        <message utf8="true">
            <source>Upstream Back to Back Test Results</source>
            <translation>Retour en amont vers resultats de test de retour</translation>
        </message>
        <message utf8="true">
            <source>Downstream Back to Back Test Results</source>
            <translation>Retour en aval vers resultats de test de retour</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Frames</source>
            <translation>Moyenne&#xA;trames de salve</translation>
        </message>
        <message utf8="true">
            <source>Average&#xA;Burst Seconds</source>
            <translation>Moyenne&#xA;secondes de salve</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results</source>
            <translation>Buffer Credit Résultats de Test</translation>
        </message>
        <message utf8="true">
            <source>Attention</source>
            <translation>ATTENTION</translation>
        </message>
        <message utf8="true">
            <source>MinimumBufferSize&#xA;(Credits)</source>
            <translation>TailleBufferMinimum&#xA;(Crédits)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results</source>
            <translation>Buffer Credit Throughput Résultats de Test</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Results</source>
            <translation>Résultats de débit de crédit buffer</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 Mbps)</source>
            <translation>Débit (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Cred 0</source>
            <translation>Créd 0</translation>
        </message>
        <message utf8="true">
            <source>Cred 1</source>
            <translation>Créd 1</translation>
        </message>
        <message utf8="true">
            <source>Cred 2</source>
            <translation>Créd 2</translation>
        </message>
        <message utf8="true">
            <source>Cred 3</source>
            <translation>Créd 3</translation>
        </message>
        <message utf8="true">
            <source>Cred 4</source>
            <translation>Créd 4</translation>
        </message>
        <message utf8="true">
            <source>Cred 5</source>
            <translation>Créd 5</translation>
        </message>
        <message utf8="true">
            <source>Cred 6</source>
            <translation>Créd 6</translation>
        </message>
        <message utf8="true">
            <source>Cred 7</source>
            <translation>Créd 7</translation>
        </message>
        <message utf8="true">
            <source>Cred 8</source>
            <translation>Créd 8</translation>
        </message>
        <message utf8="true">
            <source>Cred 9</source>
            <translation>Créd 9</translation>
        </message>
        <message utf8="true">
            <source>Buffer size&#xA;(Credits)</source>
            <translation>Taille de Buffer&#xA;(Credits)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Mbps)</source>
            <translation>Débit mesurée&#xA;(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(%)</source>
            <translation>Débit mesurée&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate&#xA;(Frames/sec)</source>
            <translation>Débit&#xA;mesuré (trames/sec)</translation>
        </message>
        <message utf8="true">
            <source>J-Proof - Ethernet L2 Transparency Test</source>
            <translation>Test de transparence Ethernet C2 - J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof</source>
            <translation>J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Frames</source>
            <translation>Trames J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Run Test</source>
            <translation>Démarrer le test</translation>
        </message>
        <message utf8="true">
            <source>Run J-Proof Test</source>
            <translation>Lancer test J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>Résumé</translation>
        </message>
        <message utf8="true">
            <source>End: Detailed Results</source>
            <translation>Fin : Résultats détaillés</translation>
        </message>
        <message utf8="true">
            <source>Exit J-Proof Test</source>
            <translation>Quitter le test J-Proof</translation>
        </message>
        <message utf8="true">
            <source>J-Proof:</source>
            <translation>J-Proof:</translation>
        </message>
        <message utf8="true">
            <source>Applying</source>
            <translation>Prise en compte</translation>
        </message>
        <message utf8="true">
            <source>Configure Frame Types to Test Service for Layer 2 Transparency</source>
            <translation>Configurer les types de trames pour tester le service pour la transparence de la couche 2</translation>
        </message>
        <message utf8="true">
            <source>  Tx  </source>
            <translation>  Tx  </translation>
        </message>
        <message utf8="true">
            <source>Tx</source>
            <translation>Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Name   </source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>Name</source>
            <translation>Nom</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>   Protocol   </source>
            <translation>   Protocole   </translation>
        </message>
        <message utf8="true">
            <source>Protocol</source>
            <translation>Protocole</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>STP</source>
            <translation>STP</translation>
        </message>
        <message utf8="true">
            <source>RSTP</source>
            <translation>RSTP</translation>
        </message>
        <message utf8="true">
            <source>MSTP</source>
            <translation>MSTP</translation>
        </message>
        <message utf8="true">
            <source>LLDP</source>
            <translation>LLDP</translation>
        </message>
        <message utf8="true">
            <source>GMRP</source>
            <translation>GMRP</translation>
        </message>
        <message utf8="true">
            <source>GVRP</source>
            <translation>GVRP</translation>
        </message>
        <message utf8="true">
            <source>CDP</source>
            <translation>CDP</translation>
        </message>
        <message utf8="true">
            <source>VTP</source>
            <translation>VTP</translation>
        </message>
        <message utf8="true">
            <source>LACP</source>
            <translation>LACP</translation>
        </message>
        <message utf8="true">
            <source>PAgP</source>
            <translation>PAgP</translation>
        </message>
        <message utf8="true">
            <source>UDLD</source>
            <translation>UDLD</translation>
        </message>
        <message utf8="true">
            <source>DTP</source>
            <translation>DTP</translation>
        </message>
        <message utf8="true">
            <source>ISL</source>
            <translation>ISL</translation>
        </message>
        <message utf8="true">
            <source>PVST-PVST+</source>
            <translation>PVST-PVST+</translation>
        </message>
        <message utf8="true">
            <source>STP-ULFAST</source>
            <translation>STP-ULFAST</translation>
        </message>
        <message utf8="true">
            <source>VLAN-BRDGSTP</source>
            <translation>VLAN-BRDGSTP</translation>
        </message>
        <message utf8="true">
            <source>802.1d</source>
            <translation>802.1d</translation>
        </message>
        <message utf8="true">
            <source> Frame Type </source>
            <translation> Type de Trame </translation>
        </message>
        <message utf8="true">
            <source>802.3-LLC</source>
            <translation>802.3-LLC</translation>
        </message>
        <message utf8="true">
            <source>802.3-SNAP</source>
            <translation>802.3-SNAP</translation>
        </message>
        <message utf8="true">
            <source> Encapsulation </source>
            <translation> Encapsulation </translation>
        </message>
        <message utf8="true">
            <source>Stacked</source>
            <translation>Empilé</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Size</source>
            <translation>Taille de&#xA;Trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>Taille de Trame</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Count</source>
            <translation>Nombre</translation>
        </message>
        <message utf8="true">
            <source>Rate&#xA;(fr/sec)</source>
            <translation>Débit&#xA;(tr/sec)</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>Taux</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Timeout&#xA;(msec)</source>
            <translation>Timeout&#xA;(msec)</translation>
        </message>
        <message utf8="true">
            <source>Timeout</source>
            <translation>Timeout</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>OUI</source>
            <translation>OUI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SSAP</source>
            <translation>SSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DSAP</source>
            <translation>DSAP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Control</source>
            <translation>Champs Control</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>Type</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>DEI&#xA;Bit</source>
            <translation>Bit&#xA;du DEI</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>SVLAN TPID</source>
            <translation>SVLAN TPID</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>User SVLAN TPID</source>
            <translation>SVLAN TPID utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Auto-inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc CPbit</source>
            <translation>Auto-inc CPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auto-inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
        </message>
        <message utf8="true">
            <source>Auto Inc SPbit</source>
            <translation>Auto-inc SPbit</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source> Encap. </source>
            <translation>Encap.</translation>
        </message>
        <message utf8="true">
            <source>Encap.</source>
            <translation>Encap.</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Type</source>
            <translation>Type de&#xA;Trame</translation>
        </message>
        <message utf8="true">
            <source>DA</source>
            <translation>DA</translation>
        </message>
        <message utf8="true">
            <source>Oui</source>
            <translation>Oui</translation>
        </message>
        <message utf8="true">
            <source>VLAN Id</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Pbit Inc</source>
            <translation>Pbit Inc</translation>
        </message>
        <message utf8="true">
            <source>Quick&#xA;Config</source>
            <translation>Config&#xA;Rapide</translation>
        </message>
        <message utf8="true">
            <source>Add&#xA;Frame</source>
            <translation>Ajouter&#xA;Trame</translation>
        </message>
        <message utf8="true">
            <source>Remove&#xA;Frame</source>
            <translation>Retirer&#xA;Trame</translation>
        </message>
        <message utf8="true">
            <source>SA</source>
            <translation>SA</translation>
        </message>
        <message utf8="true">
            <source>Source Address is common for all Frames. This is configured on the Local Settings page.</source>
            <translation>L'adresse source est commune pour toutes les trames. Ceci est configuré sur la page des réglages locaux.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN</source>
            <translation>SVLAN</translation>
        </message>
        <message utf8="true">
            <source>Pbit Increment</source>
            <translation>Incrément du Pbit</translation>
        </message>
        <message utf8="true">
            <source>VLAN Stack</source>
            <translation>Pile VLAN</translation>
        </message>
        <message utf8="true">
            <source>SPbit Increment</source>
            <translation>Incrément de bit SP</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>Longueur</translation>
        </message>
        <message utf8="true">
            <source>LLC</source>
            <translation>LLC</translation>
        </message>
        <message utf8="true">
            <source>Data</source>
            <translation>Données</translation>
        </message>
        <message utf8="true">
            <source>FCS</source>
            <translation>FCS</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Configuration</source>
            <translation>Configuration J-Proof</translation>
        </message>
        <message utf8="true">
            <source>Intensity</source>
            <translation>Intensité</translation>
        </message>
        <message utf8="true">
            <source>Quick (10)</source>
            <translation>Rapide (10)</translation>
        </message>
        <message utf8="true">
            <source>Full (100)</source>
            <translation>Full (100)</translation>
        </message>
        <message utf8="true">
            <source>Family</source>
            <translation>Famille</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>Tout</translation>
        </message>
        <message utf8="true">
            <source>Spanning Tree</source>
            <translation>Spanning Tree</translation>
        </message>
        <message utf8="true">
            <source>Cisco</source>
            <translation>Cisco</translation>
        </message>
        <message utf8="true">
            <source>IEEE</source>
            <translation>IEEE</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;On</source>
            <translation>Laser&#xA;activé</translation>
        </message>
        <message utf8="true">
            <source>Laser&#xA;Off</source>
            <translation>Laser&#xA;éteint</translation>
        </message>
        <message utf8="true">
            <source>Start Frame&#xA;Sequence</source>
            <translation>Trame de départ&#xA;Séquence</translation>
        </message>
        <message utf8="true">
            <source>Stop Frame&#xA;Sequence</source>
            <translation>Trame de fin&#xA;Séquence</translation>
        </message>
        <message utf8="true">
            <source>J-Proof Test</source>
            <translation>J-Proof Test</translation>
        </message>
        <message utf8="true">
            <source>Svc 1</source>
            <translation>Svc 1</translation>
        </message>
        <message utf8="true">
            <source>STOPPED</source>
            <translation>ARRÊTÉ</translation>
        </message>
        <message utf8="true">
            <source>IN PROGRESS</source>
            <translation>EN COURS</translation>
        </message>
        <message utf8="true">
            <source>Payload Errors</source>
            <translation>Erreurs données utiles</translation>
        </message>
        <message utf8="true">
            <source>Header Errors</source>
            <translation>Erreurs d'En-têtes</translation>
        </message>
        <message utf8="true">
            <source>Count Mismatch</source>
            <translation>Disparité de compte</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>Desactive</translation>
        </message>
        <message utf8="true">
            <source>Results Summary</source>
            <translation>Résumé des résultats</translation>
        </message>
        <message utf8="true">
            <source>Idle</source>
            <translation>En attente</translation>
        </message>
        <message utf8="true">
            <source>In Progress</source>
            <translation>En cours</translation>
        </message>
        <message utf8="true">
            <source>Payload Mismatch</source>
            <translation>Payload Mismatch</translation>
        </message>
        <message utf8="true">
            <source>Header Mismatch</source>
            <translation>L'entête ne correspond pas</translation>
        </message>
        <message utf8="true">
            <source>Ethernet</source>
            <translation>Ethernet</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>J-Proof Results</source>
            <translation>Résultats de J-Proof</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>  Name   </source>
            <translation>Nom</translation>
        </message>
        <message utf8="true">
            <source>  Rx  </source>
            <translation>  Rx  </translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Etat</translation>
        </message>
        <message utf8="true">
            <source>Optical Rx&#xA;Reset</source>
            <translation>Récepteur&#xA;Optique Reset</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck</source>
            <translation>VérificationRapide</translation>
        </message>
        <message utf8="true">
            <source>Run QuickCheck Test</source>
            <translation>Exécuter un test de VérificationRapide</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Settings</source>
            <translation>Paramétrages de VérificationRapide</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Extended Load Results</source>
            <translation>Résultats de charge étendue de VérificationRapide</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>Détails</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>Unique</translation>
        </message>
        <message utf8="true">
            <source>Per Stream</source>
            <translation>Par Flux</translation>
        </message>
        <message utf8="true">
            <source>FROM_TEST</source>
            <translation>I don't know what that means</translation>
        </message>
        <message utf8="true">
            <source>256</source>
            <translation>256</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck:</source>
            <translation>J-QuickCheck:</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test:</source>
            <translation>Test du "Throughput":</translation>
        </message>
        <message utf8="true">
            <source>QuickCheck Results</source>
            <translation>Résultats de QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Test Results</source>
            <translation>Résultats du test de charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Tx Frame Count</source>
            <translation>Compteur de trames Tx</translation>
        </message>
        <message utf8="true">
            <source>Rx Frame Count</source>
            <translation>Compteur de trames Rx</translation>
        </message>
        <message utf8="true">
            <source>Errored Frame Count</source>
            <translation>Compteur de trames erroné</translation>
        </message>
        <message utf8="true">
            <source>OoS Frame Count</source>
            <translation>Compteur de trames OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frame Count</source>
            <translation>Compte de trame perdu</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>Taux de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>Matériel</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>Permanent</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>Activée</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 Mbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Débit: %1 Mbps (L1), Durée:%2 secondes, taille de trame:%3 octets</translation>
        </message>
        <message utf8="true">
            <source>Throughput: %1 kbps (L1), Duration: %2 Seconds, Frame Size: %3 Bytes</source>
            <translation>Débit: %1 kbps (L1) , Durée : %2 secondes, Taille de l'image : %3 Octets</translation>
        </message>
        <message utf8="true">
            <source>Not what you wanted?</source>
            <translation>Ce n'est pas ce que vous vouliez?</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>Arrêter</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>Début</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>Réussit</translation>
        </message>
        <message utf8="true">
            <source>Looking for Destination</source>
            <translation>Recherche de destination</translation>
        </message>
        <message utf8="true">
            <source>ARP Status:</source>
            <translation>Etat du ARP:</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>Boucle distante</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>Vérification de la boucle active</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>Vérification de la boucle dure</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>Vérification de la boucle permanente</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>Vérification de la boucle LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>Boucle active</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>Boucle permanente</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>Boucle LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>La boucle à échouée</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop:</source>
            <translation>Boucle distante :</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 Mbps)</source>
            <translation>Débit mesuré (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>Non déterminé</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 Mbps</source>
            <translation>#1 C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps</source>
            <translation>#1 C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L1 kbps</source>
            <translation>#1 C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 kbps</source>
            <translation>#1 C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>Non disponible</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput (L1 kbps)</source>
            <translation>Débit mesuré ( kbps L1 )</translation>
        </message>
        <message utf8="true">
            <source>See Errors</source>
            <translation>Voir erreurs</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 Mbps)</source>
            <translation>Charge (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Load (L1 kbps)</source>
            <translation>Charge ( kbps L1 )</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Duration (seconds)</source>
            <translation>Durée (secondes) de test de seuil</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (bytes)</source>
            <translation>Taille (octets) de trame</translation>
        </message>
        <message utf8="true">
            <source>Bit Rate</source>
            <translation>Débit</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration</source>
            <translation>Durée de test de performance</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>5 Seconds</source>
            <translation>5 Secondes</translation>
        </message>
        <message utf8="true">
            <source>30 Seconds</source>
            <translation>30 Secondes</translation>
        </message>
        <message utf8="true">
            <source>1 Minute</source>
            <translation>1 Minute</translation>
        </message>
        <message utf8="true">
            <source>3 Minutes</source>
            <translation>3 Minutes</translation>
        </message>
        <message utf8="true">
            <source>10 Minutes</source>
            <translation>10 Minutes</translation>
        </message>
        <message utf8="true">
            <source>30 Minutes</source>
            <translation>30 Minutes</translation>
        </message>
        <message utf8="true">
            <source>1 Hour</source>
            <translation>1 heure</translation>
        </message>
        <message utf8="true">
            <source>2 Hours</source>
            <translation>2 Heures</translation>
        </message>
        <message utf8="true">
            <source>24 Hours</source>
            <translation>24 Heures</translation>
        </message>
        <message utf8="true">
            <source>72 Hours</source>
            <translation>72 Heures</translation>
        </message>
        <message utf8="true">
            <source>User defined</source>
            <translation>Défini par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Test Duration (sec)</source>
            <translation>Durée (sec) de test</translation>
        </message>
        <message utf8="true">
            <source>Performance Test Duration (minutes)</source>
            <translation>Durée du test de performance (minutes)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>Longueur de Trame (Octets)</translation>
        </message>
        <message utf8="true">
            <source>Jumbo</source>
            <translation>Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Length</source>
            <translation>Longueur de l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Length</source>
            <translation>Jumbo Longueur</translation>
        </message>
        <message utf8="true">
            <source>  Electrical Connector:  10/100/1000</source>
            <translation>  Connecteur électrique :  10/100/1000</translation>
        </message>
        <message utf8="true">
            <source>Laser Wavelength</source>
            <translation>Longueur d'Onde du Laser</translation>
        </message>
        <message utf8="true">
            <source>Electrical Connector</source>
            <translation>Connecteur électrique</translation>
        </message>
        <message utf8="true">
            <source>Optical Connector</source>
            <translation>Connecteur Optique</translation>
        </message>
        <message utf8="true">
            <source>850 nm</source>
            <translation>850 nm</translation>
        </message>
        <message utf8="true">
            <source>1310 nm</source>
            <translation>1310 nm</translation>
        </message>
        <message utf8="true">
            <source>1550 nm</source>
            <translation>1550 nm</translation>
        </message>
        <message utf8="true">
            <source>Details...</source>
            <translation>Détails...</translation>
        </message>
        <message utf8="true">
            <source>Link Active</source>
            <translation>Lien Actif</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>Résultats du trafic</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>Nombre d'erreurs</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>Trames Erronées</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>Trames OoS</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>Perte de Trames</translation>
        </message>
        <message utf8="true">
            <source>Interface Details</source>
            <translation>Détails de l'interface</translation>
        </message>
        <message utf8="true">
            <source>SFP/XFP Details</source>
            <translation>Détails SFP/XFP</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable SFP, refer to Channel/Wavelength tuning setup.</source>
            <translation>Longueur d'onde (nm): SFP accordable, fait référence au réglage de la configuration de la longueur d'onde de canal.</translation>
        </message>
        <message utf8="true">
            <source>Cable Length (m)</source>
            <translation>Longueur de câble (m)</translation>
        </message>
        <message utf8="true">
            <source>Tuning Supported</source>
            <translation>Réglage supporté</translation>
        </message>
        <message utf8="true">
            <source>Wavelength;Channel</source>
            <translation>Longueur d'onde; Canal</translation>
        </message>
        <message utf8="true">
            <source>Wavelength</source>
            <translation>Longueur d'onde</translation>
        </message>
        <message utf8="true">
            <source>Channel</source>
            <translation>Voie</translation>
        </message>
        <message utf8="true">
            <source>&lt;p>Recommended Rates&lt;/p></source>
            <translation>&lt;p>Taux recommandés&lt;/p></translation>
        </message>
        <message utf8="true">
            <source>Nominal Rate (Mbits/sec)</source>
            <translation>Débit nominal (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Min Rate (Mbits/sec)</source>
            <translation>Débit min (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Max Rate (Mbits/sec)</source>
            <translation>Débit max (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>Vendor</source>
            <translation>Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Vendor PN</source>
            <translation>PN Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Vendor Rev</source>
            <translation>Rev Vendeur</translation>
        </message>
        <message utf8="true">
            <source>Power Level Type</source>
            <translation>Type de Niveau de Puissance</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Monitoring</source>
            <translation>Surveillance du Diagnostique</translation>
        </message>
        <message utf8="true">
            <source>Diagnostic Byte</source>
            <translation>Octet de Diagnostique</translation>
        </message>
        <message utf8="true">
            <source>Configure JMEP</source>
            <translation>Configurez JMEP</translation>
        </message>
        <message utf8="true">
            <source>SFP281</source>
            <translation>SFP281</translation>
        </message>
        <message utf8="true">
            <source>Wavelength (nm): Tunable Device, refer to Channel/Wavelength tuning setup.</source>
            <translation>Longueur d'onde (nm): Périphérique syntonisable, référerez-vous à configuration de syntonisation de chaîne/longueur d'onde</translation>
        </message>
        <message utf8="true">
            <source>CFP/QSFP Details</source>
            <translation>Détails CFP/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2/QSFP Details</source>
            <translation>Détails CFP2/QSFP</translation>
        </message>
        <message utf8="true">
            <source>CFP4/QSFP Details</source>
            <translation>Détails CFP4/QSFP</translation>
        </message>
        <message utf8="true">
            <source>Vendor SN</source>
            <translation>SN du vendeur</translation>
        </message>
        <message utf8="true">
            <source>Date Code</source>
            <translation>Code des date</translation>
        </message>
        <message utf8="true">
            <source>Lot Code</source>
            <translation>Code du lot</translation>
        </message>
        <message utf8="true">
            <source>HW / SW Version#</source>
            <translation># de version matériel/logiciel</translation>
        </message>
        <message utf8="true">
            <source>MSA HW Spec. Rev#</source>
            <translation># Rev Spec. matériel MSA</translation>
        </message>
        <message utf8="true">
            <source>MSA Mgmt. I/F Rev#</source>
            <translation># Rev I/F Gestion MSA </translation>
        </message>
        <message utf8="true">
            <source>Power Class</source>
            <translation>Classe de puissance</translation>
        </message>
        <message utf8="true">
            <source>Rx Power Level Type</source>
            <translation>Type de Niveau de Puissance Rx</translation>
        </message>
        <message utf8="true">
            <source>Rx Max Lambda Power (dBm)</source>
            <translation>Puissance lambda max Rx (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Max Lambda Power (dBm)</source>
            <translation>Puissance lambda max Tx (dBm)</translation>
        </message>
        <message utf8="true">
            <source># of Active Fibers</source>
            <translation># de fibres actives</translation>
        </message>
        <message utf8="true">
            <source>Wavelengths per Fiber</source>
            <translation>Longueurs d'onde par fibre</translation>
        </message>
        <message utf8="true">
            <source>Diagnositc Byte</source>
            <translation>Octet de diagnostic</translation>
        </message>
        <message utf8="true">
            <source>WL per Fiber Range (nm)</source>
            <translation>WL par plage de fibre (nm)</translation>
        </message>
        <message utf8="true">
            <source>Max Ntwk Lane Bit Rate (Gbps)</source>
            <translation>Débit binaire max de la voie réseau (Gbps)</translation>
        </message>
        <message utf8="true">
            <source>Module ID</source>
            <translation>Module ID</translation>
        </message>
        <message utf8="true">
            <source>Rates Supported</source>
            <translation>débits supportés</translation>
        </message>
        <message utf8="true">
            <source>Nominal Wavelength (nm)</source>
            <translation>Longueur d'onde nominale (nm)</translation>
        </message>
        <message utf8="true">
            <source>Nominal Bit Rate (Mbits/sec)</source>
            <translation>Débit Nominal (Mbits/sec)</translation>
        </message>
        <message utf8="true">
            <source>*** Recommended use for 100GigE RS-FEC applications ***</source>
            <translation>*** Usage recommandé pour applications 100GigE RS-FEC ***</translation>
        </message>
        <message utf8="true">
            <source>CFP Expert</source>
            <translation>Mode expert CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Expert</source>
            <translation>Mode expert CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Expert</source>
            <translation>Expert CFP4</translation>
        </message>
        <message utf8="true">
            <source>Enable Viavi Loopback</source>
            <translation>Activer Viavi bouclage</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP Viavi Loopback</source>
            <translation>Activer loopback CFP Viavi</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Enable CFP Expert Mode</source>
            <translation>Activer le mode expert CFP</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP2 Expert Mode</source>
            <translation>Activer le mode expert CFP2</translation>
        </message>
        <message utf8="true">
            <source>Enable CFP4 Expert Mode</source>
            <translation>Activer mode experte CFP4</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx</source>
            <translation>CFP Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Tx</source>
            <translation>CFP2 Tx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Tx</source>
            <translation>CFP4 Tx</translation>
        </message>
        <message utf8="true">
            <source>Pre-Emphasis</source>
            <translation>Pré-accentuation</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Pre-Emphasis</source>
            <translation>Pré-emphase Tx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Default</source>
            <translation>Défaut</translation>
        </message>
        <message utf8="true">
            <source>Low</source>
            <translation>Bas</translation>
        </message>
        <message utf8="true">
            <source>Nominal</source>
            <translation>Nominal</translation>
        </message>
        <message utf8="true">
            <source>High</source>
            <translation>Haut</translation>
        </message>
        <message utf8="true">
            <source>Clock Divider</source>
            <translation>Diviseur d'horloge</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Clock Divider</source>
            <translation>Disiseur d'horloge Tx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>1/16</source>
            <translation>1/16</translation>
        </message>
        <message utf8="true">
            <source>1/64</source>
            <translation>1/64</translation>
        </message>
        <message utf8="true">
            <source>1/40</source>
            <translation>1/40</translation>
        </message>
        <message utf8="true">
            <source>1/160</source>
            <translation>1/160</translation>
        </message>
        <message utf8="true">
            <source>Skew Offset (bytes)</source>
            <translation>Délai de décalage (bytes)</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Skew Offset</source>
            <translation>Offset oblique Tx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>-32</source>
            <translation>-32</translation>
        </message>
        <message utf8="true">
            <source>32</source>
            <translation>32</translation>
        </message>
        <message utf8="true">
            <source>Invert Polarity</source>
            <translation>Inverser la polarité</translation>
        </message>
        <message utf8="true">
            <source>CFP Tx Invert Polarity</source>
            <translation>Polarité inverse CFP Tx</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Tx FIFO</source>
            <translation>Réinitialisation Tx FIFO</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx</source>
            <translation>CFP Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Rx</source>
            <translation>CFP2 Rx</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Rx</source>
            <translation>Rx CFP4</translation>
        </message>
        <message utf8="true">
            <source>Equalization</source>
            <translation>Égalisation</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Equalization</source>
            <translation>Egalisation Rx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CFP Rx Invert Polarity</source>
            <translation>Polarité inverse Rx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Ignore LOS</source>
            <translation>ignorer LOS</translation>
        </message>
        <message utf8="true">
            <source>CFP Rx Ignore LOS</source>
            <translation>LOS ignore Rx CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Reset Rx FIFO</source>
            <translation>Réinitialisation Rx FIFO</translation>
        </message>
        <message utf8="true">
            <source>QSFP Expert</source>
            <translation>Mode expert QSFP</translation>
        </message>
        <message utf8="true">
            <source>Enable QSFP Expert Mode</source>
            <translation>Activer le mode expert QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx</source>
            <translation>QSFP Rx</translation>
        </message>
        <message utf8="true">
            <source>QSFP Rx Ignore LOS</source>
            <translation>QSFP Rx Ignorer LOS</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR Bypass</source>
            <translation>Bypass CDR</translation>
        </message>
        <message utf8="true">
            <source>Receive</source>
            <translation>Recevoir</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Receive Bypass</source>
            <translation>QSFP CDR Bypass (évitement) de réception</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Transmit</source>
            <translation>Transmettre</translation>
        </message>
        <message utf8="true">
            <source>QSFP CDR Transmit Bypass</source>
            <translation>QSFP CDR Bypas (évitement) de transmission</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>CDR transmit and receive bypass control would be available if QSFP module supported it.</source>
            <translation>La commande bypass de réception et transmission CRD pourrait être disponible si le module QSFP le supportait.</translation>
        </message>
        <message utf8="true">
            <source>Engineering</source>
            <translation>ingénierie</translation>
        </message>
        <message utf8="true">
            <source>XCVR Core Loopback</source>
            <translation>Loopback (boucle) central XCVR</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Core Loopback</source>
            <translation>Loopback de cœur CFP XCVR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>XCVR Line Loopback</source>
            <translation>Loopback (boucle) de ligne XCVR</translation>
        </message>
        <message utf8="true">
            <source>CFP XCVR Line Loopback</source>
            <translation>Loopback de ligne CFP XCVR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Host Loopback</source>
            <translation>Module Hôte Loopback (boucle retour)</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Host Loopback</source>
            <translation>Loopback d'hôte de module CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Module Network Loopback</source>
            <translation>Module Réseau Loppback</translation>
        </message>
        <message utf8="true">
            <source>CFP Module Network Loopback</source>
            <translation>Loopback de réseau de module CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox System Loopback</source>
            <translation>Gearbox System Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox System Loopback</source>
            <translation>Loopback de système de boîte de vitesse CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Line Loopback</source>
            <translation>Gearbox Line Loopback</translation>
        </message>
        <message utf8="true">
            <source>CFP Gearbox Line Loopback</source>
            <translation>Loopback de ligne de boîte de vitesse CFP</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Gearbox Chip ID</source>
            <translation>ID de puce de boîte de vitesse</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Chip Rev</source>
            <translation>Version de puce de boîte de vitesse</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode Ver</source>
            <translation>Ver Ucode de boîte de vitesse</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Ucode CRC</source>
            <translation>CRC Ucode de boîte de vitesse</translation>
        </message>
        <message utf8="true">
            <source>Gearbox System Lanes</source>
            <translation>Voies de système de boîte de vitesse</translation>
        </message>
        <message utf8="true">
            <source>Gearbox Line Lanes</source>
            <translation>Voies de ligne de boîte de vitesse</translation>
        </message>
        <message utf8="true">
            <source>CFPn Host Lanes</source>
            <translation>Voies d'hôte CFPn</translation>
        </message>
        <message utf8="true">
            <source>CFPn Network Lanes</source>
            <translation>Voies de réseau CFPn</translation>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Core Loopback</source>
            <translation>QSFP XCVR Loppback central</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>QSFP XCVR Line Loopback</source>
            <translation>QSFP XCVR Loopback de ligne</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>MDIO</source>
            <translation>MDIO</translation>
        </message>
        <message utf8="true">
            <source>Peek</source>
            <translation>Peek</translation>
        </message>
        <message utf8="true">
            <source>Peek DevType</source>
            <translation>Peek DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek DevType</source>
            <translation>MDIO Peek DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek PhyAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek PhyAddr</source>
            <translation>MDIO Peek PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek RegAddr</source>
            <translation>Peek PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Peek RegAddr</source>
            <translation>MDIO Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek Value</source>
            <translation>Peek Value</translation>
        </message>
        <message utf8="true">
            <source>Peek Success</source>
            <translation>Peek Success</translation>
        </message>
        <message utf8="true">
            <source>Poke</source>
            <translation>Poke</translation>
        </message>
        <message utf8="true">
            <source>Poke DevType</source>
            <translation>Poke DevType</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke DevType</source>
            <translation>MDIO Poke DevType</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PhyAddr</source>
            <translation>Poke PhyAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke PhyAddr</source>
            <translation>MDIO Poke PhyAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke RegAddr</source>
            <translation>Poke RegAddr</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke RegAddr</source>
            <translation>MDIO Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Value</source>
            <translation>Poke Value</translation>
        </message>
        <message utf8="true">
            <source>MDIO Poke Value</source>
            <translation>MDIO Poke Valeur</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke Success</source>
            <translation>Poke Success</translation>
        </message>
        <message utf8="true">
            <source>Register A013 controls per-lane laser enable/disable.</source>
            <translation>Contrôles de registre A013 par voie laser actif/désactivé</translation>
        </message>
        <message utf8="true">
            <source>I2C</source>
            <translation>I2C</translation>
        </message>
        <message utf8="true">
            <source>Peek PartSel</source>
            <translation>Peek PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek PartSel</source>
            <translation>I2C Peek PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Peek DevAddr</source>
            <translation>Peek DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Peek DevAddr</source>
            <translation>I2C Peek DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Peek RegAddr</source>
            <translation>I2C Peek RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke PartSel</source>
            <translation>Poke PartSel</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke PartSel</source>
            <translation>I2C Poke PartSel</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Poke DevAddr</source>
            <translation>Poke DevAddr</translation>
        </message>
        <message utf8="true">
            <source>I2C Poke DevAddr</source>
            <translation>I2C Poke DevAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke RegAddr</source>
            <translation>I2C Poke RegAddr</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>I2C Poke Value</source>
            <translation>I2C Poke Valeur</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Register 0x56 controls per-lane laser enable/disable.</source>
            <translation>Le registre 0x56 coontrôle le laser actif/inactif par bande</translation>
        </message>
        <message utf8="true">
            <source>SFP+ Details</source>
            <translation>SFP+détails</translation>
        </message>
        <message utf8="true">
            <source>Signal</source>
            <translation>Signal</translation>
        </message>
        <message utf8="true">
            <source>Tx Signal Clock</source>
            <translation>Signal d'horloge en Tx</translation>
        </message>
        <message utf8="true">
            <source>Clock Source</source>
            <translation>Source d'horloge</translation>
        </message>
        <message utf8="true">
            <source>Internal</source>
            <translation>Interne</translation>
        </message>
        <message utf8="true">
            <source>Recovered</source>
            <translation>Récupéré</translation>
        </message>
        <message utf8="true">
            <source>External</source>
            <translation>Externe</translation>
        </message>
        <message utf8="true">
            <source>External 1.5M</source>
            <translation>1.5M Externe</translation>
        </message>
        <message utf8="true">
            <source>External 2M</source>
            <translation>2M Externe</translation>
        </message>
        <message utf8="true">
            <source>External 10M</source>
            <translation>10M Externe</translation>
        </message>
        <message utf8="true">
            <source>Remote Recovered</source>
            <translation>Distant Récupéré</translation>
        </message>
        <message utf8="true">
            <source>STM Tx</source>
            <translation>STM Tx</translation>
        </message>
        <message utf8="true">
            <source>Timing Module</source>
            <translation>Module de durée</translation>
        </message>
        <message utf8="true">
            <source>VC-12 Source</source>
            <translation>Source VC-12 </translation>
        </message>
        <message utf8="true">
            <source>Internal - Frequency Offset (ppm)</source>
            <translation>Offset de Fréquence Interne (ppm)</translation>
        </message>
        <message utf8="true">
            <source>Remote Clock</source>
            <translation>Horloge Distante</translation>
        </message>
        <message utf8="true">
            <source>Tunable Device</source>
            <translation>Périphérique syntonisable</translation>
        </message>
        <message utf8="true">
            <source>Tuning Mode</source>
            <translation>Mode de réglage</translation>
        </message>
        <message utf8="true">
            <source>Frequency</source>
            <translation>Fréquence</translation>
        </message>
        <message utf8="true">
            <source>Frequency (GHz)</source>
            <translation>Fréquence (GHz)</translation>
        </message>
        <message utf8="true">
            <source>First Tunable Frequency (GHz)</source>
            <translation>Première fréquence réglable (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Last Tunable Frequency (GHz)</source>
            <translation>Dernière fréquence réglable (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Grid Spacing (GHz)</source>
            <translation>Espacement de grille (GHz)</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm</source>
            <translation>alarme obliquitée</translation>
        </message>
        <message utf8="true">
            <source>Skew Alarm Threshold (ns)</source>
            <translation>Seuil d'alarme obliquitée (ns)</translation>
        </message>
        <message utf8="true">
            <source>Resync needed</source>
            <translation>Resynchronisation requise</translation>
        </message>
        <message utf8="true">
            <source>Resync complete</source>
            <translation>Resynchonisation terminée</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration passed</source>
            <translation>Etalonnage RS-FEC passé</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC calibration failed</source>
            <translation>Echec de l'étalonnage RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC is typically used with SR4, PSM4, CWDM4.</source>
            <translation>RS-FEC est généralement utilisé avec SR4, PSM4, CWDM4.</translation>
        </message>
        <message utf8="true">
            <source>To perform RS-FEC calibration, do the following (also applies to CFP4):</source>
            <translation>Pour réaliser l'étalonnage RS-FEC, faites ce qui suit (s'applique également au CFP4):</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 adapter into the CSAM</source>
            <translation>Insérer un adaptateur QSFP28 dans le CSAM</translation>
        </message>
        <message utf8="true">
            <source>Insert a QSFP28 transceiver into the adapter</source>
            <translation>Insérer un émetteur-récepteur QSFP28 dans l'adaptateur</translation>
        </message>
        <message utf8="true">
            <source>Insert a fiber loopback device into the transceiver</source>
            <translation>Insérer un périphérique de loopback fibre dans l'émetteur-récepteur</translation>
        </message>
        <message utf8="true">
            <source>Click Calibrate</source>
            <translation>Cliquez sur Etalonnner</translation>
        </message>
        <message utf8="true">
            <source>Calibrate</source>
            <translation>Etalonner</translation>
        </message>
        <message utf8="true">
            <source>Calibrating...</source>
            <translation>Etalonnage...</translation>
        </message>
        <message utf8="true">
            <source>Resync</source>
            <translation>Resynchroniser</translation>
        </message>
        <message utf8="true">
            <source>Must now resync the transceiver to the device under test (DUT).</source>
            <translation>Doit à présent resynchroniser l'émetteur-récepteur vers le périphérique sous test (DUT).</translation>
        </message>
        <message utf8="true">
            <source>Remove the fiber loopback device from the transceiver</source>
            <translation>Supprimez le périphérique loopback fibre depuis l'émetteur-récepteur</translation>
        </message>
        <message utf8="true">
            <source>Establish a connection to the device under test (DUT)</source>
            <translation>Etablir une connexion au périphérique sous test (DUT)</translation>
        </message>
        <message utf8="true">
            <source>Turn the DUT laser ON</source>
            <translation>Mettez en marche le laser DUT</translation>
        </message>
        <message utf8="true">
            <source>Verify that the Signal Present LED on your CSAM is green</source>
            <translation>Vérifiez que le signal présent de LED sur votre CSAM est vert</translation>
        </message>
        <message utf8="true">
            <source>Click Lane Resync</source>
            <translation>Cliquez sur resynchroniser voie</translation>
        </message>
        <message utf8="true">
            <source>Lane&#xA;Resync</source>
            <translation>Resync.&#xA;voie</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Results</source>
            <translation>Résultats de J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin J-QuickCheck test.</source>
            <translation>Pressez le bouton "Start" pour commencer les test J-QuickCheck.</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode.</source>
            <translation>Continuant en mode semi-duplex.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the upstream direction.</source>
            <translation>Vérification de la connectivité de trafic dans le sens du flux montant.</translation>
        </message>
        <message utf8="true">
            <source>Checking traffic connectivity in the downstream direction.</source>
            <translation>Vérification de la connectivité de trafic dans le sens du flux descendant.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity for all services has been successfully verified.</source>
            <translation>La connectivité de trafic pour tous les services a été vérifié avec succès.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1</source>
            <translation>Le connectivité de trafic n'a pas pu être vérifiée avec succès dans le sens du flux montant pour le(s) service(s) : #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction on the following service(s): #1</source>
            <translation>Le connectivité de trafic n'a pas pu être vérifiée avec succès dans le sens du flux descendant pour le(s) service(s) suivant(s) : #1</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction for service(s) : #1 and in the downstream direction for service(s) : #2</source>
            <translation>Le connectivité de trafic n'a pas pu être vérifiée avec succès dans le sens du flux montant pour le(s) service(s) : #1 et dans le sens du flux descendant pour le(s) service(s) : #2</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the upstream direction.</source>
            <translation>Le connectivité de trafic n'a pas pu être vérifiée avec succès dans le sens du flux montant.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified in the downstream direction.</source>
            <translation>Le connectivité de trafic n'a pas pu être vérifiée avec succès dans le sens du flux descendant.</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity could not be successfully verified for upstream or the downstream direction.</source>
            <translation>Le connectivité de trafic n'a pas pu être vérifiée avec succès dans le sens du flux montant ou descendant.</translation>
        </message>
        <message utf8="true">
            <source>Checking Connectivity</source>
            <translation>Vérification de la connectivité</translation>
        </message>
        <message utf8="true">
            <source>Traffic Connectivity:</source>
            <translation>Connectivité de trafic :</translation>
        </message>
        <message utf8="true">
            <source>Start QC</source>
            <translation>Démarrer QC</translation>
        </message>
        <message utf8="true">
            <source>Stop QC</source>
            <translation>Arrêt QC</translation>
        </message>
        <message utf8="true">
            <source>Optics Self-Test</source>
            <translation>Auto-test d'optiques</translation>
        </message>
        <message utf8="true">
            <source>End: Save Profiles</source>
            <translation>Fin: Sauvegarder profils</translation>
        </message>
        <message utf8="true">
            <source>Reports</source>
            <translation>Rapports</translation>
        </message>
        <message utf8="true">
            <source>Optics</source>
            <translation>Optiques</translation>
        </message>
        <message utf8="true">
            <source>Exit Optics Self-Test</source>
            <translation>Quitter Auto-test Optiques</translation>
        </message>
        <message utf8="true">
            <source>---</source>
            <translation>---</translation>
        </message>
        <message utf8="true">
            <source>QSFP+</source>
            <translation>QSFP+</translation>
        </message>
        <message utf8="true">
            <source>Optical signal was lost. The test has been aborted.</source>
            <translation>Le signal optique a été perdu. Le test a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>Low power level detected. The test has been aborted.</source>
            <translation>Niveau faible d'alimentation détecté. Le test a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>High power level detected. The test has been aborted.</source>
            <translation>Haut niveau de puissance  détecté. Le test a été interrompu .</translation>
        </message>
        <message utf8="true">
            <source>An error was detected. The test has been aborted.</source>
            <translation>Une erreur a été détectée. Le test a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected.</source>
            <translation>Le test a ECHOUE car une obliquité excessive a été détectée.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED because excessive skew was detected. The test has been aborted</source>
            <translation>Le test a ECHOUE parce qu'une obliquité excessive a été détectée. Le test a été interrompu</translation>
        </message>
        <message utf8="true">
            <source>A Bit Error was detected. The test has been aborted.</source>
            <translation>Un bit d'erreur a été détecté. Le test a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to the BER exceeded the configured threshold.</source>
            <translation>Le test a ECHOUE à cause du dépassement BER du seuil configuré.</translation>
        </message>
        <message utf8="true">
            <source>The test has FAILED due to loss of Pattern Sync.</source>
            <translation>Le test a ÉCHOUÉ du fait de perte de Sync. de motif.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted QSFP+.</source>
            <translation>Impossible de lire le QSFP + inséré.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP.</source>
            <translation>Impossible de lire le PCP inséré.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP2.</source>
            <translation>Impossible de lire CFP2 inséré.</translation>
        </message>
        <message utf8="true">
            <source>Unable to read inserted CFP4.</source>
            <translation>Impossible de lire CFP4 inséré.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC correctable bit error was detected.  The test has been aborted.</source>
            <translation>une erreur de bit corrigible RS-FEC a été détectée. Le test a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>An RS-FEC uncorrectable bit error was detected.  The test has been aborted.</source>
            <translation>une erreur de bit incorrigible RS-FEC a été détectée. Le test a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration failed</source>
            <translation>Echec de l'étalonnage RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Input Frequency Deviation error detected</source>
            <translation>Entrée erreur d'écart de fréquence détecté</translation>
        </message>
        <message utf8="true">
            <source>Output Frequency Deviation error detected</source>
            <translation>Erreur détecté de déviation de fréquence de sortie</translation>
        </message>
        <message utf8="true">
            <source>Sync Lost</source>
            <translation>Synchro perdue</translation>
        </message>
        <message utf8="true">
            <source>Code Violations detected</source>
            <translation>Violation de code détectée</translation>
        </message>
        <message utf8="true">
            <source>Alignment Marker Lock Lost</source>
            <translation>Perte de verrou de marqueur d'alignement</translation>
        </message>
        <message utf8="true">
            <source>Invalid Alignment Markers detected</source>
            <translation>Marqueurs d'alignement invalides détectés</translation>
        </message>
        <message utf8="true">
            <source>BIP 8 AM Bit Errors detected</source>
            <translation>Erreurs de bit BIP 8 AM détectées</translation>
        </message>
        <message utf8="true">
            <source>BIP Block Errors detected</source>
            <translation>Erreurs de bloc BIP détectées</translation>
        </message>
        <message utf8="true">
            <source>Skew detected</source>
            <translation>obliquité détectée</translation>
        </message>
        <message utf8="true">
            <source>Block Errors detected</source>
            <translation>Erreurs de bloc détectées</translation>
        </message>
        <message utf8="true">
            <source>Undersize Frames detected</source>
            <translation>trames sous dimensionnées détectés</translation>
        </message>
        <message utf8="true">
            <source>Remote Fault detected</source>
            <translation>Panne distante détectée</translation>
        </message>
        <message utf8="true">
            <source>#1 Bit Errors detected</source>
            <translation>#1 bits en erreur détectés</translation>
        </message>
        <message utf8="true">
            <source>Pattern Sync lost</source>
            <translation>Synchronisation de modèle perdue</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold could not be measured accurately due to loss of Pattern Sync</source>
            <translation>Le seuil BER ne peut pas être mesuré précisément du fait de perte de synchronisation de modèle</translation>
        </message>
        <message utf8="true">
            <source>The measured BER exceeded the chosen BER Threshold</source>
            <translation>Le BER mesuré dépasse le seuil de BER choisi</translation>
        </message>
        <message utf8="true">
            <source>LSS detected</source>
            <translation>LSS détecté</translation>
        </message>
        <message utf8="true">
            <source>FAS Errors detected</source>
            <translation>Erreurs FAS détectés</translation>
        </message>
        <message utf8="true">
            <source>Out of Frame detected</source>
            <translation>Détecté en dehors du trame</translation>
        </message>
        <message utf8="true">
            <source>Loss of Frame detected</source>
            <translation>Perte de trame détectée</translation>
        </message>
        <message utf8="true">
            <source>Frame Sync lost</source>
            <translation>Synchronisation de trame perdue</translation>
        </message>
        <message utf8="true">
            <source>Out of Logical Lane Marker detected</source>
            <translation>Détecté en dehors de marqueur de voie logique</translation>
        </message>
        <message utf8="true">
            <source>Logical Lane Marker Errors detected</source>
            <translation>Erreurs détectées de marqueur de voie logique</translation>
        </message>
        <message utf8="true">
            <source>Loss of Lane alignment detected</source>
            <translation>Perte d'alignement de voie détectée</translation>
        </message>
        <message utf8="true">
            <source>Lane Alignment lost</source>
            <translation>Alignement de voie perdu</translation>
        </message>
        <message utf8="true">
            <source>MFAS Errors detected</source>
            <translation>Erreurs détectées MFAS</translation>
        </message>
        <message utf8="true">
            <source>Out of Lane Alignment detected</source>
            <translation>Détecté en dehors d'alignement de voie</translation>
        </message>
        <message utf8="true">
            <source>OOMFAS detected</source>
            <translation>OOMFAS détecté</translation>
        </message>
        <message utf8="true">
            <source>Out of Recovery detected</source>
            <translation>Détecté en dehors de récouvrement</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew detected</source>
            <translation>Obliquité excessive détectée</translation>
        </message>
        <message utf8="true">
            <source>RS-FEC Calibration successful</source>
            <translation>Réussite de l'étalonnage RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>#1 uncorrectable bit errors detected.</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Error</source>
            <translation>Erreur</translation>
        </message>
        <message utf8="true">
            <source>Optics Test</source>
            <translation>Test d'optiques</translation>
        </message>
        <message utf8="true">
            <source>Test Type</source>
            <translation>Type de test</translation>
        </message>
        <message utf8="true">
            <source>Test utilizes 100GE RS-FEC</source>
            <translation>Le test utilise 100GE RS-FEC</translation>
        </message>
        <message utf8="true">
            <source>Optics Type</source>
            <translation>Type d' Optique</translation>
        </message>
        <message utf8="true">
            <source>Overall Test Verdict</source>
            <translation>Verdict de test global</translation>
        </message>
        <message utf8="true">
            <source>Signal Presence Test</source>
            <translation>Signal test de présence</translation>
        </message>
        <message utf8="true">
            <source>Optical Signal Level Test</source>
            <translation>Test de niveau signal optique</translation>
        </message>
        <message utf8="true">
            <source>Excessive Skew Test</source>
            <translation>Test d'obliquité Excessive</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Test</source>
            <translation>Test d'erreur de bit</translation>
        </message>
        <message utf8="true">
            <source>General Error Test</source>
            <translation>Test d'erreur générale</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Test</source>
            <translation>Test de seuil BER</translation>
        </message>
        <message utf8="true">
            <source>BER</source>
            <translation>BER</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC BER (corr + uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Post-FEC BER (uncorr.)</source>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>Optical Power</source>
            <translation>Puissance optique </translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1 (dBm)</source>
            <translation>Rx Niveau Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2 (dBm)</source>
            <translation>Rx Niveau Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3 (dBm)</source>
            <translation>Rx Niveau Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4 (dBm)</source>
            <translation>Rx Niveau Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5 (dBm)</source>
            <translation>Rx Niveau Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6 (dBm)</source>
            <translation>Rx Niveau Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7 (dBm)</source>
            <translation>Rx Niveau Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8 (dBm)</source>
            <translation>Rx Niveau Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9 (dBm)</source>
            <translation>Rx Niveau Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10 (dBm)</source>
            <translation>Rx Niveau Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum (dBm)</source>
            <translation>Rx Somme de niveau (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1 (dBm)</source>
            <translation>Tx Niveau Lambda #1 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2 (dBm)</source>
            <translation>Tx Niveau Lambda #2 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3 (dBm)</source>
            <translation>Tx Niveau Lambda #3 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4 (dBm)</source>
            <translation>Tx Niveau Lambda #4 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5 (dBm)</source>
            <translation>Tx Niveau Lambda #5 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6 (dBm)</source>
            <translation>Tx Niveau Lambda #6 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7 (dBm)</source>
            <translation>Tx Niveau Lambda #7 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8 (dBm)</source>
            <translation>Tx Niveau Lambda #8 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9 (dBm)</source>
            <translation>Tx Niveau Lambda #9 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10 (dBm)</source>
            <translation>Tx Niveau Lambda #10 (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum (dBm)</source>
            <translation>Tx Somme de niveau (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Setups</source>
            <translation>Configs</translation>
        </message>
        <message utf8="true">
            <source>Connect a short, clean patch cable between the Tx and Rx terminals of the connector you desire to test.</source>
            <translation>Connectez un câble de rallonge neuf et court entre les bornes Tx et Rx du connecteur que vous souhaitez tester.</translation>
        </message>
        <message utf8="true">
            <source>Test CFP&#xA;Optics</source>
            <translation>Test PCP&#xA;Optique</translation>
        </message>
        <message utf8="true">
            <source>Test CFP2&#xA;Optics/Slot</source>
            <translation>Test CFP2&#xA;Optiques/Slot</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>Annuler le Test</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP+&#xA;Optics</source>
            <translation>Test QSFP+ &#xA;Optique</translation>
        </message>
        <message utf8="true">
            <source>Test QSFP28&#xA;Optics</source>
            <translation>Test QSFP28&#xA;Optiques</translation>
        </message>
        <message utf8="true">
            <source>Setups:</source>
            <translation>Configs:</translation>
        </message>
        <message utf8="true">
            <source>Recommended</source>
            <translation>Recommandé</translation>
        </message>
        <message utf8="true">
            <source>5 Minutes</source>
            <translation>5 Minutes</translation>
        </message>
        <message utf8="true">
            <source>15 Minutes</source>
            <translation>15 Minutes</translation>
        </message>
        <message utf8="true">
            <source>4 Hours</source>
            <translation>4 Heures</translation>
        </message>
        <message utf8="true">
            <source>48 Hours</source>
            <translation>48 Heures</translation>
        </message>
        <message utf8="true">
            <source>User Duration (minutes)</source>
            <translation>Utilisateur Durée (minutes)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Duration (minutes)</source>
            <translation>Durée recommandée (minutes)</translation>
        </message>
        <message utf8="true">
            <source>The recommended time for this configuration is &lt;b>%1&lt;/b> minute(s).</source>
            <translation>Le temps recommandé pour cette configuration est &lt;b>%1&lt;/b> minute (s).</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold Type</source>
            <translation>Type de sueil BER</translation>
        </message>
        <message utf8="true">
            <source>Post-FEC</source>
            <translation>Post-FEC</translation>
        </message>
        <message utf8="true">
            <source>Pre-FEC</source>
            <translation>Pre-FEC</translation>
        </message>
        <message utf8="true">
            <source>BER Threshold</source>
            <translation>Seuil BER</translation>
        </message>
        <message utf8="true">
            <source>1x10^-15</source>
            <translation>1x10^-15</translation>
        </message>
        <message utf8="true">
            <source>1x10^-14</source>
            <translation>1x10^-14</translation>
        </message>
        <message utf8="true">
            <source>1x10^-13</source>
            <translation>1x10^-13</translation>
        </message>
        <message utf8="true">
            <source>1x10^-12</source>
            <translation>1x10^-12</translation>
        </message>
        <message utf8="true">
            <source>1x10^-11</source>
            <translation>1x10^-11</translation>
        </message>
        <message utf8="true">
            <source>1x10^-10</source>
            <translation>1x10^-10</translation>
        </message>
        <message utf8="true">
            <source>1x10^-9</source>
            <translation>1x10^-9</translation>
        </message>
        <message utf8="true">
            <source>Enable PPM Line Offset</source>
            <translation>Activer PPM ligne décalée</translation>
        </message>
        <message utf8="true">
            <source>PPM Max Offset (+/-)</source>
            <translation>Offset (+/-) max PPM</translation>
        </message>
        <message utf8="true">
            <source>Stop on Error</source>
            <translation>Arrêt sur erreur</translation>
        </message>
        <message utf8="true">
            <source>Results Overview</source>
            <translation>Aperçu des résultats</translation>
        </message>
        <message utf8="true">
            <source>Optics/slot Type</source>
            <translation>Type optique/slot</translation>
        </message>
        <message utf8="true">
            <source>Current PPM Offset</source>
            <translation>Offset PPM courant</translation>
        </message>
        <message utf8="true">
            <source>Current BER</source>
            <translation>BER actuel</translation>
        </message>
        <message utf8="true">
            <source>Optical Power (dBm)</source>
            <translation>Puissance optique (dBm)</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #1</source>
            <translation>Rx Niveau Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #2</source>
            <translation>Rx Niveau Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #3</source>
            <translation>Rx Niveau Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #4</source>
            <translation>Rx Niveau Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #5</source>
            <translation>Rx Niveau Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #6</source>
            <translation>Rx Niveau Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #7</source>
            <translation>Rx Niveau Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #8</source>
            <translation>Rx Niveau Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #9</source>
            <translation>Rx Niveau Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Lambda #10</source>
            <translation>Rx Niveau Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Rx Level Sum</source>
            <translation>Rx Somme de niveau</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #1</source>
            <translation>Tx Niveau Lambda #1</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #2</source>
            <translation>Tx Niveau Lambda #2</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #3</source>
            <translation>Tx Niveau Lambda #3</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #4</source>
            <translation>Tx Niveau Lambda #4</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #5</source>
            <translation>Tx Niveau Lambda #5</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #6</source>
            <translation>Tx Niveau Lambda #6</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #7</source>
            <translation>Tx Niveau Lambda #7</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #8</source>
            <translation>Tx Niveau Lambda #8</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #9</source>
            <translation>Tx Niveau Lambda #9</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Lambda #10</source>
            <translation>Tx Niveau Lambda #10</translation>
        </message>
        <message utf8="true">
            <source>Tx Level Sum</source>
            <translation>Tx Somme de niveau</translation>
        </message>
        <message utf8="true">
            <source>CFP Interface Details</source>
            <translation>Détails d'interface CFP</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Interface Details</source>
            <translation>Détails d'interface CFP2</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Interface Details</source>
            <translation>Détails d'interface CFP4</translation>
        </message>
        <message utf8="true">
            <source>QSFP Interface Details</source>
            <translation>Détails d'interface QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP+ Interface Details</source>
            <translation>QSFP+Détails d'interface</translation>
        </message>
        <message utf8="true">
            <source>QSFP28 Interface Details</source>
            <translation>QSFP28 détails d'interface</translation>
        </message>
        <message utf8="true">
            <source>No QSFP</source>
            <translation>Pas de QSFP</translation>
        </message>
        <message utf8="true">
            <source>Can't read QSFP - Please re-insert the QSFP</source>
            <translation>Ne peut pas lire le QSFP - Veuillez réinsérer le QSFP</translation>
        </message>
        <message utf8="true">
            <source>QSFP checksum error</source>
            <translation>Erreur de checksum QSFP</translation>
        </message>
        <message utf8="true">
            <source>Unable to interrogate required QSFP registers.</source>
            <translation>Impossible d' interroger les registres QSFP requis .</translation>
        </message>
        <message utf8="true">
            <source>Cannot confirm QSFP identity.</source>
            <translation>Ne peut pas confirmer l'identité QSFP.</translation>
        </message>
        <message utf8="true">
            <source>OTN Check</source>
            <translation>OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload BERT Test</source>
            <translation>Test Durée et charge BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency</source>
            <translation>Transparence GCC</translation>
        </message>
        <message utf8="true">
            <source>Select and Run Tests</source>
            <translation>Sélectionnez et lancer les tests</translation>
        </message>
        <message utf8="true">
            <source>Advanced Settings</source>
            <translation>Paramétrages avancés</translation>
        </message>
        <message utf8="true">
            <source>Run OTN Check Tests</source>
            <translation>Exécution tests OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT</source>
            <translation>Payload BERT</translation>
        </message>
        <message utf8="true">
            <source>Exit OTN Check Test</source>
            <translation>Quitter le test OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency</source>
            <translation>Fréquence de mesure</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>Etat de l'Auto Négociation</translation>
        </message>
        <message utf8="true">
            <source>RTD Configuration</source>
            <translation>Configuration RTD</translation>
        </message>
        <message utf8="true">
            <source>All Lanes</source>
            <translation>Toutes les voies</translation>
        </message>
        <message utf8="true">
            <source>OTN Check:</source>
            <translation>OTN Check</translation>
        </message>
        <message utf8="true">
            <source>*** Starting OTN Check Test ***</source>
            <translation>*** Démarrage du test OTN Check ***</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT bit error detected</source>
            <translation>Erreur de charge utile de bit BERT détectée</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 Payload BERT bit errors detected</source>
            <translation>Plus de 100,000 charge utile erreurs de bit BERT détectés</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT BER threshold exceeded</source>
            <translation>Charge utile de seuil BER BERT dépassée</translation>
        </message>
        <message utf8="true">
            <source>#1 Payload BERT bit errors detected</source>
            <translation>#1 erreurs de bit BERT charge utile détectée</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Bit Error Rate: #1</source>
            <translation>Taux d'erreur de bit BERT charge utile:#1</translation>
        </message>
        <message utf8="true">
            <source>RTD threshold exceeded</source>
            <translation>Seuil RTD dépassé</translation>
        </message>
        <message utf8="true">
            <source>#1: #2 - RTD: Min: #3, Max: #4, Avg: #5</source>
            <translation>#1: #2 - RTD: Min: #3, Max: #4, Moy.: #5</translation>
        </message>
        <message utf8="true">
            <source>#1: RTD unavailable</source>
            <translation>#1: RTD non-disponible</translation>
        </message>
        <message utf8="true">
            <source>Running Payload BERT test</source>
            <translation>Test de charge BERT en exécution</translation>
        </message>
        <message utf8="true">
            <source>Running RTD test</source>
            <translation>Test RTD (latence en boucle) en exécution</translation>
        </message>
        <message utf8="true">
            <source>Running GCC Transparency test</source>
            <translation>Test de transparence GCC en exécution</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error detected</source>
            <translation>Bit en erreur GCC détecté</translation>
        </message>
        <message utf8="true">
            <source>GCC BER threshold exceeded</source>
            <translation>Seuil dépassé GCC BER</translation>
        </message>
        <message utf8="true">
            <source>#1 GCC bit errors detected</source>
            <translation>#1 GCC bits en erreur détectés</translation>
        </message>
        <message utf8="true">
            <source>More than 100,000 GCC bit errors detected</source>
            <translation>Plus de 100,000 bits en erreur GCC détectés</translation>
        </message>
        <message utf8="true">
            <source>GCC bit error rate: #1</source>
            <translation>Taux dd bits en erreur GCC:#1</translation>
        </message>
        <message utf8="true">
            <source>*** Starting Loopback Check ***</source>
            <translation>***démarrage vérification boucleretour***</translation>
        </message>
        <message utf8="true">
            <source>*** Skipping Loopback Check ***</source>
            <translation>***ignorer vérification boucleretour***</translation>
        </message>
        <message utf8="true">
            <source>*** Loopback Check Finished ***</source>
            <translation>***vérification boucleretour terminée***</translation>
        </message>
        <message utf8="true">
            <source>Loopback detected</source>
            <translation>Boucleretour Détectée</translation>
        </message>
        <message utf8="true">
            <source>No loopback detected</source>
            <translation>Pas de boucle de retour détectée</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is unavailable</source>
            <translation>Le canal #1 est non-disponible</translation>
        </message>
        <message utf8="true">
            <source>Channel #1 is now available</source>
            <translation>Le canal #1 est à présent disponible</translation>
        </message>
        <message utf8="true">
            <source>Loss of pattern sync.</source>
            <translation>Perte de sync. de motif PRBS</translation>
        </message>
        <message utf8="true">
            <source>Loss of GCC pattern sync.</source>
            <translation>Perte de sync. de motif GCC</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: A Bit Error was detected.</source>
            <translation>test annulé: Une erreur de bit a été détectée</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The BER has exceeded the configured threshold.</source>
            <translation>Test échoué: le BER a dépassé le seuil configuré</translation>
        </message>
        <message utf8="true">
            <source>Test failed: The RTD has exceeded the configured threshold.</source>
            <translation>Test échoué: le RTD a dépassé le seuil configuré</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No loopback detected.</source>
            <translation>Test annulé: pas de boucle de retour détectée</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: No optical signal present.</source>
            <translation>Test annulé: pas de signal d'optique présent</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame.</source>
            <translation>Test annulé: perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of frame sync.</source>
            <translation>Test annulé: perte de sync de trame</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of pattern sync.</source>
            <translation>Test annulé: perte de sync. De motif   </translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of GCC pattern sync.</source>
            <translation>Test annulé: perte de sync. De motif GCC</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of lane alignment.</source>
            <translation>Test annulé: Perte d'alignement de voie</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: Loss of marker lock.</source>
            <translation>Test annulé: perte de verrou marqueur</translation>
        </message>
        <message utf8="true">
            <source>At least 1 channel must be selected.</source>
            <translation>Au moins 1 canal doit être sélectionné</translation>
        </message>
        <message utf8="true">
            <source>Loopback not checked</source>
            <translation>boucleretour non vérifiée</translation>
        </message>
        <message utf8="true">
            <source>Loopback not detected</source>
            <translation>boucleretour non détectée</translation>
        </message>
        <message utf8="true">
            <source>Test Selection</source>
            <translation>Sélection de test</translation>
        </message>
        <message utf8="true">
            <source>Test Planned Duration</source>
            <translation>Durée planifiée de test</translation>
        </message>
        <message utf8="true">
            <source>Test Run Time</source>
            <translation>Durée d'exécution de test</translation>
        </message>
        <message utf8="true">
            <source>OTN Check requires a traffic loopback to execute; this loopback is required at the far-end of the OTN circuit.</source>
            <translation>La vérif. OTN Check nécessite d'exécuter une boucle retour de trafic: cette boucle de retour est nécessaire à l'extrémité du circuit OTN</translation>
        </message>
        <message utf8="true">
            <source>OTN Check Tests</source>
            <translation>Tests OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Optics Offset, Signal Mapping</source>
            <translation>Décalage optique, Cartographie du signal</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping</source>
            <translation>Cartographie signal</translation>
        </message>
        <message utf8="true">
            <source>Signal Mapping and Optics Selection</source>
            <translation>Cartographie signal et sélection d'optiques</translation>
        </message>
        <message utf8="true">
            <source>Advanced Cfg</source>
            <translation>Cfg avancée</translation>
        </message>
        <message utf8="true">
            <source>Signal Structure</source>
            <translation>Structure du Signal</translation>
        </message>
        <message utf8="true">
            <source>QSFP Optics RTD Offset (us)</source>
            <translation>QSFP Optiques RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP Optics RTD Offset (us)</source>
            <translation>Décalage de latence en boucle du au module CFP (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP2 Optics RTD Offset (us)</source>
            <translation>CFP2 Optiques RTD Offset (us)</translation>
        </message>
        <message utf8="true">
            <source>CFP4 Optics RTD Offset (us)</source>
            <translation>Offset (décalage) RTD optiques CFPS (us)</translation>
        </message>
        <message utf8="true">
            <source>Configure Duration and Payload BERT Test</source>
            <translation>Configurer Durée et Test BERT de charge</translation>
        </message>
        <message utf8="true">
            <source>Duration and Payload Bert Cfg</source>
            <translation>Cfg Durée et charge Bert</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Setups</source>
            <translation>Configurations charge BERT</translation>
        </message>
        <message utf8="true">
            <source>PRBS</source>
            <translation>PRBS</translation>
        </message>
        <message utf8="true">
            <source>Confidence Level (%)</source>
            <translation>Niveau de confiance (%)</translation>
        </message>
        <message utf8="true">
            <source>Based on the line rate, BER Threshold, and Confidence Level, the recommended test time is &lt;b>%1&lt;/b>.</source>
            <translation>Basé sur taux de ligne, seuil BER, et Niveau de confiance, la durée recommandée du test est &lt;b>%1&lt;/b></translation>
        </message>
        <message utf8="true">
            <source>Pattern</source>
            <translation>Séquence</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern</source>
            <translation>Séquence BERT</translation>
        </message>
        <message utf8="true">
            <source>2^9-1</source>
            <translation>2^9-1</translation>
        </message>
        <message utf8="true">
            <source>2^9-1 Inv</source>
            <translation>2^9-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^11-1</source>
            <translation>2^11-1</translation>
        </message>
        <message utf8="true">
            <source>2^11-1 Inv</source>
            <translation>2^11-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^15-1</source>
            <translation>2^15-1</translation>
        </message>
        <message utf8="true">
            <source>2^15-1 Inv</source>
            <translation>2^15-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1</source>
            <translation>2^23-1</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv</source>
            <translation>2^23-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 ANSI</source>
            <translation>2^23-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^23-1 Inv ANSI</source>
            <translation>2^23-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1</source>
            <translation>2^31-1</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv</source>
            <translation>2^31-1 Inv</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 ANSI</source>
            <translation>2^31-1 ANSI</translation>
        </message>
        <message utf8="true">
            <source>2^31-1 Inv ANSI</source>
            <translation>2^31-1 Inv ANSI</translation>
        </message>
        <message utf8="true">
            <source>Error Threshold</source>
            <translation>Seuil erreur</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail</source>
            <translation>Afficher Réussi/Echec</translation>
        </message>
        <message utf8="true">
            <source>99</source>
            <translation>99</translation>
        </message>
        <message utf8="true">
            <source>95</source>
            <translation>95</translation>
        </message>
        <message utf8="true">
            <source>90</source>
            <translation>90</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Setups</source>
            <translation>Configurations de Latence en boucle</translation>
        </message>
        <message utf8="true">
            <source>RTD Cfg</source>
            <translation>Cfg RTD</translation>
        </message>
        <message utf8="true">
            <source>Include</source>
            <translation>Inclure</translation>
        </message>
        <message utf8="true">
            <source>Threshold (ms)</source>
            <translation>Seuil (ms)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Frequency (s)</source>
            <translation>Fréquence de mesure (s)</translation>
        </message>
        <message utf8="true">
            <source>30</source>
            <translation>30</translation>
        </message>
        <message utf8="true">
            <source>60</source>
            <translation>60</translation>
        </message>
        <message utf8="true">
            <source>GCC Cfg</source>
            <translation>Cfg GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Setups</source>
            <translation>Configurations de transparence GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC Channel</source>
            <translation>Canal GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BER Threshold</source>
            <translation>Seuil GCC BER</translation>
        </message>
        <message utf8="true">
            <source>GCC0 (OTU)</source>
            <translation>GCC0 (OTU)</translation>
        </message>
        <message utf8="true">
            <source>GCC1 (ODU)</source>
            <translation>GCC1 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>GCC2 (ODU)</source>
            <translation>GCC2 (ODU)</translation>
        </message>
        <message utf8="true">
            <source>1x10^-8</source>
            <translation>1x10^-8</translation>
        </message>
        <message utf8="true">
            <source>BERT Pattern 2^23-1 is used in GCC.</source>
            <translation>Motif BERT 2^23-1 utilisé dans GCC</translation>
        </message>
        <message utf8="true">
            <source>Loopback Check</source>
            <translation>Vérification Boucle retour (loopback)</translation>
        </message>
        <message utf8="true">
            <source>Skip Loopback Check</source>
            <translation>Ignorer vérification boucle de retour</translation>
        </message>
        <message utf8="true">
            <source>Press the "Start" button to begin loopback check.</source>
            <translation>Appuyez sur le bouton 'démarrer' pour commencer une vérification de boucle de retour</translation>
        </message>
        <message utf8="true">
            <source>Skip OTN Check Tests</source>
            <translation>Sauter tests de vérif OTN Check</translation>
        </message>
        <message utf8="true">
            <source>Checking Loopback</source>
            <translation>Vérification boucleretour</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detected</source>
            <translation>Boucleretour Détectée</translation>
        </message>
        <message utf8="true">
            <source>Skip loopback check</source>
            <translation>Ignorer vérification boucle de retour</translation>
        </message>
        <message utf8="true">
            <source>Loopback Detection</source>
            <translation>détection de boucleretour</translation>
        </message>
        <message utf8="true">
            <source>Loopback detection</source>
            <translation>détection de boucleretour</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Results</source>
            <translation>Résultats charge BERT</translation>
        </message>
        <message utf8="true">
            <source>Measured BER</source>
            <translation>BER mesuré</translation>
        </message>
        <message utf8="true">
            <source>Bit Error Count</source>
            <translation>Compte d'erreurs de bit</translation>
        </message>
        <message utf8="true">
            <source>Verdict</source>
            <translation>Verdict</translation>
        </message>
        <message utf8="true">
            <source>Payload BERT Configuration</source>
            <translation>Configuration charge BERT</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Results</source>
            <translation>Résultats de Latence en boucle</translation>
        </message>
        <message utf8="true">
            <source>Min (ms)</source>
            <translation>Min (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max (ms)</source>
            <translation>Max (ms)</translation>
        </message>
        <message utf8="true">
            <source>Avg (ms)</source>
            <translation>Moyenne (ms)</translation>
        </message>
        <message utf8="true">
            <source>Note: Fail condition occurs when the average RTD exceeds the threshold.</source>
            <translation>remarque: condition d'échec survenue lorsque la moyenne de latence en boucle dépasse le seuil</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Configuration</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Delay Configuration</source>
            <translation>Configuration de latence en boucle</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Results</source>
            <translation>Résultats de transparence GCC</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Error Rate</source>
            <translation>Taux d'erreur des bits  GCC BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC BERT Bit Errors</source>
            <translation>Erreurs des bits GCC BERT</translation>
        </message>
        <message utf8="true">
            <source>GCC Transparency Configuration</source>
            <translation>Configuration de transparence GCC</translation>
        </message>
        <message utf8="true">
            <source>Protocol Analysis</source>
            <translation>Analyse de protocole</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Capture</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze CDP</source>
            <translation>Capture et analyse CDP</translation>
        </message>
        <message utf8="true">
            <source>Capture and Analyze</source>
            <translation>Capture et analyse</translation>
        </message>
        <message utf8="true">
            <source>Select Protocol to Analyze</source>
            <translation>Sélectionnez le protocole à analyser</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Analysis</source>
            <translation>Analyse&#xA;démarrage</translation>
        </message>
        <message utf8="true">
            <source>Abort&#xA;Analysis</source>
            <translation>Abandonner&#xA;l'analyse</translation>
        </message>
        <message utf8="true">
            <source>Analysis</source>
            <translation>Analyse</translation>
        </message>
        <message utf8="true">
            <source>Expert PTP</source>
            <translation>PTP expert</translation>
        </message>
        <message utf8="true">
            <source>Configure Test Settings Manually </source>
            <translation>Configuration des réglages de test manuellement </translation>
        </message>
        <message utf8="true">
            <source>Thresholds</source>
            <translation>Seuils</translation>
        </message>
        <message utf8="true">
            <source>Run Quick Check</source>
            <translation>Exécuter rapide vérification</translation>
        </message>
        <message utf8="true">
            <source>Exit Expert PTP Test</source>
            <translation>Quitter test expert PTP</translation>
        </message>
        <message utf8="true">
            <source>Link is no longer active.</source>
            <translation>Le lien n'est plus actif</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay Time Source Synchronization has been lost.</source>
            <translation>La synchronisation de source de temps de délai à une voie a été perdue</translation>
        </message>
        <message utf8="true">
            <source>The PTP Slave Session stopped unexpectedly.</source>
            <translation>La session esclave PTP a été arrêtée de manière inattendue.</translation>
        </message>
        <message utf8="true">
            <source>Time Source Synchronization is not present. Please verify that your time source is properly configured and connected.</source>
            <translation>La synchronisation de source de temps n'est pas présente. Veuillez vérifier que votre source de temps est correctement configurée et connectée.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish a PTP Slave session.</source>
            <translation>Impossible d'établir une session esclave PTP</translation>
        </message>
        <message utf8="true">
            <source>No GPS Receiver detected.</source>
            <translation>Pas de récepteur GPS détecté.</translation>
        </message>
        <message utf8="true">
            <source>TOS Type</source>
            <translation>Type de TOS</translation>
        </message>
        <message utf8="true">
            <source>Announce Rx Timeout</source>
            <translation>Annonce du délai d'attente Rx</translation>
        </message>
        <message utf8="true">
            <source>Announce</source>
            <translation>Annonce</translation>
        </message>
        <message utf8="true">
            <source>128 per second</source>
            <translation>128 par seconde</translation>
        </message>
        <message utf8="true">
            <source>64 per second</source>
            <translation>64 par seconde</translation>
        </message>
        <message utf8="true">
            <source>32 per second</source>
            <translation>32 par seconde</translation>
        </message>
        <message utf8="true">
            <source>16 per second</source>
            <translation>16 par seconde</translation>
        </message>
        <message utf8="true">
            <source>8 per second</source>
            <translation>8 par seconde</translation>
        </message>
        <message utf8="true">
            <source>4 per second</source>
            <translation>4 par seconde</translation>
        </message>
        <message utf8="true">
            <source>2 per second</source>
            <translation>2 par seconde</translation>
        </message>
        <message utf8="true">
            <source>1 per second</source>
            <translation>1 par seconde</translation>
        </message>
        <message utf8="true">
            <source>Sync</source>
            <translation>Synchro</translation>
        </message>
        <message utf8="true">
            <source>Delay Request</source>
            <translation>Demande de délai</translation>
        </message>
        <message utf8="true">
            <source>Lease Duration (s)</source>
            <translation>Durée de bail (s)</translation>
        </message>
        <message utf8="true">
            <source>Enable</source>
            <translation>Activer</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold Enable</source>
            <translation>Activer seuil max erreur heure</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Time Error Max. (ns)</source>
            <translation>Max d'erreur de temps (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Max. Threshold (ns)</source>
            <translation>Seuil max d'erreur de temps (ns)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Duration (minutes)</source>
            <translation>Durée du Test (minutes)</translation>
        </message>
        <message utf8="true">
            <source>Quick Check</source>
            <translation>Vérification rapide</translation>
        </message>
        <message utf8="true">
            <source>Master IP</source>
            <translation>IP maître</translation>
        </message>
        <message utf8="true">
            <source>PTP Domain</source>
            <translation>Domaine PTP</translation>
        </message>
        <message utf8="true">
            <source>Start&#xA;Check</source>
            <translation>Démarrer&#xA;Vérifier</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Session</source>
            <translation>Démarrage&#xA;Session</translation>
        </message>
        <message utf8="true">
            <source>Session&#xA;Established</source>
            <translation>Session&#xA;Etablie</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum Threshold (ns)</source>
            <translation>Max d'erreur de temps (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error Maximum (ns)</source>
            <translation>Max d'erreur de temps (ns)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us)</source>
            <translation>Seuil max d'erreur de temps (us)</translation>
        </message>
        <message utf8="true">
            <source>Time Error, Cur (us) vs. Time</source>
            <translation>Erreur de temps, courante (us) vs temps</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>Résultats de test</translation>
        </message>
        <message utf8="true">
            <source>Duration (minutes)</source>
            <translation>Durée (minutes)</translation>
        </message>
        <message utf8="true">
            <source>PTP Check</source>
            <translation>PTP Check</translation>
        </message>
        <message utf8="true">
            <source>PTP Check Test</source>
            <translation>Test PTP Check</translation>
        </message>
        <message utf8="true">
            <source>End: PTP Check</source>
            <translation>Fin: vérification PTP</translation>
        </message>
        <message utf8="true">
            <source>Start another test</source>
            <translation>Démarrer un autre test</translation>
        </message>
        <message utf8="true">
            <source>Exit PTP Check</source>
            <translation>Quitter Vérification PTP</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544</source>
            <translation>RFC 2544 amélioré</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Invalid Settings</source>
            <translation>Paramètres non valides</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Local</source>
            <translation>Paramétrages avancés IP - Local</translation>
        </message>
        <message utf8="true">
            <source>Advanced RTD Latency Settings</source>
            <translation>Paramétrages avancés de latence RTD</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst (CBS) Test Settings</source>
            <translation>Paramétrages avancés de test de salve (CBS)</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>Lancer J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck Settings</source>
            <translation>Paramètres J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Jitter</source>
            <translation>Gigue</translation>
        </message>
        <message utf8="true">
            <source>Extended Load</source>
            <translation>charge étendue</translation>
        </message>
        <message utf8="true">
            <source>Exit RFC 2544 Test</source>
            <translation>Sortie test RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC2544:</source>
            <translation>RFC2544:</translation>
        </message>
        <message utf8="true">
            <source>Local Network Configuration</source>
            <translation>Configuration de réseau local</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Configuration</source>
            <translation>Configuration du réseau distant</translation>
        </message>
        <message utf8="true">
            <source>Local Auto Negotiation Status</source>
            <translation>Statut de négociation automatique locale</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>Vitesse (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>Duplex</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>Contrôle de Flux</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>Supporte FDX</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>Supporte HDX</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>Supporte les Trames Pause</translation>
        </message>
        <message utf8="true">
            <source>Remote Auto Negotiation Status</source>
            <translation>Statut de négociation automatique à distance</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>Half</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>Full</translation>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>100</source>
            <translation>100</translation>
        </message>
        <message utf8="true">
            <source>1000</source>
            <translation>1000</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>Aucune</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Rx et Tx</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx Seulement</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx Seulement</translation>
        </message>
        <message utf8="true">
            <source>RTD Frame Rate</source>
            <translation>Débit de trame RTD</translation>
        </message>
        <message utf8="true">
            <source>1 Frame per Second</source>
            <translation>1 trame par seconde</translation>
        </message>
        <message utf8="true">
            <source>10 Frames per Second</source>
            <translation>10 trames par seconde</translation>
        </message>
        <message utf8="true">
            <source>Burst Cfg</source>
            <translation>Cgf Salve</translation>
        </message>
        <message utf8="true">
            <source>Committed Burst Size</source>
            <translation>Taille de salve engagée</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing (MEF 34)</source>
            <translation>Contrôle CBS (MEF 34)</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt</source>
            <translation>Recherche salve</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS (kB)</source>
            <translation>CBS descendant (kB)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Sizes (kB)</source>
            <translation>Tailles de salve en aval (kO)</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>Minimum</translation>
        </message>
        <message utf8="true">
            <source>Maximum</source>
            <translation>Maximum</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS (kB)</source>
            <translation>CBS ascendant (kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Sizes (kB)</source>
            <translation>Tailles de salve en amont (kO)</translation>
        </message>
        <message utf8="true">
            <source>CBS (kB)</source>
            <translation>CBS (ko)</translation>
        </message>
        <message utf8="true">
            <source>Burst Sizes (kB)</source>
            <translation>Tailles de salve (kO)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Settings</source>
            <translation>Définir paramétrages avancés CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced CBS Policing Settings</source>
            <translation>Définit paramétrages avancés de contrôle CBS</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Hunt Settings</source>
            <translation>Définir paramétrages avancés de recherche de salve</translation>
        </message>
        <message utf8="true">
            <source>Tolerance</source>
            <translation>Tolérance</translation>
        </message>
        <message utf8="true">
            <source>- %</source>
            <translation>- %</translation>
        </message>
        <message utf8="true">
            <source>+ %</source>
            <translation>+ %</translation>
        </message>
        <message utf8="true">
            <source>Extended Load Cfg</source>
            <translation>Cfg de charge prolongée</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling (%)</source>
            <translation>Mise à l'échelle du débit (%)</translation>
        </message>
        <message utf8="true">
            <source>RFC2544 has the following invalid configuration settings:</source>
            <translation>RFC2544 a les paramètres de configuration non valides suivants :</translation>
        </message>
        <message utf8="true">
            <source>Traffic connectivity has been successfully verified. Running the load test.</source>
            <translation>La connectivité du trafic a été vérifiée avec succès. Exécution en cours du test de charge.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run at line rate.</source>
            <translation>Appuyez sur "Start" pour exécuter à fréquence de ligne.</translation>
        </message>
        <message utf8="true">
            <source>Press "Start" to run using configured RFC 2544 bandwidth.</source>
            <translation>Appuyez sur "Start" pour lancer l'aide configuré RFC 2544 bande passante.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput will NOT be used for RFC 2544 tests.</source>
            <translation>Le débit mesuré Ne sera PAS utilisé pour les tests RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput WILL be used for RFC 2544 tests.</source>
            <translation>Le débit mesuré SERA utilisé pour les tests RFC 2544.</translation>
        </message>
        <message utf8="true">
            <source>Load Test frame size: %1 bytes.</source>
            <translation>Taille de trame de test de charge : %1 octets.</translation>
        </message>
        <message utf8="true">
            <source>Load Test packet size: %1 bytes.</source>
            <translation>Taille de paquet de test de charge : %1 octets.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test frame size: %1 bytes.</source>
            <translation>Taille de trame de test de charge en amont : %1 octets.</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test frame size: %1 bytes.</source>
            <translation>Taille de trame de test de charge en aval : %1 octets.</translation>
        </message>
        <message utf8="true">
            <source>Measured Throughput:</source>
            <translation>Débit mesuré:</translation>
        </message>
        <message utf8="true">
            <source>Test using configured RFC 2544 Max Bandwidth</source>
            <translation>Testez en utilisant Bande passante maximale RFC 2544 configurée</translation>
        </message>
        <message utf8="true">
            <source>Use the Measured Throughput measurement as the RFC 2544 Max Bandwidth</source>
            <translation>Utilisez la mesure du débit mesuré comme le RFC 2544 Bande passante maximale</translation>
        </message>
        <message utf8="true">
            <source>Load Test Frame Size (bytes)</source>
            <translation>Taille de trame de test de charge (octets)</translation>
        </message>
        <message utf8="true">
            <source>Load Test Packet Size (bytes)</source>
            <translation>Taille de paquet de test de charge (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Load Test Frame Size (bytes)</source>
            <translation>Taille de trame de test de charge en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Load Test Frame Size (bytes)</source>
            <translation>Taille de trame de test de charge en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Tests</source>
            <translation>Exécutez tests RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>Skip RFC 2544 Tests</source>
            <translation>Passer RFC 2544 Tests</translation>
        </message>
        <message utf8="true">
            <source>Checking Hardware Loop</source>
            <translation>Vérification de la boucle matérielle</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>Gigue Paquet à tester</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Graph</source>
            <translation>Graphique de test de gigue</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Graph</source>
            <translation>Graphique de test de gigue en amont</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test Results</source>
            <translation>Résultats de test de gigue</translation>
        </message>
        <message utf8="true">
            <source>Upstream Jitter Test Results</source>
            <translation>Résultats de test de gigue en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Graph</source>
            <translation>Graphique de test de gigue en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Jitter Test Results</source>
            <translation>Résultats de test de gigue en aval</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter (us)</source>
            <translation>Max moyen Jitter ( nous )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Jitter</source>
            <translation>Moyenne de gigue max</translation>
        </message>
        <message utf8="true">
            <source>Max Avg&#xA;Jitter (us)</source>
            <translation>Moyenne de&#xA;gigue max (us)</translation>
        </message>
        <message utf8="true">
            <source>CBS Test Results</source>
            <translation>Résultats de test CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Test Results</source>
            <translation>Résultats de test CBS en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Test Results</source>
            <translation>Résultats de test CBS en aval</translation>
        </message>
        <message utf8="true">
            <source>Burst Hunt Test Results</source>
            <translation>Résultats de test de recherche salve</translation>
        </message>
        <message utf8="true">
            <source>Upstream Burst Hunt Test Results</source>
            <translation>Résultats de test de recherche de salve en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream Burst Hunt Test Results</source>
            <translation>Résultats de test de recherche de salve en aval</translation>
        </message>
        <message utf8="true">
            <source>CBS Policing Test Results</source>
            <translation>Résultats de test de contrôle CBS</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Policing Test Results</source>
            <translation>Résultats de test de contrôle CBS en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Policing Test Results</source>
            <translation>Résultats de test de contrôle CBS en aval</translation>
        </message>
        <message utf8="true">
            <source>Burst (CBS Policing) Test</source>
            <translation>Test de salve (contrôle CBS)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 Mbps)</source>
            <translation>CIR&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 Mbps)</source>
            <translation>CIR&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L1 kbps)</source>
            <translation>CIR&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(L2 kbps)</source>
            <translation>CIR&#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR&#xA;(%)</source>
            <translation>CIR&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Burst&#xA;Size (kB)</source>
            <translation>Cfg taille salve&#xA;(kO)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Size (kB)</source>
            <translation>Taille de salve&#xA; Tx (kO)</translation>
        </message>
        <message utf8="true">
            <source>Average Rx&#xA;Burst Size (kB)</source>
            <translation>Taille de salve Rx&#xA;moyen (kO)</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Sent</source>
            <translation>Trames&#xA;envoyées</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;Received</source>
            <translation>Trames&#xA;reçues</translation>
        </message>
        <message utf8="true">
            <source>Lost&#xA;Frames</source>
            <translation>Perte de&#xA;Trames</translation>
        </message>
        <message utf8="true">
            <source>Burst Size&#xA;(kB)</source>
            <translation>Taille de salve&#xA;(kO)</translation>
        </message>
        <message utf8="true">
            <source>Latency&#xA;(us)</source>
            <translation>Latence&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Jitter&#xA;(us)</source>
            <translation>Gigue&#xA;(us)</translation>
        </message>
        <message utf8="true">
            <source>Configured&#xA;Burst Size (kB)</source>
            <translation>Taille de rafale&#xA;configurée (ko)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst&#xA;Policing Size (kB)</source>
            <translation>Taille de contrôle de salve&#xA; Tx (kO)</translation>
        </message>
        <message utf8="true">
            <source>Estimated&#xA;CBS (kB)</source>
            <translation>Estimée&#xA;CBS (kB)</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Graph</source>
            <translation>Graphique test de Recovery System (système de recouvrement)</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Graph</source>
            <translation>Graphique de test de Récupération système en amont</translation>
        </message>
        <message utf8="true">
            <source>System Recovery Test Results</source>
            <translation>Résultats du test de récupération du système</translation>
        </message>
        <message utf8="true">
            <source>Upstream System Recovery Test Results</source>
            <translation>Résultats de test de récupération système en amont</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Graph</source>
            <translation>Graphique de test de Récupération système en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream System Recovery Test Results</source>
            <translation>Résultats de test de récupération système en aval</translation>
        </message>
        <message utf8="true">
            <source>Recovery Time (us)</source>
            <translation>Temps de récupération ( nous )</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 Mbps)</source>
            <translation>Débit de surcharge&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 Mbps)</source>
            <translation>Débit de surcharge&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L1 kbps)</source>
            <translation>Débit de surcharge&#xA;(C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(L2 kbps)</source>
            <translation>Débit de surcharge&#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Overload Rate&#xA;(%)</source>
            <translation>Taux de surcharge&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 Mbps)</source>
            <translation>Récupération débit&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 Mbps)</source>
            <translation>Récupération débit&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L1 kbps)</source>
            <translation>Débit&#xA;de recouvrement (C1 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(L2 kbps)</source>
            <translation>Récupération débit&#xA;(C2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Recovery Rate&#xA;(%)</source>
            <translation>Taux de récupération&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Average Recovery&#xA;Time (us)</source>
            <translation>Récupération moyenne&#xA;Durée (us)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Test</source>
            <translation>Test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed</source>
            <translation>TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Mode</source>
            <translation>Mode</translation>
        </message>
        <message utf8="true">
            <source>Controls</source>
            <translation>Contrôles</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls</source>
            <translation>Contrôles de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Shaping</source>
            <translation>Formation</translation>
        </message>
        <message utf8="true">
            <source>Step Config</source>
            <translation>Config de l'étape </translation>
        </message>
        <message utf8="true">
            <source>Select Steps</source>
            <translation>Sélectionnez les étapes</translation>
        </message>
        <message utf8="true">
            <source>Path MTU</source>
            <translation>Path MTU</translation>
        </message>
        <message utf8="true">
            <source>RTT</source>
            <translation>RTT</translation>
        </message>
        <message utf8="true">
            <source>Walk the Window</source>
            <translation>Walk the Window</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput</source>
            <translation>TCP Largeur de Bande</translation>
        </message>
        <message utf8="true">
            <source>Advanced TCP</source>
            <translation>TCP avancé</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Sauver</translation>
        </message>
        <message utf8="true">
            <source>Connection Settings</source>
            <translation>Réglages de connexion</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Ctl (Advanced)</source>
            <translation>Ctl de vrai vitesse (avancé)</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Units</source>
            <translation>Choisir Unités Bc</translation>
        </message>
        <message utf8="true">
            <source>Walk Window</source>
            <translation>Examinez fenêtre</translation>
        </message>
        <message utf8="true">
            <source>Exit TrueSpeed Test</source>
            <translation>Quitter le test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP host failed to establish a connection. The current test has been aborted.</source>
            <translation>TCP hôte n'a pas réussi à établir une connexion. Le test actuel a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>By selecting Troubleshoot mode, you have been disconnected from the remote unit.</source>
            <translation>En sélectionnant le mode dépannage, vous avez été déconnecté de l'unité distante.</translation>
        </message>
        <message utf8="true">
            <source>Invalid test selection: At least one test must be selected to run TrueSpeed.</source>
            <translation>Sélection du test invalide : au moins un test doit être sélectionné pour lancer TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>Le MTU mesurée est trop faible pour continuer. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>Your file transmitted too quickly! Please choose a file size for TCP throughput so the test runs for at least 5 seconds. It is recommended that the test should run for at least 10 seconds.</source>
            <translation>Votre fichier a été transféré trop vite ! Veuillez choisir une taille de fichier pour le débit TCP afin que le test s'exécute pendant au moins 5 secondes. Il est recommandé que le test doit fonctionner pendant au moins 10 secondes.</translation>
        </message>
        <message utf8="true">
            <source>Maximum re-transmit attempts reached. Test aborted.</source>
            <translation>Le nombre maximum de tentatives de retransmission est atteint. Test abandonné.</translation>
        </message>
        <message utf8="true">
            <source>TCP host has encountered an error. The current test has been aborted.</source>
            <translation>TCP hôte a rencontré une erreur. Le test actuel a été abandonné.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed:</source>
            <translation>TrueSpeed:</translation>
        </message>
        <message utf8="true">
            <source>Starting TrueSpeed test.</source>
            <translation>Démarrage du test TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on the MTU size.</source>
            <translation>Calibrage sur la taille MTU.</translation>
        </message>
        <message utf8="true">
            <source>Testing #1 byte MTU with #2 byte MSS.</source>
            <translation>Test #1 octets MTU avec #2 octets MSS.</translation>
        </message>
        <message utf8="true">
            <source>The Path MTU was determined to be #1 bytes. This equates to an MSS of #2 bytes.</source>
            <translation>Le chemin MTU a été défini à #1 octets. Cela équivaut à une MSS #2 octets.</translation>
        </message>
        <message utf8="true">
            <source>Performing RTT test. This will take #1 seconds.</source>
            <translation>Test RTT en cours. Ceci prendra #1 secondes.</translation>
        </message>
        <message utf8="true">
            <source>The Round-trip Time (RTT) was determined to be #1 msec.</source>
            <translation>Le temps aller-retour (RTT) a été déterminé pour être #1 msec.</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream Walk-the-Window test.</source>
            <translation>Test ascendant de parcours de fenêtre en cours.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream Walk-the-Window test.</source>
            <translation>Test descendant de parcours de fenêtre en cours.</translation>
        </message>
        <message utf8="true">
            <source>Sending #1 bytes of TCP traffic.</source>
            <translation>Envoie #1 octets du trafic TCP.</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Unshaped</source>
            <translation>Trafic de sortie local: non formé</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Unshaped</source>
            <translation>Trafic de sortie distant: non formé</translation>
        </message>
        <message utf8="true">
            <source>Local egress traffic: Shaped</source>
            <translation>Trafic de sortie local: formé</translation>
        </message>
        <message utf8="true">
            <source>Remote egress traffic: Shaped</source>
            <translation>Trafic de sortie distant: formé</translation>
        </message>
        <message utf8="true">
            <source>Duration (sec): #1</source>
            <translation>Durée (sec):#1</translation>
        </message>
        <message utf8="true">
            <source>Connections: #1</source>
            <translation>Connexions:#1</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB): #1 </source>
            <translation>Taille de fenêtre (kB): #1</translation>
        </message>
        <message utf8="true">
            <source>Performing upstream TCP Throughput test.</source>
            <translation>Exécution de test de débit TCP ascendant.</translation>
        </message>
        <message utf8="true">
            <source>Performing downstream TCP Throughput test.</source>
            <translation>Exécution de test de débit TCP descendant.</translation>
        </message>
        <message utf8="true">
            <source>Performing Advanced TCP test. This will take #1 seconds.</source>
            <translation>Réalisation de test TCP avancé. Cela prendra #1 secondes.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Type</source>
            <translation>Type d'IP local</translation>
        </message>
        <message utf8="true">
            <source>Local IP Address</source>
            <translation>Adresse IP locale</translation>
        </message>
        <message utf8="true">
            <source>Local Default Gateway</source>
            <translation>Passerelle locale par défaut </translation>
        </message>
        <message utf8="true">
            <source>Local Subnet Mask</source>
            <translation>Masque local de sous-réseau </translation>
        </message>
        <message utf8="true">
            <source>Local Encapsulation</source>
            <translation>Encapsulation locale</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>Adresse IP distante</translation>
        </message>
        <message utf8="true">
            <source>Select Mode</source>
            <translation>Mode de sélection </translation>
        </message>
        <message utf8="true">
            <source>What type of test are you running?</source>
            <translation>Quel type de test utilisez-vous ?</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>installing&lt;/b> or &lt;b>turning-up&lt;/b> a new circuit.*</source>
            <translation>J'&lt;b>installe&lt;/b> ou &lt;b> démarre&lt;/b> un nouveau circuit. *</translation>
        </message>
        <message utf8="true">
            <source>I'm &lt;b>troubleshooting&lt;/b> an existing circuit.</source>
            <translation>Je &lt;b>dépanne&lt;/b> un circuit existant.</translation>
        </message>
        <message utf8="true">
            <source>*Requires a remote MTS/T-BERD Test Instrument</source>
            <translation>*Nécessite un instrument de test MTS T-BERD distant</translation>
        </message>
        <message utf8="true">
            <source>How will your throughput be configured?</source>
            <translation>Comment votre débit sera-t-il configuré ?</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>the same&lt;/b>.</source>
            <translation>Mes débits ascendants et descendants sont &lt;b>les mêmes&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>My downstream and upstream throughputs are &lt;b>different&lt;/b>.</source>
            <translation>Mes débits ascendants et descendants sont &lt;b>différents&lt;/b>.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match SAMComplete CIR when loading Truespeed&#xA;configuration.</source>
            <translation>Régler la bande passante du goulot d'étranglement pour correspondre à SAMComplete CIR lors du chargement de la configuration&#xA;TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Set Bottleneck Bandwidth to match RFC 2544 Max Bandwidth when loading Truespeed&#xA;configuration.</source>
            <translation>Régler la bande passante du goulot d'étranglement pour correspondre à la norme RFC 2544 Bande passante maximale lors du chargement de la configuration&#xA;TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>Iperf Server</source>
            <translation>Serveur Iperf </translation>
        </message>
        <message utf8="true">
            <source>T-BERD/MTS Test Instrument</source>
            <translation>T-BERD/MTS Instrument de test</translation>
        </message>
        <message utf8="true">
            <source>IP Type</source>
            <translation>Type de IP</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>l'adresse IP</translation>
        </message>
        <message utf8="true">
            <source>Remote Settings</source>
            <translation>Réglages distant</translation>
        </message>
        <message utf8="true">
            <source>TCP Host Server Settings</source>
            <translation>Paramètres TCP du serveur hôte</translation>
        </message>
        <message utf8="true">
            <source>This step will configure global settings for all subsequent TrueSpeed steps. This includes the CIR (Committed Information Rate) and TCP Pass %, which is the percent of the CIR required to pass the throughput test.</source>
            <translation>Cette étape permet de configurer les paramètres globaux pour toutes les étapes ultérieures de TrueSpeed. Cela inclut le CIR (débit minimal garanti) et % TCP réussi, qui est le pourcentage du CIR requis pour passer le test de débit.</translation>
        </message>
        <message utf8="true">
            <source>Run Walk-the-Window Test</source>
            <translation>Exécuter test de suivi</translation>
        </message>
        <message utf8="true">
            <source>Total Test Time (s)</source>
            <translation>Temps de test total (s)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find MTU size</source>
            <translation>Trouver automatiquement la taille de MTU</translation>
        </message>
        <message utf8="true">
            <source>MTU Size (bytes)</source>
            <translation>Taille MTU (octets)</translation>
        </message>
        <message utf8="true">
            <source>Local VLAN ID</source>
            <translation>ID VLAN locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Priority</source>
            <translation>Priorité</translation>
        </message>
        <message utf8="true">
            <source>Local Priority</source>
            <translation>Priorité locale </translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local TOS</source>
            <translation>Adr. TOS Locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Local DSCP</source>
            <translation>Adr. DSCP Locale</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Downstream CIR (Mbps)</source>
            <translation>CIR descendant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (Mbps)</source>
            <translation>CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR (Mbps)</source>
            <translation>CIR ascendant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Remote&#xA;TCP Host</source>
            <translation>TCP hôte&#xA;distant</translation>
        </message>
        <message utf8="true">
            <source>Remote VLAN ID</source>
            <translation>VLAN ID distant</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote Priority</source>
            <translation>Priorité distant</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote TOS</source>
            <translation>Adr. TOS Distante</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Remote DSCP</source>
            <translation>Adr. DSCP Distante</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Set advanced TrueSpeed Settings</source>
            <translation>Définir paramétrages avancés TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Choose Bc Unit</source>
            <translation>Choisir unité Bc</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping</source>
            <translation>Façonnage du trafic</translation>
        </message>
        <message utf8="true">
            <source>Bc Unit</source>
            <translation>Unité Bc</translation>
        </message>
        <message utf8="true">
            <source>kbit</source>
            <translation>kbit</translation>
        </message>
        <message utf8="true">
            <source>Mbit</source>
            <translation>Mbit</translation>
        </message>
        <message utf8="true">
            <source>kB</source>
            <translation>Ko</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>Shaping Profile</source>
            <translation>Profil formation</translation>
        </message>
        <message utf8="true">
            <source>Both Local and Remote egress traffic shaped</source>
            <translation>Trafic de sortie à la fois local et distant mis en forme</translation>
        </message>
        <message utf8="true">
            <source>Only Local egress traffic shaped</source>
            <translation>Trafic de sortie local uniquement formé</translation>
        </message>
        <message utf8="true">
            <source>Only Remote egress traffic shaped</source>
            <translation>Trafic de sortie distant uniquement formé</translation>
        </message>
        <message utf8="true">
            <source>Neither Local or Remote egress traffic shaped</source>
            <translation>Trafic de sortie ni local ni distant formé</translation>
        </message>
        <message utf8="true">
            <source>Traffic Shaping (Walk the Window and Throughput tests only)</source>
            <translation>Formation de trafic (uniquement tests de débit et fenêtre)</translation>
        </message>
        <message utf8="true">
            <source>Tests run Unshaped then Shaped</source>
            <translation>Exécution de tests non formés puis formés</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Local</source>
            <translation>Tc (ms) local</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Local</source>
            <translation>Bc (kB) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Local</source>
            <translation>Bc (kbit) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Local</source>
            <translation>Bc (MB) Local</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Local</source>
            <translation>Bc (Mbit) Local</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms) Remote</source>
            <translation>Tc (ms) distant</translation>
        </message>
        <message utf8="true">
            <source>Tc (Remote)</source>
            <translation>Tc (distant)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB) Remote</source>
            <translation>Bc (kB) Distant</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit) Remote</source>
            <translation>Bc (kbit) Distant</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB) Remote</source>
            <translation>Bc (MB) Distant</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit) Remote</source>
            <translation>Bc (Mbit) Distant</translation>
        </message>
        <message utf8="true">
            <source>Do you want to shape the TCP Traffic?</source>
            <translation>Voulez-vous mettre en forme le trafic TCP?</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Traffic</source>
            <translation>Trafic non formé</translation>
        </message>
        <message utf8="true">
            <source>Shaped Traffic</source>
            <translation>Trafic formé</translation>
        </message>
        <message utf8="true">
            <source>Run the test unshaped</source>
            <translation>Exécutez le test non formé</translation>
        </message>
        <message utf8="true">
            <source>THEN</source>
            <translation>PUIS</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped</source>
            <translation>Exécutez le test formé</translation>
        </message>
        <message utf8="true">
            <source>Run the test shaped on Local</source>
            <translation>Exécutez le test formé sur local</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and Remote</source>
            <translation>Exécutez avec formation sur local et distant</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping at all</source>
            <translation>Exécutez sans formation du tout</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local and no shaping on Remote</source>
            <translation>Exécutez avec formation sur local et sans formation distant</translation>
        </message>
        <message utf8="true">
            <source>Run with shaping on Local</source>
            <translation>Exécutez avec formation sur local</translation>
        </message>
        <message utf8="true">
            <source>Run with no shaping on Local and shaping on Remote</source>
            <translation>Exécutez sans formation sur local et formation distant</translation>
        </message>
        <message utf8="true">
            <source>Shape Local traffic</source>
            <translation>Trafic local de forme</translation>
        </message>
        <message utf8="true">
            <source>Tc (ms)</source>
            <translation>Tc (ms)</translation>
        </message>
        <message utf8="true">
            <source>.5 ms</source>
            <translation>.5 ms</translation>
        </message>
        <message utf8="true">
            <source>1 ms</source>
            <translation>1 ms</translation>
        </message>
        <message utf8="true">
            <source>4 ms</source>
            <translation>4 ms</translation>
        </message>
        <message utf8="true">
            <source>5 ms</source>
            <translation>5 ms</translation>
        </message>
        <message utf8="true">
            <source>10 ms</source>
            <translation>10 ms</translation>
        </message>
        <message utf8="true">
            <source>25 ms</source>
            <translation>25 ms</translation>
        </message>
        <message utf8="true">
            <source>Bc (kB)</source>
            <translation>Bc (kB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (kbit)</source>
            <translation>Bc (kbit)</translation>
        </message>
        <message utf8="true">
            <source>Bc (MB)</source>
            <translation>Bc (MB)</translation>
        </message>
        <message utf8="true">
            <source>Bc (Mbit)</source>
            <translation>Bc (Mbit)</translation>
        </message>
        <message utf8="true">
            <source>Remote unit is&#xA;not connected</source>
            <translation>L'unité distante n'est pas connectée</translation>
        </message>
        <message utf8="true">
            <source>Shape Remote traffic</source>
            <translation>Trafic distant de forme</translation>
        </message>
        <message utf8="true">
            <source>Show additional shaping options</source>
            <translation>Montrer les options supplémentaires de formation</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Controls (Advanced)</source>
            <translation>Les contrôles TrueSpeed (Avancé)</translation>
        </message>
        <message utf8="true">
            <source>Connect to Port</source>
            <translation>Connecter au Port</translation>
        </message>
        <message utf8="true">
            <source>TCP Pass %</source>
            <translation>% TCP réussi</translation>
        </message>
        <message utf8="true">
            <source>MTU Upper Limit (bytes)</source>
            <translation>Limite supérieure MTU (octets)</translation>
        </message>
        <message utf8="true">
            <source>Use Multiple Connections</source>
            <translation>Utilisez connexions multiples</translation>
        </message>
        <message utf8="true">
            <source>Enable Saturation Window</source>
            <translation>Activer fenêtre de saturation</translation>
        </message>
        <message utf8="true">
            <source>Boost Window (%)</source>
            <translation>Booster fenêtre (%)</translation>
        </message>
        <message utf8="true">
            <source>Boost Connections (%)</source>
            <translation>Booster connexions (%)</translation>
        </message>
        <message utf8="true">
            <source>Step Configuration</source>
            <translation>Configuration de l'étape</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Steps</source>
            <translation>Étapes TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>You must have at least one step selected to run the test.</source>
            <translation>Vous devez avoir au moins une étape choisie pour exécuter le test.</translation>
        </message>
        <message utf8="true">
            <source>This step uses the procedure defined in RFC4821 to automatically determine the Maximum Transmission Unit of the end-end network path. The TCP Client test set will attempt to send TCP segments at various packet sizes and determine the MTU without the need for ICMP (as is required for traditional Path MTU Discovery).</source>
            <translation>Cette étape utilise la procédure définie dans RFC4821 pour déterminer automatiquement l'unité de transmission maximale du chemin de réseau de bout en bout. Le lot de test TCP client tentera d'envoyer les segments TCP à différentes tailles de paquets et de déterminer le MTU sans avoir besoin d'ICMP (comme cela est nécessaire pour la découverte du chemin MTU traditionnel).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a low intensity TCP transfer and report back the baseline Round Trip Time (RTT) that will be used as the basis for subsequent test step results. The baseline RTT is the inherent latency of the network, excluding the additional delays caused by network congestion.</source>
            <translation>Cette étape produira un transfert TCP de faible intensité et statuerale temps de référence aller-retour (RTT) qui sera utilisé comme base pour les résultats des étapes de test suivantes. La référence RTT est le délai implicite du réseau, en excluant les délais traditionnels dus à l'encombrement du réseau.</translation>
        </message>
        <message utf8="true">
            <source>Duration (seconds)</source>
            <translation>Durée (secondes)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct a TCP "Window scan" and report back TCP throughput results for up to four (4) TCP window size and connection combinations.  This step also reports actual versus predicted TCP throughput for each window size.</source>
            <translation>Cette étape mènera une TCP "Balayage de fenêtre» et rendra compte des résultats de débit TCP pour un maximum de quatre (4) tailles de fenêtres TCP et de combinaisons de connexions. Cette étape fait également état du débit TCP réel par rapport au débit prévu pour chaque taille de fenêtre.</translation>
        </message>
        <message utf8="true">
            <source>Window Sizes</source>
            <translation>Tailles de fenêtres</translation>
        </message>
        <message utf8="true">
            <source>Window Size 1 (bytes)</source>
            <translation>Taille 1 de fenêtre (octets)</translation>
        </message>
        <message utf8="true">
            <source># Conn.</source>
            <translation># Conn.</translation>
        </message>
        <message utf8="true">
            <source>Window 1 Connections</source>
            <translation>Connections fenêtre 1</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>=</source>
            <translation>=</translation>
        </message>
        <message utf8="true">
            <source>Window Size 2 (bytes)</source>
            <translation>Taille 2 de fenêtre (octets)</translation>
        </message>
        <message utf8="true">
            <source>Window 2 Connections</source>
            <translation>Connections fenêtre 2</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 3 (bytes)</source>
            <translation>Taille 3 de fenêtre (octets)</translation>
        </message>
        <message utf8="true">
            <source>Window 3 Connections</source>
            <translation>Connections fenêtre 3</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Window Size 4 (bytes)</source>
            <translation>Taille 4 de fenêtre (octets)</translation>
        </message>
        <message utf8="true">
            <source>Window 4 Connections</source>
            <translation>Connections fenêtre 4</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Max Seg Size (bytes)</source>
            <translation>Taille Seg Max (Octets)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found in the&#xA;Path MTU step</source>
            <translation>Utilise le MSS trouvé dans&#xA;l'étape de chemin MTU</translation>
        </message>
        <message utf8="true">
            <source>Max Segment Size (bytes)</source>
            <translation>Taille max de segment (bytes)</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the approximate ideal transfer throughput would be &lt;b>%6&lt;/b> kbps and a &lt;b>%7&lt;/b> MB file transferred across each connection should take &lt;b>%8&lt;/b> seconds.</source>
            <translation>Basé sur la bande passante du lien de &lt;b>%1&lt;/b> Mbps et un RTT de &lt;b>%2&lt;/b> ms, la fenêtre TCP idéal serait &lt;b>%3&lt;/b> octets. Avec &lt;b>%4&lt;/b> connexion (s) et un &lt;b>%5&lt;/b> octet taille de fenêtre TCP pour chaque connexion, approximativement le débit de transfert idéal serait &lt;b>%6&lt;/b> kbps et un &lt;b>%7&lt;/b> fichier Mo transférés à travers chaque connexion doit prendre &lt;b>%8&lt;/b> secondes.</translation>
        </message>
        <message utf8="true">
            <source>Based upon the link bandwidth of &lt;b>%1&lt;/b> Mbps and an RTT of &lt;b>%2&lt;/b> ms, the ideal TCP window would be &lt;b>%3&lt;/b> bytes. &lt;font color="red"> With &lt;b>%4&lt;/b> connection(s) and a &lt;b>%5&lt;/b> byte TCP Window Size for each connection, the capacity of the link is exceeded. The actual results may be worse than the predicted results due to packet loss and additional delay. Reduce the number of connections and/or TCP window size to run a test within the capacity of the link.&lt;/font></source>
            <translation>Basé sur la bande passante du lien de &lt;b>%1&lt;/b> Mbps et un RTT de &lt;b>%2&lt;/b> ms, la fenêtre TCP idéal serait &lt;b>%3&lt;/b> octets. &lt;font color="red"> Avec &lt;b>%4&lt;/b> Connexion (s) et un &lt;b>%5&lt;/b> byte taille de fenêtre TCP pour chaque connexion, la capacité de la liaison est dépassée. Les résultats réels pourraient être pires que les résultats prévus en raison de la perte de paquets et de retard supplémentaire. Réduire le nombre de connexions et / ou taille de fenêtre TCP pour effectuer un test au sein de la capacité de la liaison. &lt;/font></translation>
        </message>
        <message utf8="true">
            <source>Window Size (bytes)</source>
            <translation>Taille de fenêtre (octets)</translation>
        </message>
        <message utf8="true">
            <source>File Size per Connection (MB)</source>
            <translation>Taille de fichier par connexion (MB)</translation>
        </message>
        <message utf8="true">
            <source>Automatically find file size for 30 second transmit</source>
            <translation>Trouver automatiquement la taille du fichier pour une transmission de 30 secondes</translation>
        </message>
        <message utf8="true">
            <source>(%1 MB)</source>
            <translation>(%1 Mo)</translation>
        </message>
        <message utf8="true">
            <source>Number of Connections</source>
            <translation>Nombre de connexions</translation>
        </message>
        <message utf8="true">
            <source>RTT (ms)</source>
            <translation>RTT (ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the RTT found&#xA;in the RTT step&#xA;(%1 ms)</source>
            <translation>Utilise le RTT constaté&#xA;dans l'étape RTT&#xA;(%1 ms)</translation>
        </message>
        <message utf8="true">
            <source>Uses the MSS found&#xA;in the Path MTU step&#xA;(%1 bytes)</source>
            <translation>Utilise le MSS constaté&#xA;dans l'étape de chemin MTU&#xA;(%1 octets)</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing).</source>
            <translation>Cette étape conduira plusieurs connexions de transfert TCP pour tester si le lien partage équitablement la bande passante (débit formaté/traffic shaping) ou partage non équitablement la bande passante (débit contrôlé/traffic policing).</translation>
        </message>
        <message utf8="true">
            <source>This step will conduct multiple TCP connection transfers to test whether the link fairly shares the bandwidth (traffic shaping) or unfairly shares the bandwidth (traffic policing). For the Window Size and Number of Connections to be automatically computed, please run the RTT step.</source>
            <translation>Cette étape va effectuer plusieurs transferts de connexions TCP pour tester si le lien partage équitablement la bande passante (débit formaté/traffic shaping) ou partage non équitablement la bande passante (débit contrôlé/traffic policing). Pour que la taille de fenêtre et le nombre de connexions soient automatiquement calculés, veuillez exécuter l'étape RTT.</translation>
        </message>
        <message utf8="true">
            <source>Window Size (KB)</source>
            <translation>Taille de fenetre (Ko)</translation>
        </message>
        <message utf8="true">
            <source>Automatically computed when the&#xA;RTT step is conducted</source>
            <translation>Calculé automatiquement quand l'étape&#xA;RTT est réalisée</translation>
        </message>
        <message utf8="true">
            <source>1460 bytes</source>
            <translation>1460 octets</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed Tests</source>
            <translation>Lancer tests TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Tests</source>
            <translation>Ignorez les tests TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Maximum Transmission Unit (MTU)</source>
            <translation>Unité de transmission maximum (MTU)</translation>
        </message>
        <message utf8="true">
            <source>Maximum Segment Size (MSS)</source>
            <translation>Taille maximum de segment (MSS)</translation>
        </message>
        <message utf8="true">
            <source>This step determined that the Maximum Transmission Unit (MTU) is &lt;b>%1&lt;/b> bytes for this link (end-end). This value, minus layer 3/4 overhead, will be used as the size of the TCP Maximum Segment Size (MSS) for subsequent steps. In this case, the MSS is &lt;b>%2&lt;/b> bytes.</source>
            <translation>Cette étape a déterminé que l'unité de transmission maximale (MTU) est de &lt;b>%1&lt;/b> octets pour ce lien (de bout en bout). Cette valeur, déduction faite des sur-débits des couches 3/4, sera utilisée en tant que taille maximum des segments TCP (MSS) pour les étapes ultérieures. Dans ce cas, le MSS est &lt;b>%2&lt;/b> octets.</translation>
        </message>
        <message utf8="true">
            <source>Time (s)</source>
            <translation>Heure (s)</translation>
        </message>
        <message utf8="true">
            <source>RTT Summary Results</source>
            <translation>Résumé des résultats RTT</translation>
        </message>
        <message utf8="true">
            <source>Avg. RTT (ms)</source>
            <translation>RTT Moy (ms)</translation>
        </message>
        <message utf8="true">
            <source>Min. RTT (ms)</source>
            <translation>RTT min. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Max. RTT (ms)</source>
            <translation>RTT max. (ms)</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be &lt;b>%1&lt;/b> msec. The Minimum RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>Le temps aller-retour (RTT) a été mesuré à &lt;b>%1&lt;/b> msec. Le minimum RTT est utilisé étant donné qu'il représente la valeur la plus proche de la latence inhérente du réseau. Les étapes suivantes l'utilisent comme base pour des performances TCP prévues.</translation>
        </message>
        <message utf8="true">
            <source>The Round Trip Time (RTT) was measured to be %1 msec. The Average (Base) RTT is used as this most closely represents the inherent latency of the network. Subsequent steps use it as the basis for predicted TCP performance.</source>
            <translation>La durée d'aller-retour (RTT) était mesuré à %1 msec. La RTT moyenne (de base) est utilisée comme cela représente le plus la latence inhérente du réseau. Les étapes subséquantes l'utilisent comme la base pour performance TCP prédictive.</translation>
        </message>
        <message utf8="true">
            <source>Upstream Walk the Window</source>
            <translation>Parcours de fenêtre ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Walk the Window</source>
            <translation>Parcours de fenêtre descendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Size (Bytes)</source>
            <translation>1 taille de fenêtre en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Size (Bytes)</source>
            <translation>1 taille de fenêtre en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Size (Bytes)</source>
            <translation>2 taille de fenêtre en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Size (Bytes)</source>
            <translation>2 taille de fenêtre en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Size (Bytes)</source>
            <translation>3 taille de fenêtre en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Size (Bytes)</source>
            <translation>3 taille de fenêtre en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Size (Bytes)</source>
            <translation>4 taille de fenêtre en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Size (Bytes)</source>
            <translation>4 taille de fenêtre en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Size (Bytes)</source>
            <translation>5 taille de fenêtre en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Size (Bytes)</source>
            <translation>5 taille de fenêtre en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Connections</source>
            <translation>Connexions débit ascendant fenêtre 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Connections</source>
            <translation>Connexions débit descendant réel fenêtre 1</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Connections</source>
            <translation>Connexions débit ascendant fenêtre 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Connections</source>
            <translation>Connexions débit descendant réel fenêtre 2</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Connections</source>
            <translation>Connexions débit ascendant fenêtre 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Connections</source>
            <translation>Connexions débit descendant réel fenêtre 3</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Connections</source>
            <translation>Connexions débit ascendant fenêtre 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Connections</source>
            <translation>Connexions débit descendant réel fenêtre 4</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Connections</source>
            <translation>Connexions débit ascendant fenêtre 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Connections</source>
            <translation>Connexions débit descendant réel fenêtre 5</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual (Mbps)</source>
            <translation>Débit ascendant réel fenêtre 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Unshaped (Mbps)</source>
            <translation>Fenêtre 1 de flux montant réel non formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 1 de flux montant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Predicted (Mbps)</source>
            <translation>Débit ascendant prévu fenêtre 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual (Mbps)</source>
            <translation>Débit ascendant réel fenêtre 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Unshaped (Mbps)</source>
            <translation>Fenêtre 2 de flux montant réel non formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 2 de flux montant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Predicted (Mbps)</source>
            <translation>Débit ascendant prévu fenêtre 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual (Mbps)</source>
            <translation>Débit ascendant réel fenêtre 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Unshaped (Mbps)</source>
            <translation>Fenêtre 3 de flux montant réel non formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 3 de flux montant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Predicted (Mbps)</source>
            <translation>Débit ascendant prévu fenêtre 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual (Mbps)</source>
            <translation>Débit ascendant réel fenêtre 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Unshaped (Mbps)</source>
            <translation>Fenêtre 4 de flux montant réel non formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 4 de flux montant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Predicted (Mbps)</source>
            <translation>Débit ascendant prévu fenêtre 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual (Mbps)</source>
            <translation>Débit ascendant réel fenêtre 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Unshaped (Mbps)</source>
            <translation>Fenêtre 5 de flux montant réel non formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 5 de flux montant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Predicted (Mbps)</source>
            <translation>Débit ascendant prévu fenêtre 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual (Mbps)</source>
            <translation>Débit descendant réel fenêtre 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 1 de flux descendant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Predicted (Mbps)</source>
            <translation>Débit descendant réel prévu fenêtre 1 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual (Mbps)</source>
            <translation>Débit descendant réel fenêtre 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 2 de flux descendant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Predicted (Mbps)</source>
            <translation>Débit descendant réel prévu fenêtre 2 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual (Mbps)</source>
            <translation>Débit descendant réel fenêtre 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 3 de flux descendant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Predicted (Mbps)</source>
            <translation>Débit descendant réel prévu fenêtre 3 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual (Mbps)</source>
            <translation>Débit descendant réel fenêtre 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 4 de flux descendant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Predicted (Mbps)</source>
            <translation>Débit descendant réel prévu fenêtre 4 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual (Mbps)</source>
            <translation>Débit descendant réel fenêtre 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Actual Shaped (Mbps)</source>
            <translation>Fenêtre 5 de flux descendant réel formé (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Predicted (Mbps)</source>
            <translation>Débit descendant réel prévu fenêtre 5 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Avg.</source>
            <translation>Mbps Tx, moyenne.</translation>
        </message>
        <message utf8="true">
            <source>Window 1</source>
            <translation>Fenêtre 1</translation>
        </message>
        <message utf8="true">
            <source>Actual</source>
            <translation>Réel</translation>
        </message>
        <message utf8="true">
            <source>Unshaped Actual</source>
            <translation>Réel non formé</translation>
        </message>
        <message utf8="true">
            <source>Shaped Actual</source>
            <translation>Réel formé</translation>
        </message>
        <message utf8="true">
            <source>Ideal</source>
            <translation>Idéal</translation>
        </message>
        <message utf8="true">
            <source>Window 2</source>
            <translation>Fenêtre 2</translation>
        </message>
        <message utf8="true">
            <source>Window 3</source>
            <translation>Fenêtre 3</translation>
        </message>
        <message utf8="true">
            <source>Window 4</source>
            <translation>Fenêtre 4</translation>
        </message>
        <message utf8="true">
            <source>Window 5</source>
            <translation>Fenêtre 5</translation>
        </message>
        <message utf8="true">
            <source>The results of the TCP Walk the Window step shows the actual versus ideal throughput for each window size/connection tested. Actual less than ideal may be caused by loss or congestion. If actual is greater than ideal, then the RTT used as a baseline is too high. The TCP Throughput step provides a deeper analysis of the TCP transfers.</source>
            <translation>Les résultats de l'étape de parcours de fenêtre TCP indique le débit réel par rapport au débit idéal pour chaque taille de fenêtre/connexion testée. Si le réel est inférieur à l'idéal cela pourrait être causé par la perte ou la congestion. Si le réel est supérieur à l'idéal, dans ce cas la RTT utilisée comme base de référence est trop élevé. L'étape débit TCP fournit une analyse plus approfondie des transferts TCP.</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Actual vs. Ideal</source>
            <translation>Débit TCP ascendant réel par rapport au débit idéal</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Graphs</source>
            <translation>Graphique de débit TCP ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput Retransmission Graphs</source>
            <translation>Graphiques de retransmission de débit TCP de flux montant</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Throughput RTT Graphs</source>
            <translation>Graphiques RTT de débit TCP de flux montant</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Actual vs. Ideal</source>
            <translation>Débit descendant TCP réel par rapport à idéal</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Graphs</source>
            <translation>Graphiques débits TCP descendants</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput Retransmission Graphs</source>
            <translation>Graphiques de retransmission de débit TCP de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Throughput RTT Graphs</source>
            <translation>Graphiques RTT de débit TCP de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal Transmit Time (s)</source>
            <translation>Temps de transmission ascendant réel (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual Transmit Time (s)</source>
            <translation>Temps de transmission ascendant réel (s)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Throughput (Mbps)</source>
            <translation>Débit L4 actuel dans le sens montant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Sortie non formée L4 de flux montant réel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual L4 Shaped Throughput (Mbps)</source>
            <translation>Sortie formée L4 de flux montant réel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Ideal L4 Throughput (Mbps)</source>
            <translation>Sortie L4 de flux montant idéal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream TCP Efficiency (%)</source>
            <translation>Efficacité TCP dans le sens montant (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped TCP Efficiency (%)</source>
            <translation>Efficacité TCP non formé de flux montant (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped TCP Efficiency (%)</source>
            <translation>Efficacité TCP formé de flux montant (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Buffer Delay (%)</source>
            <translation>Délai de mémoire tampon dans le sens montant (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Unshaped Buffer Delay (%)</source>
            <translation>Délai de buffer non formé de flux montant (%)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Shaped Buffer Delay (%)</source>
            <translation>Délai de buffer formé de flux montant (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Throughput (Mbps)</source>
            <translation>Débit L4 actuel dans le sens descendant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual L4 Unshaped Throughput (Mbps)</source>
            <translation>Sortie non formée L4 de flux descendant réel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Actual L4 Throughput (Mbps)</source>
            <translation>Sortie L4 formée de flux descendant réel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Ideal L4 Throughput (Mbps)</source>
            <translation>Sortie L4 de flux descendant idéal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream TCP Efficiency (%)</source>
            <translation>Efficacité TCP dans le sens descendant (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped TCP Efficiency (%)</source>
            <translation>Efficacité TCP non formé de flux descendant (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped TCP Efficiency (%)</source>
            <translation>Efficacité TCP formé de flux descendant (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Buffer Delay (%)</source>
            <translation>Délai de mémoire tampon dans le sens descendant (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Unshaped Buffer Delay (%)</source>
            <translation>Délai de buffer non formé de flux descendant (%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Shaped Buffer Delay (%)</source>
            <translation>Délai de buffer formé de flux descendant (%)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Results</source>
            <translation>Résultats de débit TCP</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Ideal</source>
            <translation>Actuel par rapport à idéal</translation>
        </message>
        <message utf8="true">
            <source>Upstream test results may indicate:</source>
            <translation>Les résultats de test de flux ascendant peuvent indiquer:</translation>
        </message>
        <message utf8="true">
            <source>No further recommendation.</source>
            <translation>Aucune autre recommandation .</translation>
        </message>
        <message utf8="true">
            <source>The test was not run for a long enough duration</source>
            <translation>Le test n'a pas été exécuté pendant une durée assez longue</translation>
        </message>
        <message utf8="true">
            <source>Network buffer/shaper needs tuning</source>
            <translation>Buffer/Forme de réseau a besoin de réglage</translation>
        </message>
        <message utf8="true">
            <source>Policer dropped packets due to TCP bursts.</source>
            <translation>Paquets abandonnés de régulateur dû aux salves TCP.</translation>
        </message>
        <message utf8="true">
            <source>Throughput was good, but retransmissions detected.</source>
            <translation>Le seuil était bon, mais des retransmissions détectées.</translation>
        </message>
        <message utf8="true">
            <source>Network is congested or traffic is being shaped</source>
            <translation>le réseau est encombré ou le trafic est en train de se former</translation>
        </message>
        <message utf8="true">
            <source>Your CIR may be misconfigured</source>
            <translation>Votre CIR peut être mal configuré</translation>
        </message>
        <message utf8="true">
            <source>Your file transferred too quickly!&#xA;Please review the predicted transfer time for the file.</source>
            <translation>Votre fichier a été transféré trop vite !&#xA;Veuillez examiner le temps de transfert prévu pour le fichier.</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> connection(s), each with a window size of &lt;b>%2&lt;/b> bytes, were used to transfer a &lt;b>%3&lt;/b> MB file across each connection (&lt;b>%4&lt;/b> MB total).</source>
            <translation>&lt;b>%1&lt;/b> connexion(s), chacun avec une taille de fenêtre de &lt;b>%2&lt;/b> octets ont été utilisés pour transférer un fichier de &lt;b>%3&lt;/b> Mo à travers chaque connexion (&lt;b>%4&lt;/b> Mo total).</translation>
        </message>
        <message utf8="true">
            <source>&lt;b>%1&lt;/b> byte TCP window using &lt;b>%2&lt;/b> connection(s).</source>
            <translation>&lt;b>%1&lt;/b>octets de fenêtre TCP transmis sur &lt;b>%2&lt;/b> connexion(s).</translation>
        </message>
        <message utf8="true">
            <source>Actual L4 (Mbps)</source>
            <translation>C4 actuel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Ideal L4 (Mbps)</source>
            <translation>C4 idéal (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Transfer Metrics</source>
            <translation>Mesures du transfert</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency&#xA;(%)</source>
            <translation>Efficacité TCP&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay&#xA;(%)</source>
            <translation>Mémoire tampon de retard&#xA;(%)</translation>
        </message>
        <message utf8="true">
            <source>Downstream test results may indicate:</source>
            <translation>Les résultats de test de flux descendant peuvent indiquer:</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Ideal</source>
            <translation>Flux montant réel vs idéal</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency (%)</source>
            <translation>Efficacité TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay (%)</source>
            <translation>Délai de mémoire tampon (%)</translation>
        </message>
        <message utf8="true">
            <source>Unshaped test results may indicate:</source>
            <translation>Les résultats de test non formé peuvent indiquer:</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Ideal</source>
            <translation>Flux descendant réel vs idéal</translation>
        </message>
        <message utf8="true">
            <source>Throughput Graphs</source>
            <translation>Graphique de débit</translation>
        </message>
        <message utf8="true">
            <source>Retrans Frm</source>
            <translation>Cadre retransmis</translation>
        </message>
        <message utf8="true">
            <source>Baseline RTT</source>
            <translation>Référence RTT</translation>
        </message>
        <message utf8="true">
            <source>Use these graphs to correlate possible TCP performance issues due to retransmissions and/or congestive network effects (RTT exceeding baseline).</source>
            <translation>Utilisez ces graphiques pour corréler les éventuels problèmes de performances TCP en raison des retransmissions et/ou les effets congestifs de réseau (RTT dépassant la ligne de base).</translation>
        </message>
        <message utf8="true">
            <source>Retransmission Graphs</source>
            <translation>Graphiques de retransmission</translation>
        </message>
        <message utf8="true">
            <source>RTT Graphs</source>
            <translation>Graphiques RTT</translation>
        </message>
        <message utf8="true">
            <source>Ideal Throughput per Connection</source>
            <translation>Débit idéal par connexion</translation>
        </message>
        <message utf8="true">
            <source>For a link that is traffic shaped, each connection should receive a relatively even portion of the bandwidth. For a link that is traffic policed, each connection will bounce as retransmissions occur due to policing. For each of the &lt;b>%1&lt;/b> connections, each connection should consume about &lt;b>%2&lt;/b> Mbps of bandwidth.</source>
            <translation>Pour un lien qui est formé sur le trafic, chaque connexion doit recevoir une partie relativement uniforme de la bande passante. Pour un lien où le trafic est contrôlé, chaque connexion va rebondir avec les retransmissions en raison du contrôle. Pour chacune des &lt;b>%1&lt;/b> connexions, chaque connexion devrait consommer environ &lt;b>%2&lt;/b> Mbps de bande passante.</translation>
        </message>
        <message utf8="true">
            <source>L1 Kbps</source>
            <translation>C1 kbps</translation>
        </message>
        <message utf8="true">
            <source>L2 Kbps</source>
            <translation>C2 kbps</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>Aléatoire</translation>
        </message>
        <message utf8="true">
            <source>EMIX</source>
            <translation>EMIX</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF Test</source>
            <translation>TrueSpeed VNF Test</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed VNF</source>
            <translation>TrueSpeed VNF</translation>
        </message>
        <message utf8="true">
            <source>Test Configs</source>
            <translation>Configs de test</translation>
        </message>
        <message utf8="true">
            <source>Advanced Server Connect</source>
            <translation>Connexion au serveur avancée</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Configs</source>
            <translation>Configs de test avancées</translation>
        </message>
        <message utf8="true">
            <source>Create Report Locally</source>
            <translation>Créer rapport localement</translation>
        </message>
        <message utf8="true">
            <source>Test Identification</source>
            <translation>Identification de test</translation>
        </message>
        <message utf8="true">
            <source>End: View Detailed Results</source>
            <translation>Fin: Voir résultats détaillés</translation>
        </message>
        <message utf8="true">
            <source>End: Create Report Locally</source>
            <translation>Fin: Créer rapport localement</translation>
        </message>
        <message utf8="true">
            <source>Create Another Report Locally</source>
            <translation>Créer localement un autre rapport</translation>
        </message>
        <message utf8="true">
            <source>MSS Test</source>
            <translation>MSS Test</translation>
        </message>
        <message utf8="true">
            <source>RTT Test</source>
            <translation>RTT Test</translation>
        </message>
        <message utf8="true">
            <source>Upstream Test</source>
            <translation>Test de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Test</source>
            <translation>Test de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is required.</source>
            <translation>La configuration suivnate est requise</translation>
        </message>
        <message utf8="true">
            <source>The following configuration is out of range.  Please enter a value between #1 and #2.</source>
            <translation>La configuration suivante est hors limite. Veuillez entrer une valeur entre #1 et #2</translation>
        </message>
        <message utf8="true">
            <source>The following configuration has an invalid value.  Please make a new selection.</source>
            <translation>La configuration suivante e a une valeur illégale. Veuillez faire un autre choix</translation>
        </message>
        <message utf8="true">
            <source>No client-to-server test license.</source>
            <translation>Pas de licence de test client-serveur</translation>
        </message>
        <message utf8="true">
            <source>No server-to-server test license.</source>
            <translation>Pas de licence de test serveur-serveur</translation>
        </message>
        <message utf8="true">
            <source>There are too many active tests (maximum is #1).</source>
            <translation>Il y a trop de tests actifs (le max est #1)</translation>
        </message>
        <message utf8="true">
            <source>The local server is not reserved.</source>
            <translation>Le serveur local n'est pas réservé</translation>
        </message>
        <message utf8="true">
            <source>the remote server is not reserved.</source>
            <translation>Le serveur distant n'est pas réservé</translation>
        </message>
        <message utf8="true">
            <source>The test instance already exists.</source>
            <translation>L'instance de test existe déjà</translation>
        </message>
        <message utf8="true">
            <source>Test database read error.</source>
            <translation>Erreur de lecture de base de données de test</translation>
        </message>
        <message utf8="true">
            <source>The test was not found in the test database.</source>
            <translation>Le test n'a pas été trouvé dans la base de données de test</translation>
        </message>
        <message utf8="true">
            <source>The test is expired.</source>
            <translation>Le test a expiré</translation>
        </message>
        <message utf8="true">
            <source>The test type is not supported.</source>
            <translation>Le type de test n'est pas pris en charge</translation>
        </message>
        <message utf8="true">
            <source>The test server is not optioned.</source>
            <translation>Le serveur de test n'est pas en option</translation>
        </message>
        <message utf8="true">
            <source>The test server is reserved.</source>
            <translation>Le serveur de test est réservé.</translation>
        </message>
        <message utf8="true">
            <source>Test server bad request mode.</source>
            <translation>Mauvais mode de requête de serveur test</translation>
        </message>
        <message utf8="true">
            <source>Tests are not allowed on the remote server.</source>
            <translation>Les tests ne sont pas autorisée sur le serveur distant</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed on the remote server.</source>
            <translation>Echec de requête HTTP sur le serveur distant</translation>
        </message>
        <message utf8="true">
            <source>The remote server does not have sufficient resources available.</source>
            <translation>Le serveur distant n'a pas assez de ressources disponibles</translation>
        </message>
        <message utf8="true">
            <source>The test client is not optioned.</source>
            <translation>Le client test n'est pas en option.</translation>
        </message>
        <message utf8="true">
            <source>The test port is not supported.</source>
            <translation>Le port du test n'est pas pris en charge</translation>
        </message>
        <message utf8="true">
            <source>Attempting to test too many times per hour.</source>
            <translation>Tentative de test trop grande par heure</translation>
        </message>
        <message utf8="true">
            <source>The test instance build failed.</source>
            <translation>Echec de construction d'instance de test</translation>
        </message>
        <message utf8="true">
            <source>The test workflow build failed.</source>
            <translation>Echec de construction de flux de travail de test</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request is bad.</source>
            <translation>La requête HTTP de flux de travail est mauvaise</translation>
        </message>
        <message utf8="true">
            <source>The workflow HTTP request failed.</source>
            <translation>Echec de requête HTTP de flux de travail</translation>
        </message>
        <message utf8="true">
            <source>Remote tests are not allowed.</source>
            <translation>Tests distants non autorisés</translation>
        </message>
        <message utf8="true">
            <source>An error occurred in the resource manager.</source>
            <translation>Une erreur est survenue dans le gestionnaire de ressource</translation>
        </message>
        <message utf8="true">
            <source>The test instance was not found.</source>
            <translation>L'instance de test n'a pas été trouvé.</translation>
        </message>
        <message utf8="true">
            <source>The test state has a conflict.</source>
            <translation>L'état de test a un conflit</translation>
        </message>
        <message utf8="true">
            <source>The test state is invalid.</source>
            <translation>L'état de test est invalide.</translation>
        </message>
        <message utf8="true">
            <source>The test creation failed.</source>
            <translation>Echec de création de test</translation>
        </message>
        <message utf8="true">
            <source>The test update failed.</source>
            <translation>Echec de mise à jour de test</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully created.</source>
            <translation>Le test a été créé avec succès</translation>
        </message>
        <message utf8="true">
            <source>The test was successfully updated.</source>
            <translation>Le test a été mis à jour avec succès.</translation>
        </message>
        <message utf8="true">
            <source>HTTP request failed: #1 / #2 / #3</source>
            <translation>Echec de requête HTTP:#1 /#2 /#3</translation>
        </message>
        <message utf8="true">
            <source>VNF server version (#2) may not be compatible with instrument version (#1).</source>
            <translation>Version de serveur VNF (#2) peut ne pas être compatible avec version d'instrument (#1).</translation>
        </message>
        <message utf8="true">
            <source>Please enter User Name and Authentication Key for the server at #1.</source>
            <translation>Veuillez entrer un nom d'utilisateur et clé d'authentification pour le serveur à #1</translation>
        </message>
        <message utf8="true">
            <source>Test failed to initialize: #1</source>
            <translation>Echec de test pour initialiser:#1</translation>
        </message>
        <message utf8="true">
            <source>Test aborted: #1</source>
            <translation>Test annulé: #1</translation>
        </message>
        <message utf8="true">
            <source>Server Connection</source>
            <translation>Connexion au serveur</translation>
        </message>
        <message utf8="true">
            <source>Do not have information needed to connect to server.</source>
            <translation>N'a pas les informations nécessaires pour se connecter au serveur.</translation>
        </message>
        <message utf8="true">
            <source>A link is not present to perform network communications.</source>
            <translation>Un lien n'est pas présent pour réaliser les communication réseau.</translation>
        </message>
        <message utf8="true">
            <source>Do not have a valid source IP address for network communications.</source>
            <translation>N'a pas d'adresse IP de source valide pour communications réseau.</translation>
        </message>
        <message utf8="true">
            <source>Ping not done at specified IP address.</source>
            <translation>Ping non fait sur adresse IP spécifiée</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect unit at specified IP address.</source>
            <translation>Impossible de détecter unité à adresse IP spécifiée</translation>
        </message>
        <message utf8="true">
            <source>Server not yet identified specified IP address.</source>
            <translation>Le serveur n'a pas encore identifié l'adresse IP spécifié</translation>
        </message>
        <message utf8="true">
            <source>Server cannot be identified at the specified IP address.</source>
            <translation>Le serveur ne peut pas identifié à l'adresse IP spécifiée</translation>
        </message>
        <message utf8="true">
            <source>Server identified but not authenticated.</source>
            <translation>Serveur identifié mais non authentifié</translation>
        </message>
        <message utf8="true">
            <source>Server authentication failed.</source>
            <translation>Echec d'authentification de test</translation>
        </message>
        <message utf8="true">
            <source>Authorization failed, trying to identify.</source>
            <translation>Echec d'autorisation, essai d'identification.</translation>
        </message>
        <message utf8="true">
            <source>Not authorized or identified, trying ping.</source>
            <translation>Non autorisé ou non identifié, essai de ping.</translation>
        </message>
        <message utf8="true">
            <source>Server authenticated and available for testing.</source>
            <translation>Serveur authentifié et disponible pour test</translation>
        </message>
        <message utf8="true">
            <source>Server is connected and test is running.</source>
            <translation>Le serveur est connecté et le test s'exécute</translation>
        </message>
        <message utf8="true">
            <source>Identifying</source>
            <translation>Indentification</translation>
        </message>
        <message utf8="true">
            <source>Identify</source>
            <translation>Identifier</translation>
        </message>
        <message utf8="true">
            <source>TCP Proxy Version</source>
            <translation>Version de proxy TCP</translation>
        </message>
        <message utf8="true">
            <source>Test Controller Version</source>
            <translation>Version du contrôleur de test</translation>
        </message>
        <message utf8="true">
            <source>Link Active:</source>
            <translation>Lien actif:</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ID du Serveur:</translation>
        </message>
        <message utf8="true">
            <source>Server Status:</source>
            <translation>Etat du serveur:</translation>
        </message>
        <message utf8="true">
            <source>Advanced settings</source>
            <translation>Paramétrages avancés</translation>
        </message>
        <message utf8="true">
            <source>Advanced Connection Settings</source>
            <translation>Paramétrages avancés de connexion</translation>
        </message>
        <message utf8="true">
            <source>Authentication Key</source>
            <translation>Clé d'authentification</translation>
        </message>
        <message utf8="true">
            <source>Memorize User Names and Keys</source>
            <translation>Mémoriser noms d'utilisateur et clés</translation>
        </message>
        <message utf8="true">
            <source>Local Unit</source>
            <translation>Unité locale</translation>
        </message>
        <message utf8="true">
            <source>Server</source>
            <translation>Serveur</translation>
        </message>
        <message utf8="true">
            <source>Window Walk Duration (sec)</source>
            <translation>Durée de fenêtre de marche (sec)</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Auto</translation>
        </message>
        <message utf8="true">
            <source>Auto Duration</source>
            <translation>Durée auto</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Configs (Advanced)</source>
            <translation>Configs de test (avancées)</translation>
        </message>
        <message utf8="true">
            <source>Advanced Test Parameters</source>
            <translation>Paramètres de test avancés</translation>
        </message>
        <message utf8="true">
            <source>TCP Port</source>
            <translation>Port TCP</translation>
        </message>
        <message utf8="true">
            <source>Auto TCP Port</source>
            <translation>Port TCP auto</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Number of Window Walks</source>
            <translation>Nombre de fenêtre de marche</translation>
        </message>
        <message utf8="true">
            <source>Connection Count</source>
            <translation>Compte de connexion</translation>
        </message>
        <message utf8="true">
            <source>Auto Connection Count</source>
            <translation>Compte d'auto connexion</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Saturation Window</source>
            <translation>Fenêtre de saturation</translation>
        </message>
        <message utf8="true">
            <source>Run Saturation Window</source>
            <translation>Exécuter fenêtre de saturation</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Boost Connection (%)</source>
            <translation>Booster connexion (%)</translation>
        </message>
        <message utf8="true">
            <source>Server Report Information</source>
            <translation>Information de rapport de serveur</translation>
        </message>
        <message utf8="true">
            <source>Test Name</source>
            <translation>Nom du test</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>Nom du technicien</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID du technicien</translation>
        </message>
        <message utf8="true">
            <source>Customer Name*</source>
            <translation>Nom du client*</translation>
        </message>
        <message utf8="true">
            <source>Company</source>
            <translation>Entreprise</translation>
        </message>
        <message utf8="true">
            <source>Email*</source>
            <translation>Courriel*</translation>
        </message>
        <message utf8="true">
            <source>Phone</source>
            <translation>Téléphone</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>Commentaires</translation>
        </message>
        <message utf8="true">
            <source>Show Test ID</source>
            <translation>Montrer ID de test</translation>
        </message>
        <message utf8="true">
            <source>Skip TrueSpeed Test</source>
            <translation>Ignorer test TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Server is not connected.</source>
            <translation>Le serveur n'est pas connecté</translation>
        </message>
        <message utf8="true">
            <source>MSS</source>
            <translation>MSS</translation>
        </message>
        <message utf8="true">
            <source>MSS (bytes)</source>
            <translation>MSS (octets)</translation>
        </message>
        <message utf8="true">
            <source>Avg. (Mbps)</source>
            <translation>Moyenne (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Waiting for testing resource to be ready.  You are #%1 in the wait list.</source>
            <translation>Attente pour ressource de test prête. Vous êtes #%1 dans la liste d'attente.</translation>
        </message>
        <message utf8="true">
            <source>Window</source>
            <translation>Fenêtre</translation>
        </message>
        <message utf8="true">
            <source>Saturation&#xA;Window</source>
            <translation>Fenêtre de saturation</translation>
        </message>
        <message utf8="true">
            <source>Window Size (kB)</source>
            <translation>Taille de fenêtre (kO)</translation>
        </message>
        <message utf8="true">
            <source>Connections</source>
            <translation>Connexions</translation>
        </message>
        <message utf8="true">
            <source>Upstream Diagnosis:</source>
            <translation>Diagnostic de flux ascendant:</translation>
        </message>
        <message utf8="true">
            <source>Nothing to Report</source>
            <translation>Rien à rapporter</translation>
        </message>
        <message utf8="true">
            <source>Throughput Too Low</source>
            <translation>Seuil trop faible</translation>
        </message>
        <message utf8="true">
            <source>Inconsistent RTT</source>
            <translation>RTT inconsistant</translation>
        </message>
        <message utf8="true">
            <source>TCP Efficiency is Low</source>
            <translation>L'efficience TCP est faible</translation>
        </message>
        <message utf8="true">
            <source>Buffer Delay is High</source>
            <translation>Le délai de buffer est élevé</translation>
        </message>
        <message utf8="true">
            <source>Throughput Less Than 85% of CIR</source>
            <translation>Seuil inférieur à 85% de CIR</translation>
        </message>
        <message utf8="true">
            <source>MTU Less Than 1400</source>
            <translation>MTU inférieur à 1400</translation>
        </message>
        <message utf8="true">
            <source>Downstream Diagnosis:</source>
            <translation>Diagnostic de flux descendant:</translation>
        </message>
        <message utf8="true">
            <source>Auth Code</source>
            <translation>Code d'authentification</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Auth Creation Date</source>
            <translation>Date de création d'authentification</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Expiration Date</source>
            <translation>Date d'expiration</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Modify Time</source>
            <translation>Modifier heure</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Test Stop Time</source>
            <translation>Heure d'arrêt du test</translation>
        </message>
        <message utf8="true">
            <source>Last Modified</source>
            <translation>Dernière modification</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Nom du client</translation>
        </message>
        <message utf8="true">
            <source>Email</source>
            <translation>Courriel</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Results</source>
            <translation>Résultats de seuil de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Actual vs. Target</source>
            <translation>Réel vs Cible</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual vs. Target</source>
            <translation>Flux ascendant Réel vs Cible</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary</source>
            <translation>Résumé de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Peak TCP Throughput (Mbps)</source>
            <translation>Seuil TCP ic (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP MSS (bytes)</source>
            <translation>TCP MSS (octets)</translation>
        </message>
        <message utf8="true">
            <source>Round Trip Time (ms)</source>
            <translation>Tems d'aller-retour (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Summary Results (Max. Throughput Window)</source>
            <translation>Résultats de résumé de flux ascendant (Fenêtre de seuil max)</translation>
        </message>
        <message utf8="true">
            <source>Window Size per Connection (kB)</source>
            <translation>Taille de fenêtre par connexion (kO)</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Window (kB)</source>
            <translation>Fenêtre aggregate (kO)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps)</source>
            <translation>Débit TCP cible (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Average TCP Throughput (Mbps)</source>
            <translation>Seuil TCP moyen (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (Mbps)</source>
            <translation>Débit TCP (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target</source>
            <translation>Cible</translation>
        </message>
        <message utf8="true">
            <source>Window 6</source>
            <translation>Fenêtre 6</translation>
        </message>
        <message utf8="true">
            <source>Window 7</source>
            <translation>Fenêtre 7</translation>
        </message>
        <message utf8="true">
            <source>Window 8</source>
            <translation>Fenêtre 8</translation>
        </message>
        <message utf8="true">
            <source>Window 9</source>
            <translation>Fenêtre 9</translation>
        </message>
        <message utf8="true">
            <source>Window 10</source>
            <translation>Fenêtre 10</translation>
        </message>
        <message utf8="true">
            <source>Window 11</source>
            <translation>Fenêtre 11</translation>
        </message>
        <message utf8="true">
            <source>Maximum Throughput Window:</source>
            <translation>fenêtre de seuil maximum:</translation>
        </message>
        <message utf8="true">
            <source>%1 kB Window: %2 conn. x %3 kB</source>
            <translation>% Fenêtre 1 kO: %2 conn. X %3 kO</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>Moyen</translation>
        </message>
        <message utf8="true">
            <source>Upstream Throughput Graphs</source>
            <translation>Graphiques de seuil de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Details</source>
            <translation>Détails de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 1 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 1 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Window Size Per Connection (kB)</source>
            <translation>Taille de fenêtre par connexion (kO)</translation>
        </message>
        <message utf8="true">
            <source>Actual Throughput (Mbps)</source>
            <translation>Débit actuel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target Throughput (Mbps)</source>
            <translation>Seuil de cible (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Total Retransmits</source>
            <translation>Total retransmit</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 2 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 2 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 3 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 3 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 4 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 4 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 5 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 5 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 6 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 6 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 7 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 7 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 8 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 8 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 9 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 9 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Window 10 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 10 de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Saturation Window Throughput Results</source>
            <translation>Résultats de seuil de fenêtre de saturation de flux ascendant</translation>
        </message>
        <message utf8="true">
            <source>Average Throughput (Mbps)</source>
            <translation>Débit moyen (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Results</source>
            <translation>Résultats de seuil de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual vs. Target</source>
            <translation>Flux descendant Réel vs Cible</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary</source>
            <translation>Résumé de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Summary Results (Max. Throughput Window)</source>
            <translation>Résultats de résumé de flux descendant (Fenêtre de seuil max)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Throughput Graphs</source>
            <translation>Graphiques de seuil de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Details</source>
            <translation>Détails de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 1 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 1 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 2 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 2 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 3 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 3 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 4 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 4 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 5 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 5 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 6 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 6 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 7 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 7 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 8 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 8 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 9 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 9 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Window 10 Throughput Results</source>
            <translation>Résultats de seuil de fenêtre 10 de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Saturation Window Throughput Results</source>
            <translation>Résultats de seuil de fenêtre de saturation de flux descendant</translation>
        </message>
        <message utf8="true">
            <source># Window Walks</source>
            <translation># Fenêtre Marche</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Windows (%)</source>
            <translation>Fenêtes sursaturées(%)</translation>
        </message>
        <message utf8="true">
            <source>Oversaturate Connections (%)</source>
            <translation>Connexions sursaturée(%)</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan</source>
            <translation>VLAN Scan</translation>
        </message>
        <message utf8="true">
            <source>#1 Starting VLAN Scan Test #2</source>
            <translation>#1 Démarrage du test #2 de scan VLAN</translation>
        </message>
        <message utf8="true">
            <source>Testing VLAN ID #1 for #2 seconds</source>
            <translation>Test VLAN ID #1 pour #2 secondes</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: PASSED</source>
            <translation>VLAN ID #1: REUSSI</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID #1: FAILED</source>
            <translation>VLAN ID #1: ECHEC</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test failed for VLAN ID #1</source>
            <translation>Boucle active sans succès. Echec de test pour VLAN ID #1</translation>
        </message>
        <message utf8="true">
            <source>Total VLAN IDs Tested</source>
            <translation>IDs VLAN total testé</translation>
        </message>
        <message utf8="true">
            <source>Number of Passed IDs</source>
            <translation>Nombre d'IDs réussis</translation>
        </message>
        <message utf8="true">
            <source>Number of Failed IDs</source>
            <translation>Nombre d'IDs en échec</translation>
        </message>
        <message utf8="true">
            <source>Duration per ID (s)</source>
            <translation>Durée par ID (s)</translation>
        </message>
        <message utf8="true">
            <source>Number of Ranges</source>
            <translation>Nombre de gammes</translation>
        </message>
        <message utf8="true">
            <source>Selected Ranges</source>
            <translation>Gammes sélectionnées</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Min</source>
            <translation>Min VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Max</source>
            <translation>VLAN ID Max</translation>
        </message>
        <message utf8="true">
            <source>Start Test</source>
            <translation>Démarrer Test</translation>
        </message>
        <message utf8="true">
            <source>Total IDs</source>
            <translation>Total IDs</translation>
        </message>
        <message utf8="true">
            <source>Passed IDs</source>
            <translation>IDs réussi</translation>
        </message>
        <message utf8="true">
            <source>Failed IDs</source>
            <translation>IDs échec</translation>
        </message>
        <message utf8="true">
            <source>Advanced VLAN Scan settings</source>
            <translation>Paramétrages de scan VLAN avancés</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth (L1 Mbps)</source>
            <translation>Largeur de bande (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Criteria</source>
            <translation>Critères de passage</translation>
        </message>
        <message utf8="true">
            <source>No frames lost</source>
            <translation>Aucune trame perdue</translation>
        </message>
        <message utf8="true">
            <source>Some frames received</source>
            <translation>Quelques trames reçues</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM</source>
            <translation>TrueSAM</translation>
        </message>
        <message utf8="true">
            <source>Configure TrueSAM...</source>
            <translation>Configurer TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSAM...</source>
            <translation>Exécutez TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Estimated Run Time</source>
            <translation>Durée d'exécution estimée</translation>
        </message>
        <message utf8="true">
            <source>Stop on Failure</source>
            <translation>Arrêter sur échec</translation>
        </message>
        <message utf8="true">
            <source>View Report</source>
            <translation>Vue Rapport</translation>
        </message>
        <message utf8="true">
            <source>View TrueSAM Report...</source>
            <translation>Voir le rapport TrueSAM...</translation>
        </message>
        <message utf8="true">
            <source>Aborted</source>
            <translation>Abandonné</translation>
        </message>
        <message utf8="true">
            <source>Completed</source>
            <translation>Terminé</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Echec</translation>
        </message>
        <message utf8="true">
            <source>Passed</source>
            <translation>Succès</translation>
        </message>
        <message utf8="true">
            <source>Stopped by user</source>
            <translation>Arrêté par l'utilisateur</translation>
        </message>
        <message utf8="true">
            <source>TrueSAM:</source>
            <translation>TrueSAM:</translation>
        </message>
        <message utf8="true">
            <source>Signal Loss.</source>
            <translation>Perte signal.</translation>
        </message>
        <message utf8="true">
            <source>Link Loss.</source>
            <translation>Perte du lien.</translation>
        </message>
        <message utf8="true">
            <source>Communication with the remote test set has been lost. Please re-establish the communcation channel and try again.</source>
            <translation>La communication avec le lot de test à distance a été perdue. Veuillez ré-établir le canal de communication et essayer à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, communication with the remote test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Un problème a été rencontré lors de la configuration de l'ensemble de test local, la communication avec le lot de test à distance a été perdue. Veuillez ré-établir le canal de communication et essayer à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the remote test set, communication with the test set has been lost. Please re-establish the communication channel and try again.</source>
            <translation>Un problème est survenu lors de la configuration de l'ensemble de test distant, la communication avec l'ensemble de test a été perdue. Veuillez rétablir le canal de communication et réessayer.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while configuring the local test set, TrueSAM will now exit.</source>
            <translation>Un problème a été rencontré lors de la configuration de l'ensemble de test local, TrueSAM va maintenant se terminer.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered while attempting to configure the local test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Un problème a été rencontré lors de la tentative de configurer le lot de test local. Le test a été forcé de s'arrêter, et le canal de communication avec le lot de test à distance a été perdu.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encounterd while attempting to configure the remote test set. The testing has been forced to stop, and the communication channel with the remote test set has been lost.</source>
            <translation>Un problème a été rencontré lors de la tentative de configurer le jeu de test distant. Le test a été forcé de s'arrêter, et le canal de communication avec le jeu de test distant a été perdu.</translation>
        </message>
        <message utf8="true">
            <source>The screen saver has been disabled to prevent interference while testing.</source>
            <translation>L'économiseur d'écran a été désactivé pour éviter les interférences pendant les tests.</translation>
        </message>
        <message utf8="true">
            <source>A problem has been encountered on the local test set. TrueSAM will now exit.</source>
            <translation>Un problème a été rencontré sur le lot de test local. TrueSAM va maintenant se terminer.</translation>
        </message>
        <message utf8="true">
            <source>Requesting DHCP parameters.</source>
            <translation>Requiert les paramètres DHCP</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters obtained.</source>
            <translation>Obtention des paramètres DHCP</translation>
        </message>
        <message utf8="true">
            <source>Initializing Ethernet Interface. Please wait.</source>
            <translation>Initialisation de l'interface Ethernet, merci d'attendre..</translation>
        </message>
        <message utf8="true">
            <source>North America</source>
            <translation>Amérique du nord</translation>
        </message>
        <message utf8="true">
            <source>North America and Korea</source>
            <translation>Amérique du nord et Corée</translation>
        </message>
        <message utf8="true">
            <source>North America PCS</source>
            <translation>Amérique du nord PCS</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Inde</translation>
        </message>
        <message utf8="true">
            <source>Lost Time of Day signal from external time source, will cause 1PPS sync to appear off.</source>
            <translation>Perte du signal Time of Day de la source externe d'horloge, fera apparaitre la sync 1PPS désactivée.</translation>
        </message>
        <message utf8="true">
            <source>Starting synchronization with Time of Day signal from external time source...</source>
            <translation>Démarrage de la synchronisation avec le signal Time of Day d'une source d'horloge externe...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with Time of Day signal from external time source.</source>
            <translation>Synchronisation réussie avec le signal Time of Day d'une source d'horloge externe.</translation>
        </message>
        <message utf8="true">
            <source>Loop&#xA;Down</source>
            <translation>Boucle&#xA;Inactive</translation>
        </message>
        <message utf8="true">
            <source>Cover Pages</source>
            <translation>Pages de couverture</translation>
        </message>
        <message utf8="true">
            <source>Select Tests to Run</source>
            <translation>Sélectionnez les tests à exécuter</translation>
        </message>
        <message utf8="true">
            <source>End-to-end Traffic Connectivity Test</source>
            <translation>Test de connectivité de trafic de bout en bout</translation>
        </message>
        <message utf8="true">
            <source>Enhanced RFC 2544 / SAMComplete</source>
            <translation>RFC 2544 amélioré/SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>Ethernet Benchmarking Test Suite</source>
            <translation>Ensemble de test d'analyse comparative Ethernet </translation>
        </message>
        <message utf8="true">
            <source>SAMComplete</source>
            <translation>SAM-Complete</translation>
        </message>
        <message utf8="true">
            <source>Y.1564 Ethernet Services Configuration and Performance Testing</source>
            <translation>Test de configuration et de performance des services Ethernet Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Transparency Test for Control Plane Frames (CDP, STP, etc).</source>
            <translation>Couche 2 : Test de transparence pour les trames de plan de contrôle (CDP, STP, etc).</translation>
        </message>
        <message utf8="true">
            <source>RFC 6349 TCP Throughput and Performance Test</source>
            <translation>Test de débit et de performance TCP RFC 6349</translation>
        </message>
        <message utf8="true">
            <source>L3-Source Type</source>
            <translation>Type de source C3</translation>
        </message>
        <message utf8="true">
            <source>Settings for Communications Channel (using Service 1)</source>
            <translation>Réglages du canal de communications (utilisant service 1)</translation>
        </message>
        <message utf8="true">
            <source>Communications Channel Settings</source>
            <translation>Réglages du canal de communications</translation>
        </message>
        <message utf8="true">
            <source>Service 1 must be configured to agree with stream 1 on the remote Viavi test instrument.</source>
            <translation>Le service 1 doit être configuré pour être en accord avec le flux 1 sur l'équipement de test distant Viavi.</translation>
        </message>
        <message utf8="true">
            <source>Local Status</source>
            <translation>Statut local</translation>
        </message>
        <message utf8="true">
            <source>ToD Sync</source>
            <translation>Sync ToD</translation>
        </message>
        <message utf8="true">
            <source>1PPS Sync</source>
            <translation>Sync 1PPS</translation>
        </message>
        <message utf8="true">
            <source>Connect&#xA;to Channel</source>
            <translation>Connecter&#xA;au canal</translation>
        </message>
        <message utf8="true">
            <source>Physical Layer</source>
            <translation>Couche Physique</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Quanta)</source>
            <translation>Longueur de pause (quanta)</translation>
        </message>
        <message utf8="true">
            <source>Pause Length (Time - ms)</source>
            <translation>Temps de pause (ms)</translation>
        </message>
        <message utf8="true">
            <source>Run&#xA;Tests</source>
            <translation>Lancer&#xA;tests</translation>
        </message>
        <message utf8="true">
            <source>Starting&#xA;Tests</source>
            <translation>Démarrage des tests&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Stopping&#xA;Tests</source>
            <translation>Arrêt en cours&#xA;des tests</translation>
        </message>
        <message utf8="true">
            <source>Estimated time to execute tests</source>
            <translation>Temps estimé pour l'exécution des tests</translation>
        </message>
        <message utf8="true">
            <source>Total</source>
            <translation>Total</translation>
        </message>
        <message utf8="true">
            <source>Stop on failure</source>
            <translation>Arrêt sur échec</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail criteria for chosen tests</source>
            <translation>Critères de succès/échec pour les tests choisis</translation>
        </message>
        <message utf8="true">
            <source>Upstream and Downstream</source>
            <translation>En amont et en aval</translation>
        </message>
        <message utf8="true">
            <source>This has no concept of Pass/Fail, so this setting does not apply.</source>
            <translation>Cela n'a aucune notion de succès/échec, ce paramètre ne s'applique donc pas.</translation>
        </message>
        <message utf8="true">
            <source>Pass if following thresholds are met:</source>
            <translation>Succès si les seuils suivants sont atteints :</translation>
        </message>
        <message utf8="true">
            <source>No thresholds set - will not report Pass/Fail.</source>
            <translation>Pas de seuils fixés - ne signalera pas succès/échec.</translation>
        </message>
        <message utf8="true">
            <source>Local:</source>
            <translation>Local:</translation>
        </message>
        <message utf8="true">
            <source>Remote:</source>
            <translation>Distant:</translation>
        </message>
        <message utf8="true">
            <source>Local and Remote:</source>
            <translation>Local et distant:</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 Mbps)</source>
            <translation>Sortie (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L1 kbps)</source>
            <translation>Débit ( kbps L1 )</translation>
        </message>
        <message utf8="true">
            <source>Throughput (L2 kbps)</source>
            <translation>Sortie (L2 kbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>Tolérance du "Frame Lost" (%)</translation>
        </message>
        <message utf8="true">
            <source>Latency (us)</source>
            <translation>Latence(us)</translation>
        </message>
        <message utf8="true">
            <source>Pass if following SLA parameters are satisfied:</source>
            <translation>Succès si les paramètres SLA suivants sont satisfaits :</translation>
        </message>
        <message utf8="true">
            <source>Upstream:</source>
            <translation>Montant:</translation>
        </message>
        <message utf8="true">
            <source>Downstream:</source>
            <translation>Descendant:</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (Mbps)</source>
            <translation>Débit CIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L1 Mbps)</source>
            <translation>CIR débit ( L1 Mbps )</translation>
        </message>
        <message utf8="true">
            <source>CIR Throughput (L2 Mbps)</source>
            <translation>Débit CIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (Mbps)</source>
            <translation>Débit EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L1 Mbps)</source>
            <translation>Sortie EIR (L1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR Throughput (L2 Mbps)</source>
            <translation>Seuil EIR (L2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (Mbps)</source>
            <translation>M-Tolérance (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L1 Mbps)</source>
            <translation>M - Tolérance ( L1 Mbps )</translation>
        </message>
        <message utf8="true">
            <source>M - Tolerance (L2 Mbps)</source>
            <translation>Tolérance M (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (ms)</source>
            <translation>Délai trame (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation (ms)</source>
            <translation>Variation de délai (ms)</translation>
        </message>
        <message utf8="true">
            <source>Pass/Fail will be determined internally.</source>
            <translation>Le succès/échec sera déterminé en interne.</translation>
        </message>
        <message utf8="true">
            <source>Pass if measured TCP throughput meets following threshold:</source>
            <translation>Succès si le débit TCP mesuré est conforme au seuil suivant :</translation>
        </message>
        <message utf8="true">
            <source>Percentage of predicted throughput</source>
            <translation>Pourcentage de débit prévu</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput test not enabled - will not report Pass/Fail.</source>
            <translation>Le test de débit TCP n'est pas activé - ne signalera pas Succès/Échec.</translation>
        </message>
        <message utf8="true">
            <source>Stop tests</source>
            <translation>Arrêt des tests</translation>
        </message>
        <message utf8="true">
            <source>This will stop the currently running test and further test execution. Are you sure you want to stop?</source>
            <translation>Cela va arrêter le test en cours et l'exécution de nouveaux essais. Etes-vous sûr que vous voulez arrêter ?</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;Le total de la charge maximale pour le service (s) #1 est 0 ou qu'elle dépasse le #2 C1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;Le total de la charge maximale pour la direction en amont de service ( s) #1 est 0 ou qu'elle dépasse #2 C1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the #2 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;Le total de la charge maximale de l' amont vers l'aval pour le service (s) #1 est 0 ou qu'elle dépasse #2 C1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuration invalide : &#xA;&#xA;Le total de la charge maximale pour le service (s) #1 est 0 ou qu'elle dépasse le débit de ligne Mbps C2 respectif (s) .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Upstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuration invalide : &#xA;&#xA;Le total de la charge maximale pour la direction en amont de service ( s) #1 est 0 ou qu'elle dépasse le taux Mbps ligne respective C2 (s) .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load total for the Downstream direction for Service(s) #1 is 0 or it exceeds the respective L2 Mbps line rate(s).</source>
            <translation>Configuration invalide : &#xA;&#xA;Le total de la charge maximale de l' amont vers l'aval pour le service (s) #1 est 0 ou qu'elle dépasse le taux Mbps ligne respective C2 (s) .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;La charge maximale est supérieure à la #1 C1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Upstream direction.</source>
            <translation>Configuration invalide : &#xA;&#xA;La charge maximale est supérieure à la #1 C1 Mbps débit de ligne dans la direction amont .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L1 Mbps line rate in the Downstream direction.</source>
            <translation>Configuration invalide : &#xA;&#xA;La charge maximale est supérieure à la #1 C1 Mbps débit de ligne en direction de l'aval .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuration invalide:&#xA;&#xA;La charge maximale dépasse le débit de ligne #1 C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Upstream direction.</source>
            <translation>Configuration invalide:&#xA;&#xA;La charge maximale dépasse le débit de ligne #1 C2 Mbps dans la direction du flux en amont.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Maximum Load exceeds the #1 L2 Mbps line rate in the Downstream direction.</source>
            <translation>Configuration invalide:&#xA;&#xA;La charge maximale dépasse le débit de ligne #1 C2 Mbps dans la direction du flux en aval.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0.</source>
            <translation>Configuration invalide : &#xA;&#xA;CIR, EIR et le contrôle ne peuvent pas être 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Upstream direction.</source>
            <translation>Configuration invalide.&#xA;&#xA;CIR, EIR et le contrôle ne peuvent pas être tous à 0 dans le sens du flux montant</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;CIR, EIR and Policing cannot all be 0 in the Downstream direction.</source>
            <translation>Configuration invalide.&#xA;&#xA;CIR, EIR et le contrôle ne peuvent pas être tous à 0 dans le sens du flux descendant.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The smallest Step Load (#1% of CIR) cannot be attained using the #2 Mbps CIR setting for Service #3. The smallest Step Value using the current CIR for Service #3 is #4%.</source>
            <translation>Configuration invalide : &#xA;&#xA;La plus petite charge d'étape (#1% de CIR) ne peut pas être atteinte en utilisant le réglage de #2 Mbps du CIR pour le service #3. La plus petite valeur de l'étape utilisant le CIR actuel pour le service #3 est #4%.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;L' (C1 Mbps ) au total CIR est égal à 0 ou qu'il dépasse le #1 C1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;L' (C1 Mbps ) au total CIR pour la direction amont est 0 ou qu'elle dépasse le #1 L1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L1 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L1 Mbps line rate.</source>
            <translation>Configuration invalide : &#xA;&#xA;L' (C1 Mbps ) au total CIR pour la direction de l'aval est 0 ou qu'elle dépasse le #1 C1 Mbps débit de ligne .</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuration invalide:&#xA;&#xA;Le total CIR (C2 Mbps) est 0 ou il dépasse le débit de ligne #1 C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Upstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuration invalide:&#xA;&#xA;Le total CIR (C2 Mbps) pour la direction du flux en amont est 0 ou il dépasse le débit de ligne #1 C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (L2 Mbps) total for the Downstream direction is 0 or it exceeds the #1 L2 Mbps line rate.</source>
            <translation>Configuration invalide:&#xA;&#xA;Le total CIR (C2 Mbps) pour la direction du flux en aval est 0 ou il dépasse le débit de ligne #1 C2 Mbps.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;Either the Service Configuration test or the Service Performance test must be selected.</source>
            <translation>Configuration invalide :&#xA;&#xA;Soit les tests de configuration de service ou les tests de performance de service doivent être sélectionnés.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;At least one service must be selected.</source>
            <translation>Configuration invalide : &#xA;&#xA;Au moins un service doit être sélectionné.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The total of the selected services CIR (or EIR for those services that have a CIR of 0) cannot exceed line rate if you wish to run the Service Performance test.</source>
            <translation>Le total des services CIR sélectionnés (ou EIR pour les services qui ont un CIR de 0)</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuration invalide : &#xA;&#xA;La valur maximum spécifiée de la mesure de débit (RFC 2544) ne peut pas être inférieure à la somme des valeurs de CIR pour les services sélectionnés quand le test de performance de service est exécuté.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuration invalide.&#xA;&#xA;La valeur maximale spécifiée de la mesure de débit du flux montant (RFC 2544) ne peut pas être inférieure à la somme des valeurs CIR pour les services sélectionnés quand le test de performance de service est lancé.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the sum of the CIR values for the selected services when running the Service Performance test.</source>
            <translation>Configuration invalide.&#xA;&#xA;La valeur maximale spécifiée de la mesure de débit du flux descendant (RFC 2544) ne peut pas être inférieure à la somme des valeurs CIR pour les services sélectionnés quand le test de performance de service est lancé.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuration invalide : &#xA;&#xA;La valur maximum spécifiée de la mesure de débit (RFC 2544) ne peut pas être inférieure à la valeur du CIR quand le test de performance de service est exécuté.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Upstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuration invalide.&#xA;&#xA;La valeur maximale spécifiée de la mesure de débit du flux montant (RFC 2544) ne peut pas être inférieure à la valeur du CIR quand le test de performance de service est lancé.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The Downstream Throughput Measurement (RFC 2544) Max value specified cannot be less than the CIR value when running the Service Performance test.</source>
            <translation>Configuration invalide.&#xA;&#xA;La valeur maximale spécifiée de la mesure de débit du flux descendant (RFC 2544) ne peut pas être inférieure à la valeur du CIR quand le test de performance de service est lancé.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because Stop on Failure was selected and at least one KPI does not satisfy the SLA for Service #1.</source>
            <translation>SAM-Complete s'est arrêté parce que Arrêt sur échec a été sélectionné et au moins un KPI ne satisfait pas le SLA pour service #1.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAM-Complete s'est arrêté parce qu'aucune donnée n'a été retournée sur service #1 durant 10 secondes après le début du trafic. Le côté distant pourrait ne plus boucler le trafic en retour.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned on Service #1 within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAM-Complete s'est arrêté parce qu'aucune donnée n'a été retournée sur service #1 durant 10 secondes après le début du trafic. Le côté distant pourrait ne plus transmettre le trafic.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be looping traffic back.</source>
            <translation>SAMComplete s'est arrêté parce qu'aucune donnée n'est retournée dans les 10 secondes après le début du trafic. L'extrémité ne peut plus boucler le trafic au retour.</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete has stopped because no data was returned within 10 seconds after traffic was started. The far end may no longer be transmitting traffic.</source>
            <translation>SAM-Complete s'est arrêté parce qu'aucune donnée n'a été retournée durant 10 secondes après le début du trafic. Le côté distant pourrait ne plus transmettre le trafic.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a Traffic application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application locale est une application de flux et l'application distante à l'adresse IP #1 est une application de trafic.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Streams application and the remote application at IP address #1 is a TCP WireSpeed application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application locales est une application de flux et l'application distante à l'adresse IP #1 est une application de vitesse filaire.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 3 application and the remote application at IP address #1 is a Layer 2 application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application locales est une application de la couche 3 et l'application distante à l'adresse IP #1 est une application de la couche 2.</translation>
        </message>
        <message utf8="true">
            <source>The local and remote applications are incompatible. The local application is a Layer 2 application and the remote application at IP address #1 is a Layer 3 application.</source>
            <translation>Les applications locales et distantes ne sont pas compatibles. L'application locale est une application de la couche 2 et l'application distante à l'adresse IP #1 est une application de la couche 3.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for WAN IP. It is not compatible with SAMComplete.</source>
            <translation>L'application distante à l'adresse IP #1 est définie pour IP WAN. Elle n'est pas compatible avec SAMComplete</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for Stacked VLAN encapsulation. It is not compatible with SAMComplete.</source>
            <translation>L'application distante à l'adresse IP #1 est définie pour encapsulation VLAN empilée. Elle n'est pas compatible avec SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for VPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>L'application distante à l'adresse IP #1 est configurée pour une encapsulation VPLS. Il n'est pas compatible avec SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application at IP address #1 is set for MPLS encapsulation. It is not compatible with SAMComplete.</source>
            <translation>L'application distante à l'adresse IP #1 est configurée pour une encapsulation MPLS. Il n'est pas compatible avec SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>The remote application only supports #1 services. </source>
            <translation>L'application distante prends uniquement en charges les services #1.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Local unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Le test de délai dans un sens nécessite que les unités locales et distantes aient une source de synchronisation de délai dans un sens (OWD).  Le test de délai dans un sens a échoué sur l'unité locale.  Veuillez vérifier toutes les connexions au matériel source de synchronisation du délai dans un sens.</translation>
        </message>
        <message utf8="true">
            <source>The One Way Delay test requires both the local and remote test units have OWD Time Source Synchronization.  One Way Delay Synchronization failed on the Remote unit.  Please check all connections to OWD Time Source hardware.</source>
            <translation>Le test de délai dans un sens nécessite que les unités locales et distantes aient une source de synchronisation de délai dans un sens (OWD).  Le test de délai dans un sens a échoué sur l'unité distante.  Veuillez vérifier toutes les connexions au matériel source de synchronisation du délai dans un sens.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;A Round-Trip Time (RTT) test must be run before running the SAMComplete test.</source>
            <translation>Configuration invalide.&#xA;&#xA;Un test du temps de transmission aller-retour (RTT) doit être exécuté avant l'exécution du test SAMComplete.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossible d'établir une session TrueSpeed. Veuillez aller à la page "Réseau", vérifiez la configuration est réessayez.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Upstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossible d'établir une session TrueSpeed dans le sens montant. Veuillez aller à la page "Réseau",  vérifiez les configurations et réessayez.</translation>
        </message>
        <message utf8="true">
            <source>Unable to establish TrueSpeed session for the Downstream direction. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Impossible d'établir une session TrueSpeed dans le sens descendant. Veuillez aller à la page "Réseau",  vérifiez les configurations et réessayez.</translation>
        </message>
        <message utf8="true">
            <source>The Round-Trip Time (RTT) test has been invalidated by changing the currently selected services to test. Please go to the "TrueSpeed Controls" page and re-run the RTT test.</source>
            <translation>Le test du temps de transmission aller-retour (RTT) a été invalidé en changeant les service à tester en cours. Veuillez aller à la page des "contrôles TrueSpeed" et relancez le test RTT.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Downstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configuration invalide :&#xA;&#xA;Le total du CIR (Mbps) dans le sens descendant pour le(s) service(s) #1 ne peut pas être égal à 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) total for the Upstream direction for Service(s) #1 cannot be 0.</source>
            <translation>Configuration invalide :&#xA;&#xA;Le total du CIR (Mbps) dans le sens montant pour le(s) service(s) #1 ne peut pas être égal à 0.</translation>
        </message>
        <message utf8="true">
            <source>Invalid Configuration:&#xA;&#xA;The CIR (Mbps) for Service(s) #1 cannot be 0.</source>
            <translation>Le CIR (Mbps) pour Service(s) #1 ne peut pas être à 0.</translation>
        </message>
        <message utf8="true">
            <source>No traffic received. Please go to the "Network" page, verify configurations and try again.</source>
            <translation>Aucun trafic reçu. Veuillez aller à la page "Réseau", vérifier les configurations et essayer à nouveau.</translation>
        </message>
        <message utf8="true">
            <source>Main Result View</source>
            <translation>Affichage du résultat principal</translation>
        </message>
        <message utf8="true">
            <source>Stop&#xA;Tests</source>
            <translation>Arrêt&#xA;des tests</translation>
        </message>
        <message utf8="true">
            <source>  Report created, click Next to view</source>
            <translation>  Rapport créé, cliquez sur Suivant pour l'afficher</translation>
        </message>
        <message utf8="true">
            <source>Skip Report Viewing</source>
            <translation>Passer la visualisation du rapport</translation>
        </message>
        <message utf8="true">
            <source>SAMComplete - Ethernet Service Activation Test</source>
            <translation>SAM-Complete&#xA;Test d'activation du service Ethernet</translation>
        </message>
        <message utf8="true">
            <source>with TrueSpeed</source>
            <translation>avec TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Local Network Settings</source>
            <translation>Réglages de réseau local</translation>
        </message>
        <message utf8="true">
            <source>Local unit does not require configuration.</source>
            <translation>L'unité locale ne nécessite pas de configuration.</translation>
        </message>
        <message utf8="true">
            <source>Remote Network Settings</source>
            <translation>Réglages réseau distant</translation>
        </message>
        <message utf8="true">
            <source>Remote unit does not require configuration.</source>
            <translation>L'unité distante ne nécessite pas de configuration.</translation>
        </message>
        <message utf8="true">
            <source>Local IP Settings</source>
            <translation>Paramètre IP locaux</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Settings</source>
            <translation>Paramètres IP distant</translation>
        </message>
        <message utf8="true">
            <source>Services</source>
            <translation>Services</translation>
        </message>
        <message utf8="true">
            <source>Tagging</source>
            <translation>Étiquetage</translation>
        </message>
        <message utf8="true">
            <source>Tagging is not used.</source>
            <translation>L'étiquetage n'est pas utilisé.</translation>
        </message>
        <message utf8="true">
            <source>IP</source>
            <translation>IP</translation>
        </message>
        <message utf8="true">
            <source>SLA</source>
            <translation>SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput</source>
            <translation>SLA Largeur de Bande</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing</source>
            <translation>Contrôle(policing) de SLA</translation>
        </message>
        <message utf8="true">
            <source>No policing tests are selected.</source>
            <translation>Aucuns tests de contrôle ne sont sélectionnés.</translation>
        </message>
        <message utf8="true">
            <source>SLA Burst</source>
            <translation>Rafale SLA</translation>
        </message>
        <message utf8="true">
            <source>Burst testing has been disabled due to the absence of required functionality.</source>
            <translation>Un test de salve a été désactivé dû à l'absence de la fonctionnalité nécessaire.</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance</source>
            <translation>Performance SLA</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed is currently disabled.&#xA;&#xA;TrueSpeed can be enabled on the "Network" configuration page when not in Loopback measurement mode.</source>
            <translation>TrueSpeed est actuellement désactivé.&#xA;&#xA;TrueSpeed peut être activé sur la page de configuration "Réseau" quand il n'est pas dans le mode de mesure de boucle retour,</translation>
        </message>
        <message utf8="true">
            <source>Local Advanced Settings</source>
            <translation>Paramétrages avancés locaux</translation>
        </message>
        <message utf8="true">
            <source>Advanced Traffic Settings</source>
            <translation>Paramétrages avancés de trafic</translation>
        </message>
        <message utf8="true">
            <source>Advanced LBM Settings</source>
            <translation>Paramétrages avancés LBM</translation>
        </message>
        <message utf8="true">
            <source>Advanced SLA Settings</source>
            <translation>Paramétrages avancés SLA</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Size</source>
            <translation>Taille Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP</source>
            <translation>IP avancé</translation>
        </message>
        <message utf8="true">
            <source>L4 Advanced</source>
            <translation>C4 avancé</translation>
        </message>
        <message utf8="true">
            <source>Advanced Tagging</source>
            <translation>Étiquetage avancé</translation>
        </message>
        <message utf8="true">
            <source>Service Cfg Test</source>
            <translation>Test de configuration de service</translation>
        </message>
        <message utf8="true">
            <source>Service Perf Test</source>
            <translation>Perf service</translation>
        </message>
        <message utf8="true">
            <source>Exit Y.1564 Test</source>
            <translation>Quitter le test Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Y.1564:</source>
            <translation>Y.1564 :</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Status</source>
            <translation>Statut du serveur Discovery</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server Message</source>
            <translation>Message du serveur Discovery</translation>
        </message>
        <message utf8="true">
            <source>MAC Address and ARP Mode</source>
            <translation>Adresse MAC et Mode ARP</translation>
        </message>
        <message utf8="true">
            <source>SFP Selection</source>
            <translation>Choix SFP</translation>
        </message>
        <message utf8="true">
            <source>WAN Source IP</source>
            <translation>WAN IP Source</translation>
        </message>
        <message utf8="true">
            <source>Static - WAN IP</source>
            <translation>Statique - IP WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Gateway</source>
            <translation>Gateway WAN</translation>
        </message>
        <message utf8="true">
            <source>WAN Subnet Mask</source>
            <translation>Masque du sous-réseau WAN</translation>
        </message>
        <message utf8="true">
            <source>Traffic Source IP</source>
            <translation>IP de source de trafic</translation>
        </message>
        <message utf8="true">
            <source>Traffic Subnet Mask</source>
            <translation>Masque de sous réseau de trafic</translation>
        </message>
        <message utf8="true">
            <source>Is VLAN Tagging used on the Local network port?</source>
            <translation>Le marquage VLAN est-il utilisé sur le port réseau local ?</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q (Stacked VLAN)</source>
            <translation>Q-dans-Q (VLAN empilés)</translation>
        </message>
        <message utf8="true">
            <source>Waiting</source>
            <translation>Attente</translation>
        </message>
        <message utf8="true">
            <source>Accessing Server...</source>
            <translation>Accès au serveur en cours...</translation>
        </message>
        <message utf8="true">
            <source>Cannot Access Server</source>
            <translation>Ne peut pas accéder au serveur</translation>
        </message>
        <message utf8="true">
            <source>IP Obtained</source>
            <translation>IP obtenue</translation>
        </message>
        <message utf8="true">
            <source>Lease Granted: #1 min.</source>
            <translation>Allocation acquise:#1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Reduced: #1 min.</source>
            <translation>Allocation réduite:#1 min.</translation>
        </message>
        <message utf8="true">
            <source>Lease Released</source>
            <translation>Allocation relâchée</translation>
        </message>
        <message utf8="true">
            <source>Discovery Server:</source>
            <translation>Serveur Discovery:</translation>
        </message>
        <message utf8="true">
            <source>VoIP</source>
            <translation>VoIP</translation>
        </message>
        <message utf8="true">
            <source>#1 L2 Mbps CIR</source>
            <translation>#1 C2 Mbps CIR</translation>
        </message>
        <message utf8="true">
            <source>#1 kB CBS</source>
            <translation>#1 kB CBS</translation>
        </message>
        <message utf8="true">
            <source>#1 FLR, #2 ms FTD, #3 ms FDV</source>
            <translation>#1 FLR, #2 ms FTD, #3 ms FDV</translation>
        </message>
        <message utf8="true">
            <source>Service 1: VoIP - 25% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 1: VoIP - 25% de trafic - #1 et #2 trames d'octet</translation>
        </message>
        <message utf8="true">
            <source>Service 2: Video Telephony - 10% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 2: Vidéo téléphonie - 10% du trafic - #1 et #2 trames d'octet</translation>
        </message>
        <message utf8="true">
            <source>Service 3: Priority Data - 15% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 3: Données de prirorité - 15% du trafic - #1 et #2 trames d'octet</translation>
        </message>
        <message utf8="true">
            <source>Service 4: BE Data - 50% of Traffic - #1 and #2 byte frames</source>
            <translation>Service 4: Données BE - 50% du trafic -#1 et #2 trames d'octet</translation>
        </message>
        <message utf8="true">
            <source>All Services</source>
            <translation>Tous services</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Triple Play Properties</source>
            <translation>Propriétés du service 1 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Voice</source>
            <translation>Voix</translation>
        </message>
        <message utf8="true">
            <source>HDTV</source>
            <translation>HDTV</translation>
        </message>
        <message utf8="true">
            <source>SDTV</source>
            <translation>SDTV</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 56K</source>
            <translation>G.711 Loi U 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 56K</source>
            <translation>G.711 Loi A 56K</translation>
        </message>
        <message utf8="true">
            <source>G.711 U law 64K</source>
            <translation>G.711 Loi U 64K</translation>
        </message>
        <message utf8="true">
            <source>G.711 A law 64K</source>
            <translation>G.711 Loi A 64K</translation>
        </message>
        <message utf8="true">
            <source>G.723 5.3K</source>
            <translation>G.723 5.3K</translation>
        </message>
        <message utf8="true">
            <source>G.723 6.3K</source>
            <translation>G.723 6.3K</translation>
        </message>
        <message utf8="true">
            <source>G.728</source>
            <translation>G.728</translation>
        </message>
        <message utf8="true">
            <source>G.729</source>
            <translation>G.729</translation>
        </message>
        <message utf8="true">
            <source>G.729A</source>
            <translation>G.729A</translation>
        </message>
        <message utf8="true">
            <source>G.726 32K</source>
            <translation>G.726 32K</translation>
        </message>
        <message utf8="true">
            <source>G.722 64K</source>
            <translation>G.722 64K</translation>
        </message>
        <message utf8="true">
            <source>H.261</source>
            <translation>H.261</translation>
        </message>
        <message utf8="true">
            <source>H.263</source>
            <translation>H.263</translation>
        </message>
        <message utf8="true">
            <source>GSM-FR</source>
            <translation>GSM-FR</translation>
        </message>
        <message utf8="true">
            <source>GSM-EFR</source>
            <translation>GSM-EFR</translation>
        </message>
        <message utf8="true">
            <source>AMR 4.75</source>
            <translation>AMR 4.75</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.40</source>
            <translation>AMR 7.40</translation>
        </message>
        <message utf8="true">
            <source>AMR 7.95</source>
            <translation>AMR 7.95</translation>
        </message>
        <message utf8="true">
            <source>AMR 10.20</source>
            <translation>AMR 10.20</translation>
        </message>
        <message utf8="true">
            <source>AMR 12.20</source>
            <translation>AMR 12.20</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 6.6</source>
            <translation>AMR-WB G.722.2 6.6</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 8.5</source>
            <translation>AMR-WB G.722.2 8.5</translation>
        </message>
        <message utf8="true">
            <source>AMR-WB G.722.2 12.65</source>
            <translation>AMR-WB G.722.2 12.65</translation>
        </message>
        <message utf8="true">
            <source>20</source>
            <translation>20</translation>
        </message>
        <message utf8="true">
            <source>40</source>
            <translation>40</translation>
        </message>
        <message utf8="true">
            <source>50</source>
            <translation>50</translation>
        </message>
        <message utf8="true">
            <source>70</source>
            <translation>70</translation>
        </message>
        <message utf8="true">
            <source>80</source>
            <translation>80</translation>
        </message>
        <message utf8="true">
            <source>MPEG-2</source>
            <translation>MPEG-2</translation>
        </message>
        <message utf8="true">
            <source>MPEG-4</source>
            <translation>MPEG-4</translation>
        </message>
        <message utf8="true">
            <source>At 10GE, the bandwidth granularity level is 0.1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>À 10GE, le niveau de granularité de bande passante est 0,1 Mbps, par conséquent, la valeur de CIR est un multiple de ce niveau de granularité</translation>
        </message>
        <message utf8="true">
            <source>At 40GE, the bandwidth granularity level is 0.4 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>À 40GE, le niveau de granularité de la bande passante est 0,4 Mbps ; en raison de quoi la valeur de CIR est un multiple de ce niveau de granularité</translation>
        </message>
        <message utf8="true">
            <source>At 100GE, the bandwidth granularity level is 1 Mbps; as a result, the CIR value is a multiple of this granularity level</source>
            <translation>À 100GE, le niveau de granularité de la bande passante est de 1 Mbps, par conséquent, la valeur CIR est un multiple de ce niveau de granularité</translation>
        </message>
        <message utf8="true">
            <source>Configure Sizes</source>
            <translation>Configurer tailles</translation>
        </message>
        <message utf8="true">
            <source>Frame Size (Bytes)</source>
            <translation>Taille de trame (Octets)</translation>
        </message>
        <message utf8="true">
            <source>Defined Length</source>
            <translation>Longueur définie</translation>
        </message>
        <message utf8="true">
            <source>EMIX Cycle Length</source>
            <translation>Durée du cycle EMIX</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>Longueur de Paquet (octets)</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Length</source>
            <translation>Calc. de longueur de trame</translation>
        </message>
        <message utf8="true">
            <source>The Calc. Frame Size is determined by using the Packet Length and the Encapsulation.</source>
            <translation>La taille de la trame est calculée en utilisant la taille du paquet et l'encapsulation.</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Triple Play Properties</source>
            <translation>Propriétés du service 2 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Triple Play Properties</source>
            <translation>Propriétés du service 3 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Triple Play Properties</source>
            <translation>Propriétés du service 4 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Triple Play Properties</source>
            <translation>Propriétés du service 5 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Triple Play Properties</source>
            <translation>Propriétés du service 6 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Triple Play Properties</source>
            <translation>Propriétés du service 7 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Triple Play Properties</source>
            <translation>Propriétés du service 8 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Triple Play Properties</source>
            <translation>Propriétés du service 9 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Triple Play Properties</source>
            <translation>Propriétés du service 10 du triple service.</translation>
        </message>
        <message utf8="true">
            <source>VPLS</source>
            <translation>VPLS</translation>
        </message>
        <message utf8="true">
            <source>Undersized</source>
            <translation>Trop longue</translation>
        </message>
        <message utf8="true">
            <source>7</source>
            <translation>7</translation>
        </message>
        <message utf8="true">
            <source>Number of Services</source>
            <translation>Nombre de services</translation>
        </message>
        <message utf8="true">
            <source>Layer</source>
            <translation>Couche</translation>
        </message>
        <message utf8="true">
            <source>LBM settings</source>
            <translation>Paramétrages LBM</translation>
        </message>
        <message utf8="true">
            <source>Configure Triple Play...</source>
            <translation>Configure le service triple ...</translation>
        </message>
        <message utf8="true">
            <source>Service Properties</source>
            <translation>Propriétés service</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Frame Size settings</source>
            <translation>Paramétrages de taille de trame et MAC DA</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Frame Size settings and EtherType</source>
            <translation>DA MAC, paramétyrages de taille de trame et type Ethernet</translation>
        </message>
        <message utf8="true">
            <source>DA MAC, Packet Length and TTL settings</source>
            <translation>Paramétrages TTL et longueur de paquet, MAC DA</translation>
        </message>
        <message utf8="true">
            <source>DA MAC and Packet Length settings</source>
            <translation>MAC Adresse Destination et paramétrages de longueur de paquet</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Advanced)</source>
            <translation>Réglages du réseau (avancé)</translation>
        </message>
        <message utf8="true">
            <source>A local / remote unit incompatibility requires a &lt;b>%1&lt;/b> byte packet length or larger.</source>
            <translation>Une incompatibilité d'unité locale/distante nécessite une longueur de paquet d'octet &lt;b>%1&lt;/b> ou plus grande.</translation>
        </message>
        <message utf8="true">
            <source>Service</source>
            <translation>Service</translation>
        </message>
        <message utf8="true">
            <source>Configure...</source>
            <translation>Configuration...</translation>
        </message>
        <message utf8="true">
            <source>User Size</source>
            <translation> Taille utilisateur</translation>
        </message>
        <message utf8="true">
            <source>TTL (hops)</source>
            <translation>TTL (hops)</translation>
        </message>
        <message utf8="true">
            <source>Dest. MAC Address</source>
            <translation>Adresse MAC Dest</translation>
        </message>
        <message utf8="true">
            <source>Show Both</source>
            <translation>Montrer le deux</translation>
        </message>
        <message utf8="true">
            <source>Remote Only</source>
            <translation>Distant seulement</translation>
        </message>
        <message utf8="true">
            <source>Local Only</source>
            <translation>Local seulement</translation>
        </message>
        <message utf8="true">
            <source>LBM Settings (Advanced)</source>
            <translation>Paramétrages LBM (avancés)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings Random/EMIX Size</source>
            <translation>Paramétrages du réseau Aléatoire/Taille EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 1 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Frame Size 1</source>
            <translation>Taille 1 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 1</source>
            <translation>Longueur 1 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 1</source>
            <translation>Taille 1 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 1</source>
            <translation>Taille Jumbo 1</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 2</source>
            <translation>Taille 2 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 2</source>
            <translation>Longueur 2 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 2</source>
            <translation>Taille 2 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 2</source>
            <translation>Taille Jumbo 2</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 3</source>
            <translation>Taille 3 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 3</source>
            <translation>Longueur 3 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 3</source>
            <translation>Taille 3 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 3</source>
            <translation>Taille Jumbo 3</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 4</source>
            <translation>Taille 4 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 4</source>
            <translation>Longueur 4 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 4</source>
            <translation>Taille 4 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 4</source>
            <translation>Taille Jumbo 4</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 5</source>
            <translation>Taille 5 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 5</source>
            <translation>Longueur 5 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 5</source>
            <translation>Taille 5 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 5</source>
            <translation>Taille Jumbo 5</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 6</source>
            <translation>Taille 6 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 6</source>
            <translation>Longueur 6 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 6</source>
            <translation>Taille 6 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 6</source>
            <translation>Taille Jumbo 6</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 7</source>
            <translation>Taille 7 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 7</source>
            <translation>Longueur 7 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 7</source>
            <translation>Taille 7 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 7</source>
            <translation>Taille Jumbo 7</translation>
        </message>
        <message utf8="true">
            <source>Frame Size 8</source>
            <translation>Taille 8 de trame</translation>
        </message>
        <message utf8="true">
            <source>Packet Length 8</source>
            <translation>Longueur 8 de paquet</translation>
        </message>
        <message utf8="true">
            <source>User Size 8</source>
            <translation>Taille 8 Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Size 8</source>
            <translation>Taille Jumbo 8</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 1 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 1 Random/EMIX Lengths</source>
            <translation>Longueurs Service 1 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 2 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 2 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 2 Random/EMIX Lengths</source>
            <translation>Longueurs Service 2 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 3 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 3 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 3 Random/EMIX Lengths</source>
            <translation>Longueurs Service 3 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 4 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 4 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 4 Random/EMIX Lengths</source>
            <translation>Longueurs Service 4 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 5 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 5 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 5 Random/EMIX Lengths</source>
            <translation>Longueurs Service 5 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 6 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 6 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 6 Random/EMIX Lengths</source>
            <translation>Longueurs Service 6 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 7 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 7 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 7 Random/EMIX Lengths</source>
            <translation>Longueurs Service 7 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 8 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 8 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 8 Random/EMIX Lengths</source>
            <translation>Longueurs Service 8 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 9 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 9 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 9 Random/EMIX Lengths</source>
            <translation>Longueurs Service 9 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Service 10 Aléatoire/EMIX </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Service 10 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Service 10 Random/EMIX Lengths</source>
            <translation>Longueurs Service 10 Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Do services have different VLAN ID's or User Priorities?</source>
            <translation>Les services ont-ils des ID VLAN ou des priorités utilisateurs différentes ?</translation>
        </message>
        <message utf8="true">
            <source>No, all services use the same VLAN settings</source>
            <translation>Non, tous les services utilisent le même réglage VLAN</translation>
        </message>
        <message utf8="true">
            <source>DEI Bit</source>
            <translation>DEI Bit</translation>
        </message>
        <message utf8="true">
            <source>Advanced...</source>
            <translation>Avancée...</translation>
        </message>
        <message utf8="true">
            <source>User Pri.</source>
            <translation>Utilisateur du Pri.</translation>
        </message>
        <message utf8="true">
            <source>SVLAN Pri.</source>
            <translation>Pri SVLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN Tagging (Encapsulation) Settings</source>
            <translation>Paramètres de marquage VLAN (Encapsulation) </translation>
        </message>
        <message utf8="true">
            <source>SVLAN Priority</source>
            <translation>Priorité SVLAN</translation>
        </message>
        <message utf8="true">
            <source>TPID</source>
            <translation>TPID</translation>
        </message>
        <message utf8="true">
            <source>All services will use the same Traffic Destination IP.</source>
            <translation>Tous les services utiliseront la même IP de destination de trafic</translation>
        </message>
        <message utf8="true">
            <source>Traffic Destination IP</source>
            <translation>IP de destination de trafic</translation>
        </message>
        <message utf8="true">
            <source>Dest. IP</source>
            <translation>IP Dest.</translation>
        </message>
        <message utf8="true">
            <source>Traffic Dest. IP</source>
            <translation>IP de dest. De trafic</translation>
        </message>
        <message utf8="true">
            <source>Set IP ID Incrementing</source>
            <translation>Définir IP ID incrémentant</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Local Only)</source>
            <translation>Paramétrages IP (local uniquement)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings</source>
            <translation>Paramètres IP</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Remote Only)</source>
            <translation>Paramétrages IP (distant uniquement)</translation>
        </message>
        <message utf8="true">
            <source>IP Settings (Advanced)</source>
            <translation>Paramétrages IP (avancés)</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings</source>
            <translation>Paramétrages avancés IP</translation>
        </message>
        <message utf8="true">
            <source>Do services have different TOS or DSCP settings?</source>
            <translation>Les services ont-ils des réglages TOS ou DSCP différents ?</translation>
        </message>
        <message utf8="true">
            <source>No, TOS/DSCP is the same on all services</source>
            <translation>Non, TOS/DSCP est le même pour tous les services</translation>
        </message>
        <message utf8="true">
            <source>TOS/DSCP</source>
            <translation>TOS/DSCP</translation>
        </message>
        <message utf8="true">
            <source>Aggregate SLAs</source>
            <translation>SLAs total</translation>
        </message>
        <message utf8="true">
            <source>Aggregate CIR</source>
            <translation>Total CIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate CIR</source>
            <translation>CIR total de flux montant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate CIR</source>
            <translation>CIR total de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Aggregate EIR</source>
            <translation>Total EIR</translation>
        </message>
        <message utf8="true">
            <source>Upstream Aggregate EIR</source>
            <translation>EIR total de flux montant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Aggregate EIR</source>
            <translation>EIR total de flux descendant</translation>
        </message>
        <message utf8="true">
            <source>Aggregate Mode</source>
            <translation>Mode total</translation>
        </message>
        <message utf8="true">
            <source>CIR</source>
            <translation>CIR</translation>
        </message>
        <message utf8="true">
            <source>EIR</source>
            <translation>EIR</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Local (Mbps)</source>
            <translation>Débit de test local max. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Max. Remote (Mbps)</source>
            <translation>Débit de test distant max. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Enable Aggregate Mode</source>
            <translation>Autoriser le mode Total</translation>
        </message>
        <message utf8="true">
            <source>WARNING: The selected weight values currently sum up to &lt;b>%1&lt;/b>%, not 100%</source>
            <translation>AVERTISSEMENT: Les valeurs actuelles de poids sélectionnées ont pour somme &lt;b>%1&lt;/b>%, pas 100%</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, Mbps</source>
            <translation>Débit SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps</source>
            <translation>Débit SAL, C2 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps</source>
            <translation>Débit de SLA, C1 Mbps</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L1 Mbps (One Way)</source>
            <translation>Débit de SLA, C1 Mbps (aller simple)</translation>
        </message>
        <message utf8="true">
            <source>SLA Throughput, L2 Mbps (One Way)</source>
            <translation>Débit SLA, C2 Mbps (uni directionnel)</translation>
        </message>
        <message utf8="true">
            <source>Weight (%)</source>
            <translation>Poids (%)</translation>
        </message>
        <message utf8="true">
            <source>Policing</source>
            <translation>Contrôle</translation>
        </message>
        <message utf8="true">
            <source>Max Load</source>
            <translation>Charge max</translation>
        </message>
        <message utf8="true">
            <source>Downstream Only</source>
            <translation>Flux descendant seulement</translation>
        </message>
        <message utf8="true">
            <source>Upstream Only</source>
            <translation>Flux montant seulement</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Traffic settings</source>
            <translation>Définir paramétrages avancés de trafic</translation>
        </message>
        <message utf8="true">
            <source>SLA Policing, Mbps</source>
            <translation>Contrôle SLA, Mbps</translation>
        </message>
        <message utf8="true">
            <source>CIR+EIR</source>
            <translation>CIR+EIR</translation>
        </message>
        <message utf8="true">
            <source>M</source>
            <translation>M</translation>
        </message>
        <message utf8="true">
            <source>Max Policing</source>
            <translation>Contrôle max.</translation>
        </message>
        <message utf8="true">
            <source>Perform Burst Testing</source>
            <translation>Réaliser test de salve</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (+%)</source>
            <translation>Tolérance (+%)</translation>
        </message>
        <message utf8="true">
            <source>Tolerance (-%)</source>
            <translation>Tolérance (-%)</translation>
        </message>
        <message utf8="true">
            <source>Would you like to perform burst testing?</source>
            <translation>Vous souhaitez effectuer des essais de rafales ?</translation>
        </message>
        <message utf8="true">
            <source>Burst Test Type:</source>
            <translation>Type de test de salve:</translation>
        </message>
        <message utf8="true">
            <source>Set advanced Burst Settings</source>
            <translation>Définir paramétrages avancés de salve</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Precision</source>
            <translation>Précision de délai de trame</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay as an SLA requirement</source>
            <translation>Inclure délai de trame comme besoin SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation as an SLA requirement</source>
            <translation>Inclure variation de délai de trame comme besoin SLA</translation>
        </message>
        <message utf8="true">
            <source>SLA Performance (One Way)</source>
            <translation>Performance SLA (dans un sens)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (RTD, ms)</source>
            <translation>Délai de trame (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay (OWD, ms)</source>
            <translation>Délai de trame (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Set advanced SLA Settings</source>
            <translation>Définir paramétrages avancés SLA</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration</source>
            <translation>Configuration service</translation>
        </message>
        <message utf8="true">
            <source>Number of Steps Below CIR</source>
            <translation>Nombre d'étapes sous CIR</translation>
        </message>
        <message utf8="true">
            <source>Step Duration (sec)</source>
            <translation>Durée de l'étape (sec)</translation>
        </message>
        <message utf8="true">
            <source>Step 1 % CIR</source>
            <translation>Etape 1 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 2 % CIR</source>
            <translation>Etape 3 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 3 % CIR</source>
            <translation>Etape 3 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 4 % CIR</source>
            <translation>Etape 4 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 5 % CIR</source>
            <translation>Etape 5 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 6 % CIR</source>
            <translation>Etape 6 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 7 % CIR</source>
            <translation>Etape 7 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 8 % CIR</source>
            <translation>Etape 8 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 9 % CIR</source>
            <translation>Etape 9 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step 10 % CIR</source>
            <translation>Etape 10 % du CIR</translation>
            <comment>This text appears in test report for a setup item.</comment>
        </message>
        <message utf8="true">
            <source>Step Percents</source>
            <translation>Les pourcentages de l'étape</translation>
        </message>
        <message utf8="true">
            <source>% CIR</source>
            <translation>% CIR</translation>
        </message>
        <message utf8="true">
            <source>100% (CIR)</source>
            <translation>100% (CIR)</translation>
        </message>
        <message utf8="true">
            <source>0%</source>
            <translation>0%</translation>
        </message>
        <message utf8="true">
            <source>Service Performance</source>
            <translation>Performance service</translation>
        </message>
        <message utf8="true">
            <source>Each direction is tested separately, so overall test duration will be twice the entered value.</source>
            <translation>Chaque direction est testée séparément , ainsi la durée globale de test sera deux fois la valeur saisie.</translation>
        </message>
        <message utf8="true">
            <source>Stop Test on Failure</source>
            <translation>Arrêter le test sur échec</translation>
        </message>
        <message utf8="true">
            <source>Advanced Burst Test Settings</source>
            <translation>Paramétrages avancés de test de salve</translation>
        </message>
        <message utf8="true">
            <source>Skip J-QuickCheck</source>
            <translation>Passer J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>Select Y.1564 Tests</source>
            <translation>Sélectionner tests Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Select Services to Test</source>
            <translation>Sélectionner services à tester</translation>
        </message>
        <message utf8="true">
            <source>CIR (L1 Mbps)</source>
            <translation>CIR (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR (L2 Mbps)</source>
            <translation>CIR (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>   1</source>
            <translation>   1</translation>
        </message>
        <message utf8="true">
            <source>   2</source>
            <translation>   2</translation>
        </message>
        <message utf8="true">
            <source>   3</source>
            <translation>   3</translation>
        </message>
        <message utf8="true">
            <source>   4</source>
            <translation>   4</translation>
        </message>
        <message utf8="true">
            <source>   5</source>
            <translation>   5</translation>
        </message>
        <message utf8="true">
            <source>   6</source>
            <translation>   6</translation>
        </message>
        <message utf8="true">
            <source>   7</source>
            <translation>   7</translation>
        </message>
        <message utf8="true">
            <source>   8</source>
            <translation>   8</translation>
        </message>
        <message utf8="true">
            <source>   9</source>
            <translation>   9</translation>
        </message>
        <message utf8="true">
            <source>  10</source>
            <translation>  10</translation>
        </message>
        <message utf8="true">
            <source>Set All</source>
            <translation>Validé Tout</translation>
        </message>
        <message utf8="true">
            <source>  Total:</source>
            <translation>  Total :</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Test</source>
            <translation>Tests de configuration service</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Test</source>
            <translation>Tests de performance service</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements</source>
            <translation>Mesures optionnelles</translation>
        </message>
        <message utf8="true">
            <source>Throughput (RFC 2544)</source>
            <translation>Débit (RFC 2544)</translation>
        </message>
        <message utf8="true">
            <source>Max. (Mbps)</source>
            <translation>Max. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L1 Mbps)</source>
            <translation>Max. (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. (L2 Mbps)</source>
            <translation>Max. (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Optional Measurements cannot be performed when a TrueSpeed service has been enabled.</source>
            <translation>Les mesures optionnelles ne peuvent pas être effectuées quand un service TrueSpeed a été activé.</translation>
        </message>
        <message utf8="true">
            <source>Run Y.1564 Tests</source>
            <translation>Lancer tests Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Skip Y.1564 Tests</source>
            <translation>Passer les tests Y.1564</translation>
        </message>
        <message utf8="true">
            <source>8</source>
            <translation>8</translation>
        </message>
        <message utf8="true">
            <source>9</source>
            <translation>9</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Retard</translation>
        </message>
        <message utf8="true">
            <source>Delay Var</source>
            <translation>Variable de délai</translation>
        </message>
        <message utf8="true">
            <source>Summary of Test Failures</source>
            <translation>Résumé des échecs de test</translation>
        </message>
        <message utf8="true">
            <source>Verdicts</source>
            <translation>Verdicts</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Y.1564 Verdict</source>
            <translation>Verdict du test Y.1564</translation>
        </message>
        <message utf8="true">
            <source>Config Test Verdict</source>
            <translation>Verdict configuration test</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 1 Verdict</source>
            <translation>Verdict configuration test Svc 1</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 2 Verdict</source>
            <translation>Verdict configuration test Svc 2</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 3 Verdict</source>
            <translation>Verdict configuration test Svc 3</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 4 Verdict</source>
            <translation>Verdict configuration test Svc 4</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 5 Verdict</source>
            <translation>Verdict configuration test Svc 5</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 6 Verdict</source>
            <translation>Verdict configuration test Svc 6</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 7 Verdict</source>
            <translation>Verdict configuration test Svc 7</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 8 Verdict</source>
            <translation>Verdict configuration test Svc 8</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 9 Verdict</source>
            <translation>Verdict configuration test Svc 9</translation>
        </message>
        <message utf8="true">
            <source>Config Test Svc 10 Verdict</source>
            <translation>Verdict configuration test Svc 10</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Verdict</source>
            <translation>Verdict des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 1 Verdict</source>
            <translation>Verdict Svc 1 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 2 Verdict</source>
            <translation>Verdict Svc 2 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 3 Verdict</source>
            <translation>Verdict Svc 3 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 4 Verdict</source>
            <translation>Verdict Svc 4 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 5 Verdict</source>
            <translation>Verdict Svc 5 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 6 Verdict</source>
            <translation>Verdict Svc 6 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 7 Verdict</source>
            <translation>Verdict Svc 7 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 8 Verdict</source>
            <translation>Verdict Svc 8 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 9 Verdict</source>
            <translation>Verdict Svc 9 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Svc 10 Verdict</source>
            <translation>Verdict Svc 10 des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test IR Verdict</source>
            <translation>Verdict IR des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Frame Loss Verdict</source>
            <translation>Verdict des pertes de trames des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Verdict</source>
            <translation>Verdict du délai des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test Delay Variation Verdict</source>
            <translation>Verdict des variations de délai des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Perf Test TrueSpeed Verdict</source>
            <translation>Verdict TrueSpeed des tests de performance</translation>
        </message>
        <message utf8="true">
            <source>Service Configuration Results</source>
            <translation>Résultats de configuration service</translation>
        </message>
        <message utf8="true">
            <source> 1 </source>
            <translation> 1 </translation>
        </message>
        <message utf8="true">
            <source>Service 1 Configuration Results</source>
            <translation>Résultats de configuration service 1</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L1 Mbps)</source>
            <translation>Débit max (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L1 Mbps)</source>
            <translation>Débit max en aval (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L1 Mbps)</source>
            <translation>Débit max amont (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Throughput (L2 Mbps)</source>
            <translation>Débit max (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Max Throughput (L2 Mbps)</source>
            <translation>Débit max en aval (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Max Throughput (L2 Mbps)</source>
            <translation>Débit max amont (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CIR Verdict</source>
            <translation>Verdict CIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR Verdict</source>
            <translation>Verdict CIR en aval</translation>
        </message>
        <message utf8="true">
            <source>IR (L1 Mbps)</source>
            <translation>IR (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (L2 Mbps)</source>
            <translation>IR (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Delay Variation (ms)</source>
            <translation>Délai trame variation (ms)</translation>
        </message>
        <message utf8="true">
            <source>OoS Count</source>
            <translation>Comptage OoS</translation>
        </message>
        <message utf8="true">
            <source>Error Frame Detect</source>
            <translation>trame de détection d'erreur</translation>
        </message>
        <message utf8="true">
            <source>Pause Detect</source>
            <translation>Détection de pause</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR Verdict</source>
            <translation>Verdict CIR amont</translation>
        </message>
        <message utf8="true">
            <source>CBS Verdict</source>
            <translation>Verdict CBS</translation>
        </message>
        <message utf8="true">
            <source>Downstream CBS Verdict</source>
            <translation>Verdict CBS en aval</translation>
        </message>
        <message utf8="true">
            <source>Configured Burst Size (kB)</source>
            <translation>Taille de rafale configurée (ko)</translation>
        </message>
        <message utf8="true">
            <source>Tx Burst Size (kB)</source>
            <translation>Taille de salve Tx (kO)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rx Burst Size (kB)</source>
            <translation>Taille Moy de rafale Rx (kB)</translation>
        </message>
        <message utf8="true">
            <source>Estimated CBS (kB)</source>
            <translation>Estimée CBS ( kB)</translation>
        </message>
        <message utf8="true">
            <source>Upstream CBS Verdict</source>
            <translation>Verdict CBS amont</translation>
        </message>
        <message utf8="true">
            <source>EIR Verdict</source>
            <translation>Verdict du test EIR</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR Verdict</source>
            <translation>Verdict du test EIR en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR Verdict</source>
            <translation>Verdict du test EIR amont</translation>
        </message>
        <message utf8="true">
            <source>Policing Verdict</source>
            <translation>Résultat du contrôle</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing Verdict</source>
            <translation>Résultat du contrôle en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing Verdict</source>
            <translation>Résultat du contrôle amont</translation>
        </message>
        <message utf8="true">
            <source>Step 1 Verdict</source>
            <translation>Verdict étape 1</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 1 Verdict</source>
            <translation>Verdict étape 1 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 1 Verdict</source>
            <translation>Verdict étape 1 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 2 Verdict</source>
            <translation>Verdict étape 2</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 2 Verdict</source>
            <translation>Verdict étape 2 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 2 Verdict</source>
            <translation>Verdict étape 2 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 3 Verdict</source>
            <translation>Verdict étape 3</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 3 Verdict</source>
            <translation>Verdict étape 3 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 3 Verdict</source>
            <translation>Verdict étape 3 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 4 Verdict</source>
            <translation>Verdict étape 4</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 4 Verdict</source>
            <translation>Verdict étape 4 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 4 Verdict</source>
            <translation>Verdict étape 4 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 5 Verdict</source>
            <translation>Verdict étape 5</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 5 Verdict</source>
            <translation>Verdict étape 5 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 5 Verdict</source>
            <translation>Verdict étape 5 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 6 Verdict</source>
            <translation>Verdict étape 6</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 6 Verdict</source>
            <translation>Verdict étape 6 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 6 Verdict</source>
            <translation>Verdict étape 6 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 7 Verdict</source>
            <translation>Verdict étape 7</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 7 Verdict</source>
            <translation>Verdict étape 7 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 7 Verdict</source>
            <translation>Verdict étape 7 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 8 Verdict</source>
            <translation>Verdict étape 8</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 8 Verdict</source>
            <translation>Verdict étape 8 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 8 Verdict</source>
            <translation>Verdict étape 8 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 9 Verdict</source>
            <translation>Verdict étape 9</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 9 Verdict</source>
            <translation>Verdict étape 9 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 9 Verdict</source>
            <translation>Verdict étape 9 amont</translation>
        </message>
        <message utf8="true">
            <source>Step 10 Verdict</source>
            <translation>Verdict étape 10</translation>
        </message>
        <message utf8="true">
            <source>Downstream Step 10 Verdict</source>
            <translation>Verdict étape 10 en aval</translation>
        </message>
        <message utf8="true">
            <source>Upstream Step 10 Verdict</source>
            <translation>Verdict étape 10 amont</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L1 Mbps)</source>
            <translation>Débit max (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (L2 Mbps)</source>
            <translation>Max. débit (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max. Throughput (Mbps)</source>
            <translation>Débit max. (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Steps</source>
            <translation>Étapes</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>Burst</translation>
        </message>
        <message utf8="true">
            <source>Key</source>
            <translation>Clé</translation>
        </message>
        <message utf8="true">
            <source>Click bars to review results for each step.</source>
            <translation>Cliquer les barres pour voir les résultats de chaque étape.</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput Mbps)</source>
            <translation>IR (Débit Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L1 Mbps)</source>
            <translation>IR (Débit C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR (Throughput L2 Mbps)</source>
            <translation>IR (Débit C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>CBS</source>
            <translation>CBS</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>IR (Mbps)</source>
            <translation>IR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Error Detect</source>
            <translation>détection d'erreur</translation>
        </message>
        <message utf8="true">
            <source>#1</source>
            <translation>#1</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#2</source>
            <translation>#2</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#3</source>
            <translation>#3</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#4</source>
            <translation>#4</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#5</source>
            <translation>#5</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#6</source>
            <translation>#6</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#7</source>
            <translation>#7</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#8</source>
            <translation>#8</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#9</source>
            <translation>#9</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>#10</source>
            <translation>#10</translation>
            <comment>This is a result category name.</comment>
        </message>
        <message utf8="true">
            <source>Step 1</source>
            <translation>Etape 1</translation>
        </message>
        <message utf8="true">
            <source>Step 2</source>
            <translation>Etape 2</translation>
        </message>
        <message utf8="true">
            <source>Step 3</source>
            <translation>Etape 3</translation>
        </message>
        <message utf8="true">
            <source>Step 4</source>
            <translation>Etape 4</translation>
        </message>
        <message utf8="true">
            <source>Step 5</source>
            <translation>Etape 5</translation>
        </message>
        <message utf8="true">
            <source>Step 6</source>
            <translation>Etape 6</translation>
        </message>
        <message utf8="true">
            <source>Step 7</source>
            <translation>Etape 7</translation>
        </message>
        <message utf8="true">
            <source>Step 8</source>
            <translation>Etape 8</translation>
        </message>
        <message utf8="true">
            <source>Step 9</source>
            <translation>Etape 9</translation>
        </message>
        <message utf8="true">
            <source>Step 10</source>
            <translation>Etape 10</translation>
        </message>
        <message utf8="true">
            <source>SLA Thresholds</source>
            <translation>Seuils SLA</translation>
        </message>
        <message utf8="true">
            <source>EIR (Mbps)</source>
            <translation>EIR (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L1 Mbps)</source>
            <translation>EIR (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>EIR (L2 Mbps)</source>
            <translation>EIR (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (Mbps)</source>
            <translation>M (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L1 Mbps)</source>
            <translation>M (C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>M (L2 Mbps)</source>
            <translation>M (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source> 2 </source>
            <translation> 2 </translation>
        </message>
        <message utf8="true">
            <source>Service 2 Configuration Results</source>
            <translation>Résultats de configuration service 2</translation>
        </message>
        <message utf8="true">
            <source> 3 </source>
            <translation> 3 </translation>
        </message>
        <message utf8="true">
            <source>Service 3 Configuration Results</source>
            <translation>Résultats de configuration service 3</translation>
        </message>
        <message utf8="true">
            <source> 4 </source>
            <translation> 4 </translation>
        </message>
        <message utf8="true">
            <source>Service 4 Configuration Results</source>
            <translation>Résultats de configuration service 4</translation>
        </message>
        <message utf8="true">
            <source> 5 </source>
            <translation> 5 </translation>
        </message>
        <message utf8="true">
            <source>Service 5 Configuration Results</source>
            <translation>Résultats de configuration service 5</translation>
        </message>
        <message utf8="true">
            <source> 6 </source>
            <translation> 6 </translation>
        </message>
        <message utf8="true">
            <source>Service 6 Configuration Results</source>
            <translation>Résultats de configuration service 6</translation>
        </message>
        <message utf8="true">
            <source> 7 </source>
            <translation> 7 </translation>
        </message>
        <message utf8="true">
            <source>Service 7 Configuration Results</source>
            <translation>Résultats de configuration service 7</translation>
        </message>
        <message utf8="true">
            <source> 8 </source>
            <translation> 8 </translation>
        </message>
        <message utf8="true">
            <source>Service 8 Configuration Results</source>
            <translation>Résultats de configuration service 8</translation>
        </message>
        <message utf8="true">
            <source> 9 </source>
            <translation> 9 </translation>
        </message>
        <message utf8="true">
            <source>Service 9 Configuration Results</source>
            <translation>Résultats de configuration service 9</translation>
        </message>
        <message utf8="true">
            <source> 10 </source>
            <translation> 10 </translation>
        </message>
        <message utf8="true">
            <source>Service 10 Configuration Results</source>
            <translation>Résultats de configuration service 10</translation>
        </message>
        <message utf8="true">
            <source>Service Performance Results</source>
            <translation>Résultats de performance service</translation>
        </message>
        <message utf8="true">
            <source>Svc. Verdict</source>
            <translation>Svc. Verdict</translation>
        </message>
        <message utf8="true">
            <source>IR Cur.</source>
            <translation>IR act.</translation>
        </message>
        <message utf8="true">
            <source>IR Max.</source>
            <translation>IR max.</translation>
        </message>
        <message utf8="true">
            <source>IR Min.</source>
            <translation>IR min.</translation>
        </message>
        <message utf8="true">
            <source>IR Avg.</source>
            <translation>IR moy.</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Seconds</source>
            <translation>Secondes de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Count</source>
            <translation>Comptage de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Delay Cur.</source>
            <translation>Délai Act.</translation>
        </message>
        <message utf8="true">
            <source>Delay Max.</source>
            <translation>Délai Max.</translation>
        </message>
        <message utf8="true">
            <source>Delay Min.</source>
            <translation>Délai Min.</translation>
        </message>
        <message utf8="true">
            <source>Delay Avg.</source>
            <translation>Délai Moy.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Cur.</source>
            <translation>Délai Var. Act.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Max.</source>
            <translation>Délai Var. Max.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Min.</source>
            <translation>Délai Var. Min.</translation>
        </message>
        <message utf8="true">
            <source>Delay Var. Avg.</source>
            <translation>Délai Var. Moy.</translation>
        </message>
        <message utf8="true">
            <source>Availability</source>
            <translation>Disponibilité</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>Disponible</translation>
        </message>
        <message utf8="true">
            <source>Available Seconds</source>
            <translation>Secondes disponibles</translation>
        </message>
        <message utf8="true">
            <source>Unavailable Seconds</source>
            <translation>Secondes indisponibles</translation>
        </message>
        <message utf8="true">
            <source>Severely Errored Seconds</source>
            <translation>Secondes sévèrement erronées </translation>
        </message>
        <message utf8="true">
            <source>Service Perf Throughput</source>
            <translation>Débit de perf service</translation>
        </message>
        <message utf8="true">
            <source>Overview</source>
            <translation>Vue Générale</translation>
        </message>
        <message utf8="true">
            <source>IR</source>
            <translation>IR</translation>
        </message>
        <message utf8="true">
            <source>Delay Variation</source>
            <translation>Variation de délai</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed service. View results on TrueSpeed result page.</source>
            <translation>Service TrueSpeed. Voir les résultats sur la page des résultats TrueSpeed.</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L1 Mbps)</source>
            <translation>IR, Moyen.&#xA;(Débit&#xA;C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Avg.&#xA;(Throughput&#xA;L2 Mbps)</source>
            <translation>IR, Moyen.&#xA;(Débit&#xA;C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio</source>
            <translation>Taux de&#xA;perte de trame</translation>
        </message>
        <message utf8="true">
            <source>One Way Delay&#xA;Avg. (ms)</source>
            <translation>Délai dans un sens&#xA;Moy. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Max. (ms)</source>
            <translation>Variable de délai&#xA;Max. (ms)</translation>
        </message>
        <message utf8="true">
            <source>Errors Detected</source>
            <translation>Les erreurs détectées</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg. (RTD, ms)</source>
            <translation>Délai&#xA;Moy. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L1 Mbps)</source>
            <translation>IR, Cur.&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Cur.&#xA;(L2 Mbps)</source>
            <translation>IR, Cur.&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L1 Mbps)</source>
            <translation>IR, Max.&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Max.&#xA;(L2 Mbps)</source>
            <translation>IR, Max.&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L1 Mbps)</source>
            <translation>IR, Min.&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>IR, Min.&#xA;(L2 Mbps)</source>
            <translation>IR, Min.&#xA;(C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Count</source>
            <translation>Comptage&#xA;de perte de trame</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss&#xA;Ratio&#xA;Threshold</source>
            <translation>Perte de trame&#xA;Ratio&#xA;Seuil</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (OWD, ms)</source>
            <translation>Délai&#xA;Act. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (OWD, ms)</source>
            <translation>Délai&#xA;Max. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min (OWD, ms)</source>
            <translation>Délai&#xA;Min (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Avg (OWD, ms)</source>
            <translation>Délai&#xA;Moy. (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(OWD, ms)</source>
            <translation>Délai&#xA;Seuil&#xA;(OWD,ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Cur (RTD, ms)</source>
            <translation>Délai&#xA;Act. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Max. (RTD, ms)</source>
            <translation>Délai&#xA;Max. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Min. (RTD, ms)</source>
            <translation>Délai&#xA;Min. (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay&#xA;Threshold&#xA;(RTD, ms)</source>
            <translation>Délai&#xA;Seuil&#xA;(RTD,ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Cur (ms)</source>
            <translation>Délai Var.&#xA;Act (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Avg. (ms)</source>
            <translation>Variable de délai&#xA;Moyenne (ms)</translation>
        </message>
        <message utf8="true">
            <source>Delay Var.&#xA;Threshold (ms)</source>
            <translation>Variable de délai&#xA;Seuil (ms)</translation>
        </message>
        <message utf8="true">
            <source>Available&#xA;Seconds&#xA;Ratio</source>
            <translation>Secondes&#xA;disponibles&#xA;ratio</translation>
        </message>
        <message utf8="true">
            <source>Unavailable&#xA;Seconds</source>
            <translation>Secondes&#xA;indisponibles</translation>
        </message>
        <message utf8="true">
            <source>Severely&#xA;Errored&#xA;Seconds</source>
            <translation>Secondes&#xA;sévèrement&#xA;erronées</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Config de service&#xA;débit &#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Config&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Débit&#xA;de config&#xA; de service (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L1 Mbps)</source>
            <translation>Perf de service&#xA;Débit&#xA;(C1 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Service Perf&#xA;Throughput&#xA;(L2 Mbps)</source>
            <translation>Débit&#xA;de perf&#xA; de service (C2 Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings</source>
            <translation>Réglage réseau</translation>
        </message>
        <message utf8="true">
            <source>User Frame Size</source>
            <translation>Taille de trame, Utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Frame Size</source>
            <translation>Taille de trame Jumbo</translation>
        </message>
        <message utf8="true">
            <source>User Packet Length</source>
            <translation>Longueur de paquet utilisateur</translation>
        </message>
        <message utf8="true">
            <source>Jumbo Packet Length</source>
            <translation>Longueur de paquet Jumbo</translation>
        </message>
        <message utf8="true">
            <source>Calc. Frame Size (Bytes)</source>
            <translation>Calc. Taille de Trame (Octets)</translation>
        </message>
        <message utf8="true">
            <source>Network Settings (Random/EMIX Size)</source>
            <translation>Paramétrages du réseau (Aléatoire/Taille EMIX)</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Remote)</source>
            <translation>Longueurs (distantes) Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths (Local)</source>
            <translation>Longueurs (locales) Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Random/EMIX Lengths</source>
            <translation>Longueurs Aléatoire/EMIX</translation>
        </message>
        <message utf8="true">
            <source>Advanced IP Settings - Remote</source>
            <translation>Paramétrages avancés IP - Distant</translation>
        </message>
        <message utf8="true">
            <source>Downstream CIR</source>
            <translation>CIR dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream EIR</source>
            <translation>EIR dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Policing</source>
            <translation>Contrôle(policing) dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream M</source>
            <translation>M dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Upstream CIR</source>
            <translation>CIR dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>Upstream EIR</source>
            <translation>EIR dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Policing</source>
            <translation>Contrôle(policing) dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>Upstream M</source>
            <translation>M dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>SLA Advanced Burst</source>
            <translation>SLA Salve avancée</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Loss Ratio</source>
            <translation>Taux de perte de trame dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (OWD, ms)</source>
            <translation>Délai de trame dans le sens descendant (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Frame Delay (RTD, ms)</source>
            <translation>Délai de trame dans le sens descendant (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Delay Variation (ms)</source>
            <translation>Variation de délai dans le sens descendant (ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Loss Ratio</source>
            <translation>Taux de perte de trame dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (OWD, ms)</source>
            <translation>Délai de trame dans le sens montant (OWD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Frame Delay (RTD, ms)</source>
            <translation>Délai de trame dans le sens montant (RTD, ms)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Delay Variation (ms)</source>
            <translation>Variation de délai dans le sens montant (ms)</translation>
        </message>
        <message utf8="true">
            <source>Include as an SLA Requirement</source>
            <translation>Inclure comme besoin SLA</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay</source>
            <translation>Inclure délai de trame</translation>
        </message>
        <message utf8="true">
            <source>Include Frame Delay Variation</source>
            <translation>Inclure Variation de délai de trame</translation>
        </message>
        <message utf8="true">
            <source>SAM-Complete has the following invalid configuration settings:</source>
            <translation>SAM-Complete a les paramètres de configuration non valides suivants :</translation>
        </message>
        <message utf8="true">
            <source>Service  Configuration Results</source>
            <translation>Résultats de configuration service</translation>
        </message>
        <message utf8="true">
            <source>Run TrueSpeed</source>
            <translation>Exécuter TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>NOTE: TrueSpeed Test will only be run if Service Performance Test is enabled.</source>
            <translation>REMARQUE : Le test TrueSpeed ne sera exécuté que si le test de performance du service est activé.</translation>
        </message>
        <message utf8="true">
            <source>Set Packet Length TTL</source>
            <translation>Définit longueur TTL de paquet</translation>
        </message>
        <message utf8="true">
            <source>Set TCP/UDP Ports</source>
            <translation>Définir ports TCP/UDP</translation>
        </message>
        <message utf8="true">
            <source>Src. Type</source>
            <translation>Type de sourc.</translation>
        </message>
        <message utf8="true">
            <source>Src. Port</source>
            <translation>Src. Port</translation>
        </message>
        <message utf8="true">
            <source>Dest. Type</source>
            <translation>Type de Dest. </translation>
        </message>
        <message utf8="true">
            <source>Dest. Port</source>
            <translation>Dest. Port</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput Threshold (%)</source>
            <translation>TCP seuil de débit (%)</translation>
        </message>
        <message utf8="true">
            <source>Recommended Total Window Size (bytes)</source>
            <translation>Taille de fenêtre totale recommandée (octets)</translation>
        </message>
        <message utf8="true">
            <source>Boosted Total Window Size (bytes)</source>
            <translation>Taille de fenêtre totale boostée (octets)</translation>
        </message>
        <message utf8="true">
            <source>Recommended # Connections</source>
            <translation>Connexions # recommandées</translation>
        </message>
        <message utf8="true">
            <source>Boosted # Connections</source>
            <translation>Connexions # boostées</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended Total Window Size (bytes)</source>
            <translation>Taill de fenêtre totale recommandée en amont (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted Total Window Size (bytes)</source>
            <translation>Flux ascendant Boosté de taille de fenêtre totale (octets)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Recommended # Connections</source>
            <translation>Connexions # recommandées en amont</translation>
        </message>
        <message utf8="true">
            <source>Upstream Boosted # Connections</source>
            <translation>Flux ascendant Boosté # connexions</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended Total Window Size (bytes)</source>
            <translation>Taill de fenêtre totale recommandée en aval (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted Total Window Size (bytes)</source>
            <translation>Flux descendant Boosté de taille de fenêtre totale (octets)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Recommended # Connections</source>
            <translation>Connexions # recommandées en aval</translation>
        </message>
        <message utf8="true">
            <source>Downstream Boosted # Connections</source>
            <translation>Flux descendant Boosté # connexions</translation>
        </message>
        <message utf8="true">
            <source>Recommended # of Connections</source>
            <translation># de connexions recommandées</translation>
        </message>
        <message utf8="true">
            <source>TCP Throughput (%)</source>
            <translation>Débit TCP (%)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed RTT</source>
            <translation>TrueSpeed RTT</translation>
        </message>
        <message utf8="true">
            <source>Acterna Payload</source>
            <translation>Payload Acterna</translation>
        </message>
        <message utf8="true">
            <source>Round-Trip Time (ms)</source>
            <translation>Durée de chute (ms)</translation>
        </message>
        <message utf8="true">
            <source>The RTT will be used in subsequent steps to make a window size and number of connections recommendation.</source>
            <translation>La RTT sera utilisé dans les étapes subséquentes pour faire une recommandation de taille de fenêtre et nombre de connexions.</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Results</source>
            <translation>Résultats de TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>TCP Transfer Metrics</source>
            <translation>Mesures du transfert TCP</translation>
        </message>
        <message utf8="true">
            <source>Target L4 (Mbps)</source>
            <translation>C4 cible (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>TrueSpeed Service Verdict</source>
            <translation>Verdict du service TrueSpeed</translation>
        </message>
        <message utf8="true">
            <source>Upstream TrueSpeed Service Verdict</source>
            <translation>Verdict du service TrueSpeed dans le sens montant</translation>
        </message>
        <message utf8="true">
            <source>Actual TCP Throughput (Mbps)</source>
            <translation>Débit TCP actuel (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Actual TCP Throughput (Mbps)</source>
            <translation>Débit TCP actuel dans le sens montant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Upstream Target TCP Throughput (Mbps)</source>
            <translation>Débit TCP cible dans le sens montant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Target TCP Throughput (Mbps))</source>
            <translation>Débit TCP cible (Mbps))</translation>
        </message>
        <message utf8="true">
            <source>Downstream TrueSpeed Service Verdict</source>
            <translation>Verdict du service TrueSpeed dans le sens descendant</translation>
        </message>
        <message utf8="true">
            <source>Downstream Actual TCP Throughput (Mbps)</source>
            <translation>Débit TCP actuel dans le sens descendant (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Downstream Target TCP Throughput (Mbps)</source>
            <translation>Débit TCP objectif dans le sens descendant (Mbps)</translation>
        </message>
    </context>
</TS>
