<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>Invalid IP Address assignment has been rejected.</source>
            <translation>無効な IP ｱﾄﾞﾚｽの割り当てが拒否されました。</translation>
        </message>
        <message utf8="true">
            <source>Invalid gateway assignment: 172.29.0.7. Restart MTS System.</source>
            <translation>無効なｹﾞｰﾄｳｪｲ割り当て:172.29.0.7. MTS ｼｽﾃﾑを再起動してください。</translation>
        </message>
        <message utf8="true">
            <source>IP address has changed.  Restart BERT Module.</source>
            <translation>IP ｱﾄﾞﾚｽが変更されました。 BERT ﾓｼﾞｭｰﾙを再起動してください。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing.  OFF request rejected.</source>
            <translation>BERT ﾓｼﾞｭｰﾙの初期化。ｵﾌの要求は拒否されました。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module initializing with optical jitter function. OFF request rejected.</source>
            <translation>光ｼﾞｯﾀ機能による BERT ﾓｼﾞｭｰﾙの初期化。ｵﾌの要求は拒否されました。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module shutting down</source>
            <translation>BERT ﾓｼﾞｭｰﾙのｼｬｯﾄﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>BERT Module OFF</source>
            <translation>BERT ﾓｼﾞｭｰﾙ OFF</translation>
        </message>
        <message utf8="true">
            <source>The BERT module is off. Press HOME/SYSTEM, then the BERT icon to activate it.</source>
            <translation>BERT ﾓｼﾞｭｰﾙがｵﾌになっています。 [ ﾎｰﾑ / ｼｽﾃﾑ ] を押して、 BERT ｱｲｺﾝをｸﾘｯｸすると起動します。</translation>
        </message>
        <message utf8="true">
            <source>BERT Module ON</source>
            <translation>BERT ﾓｼﾞｭｰﾙ ON</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING APPLICATION SOFTWARE *****</source>
            <translation>***** ｱﾌﾟﾘｹｰｼｮﾝｿﾌﾄｱｯﾌﾟｸﾞﾚｰﾄﾞ中 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE COMPLETE *****</source>
            <translation>***** ｱｯﾌﾟｸﾞﾚｰﾄﾞ完了 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING JITTER SOFTWARE *****</source>
            <translation>***** ｼﾞｯﾀｿﾌﾄｱｯﾌﾟｸﾞﾚｰﾄﾞ中 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADING KERNEL SOFTWARE *****</source>
            <translation>***** ｶｰﾈﾙｿﾌﾄｱｯﾌﾟｸﾞﾚｰﾄﾞ中 *****</translation>
        </message>
        <message utf8="true">
            <source>***** UPGRADE STARTING *****</source>
            <translation>***** ｱｯﾌﾟｸﾞﾚｰﾄﾞ開始 *****</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failed</source>
            <translation>BERT ﾓｼﾞｭｰﾙのﾕｰｻﾞｲﾝﾀｰﾌｪｰｽに障害が起きました</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI with optical jitter function</source>
            <translation>光学ｼﾞｯﾀｰﾌｧﾝｸｼｮﾝ付きで BERT ﾓｼﾞｭｰﾙのﾕｰｻﾞｲﾝﾀｰﾌｪｰｽを起動します</translation>
        </message>
        <message utf8="true">
            <source>Launching BERT Module UI</source>
            <translation>BERT ﾓｼﾞｭｰﾙのﾕｰｻﾞｲﾝﾀｰﾌｪｰｽを起動します</translation>
        </message>
        <message utf8="true">
            <source>BERT Module UI failure. Module OFF</source>
            <translation>BERT ﾓｼﾞｭｰﾙのに障害が起きました。ﾓｼﾞｭｰﾙをｵﾌにします。</translation>
        </message>
    </context>
    <context>
        <name>isumgr::CProgressScreen</name>
        <message utf8="true">
            <source>Module 0B</source>
            <translation>ﾓｼﾞｭｰﾙ 0B</translation>
        </message>
        <message utf8="true">
            <source>Module 0A</source>
            <translation>ﾓｼﾞｭｰﾙ 0A</translation>
        </message>
        <message utf8="true">
            <source>Module    0</source>
            <translation>ﾓｼﾞｭｰﾙ  0</translation>
        </message>
        <message utf8="true">
            <source>Module 1</source>
            <translation>ﾓｼﾞｭｰﾙ 1</translation>
        </message>
        <message utf8="true">
            <source>Module </source>
            <translation>ﾓｼﾞｭｰﾙ </translation>
        </message>
        <message utf8="true">
            <source>Module 1B</source>
            <translation>ﾓｼﾞｭｰﾙ 1B</translation>
        </message>
        <message utf8="true">
            <source>Module 1A</source>
            <translation>ﾓｼﾞｭｰﾙ 1A</translation>
        </message>
        <message utf8="true">
            <source>Module    1</source>
            <translation>ﾓｼﾞｭｰﾙ 1</translation>
        </message>
        <message utf8="true">
            <source>Module 2B</source>
            <translation>ﾓｼﾞｭｰﾙ 2B</translation>
        </message>
        <message utf8="true">
            <source>Module 2A</source>
            <translation>ﾓｼﾞｭｰﾙ 2A</translation>
        </message>
        <message utf8="true">
            <source>Module    2</source>
            <translation>ﾓｼﾞｭｰﾙ 2</translation>
        </message>
        <message utf8="true">
            <source>Module 3B</source>
            <translation>ﾓｼﾞｭｰﾙ 3B</translation>
        </message>
        <message utf8="true">
            <source>Module 3A</source>
            <translation>ﾓｼﾞｭｰﾙ 3A</translation>
        </message>
        <message utf8="true">
            <source>Module    3</source>
            <translation>ﾓｼﾞｭｰﾙ 3</translation>
        </message>
        <message utf8="true">
            <source>Module 4B</source>
            <translation>ﾓｼﾞｭｰﾙ 4B</translation>
        </message>
        <message utf8="true">
            <source>Module 4A</source>
            <translation>ﾓｼﾞｭｰﾙ 4A</translation>
        </message>
        <message utf8="true">
            <source>Module    4</source>
            <translation>ﾓｼﾞｭｰﾙ 4</translation>
        </message>
        <message utf8="true">
            <source>Module 5B</source>
            <translation>ﾓｼﾞｭｰﾙ 5B</translation>
        </message>
        <message utf8="true">
            <source>Module 5A</source>
            <translation>ﾓｼﾞｭｰﾙ 5A</translation>
        </message>
        <message utf8="true">
            <source>Module    5</source>
            <translation>ﾓｼﾞｭｰﾙ 5</translation>
        </message>
        <message utf8="true">
            <source>Module 6B</source>
            <translation>ﾓｼﾞｭｰﾙ 6B</translation>
        </message>
        <message utf8="true">
            <source>Module 6A</source>
            <translation>ﾓｼﾞｭｰﾙ 6A</translation>
        </message>
        <message utf8="true">
            <source>Module    6</source>
            <translation>ﾓｼﾞｭｰﾙ 6</translation>
        </message>
        <message utf8="true">
            <source>Base software upgrade required. Current revision not compatible with BERT Module.</source>
            <translation>ﾍﾞｰｽｿﾌﾄｳｪｱのｱｯﾌﾟｸﾞﾚｰﾄﾞが必要です。現在の場おｼﾞｮﾝは BERT ﾓｼﾞｭｰﾙに適合性がありません。</translation>
        </message>
        <message utf8="true">
            <source>BERT software reinstall required. The proper BERT software is not installed for attached BERT Module.</source>
            <translation>BERT ｿﾌﾄｳｪｱの再ｲﾝｽﾄｰﾙが必要です。 付属の BERT ﾓｼﾞｭｰﾙに対応する BERT ｿﾌﾄｳｪｱがｲﾝｽﾄｰﾙされていません。</translation>
        </message>
        <message utf8="true">
            <source>BERT hardware upgrade required. Current hardware is not compatible.</source>
            <translation>BERT のﾊｰﾄﾞｳｪｱをｱｯﾌﾟｸﾞﾚｰﾄﾞする必要があります。現在のﾊﾞｰｼﾞｮﾝは適合性がありません。</translation>
        </message>
    </context>
</TS>
