<!DOCTYPE TS>
<TS>
    <context>
        <name>SCRIPTS</name>
        <message utf8="true">
            <source/>
            <translation type="unfinished"/>
        </message>
        <message utf8="true">
            <source>10</source>
            <translation>10</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX FDX</source>
            <translation>1000Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>1000Base-TX HDX</source>
            <translation>1000Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX FDX</source>
            <translation>100Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>100Base-TX HDX</source>
            <translation>100Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX FDX</source>
            <translation>10Base-TX FDX</translation>
        </message>
        <message utf8="true">
            <source>10Base-TX HDX</source>
            <translation>10Base-TX HDX</translation>
        </message>
        <message utf8="true">
            <source>10 MB</source>
            <translation>10 MB</translation>
        </message>
        <message utf8="true">
            <source> {1}\:  {2} {3}&#xA;</source>
            <translation> {1}\:  {2} {3}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>{1}{2}{3}\{4}</source>
            <translation>{1}{2}{3}\{4}</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames:</source>
            <translation>{1} ﾊﾞｲﾄ ﾌﾚｰﾑ数 :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte frames</source>
            <translation>{1} ﾊﾞｲﾄ ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets:</source>
            <translation>{1} ﾊﾞｲﾄ ﾊﾟｹｯﾄ数 :</translation>
        </message>
        <message utf8="true">
            <source>{1} byte packets</source>
            <translation>{1} ﾊﾞｲﾄ ﾊﾟｹｯﾄ数</translation>
        </message>
        <message utf8="true">
            <source>{1} Error: A timeout has occured while attempting to retrieve {2}, please check your connection and try again</source>
            <translation>{1} ｴﾗｰ : {2} 回復の試行中に ﾀｲﾑｱｳﾄ が発生しました。接続を確認し、再度試してください。</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: fail</source>
            <translation>{1} ﾌﾚｰﾑ ﾊﾞｰｽﾄ : 失敗</translation>
        </message>
        <message utf8="true">
            <source>{1} frame burst: pass</source>
            <translation>{1} ﾌﾚｰﾑ ﾊﾞｰｽﾄ : 合格</translation>
        </message>
        <message utf8="true">
            <source>1 MB</source>
            <translation>1 MB</translation>
        </message>
        <message utf8="true">
            <source>{1} of {2}</source>
            <translation>{2} の {1}</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: fail</source>
            <translation>{1} ﾊﾟｹｯﾄ ﾊﾞｰｽﾄ : 失敗</translation>
        </message>
        <message utf8="true">
            <source>{1} packet burst: pass</source>
            <translation>{1} ﾊﾟｹｯﾄ ﾊﾞｰｽﾄ : 合格</translation>
        </message>
        <message utf8="true">
            <source>{1} Retrieving {2} ...</source>
            <translation>{1} {2} 回復中 ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;		   Do you want to replace it?</source>
            <translation>{1}&#xA;&#xA;		   置換しますか ?</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;&#xA;			        Hit OK to retry</source>
            <translation>{1}&#xA;&#xA;			        再試行するには OK をｸﾘｯｸします</translation>
        </message>
        <message utf8="true">
            <source>{1} Testing VLAN ID {2} for {3}...</source>
            <translation>{1} は {3} の VLAN ID {2} をﾃｽﾄ中 ...</translation>
        </message>
        <message utf8="true">
            <source>&lt; {1} us</source>
            <translation>&lt; {1} us</translation>
        </message>
        <message utf8="true">
            <source>{1} (us)</source>
            <translation>{1} μ s</translation>
        </message>
        <message utf8="true">
            <source>{1} us</source>
            <translation>{1} μ s</translation>
        </message>
        <message utf8="true">
            <source>{1} Waiting...</source>
            <translation>{1} 待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>{1}&#xA;       You may alter the name to create a new configuration.</source>
            <translation>{1}&#xA;       新しい構成を作成するために、名前を変更できます。</translation>
        </message>
        <message utf8="true">
            <source>25 MB</source>
            <translation>25 MB</translation>
        </message>
        <message utf8="true">
            <source>2 MB</source>
            <translation>2 MB</translation>
        </message>
        <message utf8="true">
            <source>50 Top Talkers (out of {1} total IP conversations)</source>
            <translation>上位 50 の送話者 ( 合計 {1} IP 通話のうち )</translation>
        </message>
        <message utf8="true">
            <source>50 Top TCP Retransmitting Conversations (out of {1} total conversations)</source>
            <translation>上位 50 の TCP 再 Tx 通話 ( 合計 {1} 通話のうち )</translation>
        </message>
        <message utf8="true">
            <source>5 MB</source>
            <translation>5 MB</translation>
        </message>
        <message utf8="true">
            <source>Abort</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Abort Test</source>
            <translation>ﾃｽﾄの中止</translation>
        </message>
        <message utf8="true">
            <source>Active</source>
            <translation>有効</translation>
        </message>
        <message utf8="true">
            <source>Active Loop</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Active loop not successful. Test Aborted for VLAN ID</source>
            <translation>ｱｸﾃｨﾌﾞ ﾙｰﾌﾟは成功しませんでした。 VLAN ID のためﾃｽﾄは中止されました</translation>
        </message>
        <message utf8="true">
            <source>Actual Test</source>
            <translation>実 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Add Range</source>
            <translation>追加の範囲</translation>
        </message>
        <message utf8="true">
            <source>A frame loss rate that exceeded the configured frame loss threshold</source>
            <translation>構成されたﾌﾚｰﾑ損失しきい値を超えるﾌﾚｰﾑ損失率</translation>
        </message>
        <message utf8="true">
            <source>After you done your manual tests or anytime you need to you can</source>
            <translation>手動ﾃｽﾄの完了後、または必要時に可能</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was found</source>
            <translation>ﾊｰﾄﾞｳｪｱ･ﾙｰﾌﾟが検出されました</translation>
        </message>
        <message utf8="true">
            <source>A hardware loop was not found</source>
            <translation>ﾊｰﾄﾞｳｪｱ ﾙｰﾌﾟが見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>All Tests</source>
            <translation>全ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>A Loopback application is not a compatible application</source>
            <translation>ﾙｰﾌﾟﾊﾞｯｸ･ｱﾌﾟﾘｹｰｼｮﾝは互換性のあるｱﾌﾟﾘｹｰｼｮﾝではありません</translation>
        </message>
        <message utf8="true">
            <source>A maximum throughput measurement is Unavailable</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄの測定はできません</translation>
        </message>
        <message utf8="true">
            <source>An active loop was not found</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟが検出できませんでした</translation>
        </message>
        <message utf8="true">
            <source>Analyze</source>
            <translation>解析</translation>
        </message>
        <message utf8="true">
            <source>Analyzing</source>
            <translation>解析中 </translation>
        </message>
        <message utf8="true">
            <source>and</source>
            <translation>および</translation>
        </message>
        <message utf8="true">
            <source>and RFC 2544 Test</source>
            <translation>RFC 2544 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR loop was found.</source>
            <translation>LBM/LBR ﾙｰﾌﾟが見つかりました。</translation>
        </message>
        <message utf8="true">
            <source>An LBM/LBR Loop was found.</source>
            <translation>LBM/LBR ﾙｰﾌﾟが見つかりました。</translation>
        </message>
        <message utf8="true">
            <source>A permanent loop was found</source>
            <translation>無限ﾙｰﾌﾟが見つかりました</translation>
        </message>
        <message utf8="true">
            <source>Append progress log to the end of the report</source>
            <translation>ﾚﾎﾟｰﾄの最後に進捗ﾛｸﾞを追加する</translation>
        </message>
        <message utf8="true">
            <source>Application Name</source>
            <translation>ｱﾌﾟﾘｹｰｼｮﾝ名</translation>
        </message>
        <message utf8="true">
            <source>Approx Total Time:</source>
            <translation>推定合計時間 :</translation>
        </message>
        <message utf8="true">
            <source>A range of theoretical FTP throughput values will be calculated based on actual measured values of the link.  Enter the measured link bandwidth, roundtrip delay, and Encapsulation.</source>
            <translation>論理 FTP ｽﾙｰﾌﾟｯﾄ 値の範囲は、ﾘﾝｸ の実測値を基に計算されます。測定したﾘﾝｸ 帯域幅，ﾗｳﾝﾄﾞﾄﾘｯﾌﾟ ﾃﾞｨﾚｲ，ｶﾌﾟｾﾙ化を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>A response timeout has occurred.&#xA;There was no response to the last command&#xA;within {1} seconds.</source>
            <translation>応答のﾀｲﾑｱｳﾄが発生しました。 &#xA; 最後のｺﾏﾝﾄﾞに対して {1} 秒以内に &#xA; 応答がありませんでした。</translation>
        </message>
        <message utf8="true">
            <source> Assuming a hard loop is in place.        </source>
            <translation> ﾊｰﾄﾞﾙｰﾌﾟが所定位置にあると仮定します。</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric</source>
            <translation>非対称</translation>
        </message>
        <message utf8="true">
            <source>Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode. Combined mode will run the test twice, sequentially transmitting in the Upstream direction using the Local Setup and then in the Downstream direction using the Remote Setup. Use the button to overwrite the remote setup with the current local setup.</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ･ﾓｰﾄﾞでの近端から遠端への非対称 Tx 、およびﾀﾞｳﾝｽﾄﾘｰﾑ･ﾓｰﾄﾞでの遠端から近端への非対称 Tx 。統合ﾓｰﾄﾞではﾃｽﾄが 2 回実行されます。最初にﾛｰｶﾙ設定を使用したｱｯﾌﾟｽﾄﾘｰﾑ方向の Tx 、次にﾘﾓｰﾄ設定を使用したﾀﾞｳﾝｽﾄﾘｰﾑ方向の Tx が行われます。ﾎﾞﾀﾝを使用して、現在のﾛｰｶﾙ設定と共にﾘﾓｰﾄ設定を上書きできます。</translation>
        </message>
        <message utf8="true">
            <source>Attempting</source>
            <translation>試行中</translation>
        </message>
        <message utf8="true">
            <source>Attempting a loop up</source>
            <translation>ﾙｰﾌﾟｱｯﾌﾟの試行中</translation>
        </message>
        <message utf8="true">
            <source>Attempting to Log-On to the server...</source>
            <translation>ｻｰﾊﾞーへのﾛｸﾞｵﾝを試行中 ...</translation>
        </message>
        <message utf8="true">
            <source>Attempts to loop up have failed. Test stopping</source>
            <translation>ﾙｰﾌﾟ ｱｯﾌﾟに失敗しました。 ﾃｽﾄ停止</translation>
        </message>
        <message utf8="true">
            <source>Attempt to loop up was unsuccessful</source>
            <translation>ﾙｰﾌﾟｱｯﾌﾟの試行が失敗しました</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation</source>
            <translation>オートネゴシエーション</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Done</source>
            <translation>ｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ 完了</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Settings</source>
            <translation>ｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ 設定</translation>
        </message>
        <message utf8="true">
            <source>Auto Negotiation Status</source>
            <translation>ｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Available</source>
            <translation>使用可能</translation>
        </message>
        <message utf8="true">
            <source>Average</source>
            <translation>平均</translation>
        </message>
        <message utf8="true">
            <source>Average Burst</source>
            <translation>平均 ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Average packet rate</source>
            <translation>平均ﾊﾟｹｯﾄ率</translation>
        </message>
        <message utf8="true">
            <source>Average packet size</source>
            <translation>平均ﾊﾟｹｯﾄ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Avg</source>
            <translation>平均</translation>
        </message>
        <message utf8="true">
            <source>Avg and Max Avg Pkt Jitter Test Results:</source>
            <translation>平均 / 最大平均 ﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD):</source>
            <translation>平均ﾚｲﾃﾝｼ (RTD): </translation>
        </message>
        <message utf8="true">
            <source>Avg Latency (RTD): N/A</source>
            <translation>平均ﾚｲﾃﾝｼ (RTD): 該当なし</translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter:</source>
            <translation>平均ﾊﾟｹｯﾄ･ｼﾞｯﾀ : </translation>
        </message>
        <message utf8="true">
            <source>Avg Packet Jitter: N/A</source>
            <translation>平均 ﾊﾟｹｯﾄ ｼﾞｯﾀ : N/A</translation>
        </message>
        <message utf8="true">
            <source>Avg Pkt Jitter (us)</source>
            <translation>平均ﾊﾟｹｯﾄ･ｼﾞｯﾀ時間 (us)</translation>
        </message>
        <message utf8="true">
            <source>Avg Rate</source>
            <translation>平均 ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Back to Back</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity:</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾌﾚｰﾑ 粒度 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frame Granularity</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾌﾚｰﾑ 粒度</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾌﾚｰﾑ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Frames Test Results:</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾌﾚｰﾑ ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time:</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ 最大試行回数 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Max Trial Time</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ 最大試行回数</translation>
        </message>
        <message utf8="true">
            <source>Back to Back Test Results:</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Back to Summary</source>
            <translation>要約に戻る</translation>
        </message>
        <message utf8="true">
            <source>$balloon::msg</source>
            <translation>$ ﾊﾞﾙｰﾝ :: ﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (%)</source>
            <translation>帯域幅 粒度 (%)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Granularity (Mbps)</source>
            <translation>帯域幅Granularity (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy:</source>
            <translation>帯域幅 測定 精度 :</translation>
        </message>
        <message utf8="true">
            <source>Bandwidth Measurement Accuracy</source>
            <translation>帯域幅 測定 精度</translation>
        </message>
        <message utf8="true">
            <source>Basic Load Test</source>
            <translation>基本負荷ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Beginning of range:</source>
            <translation>範囲の最初 :</translation>
        </message>
        <message utf8="true">
            <source>Bits</source>
            <translation>BITS</translation>
        </message>
        <message utf8="true">
            <source>Both</source>
            <translation>両方</translation>
        </message>
        <message utf8="true">
            <source>Both Rx and Tx</source>
            <translation>Both Rx and Tx</translation>
        </message>
        <message utf8="true">
            <source>Both the local and remote source IP addresses are Unavailable</source>
            <translation>ﾛｰｶﾙおよびﾘﾓｰﾄ発信元 IP ｱﾄﾞﾚｽの両方が使用できません</translation>
        </message>
        <message utf8="true">
            <source>Bottom Up</source>
            <translation>ﾎﾞﾄﾑ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Buffer</source>
            <translation>ﾊﾞｯﾌｧ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit&#xA;(requires Throughput)</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ &#xA;( ｽﾙｰﾌﾟｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration:</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ 試行 期間 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credits Trial Duration</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ 試行 期間</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Test Results:</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput {1} Bytes:</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ {1} ﾊﾞｲﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput&#xA;(requires Buffer Credit)</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ &#xA;( ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Buffer Credit Throughput Test Results:</source>
            <translation>ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Buffer Size</source>
            <translation>ﾊﾞｯﾌｧ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Burst</source>
            <translation>ﾊﾞｰｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Burst Granularity (frames)</source>
            <translation>ﾊﾞｰｽﾄ 粒度 ( ﾌﾚｰﾑ数 )</translation>
        </message>
        <message utf8="true">
            <source>BW</source>
            <translation>BW</translation>
        </message>
        <message utf8="true">
            <source>By looking at TCP retransmissions versus network utilization over time, it is possible to correlate poor network performance with lossy network conditions such as congestion.</source>
            <translation>TCP 再 Tx とﾈｯﾄﾜｰｸ使用率を時間の経過に応じて確認することで、ﾊﾟﾌｫｰﾏﾝｽの低いﾈｯﾄﾜｰｸ状態と、混雑状態で損失の多いﾈｯﾄﾜｰｸ状態を関連付けることができます。</translation>
        </message>
        <message utf8="true">
            <source>By looking at the IP Conversations table, the "Top Talkers" can be identified by either Bytes or Frames.  The nomenclature "S &lt;- D" and "S -> D" refer to source to destination and destination to source traffic direction of the bytes and frames.</source>
            <translation>IP 通話ﾃｰﾌﾞﾙを確認することで、上位送話者 をﾊﾞｲﾄまたはﾌﾚｰﾑ単位で特定できます。 S &lt;- D および S -> D という表現は、ﾊﾞｲﾄまたはﾌﾚｰﾑのﾄﾗﾌｨｯｸ方向で、それぞれ Tx 元から宛先、宛先から Tx 元を表します。</translation>
        </message>
        <message utf8="true">
            <source>(bytes)</source>
            <translation>(ﾊﾞｲﾄ)</translation>
        </message>
        <message utf8="true">
            <source>bytes</source>
            <translation>ﾊﾞｲﾄ数</translation>
        </message>
        <message utf8="true">
            <source>(Bytes)</source>
            <translation>(ﾊﾞｲﾄ)</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S &lt;- D</source>
            <translation>ﾊﾞｲﾄ &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Bytes&#xA;S -> D</source>
            <translation>ﾊﾞｲﾄ &#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Frame Length</source>
            <translation>計算 &#xA; ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Calculated&#xA;Packet Length</source>
            <translation>計算 &#xA; ﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>Calculating ...</source>
            <translation>計算中 ...</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
        <message utf8="true">
            <source>Cannot proceed!</source>
            <translation>続行できません。</translation>
        </message>
        <message utf8="true">
            <source>Capture Analysis Summary</source>
            <translation>ｷｬﾌﾟﾁｬ分析の要約</translation>
        </message>
        <message utf8="true">
            <source>Capture duration</source>
            <translation>ｷｬﾌﾟﾁｬ時間</translation>
        </message>
        <message utf8="true">
            <source>Capture&#xA;Screen</source>
            <translation>ｽｸﾘｰﾝを～～ｷｬﾌﾟﾁｬします</translation>
        </message>
        <message utf8="true">
            <source>CAUTION!&#xA;&#xA;Are you sure you want to permanently&#xA;delete this configuration?&#xA;{1}...</source>
            <translation>警告 !&#xA;&#xA; 本当にこの構成を永久に削除しても &#xA; よろしいですか ?&#xA;{1} …</translation>
        </message>
        <message utf8="true">
            <source>Cfg</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (%)</source>
            <translation>構成ﾚｰﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate</source>
            <translation>構成ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Cfg Rate (Mbps)</source>
            <translation>構成ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Chassis ID</source>
            <translation>ｼｬｰｼ ID</translation>
        </message>
        <message utf8="true">
            <source>Checked Rx item (s) will be used to configure filter source setups.</source>
            <translation>確認済み Rx 項目は、 Tx 元ﾌｨﾙﾀ設定の構成に使用されます。</translation>
        </message>
        <message utf8="true">
            <source>Checked Tx item (s) will be used to configure Tx destination setups.</source>
            <translation>確認済み Tx 項目は、 Tx 先設定の構成に使用されます。</translation>
        </message>
        <message utf8="true">
            <source>Checking Active Loop</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking for a hardware loop</source>
            <translation>ﾊｰﾄﾞｳｪｱ･ﾙｰﾌﾟの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking for an active loop</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking for an LBM/LBR loop.</source>
            <translation>LBM/LBR ﾙｰﾌﾟを確認しています。</translation>
        </message>
        <message utf8="true">
            <source>Checking for a permanent loop</source>
            <translation>無限ﾙｰﾌﾟを確認しています</translation>
        </message>
        <message utf8="true">
            <source>Checking for detection of Half Duplex ports</source>
            <translation>半二重ﾎﾟｰﾄの検出の確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking for ICMP frames</source>
            <translation>ICMP ﾌﾚｰﾑの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking for possible retransmissions or high bandwidth utilization</source>
            <translation>再 Tx または高帯域幅使用の可能性の確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking Hard Loop</source>
            <translation>ﾊｰﾄﾞ･ﾙｰﾌﾟの確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking LBM/LBR Loop</source>
            <translation>LBM/LBR ﾙｰﾌﾟを確認しています</translation>
        </message>
        <message utf8="true">
            <source>Checking Permanent Loop</source>
            <translation>無限ﾙｰﾌﾟの確認</translation>
        </message>
        <message utf8="true">
            <source>Checking protocol hierarchy statistics</source>
            <translation>ﾌﾟﾛﾄｺﾙ階層統計を確認中</translation>
        </message>
        <message utf8="true">
            <source>Checking source address availability...</source>
            <translation>Tx 元ｱﾄﾞﾚｽの有効性の確認中 ...</translation>
        </message>
        <message utf8="true">
            <source>Checking this box will cause test setups to be restored to their original settings when exiting the test. For asymmetric testing, they will be restored on both the local and remote side. Restoring setups will cause the link to be reset.</source>
            <translation>このﾁｪｯｸ･ﾎﾞｯｸｽをｵﾝにすると、ﾃｽﾄの終了時にﾃｽﾄ設定が元の設定に戻ります。非対称ﾃｽﾄの場合は、ﾛｰｶﾙとﾘﾓｰﾄの両方で設定が復元されます。設定を復元すると、ﾘﾝｸはﾘｾｯﾄされます。</translation>
        </message>
        <message utf8="true">
            <source>Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>Tx 元 IP と接続先ﾃﾞﾊﾞｲｽの間のﾎﾟｰﾄ設定をﾁｪｯｸしてください。半二重状態が存在しないことを確認してください。また、ｱﾅﾗｲｻﾞを宛先 IP に近づけることにより、さらに区分化できます。ﾘﾝｸの障害を分離するために再 Tx を排除するどうかを判別してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose a capture file to analyze</source>
            <translation>分析するｷｬﾌﾟﾁｬ･ﾌｧｲﾙの選択</translation>
        </message>
        <message utf8="true">
            <source>Choose&#xA;PCAP File</source>
            <translation>PCAP ﾌｧｲﾙの &#xA; 選択</translation>
        </message>
        <message utf8="true">
            <source>Choose the Bandwidth Measurement Accuracy you desire&#xA;( 1% is recommended for a shorter test time ).</source>
            <translation>希望する 帯域幅 測定精度を選択してください。 &#xA;( 1% は短いﾃｽﾄ時間に対してお勧めします。 )</translation>
        </message>
        <message utf8="true">
            <source>Choose the Flow Control login type</source>
            <translation>ﾌﾛｰ制御 ﾛｸﾞｲﾝ ﾀｲﾌﾟの選択</translation>
        </message>
        <message utf8="true">
            <source>Choose the Frame or Packet Size Preference</source>
            <translation>ﾌﾚｰﾑ または ﾊﾟｹｯﾄ ｻｲｽﾞの選択</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Back to Back test.</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄを実行する場合の細分性を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the granularity at which you would like to run the Frame Loss test.</source>
            <translation>ﾌﾚｰﾑ損失ﾃｽﾄを実行する粒度を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Bandwidth for which the circuit is configured.  The unit will use this number as a maximum bandwidth to transmit, reducing the length of the test:</source>
            <translation>回路を構成する最大帯域幅を選択してください。装置はﾃｽﾄの長さを減らして転送するための最大帯域幅として、この数値を使用します。 :</translation>
        </message>
        <message utf8="true">
            <source>Choose the Maximum Buffer Credit Size.&#xA; The test will start with this number, reducing the length of the test:&#xA;NOTE:  The remote device looping the traffic must be set up with &#xA;the following parameters:&#xA;&#xA;1.  Flow Control ON&#xA;2.  {1}&#xA;3.  {2} Buffer Credits set to the same value as entered above.</source>
            <translation>最大ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ ｻｲｽﾞを選択してください。 &#xA; ﾃｽﾄの長さを減らして、ﾃｽﾄはこの番号で開始します :&#xA; 注意 : ﾄﾗﾌｨｯｸをﾙｰﾌﾟするﾘﾓｰﾄ ﾃﾞﾊﾞｲｽは、以下のﾊﾟﾗﾒｰﾀで &#xA; 設定されなければなりません :&#xA;&#xA;1.  ﾌﾛｰ 制御 ON&#xA;2.  {1}&#xA;3.  {2} ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄは上で入力した同じ値が設定される</translation>
        </message>
        <message utf8="true">
            <source>Choose the maximum trial time for the Back to Back test.</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄの最大試行時間を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the minimum and maximum load values to use with the 'Top Down' or 'Bottom Up' test procedures</source>
            <translation>' ﾄｯﾌﾟ ﾀﾞｳﾝ ' や ' ﾎﾞﾄﾑ ｱｯﾌﾟ ' ﾃｽﾄ手順で使用する最少と最大の負荷値を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Back to Back test for each frame size.</source>
            <translation>各ﾌﾚｰﾑ ｻｲｽﾞに対するﾊﾞｯｸﾂｰﾊﾞｯｸ ﾃｽﾄを実行する試行回数を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Latency (RTD) test for each frame size.</source>
            <translation>各ﾌﾚｰﾑ ｻｲｽﾞに対するﾚｲﾃﾝｼ (RTD) ﾃｽﾄを実行する試行回数を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the number of trials you would like to run the Packet Jitter test for each frame size.</source>
            <translation>各ﾌﾚｰﾑ ｻｲｽﾞに対するﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄを実行する試行回数を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the Throughput Frame Loss Tolerance percentage allowed.&#xA;NOTE: A setting > 0.00 does NOT COMPLY with RFC2544</source>
            <translation>許容されたｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ損失許容割合を選択してください。 &#xA; 注意 : 0.00 未満の設定は RFC2544 に適合しません。</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Latency (RTD) trial will last.</source>
            <translation>各ﾚｲﾃﾝｼ (RTD) 試行が継続する時間を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the time each Packet Jitter trial will last.</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀーの各試行の持続時間を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the time for which a rate must be sent without error in order to pass the Throughput Test.</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄにﾊﾟｽするために必要な、一定のﾚｰﾄをｴﾗーなく送信する時間を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the time you would like each Frame Loss trial to last.</source>
            <translation>ﾌﾚｰﾑ損失の各試行の持続時間を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Choose the trial time for Buffer Credit Test</source>
            <translation>ﾊﾞﾌｧ ｸﾚｼﾞｯﾄ ﾃｽﾄの試行時間を選択してください</translation>
        </message>
        <message utf8="true">
            <source>Choose which procedure to use in the Frame Loss test.&#xA;NOTE: The RFC2544 procedure runs from the Max Bandwidth and decreases by the Bandwidth Granularity each trial, and terminates after two consecutive trials in which no frames are lost.</source>
            <translation>ﾌﾚｰﾑ損失ﾃｽﾄでどの手順を使用するか選択してください。 &#xA; 注意 : RFC2544 手順は最大帯域幅から実行し、各試行毎に帯域幅粒度で減少します。そしてﾌﾚｰﾑ損失がない 2 つの連続試行の後に終了します。</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol</source>
            <translation>ｼｽｺ検出ﾌﾟﾛﾄｺﾙ</translation>
        </message>
        <message utf8="true">
            <source>Cisco Discovery Protocol (CDP) messages were detected on this network and the table lists those MAC addresses and ports which advertised Half Duplex settings.</source>
            <translation>このﾈｯﾄﾜｰｸ上で Cisco Discovery Protocol (CDP) ﾒｯｾｰｼﾞが検出されました。半二重設定をｱﾄﾞﾊﾞﾀｲﾂﾞした MAC ｱﾄﾞﾚｽとﾎﾟｰﾄを表に示します。</translation>
        </message>
        <message utf8="true">
            <source>Clear All</source>
            <translation>全てｸﾘｱ</translation>
        </message>
        <message utf8="true">
            <source>Click on "Results" button to switch to the standard user interface.</source>
            <translation>\[ 結果 \] ﾎﾞﾀﾝをｸﾘｯｸすると、標準ﾕｰｻﾞｰ･ｲﾝﾀｰﾌｪｲｽに切り替えることができます。</translation>
        </message>
        <message utf8="true">
            <source>Close</source>
            <translation>閉じる</translation>
        </message>
        <message utf8="true">
            <source>Code Violations</source>
            <translation>ｺｰﾄﾞ 違反</translation>
        </message>
        <message utf8="true">
            <source>Combined</source>
            <translation>統合</translation>
        </message>
        <message utf8="true">
            <source> Comments</source>
            <translation>ｺﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Comments</source>
            <translation>ｺﾒﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Communication successfully established with the far end</source>
            <translation>遠隔地との通信が確立されました</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end cannot be established</source>
            <translation>遠隔地との通信が確立できません</translation>
        </message>
        <message utf8="true">
            <source>Communication with the far end has been lost</source>
            <translation>遠端との通信が切断されました</translation>
        </message>
        <message utf8="true">
            <source>complete</source>
            <translation>完了</translation>
        </message>
        <message utf8="true">
            <source>Complete</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>completed&#xA;</source>
            <translation>完了 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Configs</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>構成</translation>
        </message>
        <message utf8="true">
            <source> Configuration Name</source>
            <translation>構成名</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name:</source>
            <translation>構成名 :</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name</source>
            <translation>構成名</translation>
        </message>
        <message utf8="true">
            <source>Configuration Name Required</source>
            <translation>構成名 必要</translation>
        </message>
        <message utf8="true">
            <source>Configuration Read-Only</source>
            <translation>構成 読込専用</translation>
        </message>
        <message utf8="true">
            <source>Configuration Summary</source>
            <translation>構成 概要</translation>
        </message>
        <message utf8="true">
            <source>Configure Checked Item (s)</source>
            <translation>確認済み項目の構成</translation>
        </message>
        <message utf8="true">
            <source>Configure how long the {1} will send traffic.</source>
            <translation>{1} でﾄﾗﾌｨｯｸを送信する持続時間の構成</translation>
        </message>
        <message utf8="true">
            <source>Confirm Configuration Replacement</source>
            <translation>構成の上書き確認</translation>
        </message>
        <message utf8="true">
            <source>Confirm Deletion</source>
            <translation>削除の確認</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>接続完了</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>接続中</translation>
        </message>
        <message utf8="true">
            <source>Connect to Test Measurement Application</source>
            <translation>ﾃｽﾄ 測定 ｱﾌﾟﾘｹｰｼｮﾝの接続</translation>
        </message>
        <message utf8="true">
            <source>Continue in Half Duplex</source>
            <translation>半二重で続行</translation>
        </message>
        <message utf8="true">
            <source>Continuing in half-duplex mode</source>
            <translation>半二重ﾓｰﾄﾞで続行中</translation>
        </message>
        <message utf8="true">
            <source>Copy Local Setup&#xA;to Remote Setup</source>
            <translation>ﾛｰｶﾙ設定を &#xA; ﾘﾓｰﾄ設定にｺﾋﾟｰ</translation>
        </message>
        <message utf8="true">
            <source>Copy&#xA;Selected</source>
            <translation>選択 &#xA; のｺﾋﾟｰ</translation>
        </message>
        <message utf8="true">
            <source>Could not loop up the remote end</source>
            <translation>ﾘﾓｰﾄ･ｴﾝﾄﾞをﾙｰﾌﾟｱｯﾌﾟできませんでした</translation>
        </message>
        <message utf8="true">
            <source>Create Report</source>
            <translation>ﾚﾎﾟｰﾄの作成</translation>
        </message>
        <message utf8="true">
            <source>credits</source>
            <translation>ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>(Credits)</source>
            <translation>(ｸﾚｼﾞｯﾄ)</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Current Script: {1}</source>
            <translation>&#xA; 現在の ｽｸﾘﾌﾟﾄ : {1}</translation>
        </message>
        <message utf8="true">
            <source>Current Selection</source>
            <translation>現在 選択</translation>
        </message>
        <message utf8="true">
            <source> Customer</source>
            <translation>顧客</translation>
        </message>
        <message utf8="true">
            <source>Customer</source>
            <translation>顧客</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>顧客名</translation>
        </message>
        <message utf8="true">
            <source>Data bit rate</source>
            <translation>ﾃﾞｰﾀ ﾋﾞｯﾄ率</translation>
        </message>
        <message utf8="true">
            <source>Data byte rate</source>
            <translation>ﾃﾞｰﾀ ﾊﾞｲﾄ率</translation>
        </message>
        <message utf8="true">
            <source>Data Layer Stopped</source>
            <translation>ﾃﾞｰﾀ ﾚｲﾔｰ 停止</translation>
        </message>
        <message utf8="true">
            <source>Data Mode</source>
            <translation>ﾃﾞｰﾀﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Data Mode set to PPPoE</source>
            <translation>ﾃﾞｰﾀ･ﾓｰﾄﾞを PPPoE に設定</translation>
        </message>
        <message utf8="true">
            <source>Data size</source>
            <translation>ﾃﾞｰﾀ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source> Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>日付</translation>
        </message>
        <message utf8="true">
            <source>Date &amp; Time</source>
            <translation>日時</translation>
        </message>
        <message utf8="true">
            <source>days</source>
            <translation>日</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us):</source>
            <translation>遅延 , 現在 (us):</translation>
        </message>
        <message utf8="true">
            <source>Delay, Cur (us)</source>
            <translation>遅延 , 現在 (us)</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>削除</translation>
        </message>
        <message utf8="true">
            <source>Destination Address</source>
            <translation>Destination Address</translation>
        </message>
        <message utf8="true">
            <source>Destination Configuration</source>
            <translation>宛先 構成</translation>
        </message>
        <message utf8="true">
            <source>Destination ID</source>
            <translation>送信先 ID</translation>
        </message>
        <message utf8="true">
            <source>Destination IP</source>
            <translation>送信先 IP</translation>
        </message>
        <message utf8="true">
            <source>Destination IP&#xA;Address</source>
            <translation>宛先 &#xA;IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC for IP Address {1} was not found</source>
            <translation>IP ｱﾄﾞﾚｽ {1} の宛先 MAC が見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>Destination MAC found.</source>
            <translation>宛先 MAC が発見されました。 .</translation>
        </message>
        <message utf8="true">
            <source>Dest MAC Addr</source>
            <translation>宛先 MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Detail Label</source>
            <translation>詳細ﾗﾍﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Details</source>
            <translation>詳細</translation>
        </message>
        <message utf8="true">
            <source>detected</source>
            <translation>検出済み</translation>
        </message>
        <message utf8="true">
            <source>Detected</source>
            <translation>検出済み</translation>
        </message>
        <message utf8="true">
            <source>Detected link bandwidth</source>
            <translation>検出済みﾘﾝｸ帯域幅</translation>
        </message>
        <message utf8="true">
            <source>       Detected more frames than transmitted for {1} Bandwidth - Invalid Test.</source>
            <translation>       {1} 帯域幅に対して、送信ﾌﾚｰﾑより多くのﾌﾚｰﾑが検出されました。 - 無効なﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Determining the symmetric throughput</source>
            <translation>対称ｽﾙｰﾌﾟｯﾄの確認中</translation>
        </message>
        <message utf8="true">
            <source>Device Details</source>
            <translation>ﾃﾞﾊﾞｲｽの詳細</translation>
        </message>
        <message utf8="true">
            <source>Device ID</source>
            <translation>ﾃﾞﾊﾞｲｽ ID</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters are unavailable</source>
            <translation>DHCP ﾊﾟﾗﾒｰﾀーを使用できません</translation>
        </message>
        <message utf8="true">
            <source>DHCP parameters found.</source>
            <translation>DHCP ﾊﾟﾗﾒｰﾀ が発見されました。</translation>
        </message>
        <message utf8="true">
            <source>Discovered Devices</source>
            <translation>見つかった機器</translation>
        </message>
        <message utf8="true">
            <source>Discovering</source>
            <translation>検出中</translation>
        </message>
        <message utf8="true">
            <source>Discovering Far end loop type...</source>
            <translation>遠端のﾙｰﾌﾟ ﾀｲﾌﾟを検出中です ...</translation>
        </message>
        <message utf8="true">
            <source>Discovery&#xA;Not&#xA;Currently&#xA;Available</source>
            <translation>検出 &#xA; は &#xA; 現在 &#xA; 使用不可</translation>
        </message>
        <message utf8="true">
            <source>Display by:</source>
            <translation>表示元 :</translation>
        </message>
        <message utf8="true">
            <source>Downstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Downstream Direction</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑ方向</translation>
        </message>
        <message utf8="true">
            <source> Do you wish to proceed anyway? </source>
            <translation> 続行しますか？</translation>
        </message>
        <message utf8="true">
            <source>DSCP</source>
            <translation>DSCP</translation>
        </message>
        <message utf8="true">
            <source>Duplex</source>
            <translation>ﾃﾞｭﾌﾟﾚｸｽ</translation>
        </message>
        <message utf8="true">
            <source>Duration</source>
            <translation>期間</translation>
        </message>
        <message utf8="true">
            <source>Dur./Val.</source>
            <translation>Dur./Val.</translation>
        </message>
        <message utf8="true">
            <source>Enabled</source>
            <translation>有効</translation>
        </message>
        <message utf8="true">
            <source>Enable extended Layer 2 Traffic Test</source>
            <translation>拡張ﾚｲﾔ 2 ﾄﾗﾌｨｯｸ ﾃｽﾄを有効にする</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation:</source>
            <translation>ｶﾌﾟｾﾙ化 :</translation>
        </message>
        <message utf8="true">
            <source>Encapsulation</source>
            <translation>ｶﾌﾟｾﾙ化</translation>
        </message>
        <message utf8="true">
            <source>End</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>End Date</source>
            <translation>終了日</translation>
        </message>
        <message utf8="true">
            <source>End of range:</source>
            <translation>範囲の最後 :</translation>
        </message>
        <message utf8="true">
            <source>End time</source>
            <translation>終了時間</translation>
        </message>
        <message utf8="true">
            <source>Enter the IP address or server name that you would like to perform the FTP test with.</source>
            <translation>FTP ﾃｽﾄを実行する IP ｱﾄﾞﾚｽ または ｻｰﾊﾞ名を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Enter the Login Name for the server to which you want to connect</source>
            <translation>接続するｻｰﾊﾞのﾛｸﾞｲﾝ名を入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Enter the password to the account you want to use</source>
            <translation>使用するｱｶｳﾝﾄのﾊﾟｽﾜｰﾄﾞを入力してください。</translation>
        </message>
        <message utf8="true">
            <source>Enter your new configuration name&#xA;(Use letters, numbers, spaces, dashes and underscores only):</source>
            <translation>新しい構成名を入力してください。 &#xA;( 文字，数字，ｽﾍﾟｰｽ，句点，下線のみ使用可 ):</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Error: {1}</source>
            <translation>&#xA;Error: {1}</translation>
        </message>
        <message utf8="true">
            <source>ERROR: A response timeout has occurred&#xA;There was no response within</source>
            <translation>ｴﾗｰ : 応答のﾀｲﾑｱｳﾄが発生しました &#xA; 所定時間以内に応答がありませんでした</translation>
        </message>
        <message utf8="true">
            <source>Error: Could not establish a connection</source>
            <translation>ｴﾗｰ : 接続を確立できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Error Counts</source>
            <translation>ｴﾗー数</translation>
        </message>
        <message utf8="true">
            <source>Errored Frames</source>
            <translation>ｴﾗｰﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Error loading PCAP file</source>
            <translation>PCAP ﾌｧｲﾙ読み込み中のｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Error: Primary DNS failed name resolution.</source>
            <translation>ｴﾗｰ : ﾌﾟﾗｲﾏﾘ DNS が名前解決に失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>Error: unable to locate site</source>
            <translation>ｴﾗｰ : ｻｲﾄを見つけることができません。</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Left</source>
            <translation>残り時間の予測</translation>
        </message>
        <message utf8="true">
            <source>Estimated Time Remaining</source>
            <translation>残り時間の予測</translation>
        </message>
        <message utf8="true">
            <source>     Ethernet Test Report</source>
            <translation>     ｲｰｻﾈｯﾄ ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Event</source>
            <translation>ｲﾍﾞﾝﾄ</translation>
        </message>
        <message utf8="true">
            <source>Event log is full.</source>
            <translation>ｲﾍﾞﾝﾄ ﾛｸﾞがいっぱいです。</translation>
        </message>
        <message utf8="true">
            <source>Excessive Retransmissions Found</source>
            <translation>過度の再 Tx を検出</translation>
        </message>
        <message utf8="true">
            <source>Exit</source>
            <translation>終了</translation>
        </message>
        <message utf8="true">
            <source>Exit J-QuickCheck</source>
            <translation>J-QuickCheck の終了</translation>
        </message>
        <message utf8="true">
            <source>Expected Throughput</source>
            <translation>予想ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is</source>
            <translation>予想ｽﾙｰﾌﾟｯﾄは</translation>
        </message>
        <message utf8="true">
            <source>Expected throughput is Unavailable</source>
            <translation>予想ｽﾙｰﾌﾟｯﾄは使用できません</translation>
        </message>
        <message utf8="true">
            <source>"Expert RFC 2544 Test" button.</source>
            <translation>\[ ｴｷｽﾊﾟｰﾄ RFC 2544 ﾃｽﾄ \] ﾎﾞﾀﾝ。</translation>
        </message>
        <message utf8="true">
            <source>Explicit (E-Port)</source>
            <translation>Explicit (E-Port)</translation>
        </message>
        <message utf8="true">
            <source>Explicit (Fabric/N-Port)</source>
            <translation>Explicit ( ﾌｧﾌﾞﾘｯｸ /N ﾎﾟｰﾄ )</translation>
        </message>
        <message utf8="true">
            <source> Explicit login was unable to complete. </source>
            <translation> 明示的 ﾛｸﾞｲﾝが完了できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>FAIL</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>FAILED</source>
            <translation>失敗</translation>
        </message>
        <message utf8="true">
            <source>Far end is a JDSU Smart Class Ethernet test set</source>
            <translation>遠端は JDSU Smart Class イーサネット テスト セットです</translation>
        </message>
        <message utf8="true">
            <source>Far end is a Viavi Smart Class Ethernet test set</source>
            <translation>遠隔地の装置は Viavi Smart Class Ethernet です</translation>
        </message>
        <message utf8="true">
            <source>FC</source>
            <translation>FC</translation>
        </message>
        <message utf8="true">
            <source>FC Test</source>
            <translation>FC ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>FC test executes using Acterna Test Payload</source>
            <translation>FC ﾃｽﾄ は Acterna ﾃｽﾄ ﾍﾟｲﾛｰﾄﾞ を使って実行</translation>
        </message>
        <message utf8="true">
            <source>FC_Test_Report</source>
            <translation>FC_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>FD</source>
            <translation>FD</translation>
        </message>
        <message utf8="true">
            <source>FDX Capable</source>
            <translation>FDX 可能</translation>
        </message>
        <message utf8="true">
            <source>Fibre Channel Test Report</source>
            <translation>ﾌｧｲﾊﾞ ﾁｬﾝﾈﾙ ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>File Configuration</source>
            <translation>ﾌｧｲﾙ 構成</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>ﾌｧｲﾙ名</translation>
        </message>
        <message utf8="true">
            <source>Files</source>
            <translation>ﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>File size</source>
            <translation>ﾌｧｲﾙ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>File Size:</source>
            <translation>ﾌｧｲﾙ ｻｲｽﾞ :</translation>
        </message>
        <message utf8="true">
            <source>File Size</source>
            <translation>ﾌｧｲﾙ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>File Size: {1} MB</source>
            <translation>ﾌｧｲﾙ ｻｲｽﾞ : {1} MB</translation>
        </message>
        <message utf8="true">
            <source>File Sizes:</source>
            <translation>ﾌｧｲﾙ ｻｲｽﾞ :</translation>
        </message>
        <message utf8="true">
            <source>File Sizes</source>
            <translation>ﾌｧｲﾙ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>File transferred too quickly. Test aborted.</source>
            <translation>ﾌｧｲﾙの転送速度が速すぎます。ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Finding the expected throughput</source>
            <translation>予想ｽﾙｰﾌﾟｯﾄの検出中</translation>
        </message>
        <message utf8="true">
            <source>Finding the "Top Talkers"</source>
            <translation>上位送話者 の検索中</translation>
        </message>
        <message utf8="true">
            <source>First 50 Half Duplex Ports (out of {1} total)</source>
            <translation>先頭 50 の半二重ﾎﾟｰﾄ ( 合計 {1} のうち )</translation>
        </message>
        <message utf8="true">
            <source>First 50 ICMP Messages (out of {1} total)</source>
            <translation>先頭 50 の ICMP ﾒｯｾｰｼﾞ ( 合計 {1} のうち )</translation>
        </message>
        <message utf8="true">
            <source>Flow Control</source>
            <translation>ﾌﾛｰ制御</translation>
        </message>
        <message utf8="true">
            <source>Flow Control Login Type</source>
            <translation>ﾌﾛｰ 制御 ﾛｸﾞｲﾝ ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Folders</source>
            <translation>ﾌｫﾙﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source> for each frame is reduced to half to compensate double length of fibre.</source>
            <translation> ﾌｧｲﾊﾞの 2 倍長を補正するために半分に減らされた各ﾌﾚｰﾑに対して、</translation>
        </message>
        <message utf8="true">
            <source>found</source>
            <translation>検出</translation>
        </message>
        <message utf8="true">
            <source>Found active loop.</source>
            <translation>ｱｸﾃｨﾌﾞ ﾙｰﾌﾟが見つかりました。</translation>
        </message>
        <message utf8="true">
            <source>Found hardware loop.</source>
            <translation>ﾊｰﾄﾞｳｪｱ ﾙｰﾌﾟが見つかりました。</translation>
        </message>
        <message utf8="true">
            <source>Frame</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Frame&#xA;Length</source>
            <translation>ﾌﾚｰﾑ &#xA; 長</translation>
        </message>
        <message utf8="true">
            <source>Frame Length</source>
            <translation>ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Frame Length (Bytes)</source>
            <translation>ﾌﾚｰﾑ長 ( ﾊﾞｲﾄ数 )</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths:</source>
            <translation>ﾌﾚｰﾑ 長 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths</source>
            <translation>ﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Frame Lengths to Test</source>
            <translation>ﾃｽﾄするﾌﾚｰﾑ長</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss (%)</source>
            <translation>ﾌﾚｰﾑ 損失 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss</source>
            <translation>ﾌﾚｰﾑﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes:</source>
            <translation>ﾌﾚｰﾑ 損失 {1} ﾊﾞｲﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss {1} Bytes</source>
            <translation>ﾌﾚｰﾑ 損失 {1} ﾊﾞｲﾄ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity:</source>
            <translation>ﾌﾚｰﾑ 損失 帯域幅 粒度 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Bandwidth Granularity</source>
            <translation>ﾌﾚｰﾑ 損失 帯域幅 粒度</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Maximum Bandwidth</source>
            <translation>ﾌﾚｰﾑ 損失 最大 帯域幅</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Minimum Bandwidth</source>
            <translation>ﾌﾚｰﾑ 損失 最小 帯域幅</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Rate</source>
            <translation>ﾌﾚｰﾑ 損失 ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Ratio</source>
            <translation>ﾌﾚｰﾑﾛｽ率</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test</source>
            <translation>ﾌﾚｰﾑ ﾛｽ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure:</source>
            <translation>ﾌﾚｰﾑ 損失 ﾃｽﾄ 手順 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Procedure</source>
            <translation>ﾌﾚｰﾑ 損失 ﾃｽﾄ 手順</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Test Results:</source>
            <translation>ﾌﾚｰﾑ 損失 ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Tolerance (%)</source>
            <translation>ﾌﾚｰﾑ 損失 しきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration:</source>
            <translation>ﾌﾚｰﾑ 損失 試行 期間 :</translation>
        </message>
        <message utf8="true">
            <source>Frame Loss Trial Duration</source>
            <translation>ﾌﾚｰﾑ 損失 試行 期間</translation>
        </message>
        <message utf8="true">
            <source>Frame or Packet</source>
            <translation>ﾌﾚｰﾑ または ﾊﾟｹｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>frames</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Frames</source>
            <translation>ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>frame size</source>
            <translation>ﾌﾚｰﾑ･ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Frame Size</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Frame Size:  {1} bytes</source>
            <translation>ﾌﾚｰﾑ ｻｲｽﾞ :  {1} ( ﾊﾞｲﾄ数 )</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S &lt;- D</source>
            <translation>ﾌﾚｰﾑ &#xA;S &lt;- D</translation>
        </message>
        <message utf8="true">
            <source>Frames&#xA;S -> D</source>
            <translation>ﾌﾚｰﾑ &#xA;S -> D</translation>
        </message>
        <message utf8="true">
            <source>Framing</source>
            <translation>Framing</translation>
        </message>
        <message utf8="true">
            <source>(frms)</source>
            <translation>(ﾌﾚｰﾑ数)</translation>
        </message>
        <message utf8="true">
            <source>(frms/sec)</source>
            <translation>(ﾌﾚｰﾑ数 / 秒)</translation>
        </message>
        <message utf8="true">
            <source>FTP_TEST_REPORT</source>
            <translation>FTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput</source>
            <translation>FTP Throughput</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test&#xA;</source>
            <translation>FTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ &#xA;</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Complete!</source>
            <translation>FTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>FTP Throughput Test Report</source>
            <translation>FTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Full</source>
            <translation>ﾌﾙ</translation>
        </message>
        <message utf8="true">
            <source>GET</source>
            <translation>GET</translation>
        </message>
        <message utf8="true">
            <source>Get PCAP Info</source>
            <translation>PCAP 情報の取得</translation>
        </message>
        <message utf8="true">
            <source>Half</source>
            <translation>ﾊｰﾌ</translation>
        </message>
        <message utf8="true">
            <source>Half Duplex Ports</source>
            <translation>半二重ﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Hardware</source>
            <translation>ﾊｰﾄﾞｳｪｱ</translation>
        </message>
        <message utf8="true">
            <source>Hardware Loop</source>
            <translation>ﾊｰﾄﾞｳｪｱ･ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>(Hardware&#xA;or Active)</source>
            <translation>( ﾊｰﾄﾞｳｪｱ &#xA; またはｱｸﾃｨﾌﾞ )</translation>
        </message>
        <message utf8="true">
            <source>(Hardware,&#xA;Permanent&#xA;or Active)</source>
            <translation>( ﾊｰﾄﾞｳｪｱ、 &#xA; 無限 &#xA; またはｱｸﾃｨﾌﾞ )</translation>
        </message>
        <message utf8="true">
            <source>HD</source>
            <translation>HD</translation>
        </message>
        <message utf8="true">
            <source>HDX Capable</source>
            <translation>HDX 可能</translation>
        </message>
        <message utf8="true">
            <source>High utilization</source>
            <translation>高使用率</translation>
        </message>
        <message utf8="true">
            <source>Home</source>
            <translation>ﾎｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>hours</source>
            <translation>時間</translation>
        </message>
        <message utf8="true">
            <source>HTTP_TEST_REPORT</source>
            <translation>HTTP_TEST_REPORT</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test</source>
            <translation>HTTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>HTTP Throughput Test Report</source>
            <translation>HTTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>HW</source>
            <translation>HW</translation>
        </message>
        <message utf8="true">
            <source>ICMP&#xA;Code</source>
            <translation>ICMP&#xA; ｺｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>ICMP Messages</source>
            <translation>ICMP ﾒｯｾｰｼﾞ数</translation>
        </message>
        <message utf8="true">
            <source>If the error counters are incrementing in a sporadic manner run the manual</source>
            <translation>ｴﾗｰ･ｶｳﾝﾀが散発的に増加している場合は、手動で実行してください</translation>
        </message>
        <message utf8="true">
            <source>If the problem persists please 'Reset Test to Defaults' from the Tools menu.</source>
            <translation>問題が継続する場合 Tool ﾒﾆｭーから Reset Test to Defaults （ﾃｽﾄの初期設定）を実行してください</translation>
        </message>
        <message utf8="true">
            <source>If you cannot solve the problem with the sporadic errors you can set</source>
            <translation>散発的なｴﾗーの問題を解決できない場合は、以下を設定できます</translation>
        </message>
        <message utf8="true">
            <source>Implicit (Transparent Link)</source>
            <translation>暗黙的 ( ﾄﾗﾝｽﾍﾟｱﾚﾝﾄ ﾘﾝｸ )</translation>
        </message>
        <message utf8="true">
            <source>Information</source>
            <translation>情報</translation>
        </message>
        <message utf8="true">
            <source>Initializing communication with</source>
            <translation>通信を初期化しています</translation>
        </message>
        <message utf8="true">
            <source>In order to determine the bandwidth at which the</source>
            <translation>帯域幅を決定するには</translation>
        </message>
        <message utf8="true">
            <source>Input rate for local and remote side do not match</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄ側の入力ﾚｰﾄが一致しません</translation>
        </message>
        <message utf8="true">
            <source>Intermittent problems are being seen on the line.</source>
            <translation>ﾗｲﾝで断続的な問題が発生しています。</translation>
        </message>
        <message utf8="true">
            <source>Internal Error</source>
            <translation>内部ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>Internal Error - Restart PPPoE</source>
            <translation>内部ｴﾗｰ - 再ｽﾀｰﾄ PPPoE</translation>
        </message>
        <message utf8="true">
            <source>Invalid Config</source>
            <translation>不適切な設定</translation>
        </message>
        <message utf8="true">
            <source>Invalid IP Configuration</source>
            <translation>無効な IP 設定</translation>
        </message>
        <message utf8="true">
            <source>IP Address</source>
            <translation>IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>IP Addresses</source>
            <translation>IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>IP Conversations</source>
            <translation>IP 通話</translation>
        </message>
        <message utf8="true">
            <source>is exiting</source>
            <translation>存在する</translation>
        </message>
        <message utf8="true">
            <source>is starting</source>
            <translation>開始中</translation>
        </message>
        <message utf8="true">
            <source>J-Connect</source>
            <translation>J-Connect</translation>
        </message>
        <message utf8="true">
            <source>Jitter Test</source>
            <translation>ｼﾞｯﾀ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck</source>
            <translation>J-QuickCheck</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck is complete</source>
            <translation>J-QuickCheck が完了</translation>
        </message>
        <message utf8="true">
            <source>J-QuickCheck lost link or was not able to establish link</source>
            <translation>J-QuickCheck でﾘﾝｸが失われたか、ﾘﾝｸを確立できませんでした</translation>
        </message>
        <message utf8="true">
            <source>kbps</source>
            <translation>kbps</translation>
        </message>
        <message utf8="true">
            <source>kbytes</source>
            <translation>kB</translation>
        </message>
        <message utf8="true">
            <source>Kill</source>
            <translation>強制終了</translation>
        </message>
        <message utf8="true">
            <source>L1</source>
            <translation>L1</translation>
        </message>
        <message utf8="true">
            <source>L2</source>
            <translation>L2</translation>
        </message>
        <message utf8="true">
            <source>L2 Traffic test can be relaunched by running J-QuickCheck again.</source>
            <translation>J-QuickCheck を再度実行すれば、 L2 ﾄﾗﾌｨｯｸ ﾃｽﾄを再開できます。</translation>
        </message>
        <message utf8="true">
            <source>Latency</source>
            <translation>ﾚｲﾃﾝｼ</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)</source>
            <translation>ﾚｲﾃﾝｼ (RTD)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) and Packet Jitter Tests</source>
            <translation>待機時間 (RTD) とﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Avg: N/A</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 平均 : 該当なし</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold:</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 合格 しきい値 :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Pass Threshold</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 合格 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD)&#xA;(requires Throughput)</source>
            <translation>ﾚｲﾃﾝｼ (RTD)&#xA;( ｽﾙｰﾌﾟｯﾄ ﾃｽﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Results</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 結果</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results:</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: ABORTED   </source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄ 結果 : 中止   </translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: FAIL</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄ 結果 : 失敗</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results: PASS</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄ 結果 : 合格</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test Results skipped</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄ結果を省略</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Test skipped</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄを省略</translation>
        </message>
        <message utf8="true">
            <source> Latency (RTD) Threshold: {1} us</source>
            <translation> ﾚｲﾃﾝｼ (RTD) しきい値 : {1} us</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Threshold (us)</source>
            <translation>ﾚｲﾃﾝｼ (RTD) しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration:</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 試行 期間 :</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) Trial Duration</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 試行 期間</translation>
        </message>
        <message utf8="true">
            <source>Latency (RTD) (us)</source>
            <translation>ﾚｲﾃﾝｼ (RTD) (us)</translation>
        </message>
        <message utf8="true">
            <source>Layer 1</source>
            <translation>ﾚｲﾔ 1</translation>
        </message>
        <message utf8="true">
            <source>Layer 1 / 2&#xA;Ethernet Health</source>
            <translation>ﾚｲﾔ 1/2&#xA; ｲｰｻﾈｯﾄの状態</translation>
        </message>
        <message utf8="true">
            <source>Layer 2</source>
            <translation>ﾚｲﾔ 2</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Link Present Found</source>
            <translation>ﾚｲﾔ 2 ﾘﾝｸ あり 検出</translation>
        </message>
        <message utf8="true">
            <source>Layer 2 Quick Test</source>
            <translation>ﾚｲﾔ 2 ｸｲｯｸ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Layer 3</source>
            <translation>ﾚｲﾔ 3</translation>
        </message>
        <message utf8="true">
            <source>Layer 3&#xA;IP Health</source>
            <translation>ﾚｲﾔ 3&#xA;IP の状態</translation>
        </message>
        <message utf8="true">
            <source>Layer 4</source>
            <translation>ﾚｲﾔ 4</translation>
        </message>
        <message utf8="true">
            <source>Layer 4&#xA;TCP Health</source>
            <translation>ﾚｲﾔ 4&#xA;TCP の状態</translation>
        </message>
        <message utf8="true">
            <source>LBM</source>
            <translation>LBM</translation>
        </message>
        <message utf8="true">
            <source> LBM/LBR</source>
            <translation> LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR</source>
            <translation>LBM/LBR</translation>
        </message>
        <message utf8="true">
            <source>LBM/LBR Loop</source>
            <translation>LBM/LBR ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Length</source>
            <translation>長さ</translation>
        </message>
        <message utf8="true">
            <source>Link Found</source>
            <translation>ﾘﾝｸ 検出</translation>
        </message>
        <message utf8="true">
            <source>Link Layer Discovery Protocol</source>
            <translation>ﾘﾝｸ ﾚｲﾔ検出ﾌﾟﾛﾄｺﾙ</translation>
        </message>
        <message utf8="true">
            <source>Link Lost</source>
            <translation>ﾘﾝｸの損失</translation>
        </message>
        <message utf8="true">
            <source>Link speed detected in capture file</source>
            <translation>ｷｬﾌﾟﾁｬ･ﾌｧｲﾙで検出されたﾘﾝｸ速度</translation>
        </message>
        <message utf8="true">
            <source>Listen Port</source>
            <translation>ﾘｽﾝﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Load Format</source>
            <translation>負荷 ﾌｫｰﾏｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>LOADING ... Please Wait</source>
            <translation>読み込み中 ... お待ちください</translation>
        </message>
        <message utf8="true">
            <source>Local</source>
            <translation>ﾛｰｶﾙ</translation>
        </message>
        <message utf8="true">
            <source>Local destination IP address is configured to</source>
            <translation>ﾛｰｶﾙ宛先 IP ｱﾄﾞﾚｽの構成内容</translation>
        </message>
        <message utf8="true">
            <source>Local destination MAC address is configured to</source>
            <translation>ﾛｰｶﾙ宛先 MAC ｱﾄﾞﾚｽの構成内容</translation>
        </message>
        <message utf8="true">
            <source>Local destination port is configured to</source>
            <translation>ﾛｰｶﾙ宛先ﾎﾟｰﾄの構成内容</translation>
        </message>
        <message utf8="true">
            <source>Local loop type is configured to Unicast</source>
            <translation>ﾛｰｶﾙ･ﾙｰﾌﾟ･ﾀｲﾌﾟはﾕﾆｷｬｽﾄに構成済み</translation>
        </message>
        <message utf8="true">
            <source>Local Port</source>
            <translation>ﾛｰｶﾙﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Local remote IP address is configured to</source>
            <translation>ﾛｰｶﾙ･ﾘﾓｰﾄ IP ｱﾄﾞﾚｽの構成内容</translation>
        </message>
        <message utf8="true">
            <source> Local Serial Number</source>
            <translation> ﾛｰｶﾙのｼﾘｱﾙ番号</translation>
        </message>
        <message utf8="true">
            <source>Local Setup</source>
            <translation>ﾛｰｶﾙ設定</translation>
        </message>
        <message utf8="true">
            <source> Local Software Revision</source>
            <translation> ﾛｰｶﾙのｿﾌﾄｳｪｱ･ﾘﾋﾞｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Local source IP filter is configured to</source>
            <translation>ﾛｰｶﾙ Tx 元 IP ﾌｨﾙﾀの構成内容</translation>
        </message>
        <message utf8="true">
            <source>Local source MAC filter is configured to</source>
            <translation>ﾛｰｶﾙ Tx 元 MAC ﾌｨﾙﾀの構成内容</translation>
        </message>
        <message utf8="true">
            <source>Local source port filter is configured to</source>
            <translation>ﾛｰｶﾙ Tx 元ﾎﾟｰﾄ･ﾌｨﾙﾀの構成内容</translation>
        </message>
        <message utf8="true">
            <source>Local Summary</source>
            <translation>ﾛｰｶﾙの要約</translation>
        </message>
        <message utf8="true">
            <source> Local Test Instrument Name</source>
            <translation> ﾛｰｶﾙのﾃｽﾄ機器名</translation>
        </message>
        <message utf8="true">
            <source>Locate the device with the source MAC address(es) and port(s) listed in the table and ensure that duplex settings are set to "full" and not "auto".  It is not uncommon for a host to be set as "auto" and network device to be set as "auto", and the link incorrectly negotiates to half-duplex.</source>
            <translation>表に示された Tx 元 MAC ｱﾄﾞﾚｽおよびﾎﾟｰﾄを持つﾃﾞﾊﾞｲｽを見つけて、二重設定が 全 であり、自動 に設定されていないことを確認します。自動 として設定されているﾎｽﾄおよびﾈｯﾄﾜｰｸ･ﾃﾞﾊﾞｲｽは多く存在し、ﾘﾝｸが誤って半二重にﾈｺﾞｼｴｰﾄされます。</translation>
        </message>
        <message utf8="true">
            <source> Location</source>
            <translation>ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Location</source>
            <translation>ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Login:</source>
            <translation>ﾛｸﾞｲﾝ :</translation>
        </message>
        <message utf8="true">
            <source>Login</source>
            <translation>Login</translation>
        </message>
        <message utf8="true">
            <source>Login Name:</source>
            <translation>ﾛｸﾞｲﾝ名 :</translation>
        </message>
        <message utf8="true">
            <source>Login Name</source>
            <translation>ﾛｸﾞｲﾝ名</translation>
        </message>
        <message utf8="true">
            <source>Loop Failed</source>
            <translation>ﾙｰﾌﾟ失敗</translation>
        </message>
        <message utf8="true">
            <source>Looping Down far end unit...</source>
            <translation>遠端 装置のﾙｰﾌﾟ ﾀﾞｳﾝ中 ...</translation>
        </message>
        <message utf8="true">
            <source>Looping up far end unit...</source>
            <translation>遠端 装置のﾙｰﾌﾟ ｱｯﾌﾟ中 ...</translation>
        </message>
        <message utf8="true">
            <source>Loop Status Unknown</source>
            <translation>不明ﾙｰﾌﾟ･ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Loop up failed</source>
            <translation>ﾙｰﾌﾟｱｯﾌﾟ失敗</translation>
        </message>
        <message utf8="true">
            <source>Loop up succeeded</source>
            <translation>ﾙｰﾌﾟｱｯﾌﾟ成功</translation>
        </message>
        <message utf8="true">
            <source>Loop Up Successful</source>
            <translation>ﾙｰﾌﾟ ｱｯﾌﾟ 成功</translation>
        </message>
        <message utf8="true">
            <source>Loss of Layer 2 Link was detected!</source>
            <translation>ﾚｲﾔｰ 2 ﾘﾝｸの損失が検出されました !</translation>
        </message>
        <message utf8="true">
            <source>Lost</source>
            <translation>ﾛｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Lost Frames</source>
            <translation>ﾛｽﾄﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>MAC Address</source>
            <translation>MAC ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Management Address</source>
            <translation>管理ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>MAU Type</source>
            <translation>MAU ﾀｲﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Max</source>
            <translation>Max</translation>
        </message>
        <message utf8="true">
            <source>MAX</source>
            <translation>MAX</translation>
        </message>
        <message utf8="true">
            <source>( max {1} characters )</source>
            <translation>( 最大 {1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Max Avg</source>
            <translation>最大 平均</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter:</source>
            <translation>最大平均ﾊﾟｹｯﾄ･ｼﾞｯﾀ : </translation>
        </message>
        <message utf8="true">
            <source>Max Avg Packet Jitter: N/A</source>
            <translation>最大 平均 ﾊﾟｹｯﾄ ｼﾞｯﾀ : N/A</translation>
        </message>
        <message utf8="true">
            <source>Max Avg Pkt Jitter (us)</source>
            <translation>最大　平均ﾊﾟｹｯﾄ･ｼﾞｯﾀ (us)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (%)</source>
            <translation>最大 帯域幅 (%)</translation>
        </message>
        <message utf8="true">
            <source>Max Bandwidth (Mbps)</source>
            <translation>最大 帯域幅 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Max Buffer Size</source>
            <translation>最大 ﾊﾞｯﾌｧ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Maximum Latency, Avg allowed to "Pass" for the Latency (RTD) Test</source>
            <translation>最大ﾚｲﾃﾝｼ、平均はﾚｲﾃﾝｼ (RTD) ﾃｽﾄに 合格 可能</translation>
        </message>
        <message utf8="true">
            <source>Maximum Packet Jitter, Avg allowed to "Pass" for the Packet Jitter Test</source>
            <translation>最大ﾊﾟｹｯﾄ･ｼﾞｯﾀ、平均はﾊﾟｹｯﾄ･ｼﾞｯﾀ･ﾃｽﾄに 合格 可能</translation>
        </message>
        <message utf8="true">
            <source>Maximum RX Buffer Credits</source>
            <translation>最大 RX ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth:</source>
            <translation>最大 ﾃｽﾄ 帯域幅 :</translation>
        </message>
        <message utf8="true">
            <source>Maximum Test Bandwidth</source>
            <translation>最大 ﾃｽﾄ 帯域幅</translation>
        </message>
        <message utf8="true">
            <source>Maximum throughput measured:</source>
            <translation>測定された最大ｽﾙｰﾌﾟｯﾄ : </translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of {1} per VLAN ID</source>
            <translation>最大時間制限 {1}/VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Maximum time limit of 7 days per VLAN ID</source>
            <translation>最大時間制限 7 日 /VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Maximum Trial Time (seconds)</source>
            <translation>最大 試行時間 (s)</translation>
        </message>
        <message utf8="true">
            <source>Maximum TX Buffer Credits</source>
            <translation>最大 TX ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Max Rate</source>
            <translation>最大 ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Max retransmit attempts reached. Test aborted.</source>
            <translation>再送信の試行回数が上限に到達しました。ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>MB</source>
            <translation>MB</translation>
        </message>
        <message utf8="true">
            <source>(Mbps)</source>
            <translation>(Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Mbps</source>
            <translation>Mbps</translation>
        </message>
        <message utf8="true">
            <source>Measured</source>
            <translation>測定</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (%)</source>
            <translation>測定 ﾚｰﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate</source>
            <translation>測定 ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Measured Rate (Mbps)</source>
            <translation>測定 ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Measurement Accuracy</source>
            <translation>測定精度</translation>
        </message>
        <message utf8="true">
            <source>Measuring Throughput at {1} Buffer Credits</source>
            <translation>{1} ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄでｽﾙｰﾌﾟｯﾄの測定中</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>ﾒｯｾｰｼﾞ</translation>
        </message>
        <message utf8="true">
            <source>Min</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Minimum</source>
            <translation>最小</translation>
        </message>
        <message utf8="true">
            <source>Minimum  Percent Bandwidth</source>
            <translation>最小 ﾊﾟｰｾﾝﾄ 帯域幅</translation>
        </message>
        <message utf8="true">
            <source>Minimum Percent Bandwidth required to "Pass" for the Throughput Test:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ･ﾃｽﾄに 合格 するために必要な最小帯域幅率 : </translation>
        </message>
        <message utf8="true">
            <source>Minimum time limit of 5 seconds per VLAN ID</source>
            <translation>最大時間制限 5 秒 /VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>Min Rate</source>
            <translation>最小 ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>mins</source>
            <translation>分</translation>
        </message>
        <message utf8="true">
            <source>minute(s)</source>
            <translation>分</translation>
        </message>
        <message utf8="true">
            <source>minutes</source>
            <translation>分</translation>
        </message>
        <message utf8="true">
            <source>Model</source>
            <translation>ﾓﾃﾞﾙ</translation>
        </message>
        <message utf8="true">
            <source>Modify</source>
            <translation>修正</translation>
        </message>
        <message utf8="true">
            <source>MPLS/VPLS Encapsulation not currently supported ...</source>
            <translation>MPLS/VPLS ｶﾌﾟｾﾙ化は現在ｻﾎﾟｰﾄされていません ...</translation>
        </message>
        <message utf8="true">
            <source>N/A</source>
            <translation>N/A</translation>
        </message>
        <message utf8="true">
            <source>N/A (hard loop)</source>
            <translation>N/A ( ﾊｰﾄﾞ ﾙｰﾌﾟ )</translation>
        </message>
        <message utf8="true">
            <source>Neither</source>
            <translation>両方なし</translation>
        </message>
        <message utf8="true">
            <source>Network Up</source>
            <translation>ﾈｯﾄﾜｰｸ ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Network Utilization</source>
            <translation>ﾈｯﾄﾜｰｸ使用率</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this chart provides a network utilization graph</source>
            <translation>過度のﾈｯﾄﾜｰｸ使用率は検出されませんでしたが、このﾁｬｰﾄにﾈｯﾄﾜｰｸ使用率ｸﾞﾗﾌを示します</translation>
        </message>
        <message utf8="true">
            <source>Network utilization was not detected to be excessive, but this table provides an IP top talkers listing</source>
            <translation>過度のﾈｯﾄﾜｰｸ使用率は検出されませんでしたが、この表に IP 上位送話者の一覧を示します</translation>
        </message>
        <message utf8="true">
            <source>New</source>
            <translation>新規</translation>
        </message>
        <message utf8="true">
            <source>New Configuration Name</source>
            <translation>新規 構成名</translation>
        </message>
        <message utf8="true">
            <source>New URL</source>
            <translation>新規 URL</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>次</translation>
        </message>
        <message utf8="true">
            <source>No.</source>
            <translation>いいえ.</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>いいえ</translation>
        </message>
        <message utf8="true">
            <source>No compatible application found</source>
            <translation>同様なｱﾌﾟﾘｹｰｼｮﾝが見つかりません</translation>
        </message>
        <message utf8="true">
            <source>&lt;NO CONFIGURATION AVAILABLE></source>
            <translation>&lt; 有効な構成がありません ></translation>
        </message>
        <message utf8="true">
            <source>No files have been selected to test</source>
            <translation>ﾃｽﾄのためにﾌｧｲﾙが選択されていません</translation>
        </message>
        <message utf8="true">
            <source>No hardware loop was found</source>
            <translation>ﾊｰﾄﾞｳｪｱ ﾙｰﾌﾟは見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;JDSU&#xA;Devices&#xA;Discovered</source>
            <translation>JDSU&#xA;デバイスが&#xA;見つかりません</translation>
        </message>
        <message utf8="true">
            <source>No Layer 2 Link detected!</source>
            <translation>ﾚｲﾔｰ 2 ﾘﾝｸが検出されませんでした !</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established</source>
            <translation>ﾙｰﾌﾟを確立できませんでした</translation>
        </message>
        <message utf8="true">
            <source>No loop could be established or found</source>
            <translation>ﾙｰﾌﾟは確立できなかったか、見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>なし</translation>
        </message>
        <message utf8="true">
            <source>No permanent loop was found</source>
            <translation>無限ﾙｰﾌﾟは見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>No running application detected</source>
            <translation>実行中のｱﾌﾟﾘｹｰｼｮﾝの未検出</translation>
        </message>
        <message utf8="true">
            <source>NOT COMPLY with RFC2544</source>
            <translation>RFC2544 に不適合</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>接続されていません</translation>
        </message>
        <message utf8="true">
            <source>Not Determined</source>
            <translation>未確定</translation>
        </message>
        <message utf8="true">
            <source>NOTE:  A setting > 0.00 does</source>
            <translation>注意 :  設定が > 0.00 です。</translation>
        </message>
        <message utf8="true">
            <source>Note: Assumes a hard-loop with Buffer Credits less than 2.</source>
            <translation>注意 : ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄを持つﾊｰﾄﾞ ﾙｰﾌﾟは 2 未満であると仮定します。</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Note: Assumes a hard-loop with Buffer credits less than 2.&#xA; This test is invalid.&#xA;</source>
            <translation>&#xA; 注 : ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄが 2 未満のﾊｰﾄﾞ ﾙｰﾌﾟを想定しています。 &#xA; このﾃｽﾄは無効です。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, minimum buffer credits calculated</source>
            <translation>注意 : ﾊｰﾄﾞ ﾙｰﾌﾟの仮定に基づき、最小 ﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄが算出されました。</translation>
        </message>
        <message utf8="true">
            <source>Note: Based on the hard loop assumption, throughput measurements are made at twice</source>
            <translation>注意 : ﾊｰﾄﾞ ﾙｰﾌﾟの仮定に基づき、ｽﾙｰﾌﾟｯﾄ測定は 2 回行われます。</translation>
        </message>
        <message utf8="true">
            <source>Note: Once you use a Frame Loss Tolerance the test does not comply</source>
            <translation>ﾒﾓ : ﾌﾚｰﾑ損失許容度を使用すると、ﾃｽﾄは準拠しません</translation>
        </message>
        <message utf8="true">
            <source>Notes</source>
            <translation>注意</translation>
        </message>
        <message utf8="true">
            <source>Not Selected</source>
            <translation>未選択</translation>
        </message>
        <message utf8="true">
            <source>No&#xA;Viavi&#xA;Devices&#xA;Discovered</source>
            <translation>Viavi&#xA; ﾃﾞﾊﾞｲｽが &#xA; 検出 &#xA; されませんでした</translation>
        </message>
        <message utf8="true">
            <source>Now exiting...</source>
            <translation>終了中 ...</translation>
        </message>
        <message utf8="true">
            <source>Now verifying</source>
            <translation>現在 検証中</translation>
        </message>
        <message utf8="true">
            <source>Now verifying with {1} credits.  This will take {2} seconds.</source>
            <translation>現在 {1} ｸﾚｼﾞｯﾄで検証中。これには {2} 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials:</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ 試行数 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Back to Back Trials</source>
            <translation>ﾊﾞｯｸﾂｰﾊﾞｯｸ 試行数</translation>
        </message>
        <message utf8="true">
            <source>Number of Failures</source>
            <translation>失敗数</translation>
        </message>
        <message utf8="true">
            <source>Number of IDs tested</source>
            <translation>ﾃｽﾄした ID 数</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials:</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 試行数 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Latency (RTD) Trials</source>
            <translation>ﾚｲﾃﾝｼ (RTD) 試行数</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials:</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 試行数 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Packet Jitter Trials</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ試行数</translation>
        </message>
        <message utf8="true">
            <source>Number of packets</source>
            <translation>ﾊﾟｹｯﾄの数</translation>
        </message>
        <message utf8="true">
            <source>Number of Successes</source>
            <translation>成功数</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials:</source>
            <translation>試行数 :</translation>
        </message>
        <message utf8="true">
            <source>Number of Trials</source>
            <translation>ﾄﾗｲｱﾙ回数</translation>
        </message>
        <message utf8="true">
            <source>of</source>
            <translation>の</translation>
        </message>
        <message utf8="true">
            <source>Off</source>
            <translation>ｵﾌ</translation>
        </message>
        <message utf8="true">
            <source>OFF</source>
            <translation>OFF</translation>
        </message>
        <message utf8="true">
            <source>of frames were lost within one second.</source>
            <translation>1 秒以内に失われたﾌﾚｰﾑの</translation>
        </message>
        <message utf8="true">
            <source>of J-QuickCheck expected throughput</source>
            <translation>J-QuickCheck 予測ｽﾙｰﾌﾟｯﾄのうち</translation>
        </message>
        <message utf8="true">
            <source>(% of Line Rate)</source>
            <translation>(ﾗｲﾝﾚｰﾄの比率)</translation>
        </message>
        <message utf8="true">
            <source>% of Line Rate</source>
            <translation>ﾗｲﾝﾚｰﾄの比率 (%)</translation>
        </message>
        <message utf8="true">
            <source>of Line Rate</source>
            <translation>ﾗｲﾝﾚｰﾄの比率</translation>
        </message>
        <message utf8="true">
            <source>Ok</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>On</source>
            <translation>ｵﾝ</translation>
        </message>
        <message utf8="true">
            <source>ON</source>
            <translation>ON</translation>
        </message>
        <message utf8="true">
            <source> * Only {1} Trial(s) yielded usable data *</source>
            <translation> * {1} 試験のみが有効なﾃﾞｰﾀを生成します *</translation>
        </message>
        <message utf8="true">
            <source>(ON or OFF)</source>
            <translation>( ｵﾝまたはｵﾌ )</translation>
        </message>
        <message utf8="true">
            <source>OoS Frames</source>
            <translation>OoS ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Originator ID</source>
            <translation>Originator ID</translation>
        </message>
        <message utf8="true">
            <source>Out of Range</source>
            <translation>範囲外</translation>
        </message>
        <message utf8="true">
            <source>        Overall Test Result: {1}        </source>
            <translation>        全体ﾃｽﾄ結果 : {1}        </translation>
        </message>
        <message utf8="true">
            <source>    Overall Test Result: ABORTED   </source>
            <translation>    全体ﾃｽﾄ結果 : 中止  </translation>
        </message>
        <message utf8="true">
            <source>Over Range</source>
            <translation>範囲外</translation>
        </message>
        <message utf8="true">
            <source>over the last 10 seconds even though traffic should be stopped</source>
            <translation>ﾄﾗﾌｨｯｸを停止する必要があっても、直前の 10 秒間にわたって、</translation>
        </message>
        <message utf8="true">
            <source>Packet</source>
            <translation>ﾊﾟｹｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter, Avg</source>
            <translation>Packet Jitter, Avg</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold:</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 合格 しきい値 :</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Pass Threshold</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 合格 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter&#xA;(requires Throughput)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ &#xA;( ｽﾙｰﾌﾟｯﾄ ﾃｽﾄが必要 )</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Results</source>
            <translation>ﾊﾟｹｯﾄ･ｼﾞｯﾀ結果</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: ABORTED   </source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ 結果 : 中止   </translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: FAIL</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ 結果 : 失敗</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Test Results: PASS</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ ﾃｽﾄ 結果 : 合格</translation>
        </message>
        <message utf8="true">
            <source> Packet Jitter Threshold: {1} us</source>
            <translation> ﾊﾟｹｯﾄ ｼﾞｯﾀ しきい値 : {1} us</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Threshold (us)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ しきい値 (us)</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration:</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 試行 期間 :</translation>
        </message>
        <message utf8="true">
            <source>Packet Jitter Trial Duration</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀ 試行 期間</translation>
        </message>
        <message utf8="true">
            <source>Packet&#xA;Length</source>
            <translation>ﾊﾟｹｯﾄ &#xA; 長</translation>
        </message>
        <message utf8="true">
            <source>Packet Length (Bytes)</source>
            <translation>ﾊﾟｹｯﾄ 長 ( ﾊﾞｲﾄ )</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths:</source>
            <translation>ﾊﾟｹｯﾄ 長 :</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths</source>
            <translation>ﾊﾟｹｯﾄ 長</translation>
        </message>
        <message utf8="true">
            <source>Packet Lengths to Test</source>
            <translation>ﾃｽﾄするﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>packet size</source>
            <translation>ﾊﾟｹｯﾄ･ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Packet Size</source>
            <translation>ﾊﾟｹｯﾄｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>PASS</source>
            <translation>合格</translation>
        </message>
        <message utf8="true">
            <source>Pass / Fail</source>
            <translation>合格 / 失格</translation>
        </message>
        <message utf8="true">
            <source>PASS/FAIL</source>
            <translation>合格 / 失敗</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (%)</source>
            <translation>合格ﾚｰﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (frm/sec)</source>
            <translation>合格ﾚｰﾄ ( ﾌﾚｰﾑ / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (Mbps)</source>
            <translation>合格ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Pass Rate (pkts/sec)</source>
            <translation>通過ﾚｰﾄ ( ﾊﾟｹｯﾄ数 / 秒 )</translation>
        </message>
        <message utf8="true">
            <source>Password:</source>
            <translation>ﾊﾟｽﾜｰﾄﾞ :</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>ﾊﾟｽﾜｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Pause</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Pause Advrt</source>
            <translation>ﾎﾟｰｽﾞ Advrt</translation>
        </message>
        <message utf8="true">
            <source>Pause Capable</source>
            <translation>ﾎﾟｰｽﾞ可能</translation>
        </message>
        <message utf8="true">
            <source>Pause Det</source>
            <translation>ﾎﾟｰﾂﾞ検出</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected</source>
            <translation>ﾎﾟｰｽﾞ ﾌﾚｰﾑの検出</translation>
        </message>
        <message utf8="true">
            <source>Pause Frame(s) detected - Invalid Test</source>
            <translation>ﾎﾟｰｽﾞ ﾌﾚｰﾑの検出 - 無効な ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>PCAP</source>
            <translation>PCAP</translation>
        </message>
        <message utf8="true">
            <source>PCAP file parsing error</source>
            <translation>PCAP ﾌｧｲﾙ解析ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>PCAP Files</source>
            <translation>PCAP ﾌｧｲﾙ</translation>
        </message>
        <message utf8="true">
            <source>Pending</source>
            <translation>保留</translation>
        </message>
        <message utf8="true">
            <source>Performing cleanup</source>
            <translation>ｸﾘｰﾝ ｱｯﾌﾟの実行中</translation>
        </message>
        <message utf8="true">
            <source>Permanent</source>
            <translation>無限</translation>
        </message>
        <message utf8="true">
            <source>Permanent Loop</source>
            <translation>無限ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>(Permanent&#xA;or Active)</source>
            <translation>( 無限 &#xA; またはｱｸﾃｨﾌﾞ )</translation>
        </message>
        <message utf8="true">
            <source>Pkt</source>
            <translation>ﾊﾟｹｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Pkt Jitter (us)</source>
            <translation>ﾊﾟｹｯﾄ ｼﾞｯﾀｰ ( μ s)</translation>
        </message>
        <message utf8="true">
            <source>Pkt Length</source>
            <translation> ﾊﾟｹｯﾄ長</translation>
        </message>
        <message utf8="true">
            <source>Pkt Loss</source>
            <translation>ﾊﾟｹｯﾄﾛｽ</translation>
        </message>
        <message utf8="true">
            <source>(pkts)</source>
            <translation>(ﾊﾟｹｯﾄ数)</translation>
        </message>
        <message utf8="true">
            <source>Pkts</source>
            <translation>Pkts</translation>
        </message>
        <message utf8="true">
            <source>(pkts/sec)</source>
            <translation>(ﾊﾟｹｯﾄ数 / 秒)</translation>
        </message>
        <message utf8="true">
            <source>Platform</source>
            <translation>ﾌﾟﾗｯﾄﾌｫｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Please check that you have sync and link,</source>
            <translation>同期およびﾘﾝｸしていることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Please check to see that you are properly connected,</source>
            <translation>適切に接続されていることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Please check to see you are properly connected,</source>
            <translation>適切に接続されていることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Please choose another configuration name.</source>
            <translation>他の構成名を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Please enter a File Name to save the report ( max %{1} characters ) </source>
            <translation>ﾚﾎﾟｰﾄを保存するにはﾌｧｲﾙ名を入力してください ( 最大 %{1} 文字 ) </translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )</source>
            <translation>ｺﾒﾝﾄを入力してください。 ( 最大 {1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max %{1} characters )</source>
            <translation>ｺﾒﾝﾄを入力してください。 最大 %{1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter any Comments you have ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>ｺﾒﾝﾄを入力してください (最大 {1} 文字)&#xA;(使用できるのは文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみです)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )</source>
            <translation>顧客名を入力してください。 最大 %{1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max %{1} characters )</source>
            <translation>顧客名を入力してください。 最大 %{1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name&#xA;( max {1} characters ) &#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>顧客名を入力してください&#xA;(最大 {1} 文字)&#xA;(使用できるのは文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみです)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>顧客名を入力してください (最大 {1} 文字)&#xA;(使用できるのは文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみです)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Customer's Name( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>顧客名を入力してください (最大 {1} 文字)&#xA;(使用できるのは文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみです)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )</source>
            <translation>ﾃｸﾆｼｬﾝ名を入力してください。 ( 最大 {1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max %{1} characters )</source>
            <translation>ﾃｸﾆｼｬﾝ名を入力してください。 最大 %{1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Technician Name ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>技術者の名前を入力してください (最大 {1} 文字)&#xA;(使用できるのは文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみです)</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝを入力してください。 ( 最大 {1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max %{1} characters )</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝを入力してください。 最大 %{1} 文字 )</translation>
        </message>
        <message utf8="true">
            <source>Please enter your Test Location ( max {1} characters )&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>ﾃｽﾄの位置を入力してください (最大 {1} 文字)&#xA;(使用できるのは文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみです)</translation>
        </message>
        <message utf8="true">
            <source>Please press the "Connect to Remote" button</source>
            <translation>\[ ﾘﾓｰﾄに接続 \] ﾎﾞﾀﾝを押してください</translation>
        </message>
        <message utf8="true">
            <source>Please verify the performance of the link with a manual traffic test.</source>
            <translation>手動ﾄﾗﾌｨｯｸ･ﾃｽﾄでﾘﾝｸ･ﾊﾟﾌｫｰﾏﾝｽを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local and remote source IP addresses and try again</source>
            <translation>ﾛｰｶﾙおよびﾘﾓｰﾄ発信元 IP ｱﾄﾞﾚｽを確認し、再度、試行してください</translation>
        </message>
        <message utf8="true">
            <source>Please verify your local source IP address and try again</source>
            <translation>ﾛｰｶﾙ発信元 IP ｱﾄﾞﾚｽを確認し、再度、試行してください</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote IP address and try again.</source>
            <translation>ﾘﾓｰﾄ IP ｱﾄﾞﾚｽを確認し再実行してください</translation>
        </message>
        <message utf8="true">
            <source>Please verify your remote source IP address and try again</source>
            <translation>ﾘﾓｰﾄ発信元 IP ｱﾄﾞﾚｽを確認し、再度、試行してください</translation>
        </message>
        <message utf8="true">
            <source>Please wait ...</source>
            <translation>お待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Please Wait...</source>
            <translation>お待ちください ...</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.</source>
            <translation>PDF ﾌｧｲﾙを書込み中です。お待ちください。</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the PDF file is written.&#xA;This may take up to 90 seconds ...</source>
            <translation>PDF ﾌｧｲﾙを書込み中です。お待ちください。 &#xA; これには最大 90 秒かかります ...</translation>
        </message>
        <message utf8="true">
            <source>Port:</source>
            <translation>ﾎﾟｰﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Port</source>
            <translation>Port</translation>
        </message>
        <message utf8="true">
            <source>Port {1}: Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>ﾎﾟｰﾄ {1} : ﾃｽﾄ･ﾚﾎﾟｰﾄを保存しますか ?&#xA;&#xA;\[ はい \] または \[ いいえ \] を押してください。</translation>
        </message>
        <message utf8="true">
            <source>Port ID</source>
            <translation>ﾎﾟｰﾄ ID</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}&#xA;</source>
            <translation>ﾎﾟｰﾄ :				{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Port:				{1}</source>
            <translation>ﾎﾟｰﾄ :				{1}</translation>
        </message>
        <message utf8="true">
            <source>PPP Active</source>
            <translation>PPP 有効</translation>
        </message>
        <message utf8="true">
            <source>PPP Authentication Failed</source>
            <translation>PPP 認証失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP Failed Unknown</source>
            <translation>PPP 失敗 原因不明</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP</source>
            <translation>PPP IPCP</translation>
        </message>
        <message utf8="true">
            <source>PPP IPCP Failed</source>
            <translation>PPP IPCP 失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP LCP Failed</source>
            <translation>PPP LCP 失敗</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Active</source>
            <translation>PPPoE 有効</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Failed</source>
            <translation>PPPoE 失敗</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Inactive</source>
            <translation>PPPoE 無効</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Started</source>
            <translation>PPPoE 開始</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Status: </source>
            <translation>PPPoE ｽﾃｰﾀｽ : </translation>
        </message>
        <message utf8="true">
            <source>PPPoE Timeout</source>
            <translation>PPPoE ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>PPPoE Up</source>
            <translation>PPPoE ｱｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>PPPPoE Failed</source>
            <translation>PPPPoE が失敗しました</translation>
        </message>
        <message utf8="true">
            <source>PPP Timeout</source>
            <translation>PPP ﾀｲﾑｱｳﾄ</translation>
        </message>
        <message utf8="true">
            <source>PPP Unknown Failed</source>
            <translation>PPP 不明失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP Up Failed</source>
            <translation>PPP UP 失敗</translation>
        </message>
        <message utf8="true">
            <source>PPP UP Failed</source>
            <translation>PPP UP 失敗</translation>
        </message>
        <message utf8="true">
            <source>Press "Close" to return to main screen.</source>
            <translation>\[ 閉じる \] を押すと、ﾒｲﾝ画面に戻ります。</translation>
        </message>
        <message utf8="true">
            <source>Press "Exit" to return to main screen or "Run Test" to run again.</source>
            <translation>\[ 終了 \] を押すとﾒｲﾝ画面に戻り、 \[ ﾃｽﾄの実行 \] を押すと再実行できます。</translation>
        </message>
        <message utf8="true">
            <source>Press&#xA;Refresh&#xA;Button&#xA;to&#xA;Discover</source>
            <translation>\[ 更新 \]&#xA; を &#xA; 押して &#xA; 検出 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Press the "Exit J-QuickCheck" button to exit J-QuickCheck</source>
            <translation>\[J-QuickCheck の終了 \] ﾎﾞﾀﾝを押すと、 J-QuickCheck を終了できます</translation>
        </message>
        <message utf8="true">
            <source>Press the Refresh button below to discover Viavi devices currently on the subnet. Select a device to see details in the table to the right. If Refresh is not available check to make sure that Discovery is enabled and that you have sync and link.</source>
            <translation>下の [ 更新 ] ﾎﾞﾀﾝを押すと、現在ｻﾌﾞﾈｯﾄにある Viavi ﾃﾞﾊﾞｲｽを検出できます。詳細を確認するには、右の表でﾃﾞﾊﾞｲｽを選択します。 [ 更新 ] が使用できない場合は、検出が有効になっていること、また同期およびﾘﾝｸしていることを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>Press the "Run J-QuickCheck" button&#xA;to verify local and remote test setup and available bandwidth</source>
            <translation>\[J-QuickCheck の実行 \] ﾎﾞﾀﾝを押すと &#xA; ﾛｰｶﾙとﾘﾓｰﾄのﾃｽﾄ設定、および使用可能な帯域幅を確認できます</translation>
        </message>
        <message utf8="true">
            <source>Prev</source>
            <translation>前</translation>
        </message>
        <message utf8="true">
            <source>Progress</source>
            <translation>進行状況</translation>
        </message>
        <message utf8="true">
            <source>Property</source>
            <translation>ﾌﾟﾛﾊﾟﾃｨ</translation>
        </message>
        <message utf8="true">
            <source>Proposed Next Steps</source>
            <translation>Proposed Next Steps</translation>
        </message>
        <message utf8="true">
            <source>Provider</source>
            <translation>ﾌﾟﾛﾊﾞｲﾀﾞ</translation>
        </message>
        <message utf8="true">
            <source>PUT</source>
            <translation>PUT</translation>
        </message>
        <message utf8="true">
            <source>Q-in-Q</source>
            <translation>Q-in-Q</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>中止</translation>
        </message>
        <message utf8="true">
            <source>Random</source>
            <translation>ﾗﾝﾀﾞﾑ</translation>
        </message>
        <message utf8="true">
            <source>> Range</source>
            <translation>> 範囲</translation>
        </message>
        <message utf8="true">
            <source>Range</source>
            <translation>範囲</translation>
        </message>
        <message utf8="true">
            <source>Rate</source>
            <translation>ﾚｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Rate (Mbps)</source>
            <translation>ﾚｰﾄ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>rates the link may have general problems not related to maximum load.</source>
            <translation>最大負荷に関連しない一般的な問題があると思われるﾘﾝｸの割合</translation>
        </message>
        <message utf8="true">
            <source>Received {1} bytes from {2}</source>
            <translation>{1} ﾊﾞｲﾄ を {2} から受信</translation>
        </message>
        <message utf8="true">
            <source>Received Frames</source>
            <translation>受信ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Recommendation</source>
            <translation>推奨</translation>
        </message>
        <message utf8="true">
            <source>Recommended manual test configuration:</source>
            <translation>手動ﾃｽﾄ推奨構成 : </translation>
        </message>
        <message utf8="true">
            <source>Refresh</source>
            <translation>更新</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>ﾘﾓｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>Remote IP Address</source>
            <translation>ﾘﾓｰﾄ IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Remote Loop</source>
            <translation>ﾘﾓｰﾄ･ﾙｰﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source> Remote Serial Number</source>
            <translation> ﾘﾓｰﾄのｼﾘｱﾙ番号</translation>
        </message>
        <message utf8="true">
            <source>Remote Serial Number</source>
            <translation> ﾘﾓｰﾄのｼﾘｱﾙ番号</translation>
        </message>
        <message utf8="true">
            <source>Remote Setup</source>
            <translation>ﾘﾓｰﾄ設定</translation>
        </message>
        <message utf8="true">
            <source>Remote setups could not be restored</source>
            <translation>ﾘﾓｰﾄ設定を復元できませんでした</translation>
        </message>
        <message utf8="true">
            <source> Remote Software Revision</source>
            <translation> ﾘﾓｰﾄのｿﾌﾄｳｪｱ･ﾘﾋﾞｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Remote Software Version</source>
            <translation>ﾘﾓｰﾄのｿﾌﾄｳｪｱ･ﾊﾞｰｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Remote Summary</source>
            <translation>ﾘﾓｰﾄの要約</translation>
        </message>
        <message utf8="true">
            <source> Remote Test Instrument Name</source>
            <translation> ﾘﾓｰﾄのﾃｽﾄ機器名</translation>
        </message>
        <message utf8="true">
            <source>Remote Test Instrument Name</source>
            <translation> ﾘﾓｰﾄのﾃｽﾄ機器名</translation>
        </message>
        <message utf8="true">
            <source>Remove Range</source>
            <translation>範囲の削除</translation>
        </message>
        <message utf8="true">
            <source>Responder ID</source>
            <translation>Responder ID</translation>
        </message>
        <message utf8="true">
            <source>Responding&#xA;Router IP</source>
            <translation>応答 &#xA; ﾙｰﾀｰ IP</translation>
        </message>
        <message utf8="true">
            <source>Restart J-QuickCheck</source>
            <translation>J-QuickCheck の再開</translation>
        </message>
        <message utf8="true">
            <source>Restore pre-test configurations before exiting</source>
            <translation>終了する前に、ﾃｽﾄ前の構成を復元する</translation>
        </message>
        <message utf8="true">
            <source>Restoring remote test set settings ...</source>
            <translation>ﾘﾓｰﾄ･ﾃｽﾄ･ｾｯﾄ設定の復元中 ...</translation>
        </message>
        <message utf8="true">
            <source>Restrict RFC to</source>
            <translation>RFC の制限</translation>
        </message>
        <message utf8="true">
            <source>Result of the Basic Load Test is Unavailable, please click "Proposed Next Steps" for possible solutions</source>
            <translation>基本負荷ﾃｽﾄの結果が使用不可能です。考えられる解決策については、 Proposed Next Steps ( 推奨される次のｽﾃｯﾌﾟ ) をｸﾘｯｸしてください</translation>
        </message>
        <message utf8="true">
            <source>Results</source>
            <translation>結果</translation>
        </message>
        <message utf8="true">
            <source>Results to monitor:</source>
            <translation>監視する結果 : </translation>
        </message>
        <message utf8="true">
            <source>Retransmissions</source>
            <translation>再 Tx </translation>
        </message>
        <message utf8="true">
            <source>Retransmissions were found&#xA;Analyzing retransmission occurences over time</source>
            <translation>再 Tx が検出されました &#xA; 再 Tx の経時的な発生を分析中</translation>
        </message>
        <message utf8="true">
            <source>retransmissions were found. Please export the file to USB for further analysis.</source>
            <translation>再 Tx が検出されました。詳細な分析を行うには、ﾌｧｲﾙを USB にｴｸｽﾎﾟｰﾄしてください。</translation>
        </message>
        <message utf8="true">
            <source>Retrieval of {1} was aborted by the user</source>
            <translation>{1} の回復はﾕｰｻﾞに中止されました。</translation>
        </message>
        <message utf8="true">
            <source>return to the RFC 2544 user interface by clicking on the </source>
            <translation>RFC 2544 ﾕｰｻﾞｰ･ｲﾝﾀｰﾌｪｲｽに戻るには、次のﾎﾞﾀﾝをｸﾘｯｸしてください :  </translation>
        </message>
        <message utf8="true">
            <source>RFC 2544</source>
            <translation>RFC 2544</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Ethernet Test Report</source>
            <translation>RFC 2544 ｲｰｻﾈｯﾄ ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source> RFC 2544 Mode</source>
            <translation>RFC 2544 ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Mode</source>
            <translation>RFC 2544 ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Standard</source>
            <translation>RFC 2544 の標準</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 Test</source>
            <translation>RFC 2544 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>RFC 2544 test executes using Acterna Test Payload</source>
            <translation>RFC 2544 ﾃｽﾄ Acterna ﾃｽﾄ ﾍﾟｲﾛｰﾄﾞを使用して実行</translation>
        </message>
        <message utf8="true">
            <source>RFC2544_Test_Report</source>
            <translation>RFC2544_Test_Report</translation>
        </message>
        <message utf8="true">
            <source>RFC/FC Test cannot be run while Multistreams Graphical Results is running</source>
            <translation>ﾏﾙﾁｽﾄﾘｰﾑ ｸﾞﾗﾌｨｶﾙ 結果が実行中は、 RFC/FC ﾃｽﾄは実行できません。</translation>
        </message>
        <message utf8="true">
            <source>Rfc Mode</source>
            <translation>RFC 2544 ﾓｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>R_RDY</source>
            <translation>R_RDY</translation>
        </message>
        <message utf8="true">
            <source>R_RDY Det</source>
            <translation>R_RDY 検出</translation>
        </message>
        <message utf8="true">
            <source>Run</source>
            <translation>実行</translation>
        </message>
        <message utf8="true">
            <source>Run FC Test</source>
            <translation>FC ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Run J-QuickCheck</source>
            <translation>J-QuickCheck の実行</translation>
        </message>
        <message utf8="true">
            <source>Run $l2quick::testLongName</source>
            <translation>$l2quick::testLongName の実行</translation>
        </message>
        <message utf8="true">
            <source>Running</source>
            <translation>実行中</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.</source>
            <translation>{1}{2} 負荷でﾃｽﾄ実行中です。</translation>
        </message>
        <message utf8="true">
            <source>Running test at {1}{2} load.  This will take {3} seconds.</source>
            <translation>{1}{2} 負荷でﾃｽﾄ実行中です。これには {3} 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Run RFC 2544 Test</source>
            <translation>RFC 2544 ﾃｽﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Run Script</source>
            <translation>ｽｸﾘﾌﾟﾄの実行</translation>
        </message>
        <message utf8="true">
            <source>Rx Mbps, Cur L1</source>
            <translation>Rx Mbps, 現在 L1</translation>
        </message>
        <message utf8="true">
            <source>Rx Only</source>
            <translation>Rx のみ</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>保存</translation>
        </message>
        <message utf8="true">
            <source>Save FTP Throughput Test Report</source>
            <translation>FTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ﾚﾎﾟｰﾄの保存</translation>
        </message>
        <message utf8="true">
            <source>Save HTTP Throughput Test Report</source>
            <translation>HTTP ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ ﾚﾎﾟｰﾄの保存</translation>
        </message>
        <message utf8="true">
            <source>Save VLAN Scan Test Report</source>
            <translation>VLAN ｽｷｬﾝﾃｽﾄﾚﾎﾟｰﾄの保存</translation>
        </message>
        <message utf8="true">
            <source> scaled bandwidth</source>
            <translation> ｽｹｰﾘﾝｸﾞされた帯域幅</translation>
        </message>
        <message utf8="true">
            <source>Screenshot captured</source>
            <translation>ｷｬﾌﾟﾁｬ済みｽｸﾘｰﾝｼｮｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Script aborted.</source>
            <translation>ｽｸﾘﾌﾟﾄ 中止 .</translation>
        </message>
        <message utf8="true">
            <source>seconds</source>
            <translation>s</translation>
        </message>
        <message utf8="true">
            <source>(secs)</source>
            <translation>(s)</translation>
        </message>
        <message utf8="true">
            <source>secs</source>
            <translation>秒数</translation>
        </message>
        <message utf8="true">
            <source>&lt;Select></source>
            <translation>&lt;Select></translation>
        </message>
        <message utf8="true">
            <source>Select a name for the copied configuration</source>
            <translation>ｺﾋﾟｰした構成の名前を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Select a name for the new configuration</source>
            <translation>新規構成の名前を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Select a range of VLAN IDs</source>
            <translation>VLAN ID の範囲を選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction:</source>
            <translation>選択された Tx 方向 : </translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Downstream</source>
            <translation>選択された Tx 方向 &#xA; ﾀﾞｳﾝｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Selected Tx Direction&#xA;Upstream</source>
            <translation>選択された Tx 方向 &#xA; ｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Selection Warning</source>
            <translation>選択 警告</translation>
        </message>
        <message utf8="true">
            <source>Select "OK" to modify the configuration&#xA;Edit the name to create a new configuration&#xA;(Use letters, numbers, spaces, dashes and underscores only)</source>
            <translation>\[OK\] を選択して構成を変更します &#xA; 名前を編集して新しい構成を作成します &#xA;( 文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみ使用 )</translation>
        </message>
        <message utf8="true">
            <source>Select Test Configuration:</source>
            <translation>ﾃｽﾄ構成の選択 :</translation>
        </message>
        <message utf8="true">
            <source>Select the property by which you wish to see the discovered devices listed.</source>
            <translation>一覧の検出済みﾃﾞﾊﾞｲｽの表示基準とするﾌﾟﾛﾊﾟﾃｨを選択します。</translation>
        </message>
        <message utf8="true">
            <source>Select the tests you would like to run:</source>
            <translation>実行するﾃｽﾄの選択 :</translation>
        </message>
        <message utf8="true">
            <source>Select URL</source>
            <translation>URL の選択</translation>
        </message>
        <message utf8="true">
            <source>Select which format to use for load related setups.</source>
            <translation>関連する設定の読み込みに使用するﾌｫｰﾏｯﾄを選択してください。</translation>
        </message>
        <message utf8="true">
            <source>Sending ARP request for destination MAC.</source>
            <translation>宛先 MAC へ ARP ﾘｸｴｽﾄを送信中</translation>
        </message>
        <message utf8="true">
            <source>Sending traffic for</source>
            <translation>ﾄﾗﾌｨｯｸの送信先</translation>
        </message>
        <message utf8="true">
            <source>sends traffic, the expected throughput discovered by J-QuickCheck will by scaled by this value.</source>
            <translation>ﾄﾗﾌｨｯｸを送信します。 J-QuickCheck によって検出された予測ｽﾙｰﾌﾟｯﾄはこの値によってｽｹｰﾘﾝｸﾞされます。</translation>
        </message>
        <message utf8="true">
            <source>Sequence ID</source>
            <translation>Sequence ID</translation>
        </message>
        <message utf8="true">
            <source> Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>Serial Number</source>
            <translation>ｼﾘｱﾙ 番号</translation>
        </message>
        <message utf8="true">
            <source>Server ID:</source>
            <translation>ｻｰﾊﾞ ID:</translation>
        </message>
        <message utf8="true">
            <source>Server ID</source>
            <translation>ｻｰﾊﾞ ID</translation>
        </message>
        <message utf8="true">
            <source>Service Name</source>
            <translation>ｻｰﾋﾞｽ名</translation>
        </message>
        <message utf8="true">
            <source>Setup</source>
            <translation>設定</translation>
        </message>
        <message utf8="true">
            <source>Show Pass/Fail status</source>
            <translation>合格 / 失敗 ｽﾃｰﾀｽの表示</translation>
        </message>
        <message utf8="true">
            <source>&#xA;Show Pass/Fail status for:</source>
            <translation>&#xA; 合格 / 失敗のｽﾃｰﾀｽを示します :</translation>
        </message>
        <message utf8="true">
            <source>Size</source>
            <translation>ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>Skip</source>
            <translation>ｽｷｯﾌﾟ</translation>
        </message>
        <message utf8="true">
            <source>Skipping the Latency (RTD) test and continuing</source>
            <translation>ﾚｲﾃﾝｼ (RTD) ﾃｽﾄを省略して続行中</translation>
        </message>
        <message utf8="true">
            <source>Software Rev</source>
            <translation>ｿﾌﾄｳｪｱ･ﾚﾋﾞｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source> Software Revision</source>
            <translation> ｿﾌﾄｳｪｱ ﾘﾋﾞｼﾞｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Source Address</source>
            <translation>Source Address</translation>
        </message>
        <message utf8="true">
            <source>Source address is not available</source>
            <translation>ｿｰｽ ｱﾄﾞﾚｽを使用できません</translation>
        </message>
        <message utf8="true">
            <source>Source availability established...</source>
            <translation>Tx 元の有効性が確認されました ...</translation>
        </message>
        <message utf8="true">
            <source>Source ID</source>
            <translation>送信元 ID</translation>
        </message>
        <message utf8="true">
            <source>Source IP</source>
            <translation>送信元 IP</translation>
        </message>
        <message utf8="true">
            <source>Source IP&#xA;Address</source>
            <translation>送信元 &#xA;IP ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Source IP Address is the same as the Destination Address</source>
            <translation>送信元 IP ｱﾄﾞﾚｽは宛先ｱﾄﾞﾚｽと同じです｡</translation>
        </message>
        <message utf8="true">
            <source>Source MAC</source>
            <translation>送信元 MAC</translation>
        </message>
        <message utf8="true">
            <source>Source MAC&#xA;Address</source>
            <translation>Tx 元 MAC&#xA; ｱﾄﾞﾚｽ</translation>
        </message>
        <message utf8="true">
            <source>Specify the link bandwidth</source>
            <translation>ﾘﾝｸ帯域幅の指定</translation>
        </message>
        <message utf8="true">
            <source>Speed</source>
            <translation>ｽﾋﾟｰﾄﾞ</translation>
        </message>
        <message utf8="true">
            <source>Speed (Mbps)</source>
            <translation>ｽﾋﾟｰﾄﾞ (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Start</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>START</source>
            <translation>開始</translation>
        </message>
        <message utf8="true">
            <source>Start Date</source>
            <translation>開始日</translation>
        </message>
        <message utf8="true">
            <source>Starting Basic Load test</source>
            <translation>基本負荷ﾃｽﾄの開始中</translation>
        </message>
        <message utf8="true">
            <source>Starting Trial</source>
            <translation>試行の開始中</translation>
        </message>
        <message utf8="true">
            <source>Start time</source>
            <translation>ｽﾀｰﾄﾀｲﾑ</translation>
        </message>
        <message utf8="true">
            <source>Status Unknown</source>
            <translation>不明ｽﾃｰﾀｽ</translation>
        </message>
        <message utf8="true">
            <source>Stop</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>STOP</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Study the graph to determine if the TCP retransmissions align with degraded network utilization.  Look at the TCP Retransmissions tab to determine the Source IP that is causing significant TCP retransmissions. Check the port settings between the Source IP and the device it is connected to; verify that the half duplex condition does not exist.  Further sectionalization can also be achieved by moving the analyzer closer to the Destination IP; determine if retransmissions are eliminated to isolate the faulty link(s).</source>
            <translation>ｸﾞﾗﾌを調べ、ﾈｯﾄﾜｰｸ使用率の低下に応じて TCP 再 Tx が調整されているかどうかを判別します。重大な TCP 再 Tx の原因となっている Tx 元 IP を判別するには、 \[TCP 再 Tx  \] ﾀﾌﾞを確認します。 Tx 元 IP と接続先ﾃﾞﾊﾞｲｽの間のﾎﾟｰﾄ設定をﾁｪｯｸしてください。半二重状態が存在しないことを確認してください。また、ｱﾅﾗｲｻﾞを宛先 IP に近づけることにより、さらに区分化できます。ﾘﾝｸの障害を分離するために再 Tx を排除するどうかを判別してください。</translation>
        </message>
        <message utf8="true">
            <source>Success!</source>
            <translation>成功</translation>
        </message>
        <message utf8="true">
            <source>Success</source>
            <translation>成功</translation>
        </message>
        <message utf8="true">
            <source>Summary</source>
            <translation>ｻﾏﾘ</translation>
        </message>
        <message utf8="true">
            <source>Summary of Measured Values:</source>
            <translation>測定値の概要 :</translation>
        </message>
        <message utf8="true">
            <source>Summary of Page {1}:</source>
            <translation>ﾍﾟｰｼﾞ {1} の概要 :</translation>
        </message>
        <message utf8="true">
            <source>SVLAN ID</source>
            <translation>SVLAN ID</translation>
        </message>
        <message utf8="true">
            <source>SVLAN User Priority</source>
            <translation>SVLAN User Priority</translation>
        </message>
        <message utf8="true">
            <source>Symmetric</source>
            <translation>対称</translation>
        </message>
        <message utf8="true">
            <source>Symmetric mode transmits and receives on the near end using loopback. Asymmetric transmits from the near end to the far end in Upstream mode and from the far end to the near end in Downstream mode.</source>
            <translation>対称ﾓｰﾄﾞは､ﾙｰﾌﾟﾊﾞｯｸで送受信します｡非対称ﾓｰﾄﾞはｱｯﾌﾟｽﾄﾘｰﾑﾓｰﾄﾞで近端から遠端に､ﾀﾞｳﾝｽﾄﾘｰﾑﾓｰﾄﾞで遠端から近端に送信します｡</translation>
        </message>
        <message utf8="true">
            <source>Symmetry</source>
            <translation>対称</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	      Click on a configuration name to select it </source>
            <translation>&#xA;&#xA;&#xA;	      構成名をｸﾘｯｸして選択します </translation>
        </message>
        <message utf8="true">
            <source>	Get {1} MB file....</source>
            <translation>	Get {1} MB ﾌｧｲﾙ ....</translation>
        </message>
        <message utf8="true">
            <source>	Put {1} MB file....</source>
            <translation>	{1} MB のﾌｧｲﾙを使用 ...</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} kbps&#xA;</source>
            <translation>	   転送速度 :{1} kbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	   Rate: {1} Mbps&#xA;</source>
            <translation>	   転送速度 :{1} Mbps&#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Rx Frames {1}</source>
            <translation>	Rx ﾌﾚｰﾑ数 {1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		              CAUTION!&#xA;&#xA;	    Are you sure you want to permanently&#xA;	           delete this configuration?&#xA;	{1}</source>
            <translation>&#xA;&#xA;		              注意 !&#xA;&#xA;	    この構成を完全に削除しても &#xA;	           よろしいですか ?&#xA;	{1}</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   Please use letters, numbers, spaces,&#xA;		   dashes and underscores only!</source>
            <translation>&#xA;&#xA;		   文字、数字、ｽﾍﾟｰｽ、ﾀﾞｯｼｭ、下線のみを &#xA;		   使用できます。</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;		   This configuration is read-only&#xA;		   and cannot be deleted.</source>
            <translation>&#xA;&#xA;		   この構成は読み取り専用で &#xA;		   削除できません</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;		         You must enter a name for the new&#xA;		           configuration using the keypad.</source>
            <translation>&#xA;&#xA;&#xA;		         ｷｰﾊﾟｯﾄﾞを使用して、新しい構成の名前を &#xA;		           入力する必要があります。</translation>
        </message>
        <message utf8="true">
            <source>&#xA;&#xA;&#xA;	   The configuration specified already exists.</source>
            <translation>&#xA;&#xA;&#xA;	   指定された構成はすでに存在します。</translation>
        </message>
        <message utf8="true">
            <source>	   Time: {1} seconds&#xA;</source>
            <translation>	   時間 :{1} 秒 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>	Tx Frames {1}</source>
            <translation>	Tx ﾌﾚｰﾑ {1}</translation>
        </message>
        <message utf8="true">
            <source>TCP Host failed to establish a connection. Test aborted.</source>
            <translation>TCP ﾎｽﾄが接続の確立に失敗しました。ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>TCP Host has encountered an error. Test aborted.</source>
            <translation>TCP ﾎｽﾄにｴﾗーが発生しました。ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>TCP Retransmissions</source>
            <translation>TCP 再 Tx </translation>
        </message>
        <message utf8="true">
            <source> Technician</source>
            <translation>ﾃｸﾆｼｬﾝ</translation>
        </message>
        <message utf8="true">
            <source>Technician</source>
            <translation>ﾃｸﾆｼｬﾝ</translation>
        </message>
        <message utf8="true">
            <source>Technician Name</source>
            <translation>ﾃｸﾆｼｬﾝ名</translation>
        </message>
        <message utf8="true">
            <source>Termination</source>
            <translation>終端</translation>
        </message>
        <message utf8="true">
            <source>                              Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Test Aborted</source>
            <translation>ﾃｽﾄ 中止</translation>
        </message>
        <message utf8="true">
            <source>Test aborted by user.</source>
            <translation>ﾕｰｻﾞによってﾃｽﾄが中止されました。</translation>
        </message>
        <message utf8="true">
            <source>Test at configured Max Bandwidth setting from the Setup - All Tests tab</source>
            <translation>ｾｯﾄｱｯﾌﾟで構成した最大帯域幅設定値によるﾃｽﾄ - [ すべてのﾃｽﾄ ] ﾀﾌﾞ</translation>
        </message>
        <message utf8="true">
            <source>test at different lower traffic rates. If you still get errors even on lower</source>
            <translation>さまざまな低速ﾄﾗﾌｨｯｸでﾃｽﾄします。低速でもｴﾗーが発生する場合</translation>
        </message>
        <message utf8="true">
            <source>Test Complete</source>
            <translation>ﾃｽﾄ 完了</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration:</source>
            <translation>ﾃｽﾄ 構成 :</translation>
        </message>
        <message utf8="true">
            <source>Test Configuration</source>
            <translation>ﾃｽﾄ 構成</translation>
        </message>
        <message utf8="true">
            <source>Test Duration</source>
            <translation>ﾃｽﾄ持続時間</translation>
        </message>
        <message utf8="true">
            <source>Test duration: At least 3 times the configured test duration.</source>
            <translation>ﾃｽﾄ期間 : 構成済みﾃｽﾄ期間の 3 倍以上。</translation>
        </message>
        <message utf8="true">
            <source>Tested Bandwidth</source>
            <translation>ﾃｽﾄされた帯域幅</translation>
        </message>
        <message utf8="true">
            <source>### Test Execution Complete ###</source>
            <translation>### ﾃｽﾄ 実行 完了 ###</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits </source>
            <translation>{1} ｸﾚｼﾞｯﾄ のﾃｽﾄ中</translation>
        </message>
        <message utf8="true">
            <source>Testing {1} credits</source>
            <translation>{1} ｸﾚｼﾞｯﾄ のﾃｽﾄ中</translation>
        </message>
        <message utf8="true">
            <source>Testing at </source>
            <translation>ﾃｽﾄの条件 </translation>
        </message>
        <message utf8="true">
            <source>Testing Connection... </source>
            <translation>接続のﾃｽﾄ中 ... </translation>
        </message>
        <message utf8="true">
            <source> Test Instrument Name</source>
            <translation> ﾃｽﾄ機器名</translation>
        </message>
        <message utf8="true">
            <source>Test is starting up</source>
            <translation>ﾃｽﾄの起動中</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>ﾃｽﾄ ﾛｹｰｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Test Log:</source>
            <translation>ﾃｽﾄ ﾛｸﾞ :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:</source>
            <translation>ﾃｽﾄ 名 :</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}&#xA;</source>
            <translation>ﾃｽﾄ名 :			{1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>Test Name:			{1}</source>
            <translation>ﾃｽﾄ名 :			{1}</translation>
        </message>
        <message utf8="true">
            <source>Test Procedure</source>
            <translation>ﾃｽﾄ手順</translation>
        </message>
        <message utf8="true">
            <source>Test Progress Log</source>
            <translation>ﾃｽﾄ 進捗 ﾛｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Test Range (%)</source>
            <translation>ﾃｽﾄ 範囲 (%)</translation>
        </message>
        <message utf8="true">
            <source>Test Range (Mbps)</source>
            <translation>ﾃｽﾄ 範囲 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Test Results</source>
            <translation>ﾃｽﾄ結果</translation>
        </message>
        <message utf8="true">
            <source>Test Set Setup</source>
            <translation>ﾃｽﾄｾｯﾄ 設定</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run:</source>
            <translation>実行するﾃｽﾄ :</translation>
        </message>
        <message utf8="true">
            <source>Tests to Run</source>
            <translation>実行するﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Test was aborted</source>
            <translation>ﾃｽﾄ は中止されました</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard loop was found</source>
            <translation>ｱｸﾃｨﾌﾞなﾙｰﾌﾟ ｱｯﾌﾟは失敗し、ﾊｰﾄﾞ ﾙｰﾌﾟは見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>The active loop up failed and no hard or permanent loop was found</source>
            <translation>ｱｸﾃｨﾌﾞなﾙｰﾌﾟ ｱｯﾌﾟは失敗し、ﾊｰﾄﾞあるいは無限ﾙｰﾌﾟは見つかりませんでした</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} kbps</source>
            <translation>{1} の平均ﾚｰﾄは {2} kbps でした</translation>
        </message>
        <message utf8="true">
            <source>The average rate for {1} was {2} Mbps</source>
            <translation>{1} の平均ﾚｰﾄは {2} Mbps でした</translation>
        </message>
        <message utf8="true">
            <source>The file exceeds the 50000 packet limit for JMentor</source>
            <translation>ﾌｧｲﾙは JMentor の 50000 ﾊﾟｹｯﾄ制限を超えています</translation>
        </message>
        <message utf8="true">
            <source>the Frame Loss Tolerance Threshold to tolerate small frame loss rates.</source>
            <translation>ｽﾓｰﾙ･ﾌﾚｰﾑ損失率を許容するﾌﾚｰﾑ損失許容度しきい値</translation>
        </message>
        <message utf8="true">
            <source>The Internet Control Message Protocol (ICMP) is most widely known in the context of the ICMP "Ping". The "ICMP Destination Unreachable" message indicates that a destination cannot be reached by the router or network device.</source>
            <translation>Internet Control Message Protocol (ICMP) は ICMP ping の関係で最も広く知られています。 ICMP 宛先到達不能 ﾒｯｾｰｼﾞは、ﾙｰﾀーまたはﾈｯﾄﾜｰｸ･ﾃﾞﾊﾞｲｽが宛先に到達できないことを示します。</translation>
        </message>
        <message utf8="true">
            <source>The L2 Filter Rx Acterna Frame count has continued to increment</source>
            <translation>L2 ﾌｨﾙﾀ Rx Acterna ﾌﾚｰﾑ･ｶｳﾝﾄは引き続き増加します</translation>
        </message>
        <message utf8="true">
            <source>The LBM/LBR loop failed.</source>
            <translation>LBM/LBR ﾙｰﾌﾟは失敗しました。</translation>
        </message>
        <message utf8="true">
            <source>The local and remote source IP addresses are identical.</source>
            <translation>ﾛｰｶﾙとﾘﾓｰﾄの送信元 IP ｱﾄﾞﾚｽは同じです｡</translation>
        </message>
        <message utf8="true">
            <source> The local setup settings were successfully copied to the remote setup</source>
            <translation> ﾛｰｶﾙ設定が正常にﾘﾓｰﾄ設定にｺﾋﾟーされました</translation>
        </message>
        <message utf8="true">
            <source>The local source IP address is  Unavailable</source>
            <translation>ﾛｰｶﾙ発信元 IP ｱﾄﾞﾚｽは使用できません</translation>
        </message>
        <message utf8="true">
            <source>The measured MTU is too small to continue. Test aborted.</source>
            <translation>測定された MTU が小さすぎて続行できません。 ﾃｽﾄは中止されました。</translation>
        </message>
        <message utf8="true">
            <source>The name you chose is already in use.</source>
            <translation>選択した名前は既に使用中です。</translation>
        </message>
        <message utf8="true">
            <source>The network element port we are connected to is provisioned to half duplex. If this is correct, press the "Continue in Half Duplex" button. Otherwise, press "Exit J-QuickCheck" and reprovision the port.</source>
            <translation>接続しているﾈｯﾄﾜｰｸ要素ﾎﾟｰﾄは、半二重用に準備されています。これで正しい場合は、 \[Continue in Half Duplex\] ( 半二重で続行 ) ﾎﾞﾀﾝを押します。正しくない場合は、 \[J-QuickCheck の終了 \] を押して、ﾎﾟｰﾄを、再度、準備してください。</translation>
        </message>
        <message utf8="true">
            <source>The network utilization chart displays the bandwidth consumed by all packets in the capture file over the time duration of the capture.  If TCP retransmissions were also detected, it is advisable to study the Layer TCP layer results by returning to the main analysis screen.</source>
            <translation>ﾈｯﾄﾜｰｸ使用率ﾁｬｰﾄには、ｷｬﾌﾟﾁｬ･ﾌｧｲﾙ内のすべてのﾊﾟｹｯﾄによって消費された帯域幅がｷｬﾌﾟﾁｬ期間の経過に沿って表示されます。 TCP 再 Tx も検出された場合は、ﾒｲﾝ分析画面に戻って、ﾚｲﾔ TCP ﾚｲﾔ結果を調べることをお勧めします。</translation>
        </message>
        <message utf8="true">
            <source>the number of buffer credits at each step to compensate for the double length of fibre.</source>
            <translation>ﾌｧｲﾊﾞの 2 倍長を補正するための各ｽﾃｯﾌﾟのﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ番号</translation>
        </message>
        <message utf8="true">
            <source>Theoretical Calculation</source>
            <translation>理論上の計算</translation>
        </message>
        <message utf8="true">
            <source>Theoretical &amp; Measured Values:</source>
            <translation>理論値と測定値 : </translation>
        </message>
        <message utf8="true">
            <source>The partner port (network element) has AutoNeg OFF and the Expected Throughput is Unavailable, so the partner port is most likely in half duplex mode. If half duplex at the partner port is not correct, please change the settings at the partner port to full duplex and run J-QuickCheck again. After that, if the measured Expected Throughput is more reasonable, you can run the RFC 2544 test. If the Expected Throughput is still Unavailable check the port configurations at the remote side. Maybe there is an HD to FD port mode mismatch.&#xA;&#xA;If half duplex at the partner port is correct, please go to Results -> Setup -> Interface -> Physical Layer and change Duplex setting from Full to Half. Than go back to the RFC2544 script (Results -> Expert RFC2544 Test), Exit J-QuickCheck, go to Throughput Tap and select Zeroing-in Process "RFC 2544 Standard (Half Duplex)" and run the RFC 2544 Test.</source>
            <translation>ﾊﾟｰﾄﾅｰ ﾎﾟｰﾄ ( ﾈｯﾄﾜｰｸ要素 ) で、 \[ 自動ﾈｺﾞｼｴｰｼｮﾝ \] が \[ ｵﾌ \] 、 \[ 予測ｽﾙｰﾌﾟｯﾄ \] が \[ 使用不可能 \] になっているため、最も可能性が高いのはﾘﾓｰﾄ ﾎﾟｰﾄが半二重ﾓｰﾄﾞになっていることです。ﾊﾟｰﾄﾅｰ ﾎﾟｰﾄが半二重になっているのが誤りである場合は、ﾊﾟｰﾄﾅｰ ﾎﾟｰﾄで設定を全二重に変更し、 J-QuickCheck をもう一度実行してください。その後、測定された \[ 予測ｽﾙｰﾌﾟｯﾄ \] がより適切な値になっている場合は、 RFC 2544 ﾃｽﾄを実行できます。 \[ 予測ｽﾙｰﾌﾟｯﾄ \] が依然として \[ 使用不可能 \] になっている場合は、ﾘﾓｰﾄ側でﾎﾟｰﾄ構成を確認してください。おそらく、 HD ( 半二重 ) と FD ( 全二重 ) のﾎﾟｰﾄ ﾓｰﾄﾞが不一致です。 &#xA;&#xA; ﾊﾟｰﾄﾅｰ ﾎﾟｰﾄが半二重になっているのが正しい場合は、 \[ 結果 \] -> \[ ｾｯﾄｱｯﾌﾟ \] -> \[ ｲﾝﾀｰﾌｪｲｽ \] -> \[ 物理ﾚｲﾔ \] を選択し、 \[ 二重 \] の設定を \[ 全 \] から \[ 半 \] に変更します。次に、 RFC2544 ｽｸﾘﾌﾟﾄに戻り (\[ 結果 \] -> \[ ｴｷｽﾊﾟｰﾄ RFC2544 ﾃｽﾄ \]) 、 \[J-QuickCheck の終了 \] を選択し、 \[ ｽﾙｰﾌﾟｯﾄ ﾀｯﾌﾟ \] に移動し、 \[ ｽﾞﾛｲﾝ ﾌﾟﾛｾｽ RFC 2544 標準 ( 半二重 )\] を選択し、 RFC 2544 ﾃｽﾄを実行します。</translation>
        </message>
        <message utf8="true">
            <source>There is a communication problem with the far end.</source>
            <translation>遠端との通信に問題があります。</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device does not respond to the Viavi loopback command but returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾃﾞﾊﾞｲｽは Viavi ﾙｰﾌﾟﾊﾞｯｸ ｺﾏﾝﾄﾞに応答しませんが、発信元と送信先のｱﾄﾞﾚｽ ﾌｨｰﾙﾄﾞを入れ替えて、送信されたﾌﾚｰﾑをﾛｰｶﾙ ﾃﾞﾊﾞｲｽに返します</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device responds to the Viavi loopback command and returns the transmitted frames back to the local device with the source and destination address fields swapped</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾃﾞﾊﾞｲｽは Viavi ﾙｰﾌﾟﾊﾞｯｸ ｺﾏﾝﾄﾞに応答し、発信元と送信先のｱﾄﾞﾚｽ ﾌｨｰﾙﾄﾞを入れ替えて、送信されたﾌﾚｰﾑをﾛｰｶﾙ ﾃﾞﾊﾞｲｽに返します</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device returns the transmitted frames unchanged back to the local device</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾃﾞﾊﾞｲｽは、送信されたﾌﾚｰﾑを変更せずにﾛｰｶﾙ ﾃﾞﾊﾞｲｽに返します</translation>
        </message>
        <message utf8="true">
            <source>the remote looping device supports OAM LBM and responds to a recieved LBM frame by transmitting a corresponding LBR frame back to the local device.</source>
            <translation>ﾘﾓｰﾄ ﾙｰﾌﾟ ﾃﾞﾊﾞｲｽは OAM LBM をｻﾎﾟｰﾄしており、受信された LBM ﾌﾚｰﾑに対応する LBR ﾌﾚｰﾑをﾛｰｶﾙ ﾃﾞﾊﾞｲｽに送信することにより、応答を返します。</translation>
        </message>
        <message utf8="true">
            <source>The remote side is set for MPLS encapsulation</source>
            <translation>ﾘﾓｰﾄ側は MPLS ｶﾌﾟｾﾙ化用に設定されています</translation>
        </message>
        <message utf8="true">
            <source>The remote side seems to be a Loopback application</source>
            <translation>ﾘﾓｰﾄ側はﾙｰﾌﾟﾊﾞｯｸ･ｱﾌﾟﾘｹｰｼｮﾝのようです</translation>
        </message>
        <message utf8="true">
            <source>The remote source IP address is Unavailable</source>
            <translation>ﾘﾓｰﾄ発信元 IP ｱﾄﾞﾚｽは使用できません</translation>
        </message>
        <message utf8="true">
            <source>&#xA;The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA; ﾚﾎﾟｰﾄは {1}{2} として PDF 形式で保存されています</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF format</source>
            <translation>&#xA; ﾚﾎﾟｰﾄは {1}{2} として PDF 形式で保存されています</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" in PDF, TXT and LOG formats</source>
            <translation> ﾚﾎﾟｰﾄは {1}{2} として PDF 、 TXT 、および LOG 形式で保存されています</translation>
        </message>
        <message utf8="true">
            <source>The report has been saved as "{1}{2}" TXT and LOG formats</source>
            <translation>ﾚﾎﾟｰﾄは {1}{2} として TXT および LOG 形式で保存されています</translation>
        </message>
        <message utf8="true">
            <source>The Responding Router IP cannot forward the packet to the Destination IP address, so troubleshooting should be conducted between the Responding Router IP and the Destination.</source>
            <translation>応答ﾙｰﾀｰ IP は宛先 IP ｱﾄﾞﾚｽにﾊﾟｹｯﾄを転送できないため、応答ﾙｰﾀｰ IP と宛先との間でﾄﾗﾌﾞﾙｼｭｰﾃｨﾝｸﾞを行う必要があります。</translation>
        </message>
        <message utf8="true">
            <source>The RFC 2544 test does not support MPLS encapsulation.</source>
            <translation>RFC 2544 ﾃｽﾄは MPLS ｶﾌﾟｾﾙ化をｻﾎﾟｰﾄしていません。</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser has been turned On&#xA;</source>
            <translation>ﾚｰｻﾞｰ出力が On になっています。 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!&#xA;Would you like to turn on the Laser?&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Tx ﾚｰｻﾞーがｵﾌです。&#xA;ﾚｰｻﾞーをｵﾝにしますか ?&#xA;&#xA; ﾚｰｻﾞーをｵﾝにする場合は &#xA;\[ はい \] 、中止する場合は &#xA;\[ いいえ \] を押してください。</translation>
        </message>
        <message utf8="true">
            <source>The Transmit Laser is Off!  Would you like to turn on the Laser?&#xA;&#xA;Press "Yes" to turn Laser On or "No" to Abort.</source>
            <translation>Tx ﾚｰｻﾞーがｵﾌです。ﾚｰｻﾞーをｵﾝにしますか ?&#xA;&#xA; ﾚｰｻﾞーをｵﾝにする場合は \[ はい \] 、中止する場合は \[ いいえ \] を押してください。</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be completed with OAM CCM On.</source>
            <translation>VLAN ﾃｽﾄは OAM CCM が On の場合完了できません</translation>
        </message>
        <message utf8="true">
            <source>The VLAN Scan test cannot be properly configured.</source>
            <translation>VLAN ﾃｽﾄは適切に設定できませんでした</translation>
        </message>
        <message utf8="true">
            <source>&#xA;       This configuration is read-only and cannot be modified.</source>
            <translation>&#xA;       この構成は 読込専用 で、修正できません。</translation>
        </message>
        <message utf8="true">
            <source>This should be the IP address of the far end when using Asymmetric mode</source>
            <translation>非対称ﾓｰﾄﾞを使用している場合は､遠端の IP ｱﾄﾞﾚｽを入力してください｡</translation>
        </message>
        <message utf8="true">
            <source>This table identifies the IP Source Addresses that are experiencing TCP retransmissions. When TCP retransmissions are detected, this could be due to downstream packet loss (toward the destination side).  It could also indicate that there is a half duplex port issue.</source>
            <translation>この表は、 TCP 再 Tx が発生している Tx 元 IP ｱﾄﾞﾚｽを示しています。 TCP 再 Tx が検出される場合、これはﾀﾞｳﾝｽﾄﾘｰﾑ･ﾊﾟｹｯﾄ損失 ( 宛先の方向 ) によるものである可能性があります。半二重ﾎﾟｰﾄの問題について示す可能性もあります。</translation>
        </message>
        <message utf8="true">
            <source>This test executes using Acterna Test Payload</source>
            <translation>このﾃｽﾄは Acterna Test Payload を使用して実行します｡</translation>
        </message>
        <message utf8="true">
            <source>This test is invalid.</source>
            <translation>このﾃｽﾄは無効です｡</translation>
        </message>
        <message utf8="true">
            <source>This test requires that traffic has a VLAN encapsulation. Ensure that the connected network will provide an IP address for this configuration.</source>
            <translation>このﾃｽﾄでは、ﾄﾗﾌｨｯｸが VLAN ｶﾌﾟｾﾙ化を持っている必要があります。接続されたﾈｯﾄﾜｰｸが、この構成の IP ｱﾄﾞﾚｽを提供することを確認してください。</translation>
        </message>
        <message utf8="true">
            <source>This will take {1} seconds.</source>
            <translation>これには {1} 秒かかります。</translation>
        </message>
        <message utf8="true">
            <source>Throughput (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ (%)</translation>
        </message>
        <message utf8="true">
            <source>Throughput</source>
            <translation>ｽﾙｰﾌﾟｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput ({1})</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ({1})</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Latency (RTD) Tests</source>
            <translation>ｽﾙｰﾌﾟｯﾄおよびﾚｲﾃﾝｼ (RTD) のﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput and Packet Jitter Tests</source>
            <translation>ｽﾙｰﾌﾟｯﾄおよびﾊﾟｹｯﾄ･ｼﾞｯﾀのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ 損失 しきい値 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Frame Loss Tolerance</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾌﾚｰﾑ 損失 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Throughput, Latency (RTD) and Packet Jitter Tests</source>
            <translation>ｽﾙｰﾌﾟｯﾄ、ﾚｲﾃﾝｼ (RTD) 、およびﾊﾟｹｯﾄ･ｼﾞｯﾀのﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ 合格 しきい値 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Pass Threshold</source>
            <translation>ｽﾙｰﾌﾟｯﾄ 合格 しきい値</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｽｹｰﾘﾝｸﾞ</translation>
        </message>
        <message utf8="true">
            <source>Throughput Scaling Factor</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ｽｹｰﾘﾝｸﾞ係数</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>Throughput test duration was {1} seconds.</source>
            <translation>ｽﾙｰﾌﾟｯﾄのﾃｽﾄ期間は {1} 秒でした。</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: ABORTED   </source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果 : 中止   </translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: FAIL</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果 : 失敗</translation>
        </message>
        <message utf8="true">
            <source>Throughput Test Results: PASS</source>
            <translation>ｽﾙｰﾌﾟｯﾄ ﾃｽﾄ 結果 : 合格</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (%)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ しきい値 (%)</translation>
        </message>
        <message utf8="true">
            <source> Throughput Threshold: {1}</source>
            <translation> ｽﾙｰﾌﾟｯﾄ しきい値 : {1}</translation>
        </message>
        <message utf8="true">
            <source>Throughput Threshold (Mbps)</source>
            <translation>ｽﾙｰﾌﾟｯﾄ しきい値 (Mbps)</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration:</source>
            <translation>ｽﾙｰﾌﾟｯﾄ 試行 期間 :</translation>
        </message>
        <message utf8="true">
            <source>Throughput Trial Duration</source>
            <translation>ｽﾙｰﾌﾟｯﾄ 試行 期間</translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process:</source>
            <translation>ｽﾙｰﾌﾟｯﾄのｽﾞﾛ機能ﾌﾟﾛｾｽ : </translation>
        </message>
        <message utf8="true">
            <source>Throughput Zeroing-in Process</source>
            <translation>ｽﾙｰﾌﾟｯﾄのｽﾞﾛ機能ﾌﾟﾛｾｽ</translation>
        </message>
        <message utf8="true">
            <source> Time End</source>
            <translation>時間 終了</translation>
        </message>
        <message utf8="true">
            <source>Time End</source>
            <translation>時間 終了</translation>
        </message>
        <message utf8="true">
            <source>Time per ID:</source>
            <translation>時間 /ID:</translation>
        </message>
        <message utf8="true">
            <source>Time (seconds)</source>
            <translation>時間 ( 秒 )?</translation>
        </message>
        <message utf8="true">
            <source>Time&#xA;(secs)</source>
            <translation>時間 &#xA;(sec)</translation>
        </message>
        <message utf8="true">
            <source> Time Start</source>
            <translation>時間 開始</translation>
        </message>
        <message utf8="true">
            <source>Time Start</source>
            <translation>時間 開始</translation>
        </message>
        <message utf8="true">
            <source>Times visited</source>
            <translation>訪問回数</translation>
        </message>
        <message utf8="true">
            <source>To continue, please check your cable connection then restart J-QuickCheck</source>
            <translation>続行するには、ｹｰﾌﾞﾙ接続を確認してから J-QuickCheck を再開してください</translation>
        </message>
        <message utf8="true">
            <source>To determine the Maximum Throughput choose the standard RFC 2544 method that matches tx and rx frame counts or the Viavi Enhanced method that uses the measured L2 Avg % Util.</source>
            <translation>最大ｽﾙｰﾌﾟｯﾄを判別するには、 Tx および Rx ﾌﾚｰﾑ･ｶｳﾝﾄに適合する標準 RFC 2544 方式、または測定した L2 平均使用率を使用する Viavi 拡張方式を選択します。</translation>
        </message>
        <message utf8="true">
            <source>Top Down</source>
            <translation>ﾄｯﾌﾟ ﾀﾞｳﾝ</translation>
        </message>
        <message utf8="true">
            <source>TOS</source>
            <translation>TOS</translation>
        </message>
        <message utf8="true">
            <source>To save time Latency (RTD) in Asymmetric mode should be run in one direction only</source>
            <translation>時間を短縮するには、非対称ﾓｰﾄﾞの待機時間 (RTD) は一方向のみで実行する必要があります</translation>
        </message>
        <message utf8="true">
            <source>to see if there are sporadic or constant frame loss events.</source>
            <translation>散発的または恒常的にﾌﾚｰﾑ損失ｲﾍﾞﾝﾄがあるかどうかを確認するには</translation>
        </message>
        <message utf8="true">
            <source>Total Bytes</source>
            <translation>合計ﾊﾞｲﾄ数</translation>
        </message>
        <message utf8="true">
            <source>Total&#xA;Frames</source>
            <translation>合計 &#xA; ﾌﾚｰﾑ数</translation>
        </message>
        <message utf8="true">
            <source>Total number</source>
            <translation>合計</translation>
        </message>
        <message utf8="true">
            <source>Total Util {1}</source>
            <translation>Total Util {1}</translation>
        </message>
        <message utf8="true">
            <source>Total Util (kbps):</source>
            <translation>合計 使用率 (kbps):</translation>
        </message>
        <message utf8="true">
            <source>Total Util (Mbps):</source>
            <translation>合計 使用率 (Mbps):</translation>
        </message>
        <message utf8="true">
            <source>To view report, select "View Report" on the Report menu after exiting {1}.</source>
            <translation>&#xA; ﾚﾎﾟｰﾄを表示するには、 を終了してから \[ ﾚﾎﾟｰﾄ \] ﾒﾆｭーの \[ ﾚﾎﾟｰﾄの表示 \] を選択します。 {1}</translation>
        </message>
        <message utf8="true">
            <source>To within</source>
            <translation>次の範囲が上限 :</translation>
        </message>
        <message utf8="true">
            <source>Traffic: Constant with {1}</source>
            <translation>ﾄﾗﾌｨｯｸ : 常に {1}</translation>
        </message>
        <message utf8="true">
            <source>Traffic Results</source>
            <translation>ﾄﾗﾌｨｯｸの結果</translation>
        </message>
        <message utf8="true">
            <source>Traffic was still being generated from the remote end</source>
            <translation>ﾄﾗﾌｨｯｸは引き続きﾘﾓｰﾄ･ｴﾝﾄﾞから生成されていました</translation>
        </message>
        <message utf8="true">
            <source>Transmit Laser is Off!</source>
            <translation>ﾚｰｻﾞｰ出力は Off です</translation>
        </message>
        <message utf8="true">
            <source>Transmitted Frames</source>
            <translation>送信ﾌﾚｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Downstream</source>
            <translation>ﾀﾞｳﾝｽﾄﾘｰﾑの Tx 中</translation>
        </message>
        <message utf8="true">
            <source>Transmitting Upstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑの Tx 中</translation>
        </message>
        <message utf8="true">
            <source>Trial</source>
            <translation>試行</translation>
        </message>
        <message utf8="true">
            <source>Trial {1}:</source>
            <translation>試行 {1}:</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} complete&#xA;</source>
            <translation>試行 {1} 完了 &#xA;</translation>
        </message>
        <message utf8="true">
            <source>Trial {1} of {2}:</source>
            <translation>試行 {1} / {2}:</translation>
        </message>
        <message utf8="true">
            <source>Trial Duration (seconds)</source>
            <translation>試行 期間 ( 秒数 )</translation>
        </message>
        <message utf8="true">
            <source>trials</source>
            <translation>試行</translation>
        </message>
        <message utf8="true">
            <source>Trying a second time</source>
            <translation>2 回目の試行中</translation>
        </message>
        <message utf8="true">
            <source>tshark error</source>
            <translation>tshark ｴﾗｰ</translation>
        </message>
        <message utf8="true">
            <source>TTL</source>
            <translation>TTL</translation>
        </message>
        <message utf8="true">
            <source>TX Buffer to Buffer Credits</source>
            <translation>TX ﾊﾞｯﾌｧﾂｰﾊﾞｯﾌｧ ｸﾚｼﾞｯﾄ</translation>
        </message>
        <message utf8="true">
            <source>Tx Direction</source>
            <translation>Tx 方向</translation>
        </message>
        <message utf8="true">
            <source>Tx Laser Off</source>
            <translation>Tx ﾚｰｻﾞｰ Off</translation>
        </message>
        <message utf8="true">
            <source>Tx Mbps, Cur L1</source>
            <translation>Tx Mbps, 現在 L1</translation>
        </message>
        <message utf8="true">
            <source>Tx Only</source>
            <translation>Tx のみ</translation>
        </message>
        <message utf8="true">
            <source> Unable to automatically loop up far end. </source>
            <translation> 遠端まで自動でﾙｯｸ ｱｯﾌﾟできません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!</source>
            <translation>ﾃｽﾄ測定ｱﾌﾟﾘｹｰｼｮﾝとの接続ができません</translation>
        </message>
        <message utf8="true">
            <source>Unable to connect with Test Measurement Application!&#xA;Press "Yes" to retry. "No" to Abort.</source>
            <translation>ﾃｽﾄ測定ｱﾌﾟﾘｹｰｼｮﾝに接続できません。 &#xA; 再試行するには \[ はい \] を押してください。中止するには \[ いいえ \] を押してください。</translation>
        </message>
        <message utf8="true">
            <source>Unable to obtain a DHCP address.</source>
            <translation>DHCP ｱﾄﾞﾚｽを取得できませんでした。</translation>
        </message>
        <message utf8="true">
            <source>Unable to run RFC2544 test with Local Loopback enabled.</source>
            <translation>ﾛｰｶﾙ ﾙｰﾌﾟﾊﾞｯｸを有効にして、 RFC2544 ﾃｽﾄを実行できません。</translation>
        </message>
        <message utf8="true">
            <source>Unable to run the test</source>
            <translation>ﾃｽﾄを実行できません</translation>
        </message>
        <message utf8="true">
            <source>Unable to run VLAN Scan test with Local Loopback enabled.</source>
            <translation>ﾛｰｶﾙ ﾙｰﾌﾟﾊﾞｯｸを有効にして、 VLAN ｽｷｬﾝを実行できません。</translation>
        </message>
        <message utf8="true">
            <source>Unavail</source>
            <translation>使用不可能</translation>
        </message>
        <message utf8="true">
            <source>UNAVAIL</source>
            <translation>使用不可能</translation>
        </message>
        <message utf8="true">
            <source>Unavailable</source>
            <translation>使用できません</translation>
        </message>
        <message utf8="true">
            <source>Unit Identifier</source>
            <translation>ﾕﾆｯﾄ名</translation>
        </message>
        <message utf8="true">
            <source>UP</source>
            <translation>上</translation>
        </message>
        <message utf8="true">
            <source>(Up or Down)</source>
            <translation>( 上または下 )</translation>
        </message>
        <message utf8="true">
            <source>Upstream</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ</translation>
        </message>
        <message utf8="true">
            <source>Upstream Direction</source>
            <translation>ｱｯﾌﾟｽﾄﾘｰﾑ方向</translation>
        </message>
        <message utf8="true">
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message utf8="true">
            <source>(us)</source>
            <translation>( μs)</translation>
        </message>
        <message utf8="true">
            <source>User Aborted test</source>
            <translation>ﾕｰｻﾞ 中止 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>User Cancelled test</source>
            <translation>ﾕｰｻﾞ 中止 ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>User Name</source>
            <translation>ﾕｰｻﾞ名</translation>
        </message>
        <message utf8="true">
            <source>User Priority</source>
            <translation>ﾕｰｻﾞ優先度</translation>
        </message>
        <message utf8="true">
            <source>User Requested Inactive</source>
            <translation>ﾕｰｻﾞ要求による無効化</translation>
        </message>
        <message utf8="true">
            <source>User Selected&#xA;( {1}  - {2})</source>
            <translation>ﾕｰｻﾞ 選択 &#xA;( {1}  - {2})</translation>
        </message>
        <message utf8="true">
            <source>User Selected      ( {1} - {2} )</source>
            <translation>ﾕｰｻﾞ 選択      ( {1} - {2} )</translation>
        </message>
        <message utf8="true">
            <source>Use the Summary Status screen to look for error events.</source>
            <translation>\[ 要約 / ｽﾃｰﾀｽ \] 画面を使用してｴﾗｰ･ｲﾍﾞﾝﾄがないかどうかを確認します。</translation>
        </message>
        <message utf8="true">
            <source>Using</source>
            <translation>使用中</translation>
        </message>
        <message utf8="true">
            <source>Using frame size of</source>
            <translation>使用するﾌﾚｰﾑ ｻｲﾂﾞ</translation>
        </message>
        <message utf8="true">
            <source>Utilization and TCP Retransmissions</source>
            <translation>使用率および TCP 再 Tx </translation>
        </message>
        <message utf8="true">
            <source>Value</source>
            <translation>値</translation>
        </message>
        <message utf8="true">
            <source>Values highlighted in blue are from actual tests.</source>
            <translation>青色にﾊｲﾗｲﾄされている値は現在のﾃｽﾄから存在します。</translation>
        </message>
        <message utf8="true">
            <source>Verifying that link is active...</source>
            <translation>ﾘﾝｸの稼動を確認中 ...</translation>
        </message>
        <message utf8="true">
            <source>verify your remote ip address and try again</source>
            <translation>ﾘﾓｰﾄ IP ｱﾄﾞﾚｽを確認して再試行してください</translation>
        </message>
        <message utf8="true">
            <source>verify your remote IP address and try again</source>
            <translation>ﾘﾓｰﾄ IP ｱﾄﾞﾚｽを確認して再試行してください</translation>
        </message>
        <message utf8="true">
            <source>Viavi Enhanced</source>
            <translation>Viavi 拡張</translation>
        </message>
        <message utf8="true">
            <source>VLAN</source>
            <translation>VLAN</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID</source>
            <translation>VLAN ID</translation>
        </message>
        <message utf8="true">
            <source>VLAN ID Ranges to Test</source>
            <translation>ﾃｽﾄの VLAN ID の範囲</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test</source>
            <translation>VLAN ｽｷｬﾝ ﾃｽﾄ</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Report</source>
            <translation>VLAN ｽｷｬﾝ ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>VLAN Scan Test Results</source>
            <translation>VLAN ｽｷｬﾝ ﾃｽﾄ 結果</translation>
        </message>
        <message utf8="true">
            <source>VLAN Test Report</source>
            <translation>VLAN ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>VLAN_TEST_REPORT</source>
            <translation>VLAN ﾃｽﾄ ﾚﾎﾟｰﾄ</translation>
        </message>
        <message utf8="true">
            <source>VTP/DTP/PAgP/UDLD frame detected!</source>
            <translation>VTP/DTP/PAgP/UDLD ﾌﾚｰﾑが検出されました。</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Auto Negotiation Done ...</source>
            <translation>ｵｰﾄ ﾈｺﾞｼｴｰｼｮﾝ完了の待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for destination MAC for&#xA;  IP Address</source>
            <translation>IP ｱﾄﾞﾚｽの宛先 MAC を &#xA; 待機中</translation>
        </message>
        <message utf8="true">
            <source>Waiting for DHCP parameters ...</source>
            <translation>DHCP ﾊﾟﾗﾒｰﾀの待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Layer 2 Link Present ...</source>
            <translation>ﾚｲﾔ 2 ﾘﾝｸ ありの待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>Waiting for Link</source>
            <translation>ﾘﾝｸを待機中</translation>
        </message>
        <message utf8="true">
            <source>Waiting for OWD to be enabled, ToD Sync, and 1PPS Sync</source>
            <translation>OWD が有効になり、 ToD および 1PPS の同期が得られるまでお待ちください</translation>
        </message>
        <message utf8="true">
            <source>Waiting for successful ARP ...</source>
            <translation>成功 ARP の待機中 ...</translation>
        </message>
        <message utf8="true">
            <source>was detected in the last second.</source>
            <translation>が最後に検出されました。</translation>
        </message>
        <message utf8="true">
            <source>Website size</source>
            <translation>ｳｪﾌﾞｻｲﾄ ｻｲｽﾞ</translation>
        </message>
        <message utf8="true">
            <source>We have an active loop</source>
            <translation>ｱｸﾃｨﾌﾞ･ﾙｰﾌﾟがあります</translation>
        </message>
        <message utf8="true">
            <source>We have an error!!! {1}&#xA;</source>
            <translation>ｴﾗｰ があります !!! {1}&#xA;</translation>
        </message>
        <message utf8="true">
            <source>When testing Half-Duplex links, select RFC 2544 Standard.</source>
            <translation>半二重ﾘﾝｸをﾃｽﾄするとき、 RFC 2544 標準を選択します。</translation>
        </message>
        <message utf8="true">
            <source>Window Size/Capacity</source>
            <translation>ｳｲﾝﾄﾞｳ ｻｲｽﾞ / 容量</translation>
        </message>
        <message utf8="true">
            <source>with the RFC 2544 recommendation.</source>
            <translation>RFC 2544 推奨事項</translation>
        </message>
        <message utf8="true">
            <source>Would you like to save a test report?&#xA;&#xA;Press "Yes" or "No".</source>
            <translation>ﾃｽﾄ･ﾚﾎﾟｰﾄを保存しますか ?&#xA;&#xA;\[ はい \] または \[ いいえ \] を押してください。</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>はい</translation>
        </message>
        <message utf8="true">
            <source>You can also use the Graphical Results Frame Loss Rate Cur graph</source>
            <translation>ｸﾞﾗﾌ表示の結果で現行ﾌﾚｰﾑ損失率ｸﾞﾗﾌも使用できます</translation>
        </message>
        <message utf8="true">
            <source>You cannot run this script from {1}.</source>
            <translation>このｽｸﾘﾌﾟﾄを {1} から実行できません。</translation>
        </message>
        <message utf8="true">
            <source>You might need to wait until it stops to reconnect</source>
            <translation>再接続するには、停止するまで待つ必要がある場合があります</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on maximum throughput rate</source>
            <translation>最大 ｽﾙｰﾌﾟｯﾄ ﾚｰﾄで ｾﾞﾛにしています</translation>
        </message>
        <message utf8="true">
            <source>Zeroing in on optimal credit buffer</source>
            <translation>最適 ｸﾚｼﾞｯﾄ ﾊﾞｯﾌｧ上で ｾﾞﾛにしています</translation>
        </message>
        <message utf8="true">
            <source>Zeroing-in Process</source>
            <translation>ｾﾞﾛｲﾝｸﾞ ｲﾝ ﾌﾟﾛｾｽ</translation>
        </message>
    </context>
</TS>
