<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>SystemWindowXML</name>
        <message utf8="true">
            <source>Files</source>
            <translation>Файлы</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>USB</translation>
        </message>
        <message utf8="true">
            <source>Removable Storage</source>
            <translation>Съемное устройство хранения</translation>
        </message>
        <message utf8="true">
            <source>No devices detected.</source>
            <translation>Устройства не обнаружены.</translation>
        </message>
        <message utf8="true">
            <source>Format</source>
            <translation>Формат</translation>
        </message>
        <message utf8="true">
            <source>By formatting this usb device, all existing data will be erased. This includes all files and partitions.</source>
            <translation>В результате форматирования настоящего устройства USB все имеющиеся данные будут удалены. В том числе будут удалены все файлы и разделы.</translation>
        </message>
        <message utf8="true">
            <source>Eject</source>
            <translation>Извлечь</translation>
        </message>
        <message utf8="true">
            <source>Browse...</source>
            <translation>Обзор...</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth</source>
            <translation>Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Yes</source>
            <translation>Да</translation>
        </message>
        <message utf8="true">
            <source>No</source>
            <translation>Hет</translation>
        </message>
        <message utf8="true">
            <source>NONE</source>
            <translation>НЕТ</translation>
        </message>
        <message utf8="true">
            <source>YES</source>
            <translation>ДА</translation>
        </message>
        <message utf8="true">
            <source>NO</source>
            <translation>Нет</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth Pair Requested</source>
            <translation>Запрос ответного Bluetooth устройства</translation>
        </message>
        <message utf8="true">
            <source>Enter PIN for pairing</source>
            <translation>Введите PIN-код для установления сопряжения</translation>
        </message>
        <message utf8="true">
            <source>Pair</source>
            <translation>Пара</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>Отменить</translation>
        </message>
        <message utf8="true">
            <source>Pairing Request</source>
            <translation>Запрос на сопряжение</translation>
        </message>
        <message utf8="true">
            <source>Pairing request from:</source>
            <translation>Запрос на сопряжение от:</translation>
        </message>
        <message utf8="true">
            <source>To pair with the device, make sure the code shown below matches the code on that device</source>
            <translation>Для сопряжения с устройством, убедитесь, что показанный ниже код совпадает с кодом на данном устройстве</translation>
        </message>
        <message utf8="true">
            <source>Pairing Initiated</source>
            <translation>Сопряжение началось</translation>
        </message>
        <message utf8="true">
            <source>Pairing request sent to:</source>
            <translation>Запрос на сопряжение был отправлен:</translation>
        </message>
        <message utf8="true">
            <source>Start Scanning</source>
            <translation>Начать сканирование</translation>
        </message>
        <message utf8="true">
            <source>Stop Scanning</source>
            <translation>Остановка сканирования</translation>
        </message>
        <message utf8="true">
            <source>Settings</source>
            <translation>Настройки</translation>
        </message>
        <message utf8="true">
            <source>Enable bluetooth</source>
            <translation>Включить Bluetooth</translation>
        </message>
        <message utf8="true">
            <source>Allow other devices to pair with this device</source>
            <translation>Разрешить сопряжение других устройств с данным устройством</translation>
        </message>
        <message utf8="true">
            <source>Device name</source>
            <translation>Имя устройства</translation>
        </message>
        <message utf8="true">
            <source>Mobile app enabled</source>
            <translation>Функция мобильного приложения активирована</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>Сеть</translation>
        </message>
        <message utf8="true">
            <source>LAN</source>
            <translation>LAN</translation>
        </message>
        <message utf8="true">
            <source>IPv4</source>
            <translation>IPv4</translation>
        </message>
        <message utf8="true">
            <source>IP mode</source>
            <translation>Режим IP</translation>
        </message>
        <message utf8="true">
            <source>DHCP</source>
            <translation>DHCP</translation>
        </message>
        <message utf8="true">
            <source>Static</source>
            <translation>Статич.</translation>
        </message>
        <message utf8="true">
            <source>MAC address</source>
            <translation>MAC-адрес</translation>
        </message>
        <message utf8="true">
            <source>IP address</source>
            <translation>IP-адрес</translation>
        </message>
        <message utf8="true">
            <source>Subnet mask</source>
            <translation>Маска подсети</translation>
        </message>
        <message utf8="true">
            <source>Gateway</source>
            <translation>Шлюз</translation>
        </message>
        <message utf8="true">
            <source>DNS server</source>
            <translation>Сервер DNS</translation>
        </message>
        <message utf8="true">
            <source>IPv6</source>
            <translation>IPv6</translation>
        </message>
        <message utf8="true">
            <source>IPv6 mode</source>
            <translation>Режим IPv6</translation>
        </message>
        <message utf8="true">
            <source>Disabled</source>
            <translation>Отключен</translation>
        </message>
        <message utf8="true">
            <source>Manual</source>
            <translation>Ручн.</translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>Авто</translation>
        </message>
        <message utf8="true">
            <source>IPv6 Address</source>
            <translation>IPv6- адрес</translation>
        </message>
        <message utf8="true">
            <source>Subnet Prefix Length</source>
            <translation>Длина префикса подсети</translation>
        </message>
        <message utf8="true">
            <source>DNS Server</source>
            <translation>Сервер DNS</translation>
        </message>
        <message utf8="true">
            <source>Link-Local Address</source>
            <translation>Локальн. адр. канала связи</translation>
        </message>
        <message utf8="true">
            <source>Stateless Address</source>
            <translation>Адрес без отслеживании состояний</translation>
        </message>
        <message utf8="true">
            <source>Stateful Address</source>
            <translation>Адрес с отслеживанием состояний</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi</source>
            <translation>Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Modules not loaded</source>
            <translation>Модули не загружены</translation>
        </message>
        <message utf8="true">
            <source>Present</source>
            <translation>В наличии</translation>
        </message>
        <message utf8="true">
            <source>Not Present</source>
            <translation>Отсутствует</translation>
        </message>
        <message utf8="true">
            <source>Starting</source>
            <translation>Запуск</translation>
        </message>
        <message utf8="true">
            <source>Enabling</source>
            <translation>Включение</translation>
        </message>
        <message utf8="true">
            <source>Initializing</source>
            <translation>Инициализация</translation>
        </message>
        <message utf8="true">
            <source>Ready</source>
            <translation>Готовность</translation>
        </message>
        <message utf8="true">
            <source>Scanning</source>
            <translation>Сканирование</translation>
        </message>
        <message utf8="true">
            <source>Disabling</source>
            <translation>Отключение</translation>
        </message>
        <message utf8="true">
            <source>Stopping</source>
            <translation>Остановка</translation>
        </message>
        <message utf8="true">
            <source>Associating</source>
            <translation>Связывание</translation>
        </message>
        <message utf8="true">
            <source>Associated</source>
            <translation>Связывание выполнено</translation>
        </message>
        <message utf8="true">
            <source>Enable wireless adapter</source>
            <translation>Включить беспроводной адаптер</translation>
        </message>
        <message utf8="true">
            <source>Connected</source>
            <translation>ПОДКЛЮЧЕН</translation>
        </message>
        <message utf8="true">
            <source>Disconnected</source>
            <translation>Разъединение выполнено</translation>
        </message>
        <message utf8="true">
            <source>Failed</source>
            <translation>Ошибка</translation>
        </message>
        <message utf8="true">
            <source>PEAP</source>
            <translation>PEAP</translation>
        </message>
        <message utf8="true">
            <source>TLS</source>
            <translation>TLS</translation>
        </message>
        <message utf8="true">
            <source>TTLS</source>
            <translation>TTLS</translation>
        </message>
        <message utf8="true">
            <source>MSCHAPV2</source>
            <translation>MSCHAPV2</translation>
        </message>
        <message utf8="true">
            <source>MD5</source>
            <translation>MD5</translation>
        </message>
        <message utf8="true">
            <source>OTP</source>
            <translation>OTP</translation>
        </message>
        <message utf8="true">
            <source>GTC</source>
            <translation>GTC</translation>
        </message>
        <message utf8="true">
            <source>OK</source>
            <translation>OK</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Enterprise Network</source>
            <translation>Подключиться к корпоративной сети WPA</translation>
        </message>
        <message utf8="true">
            <source>Network name</source>
            <translation>Имя сети</translation>
        </message>
        <message utf8="true">
            <source>Outer Authentication method</source>
            <translation>Другой способ аутентификации</translation>
        </message>
        <message utf8="true">
            <source>Inner Authentication method</source>
            <translation>Способ внутренней аутентификации</translation>
        </message>
        <message utf8="true">
            <source>Username</source>
            <translation>Имя пользователя</translation>
        </message>
        <message utf8="true">
            <source>Anonymous Identity</source>
            <translation>Анонимный пользователь</translation>
        </message>
        <message utf8="true">
            <source>Password</source>
            <translation>Пароль</translation>
        </message>
        <message utf8="true">
            <source>Certificates</source>
            <translation>Сертификаты</translation>
        </message>
        <message utf8="true">
            <source>Private Key Password</source>
            <translation>Индивидуальный ключ - пароль</translation>
        </message>
        <message utf8="true">
            <source>Connect to WPA Personal Network</source>
            <translation>Подключение к персональной сети WPA</translation>
        </message>
        <message utf8="true">
            <source>Passphrase</source>
            <translation>Парольная фраза</translation>
        </message>
        <message utf8="true">
            <source>No USB wireless device found.</source>
            <translation>Беспроводное устройство USB не обнаружено.</translation>
        </message>
        <message utf8="true">
            <source>Status</source>
            <translation>Состояние</translation>
        </message>
        <message utf8="true">
            <source>Stop Connecting</source>
            <translation>Остановить соединение</translation>
        </message>
        <message utf8="true">
            <source>Forget Network</source>
            <translation>Забыть сеть</translation>
        </message>
        <message utf8="true">
            <source>Could not connect to the network.</source>
            <translation>Невозможно установить соединение с сетью .</translation>
        </message>
        <message utf8="true">
            <source>3G Service</source>
            <translation>Служба 3G</translation>
        </message>
        <message utf8="true">
            <source>Enabling modem</source>
            <translation>Идет активация модема</translation>
        </message>
        <message utf8="true">
            <source>Modem enabled</source>
            <translation>Модем включен</translation>
        </message>
        <message utf8="true">
            <source>Modem disabled</source>
            <translation>Модем отключен</translation>
        </message>
        <message utf8="true">
            <source>No modem detected</source>
            <translation>Модем не обнаружен</translation>
        </message>
        <message utf8="true">
            <source>Error - Modem is not responding.</source>
            <translation>Ошибка - Модем не отвечает .</translation>
        </message>
        <message utf8="true">
            <source>Error - Device controller has not started.</source>
            <translation>Ошибка - Контроллер устройства не запущен .</translation>
        </message>
        <message utf8="true">
            <source>Error - Could not configure modem.</source>
            <translation>Ошибка - Сбой конфигурации модема .</translation>
        </message>
        <message utf8="true">
            <source>Warning - Modem is not responding. Modem is being reset...</source>
            <translation>Внимание - Модем не отвечает . Перезагрузка модема ...</translation>
        </message>
        <message utf8="true">
            <source>Error - SIM not inserted.</source>
            <translation>Ошибка - SIM- карта не установлена .</translation>
        </message>
        <message utf8="true">
            <source>Error - Network registration denied.</source>
            <translation>Ошибка - Регистрация сети отклонена .</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>Подключиться</translation>
        </message>
        <message utf8="true">
            <source>Disconnect</source>
            <translation>Разъединить</translation>
        </message>
        <message utf8="true">
            <source>Connecting</source>
            <translation>Идет подключен.</translation>
        </message>
        <message utf8="true">
            <source>Disconnecting</source>
            <translation>Отсоединение</translation>
        </message>
        <message utf8="true">
            <source>Not connected</source>
            <translation>Отсутствует соединение</translation>
        </message>
        <message utf8="true">
            <source>Connection failed</source>
            <translation>Сбой подключения</translation>
        </message>
        <message utf8="true">
            <source>Modem Setup</source>
            <translation>Настройка модема</translation>
        </message>
        <message utf8="true">
            <source>Enable modem</source>
            <translation>Активация модема</translation>
        </message>
        <message utf8="true">
            <source>No modem device found or enabled.</source>
            <translation>Модемное устройство не найдено или не активировано .</translation>
        </message>
        <message utf8="true">
            <source>Network APN</source>
            <translation>Сеть APN</translation>
        </message>
        <message utf8="true">
            <source>Connection Status</source>
            <translation>Состояние подключения</translation>
        </message>
        <message utf8="true">
            <source>Check APN and try again.</source>
            <translation>Проверьте APN и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>Pri DNS Server</source>
            <translation>Сервер Pri DNS</translation>
        </message>
        <message utf8="true">
            <source>Sec DNS Server</source>
            <translation>Сервер Sec DNS</translation>
        </message>
        <message utf8="true">
            <source>Proxy &amp; Security</source>
            <translation>Прокси и защита</translation>
        </message>
        <message utf8="true">
            <source>Proxy</source>
            <translation>Proxy- система</translation>
        </message>
        <message utf8="true">
            <source>Proxy type</source>
            <translation>Тип прокси-сервера</translation>
        </message>
        <message utf8="true">
            <source>HTTP</source>
            <translation>HTTP</translation>
        </message>
        <message utf8="true">
            <source>Proxy server</source>
            <translation>Прокси сервер</translation>
        </message>
        <message utf8="true">
            <source>Network Security</source>
            <translation>Безопасность сети</translation>
        </message>
        <message utf8="true">
            <source>Disable FTP, telnet, and auto StrataSync</source>
            <translation>Отключить FTP, telnet, и авто StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Power Management</source>
            <translation>Управление электропитанием</translation>
        </message>
        <message utf8="true">
            <source>AC power is plugged in</source>
            <translation>Питание подключено</translation>
        </message>
        <message utf8="true">
            <source>Running on battery power</source>
            <translation>Питание от батареи</translation>
        </message>
        <message utf8="true">
            <source>No battery detected</source>
            <translation>Батарея не обнаружена</translation>
        </message>
        <message utf8="true">
            <source>Charging has been disabled due to power consumption</source>
            <translation>Функция зарядки батареи отключена для экономии энергопотребления</translation>
        </message>
        <message utf8="true">
            <source>Charging battery</source>
            <translation>Идет зарядка батареи</translation>
        </message>
        <message utf8="true">
            <source>Battery is fully charged</source>
            <translation>Батарея полностью заряжена</translation>
        </message>
        <message utf8="true">
            <source>Not charging battery</source>
            <translation>Батарея не заряжается </translation>
        </message>
        <message utf8="true">
            <source>Unable to charge battery due to power failure</source>
            <translation>Невозможно зарядить батарею в связи с отключением электропитания</translation>
        </message>
        <message utf8="true">
            <source>Unknown battery status</source>
            <translation>Неизвестное состояние батареи</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot and may not charge</source>
            <translation>Аккумулятор перегрелся и не заряжается</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Please plug in AC power</source>
            <translation>Аккумулятор перегрелся&#xA;Включите питание</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Do not unplug the AC power</source>
            <translation>Аккумулятор перегрелся&#xA;Не отключайте питание</translation>
        </message>
        <message utf8="true">
            <source>The battery is too hot&#xA;Unplugging the AC power will force a shut down</source>
            <translation>Аккумулятор перегрелся&#xA;Отключение питания приведет к выключению системы</translation>
        </message>
        <message utf8="true">
            <source>The battery is too cold and may not charge</source>
            <translation>Аккумулятор слишком холодный и не заряжается</translation>
        </message>
        <message utf8="true">
            <source>The battery is in danger of overheating</source>
            <translation>Опасность перегревания батареи</translation>
        </message>
        <message utf8="true">
            <source>Battery temperature is normal</source>
            <translation>Нормальная температура батареи</translation>
        </message>
        <message utf8="true">
            <source>Charge</source>
            <translation>Заряд</translation>
        </message>
        <message utf8="true">
            <source>Enable auto-off while on battery</source>
            <translation>Включить автоматическое отключение когда устройство работает от аккумулятора</translation>
        </message>
        <message utf8="true">
            <source>Inactive time (minutes)</source>
            <translation>Неактивное время (мин.)</translation>
        </message>
        <message utf8="true">
            <source>Date and Time</source>
            <translation>Дата и время</translation>
        </message>
        <message utf8="true">
            <source>Time Zone</source>
            <translation>Часовой пояс</translation>
        </message>
        <message utf8="true">
            <source>Region</source>
            <translation>Регион</translation>
        </message>
        <message utf8="true">
            <source>Africa</source>
            <translation>Африка</translation>
        </message>
        <message utf8="true">
            <source>Americas</source>
            <translation>Америка</translation>
        </message>
        <message utf8="true">
            <source>Antarctica</source>
            <translation>Антарктида</translation>
        </message>
        <message utf8="true">
            <source>Asia</source>
            <translation>Азия</translation>
        </message>
        <message utf8="true">
            <source>Atlantic Ocean</source>
            <translation>Атлантический океан</translation>
        </message>
        <message utf8="true">
            <source>Australia</source>
            <translation>Австралия</translation>
        </message>
        <message utf8="true">
            <source>Europe</source>
            <translation>Европа</translation>
        </message>
        <message utf8="true">
            <source>Indian Ocean</source>
            <translation>Индийский океан</translation>
        </message>
        <message utf8="true">
            <source>Pacific Ocean</source>
            <translation>Тихий океан</translation>
        </message>
        <message utf8="true">
            <source>GMT</source>
            <translation>GMT</translation>
        </message>
        <message utf8="true">
            <source>Country</source>
            <translation>Страна</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>Ничего</translation>
        </message>
        <message utf8="true">
            <source>Afghanistan</source>
            <translation>Афганистан</translation>
        </message>
        <message utf8="true">
            <source>Åland Islands</source>
            <translation>Аландские острова</translation>
        </message>
        <message utf8="true">
            <source>Albania</source>
            <translation>Албания</translation>
        </message>
        <message utf8="true">
            <source>Algeria</source>
            <translation>Алжир</translation>
        </message>
        <message utf8="true">
            <source>American Samoa</source>
            <translation>Американское Самоа</translation>
        </message>
        <message utf8="true">
            <source>Andorra</source>
            <translation>Андорра</translation>
        </message>
        <message utf8="true">
            <source>Angola</source>
            <translation>Ангола</translation>
        </message>
        <message utf8="true">
            <source>Anguilla</source>
            <translation>Ангилья</translation>
        </message>
        <message utf8="true">
            <source>Antigua and Barbuda</source>
            <translation>Антигуа и Барбуда</translation>
        </message>
        <message utf8="true">
            <source>Argentina</source>
            <translation>Аргентина</translation>
        </message>
        <message utf8="true">
            <source>Armenia</source>
            <translation>Армения</translation>
        </message>
        <message utf8="true">
            <source>Aruba</source>
            <translation>Аруба</translation>
        </message>
        <message utf8="true">
            <source>Austria</source>
            <translation>Австрия</translation>
        </message>
        <message utf8="true">
            <source>Azerbaijan</source>
            <translation>Азербайджан</translation>
        </message>
        <message utf8="true">
            <source>Bahamas</source>
            <translation>Багамские острова</translation>
        </message>
        <message utf8="true">
            <source>Bahrain</source>
            <translation>Бахрейн</translation>
        </message>
        <message utf8="true">
            <source>Bangladesh</source>
            <translation>Бангладеш</translation>
        </message>
        <message utf8="true">
            <source>Barbados</source>
            <translation>Барбадос</translation>
        </message>
        <message utf8="true">
            <source>Belarus</source>
            <translation>Беларусь</translation>
        </message>
        <message utf8="true">
            <source>Belgium</source>
            <translation>Бельгия</translation>
        </message>
        <message utf8="true">
            <source>Belize</source>
            <translation>Белиз</translation>
        </message>
        <message utf8="true">
            <source>Benin</source>
            <translation>Бенин</translation>
        </message>
        <message utf8="true">
            <source>Bermuda</source>
            <translation>Бермудские острова</translation>
        </message>
        <message utf8="true">
            <source>Bhutan</source>
            <translation>Бутан</translation>
        </message>
        <message utf8="true">
            <source>Bolivia</source>
            <translation>Боливия</translation>
        </message>
        <message utf8="true">
            <source>Bosnia and Herzegovina</source>
            <translation>Босния и Герцеговина</translation>
        </message>
        <message utf8="true">
            <source>Botswana</source>
            <translation>Ботсвана</translation>
        </message>
        <message utf8="true">
            <source>Bouvet Island</source>
            <translation>Остров Буве</translation>
        </message>
        <message utf8="true">
            <source>Brazil</source>
            <translation>Бразилия</translation>
        </message>
        <message utf8="true">
            <source>British Indian Ocean Territory</source>
            <translation>Британская территория в Индийском океане</translation>
        </message>
        <message utf8="true">
            <source>Brunei Darussalam</source>
            <translation>Бруней-Даруссалам</translation>
        </message>
        <message utf8="true">
            <source>Bulgaria</source>
            <translation>Болгария</translation>
        </message>
        <message utf8="true">
            <source>Burkina Faso</source>
            <translation>Буркина-Фасо</translation>
        </message>
        <message utf8="true">
            <source>Burundi</source>
            <translation>Бурунди</translation>
        </message>
        <message utf8="true">
            <source>Cambodia</source>
            <translation>Камбоджа</translation>
        </message>
        <message utf8="true">
            <source>Cameroon</source>
            <translation>Камерун</translation>
        </message>
        <message utf8="true">
            <source>Canada</source>
            <translation>Канада</translation>
        </message>
        <message utf8="true">
            <source>Cape Verde</source>
            <translation>Кабо-Верде</translation>
        </message>
        <message utf8="true">
            <source>Cayman Islands</source>
            <translation>Каймановы острова</translation>
        </message>
        <message utf8="true">
            <source>Central African Republic</source>
            <translation>Центральноафриканская Республика</translation>
        </message>
        <message utf8="true">
            <source>Chad</source>
            <translation>Чад</translation>
        </message>
        <message utf8="true">
            <source>Chile</source>
            <translation>Чили</translation>
        </message>
        <message utf8="true">
            <source>China</source>
            <translation>Китай</translation>
        </message>
        <message utf8="true">
            <source>Christmas Island</source>
            <translation>Остров Рождества</translation>
        </message>
        <message utf8="true">
            <source>Cocos (Keeling) Islands</source>
            <translation>Кокосовые острова (острова Килинг)</translation>
        </message>
        <message utf8="true">
            <source>Colombia</source>
            <translation>Колумбия</translation>
        </message>
        <message utf8="true">
            <source>Comoros</source>
            <translation>Коморские острова</translation>
        </message>
        <message utf8="true">
            <source>Congo</source>
            <translation>Конго</translation>
        </message>
        <message utf8="true">
            <source>Congo, the Democratic Republic of the</source>
            <translation>Демократическая Республика Конго</translation>
        </message>
        <message utf8="true">
            <source>Cook Islands</source>
            <translation>Острова Кука</translation>
        </message>
        <message utf8="true">
            <source>Costa Rica</source>
            <translation>Коста-Рика</translation>
        </message>
        <message utf8="true">
            <source>Côte d'Ivoire</source>
            <translation>Кот-д'Ивуар</translation>
        </message>
        <message utf8="true">
            <source>Croatia</source>
            <translation>Хорватия</translation>
        </message>
        <message utf8="true">
            <source>Cuba</source>
            <translation>Куба</translation>
        </message>
        <message utf8="true">
            <source>Cyprus</source>
            <translation>Кипр</translation>
        </message>
        <message utf8="true">
            <source>Czech Republic</source>
            <translation>Чешская Республика</translation>
        </message>
        <message utf8="true">
            <source>Denmark</source>
            <translation>Дания</translation>
        </message>
        <message utf8="true">
            <source>Djibouti</source>
            <translation>Джибути</translation>
        </message>
        <message utf8="true">
            <source>Dominica</source>
            <translation>Доминика</translation>
        </message>
        <message utf8="true">
            <source>Dominican Republic</source>
            <translation>Доминиканская Республика</translation>
        </message>
        <message utf8="true">
            <source>Ecuador</source>
            <translation>Эквадор</translation>
        </message>
        <message utf8="true">
            <source>Egypt</source>
            <translation>Египет</translation>
        </message>
        <message utf8="true">
            <source>El Salvador</source>
            <translation>Сальвадор</translation>
        </message>
        <message utf8="true">
            <source>Equatorial Guinea</source>
            <translation>Экваториальная Гвинея</translation>
        </message>
        <message utf8="true">
            <source>Eritrea</source>
            <translation>Эритрея</translation>
        </message>
        <message utf8="true">
            <source>Estonia</source>
            <translation>Эстония</translation>
        </message>
        <message utf8="true">
            <source>Ethiopia</source>
            <translation>Эфиопия</translation>
        </message>
        <message utf8="true">
            <source>Falkland Islands (Malvinas)</source>
            <translation>Фолклендские (Мальвинские) острова</translation>
        </message>
        <message utf8="true">
            <source>Faroe Islands</source>
            <translation>Фарерские острова</translation>
        </message>
        <message utf8="true">
            <source>Fiji</source>
            <translation>Фиджи</translation>
        </message>
        <message utf8="true">
            <source>Finland</source>
            <translation>Финляндия</translation>
        </message>
        <message utf8="true">
            <source>France</source>
            <translation>Франция</translation>
        </message>
        <message utf8="true">
            <source>French Guiana</source>
            <translation>Французская Гвиана</translation>
        </message>
        <message utf8="true">
            <source>French Polynesia</source>
            <translation>Французская Полинезия</translation>
        </message>
        <message utf8="true">
            <source>French Southern Territories</source>
            <translation>Французские южные территории</translation>
        </message>
        <message utf8="true">
            <source>Gabon</source>
            <translation>Габон</translation>
        </message>
        <message utf8="true">
            <source>Gambia</source>
            <translation>Гамбия</translation>
        </message>
        <message utf8="true">
            <source>Georgia</source>
            <translation>Грузия</translation>
        </message>
        <message utf8="true">
            <source>Germany</source>
            <translation>Германия</translation>
        </message>
        <message utf8="true">
            <source>Ghana</source>
            <translation>Гана</translation>
        </message>
        <message utf8="true">
            <source>Gibraltar</source>
            <translation>Гибралтар</translation>
        </message>
        <message utf8="true">
            <source>Greece</source>
            <translation>Греция</translation>
        </message>
        <message utf8="true">
            <source>Greenland</source>
            <translation>Гренландия</translation>
        </message>
        <message utf8="true">
            <source>Grenada</source>
            <translation>Гренада</translation>
        </message>
        <message utf8="true">
            <source>Guadeloupe</source>
            <translation>Гваделупа</translation>
        </message>
        <message utf8="true">
            <source>Guam</source>
            <translation>Гуам</translation>
        </message>
        <message utf8="true">
            <source>Guatemala</source>
            <translation>Гватемала</translation>
        </message>
        <message utf8="true">
            <source>Guernsey</source>
            <translation>Гернси</translation>
        </message>
        <message utf8="true">
            <source>Guinea</source>
            <translation>Гвинея</translation>
        </message>
        <message utf8="true">
            <source>Guinea-Bissau</source>
            <translation>Гвинея-Бисау</translation>
        </message>
        <message utf8="true">
            <source>Guyana</source>
            <translation>Гайана</translation>
        </message>
        <message utf8="true">
            <source>Haiti</source>
            <translation>Гаити</translation>
        </message>
        <message utf8="true">
            <source>Heard Island and McDonald Islands</source>
            <translation>Остров Херд и острова Макдональд</translation>
        </message>
        <message utf8="true">
            <source>Honduras</source>
            <translation>Гондурас</translation>
        </message>
        <message utf8="true">
            <source>Hong Kong</source>
            <translation>Гонконг</translation>
        </message>
        <message utf8="true">
            <source>Hungary</source>
            <translation>Венгрия</translation>
        </message>
        <message utf8="true">
            <source>Iceland</source>
            <translation>Исландия</translation>
        </message>
        <message utf8="true">
            <source>India</source>
            <translation>Индия</translation>
        </message>
        <message utf8="true">
            <source>Indonesia</source>
            <translation>Индонезия</translation>
        </message>
        <message utf8="true">
            <source>Iran</source>
            <translation>Иран</translation>
        </message>
        <message utf8="true">
            <source>Iraq</source>
            <translation>Ирак</translation>
        </message>
        <message utf8="true">
            <source>Ireland</source>
            <translation>Ирландия</translation>
        </message>
        <message utf8="true">
            <source>Isle of Man</source>
            <translation>Остров Мэн</translation>
        </message>
        <message utf8="true">
            <source>Israel</source>
            <translation>Израиль</translation>
        </message>
        <message utf8="true">
            <source>Italy</source>
            <translation>Италия</translation>
        </message>
        <message utf8="true">
            <source>Jamaica</source>
            <translation>Ямайка</translation>
        </message>
        <message utf8="true">
            <source>Japan</source>
            <translation>Япония</translation>
        </message>
        <message utf8="true">
            <source>Jersey</source>
            <translation>Джерси</translation>
        </message>
        <message utf8="true">
            <source>Jordan</source>
            <translation>Иордания</translation>
        </message>
        <message utf8="true">
            <source>Kazakhstan</source>
            <translation>Казахстан</translation>
        </message>
        <message utf8="true">
            <source>Kenya</source>
            <translation>Кения</translation>
        </message>
        <message utf8="true">
            <source>Kiribati</source>
            <translation>Кирибати</translation>
        </message>
        <message utf8="true">
            <source>Korea, Democratic People's Republic of</source>
            <translation>Корейская Народно-Демократическая Республика""</translation>
        </message>
        <message utf8="true">
            <source>Korea, Republic of</source>
            <translation>Республика Корея</translation>
        </message>
        <message utf8="true">
            <source>Kuwait</source>
            <translation>Кувейт</translation>
        </message>
        <message utf8="true">
            <source>Kyrgyzstan</source>
            <translation>Киргизия</translation>
        </message>
        <message utf8="true">
            <source>Lao People's Democratic Republic</source>
            <translation>Лаосская Народно-Демократическая Республика</translation>
        </message>
        <message utf8="true">
            <source>Latvia</source>
            <translation>Латвия</translation>
        </message>
        <message utf8="true">
            <source>Lebanon</source>
            <translation>Ливан</translation>
        </message>
        <message utf8="true">
            <source>Lesotho</source>
            <translation>Лесото</translation>
        </message>
        <message utf8="true">
            <source>Liberia</source>
            <translation>Либерия</translation>
        </message>
        <message utf8="true">
            <source>Libya</source>
            <translation>Ливия</translation>
        </message>
        <message utf8="true">
            <source>Liechtenstein</source>
            <translation>Лихтенштейн</translation>
        </message>
        <message utf8="true">
            <source>Lithuania</source>
            <translation>Литва</translation>
        </message>
        <message utf8="true">
            <source>Luxembourg</source>
            <translation>Люксембург</translation>
        </message>
        <message utf8="true">
            <source>Macao</source>
            <translation>Макао</translation>
        </message>
        <message utf8="true">
            <source>Macedonia, the Former Yugoslav Republic of</source>
            <translation>Республика Македония</translation>
        </message>
        <message utf8="true">
            <source>Madagascar</source>
            <translation>Мадагаскар</translation>
        </message>
        <message utf8="true">
            <source>Malawi</source>
            <translation>Малави</translation>
        </message>
        <message utf8="true">
            <source>Malaysia</source>
            <translation>Малайзия</translation>
        </message>
        <message utf8="true">
            <source>Maldives</source>
            <translation>Мальдивы</translation>
        </message>
        <message utf8="true">
            <source>Mali</source>
            <translation>Мали</translation>
        </message>
        <message utf8="true">
            <source>Malta</source>
            <translation>Мальта</translation>
        </message>
        <message utf8="true">
            <source>Marshall Islands</source>
            <translation>Маршалловы острова</translation>
        </message>
        <message utf8="true">
            <source>Martinique</source>
            <translation>Остров Мартиника</translation>
        </message>
        <message utf8="true">
            <source>Mauritania</source>
            <translation>Мавритания</translation>
        </message>
        <message utf8="true">
            <source>Mauritius</source>
            <translation>Маврикий</translation>
        </message>
        <message utf8="true">
            <source>Mayotte</source>
            <translation>Майотта</translation>
        </message>
        <message utf8="true">
            <source>Mexico</source>
            <translation>Мексика</translation>
        </message>
        <message utf8="true">
            <source>Micronesia, Federated States of</source>
            <translation>Федеративные Штаты Микронезии</translation>
        </message>
        <message utf8="true">
            <source>Moldova, Republic of</source>
            <translation>Республика Молдова</translation>
        </message>
        <message utf8="true">
            <source>Monaco</source>
            <translation>Монако</translation>
        </message>
        <message utf8="true">
            <source>Mongolia</source>
            <translation>Монголия</translation>
        </message>
        <message utf8="true">
            <source>Montenegro</source>
            <translation>Черногория</translation>
        </message>
        <message utf8="true">
            <source>Montserrat</source>
            <translation>Монтсеррат</translation>
        </message>
        <message utf8="true">
            <source>Morocco</source>
            <translation>Марокко</translation>
        </message>
        <message utf8="true">
            <source>Mozambique</source>
            <translation>Мозамбик</translation>
        </message>
        <message utf8="true">
            <source>Myanmar</source>
            <translation>Мьянма</translation>
        </message>
        <message utf8="true">
            <source>Namibia</source>
            <translation>Намибия</translation>
        </message>
        <message utf8="true">
            <source>Nauru</source>
            <translation>Науру</translation>
        </message>
        <message utf8="true">
            <source>Nepal</source>
            <translation>Непал</translation>
        </message>
        <message utf8="true">
            <source>Netherlands</source>
            <translation>Нидерланды</translation>
        </message>
        <message utf8="true">
            <source>Netherlands Antilles</source>
            <translation>Нидерландские Антильские острова</translation>
        </message>
        <message utf8="true">
            <source>New Caledonia</source>
            <translation>Новая Каледония</translation>
        </message>
        <message utf8="true">
            <source>New Zealand</source>
            <translation>Новая Зеландия</translation>
        </message>
        <message utf8="true">
            <source>Nicaragua</source>
            <translation>Никарагуа</translation>
        </message>
        <message utf8="true">
            <source>Niger</source>
            <translation>Нигер</translation>
        </message>
        <message utf8="true">
            <source>Nigeria</source>
            <translation>Нигерия</translation>
        </message>
        <message utf8="true">
            <source>Niue</source>
            <translation>Ниуэ</translation>
        </message>
        <message utf8="true">
            <source>Norfolk Island</source>
            <translation>Остров Норфолк</translation>
        </message>
        <message utf8="true">
            <source>Northern Mariana Islands</source>
            <translation>Северные Марианские острова</translation>
        </message>
        <message utf8="true">
            <source>Norway</source>
            <translation>Норвегия</translation>
        </message>
        <message utf8="true">
            <source>Oman</source>
            <translation>Оман</translation>
        </message>
        <message utf8="true">
            <source>Pakistan</source>
            <translation>Пакистан</translation>
        </message>
        <message utf8="true">
            <source>Palau</source>
            <translation>Палау</translation>
        </message>
        <message utf8="true">
            <source>Palestinian Territory</source>
            <translation>Палестинские территории</translation>
        </message>
        <message utf8="true">
            <source>Panama</source>
            <translation>Панама</translation>
        </message>
        <message utf8="true">
            <source>Papua New Guinea</source>
            <translation>Папуа-Новая Гвинея</translation>
        </message>
        <message utf8="true">
            <source>Paraguay</source>
            <translation>Парагвай</translation>
        </message>
        <message utf8="true">
            <source>Peru</source>
            <translation>Перу</translation>
        </message>
        <message utf8="true">
            <source>Philippines</source>
            <translation>Филиппины</translation>
        </message>
        <message utf8="true">
            <source>Pitcairn</source>
            <translation>Питкэрн</translation>
        </message>
        <message utf8="true">
            <source>Poland</source>
            <translation>Польша</translation>
        </message>
        <message utf8="true">
            <source>Portugal</source>
            <translation>Португалия</translation>
        </message>
        <message utf8="true">
            <source>Puerto Rico</source>
            <translation>Пуэрто-Рико</translation>
        </message>
        <message utf8="true">
            <source>Qatar</source>
            <translation>Катар</translation>
        </message>
        <message utf8="true">
            <source>Réunion</source>
            <translation>Реюньон</translation>
        </message>
        <message utf8="true">
            <source>Romania</source>
            <translation>Румыния</translation>
        </message>
        <message utf8="true">
            <source>Russian Federation</source>
            <translation>Российская Федерация</translation>
        </message>
        <message utf8="true">
            <source>Rwanda</source>
            <translation>Руанда</translation>
        </message>
        <message utf8="true">
            <source>Saint Barthélemy</source>
            <translation>Сен-Бартельми</translation>
        </message>
        <message utf8="true">
            <source>Saint Helena, Ascension and Tristan da Cunha</source>
            <translation>Остров Святой Елены, остров Вознесения и острова Тристан-да-Кунья</translation>
        </message>
        <message utf8="true">
            <source>Saint Kitts and Nevis</source>
            <translation>Сент-Китс и Невис</translation>
        </message>
        <message utf8="true">
            <source>Saint Lucia</source>
            <translation>Сент-Люсия</translation>
        </message>
        <message utf8="true">
            <source>Saint Martin</source>
            <translation>Остров Святого Мартина</translation>
        </message>
        <message utf8="true">
            <source>Saint Pierre and Miquelon</source>
            <translation>Сен-Пьер и Микелон</translation>
        </message>
        <message utf8="true">
            <source>Saint Vincent and the Grenadines</source>
            <translation>Сент-Винсент и Гренадины</translation>
        </message>
        <message utf8="true">
            <source>Samoa</source>
            <translation>Самоа</translation>
        </message>
        <message utf8="true">
            <source>San Marino</source>
            <translation>Сан-Мариноа</translation>
        </message>
        <message utf8="true">
            <source>Sao Tome And Principe</source>
            <translation>Сан-Томе и Принсипи</translation>
        </message>
        <message utf8="true">
            <source>Saudi Arabia</source>
            <translation>Саудовская Аравия</translation>
        </message>
        <message utf8="true">
            <source>Senegal</source>
            <translation>Сенегал</translation>
        </message>
        <message utf8="true">
            <source>Serbia</source>
            <translation>Сербия</translation>
        </message>
        <message utf8="true">
            <source>Seychelles</source>
            <translation>Сейшельские острова</translation>
        </message>
        <message utf8="true">
            <source>Sierra Leone</source>
            <translation>Сьерра-Леоне</translation>
        </message>
        <message utf8="true">
            <source>Singapore</source>
            <translation>Сингапур</translation>
        </message>
        <message utf8="true">
            <source>Slovakia</source>
            <translation>Словакия</translation>
        </message>
        <message utf8="true">
            <source>Slovenia</source>
            <translation>Словения</translation>
        </message>
        <message utf8="true">
            <source>Solomon Islands</source>
            <translation>Соломоновы острова</translation>
        </message>
        <message utf8="true">
            <source>Somalia</source>
            <translation>Сомали</translation>
        </message>
        <message utf8="true">
            <source>South Africa</source>
            <translation>Южно-Африканская Республика</translation>
        </message>
        <message utf8="true">
            <source>South Georgia and the South Sandwich Islands</source>
            <translation>Южная Георгия и Южные Сандвичевы острова</translation>
        </message>
        <message utf8="true">
            <source>Spain</source>
            <translation>Испания</translation>
        </message>
        <message utf8="true">
            <source>Sri Lanka</source>
            <translation>Шри-Ланка</translation>
        </message>
        <message utf8="true">
            <source>Sudan</source>
            <translation>Судан</translation>
        </message>
        <message utf8="true">
            <source>Suriname</source>
            <translation>Суринам</translation>
        </message>
        <message utf8="true">
            <source>Svalbard and Jan Mayen</source>
            <translation>Шпицберген и Ян-Майен</translation>
        </message>
        <message utf8="true">
            <source>Swaziland</source>
            <translation>Свазиленд</translation>
        </message>
        <message utf8="true">
            <source>Sweden</source>
            <translation>Швеция</translation>
        </message>
        <message utf8="true">
            <source>Switzerland</source>
            <translation>Швейцария</translation>
        </message>
        <message utf8="true">
            <source>Syrian Arab Republic</source>
            <translation>Сирийская Арабская Республика</translation>
        </message>
        <message utf8="true">
            <source>Taiwan</source>
            <translation>Тайвань</translation>
        </message>
        <message utf8="true">
            <source>Tajikistan</source>
            <translation>Таджикистан</translation>
        </message>
        <message utf8="true">
            <source>Tanzania, United Republic of</source>
            <translation>Объединенная Республика Танзания</translation>
        </message>
        <message utf8="true">
            <source>Thailand</source>
            <translation>Таиланд</translation>
        </message>
        <message utf8="true">
            <source>Timor-Leste</source>
            <translation>Демократическая Республика Тимор Лоросае (Восточный Тимор)</translation>
        </message>
        <message utf8="true">
            <source>Togo</source>
            <translation>Того</translation>
        </message>
        <message utf8="true">
            <source>Tokelau</source>
            <translation>Токелау</translation>
        </message>
        <message utf8="true">
            <source>Tonga</source>
            <translation>Тонга</translation>
        </message>
        <message utf8="true">
            <source>Trinidad and Tobago</source>
            <translation>Тринидад и Тобаго</translation>
        </message>
        <message utf8="true">
            <source>Tunisia</source>
            <translation>Тунис</translation>
        </message>
        <message utf8="true">
            <source>Turkey</source>
            <translation>Турция</translation>
        </message>
        <message utf8="true">
            <source>Turkmenistan</source>
            <translation>Туркменистан</translation>
        </message>
        <message utf8="true">
            <source>Turks and Caicos Islands</source>
            <translation>Острова Теркс и Кайкос</translation>
        </message>
        <message utf8="true">
            <source>Tuvalu</source>
            <translation>Тувалу</translation>
        </message>
        <message utf8="true">
            <source>Uganda</source>
            <translation>Уганда</translation>
        </message>
        <message utf8="true">
            <source>Ukraine</source>
            <translation>Украина</translation>
        </message>
        <message utf8="true">
            <source>United Arab Emirates</source>
            <translation>Объединенные Арабские Эмираты</translation>
        </message>
        <message utf8="true">
            <source>United Kingdom</source>
            <translation>Великобритания</translation>
        </message>
        <message utf8="true">
            <source>United States</source>
            <translation>США</translation>
        </message>
        <message utf8="true">
            <source>U.S. Minor Outlying Islands</source>
            <translation>Внешние малые острова США</translation>
        </message>
        <message utf8="true">
            <source>Uruguay</source>
            <translation>Уругвай</translation>
        </message>
        <message utf8="true">
            <source>Uzbekistan</source>
            <translation>Узбекистан</translation>
        </message>
        <message utf8="true">
            <source>Vanuatu</source>
            <translation>Вануату</translation>
        </message>
        <message utf8="true">
            <source>Vatican City</source>
            <translation>Ватикан</translation>
        </message>
        <message utf8="true">
            <source>Venezuela</source>
            <translation>Венесуэла</translation>
        </message>
        <message utf8="true">
            <source>Viet Nam</source>
            <translation>Вьетнам</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, British</source>
            <translation>Британские Виргинские острова</translation>
        </message>
        <message utf8="true">
            <source>Virgin Islands, U.S.</source>
            <translation>Виргинские острова США</translation>
        </message>
        <message utf8="true">
            <source>Wallis and Futuna</source>
            <translation>Уоллис и Футуна</translation>
        </message>
        <message utf8="true">
            <source>Western Sahara</source>
            <translation>Западная Сахара</translation>
        </message>
        <message utf8="true">
            <source>Yemen</source>
            <translation>Йемен</translation>
        </message>
        <message utf8="true">
            <source>Zambia</source>
            <translation>Замбия</translation>
        </message>
        <message utf8="true">
            <source>Zimbabwe</source>
            <translation>Зимбабве</translation>
        </message>
        <message utf8="true">
            <source>Area</source>
            <translation>Территория</translation>
        </message>
        <message utf8="true">
            <source>Casey</source>
            <translation>Станция Кейси</translation>
        </message>
        <message utf8="true">
            <source>Davis</source>
            <translation>Станция Дейвис</translation>
        </message>
        <message utf8="true">
            <source>Dumont d'Urville</source>
            <translation>Станция Дюмон д’Юрвиль</translation>
        </message>
        <message utf8="true">
            <source>Mawson</source>
            <translation>Станция Моусон</translation>
        </message>
        <message utf8="true">
            <source>McMurdo</source>
            <translation>Станция Мак-Мердо</translation>
        </message>
        <message utf8="true">
            <source>Palmer</source>
            <translation>Земля Пальмера</translation>
        </message>
        <message utf8="true">
            <source>Rothera</source>
            <translation>Станция Ротера</translation>
        </message>
        <message utf8="true">
            <source>South Pole</source>
            <translation>Южный полюс</translation>
        </message>
        <message utf8="true">
            <source>Syowa</source>
            <translation>Станция Сёва</translation>
        </message>
        <message utf8="true">
            <source>Vostok</source>
            <translation>Станция Восток</translation>
        </message>
        <message utf8="true">
            <source>Australian Capital Territory</source>
            <translation>Австралийская столичная территория</translation>
        </message>
        <message utf8="true">
            <source>North</source>
            <translation>Север</translation>
        </message>
        <message utf8="true">
            <source>New South Wales</source>
            <translation>Новый Южный Уэльс</translation>
        </message>
        <message utf8="true">
            <source>Queensland</source>
            <translation>Квинсленд</translation>
        </message>
        <message utf8="true">
            <source>South</source>
            <translation>Юг</translation>
        </message>
        <message utf8="true">
            <source>Tasmania</source>
            <translation>Тасмания</translation>
        </message>
        <message utf8="true">
            <source>Victoria</source>
            <translation>Виктория</translation>
        </message>
        <message utf8="true">
            <source>West</source>
            <translation>Запад</translation>
        </message>
        <message utf8="true">
            <source>Brasilia</source>
            <translation>Бразилиа</translation>
        </message>
        <message utf8="true">
            <source>Brasilia - 1</source>
            <translation>Бразилиа - 1</translation>
        </message>
        <message utf8="true">
            <source>Brasilia + 1</source>
            <translation>Бразилиа + 1</translation>
        </message>
        <message utf8="true">
            <source>Alaska</source>
            <translation>Аляска</translation>
        </message>
        <message utf8="true">
            <source>Arizona</source>
            <translation>Аризона</translation>
        </message>
        <message utf8="true">
            <source>Atlantic</source>
            <translation>Атлантический регион</translation>
        </message>
        <message utf8="true">
            <source>Central</source>
            <translation>Центральный регион</translation>
        </message>
        <message utf8="true">
            <source>Eastern</source>
            <translation>Восточный регион</translation>
        </message>
        <message utf8="true">
            <source>Hawaii</source>
            <translation>Гавайи</translation>
        </message>
        <message utf8="true">
            <source>Mountain</source>
            <translation>Гора</translation>
        </message>
        <message utf8="true">
            <source>New Foundland</source>
            <translation>Ньюфаундленд</translation>
        </message>
        <message utf8="true">
            <source>Pacific</source>
            <translation>Тихоокеанский регион</translation>
        </message>
        <message utf8="true">
            <source>Saskatchewan</source>
            <translation>Саскачеван</translation>
        </message>
        <message utf8="true">
            <source>Easter Island</source>
            <translation>Остров Пасхи</translation>
        </message>
        <message utf8="true">
            <source>Kinshasa</source>
            <translation>Киншаса</translation>
        </message>
        <message utf8="true">
            <source>Lubumbashi</source>
            <translation>Лубумбаши</translation>
        </message>
        <message utf8="true">
            <source>Galapagos</source>
            <translation>Галапагосские острова</translation>
        </message>
        <message utf8="true">
            <source>Gambier</source>
            <translation>Острова Гамбье</translation>
        </message>
        <message utf8="true">
            <source>Marquesas</source>
            <translation>Маркизские острова</translation>
        </message>
        <message utf8="true">
            <source>Tahiti</source>
            <translation>Остров Таити</translation>
        </message>
        <message utf8="true">
            <source>Western</source>
            <translation>Западный регион</translation>
        </message>
        <message utf8="true">
            <source>Danmarkshavn</source>
            <translation>Станция Данмарксхавн</translation>
        </message>
        <message utf8="true">
            <source>East</source>
            <translation>Восточный регион</translation>
        </message>
        <message utf8="true">
            <source>Phoenix Islands</source>
            <translation>Острова Феникс</translation>
        </message>
        <message utf8="true">
            <source>Line Islands</source>
            <translation>Острова Лайн</translation>
        </message>
        <message utf8="true">
            <source>Gilbert Islands</source>
            <translation>Острова Гилберта</translation>
        </message>
        <message utf8="true">
            <source>Northwest</source>
            <translation>Северо-западный регион</translation>
        </message>
        <message utf8="true">
            <source>Kosrae</source>
            <translation>Кусаие</translation>
        </message>
        <message utf8="true">
            <source>Truk</source>
            <translation>Острова Трук</translation>
        </message>
        <message utf8="true">
            <source>Azores</source>
            <translation>Азорские острова</translation>
        </message>
        <message utf8="true">
            <source>Madeira</source>
            <translation>Остров Мадейра</translation>
        </message>
        <message utf8="true">
            <source>Irkutsk</source>
            <translation>Иркутск</translation>
        </message>
        <message utf8="true">
            <source>Kaliningrad</source>
            <translation>Калининград</translation>
        </message>
        <message utf8="true">
            <source>Krasnoyarsk</source>
            <translation>Красноярск</translation>
        </message>
        <message utf8="true">
            <source>Magadan</source>
            <translation>Магадан</translation>
        </message>
        <message utf8="true">
            <source>Moscow</source>
            <translation>Москва</translation>
        </message>
        <message utf8="true">
            <source>Omsk</source>
            <translation>Омск</translation>
        </message>
        <message utf8="true">
            <source>Vladivostok</source>
            <translation>Владивосток</translation>
        </message>
        <message utf8="true">
            <source>Yakutsk</source>
            <translation>Якутск</translation>
        </message>
        <message utf8="true">
            <source>Yekaterinburg</source>
            <translation>Екатеринбург</translation>
        </message>
        <message utf8="true">
            <source>Canary Islands</source>
            <translation>Канарские острова</translation>
        </message>
        <message utf8="true">
            <source>Svalbard</source>
            <translation>Шпицберген</translation>
        </message>
        <message utf8="true">
            <source>Jan Mayen</source>
            <translation>Ян-Майен</translation>
        </message>
        <message utf8="true">
            <source>Johnston</source>
            <translation>Джонстон</translation>
        </message>
        <message utf8="true">
            <source>Midway</source>
            <translation>Остров Мидуэй</translation>
        </message>
        <message utf8="true">
            <source>Wake</source>
            <translation>Остров Уэйк</translation>
        </message>
        <message utf8="true">
            <source>GMT+0</source>
            <translation>GMT+0</translation>
        </message>
        <message utf8="true">
            <source>GMT+1</source>
            <translation>GMT+1</translation>
        </message>
        <message utf8="true">
            <source>GMT+2</source>
            <translation>GMT+2</translation>
        </message>
        <message utf8="true">
            <source>GMT+3</source>
            <translation>GMT+3</translation>
        </message>
        <message utf8="true">
            <source>GMT+4</source>
            <translation>GMT+4</translation>
        </message>
        <message utf8="true">
            <source>GMT+5</source>
            <translation>GMT+5</translation>
        </message>
        <message utf8="true">
            <source>GMT+6</source>
            <translation>GMT+6</translation>
        </message>
        <message utf8="true">
            <source>GMT+7</source>
            <translation>GMT+7</translation>
        </message>
        <message utf8="true">
            <source>GMT+8</source>
            <translation>GMT+8</translation>
        </message>
        <message utf8="true">
            <source>GMT+9</source>
            <translation>GMT+9</translation>
        </message>
        <message utf8="true">
            <source>GMT+10</source>
            <translation>GMT+10</translation>
        </message>
        <message utf8="true">
            <source>GMT+11</source>
            <translation>GMT+11</translation>
        </message>
        <message utf8="true">
            <source>GMT+12</source>
            <translation>GMT+12</translation>
        </message>
        <message utf8="true">
            <source>GMT-0</source>
            <translation>GMT-0</translation>
        </message>
        <message utf8="true">
            <source>GMT-1</source>
            <translation>GMT-1</translation>
        </message>
        <message utf8="true">
            <source>GMT-2</source>
            <translation>GMT-2</translation>
        </message>
        <message utf8="true">
            <source>GMT-3</source>
            <translation>GMT-3</translation>
        </message>
        <message utf8="true">
            <source>GMT-4</source>
            <translation>GMT-4</translation>
        </message>
        <message utf8="true">
            <source>GMT-5</source>
            <translation>GMT-5</translation>
        </message>
        <message utf8="true">
            <source>GMT-6</source>
            <translation>GMT-6</translation>
        </message>
        <message utf8="true">
            <source>GMT-7</source>
            <translation>GMT-7</translation>
        </message>
        <message utf8="true">
            <source>GMT-8</source>
            <translation>GMT-8</translation>
        </message>
        <message utf8="true">
            <source>GMT-9</source>
            <translation>GMT-9</translation>
        </message>
        <message utf8="true">
            <source>GMT-10</source>
            <translation>GMT-10</translation>
        </message>
        <message utf8="true">
            <source>GMT-11</source>
            <translation>GMT-11</translation>
        </message>
        <message utf8="true">
            <source>GMT-12</source>
            <translation>GMT-12</translation>
        </message>
        <message utf8="true">
            <source>GMT-13</source>
            <translation>GMT-13</translation>
        </message>
        <message utf8="true">
            <source>GMT-14</source>
            <translation>GMT-14</translation>
        </message>
        <message utf8="true">
            <source>Automatically adjust for daylight savings time</source>
            <translation>Автоматический переход на летнее и зимнее время</translation>
        </message>
        <message utf8="true">
            <source>Current Date &amp; Time</source>
            <translation>Текущие дата &amp; время</translation>
        </message>
        <message utf8="true">
            <source>24 hour</source>
            <translation>24-часовой режим</translation>
        </message>
        <message utf8="true">
            <source>12 hour</source>
            <translation>12-часовой режим</translation>
        </message>
        <message utf8="true">
            <source>Set clock with NTP</source>
            <translation>Установить часы с помощью NTP</translation>
        </message>
        <message utf8="true">
            <source>true</source>
            <translation>верно</translation>
        </message>
        <message utf8="true">
            <source>false</source>
            <translation>неверно</translation>
        </message>
        <message utf8="true">
            <source>Use 24-hour time</source>
            <translation>Использовать 24-часовой режим</translation>
        </message>
        <message utf8="true">
            <source>LAN NTP Server</source>
            <translation>Сервер LAN NTP</translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi NTP Server</source>
            <translation>Сервер Wi-Fi NTP</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 1</source>
            <translation>NTP сервер 1</translation>
        </message>
        <message utf8="true">
            <source>NTP Server 2</source>
            <translation>NTP сервер 2</translation>
        </message>
        <message utf8="true">
            <source>Date</source>
            <translation>Дата</translation>
        </message>
        <message utf8="true">
            <source>English</source>
            <translation>English (Английский)</translation>
        </message>
        <message utf8="true">
            <source>Deutsch (German)</source>
            <translation>Deutsch (Немецкий)</translation>
        </message>
        <message utf8="true">
            <source>Español (Spanish)</source>
            <translation>Español (Испанский)</translation>
        </message>
        <message utf8="true">
            <source>Français (French)</source>
            <translation>Français (Французский)</translation>
        </message>
        <message utf8="true">
            <source>中文 (Simplified Chinese)</source>
            <translation>中文 (китайский , упрощенное письмо)</translation>
        </message>
        <message utf8="true">
            <source>日本語 (Japanese)</source>
            <translation>日本語 (Японский)</translation>
        </message>
        <message utf8="true">
            <source>한국어 (Korean)</source>
            <translation>한국어  корейский)</translation>
        </message>
        <message utf8="true">
            <source>Русский (Russian)</source>
            <translation>Русский</translation>
        </message>
        <message utf8="true">
            <source>Português (Portuguese)</source>
            <translation>Português (Португальский)</translation>
        </message>
        <message utf8="true">
            <source>Italiano (Italian)</source>
            <translation>Italiano (Итальянский)</translation>
        </message>
        <message utf8="true">
            <source>Türk (Turkish)</source>
            <translation>Türk (Турецкий)</translation>
        </message>
        <message utf8="true">
            <source>Language:</source>
            <translation>Язык:</translation>
        </message>
        <message utf8="true">
            <source>Change formatting standard:</source>
            <translation>Изменить стандарт&#xA;форматирования:</translation>
        </message>
        <message utf8="true">
            <source>Samples for selected formatting:</source>
            <translation>Образцы выбранного форматирования:</translation>
        </message>
        <message utf8="true">
            <source>Customize</source>
            <translation>Настроить</translation>
        </message>
        <message utf8="true">
            <source>Display</source>
            <translation>Дисплей</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Яркость</translation>
        </message>
        <message utf8="true">
            <source>Screen Saver</source>
            <translation>Заставка</translation>
        </message>
        <message utf8="true">
            <source>Enable automatic screen saver</source>
            <translation>Разрешить автоматический запуск заставки</translation>
        </message>
        <message utf8="true">
            <source>Message</source>
            <translation>Сообщение</translation>
        </message>
        <message utf8="true">
            <source>Delay</source>
            <translation>Задержка</translation>
        </message>
        <message utf8="true">
            <source>Screen saver password</source>
            <translation>Пароль заставки</translation>
        </message>
        <message utf8="true">
            <source>Calibrate touchscreen...</source>
            <translation>Настроить сенсорный экран...</translation>
        </message>
        <message utf8="true">
            <source>Remote</source>
            <translation>Дистанцион.</translation>
        </message>
        <message utf8="true">
            <source>Enable VNC access</source>
            <translation>Разрешить доступ к VNC</translation>
        </message>
        <message utf8="true">
            <source>Toggling VNC access or password protection will disconnect existing connections.</source>
            <translation>Изменение значения доступа к VNC или смена параметров защиты паролем приведет к отключению текущих соединений.</translation>
        </message>
        <message utf8="true">
            <source>Remote access password</source>
            <translation>Пароль удаленного доступа</translation>
        </message>
        <message utf8="true">
            <source>This password is used for all remote access, e.g. vnc, ftp, ssh.</source>
            <translation>Данный пароль используется для удаленного доступа, например, vnc, ftp, ssh.</translation>
        </message>
        <message utf8="true">
            <source>VNC</source>
            <translation>VNC</translation>
        </message>
        <message utf8="true">
            <source>There was an error starting VNC.</source>
            <translation>При запуске VNC произошла ошибка.</translation>
        </message>
        <message utf8="true">
            <source>Require password for VNC access</source>
            <translation>Необходим пароль для доступа к VNC</translation>
        </message>
        <message utf8="true">
            <source>Smart Access Anywhere</source>
            <translation>Smart Access в любой точке</translation>
        </message>
        <message utf8="true">
            <source>Access code</source>
            <translation>Код доступа</translation>
        </message>
        <message utf8="true">
            <source>LAN IP address</source>
            <translation>IP адрес LAN </translation>
        </message>
        <message utf8="true">
            <source>Wi-Fi IP address</source>
            <translation>IP адрес Wi-Fi</translation>
        </message>
        <message utf8="true">
            <source>Number of VNC connections</source>
            <translation>Количество соединений VNC</translation>
        </message>
        <message utf8="true">
            <source>Upgrade</source>
            <translation>Обновление</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade, AC power is not plugged in. Please plug in AC power and try again.</source>
            <translation>Не удается обновить, сеть переменного тока не подключена. Пожалуйста, подключите сеть переменного тока и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>Unable to detect a USB flash device. Please confirm the USB flash device is securely inserted and try again.</source>
            <translation>Невозможно обнаружить флэш-диск USB. Установите флэш-диск USB надлежащим образом и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>Unable to upgrade. Please specify upgrade server and try again.</source>
            <translation>Невозможно выполнить обновление. Укажите сервер обновления и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>Network connection is unavailable. Please verify network connection and try again.</source>
            <translation>Отсутствует подключение к сети. Проверьте подключение к сети и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>Unable to contact upgrade server. Please verify the server address and try again.</source>
            <translation>Невозможно связаться с сервером обновления. Проверьте адрес сервера и повторите попытку.</translation>
        </message>
        <message utf8="true">
            <source>Error encountered looking for available upgrades.</source>
            <translation>При поиске доступных обновлений произошла ошибка.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has missing or corrupted files.</source>
            <translation>Не удалось выполнить обновление, выбранные обновления содержат утерянные или поврежденные файлы.</translation>
        </message>
        <message utf8="true">
            <source>Unable to perform upgrade, the selected upgrade has corrupted files.</source>
            <translation>Не удалось выполнить обновление, выбранные обновления содержат поврежденные файлы.</translation>
        </message>
        <message utf8="true">
            <source>Failed generating the USB upgrade data.</source>
            <translation>Ошибка формирования данных обновления USB.</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found, please check your settings and try again.</source>
            <translation>Обновления отсутствуют , проверьте настройки и повторите попытку .</translation>
        </message>
        <message utf8="true">
            <source>Upgrade attempt failed, please try again. If the problem persists contact your sales representative.</source>
            <translation>Сбой при попытке обновления. Повторите попытку. Если проблема не будет устранена, свяжитесь со своим торговым представителем.</translation>
        </message>
        <message utf8="true">
            <source>Test Set Lock</source>
            <translation>Блокировка прибора</translation>
        </message>
        <message utf8="true">
            <source>Locking the test set prevents unauthroized access. The screen saver is displayed to secure the screen.</source>
            <translation>Блокировка прибора предотвращает несанкционированный доступ. Заставка будет выведена на экран с целью защиты содержимого экрана.</translation>
        </message>
        <message utf8="true">
            <source>Lock test set</source>
            <translation>Заблокировать прибор</translation>
        </message>
        <message utf8="true">
            <source>A required password has not been set. Users will be able to unlock the test set without entering a password.</source>
            <translation>Не установлен необходимый пароль. Другие пользователи смогут разблокировать комплект испытаний, не вводя пароль.</translation>
        </message>
        <message utf8="true">
            <source>Password settings</source>
            <translation>Параметры пароля</translation>
        </message>
        <message utf8="true">
            <source>These settings are shared with the screen saver.</source>
            <translation>Данные параметры используются совместно с заставкой.</translation>
        </message>
        <message utf8="true">
            <source>Require password</source>
            <translation>Требуется пароль</translation>
        </message>
        <message utf8="true">
            <source>Audio</source>
            <translation>Параметры воспроизведения звука</translation>
        </message>
        <message utf8="true">
            <source>Speaker volume</source>
            <translation>Громкость динамиков</translation>
        </message>
        <message utf8="true">
            <source>Mute</source>
            <translation>Подавление звука</translation>
        </message>
        <message utf8="true">
            <source>Microphone volume</source>
            <translation>Громкость микрофона</translation>
        </message>
        <message utf8="true">
            <source>GPS</source>
            <translation>GPS</translation>
        </message>
        <message utf8="true">
            <source>Searching for Satellites</source>
            <translation>Поиск спутников</translation>
        </message>
        <message utf8="true">
            <source>Latitude</source>
            <translation>Широта</translation>
        </message>
        <message utf8="true">
            <source>Longitude</source>
            <translation>Долгота</translation>
        </message>
        <message utf8="true">
            <source>Altitude</source>
            <translation>Высота над уровнем моря</translation>
        </message>
        <message utf8="true">
            <source>Timestamp</source>
            <translation>Временная метка</translation>
        </message>
        <message utf8="true">
            <source>Number of satellites in fix</source>
            <translation>Количество спутников</translation>
        </message>
        <message utf8="true">
            <source>Average SNR</source>
            <translation>Среднее SNR</translation>
        </message>
        <message utf8="true">
            <source>Individual Satellite Information</source>
            <translation>Информация по отдельным спутникам</translation>
        </message>
        <message utf8="true">
            <source>System Info</source>
            <translation>Сведения о системе</translation>
        </message>
        <message utf8="true">
            <source>StrataSync</source>
            <translation>StrataSync</translation>
        </message>
        <message utf8="true">
            <source>Establishing connection to server...</source>
            <translation>Выполняется соединение с сервером ...</translation>
        </message>
        <message utf8="true">
            <source>Connection established...</source>
            <translation>Соединение установлено ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading files...</source>
            <translation>Получение файлов ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading files...</source>
            <translation>Загрузка файлов ...</translation>
        </message>
        <message utf8="true">
            <source>Downloading upgrade information...</source>
            <translation>Информация об обновлении загрузки ...</translation>
        </message>
        <message utf8="true">
            <source>Uploading log file...</source>
            <translation>Загрузка файла журнала ...</translation>
        </message>
        <message utf8="true">
            <source>Successfully synchronized with server.</source>
            <translation>Синхронизация с сервером выполнена успешно</translation>
        </message>
        <message utf8="true">
            <source>In holding bin. Waiting to be added to inventory...</source>
            <translation>В накопителе . Ожидание добавления на склад ...</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server.</source>
            <translation>Не удалось синхронизироваться с сервером .</translation>
        </message>
        <message utf8="true">
            <source>Connection lost during synchronization. Check network configuration.</source>
            <translation>Список соединений во время синхронизации . Проверьте конфигурацию сети .</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. Server is busy.</source>
            <translation>Не удалось синхронизироваться с сервером . Сервер занят .</translation>
        </message>
        <message utf8="true">
            <source>Failed to synchronize with server. System error detected.</source>
            <translation>Не удалось синхронизироваться с сервером . Обнаружена системная ошибка .</translation>
        </message>
        <message utf8="true">
            <source>Failed to establish a connection. Check network configuration or account ID.</source>
            <translation>Не удалось создать соединение . Проверьте конфигурацию сети или идентификатор учетной записи .</translation>
        </message>
        <message utf8="true">
            <source>Synchronization aborted.</source>
            <translation>Синхронизация прервана</translation>
        </message>
        <message utf8="true">
            <source>Configuration Data Complete.</source>
            <translation>Конфигурационные данные полные .</translation>
        </message>
        <message utf8="true">
            <source>Reports Complete.</source>
            <translation>Отчеты завершены .</translation>
        </message>
        <message utf8="true">
            <source>Enter Account ID.</source>
            <translation>Введите идентификатор учетной записи .</translation>
        </message>
        <message utf8="true">
            <source>Start Sync</source>
            <translation>Начать синхронизацию</translation>
        </message>
        <message utf8="true">
            <source>Stop Sync</source>
            <translation>Остановка синхронизации</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>Адрес сервера</translation>
        </message>
        <message utf8="true">
            <source>Account ID</source>
            <translation>Идентификатор учетной записи</translation>
        </message>
        <message utf8="true">
            <source>Technician ID</source>
            <translation>ID- Техник</translation>
        </message>
        <message utf8="true">
            <source>Configuration</source>
            <translation>Конфигурация</translation>
        </message>
        <message utf8="true">
            <source>Report</source>
            <translation> Отчет</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>Опция</translation>
        </message>
        <message utf8="true">
            <source>Reset to Defaults</source>
            <translation>Сброс к значениям по умолчанию</translation>
        </message>
        <message utf8="true">
            <source>Video Player</source>
            <translation>Видео проигрыватель</translation>
        </message>
        <message utf8="true">
            <source>Web Browser</source>
            <translation>Веб - браузер</translation>
        </message>
        <message utf8="true">
            <source>Debug</source>
            <translation>Отладка</translation>
        </message>
        <message utf8="true">
            <source>Serial Device</source>
            <translation>Последовательное устройство</translation>
        </message>
        <message utf8="true">
            <source>Allow Getty to control serial device</source>
            <translation>Позволить устройству Getty управлять последовательным устройством</translation>
        </message>
        <message utf8="true">
            <source>Contents of /root/debug.txt</source>
            <translation>Содержимое / корнев . каталог /debug.txt</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Испытания</translation>
        </message>
        <message utf8="true">
            <source>Set Up Screening Test</source>
            <translation>Настройка испытания экранирования</translation>
        </message>
        <message utf8="true">
            <source>Job Manager</source>
            <translation>Менеджер нарядов</translation>
        </message>
        <message utf8="true">
            <source>Job Information</source>
            <translation>Сведения о вакансии</translation>
        </message>
        <message utf8="true">
            <source>Customer Name</source>
            <translation>Имя абонента</translation>
        </message>
        <message utf8="true">
            <source>Job Number</source>
            <translation>Номер задания</translation>
        </message>
        <message utf8="true">
            <source>Test Location</source>
            <translation>Местоположение испытания</translation>
        </message>
        <message utf8="true">
            <source>Job information is automatically added to the test reports.</source>
            <translation>Информация о вакансии автоматически добавляется в тестовые отчеты.</translation>
        </message>
        <message utf8="true">
            <source>History</source>
            <translation>История</translation>
        </message>
    </context>
</TS>
