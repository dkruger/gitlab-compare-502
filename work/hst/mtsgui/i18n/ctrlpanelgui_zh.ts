<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CBertMobileCapabilityHardwarePropertyGenerator</name>
        <message utf8="true">
            <source>Mobile app capability</source>
            <translation>移动应用的功能</translation>
        </message>
        <message utf8="true">
            <source>Bluetooth and WiFi</source>
            <translation>蓝牙和 WiFi</translation>
        </message>
        <message utf8="true">
            <source>WiFi only</source>
            <translation>仅 WiFi</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CCpRestartRequiredLabel</name>
        <message utf8="true">
            <source>Please restart the test set for the changes to take full effect.</source>
            <translation>请重启测试设备，使所有更该全部生效。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CFileManagerScreen</name>
        <message utf8="true">
            <source>Paste</source>
            <translation>粘贴</translation>
        </message>
        <message utf8="true">
            <source>Copy</source>
            <translation>复制</translation>
        </message>
        <message utf8="true">
            <source>Cut</source>
            <translation>剪切</translation>
        </message>
        <message utf8="true">
            <source>Delete</source>
            <translation>删除</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>选择</translation>
        </message>
        <message utf8="true">
            <source>Single</source>
            <translation>单个</translation>
        </message>
        <message utf8="true">
            <source>Multiple</source>
            <translation>多个</translation>
        </message>
        <message utf8="true">
            <source>All</source>
            <translation>全部</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>无</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>Rename</source>
            <translation>重命名</translation>
        </message>
        <message utf8="true">
            <source>New Folder</source>
            <translation>新建文件夹</translation>
        </message>
        <message utf8="true">
            <source>Disk</source>
            <translation>磁盘</translation>
        </message>
        <message utf8="true">
            <source>Pasting will overwrite existing files.</source>
            <translation>粘贴将覆盖现有文件。</translation>
        </message>
        <message utf8="true">
            <source>Overwrite file</source>
            <translation>覆盖文件</translation>
        </message>
        <message utf8="true">
            <source>We are unable to open the file due to an unknown error.</source>
            <translation>发生未知错误，无法打开文件。</translation>
        </message>
        <message utf8="true">
            <source>Unable to open file.</source>
            <translation>无法打开文件。</translation>
        </message>
        <message utf8="true">
            <source>The selected file will be permanently deleted.</source>
            <translation>所选文件将永久删除。</translation>
        </message>
        <message utf8="true">
            <source>The %1 selected files will be permanently deleted.</source>
            <translation>%1所选文件将永久删除。</translation>
        </message>
        <message utf8="true">
            <source>Delete file</source>
            <translation>删除文件</translation>
        </message>
        <message utf8="true">
            <source>Please enter the folder name:</source>
            <translation>请输入文件夹名称：</translation>
        </message>
        <message utf8="true">
            <source>Create folder</source>
            <translation>创建文件夹</translation>
        </message>
        <message utf8="true">
            <source>Could not create the folder "%1".</source>
            <translation>无法创建文件夹“%1。”</translation>
        </message>
        <message utf8="true">
            <source>The folder "%1" already exists.</source>
            <translation>文件夹“%1”已经存在。</translation>
        </message>
        <message utf8="true">
            <source>Create fold</source>
            <translation>创建文件夹</translation>
        </message>
        <message utf8="true">
            <source>Pasting...</source>
            <translation>粘贴...</translation>
        </message>
        <message utf8="true">
            <source>Deleting...</source>
            <translation>删除...</translation>
        </message>
        <message utf8="true">
            <source>Completed.</source>
            <translation>完成.</translation>
        </message>
        <message utf8="true">
            <source>%1 failed.</source>
            <translation>%1 失败。</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste the file.</source>
            <translation>无法粘贴文件。</translation>
        </message>
        <message utf8="true">
            <source>Failed to paste %1 files.</source>
            <translation>无法粘贴%1文件。</translation>
        </message>
        <message utf8="true">
            <source>Not enough free space.</source>
            <translation>无足够的剩余空间。</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete the file.</source>
            <translation>无法删除文件。</translation>
        </message>
        <message utf8="true">
            <source>Failed to delete %1 files.</source>
            <translation>无法删除%1文件。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CHistoryScreen</name>
        <message utf8="true">
            <source>Back</source>
            <translation>返回</translation>
        </message>
        <message utf8="true">
            <source>Option</source>
            <translation>选项</translation>
        </message>
        <message utf8="true">
            <source>Time</source>
            <translation>时间</translation>
        </message>
        <message utf8="true">
            <source>Upgrade URL</source>
            <translation>升级 URL</translation>
        </message>
        <message utf8="true">
            <source>File Name</source>
            <translation>文件名</translation>
        </message>
        <message utf8="true">
            <source>Action</source>
            <translation>动作</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CModuleHardwareInfo</name>
        <message utf8="true">
            <source>Module</source>
            <translation>模块</translation>
        </message>
    </context>
    <context>
        <name>ui::COtherWirelessNetworkDialog</name>
        <message utf8="true">
            <source>Other wireless network</source>
            <translation>其他无线网络</translation>
        </message>
        <message utf8="true">
            <source>Enter the wireless network information below.</source>
            <translation>在下面输入无线网络信息。</translation>
        </message>
        <message utf8="true">
            <source>Name (SSID):</source>
            <translation>名称（SSID）：</translation>
        </message>
        <message utf8="true">
            <source>Encryption type:</source>
            <translation>加密类型：</translation>
        </message>
        <message utf8="true">
            <source>None</source>
            <translation>没有</translation>
        </message>
        <message utf8="true">
            <source>WPA-PSK</source>
            <translation>WPA-PSK</translation>
        </message>
        <message utf8="true">
            <source>WPA-EAP</source>
            <translation>WPA-EAP</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CStrataSyncScreen</name>
        <message utf8="true">
            <source>Upgrade available</source>
            <translation>可升级</translation>
        </message>
        <message utf8="true">
            <source>StrataSync has determined that an upgrade is available.&#xA;Would you like to upgrade now?&#xA;&#xA;Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>StrataSync 确定可以升级。 &#xA; 您希望现在升级吗？ &#xA;&#xA; 升级将终止所有运行测试。 在升级过程结束时，测试设备会自动重启完成升级。</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>遇到未知错误。请重试。</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>升级失败</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemInfoScreen</name>
        <message utf8="true">
            <source>Instrument info:</source>
            <translation>仪器信息：</translation>
        </message>
        <message utf8="true">
            <source>Base options:</source>
            <translation>基本选项：</translation>
        </message>
        <message utf8="true">
            <source>Reset instrument&#xA;to defaults</source>
            <translation>重设仪器&#xA;到默认值</translation>
        </message>
        <message utf8="true">
            <source>Export logs&#xA;to usb stick</source>
            <translation>导出日志 &#xA; 到 USB 磁盘</translation>
        </message>
        <message utf8="true">
            <source>Copy system&#xA;info to file</source>
            <translation>复制系统&#xA;信息到文件</translation>
        </message>
        <message utf8="true">
            <source>The system information was copied&#xA;to this file:</source>
            <translation>系统信息复制到该文件夹：</translation>
        </message>
        <message utf8="true">
            <source>This requires a reboot and will reset the System settings and Test settings to defaults.</source>
            <translation>需要重启，将重设系统设置和测试设置到默认值。</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>取消</translation>
        </message>
        <message utf8="true">
            <source>Reboot and Reset</source>
            <translation>重启和重设</translation>
        </message>
        <message utf8="true">
            <source>Log export was successful.</source>
            <translation>日志导出成功</translation>
        </message>
        <message utf8="true">
            <source>Unable to export logs. Please verify that a usb stick is properly inserted and try again.</source>
            <translation>不能导出日志。请确认 USB 磁盘已正确插入，并重试。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CSystemWindow</name>
        <message utf8="true">
            <source>%1 Version %2</source>
            <translation>%1版本%2</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CUpgradeScreen</name>
        <message utf8="true">
            <source>Start Over</source>
            <translation>启动</translation>
        </message>
        <message utf8="true">
            <source>Reset to Default</source>
            <translation>恢复默认设置</translation>
        </message>
        <message utf8="true">
            <source>Select your upgrade method:</source>
            <translation>选择升级方式：</translation>
        </message>
        <message utf8="true">
            <source>USB</source>
            <translation>U 盘</translation>
        </message>
        <message utf8="true">
            <source>Upgrade from files stored on a USB flash drive.</source>
            <translation>从存储在U 盘存储器上的文件升级。</translation>
        </message>
        <message utf8="true">
            <source>Network</source>
            <translation>网络</translation>
        </message>
        <message utf8="true">
            <source>Download upgrade from a web server over the network.</source>
            <translation>通过网络从网页服务器下载升级。</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the upgrade server:</source>
            <translation>输入升级服务器的地址：</translation>
        </message>
        <message utf8="true">
            <source>Server address</source>
            <translation>服务器地址</translation>
        </message>
        <message utf8="true">
            <source>Enter the address of the proxy server, if needed to access upgrade server:</source>
            <translation>如果需要通过代理服务器访问升级服务器，请输入代理服务器的地址：</translation>
        </message>
        <message utf8="true">
            <source>Type</source>
            <translation>类型</translation>
        </message>
        <message utf8="true">
            <source>Proxy address</source>
            <translation>代理服务器地址</translation>
        </message>
        <message utf8="true">
            <source>Connect</source>
            <translation>连接</translation>
        </message>
        <message utf8="true">
            <source>Query the server for available upgrades.</source>
            <translation>检索服务器可用升级。</translation>
        </message>
        <message utf8="true">
            <source>Previous</source>
            <translation>上一步</translation>
        </message>
        <message utf8="true">
            <source>Next</source>
            <translation>下一步</translation>
        </message>
        <message utf8="true">
            <source>Start Upgrade</source>
            <translation>开始升级</translation>
        </message>
        <message utf8="true">
            <source>Start Downgrade</source>
            <translation>开始降级</translation>
        </message>
        <message utf8="true">
            <source>No upgrades found</source>
            <translation>未找到升级</translation>
        </message>
        <message utf8="true">
            <source>Upgrade failed</source>
            <translation>升级失败</translation>
        </message>
        <message utf8="true">
            <source>Upgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the upgrade.</source>
            <translation>升级将终止所有运行测试。在升级过程结束时，测试设备会自动重启完成升级。</translation>
        </message>
        <message utf8="true">
            <source>Upgrade confirmation</source>
            <translation>升级确认</translation>
        </message>
        <message utf8="true">
            <source>Downgrading the test set is possible but not recommended. If you need to do this you are advised to call Viavi TAC.</source>
            <translation>可以对测试设备进行降级，但不建议进行该操作。如果您需要降级，建议您致电 Viavi TAC。</translation>
        </message>
        <message utf8="true">
            <source>Downgrading will terminate any running tests. At the end of the process the test set will reboot itself to complete the downgrade.</source>
            <translation>降级将终止所有运行中的测试。在降级过程结束时，测试设备会自动重启完成降级。</translation>
        </message>
        <message utf8="true">
            <source>Downgrade not recommended</source>
            <translation>不建议进行降级操作</translation>
        </message>
        <message utf8="true">
            <source>An unknown error was encountered. Please try again.</source>
            <translation>遇到未知错误。请重试。</translation>
        </message>
    </context>
    <context>
        <name>QObject</name>
        <message utf8="true">
            <source>System</source>
            <translation>系统</translation>
        </message>
    </context>
</TS>
