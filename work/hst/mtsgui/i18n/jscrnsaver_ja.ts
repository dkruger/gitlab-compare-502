<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CScreenSaverWindow</name>
        <message utf8="true">
            <source>This test set has been locked by a user to prevent unauthorized access.</source>
            <translation>このﾃｽﾄ ｾｯﾄは、許可されていないｱｸｾｽを防ぐ目的でﾕｰｻﾞーによりﾛｯｸされています。</translation>
        </message>
        <message utf8="true">
            <source>No password required, press OK to unlock.</source>
            <translation>ﾊﾟｽﾜｰﾄﾞは不要です。OK をｸﾘｯｸするとﾛｯｸ解除されます。</translation>
        </message>
        <message utf8="true">
            <source>Unlock test set?</source>
            <translation>ﾃｽﾄ ｾｯﾄをﾛｯｸ解除しますか?</translation>
        </message>
        <message utf8="true">
            <source>Please enter password to unlock:</source>
            <translation>ﾛｯｸ解除するにはﾊﾟｽﾜｰﾄﾞを入力してください:</translation>
        </message>
        <message utf8="true">
            <source>Enter password</source>
            <translation>ﾊﾟｽﾜｰﾄﾞを入力してください</translation>
        </message>
    </context>
</TS>
