<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>microscope</name>
        <message utf8="true">
            <source>Viavi Microscope</source>
            <translation>Microscopio Viavi</translation>
        </message>
        <message utf8="true">
            <source>Load</source>
            <translation>Carica</translation>
        </message>
        <message utf8="true">
            <source>Save</source>
            <translation>Salva</translation>
        </message>
        <message utf8="true">
            <source>Snapshot</source>
            <translation>Istantanea</translation>
        </message>
        <message utf8="true">
            <source>View 1</source>
            <translation>Visualizza 1</translation>
        </message>
        <message utf8="true">
            <source>View 2</source>
            <translation>Visualizza 2</translation>
        </message>
        <message utf8="true">
            <source>View 3</source>
            <translation>Visualizza 3</translation>
        </message>
        <message utf8="true">
            <source>View 4</source>
            <translation>Visualizza 4</translation>
        </message>
        <message utf8="true">
            <source>View FS</source>
            <translation>Visualizza FS</translation>
        </message>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Microscopio</translation>
        </message>
        <message utf8="true">
            <source>Capture</source>
            <translation>Acquisizione</translation>
        </message>
        <message utf8="true">
            <source>Freeze</source>
            <translation>Blocca</translation>
        </message>
        <message utf8="true">
            <source>Image</source>
            <translation>Immagine</translation>
        </message>
        <message utf8="true">
            <source>Brightness</source>
            <translation>Luminosità</translation>
        </message>
        <message utf8="true">
            <source>Contrast</source>
            <translation>Contrasto</translation>
        </message>
        <message utf8="true">
            <source>Screen Layout</source>
            <translation>Layout schermo</translation>
        </message>
        <message utf8="true">
            <source>Full Screen</source>
            <translation>Schermo intero</translation>
        </message>
        <message utf8="true">
            <source>Tile</source>
            <translation>Affianca</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>Esci</translation>
        </message>
    </context>
    <context>
        <name>scxgui::microscopeViewLabel</name>
        <message utf8="true">
            <source>Save Image</source>
            <translation>Salva immagine</translation>
        </message>
    </context>
    <context>
        <name>scxgui::OpenFileDialog</name>
        <message utf8="true">
            <source>Select Image</source>
            <translation>Seleziona immagine</translation>
        </message>
        <message utf8="true">
            <source>Image files (*.png)</source>
            <translation>File immagine (*.png)</translation>
        </message>
        <message utf8="true">
            <source>All files (*)</source>
            <translation>Tutti i file (*)</translation>
        </message>
        <message utf8="true">
            <source>Select</source>
            <translation>Seleziona</translation>
        </message>
    </context>
</TS>
