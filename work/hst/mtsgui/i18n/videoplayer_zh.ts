<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>videoplayer</name>
        <message utf8="true">
            <source>Viavi Video Player</source>
            <translation>Viavi 视频播放器</translation>
        </message>
        <message utf8="true">
            <source>Quit</source>
            <translation>退出</translation>
        </message>
        <message utf8="true">
            <source>Video File Name</source>
            <translation>视频文件名</translation>
        </message>
        <message utf8="true">
            <source>Time:</source>
            <translation>时间:</translation>
        </message>
        <message utf8="true">
            <source>Video Status</source>
            <translation>视频姿态</translation>
        </message>
        <message utf8="true">
            <source>00:00:00 / 00:00:00</source>
            <translation>00:00:00 / 00:00:00</translation>
        </message>
    </context>
    <context>
        <name>scxgui::MediaPlayer</name>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>比例与收获</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>刻度</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
    </context>
    <context>
        <name>scxgui::videoplayerGui</name>
        <message utf8="true">
            <source>Open &amp;File...</source>
            <translation>打开 &amp; 文件…</translation>
        </message>
        <message utf8="true">
            <source>Aspect ratio</source>
            <translation>长宽比   </translation>
        </message>
        <message utf8="true">
            <source>Auto</source>
            <translation>自动</translation>
        </message>
        <message utf8="true">
            <source>Scale</source>
            <translation>刻度</translation>
        </message>
        <message utf8="true">
            <source>16/9</source>
            <translation>16/9</translation>
        </message>
        <message utf8="true">
            <source>4/3</source>
            <translation>4/3</translation>
        </message>
        <message utf8="true">
            <source>Scale mode</source>
            <translation>比例模式</translation>
        </message>
        <message utf8="true">
            <source>Fit in view</source>
            <translation>适合视图</translation>
        </message>
        <message utf8="true">
            <source>Scale and crop</source>
            <translation>比例与收获</translation>
        </message>
        <message utf8="true">
            <source>Open File...</source>
            <translation>打开文件…</translation>
        </message>
        <message utf8="true">
            <source>Multimedia (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</source>
            <translation>多媒体 (*.avi *.mp4 *.mpeg *.mpg *.mpe *.mp2 *.mp3 *.aac *.m4a *.m4p *.aif *.wav)</translation>
        </message>
        <message utf8="true">
            <source>Open</source>
            <translation>打开</translation>
        </message>
        <message utf8="true">
            <source>No Open Media</source>
            <translation>未打开介质</translation>
        </message>
        <message utf8="true">
            <source>Stopped</source>
            <translation>停止</translation>
        </message>
        <message utf8="true">
            <source>Loading...</source>
            <translation>加载</translation>
        </message>
        <message utf8="true">
            <source>Buffering...</source>
            <translation>缓冲...</translation>
        </message>
        <message utf8="true">
            <source>Playing...</source>
            <translation>播放…</translation>
        </message>
        <message utf8="true">
            <source>Paused</source>
            <translation>暂停</translation>
        </message>
        <message utf8="true">
            <source>Error...</source>
            <translation>差错...</translation>
        </message>
        <message utf8="true">
            <source>Idle - Stopping Media</source>
            <translation>空闲 - 停止媒介</translation>
        </message>
    </context>
</TS>
