<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS>
    <context>
        <name>Tasks</name>
        <message utf8="true">
            <source>Microscope</source>
            <translation>Mikroskop</translation>
        </message>
        <message utf8="true">
            <source>Tests</source>
            <translation>Prüfungen</translation>
        </message>
        <message utf8="true">
            <source>PowerMeter</source>
            <translation>PowerMeter</translation>
        </message>
        <message utf8="true">
            <source>System</source>
            <translation>System</translation>
        </message>
    </context>
</TS>
