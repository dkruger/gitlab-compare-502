<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" sourcelanguage="en">
    <context>
        <name>scxgui::CPowerOffProcess</name>
        <message utf8="true">
            <source>Power off</source>
            <translation>電源ｵﾌ</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set shuts down.</source>
            <translation>ﾃｽﾄ ｾｯﾄが終了するまでお待ちください。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CRebootProcess</name>
        <message utf8="true">
            <source>Reboot</source>
            <translation>再起動</translation>
        </message>
        <message utf8="true">
            <source>Please wait while the test set reboots.</source>
            <translation>ﾃｽﾄ ｾｯﾄが再起動するまでお待ちください。</translation>
        </message>
    </context>
    <context>
        <name>scxgui::CShutdownPrompt</name>
        <message utf8="true">
            <source>Power options</source>
            <translation>電源ｵﾌﾟｼｮﾝ</translation>
        </message>
        <message utf8="true">
            <source>Cancel</source>
            <translation>ｷｬﾝｾﾙ</translation>
        </message>
    </context>
</TS>
